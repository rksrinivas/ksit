<@page>
    
	
	
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <!-- start blog archive  -->
              <div class="row">
              
               <!-- start single blog archive -->
              	 
              	 <#list teleDepartmentAchieversList as teleDepartmentAchievers>
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${teleDepartmentAchievers.imageURL!}">
                      </a>
                    </div>
                    <h2 class="blog_title">${teleDepartmentAchievers.heading!}</h2>
					
                    <div class="blog_commentbox">
                      <p>${teleDepartmentAchievers.eventDate!}</p>
                                            
                    </div>
					
                    <p class="blog_summary">${teleDepartmentAchievers.content!}</p>
					
					<!--<a class="blog_readmore" href="#">Read More</a>-->
                    
                  </div>
                </div>
                
                </#list>
                <!-- start single blog archive -->
			  
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/tele/pruthvi.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Name</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					Mr. Pruthvi Raj and Mr. Thejas S B won the best paper award in their session, and Prof. Srividya. R has guided their paper entitled Structural monitoring and Bridge Exploration at one day National Level Symposium on Computer Technology and Networking organized by Department of Computer Science and Engineering on 1st December 2016.
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				
				
				
				
				
              </div>
              <!-- end blog archive  -->
             
            </div>
          </div>
          <!-- End course content -->

         
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>    