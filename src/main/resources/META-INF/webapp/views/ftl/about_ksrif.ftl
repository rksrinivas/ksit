<@page>
   

   <section id="about">
	<div class="container" >
	<div class="news_area home-4">
		<div class="row">
			<div class="col-md-12">
			<h1 class="titile text-center">Welcome to K S Research and Innovation Foundation</h1>

  
		 
          <div class="aboutus_area wow fadeInLeft">
		 
        <!--    <p>
            	The Kammavari Sangham, a multi-activity non-profit oriented voluntary service organization, was established in the year 1952 with the sole objective of providing charitable service to community and society. 
            	The Sangham has diversified its activities since its establishment over five decades ago. With a firm belief that quality and meaningful education only can lay the strong foundation for bringing about economic and social changes to the lives of thousand, the Sangham went about establishing educational institutions, starting with K.S. Polytechnic in 1992.
            </p>
            <p>
				Enthused with this success of its foray into technical education, the Sangham moved forward by starting the K.S School of Engineering and Management (KSSEM) in the year 2010.
			</p>
			<p>
				The campus of K S School of Engineering and Management, nestled in a quiet location off the Kanakapura Main Road, provides good facilities that are required for quality technical  education. 
				KSSEM's strength lies in its good vision; dedicated, experienced and well qualified teaching staff; the establishment with recent equipment in the laboratories and the interactive relationship that it has forged with the industry; all with the active support of an eminent Management. 
				KSSEM is offering six Under Graduate courses namely Civil Engineering, Computer Science and Engineering, Electronics and Communication Engineering, Artificial Intelligence and Data Science, Computer Science & Business System and Mechanical Engineering. It is also offering M Tech course in Structural Engineering and Masters in Business Administration.
			</p>
		  </div>
		  <div class="title_area text-dark">
 	        <h2 class="titile title-red fs-20"><span class="material-icons" style="font-size:24px; color:#0c2b5a;">school</span>VISION</h2>
			
			<ul class="list" style="list-style: linear-gradient(#0c2b5a, #0066ff); color:#0c2b5a;">
				<li>To impart quality education in engineering and management to meet technological business and societal needs through holistic education and research.</li>
			</ul>
			<h2 class="titile title-red fs-20"><span class="material-icons" style="font-size:24px; color:#0c2b5a;">workspace_premium</span>MISSION</h2>
			<h2 class="title text-left fs-20">K. S. School Of Engineering and Management shall,</h2>
			
			<ul class="list" style="list-style: linear-gradient(#0c2b5a, #0066ff); color:#0c2b5a;">
			
				<li>Establish state-of-art infrastructure to facilitate effective dissemination of technical and managerial knowledge.</li>
				<li>Provide comprehensive educational experience through a combination of curricular and experiential learning, strengthened by industry-institute interaction.</li>
				<li>Pursue socially relevant research and disseminate knowledge.</li>
				<li>Inculcate leadership skills and foster entrepreneurial spirit among students.</li>
			</ul> -->
          </div>
        </div>
		</div>
		</div>
	</section>
   
    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs" style="margin-top:0;">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        <!-- <div class="media_mask" style="background-color:pink;opacity:0.8;height:auto;">-->
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="newsfeed_area wow fadeInRight">

            <!-- Tab panes -->
            <div class="tab-content">
              <!-- Start news tab content -->
            <div class="tab-pane fade in active" id="news">                
               
				
				
				
                    
					   
						<div class="row">
							
						<div class="aboutus_area wow fadeInLeft">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<img class="img-ksrif" src="${img_path!}/ksrif/ksriflogo.webp" alt="image" />
								<!--<p class="media_text"><span class="title-blue">The Kammavari Sangham</span>, a multi-activity non-profit oriented voluntary service organization,
								 was established in the year 1952 with the sole objective of providing charitable service to community and society. 
								 The Sangham has diversified its activities since its establishment over five decades ago. With a firm belief that quality and 
								 meaningful education only can lay the strong foundation for bringing about economic and social changes to the lives of thousand, 
								 the Sangham went about establishing educational institutions, starting with K.S. Polytechnic in 1992. Enthused with this success 
								 of its foray into technical education, the Sangham moved forward by starting the K. S Institute Of Technology (KSIT).
								  Its Engineering College in the year 1999. In the following years both these institutions 
								  have carved for themselves an enviable niche through academic excellence achieved in 
								  a very short span of time.</p> -->
								  	<p>The K S Management in April 2023 took a strategic decision to form the KS Research and Innovation Foundation (KSRIF).</p>
								  	<p>The KS Research and Innovation Foundation focuses on interdisciplinary research and innovation, creating dynamic hubs for collaboration among students, faculty, and industry professionals.</p>
									<p>The Objectives of K S Research and Innovation Foundation is centered around creating a conducive environment that fosters research and innovation with social impact. 	</p>
									<p>The foundation aims to improve the quality of Research and Innovation for faculty members. Motivate them to engage in Consultancy projects for the industry.</p>
									<p>It promotes National and International collaborations for Research and Innovation.</p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="${img_path!}/ksrif/about_KSRIF.webp" alt="image" />
								</div>
							</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
								
							
							<h1 class="  text-blue">Vision</h1>
			
								<ul class="list" style="list-style: linear-gradient(#0c2b5a, #0066ff); color:#0c2b5a;" >
								<li>K S Research and Innovation Foundation will provide the platform and support for doing Research and Innovation for the K S Group of Institutions.</li>
								</ul>

							
							<h1 class="  text-blue">Mission</h1>
			
								<ul class="list" style="list-style: linear-gradient(#0c2b5a, #0066ff); color:#0c2b5a;" >
									<li>Create an environment and infrastructure for research, and innovation.</li>
									<li>Attract researchers and innovators.</li>
									<li>Write proposals, publish in top journals.</li>
									<li>Produce value for Industry and Society.</li>
								</ul>

							
							<h1 class="  text-blue">Values</h1>
								
								<ul class="list" style="list-style: linear-gradient(#0c2b5a, #0066ff); color:#0c2b5a;" >
									<li>Excellence</li>
									<li>Collaboration</li>
									<li>Integrity & Honesty</li>
									<li>Accountability</li>
									<li>Diversity</li>
									<li>Equity & Inclusion</li>
								</ul>
					   </div>
					</div>
				</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
	</section>	   
					   
							
			<!--content -->
	<section id="courseArchive" style="margin-top:0px;">
      <div class="container-fluid">
	  
	  <div class="row">
					
		<div class="col-lg-12 col-md-12 col-sm-12">
		<h2 class="titile text-center text-blue fs-20">Meet Our Research Mavericks</h2>
				
		
			
			<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="">
						<img class="img-responsive" src="${img_path!}/ksrif/Dr._Pradeep_Desai.webp" alt="image" />
					</div>
													
					<h6 class="text-blue text-center">Dr. Pradeep Desai</h6>
					<h6 class="color-red text-center">BE, M.Tech, Ph.D</h6>
					<h6 class="text-blue text-center">Professor</h6>
													
			</div>
												
			<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="">
						<img class="img-responsive" src="${img_path!}/ksrif/Dr._Swamy_D_R.webp" alt="image" />
					</div>
													
					<h6 class="text-blue text-center">Dr. Swamy D R</h6>
					<h6 class="color-red text-center">B.E, M.Tech, Ph.D </h6>
					<h6 class="text-blue text-center">Professor</h6>
													
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="">
						<img class="img-responsive" src="${img_path!}/ksrif/Dr._M_R_Bhatt.webp" alt="image" />
					</div>
													
					<h6 class="text-blue text-center">Dr. M R Bhatt</h6>
					<h6 class="color-red text-center">B.E, M.Tech, Ph.D </h6>
					<h6 class="text-blue text-center">Professor</h6>
													
			</div>
												
			
																					
												
		</div>
	<!-- members-->


</section>
			
			  
			  
             
			  
			  
             

	
	
	
</@page>