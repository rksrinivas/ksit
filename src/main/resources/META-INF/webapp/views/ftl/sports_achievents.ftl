<@page>
			    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3>SPORTS ACHIEVERS</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>SPORTS ACTIVITES / ACHIEVEMENTS IN THE YEAR –2015-16</p>
					  				  
                    </blockquote>
                    
							
                <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Badminton</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> SJBIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 21st & 22nd August 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Lost in second round</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Chess</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> HKBK Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 30th & 31st August 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	SWIMMING</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> BMSCE  Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 23rd sept 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men & Women</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	CRICKET</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> Vtu cricket  Team Selection Trails</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> SJBIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 28th & 29th sept 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Net Ball</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> Vtu Net Ball(Women) Team Selection Trails</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> KLECET CHIKKODI</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 5th & 6th October 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Women</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Throw Ball</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> KSIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 8th & 9th October 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Women</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Organized Successfully & Our College secured 2nd place</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kabaddi</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> Sir MVIT kalanjali</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> Sir MVIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 16th October 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Women</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Our College secured 1st place</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kabaddi</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> PESIT</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> PESIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 27th to 30th October 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Our College secured 2nd place</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Power lifting</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> GAT  Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 29th to 31st October 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Women</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Secured 1 silver & 1bronze medals</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kabaddi</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> Noble school of business</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> Noble school of business bengaluru</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 22nd & 23rd January 2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> our college team qualified for Semi Finals</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kabaddi</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> Noble school of business</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> Noble school of business bengaluru</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 22nd & 23rd January 2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Women</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Our College secured 1st place</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kho-Kho</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> GAT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 8th & 9th March  2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> our college team qualified for Semi Finals</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Cricket</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> SJBIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 2nd March  2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated.</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Vtu Kho-Kho Team SelectionTrails</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> NCE Devana halli</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 12th &13th March 2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated.</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Athletic</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> Sir MVIT Bengaluru</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 23th to 27th March  2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> M & W</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Volley ball</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> DBIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 29th feb & 1st march  2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated.</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Foot ball</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> SJBIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 11th & 12th april 2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Participated.</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kabaddi</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> Sai Ram CE</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> Sai Ram CE Anekal</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 12th &13th april 2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Women</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Our College secured 1st place</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kabaddi</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span>VTU </h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span>Nandi IT </h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 21th &22th april 2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> our college team qualified for Semi Finals</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Kabaddi</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> Sai Ram CE</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span>Sai Ram CE Anekal </h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 12th &13th april 2016	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> our college team qualified for Semi Finals</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				   
				
				
				
			

			</div>
			
			 <div class="row">
					<h3>SPORTS ACHIEVERS</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>SPORTS ACTIVITES / ACHIEVEMENTS IN THE YEAR –2015-16</p>
					  				  
                    </blockquote>
                    
							
                <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>EVENTS: </span>	Badminton</h3>
					<h3 class="singCourse_title"><span>VTU OR INTER COLLEGIATE TOURNAMENT: </span> VTU</h3>
					<h3 class="singCourse_title"><span>HOST COLLEGE’S NAME: </span> SJBIT Bangalore</h3>
					<h3 class="singCourse_title"><span>EVENT DATES: </span> 21st & 22nd August 2015	</h3>
					<h3 class="singCourse_title"><span>M or W: </span> Men</h3>
					
					<h3 class="singCourse_title"><span>REMARKS: </span> Lost in second round</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
			</div>
			
		 </div>
		</div>
	</div>
  </div>
</section>  
</@page>