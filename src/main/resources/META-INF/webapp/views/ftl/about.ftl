<@page>
    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs" style="margin-top:0;">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        <!-- <div class="media_mask" style="background-color:pink;opacity:0.8;height:auto;">-->
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="newsfeed_area wow fadeInRight">
            <ul class="nav nav-tabs feed_tabs" id="myTab2">
              <li class="active"><a href="#news" data-toggle="tab"><span style="color:darkblue">&#9656; History</span></a></li>
			  <!--<li><a href="#events" data-toggle="tab">Members</a></li> -->
			  <!--
              <li><a href="#notice" data-toggle="tab">Sangham</a></li>
			  <li><a href="#events" data-toggle="tab">Events</a></li>-->    
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <!-- Start news tab content -->
              <div class="tab-pane fade in active" id="news">                
                <ul class="news_tab">
				<li>
                    <div class="media">
                      <div class="media-body">
                       <a href="#"><p></p></a>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="${img_path!}/about/about.jpeg" alt="image" />
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6">
								<p id="news"></p>
								<p class="media_text"><span style="color:darkblue;">The Kammavari Sangham</span>, a multi-activity non-profit oriented voluntary service organization,
								 was established in the year 1952 with the sole objective of providing charitable service to community and society. 
								 The Sangham has diversified its activities since its establishment over five decades ago. With a firm belief that quality and 
								 meaningful education only can lay the strong foundation for bringing about economic and social changes to the lives of thousand, 
								 the Sangham went about establishing educational institutions, starting with K.S. Polytechnic in 1992. Enthused with this success 
								 of its foray into technical education, the Sangham moved forward by starting the K.S Institute of Technology (KSIT).
								  Its Engineering College in the year 1999. In the following years both these institutions 
								  have carved for themselves an enviable niche through academic excellence achieved in 
								  a very short span of time.</p>
								  <p> By providing FREE hostel accommodation and scholarship to the deserving students in the community, 
						it has furthered its Commitment to education.</p>
								</div>
					<div class="col-lg-12 col-md-12 col-sm-12">	
						<p></p>
						<p>Apart from the educational initiatives, on the cultural front, the Sangham has
					     ventured to build and manage Convention centers. These serve as venues for community cultural events including conducting of mass marriages among the economically underprivileged in the Kamma Community.	
					   	<p>Towards encouraging and nourishing entrepreneurial trait among the community members, 
					   the Sangham also started Kammavari Credit Co-operative Society to mobilize deposits and finance enterprises.
					   Furthermore, to be of service to the sick and the suffering, the Sangham has established a modern hospital in Bangalore with the latest diagnostic and treatment services.</p>
					   	<p>K. S. Institute of Technology, KSIT in short, was born on 11th October 1999, out of a commitment to the cause of providing value based technical education. Its strength lies in its founding objectives, the eminence of its management who are experienced men drawn from various fields, the coming together of dedicated, experienced and well qualified teaching staff, the establishment of a modern infrastructure with state-of-art equipment and laboratory facilities and the interactive relationship its forged with the industry.</p>
					   	<p>Excellent academic performance in the University examinations in all the years so far gives us
					    the impetus to embark upon post graduate programmes and the establishment of a research cell in the near future. Establishment of these institutions will be a quick reality with hundreds of the Sangham members contributing generously.</p>
					   	<p>Set on the mission to excel in every activity that the Sangham has embarked upon, KSIT carries forward the Sanghams vision under an eminent Governing Council, and an inspiring academic leadership translated in the enviable academic performance of its students.</p>
					   	<p>The objective of the management committee of KSIT is to impart quality education and to help students develop the abilities of problem solving, creative thinking and adaptability in their chosen field.</p>
					   	<p>Realizing the importance of technical education, the energetic, dedicated and visionary members of the Kammavari Sangham under the leadership of our President Sri. R. Rajagopal Naidu, are executing their responsibilities sincerely for the growth of the institution since its establishment.</p>
					   
					</div>		   	
					</div> 
                    </div>
                    </div>                    
                  </li>
                 </ul>        
           <!--     <a class="see_all" href="#">See All</a>-->
              </div>

              <!-- Start events tab content -->
              <div class="tab-pane fade " id="events">
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      <div class="media-body">
                       <a href="#"><p></p></a>
						<div class="row">	   
							<section id="whyUs" style="margin-top:0%;">
							  <!-- Start why us top -->
							  <div class="row">        
								<div class="col-lg-12 col-sm-12">
								  <div class="whyus_top" style="background-color:transparent;">
									<div class="container">
									 
									  <!-- Start Why us top content  -->
											<div class="row">
												<div class="col-lg-4 col-md-6 col-sm-6">
													<div class="">
														<img class="img-responsive" src="${img_path!}/dummy.jpeg" alt="image" />
													</div>
													<p class="image_caption"><span style="color: darkblue;">Sri. R. Rajagopal Naidu</span></p>
													<p class="image_caption"><span style="color: red;">President<span></p>
												</div>
												
												<div class="col-lg-4 col-md-6 col-sm-6">
													<div class="">
														<img class="img-responsive" src="${img_path!}/dummy.jpeg" alt="image" />
													</div>
													<p class="image_caption"><span style="color: darkblue;">Sri. R. Leela Shankar Rao</span></p>
													<p class="image_caption"><span style="color: red;">Hon. Secretary</span></p>
												</div>
												<div class="col-lg-4 col-md-6 col-sm-6">
													<div class="">
														<img class="img-responsive" src="${img_path!}/dummy.jpeg" alt="image" />
													</div>
													<p class="image_caption"><span style="color: darkblue;">Sri. T. Neerajakshulu</span></p>
													<p class="image_caption"><span style="color: red;">Treasurer</span></p>
												</div>
											</div>
									  <!-- End Why us top content  -->
									</div>
								  </div>
								</div>        
							  </div>
							<!-- End why us top -->

							<!-- list of committee members-->					  
							  
								<div class="container">
									<div class="row">
									 <div class="col-md-12 col-lg-12 col-sm-12">
									</div>
								   </div>
								</div>
							<!-- end table-->
										
							<!--<a class="" href="administration.html" style="margin-left:67%;color:#fff;">Click here to see details </a>-->	
									  </div>
									</div>            
								  </div>
								</div>        
							  </div>
							<!-- End why us bottom -->
							</section>
							<!--=========== END WHY US SECTION ================-->
						</div>
                       
                      </div>
                    </div>                    
                  </li>            
                </ul>
              </div>
            </div>
          </div>
        </div>
		<!--<div class="col-lg-12 col-md-12 col-sm-12">
          <div class="aboutus_area wow fadeInLeft">
            <h2 class="titile">Highlights of department</h2>	
			<p>Young, dynamic and energetic faculty.</p>
			</div>
        </div>-->
      </div>
      </div>
    </section>
<!--=========== END ABOUT US SECTION ================--> 
</@page>