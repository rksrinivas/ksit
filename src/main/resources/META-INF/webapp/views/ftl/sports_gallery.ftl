<@page>
	<!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
            
            	 <#list sportsGalleryList as sportsGallery>
				
					<a href="${sportsGallery.imageURL!}" title="">
	                    <img class="gallery_img" src="${sportsGallery.imageURL!}" alt="img" />
	                	<span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
	                </a>
				
				</#list>
            
                <a href="${img_path!}/sports/sp1.jpg" title="This is Title">
                  <img class="gallery_img" src="${img_path!}/sports/sp1.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp2.jpg" title="This is Title2">
                  <img class="gallery_img" src="${img_path!}/sports/sp2.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp3.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/sports/sp3.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp4.jpg" title="This is Title4">
                  <img class="gallery_img" src="${img_path!}/sports/sp4.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp5.jpg" title="This is Title5">
                  <img class="gallery_img" src="${img_path!}/sports/sp5.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp6.jpg">
                  <img class="gallery_img" src="${img_path!}/sports/sp6.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp7.jpg">
                  <img class="gallery_img" src="${img_path!}/sports/sp7.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp8.jpg">
                  <img class="gallery_img" src="${img_path!}/sports/sp8.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                 <a href="${img_path!}/sports/sp9.jpg" title="This is Title">
                  <img class="gallery_img" src="${img_path!}/sports/sp9.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp10.jpg" title="This is Title2">
                  <img class="gallery_img" src="${img_path!}/sports/sp10.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp11.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/sports/sp11.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp12.jpg" title="This is Title4">
                  <img class="gallery_img" src="${img_path!}/sports/sp12.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/gallery/img-large13.jpg" title="This is Title5">
                  <img class="gallery_img" src="${img_path!}/sports/sp13.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp14.jpg">
                  <img class="gallery_img" src="${img_path!}/sports/sp14.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp15.jpg">
                  <img class="gallery_img" src="${img_path!}/sports/sp15.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/sports/sp16.jpg">
                  <img class="gallery_img" src="${img_path!}/sports/sp16.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>