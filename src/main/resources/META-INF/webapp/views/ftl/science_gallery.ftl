<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <!--<section id="imgBanner">
	  <h2>Gallery</h2>
	  <h2>Science Department</h2>
      
    </section>-->
    <!--=========== END COURSE BANNER SECTION ================-->
    
    <!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
            
             <#list scienceGalleryList as scienceGallery>
				
					<a href="${scienceGallery.imageURL!}" title="">
	                    <img class="gallery_img" src="${scienceGallery.imageURL!}" alt="img" />
	                	<span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
	                </a>
				
				</#list>
            
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>