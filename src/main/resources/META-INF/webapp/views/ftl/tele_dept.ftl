<@page>
<#include "feedback/feedback_list_modal.ftl">
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#if telePageSliderImageList?has_content>
                     		<#list telePageSliderImageList as telePageSliderImage>
                     			<#if telePageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${telePageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">${telePageSliderImage.heading}</p>
										<p class="slideInRight">${telePageSliderImage.content}</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>
	                        <article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/tele/slider/s41.jpg" alt="">                       
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/tele/slider/s1.jpg" alt="">                          
	                        </article>
	                        
	                         <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/tele/slider/s2.jpg" alt="">                          
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/tele/slider/s3.jpg" alt="">                          
	                        </article>
						</#if>
                    
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 

<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 

	  <!-- upcoming and latest news list-->
      
	<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at TCE
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
					
				<div class="col-md-12">
					<div class="row">
					
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								
								<h2 class="outline">Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=telePageLatestEventsList/>																			
									</div>
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline">Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
									<@eventDisplayMacro eventList=telePageUpcomingEventsList/>
								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->
	
	<!-- welcome -->
	<div class="dept-title">
	   <h1>Welcome To Electronics and TeleCommunication Engineering</h1>
	   <p class="welcome-text">Telecommunications Engineering is an engineering discipline centered on electrical and 
	   computer engineering which seeks to support and enhance telecommunication systems.</p>
	</div>
	<!-- welcome -->
	


<!--dept tabs -->
<section id="vertical-tabs">
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
         <div class="vertical-tab padding" role="tabpanel">
          
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
               <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="dept-cart">Profile</a></li>
               <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision & Mission</a></li>
               <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>
              
                <li role="presentation"><a href="#professional_bodies" aria-controls="professional_bodies" role="tab" data-toggle="tab" class="dept-cart">Professional Bodies</a></li>
               <li role="presentation"><a href="#professional_links" aria-controls="professional_links" role="tab" data-toggle="tab" class="dept-cart">Professional Links</a></li>
              
              
               <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
               
               
               <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Time Table</a></li>
               
               <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>
               
                              
               <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li>
               <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
              
               <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li>
              
              
               <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab" class="dept-cart">Achievers</a></li>
               <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
               <li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
               <li role="presentation"><a href="#library_dept" aria-controls="library" role="tab" data-toggle="tab" class="dept-cart">Departmental Library</a></li>
               <li role="presentation"><a href="#placement_dept" aria-controls="placement" role="tab" data-toggle="tab" class="dept-cart">Placement Details</a></li>
               
               <li role="presentation"><a href="#higher_education" aria-controls="higher_education" role="tab" data-toggle="tab" class="dept-cart">Higher Education</a></li>

               <li role="presentation"><a href="#student_club" aria-controls="student_club" role="tab" data-toggle="tab" class="dept-cart">Student Club</a></li>


               <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab" class="dept-cart">Internships & Projects </a></li>
               
               <li role="presentation"><a href="#industrial_visit" aria-controls="industrial_visit" role="tab" data-toggle="tab" class="dept-cart">Industrial Visit</a></li>
             
             
               
               <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
               <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
               
               
         
  
               <li role="presentation"><a href="#project_exhibition" aria-controls="project_exhibition" role="tab" data-toggle="tab" class="dept-cart">Project Exhibition</a></li>
               
               <li role="presentation"><a href="#mou-signed" aria-controls="mou-signed" role="tab" data-toggle="tab" class="dept-cart">MOUs Signed</a></li>
               <li role="presentation"><a href="#alumni-association" aria-controls="alumni-association" role="tab" data-toggle="tab" class="dept-cart">Alumni Association</a></li>
               
               <li role="presentation"><a href="#social_activities" aria-controls="social_activities" role="tab" data-toggle="tab" class="dept-cart">Social Activities</a></li>
               
               <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="tab" class="dept-cart">Testimonials</a></li>
                                 
               
               <!-- <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li> -->
            </ul>
            <!-- Nav tabs -->
            <!-- Tab panes -->
            <div class="tab-content tabs">
            
                  	
            	
               <!-- profile -->
               <div role="tabpanel" class="tab-pane fade in active" id="profile">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Profile</h3>
                     <p>Telecommunication Engineering program was started in the year 2000. The key
						focus of the department is to inculcate strong fundamentals of engineering &amp;
						produce futuristic graduates.</p>
						
						<p>This objective based course aid student to achieve their purpose. The course
						is accredited by NAAC. Aspirants get admitted to this course through Common
						Entrance Test and COMED-K conducted by the State government.</p>
						
						<p>The department has total of 9 teaching faculty members with profuse
						specializations, out of which 1 is PhD holder and 5 are pursuing PhD. Faculties of
						the department have exceptional teaching experience. Also there are 3 technical
						staff with adequate technical knowledge. The staff and student ratio of the
						department is 1:20. The department has excellent infrastructure with state of art
						laboratories.</p>
						<p>Our department has a record of achieving 100% final semester results
						numerous times. On an average all semester results of the department is around
						90%. The department has memorandum of understanding (MOU) with companies
						like TENET TECHNOTRONICS, LIVEWIRE and INVERSA TECHNO SOFT
						pvt ltd.</p>
						<p>Placement statistics of the department is exceptional and our students have
						been placed in major companies like CGI, COGNIZANT, TCS DIGITAL,
						INFOSYS, ADVENT GLOBAL SOLUTIONS, ABC TECHNOLOGY, WIPRO
						and many more such companies.</p>
                     
			       </div>
                  <h3>Head Of The Department Desk</h3>
                  <div class="row aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
						<img class="hod-img img-responsive" src="/img/tele/telecom-hod-new.jpg" alt="image">          
						<div class="qualification_info">
	                        <h3 class="qual-text">Dr. Chanda V Reddy</h3>
	                        <h4 class="qual-text">Professor & Head</h4>
	                        <h3 class="qual-text">B.E, M.Tech, Ph.D</h3>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>It gives me great pleasure and honor to write about the efforts that were put on and the milestones that were achieved by the department of TCE. The course of Telecommunication Engineering was introduced in KSIT curriculum in the year 2000.
						<p>I appreciate the teachers and students for conducting various workshops, conferences, guest lectures and Industrial visits to widen the wisdom wall of the students and help them with various other positive aspects.</p>
						<p>Predicting from the level of passion and commitment shown by the faculties and students, to learn, explore and innovate, I believe that our budding engineers are ready to conquer the world. Department has MOUs signed with companies such as Viswa Jyothi Technologies and so on to provide students with a platform to innovate and invent in the field of Telcommunication .</p></p>
                     </div>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     	
                     </div>
                  </div>
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Highlights of department</h3>
                       <ul class="list">
                       		<li>100% results of 4th year students, successively for 3 years</li>
                       		<li>Departmental Library with adequate books</li>
                       		<li>Laboratories with accurate and high quality instruments and computers</li>
                       		<li>Skilled technical staff</li>
                       		<li>Seminar hall with a good attire</li>
                       		<li>Adequate teaching Abet</li>
                       		<li>Vanquish a technical event
                       		 conducted every year as an inspiration or tribute to great personalities in the field of inventions and innovations</li>
                       </ul>
                  
                     
                  </div>
   
               </div>
               <!-- profile -->
               
               <!-- vision and mission -->
               <div role="tabpanel" class="tab-pane fade in" id="vision">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Vision</h3>
				
						<ul class="list">
							<li>To bestow quality education in Electronics &  Telecommunication 
							Engineering endowed with human values to secure a better 
							connected society.</li>
						</ul>
						<button type="button" style="float:right;" class="btn btn-info btn-sm" data-backgrop="static" data-toggle="modal" data-page="tele_page" data-target="#feedbackListModal">Feedback</button>
						
				 		 <h3>Mission</h3>
						
						<ul class="list">
						<li>To provide effective pedagogy for building and realizing the     required knowledge and skills
						 that constitute a telecom professional.</li>
						<li> To impart learning through innovations and team work with ethical values.</li>
						<li>To instill the quest for continuous learning and enhancing
						 their communication skills through professional bodies and clubs.</li>
						</ul>
					
			
					
						
						
						
						<h3>PROGRAM EDUCATIONAL OBJECTIVES (PEOs)</h3>	
						<p>PEO1: Excel in Professional career by acquiring knowledge in their domain</p>
						<p>PEO2: Capable of pursuing higher education & indulge in various research activities by continuous learning in association with professional bodies and clubs</p>
						<p>PEO3: Inherit leadership qualities, professional ethics and soft skills to succeed globally </p>
					
						<h3>PROGRAM SPECIFIC OUTCOMES (PSOs)</h3>
						<p>PSO1: Ability to understand basic concepts, analyze subsystems/modules and apply them in various fields like signal processing, networking and communication.</p>
						<p>PSO2: Should be able to associate the learning, understand the published literature and project work effectively.</p>
												  
                  </div>
               </div>
               <!-- vision and mission -->
				               
               
               <!-- Research and development -->
               <div role="tabpanel" class="tab-pane fade" id="research">
                  <h3>Research and Development</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
               						 <h3>R & D Activities</h3>
                        
                        <#list teleResearchList as teleResearch>
                        	<p>${teleResearch.heading!}  <a href="${teleResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        <p>Renowned projects have been awarded with cash prizes by prestigious government and private bodies to encourage students.</p>
                        
                        <#list teleSponsoredProjectsList as teleSponsoredProjects>
                        	<p>${teleSponsoredProjects.heading!}  <a href="${teleSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        
                        <#list telePhdPursuingList as telePhdPursuing>
                        	<p>${telePhdPursuing.heading!}  <a href="${telePhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        
                        <h3>Ph.D Awardees</h3>
                        
                         <#list telePhdAwardeesList as telePhdAwardees>
                        	<p>${telePhdAwardees.heading!}  <a href="${telePhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                    
					
                     	
                     
                     </div>
                  </div>
               </div>
               <!-- Research and development -->
               
               
               <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="row">
                     <h3>Time Table</h3>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list teleClassTimeTableList as teleClassTimeTable>              
                        <p>${teleClassTimeTable.heading!}  <a href="${teleClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list teleTestTimeTableList as teleTestTimeTable>
                        <p>${teleTestTimeTable.heading!}  <a href="${teleTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list teleCalanderList as teleCalander>              
                        <p>${teleCalander.heading!}  <a href="${teleCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--Time Table  -->
               
            <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>Syllabus</h3>
                     
                        <h3>UG (B.E in Electronics & TCE)</h3>
                        
                        <#list teleUgSyllabusList as teleUgSyllabus>
                        <p>${teleUgSyllabus.heading!}  <a href="${teleUgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                        
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
             
               
               
               
               <!--News Letter  -->
               <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Newsletter - Intelegeneia</h3>
                        
                        <p>The intent of having a department newsletter was initiated in the year 2017. Till date, three volumes of newsletters have been released. The main purpose of newsletter is to encourage students and faculties to involve themselves in overall development. Newsletter includes the vision-mission of college and department along with encouraging words from Principal & HOD.
                        </p>
						<p>News letter mainly projects the events conducted and attended by students & Staff. It also throws light on the latest technology and views of Alumni about the department & Telecommunication Engineering</p>
                        
                        
                        <#list teleNewsLetterList as teleNewsLetter>
                        <p>${teleNewsLetter.heading!}  <a href="${teleNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--News Letter  -->
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="row">
                     <h3>Faculty</h3>
                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Faculty</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name}</p>
                                 <p>${faculty.designation}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id!}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               <!--testimonials  -->
               <div role="tabpanel" class="tab-pane fade" id="testimonials">
                  <!--slider start-->
                  <div class="row">
                     <div class="col-sm-8">
                        <h3><strong>Testimonials</strong></h3>
                        <!--<div class="seprator"></div>-->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          
                           <!-- Wrapper for slides -->
                           <div class="carousel-inner">
                              <#list testimonialList as testimonial>
                              <div class="item <#if testimonial?is_first>active</#if>">
                                 <div class="row" style="padding: 20px">
                                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                    <p class="testimonial_para">${testimonial.content!}</p>
                                    <br>
                                    <div class="row">
                                       <div class="col-sm-2">
                                          <img src="${testimonial.imageURL!}" class="img-responsive" style="width: 80px">
                                       </div>
                                       <div class="col-sm-10">
                                          <h4><strong>${testimonial.heading!}</strong></h4>
                                          <!--<p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                             <span>Officeal All Star Cafe</span>-->
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </#list>  
                           </div>
                           <div class="controls testimonial_control pull-right">									            	
                              <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--slider end -->
               </div>
               <!--testimonials  -->
 
 
 			 <!-- Achievers -->
               <div role="tabpanel" class="tab-pane fade" id="achievers">
               	<div class="row">
               		 <h3>Department Achievers</h3>
                     <!--Department Achievers-->
		                  <#list teleDepartmentAchieversList as teleDepartmentAchievers>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${teleDepartmentAchievers.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${teleDepartmentAchievers.heading!}</h3>	                      	 
			                       	 <p>${teleDepartmentAchievers.eventDate!} </p>
			                       	 <p> ${teleDepartmentAchievers.content!}</p>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list> 
		                    
		              <h3>List of FCD Students</h3>
		              <#list teleFcdStudentsList as teleFcdStudentsList>
                        <p>${teleFcdStudentsList.heading!}  <a href="${teleFcdStudentsList.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                      </#list>  
                    
               	</div>
               </div>
               <!-- Achievers -->

 				
 
               
                      <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="row">
               		
               		<h3>Infrastructure / Facilities</h3>
               		 <p>The department has good ambience and is wifi enabled. The laboratories have state-of-art equipments, and computer systems with higher end configuration. Safety measures are incorporated in the department</p>
               	
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
		                  <#list teleLabList as teleLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${teleLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${teleLab.name!}</h3>	                      	 
			                       	 <p>${teleLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${teleLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
               
                  <!--phd pursuing  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdPursuing">
               	<div class="row">
               		 <h3>Ph.D. Pursuing</h3>
		                  <#list telePhdPursuingList as telePhdPursuing>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${telePhdPursuing.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${telePhdPursuing.name!}</h3>	                      	 
			                       	 <p>Description: ${telePhdPursuing.designation!}</p>
			                       	 <a class="btn btn-primary" href="${telePhdPursuing.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd pursuing -->
               
                 <!--phd awardees  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdAwardees">
               	<div class="row">
               		 <h3>Ph.D. Awardees</h3>
		                  <#list telePhdAwardeesList as telePhdAwardees>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${telePhdAwardees.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${telePhdAwardees.name!}</h3>	                      	 
			                       	 <p>Description: ${telePhdAwardees.designation!}</p>
			                       	 <a class="btn btn-primary" href="${telePhdAwardees.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd awardees -->
               
               
              <!-- Gallery  -->
               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
                  <h3>Gallery</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="gallerySLide" class="gallery_area">
                           <#list teleGalleryList as teleGallery>	
                           <a href="${teleGallery.imageURL!}" target="_blank">
                           <img class="gallery_img" src="${teleGallery.imageURL!}" alt="" />					                    			                   
                           <span class="view_btn">${teleGallery.heading!}</span>
                           </a>
                           </#list>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Gallery -->
               
                <!-- Dept Library  -->
               <div role="tabpanel" class="tab-pane fade" id="library_dept">
                  <h3>Departmental Library</h3>
                  <p>The department has its own library which has more than 500 text books and reference book catering to the needs of students as well as Teaching Staffs�. The library preserves previous year's project, internship reports, journals; dissertation and seminar reports and the books contributed by Alumni�s and these are available for students and staffs. Every year 50 to 100 books are being added to the stock. The students and Staffs borrow books from the library periodically. The department maintains separate register for borrowing books.
                  </p>
                  <h2 class="titile">Collection Of books</h2>
                  <table class="table table-striped course_table">
                     <#list teleLibraryBooksList as libraryBooks>
                     <tr>
                        <th style="width:50%">${libraryBooks.heading!}</th>
                        <td>${libraryBooks.content!}</td>
                     </tr>
                     </#list>
                  </table>
               </div>
               <!--  Dept Library   -->
               
               <!-- Department placements -->
               <div role="tabpanel" class="tab-pane fade" id="placement_dept">
                  <h3>Placement Details</h3>
                  
                  <p>Placement Training will be provided to all semester students at the beginning of the semester.
                  </p>
                  <p>Placement Drives will be arranged for final year students from August. Company Specific
                  </p>
                  <p>Trainings will be arranged for final year students to grab more offers in their dream
                  </p>
                  <p>Companies. Technical training sessions will be provided to Pre-final Year students. Placement rate for Electronics and Telecommunication students for last 3 years are 85%.
                  </p>
                  
                  
                  
                  
                  <#list telePlacementList as telePlacement>              
                  <p>${telePlacement.heading!}  <a href="${telePlacement.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>											
                  <!-- Department placements -->
                  
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                  
                    <h3>Events</h3>
                    
                    <#list teleEventsList[0..*3] as teleEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${teleEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${teleEvent.heading!}</h3>	                      	 
	                       	 <p>"${teleEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${teleEvent.eventDate!}  </span></p>
	                       	 <a class="btn btn-primary" href="tele_events.html">View more</a>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
  						
                        
                  </div>
                  <!-- Events  -->
                  
                      <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        
                          <#list teleEresourcesList as teleEresources>
	                    
	                        <p>${teleEresources.heading!}  
	                      	    <a href="//${teleEresources.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
                              
               <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     <h3>Teaching and Learning</h3>
                     
                        <h3>Instructional Materials</h3>
                        
                          <#list teleInstructionalMaterialsList as teleInstructionalMaterials>
	                    
	                        <p>${teleInstructionalMaterials.heading!}  
	                      	    <a href="//${teleInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogical Activities</h3>
                        
                          <#list telePedagogicalActivitiesList as telePedagogicalActivities>
	                    
	                        <p>${telePedagogicalActivities.heading!}  
	                      	    <a href="//${telePedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogy Review Form</h3>
                        
                          <#list telePedagogyReviewFormList as telePedagogyReviewForm>
	                    
	                        <p>${telePedagogyReviewForm.heading!}  
	                      	    <a href="//${telePedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Lab Manuals</h3>
                        
                          <#list teleLabManualList as teleLabManual>
	                    
	                        <p>${teleLabManual.heading!}  
	                      	    <a href="//${teleLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                    
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
               
               
               <!-- MOU's signed -->
               <div role="tabpanel" class="tab-pane fade" id="mou-signed">
                <div class="row">
                  <h3>MOUs Signed</h3>
                  <p>MoU's have been signed with companies to enhance the practical knowledge of students and have hands on experience with the live ongoing projects in companies.</p>
                  <#list teleMouSignedList as teleMouSigned>              
                  <p>${teleMouSigned.heading!}  <a href="${teleMouSigned.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- MOU's signed -->
               
                <!-- Alumni Association -->
               <div role="tabpanel" class="tab-pane fade" id="alumni-association">
                <div class="row">
                  <h3>Alumni Association</h3>
                  <p>CHIRANTHANA with a tagline of EVERLASTING FOREVER is the name given to KSIT ALUMNI ASSOCIATION, with a motto of uniting all the alumni members under the umbrella of KSIT and growing together forever.KSIT alumni association was started in the year 2014.The association provides a platform to their alumni�s to share their knowledge & experience with the budding engineers through technical talks, seminars & workshops.</p>
                  <#list teleAlumniAssociationList as teleAlumniAssociation>              
                  <p>${teleAlumniAssociation.heading!}  <a href="${teleAlumniAssociation.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- Alumni Association -->
               
               
               <!-- Professional Bodies  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_bodies">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Bodies</h3>
                        <p>We in our department have several professional bodies like IEEE, IEI, TEF and IETE. Several events and workshops are conducted under these bodies to enrich the knowledge of students in various fields.</p>
                        <#list teleProfessionalBodiesList as teleProfessionalBodies>
                        <p>${teleProfessionalBodies.heading!}  <a href="${teleProfessionalBodies.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Professional Bodies   -->
           
           
           	<!-- Professional Links  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_links">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Links</h3>
                        
                         <#list teleProfessionalLinksList as teleProfessionalLinks>
	                    
	                        <p>${teleProfessionalLinks.heading!}  
	                      	    <a href="//${teleProfessionalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                												
                     </div>
                  </div>
               </div>
               <!-- Professional Links   -->
           
                 <!-- Higher Education  -->
               <div role="tabpanel" class="tab-pane fade" id="higher_education">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Higher Education</h3>
                        <#list teleHigherEducationList as teleHigherEducation>
                        <p>${teleHigherEducation.heading!}  <a href="${teleHigherEducation.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Higher Education   -->
           
              
              
                 <!-- Students Club  -->
               <div role="tabpanel" class="tab-pane fade" id="student_club">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Telecom Engineers Forum</h3>
                     
                     
                        <h3>Club Activities</h3>
                        <#list teleClubActivitiesList as teleClubActivities>
                        <p>${teleClubActivities.heading!}  <a href="${teleClubActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>External Links (Club)</h3>	
                        
                         <#list teleClubExternalLinksList as teleClubExternalLinks>
	                    
	                        <p>${teleClubExternalLinks.heading!}  
	                      	    <a href="//${teleClubExternalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                        			
                        
                        							
                     </div>
                  </div>
               </div>
               <!-- Students Club   -->
           
               
                 <!-- Internship  -->
               <div role="tabpanel" class="tab-pane fade" id="internship">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Internship</h3>
                        <p>Students are facilitated by providing list of companies /Industries, where they can undergo 4 weeks of Internship/ Professional practice. College takes initiative to organize in house internship by calling resource persons from industry. Student cumulates their learning in a report on their internship/ professional practice and gives a presentation in front of internship coordinator and the guide.</p>
                        <#list teleInternshipList as teleInternship>
                        <p>${teleInternship.heading!}  <a href="${teleInternship.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                  
             			<h3>Projects</h3>
                        <#list teleProjectsList as teleProjects>
                        <p>${teleProjects.heading!}  <a href="${teleProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Mini-Projects</h3>                       
                        <#list teleMiniProjectsList as teleMiniProjects>
                        <p>${teleMiniProjects.heading!}  <a href="${teleMiniProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        											
                     </div>
                  </div>
               </div>
               <!-- Internship   -->
               
               <!-- Social Activities  -->
               <div role="tabpanel" class="tab-pane fade" id="social_activities">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Social Activities</h3>
                        <#list teleSocialActivitiesList as teleSocialActivities>
                        <p>${teleSocialActivities.heading!}  <a href="${teleSocialActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Social Activities  -->   
               
                 <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list teleFdpList as teleFdp>
                        <p>${teleFdp.heading!}  <a href="${teleFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
           
           
             <!-- Industrial Visit -->
               <div role="tabpanel" class="tab-pane fade" id="industrial_visit">
               	<div class="row">
               		 <h3>Industrial Visit </h3>
               		 	<p>The department strives to offer a great source of practical knowledge to students by exposing
							them to real working environment through Industrial visits. Regularly Industrial visits are
							arranged to organizations like Indian Space Research Organization, Karnataka State Load
							Dispatch Centre, All India Radio. Visits are arranged for students to view project exhibitions
							Organized by Indian Institute of Science, Visvesvaraya Industrial and Technological Museum, EMMA-Expo 2014 and so on. Also visits are arranged to places like HAL Heritage Centre and Aerospace Museum.
							               		 	</p>
		                  <#list teleIndustrialVisitList as teleIndustrialVisit>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${teleIndustrialVisit.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${teleIndustrialVisit.name!}</h3>	                      	 
			                       	 <p>${teleIndustrialVisit.designation!}</p>
			                       	 <a class="btn btn-primary" href="${teleIndustrialVisit.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Industrial Visit -->
           
           
            <!-- Project Exhibition -->
               <div role="tabpanel" class="tab-pane fade" id="project_exhibition">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
               		 <p>The department conducts the project exhibition in the even semester of every academic year. Students are encouraged to present their projects and mini projects to the invited guests and evaluators. The best innovative projects are selected and are awarded.</p>
		                  <#list teleProjectExhibitionList as teleProjectExhibition>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${teleProjectExhibition.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${teleProjectExhibition.name!}</h3>	                      	 
			                       	 <p>Description: ${teleProjectExhibition.designation!}</p>
			                       	 <a class="btn btn-primary" href="${teleProjectExhibition.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Project Exhibition -->
           
               
                  
                  
                  <!--  -->
                  <div role="tabpanel" class="tab-pane fade" id="Section4">
                     <h3>Section 4</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
                  </div>
                  <!--  -->
                  
               </div>
            </div>
         </div>
      </div>
</section>
<!--dept tabs -->
	


	
   
   
   
    	
	 
</@page>