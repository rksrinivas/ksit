<@page>
	 
    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <h3>Computer and Engineering Events</h3>
                    </blockquote>
                    
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                
                  <#list cseEventsList as cseEvent>
                  <div class="single_course wow fadeInUp">
                  
                    <div class="singCourse_content">
                    
                    	
						<h2 class="singCourse_title">
						
								"${cseEvent.heading!}"
						
						</h2>
						
						<p>	 <img alt="img" src="${cseEvent.imageURL!}" class="media-object">	</p>
						
						
						<p>
						
							"${cseEvent.content!}"
						
						</p>
						
						<p>
						
							"${cseEvent.eventDate!}"
						
						</p>
						
                    </div>
                    
                  </div>
                  
                  </#list>
                  
                </div>
                <!-- End single course -->
				
				
				
				
				
              
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Research Review Meet</h2>
						<p><img alt="img" src="img\cse\cse_events\rrm6june.jpg" class="media-object"></p>
						<p>Department of CSE conducted Research Review Meet for 4 Scholar's on 6th and 9th June 2017. The Internal and External Domain experts and respective guides Reviewed the research progress.</p>
						<p>2017-06-06</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Farewell</h2>
					
						<p><img alt="img" src="img\cse\cse_events\sriraksha.jpg" class="media-object"></p>
						<p>Farewell has been conducted for the 2017 outgoing students of KSIT . CSE student Sri Raksha N has been awarded as the best student of the Year 2017</p>
						
						<p>2017-05-26</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Final Year CSE department students won 1st place in project exhibition 'Tech Menia-17'</h2>
					
						<p><img alt="img" src="img\cse\cse_events\agri-robo.jpg" class="media-object"></p>
						
						<p>Students from final semester Chandana V, Diksha, Kalpana G N, Mehtaz N Sidnale CSE department won 1st place in project exhibition 'Tech Menia-17' at City Engineering College,Bengaluru on 24th May 2017. The project entitled 'Agribot' carried out under the guidence Assoc Prof K Venkata Rao Dept of CSE,KSIT. Principal Of KSIT felicitated the students.</p>
						<p>2017-05-24</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Students of CSE WON 2nd place in 'Graphics Day'</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2017-05-16-12-06-17-782.jpg" class="media-object"></p>
						
						<p>Students Prabhu and Shravani from CSE department WON 2nd place in Graphics Day conducted by 'Jyothi Institute of Technology' on 12th May 2017. Students felicitated by principal of KSIT.</p>
						<p><span class="feed_date">2017-05-12</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Career Talk on 'How Journalism is an Option after Engineering ?'</h2>
					
						<p><img alt="img" src="img\cse\cse_events\career talk.JPG" class="media-object"></p>
						<p>Career Talk was Conducted on 21st April 2017 in association with KSIT ALUMNI ASSOCIATION 'CHIRANTHANA' in Department of CSE. Talk on 'How Journalism is an Option after Engineering ?' given by Alumni Ms. Swetha R, Studied Broadcast Journalism, Indian Institute of Journalism and New Midea, Bangalore. She was graduated in the year 2015 from Department of CSE</p>
						<p><span class="feed_date">2017-04-21</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition 2017</h2>
					
						<p><img alt="img" src="img\cse\cse_events\p14.jpg" class="media-object"></p>
						   <p>33 Batches given demo on their Project, Dr. Suma V, Dean R&D, DSCE and Dr. Ashok Kumar, Professor, RVCE, Bengaluru</p>
						<p>                       <span class="feed_date">2017-05-12</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Mini Project Exhibition 2017</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\p13.jpg" class="media-object"></p>
						
						<p>15 Projects are evaluated by Dr K N Rama mohan Babu, HOD ISE, DSCE, Bengaluru. First Prize 2D and 3D Bus stop by Parth Purohit Second Prize Save Elecricity by Achyuth S R Consolation Prizes 1. Scooba Diving by Mamatha P N 2. Dispersion of Light by Manasa M</p>
						<p><span class="feed_date">2017-05-12</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> National Conference NaCONIM 2017</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\naconim.JPG" class="media-object"></p>
						
						   <p>Totally 41 papers are presented in 4 Parallel sessions. Dr. Balaji, CIIR, Jyothi Institute of Technology, Dr. Girijamma, Prosfessor, RNSIT, Dr.S.V. Sheela, Professor, BMSCE, Dr. K N RamaMohan Babu, HOD,ISE, DSCE were session Chairs.</p>
						<p><span class="feed_date">2017-05-10</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Student of CSE WON 'SMART INDIA HACKATHON 2017' conducted by Ministry of HRD Govt of India in Jaipur</h2>
					
						<p><img alt="img" src="img\cse\cse_events\smart.jpg" class="media-object"></p>
						
						<p>CSE students secured 1st Prize of 1 Lakh in 36 hours National Level Non stop Coding Competition termed to be Worlds Biggest Hackathon on 1st and 2nd April 2017.The team provided solution to Department of Posts and Ministry of Communication. They are Feliciatated in Ananya -2017 at KSIT, Staff members Mr. Pradeep Kumar G H and Mr. Kumar K mentored students along with Alumni for the event.</p>
						<p><span class="feed_date">2017-04-07</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A team from CSE department won 1st prize in 24 hours HACKATHON conducted in Amrita School of Enginee</h2>
					
						<p><img alt="img" src="img\cse\cse_events\01.jpeg" class="media-object"></p>
						
						<p>A team from CSE department won in 24 hours HACKATHON conducted in Amrita School of Engineering on 14th April 2017 under the guidance of Assistant Professor Pradeep Kumar G H among 128 teams. We heartly congratulate Kushal S T, Prabhu B, Abhishek M, Narasimha Prasanna H N.</p>
						<p><span class="feed_date">2017-04-14</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture On Job Placements By IIHT Training Institute </h2>
					
						<p><img alt="img" src="img\cse\cse_events\aaa.JPG" class="media-object"></p>
						
						   <p>Ms. Roopa,Managing Partner & Director, IIHT, Kanakapura Road, Bengaluru has given a guest Lecture on upcoming technologies and its job related opportunities for 6th Semester CSE Students.</p>
						<p>                       <span class="feed_date"> 2017-04-25</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical talk on 'Connecting Talents to Opportunities'</h2>
					
						<p><img alt="img" src="img\cse\cse_events\20170322_120531.jpg" class="media-object"></p>
						
						<p>Technical talk on 'Connecting Talents to Opportunities' by IIHT on 22nd March 2017 for 8th semester students. Speaker: Ms Roopa deliver the talk.</p>
						<p>                          <span class="feed_date"> 2017-03-22</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture on 'Big Data Analytics and Cloud Computing'</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Guest Lecture 1.jpg" class="media-object"></p>
						
						<p>Department of CSE, KSIT has organised Guest Lecture on 'Big Data Analytics and Cloud Computing'. The resource person Mr.Robin Mazumdar, Product Manager, Manipal Prolearn ,Bengaluru addressed the students.</p>
						<p>                          <span class="feed_date">2017-02-17</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial Visit to IMTEX</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Indus.jpg" class="media-object"></p>
						<p>IMTEX 2017 and the concurrent Tooltech 2017 exhibition is organized by Indian Machine Tool Manufacturers' Association (IMTMA) from 26th Jan to 1st Feb 2017 at the Bangalore International Exhibition Centre (BIEC), Bengaluru, India. All staff members of Computer Science & Engineering Department participated in the Industrial Visit.</p>
						<p>                          <span class="feed_date">2017-01-27</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">RaspBerry Pi and its Applications in IOT </h2>
					
						<p><img alt="img" src="img\cse\cse_events\FDP_RASBERY1.JPG" class="media-object"></p>
						<p>A Four day Faculty Development Program on 'RaspBerry Pi and its Applications in IOT' was successfully conducted in association with IEEE, ISTE, and CSI-India, from 18th to 21st Jan 2017, under the banner of IEEE, IETE and CSI at KSIT. The FDP programme resource person Mr. Ganeshan, CEO,Vishwajyothi Technologies took hands-on sessions and received a good response with 42 participants.</p>
						<p><span class="feed_date">2017-01-22</span>	</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">30th CSI Karnataka Student Convention</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\CSIC_Dec_2016_47.jpg" class="media-object"></p>
					   <p>30th CSI Karnataka Student Convention photo published in CSI Communication December 2016 Issue</p>

						<p>                       <span class="feed_date">2016-12-14</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial Visit to ISRO </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\IMG-20161121-WA0002.jpg" class="media-object"></p>
					   <p>Department of CSE, KSIT organized an official industrial visit to ISRO Bengaluru for 5th sem students.</p>

						<p>                       <span class="feed_date">2016-11-15</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">30th CSI Karnataka Students Convention</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\csi.jpg" class="media-object"></p>
					   <p>Department of CSE, K.S.Institute of Technology hosted 30th CSI Karnataka students convention in collaboration with Computer Society of India on 10th, 11th and 12th of November 2016 in the college premises. Theme of convention is 'ROLE OF ICT TO MAKE DIGITAL INDIA'</p>

						<p><span class="feed_date">2016-11-10</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A Technical Talk On 'ARTIFICIAL INTELLIGENCES'</h2>
					
						<p><img alt="img" src="img\cse\cse_events\ARTFICIAL_INTELIGENCE.jpg" class="media-object"></p>
					   <p>A Technical talk on 'Artificial Intelligence' held for 3rd, 5th and 7th Semester CSE girl students under the banner of ISTE co-ordinated by our staff members Mrs.Vaneeta M and Mr.Aditya Pai H Assistant Professors,CSE,KSIT. The talk is organized by Anita Borg Institute (ABI) and the resource person is Ms. Hima M Patel working as research scientist in Shell Technology Centre, Bangalore.</p>

						<p><span class="feed_date">2016-09-29</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">YOUNG INDIA CHALLENGE 2016 </h2>
					
						<p><img alt="img" src="img\cse\cse_events\IMG_20160929_103659303.jpg" class="media-object"></p>
					   <p>Career Launcher one of ASIA's Leading EDU-corporates with a focus on diverse segments of education conducted level-1 test for III & V semester students to test their analytical skills, GK , creativity and global awareness.The event was held on 26.09.2016 co-ordinated by Mr.Pradeep kumar G H Assistant Professor CSE, KSIT.</p>

						<p> <span class="feed_date">2016-09-26</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">EQUINOX 2015</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\fi.JPG" class="media-object"></p>
						<p>The KSIT FIREFOX CLUB conducted its first club activity named EQUINOX on 23rd September ,2016 co-ordinated by Mr.Kumar K and Mr.Pradeep Kumar G H Assistant Professors CSE KSIT. The EQUINOX event was a 4 tiered event with the four phases being BRAIN GRILL,PIC-CHARADS,ALL THAT CIPHERS and the final round I AM THE BEST.</p>
						<p><span class="feed_date">2016-09-23</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">KSIT FIREFOX CLUB LAUNCH</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\z.JPG" class="media-object"></p>
						<p>KSIT FIREFOX CLUB was officially launched on 15th September 2016 at the conference hall of KSIT on the account of Engineers day. The KSIT FIREFOX CLUB known by the name EQUINOX, led by Shri Raksha N,Firefox Student Ambassador & co-ordinated by our staff members Mr.Kumar K and Mr.Pradeep Kumar G H , Assistant Professors CSE KSIT.</p>
						<p><span class="feed_date">2016-09-15</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Research Review Meeting </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\rrm.JPG" class="media-object"></p>
					   <p>Research Review Meeting conducted on 27-8-2016 at Research and Development Centre, KSIT.3 students presented their Research work in front of Panel of Evaluators/RRM Members, which included an Industry Expert Dr. K Vasu, System Architect, Huawei Technologies, Bengaluru, An Academic Expert - Dr. Rajashekhar, Assoc.Professor, ISE Department, Dayanand Sagar College of Engineering, Bengaluru, Dr. N C Naveen, Research Guide, CSE R & D Centre, KSIT and Dr. Rekha B Venkatapur, Head of Department , CSE, KSIT</p>

						<p>                       <span class="feed_date">2016-08-27</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Applied CS with Android Workshop </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\IMG_73132.jpg" class="media-object"></p>
					   <p>Applied CS with Android Workshop was conducted on 7th April 2016. Google Applied CS with Android program is designed for university Computer Science students and enhances various concepts from their current curricular work. The program revisits concepts from Data Structures and Algorithms using Android as a platform for development.</p>

						<p><span class="feed_date">  2016-08-07</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture on 'Programming Skills' by Prof. A.M Padma Reddy</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\IMG_73131.jpg" class="media-object"></p>
					   <p>The Department of Computer Science and Engineering Organized a Guest Lecture on 'Programming Skills' for 3rd sem students by Prof. A.M Padma Reddy,SVIT,Bangalore. It was held on 8th August, 2016, under IEI banner.</p>

						<p><span class="feed_date">2016-08-08</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">FAREWELL</h2>
					
						<p><img alt="img" src="img\cse\cse_events\DSC_8530.JPG" class="media-object"></p>
					   <p>Farewell has been conducted for the outgoing students of KSIT . CSE student Banashree gave gracious leaving speech.</p>

						<p><span class="feed_date">2016-05-21</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PROJECT EXHIBITION</h2>
					
						<p><img alt="img" src="img\cse\cse_events\IMG_7172.jpg" class="media-object"></p>
						<p>14th May 2016 Final Year students of KSIT,CSE, Participated (31 Batches, 8 Paper presentations) Prof K. Anantha Padmanabha, Professor and HOD, Dept of CSE, AIT, Bengaluru and Dr. Kavitha, Prof, Dept of CSE, GAT, Bengaluru reviewed the presentations and evaluated Project works. </p>
						<p><span class="feed_date">2016-05-14</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">MINI PROJECT EXHIBITION</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\IMG_7189.jpg" class="media-object"></p>
					   <p>14th May 2016 Pre-Final Year students of KSIT, CSE participated in Mini Project Exhibition on Computer Graphics & Visualization. 12 projects were exhibited. Prof K. Anantha Padmanabha, Professor and HOD, Dept of CSE, AIT, Bengaluru and Dr. Kavitha, Prof, Dept of CSE, GAT, Bengaluru evaluated Project works.</p>

						<p><span class="feed_date">2016-05-14</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk on IoT and Industry trends by Alumni Mr.Abhiroop Matilal</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\IMG_3533.JPG" class="media-object"></p>
					   <p>Technical Talk on IOt and Industry trends presented by Alumni Mr.Abhiroop Matilal, Manager, IoT Business Strategy(Automative), Tech Mahindra, Bangalore, Thanks to Alumni of 2004 Batch Dept of CSE,KSIT who shared his 12 years of Experience, Innovations,Contributions with our students.</p>

						<p><span class="feed_date">2016-04-30</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">AWARENESS PROGRAM ON 'John Deere presents Techno- Champ 2016'</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\123.jpg" class="media-object"></p>
						<p>The Department of Computer Science and Engineering in collaboration with Electronics and Electrical Engineering Organized Awareness program on 'John Deere presents Techno- Champ 2016' by Girish.K from BMSCE College, under KSIT-ISTE Student Chapter</p>
						<p><span class="feed_date">2016-04-21</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A Guest Lecture on - Joy of Learning Computer Networks</h2>
					
						<p><img alt="img" src="img\cse\cse_events\IMG_7129.jpg" class="media-object"></p>
						<p>A Guest Lecture on 'Joy of Learning Computer Networks' was conducted on 16th April 2016, for 6th Semester, CSE students by Prof. Chidambara, PESIT, Bangaluru under CSI Student chapter.</p>
						<p>                       <span class="feed_date"> 2016-04-16</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IISC INDUSTRIAL VISIT</h2>
					
						<p><img alt="img" src="img\cse\cse_events\IMG-20160330-WA0002.jpg" class="media-object"></p>
						   <p>Department of CSE, KSIT organized an official induatrial visit to IISC Bengaluru during Open Day - 2016 .</p>
						<p><span class="feed_date">2016-03-05</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">APP DESIGN PROTOYPING</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\IMG_7108.jpg" class="media-object"></p>
						   <p>The Department of Computer Science and Engineering, Organized WORKSHOP 'APP DESIGN PROTOYPING' on 19th March 2016, under CSI Student Chapter in association with Creative Infotech, Bangalore.</p>
						<p><span class="feed_date">2016-03-19</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> Opportunities & Trends in Animation, Visual Effects and Gaming Industry </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\7.jpg" class="media-object"></p>
						   <p>The Department of Computer Science and Engineering Organized Technical Talk on 'Opportunities & Trends in Animation, Visual Effects and Gaming Industry' by ICAT Design and Media was conducted on 9th March 2016, under KSIT-ISTE Student Chapter.</p>
						<p><span class="feed_date">2016-03-09</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">TECHNICAL TALK ON -'MOBILE APPLICATION DEVELOPMENT'</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\MOBILE APPLICATION DEVELOPMENT.png" class="media-object"></p>
						   <p>A Technical Talk on Mobile Application Development by Alumni UDAY, who graduated in the year 2015, Department of CSE. Technical Talk was Conducted on 09th Feb 2016 in association with KSIT ALUMNI ASSOCIATION in Department of CSE.</p>
						<p><span class="feed_date">2016-02-09</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">INTERNATIONAL CAREERS</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\P_20160202_114012 (1).jpg" class="media-object"></p>
						   <p>The Department of Computer Science and Engineering Organized a Guest Lecture on International Careers on 2nd February 2016, under CSI Student Chapter in association with SIEC, Bangalore.</p>
						<p><span class="feed_date">2016-02-02</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Robotics</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Workshop_Robotics.JPG" class="media-object"></p>
						   <p>PROFESSIONALS: Mr.Vijay.V.Rao, Franchise Manager, AIEE, Mr.Kishore.G.K, Associate Director, Mr.N.Awasthi, , Vice-President of M/S APSIS Solutions, Bangalore (www.apsis-solutions.com). Mr.Amarjeet Kumar BE (ECE), CBIT, Kolar. The workshop was conducted in an interdisciplinary manner, III and V Sem Students of CSE participated in it.</p>
						<p><span class="feed_date"> 2015-10-10</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Internet of Things- Ideation Boot Camp</h2>
					
						<p><img alt="img" src="img\cse\cse_events\IOT-tobe-uploaded.JPG" class="media-object"></p>
						   <p>Dr. Jamadagni, IISC and his team conducted the workshop in collaboration with Intel and PES University. Two days workshop conducted for CSE, ECE and TCE 5th semester students.</p>
						<p><span class="feed_date">2015-10-14</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Digital Marketing</h2>
					
						<p><img alt="img" src="img\cse\cse_events\SEminar_digi marketing.JPG" class="media-object"></p>
						<p>Mr. Ganesh, Internet Marketer, Blogger, gave the seminar. Mr Varun and Mr.Abhishek alumni 2014 Batch , INDGROUPs were present in the seminar. This seminar is arranged for 3rd semester students under the Alumni association, KSIT- Events.</p>
						<p>                       <span class="feed_date">2015-10-10</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Android and Web Application Development</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Workshop.JPG" class="media-object"></p>
						<p>Anupam, CEO, Dr.Code and His team gave hands on sessions on Application development for prefinal and final year students</p>
						<p>span class="feed_date">2015-10-03</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Software Testing Fundamentals</</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Technical talk.JPG" class="media-object"></p>
						   <p>First event from Entrepreneurship Development Centre in association with IEE.Mrs. Manjula Guruswamy, Test Lead HP & EMIDS has delivered the talk on Software Testing Fundamentals. Mrs.Manjula has showcased the industry oriented test cases and enriched the students knowledge with her talk.</p>
						<p>                       <span class="feed_date">2015-08-31</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Inspiring Entrepreneurship and Encouraging Volunteerism  </h2>
					
						<p><img alt="img" src="img\cse\cse_events\Seminar.JPG" class="media-object"></p>
						<p>(Inspiration Unlimited) IU Speakers Group. Mr.Subhash K.C., Mr. Prashant Yag,, Mr. Kumal Taswala, Ms. Kavyashree M.R. give session wise seminar, it covered - Importance of having a Right Direction, Motivation, Mentors and having a Platform to explore. Students of 3rd, 5th and 7th semester also learnt about the revolutionary platform called i3. This Program is Organized under CSI Student Chapter</p>
						<p>                       <span class="feed_date">2015-08-27</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">FACULTY DEVELOPMENT PROGRAM ON 'APPLE IN HIGHER EDUCATION' And 'Enhancing Digital Skills on Adobe's</h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2015-06-25.JPG" class="media-object"></p>
						   <p>The Department of Computer Science and Engineering Organized FACULTY DEVELOPMENT PROGRAM on 'APPLE IN HIGHER EDUCATION' and 'Enhancing Digital Skills on Adobes Best of Breed Creative Tools' on 25TH June 2015, under CSI Chapter in association with Creative Infotech, Bangalore, at CSE Seminar Hall, New Block .</p>
						<p><span class="feed_date">2015-06-25</span></p>
						
                    </div>  
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\CG-project-small.jpg" class="media-object"></p>
						   <p>Mr. Sameerana, HOD CSE, APSCE, Dr. Surekha B, Prof ECE, KSIT, Mr. Harshavardhan J R & Mrs. Sangeetha evaluated the Project works of 8th CSE 28 batches & selected best papers in Paper presentation competition 8 Authors. They also selected 2 Computer Graphics mini Projects of 6th semester as best among 10 batches.</p>
						<p><span class="feed_date">2015-05-09</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2015-04-24.JPG" class="media-object"></p>
						<p>Ethical hacking By Mr Srivari Kulkarni of Eikon Technologies, Bengaluru</p>
						<p><span class="feed_date"> 2015-04-24</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Eduladder-Samll.jpg" class="media-object"></p>
						<p>Internship in Eduladder Gaming Application development for Web. By Eduladder, Bengaluru</p>
						<p><span class="feed_date">2015-04-23</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\SAN_DLC-SMALL-new.jpg" class="media-object"></p>
						   <p>Importance of storage solutions. Storage area networks and its relevance. By Mr Manjunath Data Life Cycle</p>
						<p>                       <span class="feed_date">2015-04-15</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2015-03-04.JPG" class="media-object"></p>
						<p>On 4-3-2015 under CSI Student chapter a seminar on Career Guidance on the International Education Scene and Advantages of Research is conducted by Mr.Sreejith Narayan, Career Launcher</p>
						<p><span class="feed_date">2015-03-04</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\ICAT-Animation-samll.jpg" class="media-object"></p>
						   <p>18-2-2015 Career Opportunities in Animation, Visual Effects and Gaming Mr.David,Regional Manager for Image Group ICAT</p>
						<p>                       <span class="feed_date"> 2015-02-18</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2015-01-24.JPG" class="media-object"></p>
						<p>Google AdWords @ KSIT Google Group</p>
						<p>                          <span class="feed_date">2015-01-24</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\CSI-Cloud-small.JPG" class="media-object"></p>
						   <p>22-1-2015 Cloud Foundation KSIT - CSI Student Chapter Inauguration by Chief Guest Mr.Chander P Mannar, Chairman, CSI& Sessions handled by Mr.Anil Bidari,CEO, Cloud enabled</p>
						<p><span class="feed_date">2015-01-22</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\GLUG-2.JPG" class="media-object"></p>
						<p>21-9-2014 Advanced Java KSIT - KSIT GLUG Google Group, Karthik Kannapur, GDG organizer, was the guest of the day. Tejas Koundinya handled the session.</p>
						<p><span class="feed_date">2014-09-21</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop  </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Python-small.JPG" class="media-object"></p>
						<p>'Python' With Industry & FSMK collaboration,Trainer Senthil Kumar M working in Qualcomm as a wireless research engineer and Sharath from FSMK conducted the workshop</p>
						<p><span class="feed_date">2014-09-20</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial Visit </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Industrial-visit-Taiwan-small.JPG" class="media-object"></p>
						   <p>Industrial Visit International - 12-9-2014 Exhibition EMMA EXPO INDIA Taiwan Excellence Group</p>
						<p>                       <span class="feed_date">2014-09-12</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Vocational-training-small.jpg" class="media-object"></p>
						   <p>23-8-2014 Object Oriented Design Mr. Badrinath & Mr. Lakshmi Narayan Open Gyan,Chennai</p>
						<p>                       <span class="feed_date">2014-08-23</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Vocational Courses   </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\OOMD-small.jpg" class="media-object"></p>
						   <p>14-7-2014 to 18-7-2014 Latest Technologies-Intelligent Web Applications, Cloud Computing, Android Programming by KSIT-CSE Staff</p>
						<p>                       <span class="feed_date">2014-07-14</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture </h2>
					
						<p><img alt="img" src="img\cse\cse_events\SAP-G-L.JPG" class="media-object"></p>
						<p>30-7-2014 About SAP Mr.Nagaraj Shetty, Chairman , Connaissamce, Mr.Manoj Kumar, CEO Connaissance,</p>
						<p>                       <span class="feed_date">2014-07-30</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition and Paper presentation Competition</h2>
					
						<p><img alt="img" src="img\cse\cse_events\2014-05-10.JPG" class="media-object"></p>
						<p>10-5-2014 Final Year students of KSIT,CSE, Participated (34 Batches, 7 Paper presentations) Dr. C.Nandini , HOD CSE, DSATM and Dr.V. Suma, Dean, Research Industry incubation Center , DSCE reviewed the presentations and evaluated Project works.</p>
						<p><span class="feed_date">2014-05-10</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference -NaCoNIM </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2014-04-05.JPG" class="media-object"></p>
						<p>National Conference -NaCoNIM - Networking, Image Processing and Multimedia -2014 , 5-4-2014 Key Note Speaker: Dr. G .T .Raju , Prof & Head, CSE, RNSIT, Bangalore, 6 Session Chairs</p>
						<p><span class="feed_date"> 2014-04-05</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Pointers-C.JPG" class="media-object"></p>
						   <p>22-3-2014 'Pointers in C' Mr.Badrinath & Mr.LakshmiNarayan ,Open Gyan, Chennai</p>
						<p><span class="feed_date">2014-03-22</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop </h2>
					
						<p><img alt="img" src="img\cse\cse_events\HTML.JPG" class="media-object"></p>
						   <p>15-3-2014 'HTML 5 and Web Development' Mr. Anuj Duggal, Organizer, Google Developer Group,Bangalore</p>
						<p><span class="feed_date">2014-03-15</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar & KSIT-GLUG  </h2>
					
						<p><img alt="img" src="img\cse\cse_events\KSIT-GLUG-1.JPG" class="media-object"></p>
						<p>8-3-2014 'Programmingin LINUX' Mr. Shijil, Mr.Sharath FSMK</p>
						<p><span class="feed_date">2014-03-08</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\Optimized-cloud-1.jpg.png" class="media-object"></p>
						<p></p><p>7-3-2014 'Cloud Computing' Mr.Anil Bidari, Founder & CEO of Cloud enabled ,Bangalore</p>
						<p><span class="feed_date">2014-03-07</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2013-10-19.JPG" class="media-object"></p>
						   <p>Wednesday, 19th November, 2013, a seminar on Data Storage Technology Learning Objectives was arranged to enable students and faculty members to understand storage definitions for their research activity on IT infrastructure (such as Cloud computing, Virtualization and Datacenter management). The session was handled by Mr.Manjunath H R from Data Life Cycle Company, Bangalore.</p>
						<p>                       <span class="feed_date"> 2013-10-19</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Ayudha Pooja </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2013-10-12.JPG" class="media-object"></p>
						   <p>On 12th October 2013 Ayudha Pooja is conducted in the CSE department. In all the labs students decorated the floor with Color rangoli design and conducted pooja.</p>
						<p>                       <span class="feed_date">2013-10-12</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Huawei Club Inauguration </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2013-10-28.JPG" class="media-object"></p>
						   <p>Monday, 28th October, 2013, Huawei delegates were at KSIT to inaugurate the Huawei Club Event. There were a lot of fun games and activities. It was really a very good response from all the students and the staff at KSIT.</p>
						<p><span class="feed_date">2013-10-28</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2013-09-28.JPG" class="media-object"></p>
						<p>The Department of Computer Science and Engineering conducted one day workshop on Networking and Network Programming on 28th September 2013 by OpenGyan Team, at Algorithms & Networks Lab, KSIT, Bangalore - 62. Mr.Badrinath & Mr.Laxminarayan conducted this workshop for 51 V Sem CSE students.</p>
						<p><span class="feed_date"> 2013-09-28</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture </h2>
					
						<p><img alt="img" src="img\cse\cse_events\2013-09-14.JPG" class="media-object"></p>
						   <p>Saturday, 14th September, 2013, Free Software Movement Karnataka (FSMK) delegates gave a guest lecture to students and faculties of KSIT about Linux and Free softwares that are available to develop projects.</p>
						<p>                       <span class="feed_date"> 2013-09-14</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2013-09-06.JPG" class="media-object"></p>
						   <p>Friday 06th September, 2013, the Department of Computer Science and Engineering of KSIT had organized a Guest Lecture on SKILLS A GATEWAY TO SUCCESS for the benefit of pre final year students of KSIT. Prof S.N.Murthy from RNSIT, Bangalore was the resource person for this program.</p>
						<p><span class="feed_date">2013-09-06</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition and Paper presentation Competition </h2>
					
						<p>                          <img alt="img" src="img\cse\cse_events\2013-05-22.JPG" class="media-object"></p>
						   <p>A Project exhibition and Paper presentation competition was conducted in CSE department on 22nd May 2013.36 Batches participated in Exhibition and 15 batches presented papers based on their work.Dr. Basavaraj, Prof & HOD, Computer Science and Engineering Department, BTLIT and Dr. Jagannathan, Prof. CSE Department, APSCE evaluated these project works and paper presentation competition.</p>
						<p><span class="feed_date">2013-05-22</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day State Level Workshop on ETHICAL HACKING & CYBER FORENSICS </h2>
					
						<p><img alt="img" src="img\cse\cse_events\125workshop-eh.jpg" class="media-object"></p>
						      <p>Dept. of Computer Science & Engg., K S Institute of Technology conducted two days State Level workshop on  ETHICAL HACKING & CYBER FORENSICS 0n 22-02-13 & 23-02-13.</p>
						<p>                       <span class="feed_date">2013-02-22</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference and paper presentation on Networking, Image Processing and Multimedia NaCoNIM 2   </h2>
					
						<p> <img alt="img" src="img\cse\cse_events\DSC_0473.JPG" class="media-object"></p>
						   <p>Department of Computer Science and Engineering has Organized a one day National Conference  and paper presentation on Networking, Image Processing and Multimedia NaCoNIM 2012 on 8th September 2012 for academicians, students, Researchers and personnel from the industries with the continued support from the Kammavari Sangham Management. The objective of the conference was to understand, share the intricacies, progress and the renewed challenges ahead in the underlying field.</p>
						<p><span class="feed_date">2012-09-08</span></p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
							
				
				
				 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
	
</@page>