<@page>
<!--=========== slider  ================-->
<section id="home_slider">
	<div class="container" style="width:100%;">
		<!-- Start Our courses content -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="home_slider_content">
					<div class="row">
						<div id="myCarousel" class="carousel slide">
							<div class="carousel-inner">
								<article class="item active">
									<img class="animated slideInLeft" src="${img_path!}/hostel/slider/s1.jpg" alt="">
								</article>
								<article class="item">
									<img class="animated bounceIn" src="${img_path!}/hostel/slider/s2.jpg" alt="">										
								</article>
								<article class="item">
									<img class="animated rollIn" src="${img_path!}/hostel/slider/s3.jpg" alt="">
								</article>
								<article class="item">
									<img class="animated rollIn" src="${img_path!}/hostel/slider/s4.jpg" alt="">                        
								</article>
								<article class="item">
									<img class="animated bounceIn" src="${img_path!}/hostel/slider/s5.jpg" alt="">										
								</article>
								<article class="item">
									<img class="animated rollIn" src="${img_path!}/hostel/slider/s6.jpg" alt="">
								</article>
								<article class="item">
									<img class="animated rollIn" src="${img_path!}/hostel/slider/s7.jpg" alt="">                        
								</article>
								<article class="item">
									<img class="animated rollIn" src="${img_path!}/hostel/slider/s81.jpg" alt="">                        
								</article>
							</div>
							<!-- Indicators -->
							<ol class="carousel-indicators">
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								<li data-target="#myCarousel" data-slide-to="1"></li>
								<li data-target="#myCarousel" data-slide-to="2"></li>
								<li data-target="#myCarousel" data-slide-to="3"></li>
								<li data-target="#myCarousel" data-slide-to="4"></li>
								<li data-target="#myCarousel" data-slide-to="5"></li>
								<li data-target="#myCarousel" data-slide-to="6"></li>
								<li data-target="#myCarousel" data-slide-to="7"></li>
							</ol>
							<!-- Indicators -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Our courses content -->
	</div>
</section>
<!--=========== slider end ================--> 
<!-- welcome -->
<div class="dept-title">
	<h1>Welcome To KSGI Hostels</h1>
	<p class="welcome-text"></p>
</div>
<!-- welcome -->		 
<!--dept tabs -->
<section id="vertical-tabs">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="vertical-tab padding" role="tabpanel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
						<li role="presentation"><a href="#highlights" aria-controls="highlights" role="tab" data-toggle="tab">Highlights</a></li>
						<li role="presentation"><a href="#rooms" aria-controls="objectives" role="tab" data-toggle="tab">Rooms</a></li>
						<li role="presentation"><a href="#study" aria-controls="study" role="tab" data-toggle="tab">Study Room</a></li>
						<li role="presentation"><a href="#indoor" aria-controls="indoor" role="tab" data-toggle="tab">Indoor Games</a></li>
						<li role="presentation"><a href="#gym" aria-controls="gym" role="tab" data-toggle="tab">Gym</a></li>
						<li role="presentation"><a href="#tv" aria-controls="tv" role="tab" data-toggle="tab">Tv Room</a></li>
						<li role="presentation"><a href="#kitchen" aria-controls="kitchen" role="tab" data-toggle="tab">Kitchen & Dining</a></li>
						<li role="presentation"><a href="#visitors" aria-controls="visitors" role="tab" data-toggle="tab">Visitors Lobby</a></li>
						<li role="presentation"><a href="#amenities" aria-controls="amenities" role="tab" data-toggle="tab">Other Amenities</a></li>
						<li role="presentation"><a href="#fees" aria-controls="amenities" role="tab" data-toggle="tab">Fees</a></li>
						<li role="presentation"><a href="#enrolment" aria-controls="amenities" role="tab" data-toggle="tab">Hostel Enrolment</a></li>
					</ul>
					<!-- Nav tabs -->
					<!-- Tab panes -->
					<div class="tab-content tabs">
						<!-- profile -->
						<div role="tabpanel" class="tab-pane fade in active" id="profile">
							<div class="row">
								<h3>Profile</h3>
								<p>Separate hostels are provided for boys and girls situated within walking distance from the college for convenience.
									The hostels are prepared to be an extended home. the hostel facilitates completely support students to carry out serious study
									and to grow  physically, intellectually and psychology. The college tradition and discipline is also extended to the hostels.
								</p>
								<p>Students can enjoy the homely atmosphere along with hygienically maintained top class facilities.Round the clock security
									ensures that our students are safe at KSSEM and the hostels are absolutely free from ragging and the boarders are guaranteed 
									with comfortable & peaceful stay. 
								</p>
								<p>For boys from KSIT there is college bus facility to reach the boys hostel in KSSEM campus</p>
								<p>As the girls Hostel is right opposite to Ksit, there will be college bus facility for the girls from KSSEM as well</p>
								<p>Our hostel are more than home away from homes which provides the feeling of one's family.</p>
								<p>Along with huge walk around campus, monitored by securities, CCTV camera for internal monitoring ,
									well furnished spacious & clean rooms, hygienic kitchen & dining area, proactive indoor
									GameSpot, elevators, well equipped gym, interactive TV room, all of which with all time power and power 
									generator for backup power and water facility with solar water heater pumps, RO treated water from
									the top tasks for all buildings , double RO treated drinking water, friendly visitors lobby,
									playground, most time available housekeeping support staff and special separate visitors 
									room for the parents or guardians who visit the students.
								</p>
								<div class="aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<hr/>
										<img class="hod-img img-responsive" src="${img_path!}/hostel/boys-hostel-warden.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">A. Balakrishna Naidu </h3>
											<h4 class="qual-text">Boys Hostel Supervisor</h4>
											<h3 class="qual-text"></h3>
										</div>
										<hr/>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<hr/>
										<img class="hod-img img-responsive" src="${img_path!}/hostel/girls-hostel-superviser.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">L. Krishna </h3>
											<h4 class="qual-text">Girls Hostel Supervisor</h4>
											<h3 class="qual-text"></h3>
										</div>
										<hr/>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<p>Our warm greetings to all the KSIT hostel students, their parents living in India, and other parts of the world.</p>
										<p>It feels delighted to welcome you all to be a part of KSIT family that strives to provide students a homely and healthy atmosphere with complete Safety and security in the hostel. </p>
										<p>At the hostels of KSIT, we not only take efforts to provide its students a neat and clean environment and a comfortable place to live in, but encourage them to live in discipline. We understand discipline is the key to success and career building, hence we promote self-discipline among students.</p>
										<p>We know hostel life is a union of diverse cultures and different upbringings, which blends into a harmony for a rich and memorable experience. We provide atmosphere where they can learn, laugh and live to the fullest. The  caretakers ensure that the students get a clean, green and relaxed atmosphere. We promote green sustainability inside the hostel premises.   </p>
									</div>
								</div>
							</div>
						</div>
						<!-- profile -->
						<!-- highlights -->
						<div role="tabpanel" class="tab-pane fade in" id="highlights">
							<div class="row">
								<h3>Highlights</h3>
								<ul class="list">
									<li> 2 person sharing for boys hostel</li>
									<li>3 person sharing for girls hostel</li>
									<li>Study table with locker</li>
									<li>Attached Bathrooms</li>
									<li>Separate utility for each rooms (Boys Hostel)</li>
									<li>Fire safety measures</li>
									<li>RO and hot drinking water facility</li>
									<li>Solar heat pump</li>
									<li>Reading room with newspaper & magazines</li>
									<li>24 hrs water / Electricity</li>
									<li>Standby Generator</li>
									<li>Women Security in girl's residence</li>
									<li>Regular Quality & Health Checks for Kitchens</li>
									<li>House Keeping Staff</li>
									<li>Clean & Green Campus</li>
									<li>24/7 security</li>
									<li>Regular inspections from college faculty and principal</li>
									<li>Medical Services with ambulance</li>
									<li>Security cameras installed all over the hostels</li>
									<li>Playground</li>
									<li>Indoor games</li>
									<li>Elevator facility</li>
									<li>CCTV Cameras</li>
									<li>Washing area</li>
									<li>Clean and hygiene kitchen and dining</li>
									<li>Gym in boys hostel</li>
								</ul>
							</div>
						</div>
						<!-- highlights -->
						<!-- rooms -->
						<div role="tabpanel" class="tab-pane fade in" id="rooms">
							<div class="row">
								<h3>Rooms</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/r11.jpg" alt="image" />	
										<div class="qualification_info">
											<!--    <h3 class="qual-text">Two Person Sharing </h3>  -->
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/r2.jpg" alt="image" />	
										<div class="qualification_info">
											<!--   <h3 class="qual-text">Three Person Sharing</h3>  -->
											<h4 class="qual-text">Girls Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<p> Hostels are Designed in a student friendly manner, not only with comfortable spacious 
											rooms, clean and hygiene modern attached bathroom. The rooms are also well furnished with wardrobe and study table, 
											to meet the need of students. 
										</p>
										<p>The hostels are literally homes for those who want to share, care and prosper.  </p>
										<p>The six story building of boys hostel is built with 200 rooms of 2 sharing each with 400 students capacity. The five story 
											building of girls hostel consists of 54 rooms of three sharing each 162 students capacity.
										</p>
										<p>Students from various ethnic roots & languages can stay together and learn a lot to get quite an exposure to diversity.</p>
									</div>
								</div>
							</div>
						</div>
						<!-- rooms -->
						<!-- study -->
						<div role="tabpanel" class="tab-pane fade in" id="study">
							<div class="row">
								<h3>Study Rooms</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/sr1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Study Room </h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/sr2.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Study Room </h3>
											<h4 class="qual-text">Girls Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<p>A common study area for academic discussions for innovative thinking with 400 square meters of 
											space in boys hostel and 100 square meters space in girls hostel 
										</p>
										<p>The reading rooms has a provision of daily news papers and magazines.</p>
										<p>The diversity intend to promote the interactions and collaboration with an unlike communion
											of ideas, life style and better student celebrating differences and exposure.
										</p>
									</div>
								</div>
							</div>
						</div>
						<!-- study -->
						<!-- gym -->
						<div role="tabpanel" class="tab-pane fade in" id="gym">
							<div class="row">
								<h3>Gym</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/g11.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Gym </h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/g12.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Gym</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="row aboutus_area wow fadeInLeft">
										<div class="col-lg-6 col-md-6 col-sm-6">
											<img class="hod-img img-responsive" src="${img_path!}/hostel/about/g13.jpg" alt="image" />	
											<div class="qualification_info">
												<h3 class="qual-text">Gym </h3>
												<h4 class="qual-text">Boys Hostel </h4>
												<h3 class="qual-text"></h3>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6">
											<img class="hod-img img-responsive" src="${img_path!}/hostel/about/g44.jpg" alt="image" />	
											<div class="qualification_info">
												<h3 class="qual-text">Gym</h3>
												<h4 class="qual-text">Boys Hostel </h4>
												<h3 class="qual-text"></h3>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<p>We have well equipped gym in the boys hostel for students which signifies not only
												studies but also physical fitness too is important to keep the mind balanced and happy.
												To enhance the social and recreational facilities there is various clamps and fitness
												activities.
											</p>
											<p>The gym has latest machines and equipments for the students including treadmills, stationary bikes, elliptical machines,
												rowing machines and a wide range of weights and plates.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- gym -->
						<!-- tv -->
						<div role="tabpanel" class="tab-pane fade in" id="tv">
							<div class="row">
								<h3>Tv Room</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/t11.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Tv Room </h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<p>We have a common TV room  where the students can get together to get a part of entertainment
											like movies and sports matches, in the leasure time and holidays.
										</p>
										<p>The boys hostel Tv room has a seating capacity of 150 students, whereas the girls hostel 
											has the Tv mounted in the mess area itself.
										</p>
									</div>
								</div>
							</div>
						</div>
						<!-- tv -->
						<!-- indoor -->
						<div role="tabpanel" class="tab-pane fade in" id="indoor">
							<div class="row">
								<h3>Indoor Games</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/i1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Indoor Games </h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/i2.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Indoor Games </h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/i3.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Indoor Games </h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/i4.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Indoor Games </h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<p>The common area of over 1000sq. meters in boys hostel is been provided for indoor games 
											consisting of table-tennis, carrom boards, chess and other indoor games for the recreation 
											of the students. The girls staying in hostel can access the indoor games center inside the KSIT 
											campus. 
										</p>
									</div>
								</div>
							</div>
						</div>
						<!-- indoor -->
						<!-- Kitchen and Dining -->
						<div role="tabpanel" class="tab-pane fade in" id="kitchen">
							<div class="row">
								<h3>Kitchen and Dining</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k6.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining </h3>
											<h4 class="qual-text">Girls Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k2.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k3.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k4.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k5.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k7.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/k8.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Kitchen and Dining</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<p>The food bringing people together, the hostel's food/dining area brings nutritisious
											and healthy cusine with North Indian, South Indian, pure vegetarian meals.The hostel 
											premises comprises of a separate dining facility for hostel residents. The hostel mess
											has a separate dining hall and a well-equipped, Clean and hygienic kitchen. As healthy
											food is the key of healthy mind and when mind is healthy students can concentrate on
											their studies and other academic activities, this objective is being fulfilled by 
											the Hostel Mess having 200 students at a time. 
										</p>
										<p>The food served is fresh and of high quality which meets the nutritional quality
											standards.
										</p>
										<p>Breakfast, lunch and dinner included.</p>
									</div>
								</div>
							</div>
						</div>
						<!-- Kitchen and Dining -->
						<!-- visitors -->
						<div role="tabpanel" class="tab-pane fade in" id="visitors">
							<div class="row">
								<h3>Visitor's Lobby</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/v1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Visitor's Lobby</h3>
											<h4 class="qual-text">Boys Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/v2.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Visitor's Lobby </h3>
											<h4 class="qual-text">Girls Hostel </h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<p>Both boys and girls hostel has a spacious visitor's lobby where the Tv is mounted along with
											newspapers and magazines for the refreshment of visitors. 
										</p>
									</div>
								</div>
							</div>
						</div>
						<!-- visitors -->
						<!-- other amenities -->
						<div role="tabpanel" class="tab-pane fade in" id="amenities">
							<div class="row">
								<h3>Other Amenities</h3>
								<div class="row aboutus_area wow fadeInLeft">
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/so1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Solar Heat Pump</h3>
											<h4 class="qual-text">Girls Hostel</h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/so2.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Solar Heat Pump</h3>
											<h4 class="qual-text">Boys Hostel</h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/ro1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">RO Water Purifier</h3>
											<h4 class="qual-text">Boys Hostel</h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/ro2.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">RO Water Purifier</h3>
											<h4 class="qual-text">Girls Hostel</h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/ss1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">CCTV Surveillance</h3>
											<h4 class="qual-text"></h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/wa1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Washing Area</h3>
											<h4 class="qual-text"></h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/pg1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Power Generator</h3>
											<h4 class="qual-text"></h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
										<img class="hod-img img-responsive" src="${img_path!}/hostel/about/e1.jpg" alt="image" />	
										<div class="qualification_info">
											<h3 class="qual-text">Elevator</h3>
											<h4 class="qual-text"></h4>
											<h3 class="qual-text"></h3>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<p></p>
									</div>
								</div>
							</div>
						</div>
						<!-- other amenities -->
						<!-- Hostel Fees  -->
						<div role="tabpanel" class="tab-pane fade" id="fees">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12">
									<h3>Hostel Fees</h3>
									<p>For queries related to hostel admissions and fees , please contact KSIT office.</p>
									<p>Phone : 080-28435722, 080-28435724 </p>
									<#list hostelFeesList as hostelFees>
									<p>${hostelFees.heading!}  <a href="${hostelFees.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
									</#list>	
								</div>
							</div>
						</div>
						<!-- Hostel Fees -->   
						<!-- Hostel Enrolment  -->
						<div role="tabpanel" class="tab-pane fade" id="enrolment">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12">
									<h3>Hostel Enrolment</h3>
									<p>Fill in Your Details, Once we receive the filled form, we will contact you shortly to confirm room availability.</p>
									<div class="contact_form wow fadeInLeft">
										<form action="/hostel" class="submitphoto_form" method="post">
											<div class="form-group">
												<label for="collegeName">College Name</label>
												<select name="collegeName" class="form-select">
													<option value="KSIT">KSIT</option>
													<option value="KSSEM">KSSEM</option>
												</select>
											</div>
											<div class="form-group">
												<label for="firstName">First Name</label>											
												<input type="text" class="wp-form-control wpcf7-text" name="firstName" placeholder="First name" required>
											</div>
											<div class="form-group">
												<label for="lastName">Last Name</label>																						
												<input type="text" class="wp-form-control wpcf7-text" name="lastName" placeholder="Last name" required>
											</div>
											<div class="form-group">
												<label for="year">Year</label>
												<select name="year" class="form-select">
													<option value="2021">2021-2022</option>
													<option value="2022">2022-2023</option>
												</select>
											</div>
											<div class="form-group">
												<label for="sem">Semester</label>											
												<select name="sem" class="form-select">
													<#list 1..8 as index>
														<option value="${index}">${index}</option>													
													</#list>
												</select>
											</div>
											<div class="form-group">
												<label for="phone">Phone No</label>											
												<input type="phone" class="wp-form-control wpcf7-phone" name="phone" placeholder="Phone No" required>
											</div> 
											<div class="form-group">
												<label for="email">Email Address</label>	
												<input type="email" class="wp-form-control wpcf7-email" name="email" placeholder="Email address" required>
											</div>
											<div class="form-group">
												<label for="fatherName">Father Name</label>
												<input type="text" class="wp-form-control wpcf7-text" name="fatherName" placeholder="Father Name" required> 													
											</div>
											<div class="form-group">
												<label for="fatherPhone">Father Phone No</label>
												<input type="phone" class="wp-form-control wpcf7-phone" name="fatherPhone" placeholder="Father Phone No" required>    												
											</div> 
											<div class="form-group">
												<label for="motherName">Mother Name</label>
												<input type="text" class="wp-form-control wpcf7-text" name="motherName" placeholder="Mother Name" required> 													
											</div>
											<div class="form-group">
												<label for="motherPhone">Mother Phone No</label>
												<input type="phone" class="wp-form-control wpcf7-phone" name="motherPhone" placeholder="Mother Phone No" required>    												
											</div>
											<div class="form-group">
												<label for="address">Address</label>
												<textarea class="wp-form-control wpcf7-textarea" name="address" cols="30" rows="10" placeholder="Address" required></textarea>																							
											</div>
											<div class="form-group"> 		 																																				
												<input type="submit" value="Submit" class="wpcf7-submit">
											</div>
										</form>
										<br/>
									</div>
								</div>
							</div>
						</div>
						<!-- Hostel Enrolment -->  
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--dept tabs -->
<script>
	function showSuccessAlert() {
	  alert("Hostel Enquiry Submitted successfully");
	}
	
	$(document).ready(function () {
		<#if showSuccess?? && showSuccess>
			showSuccessAlert();
		</#if>
	});
</script>
</@page>