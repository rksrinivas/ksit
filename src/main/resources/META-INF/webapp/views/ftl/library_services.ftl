<@page>
	 
	<!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs_course">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="aboutus_area wow fadeInLeft">
            <h1> Services / Facilities</h1>
				
				
				<p>
			  
				
					<ul class="list" id="circulation_service">
					
						<h3>FACILITIES</h3>
						
						<li>Stack Area</li>
						<li>Reference Section</li>
						<li>Periodicals Section</li>
						<li>Reading Hall</li>
						<li>Digital Library</li>
						<li>Discussion Room</li>
						
						<h3>SERVICES</h3>
						
						<li>Circulation Service</li>
						<li>Reference Service</li>
						<li>User Awareness Programme</li>
						<li>Institutional Repository</li>
						<li>Reprographic Service</li>
						<li>Inter Library Loan </li>
						<li>OPAC (Online Public Access Catalog)</li>
						<li>NPTEL</li>
						<li>News Paper Clippings</li>	 
						
			   
					</ul>
			   
			   </p>
			        

				
			</div>
        </div>
        
	  
			   
          </div>
        </div>
	  
	 
	
	
	  
	  </div>
	  </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 

 
	 
	 
</@page>