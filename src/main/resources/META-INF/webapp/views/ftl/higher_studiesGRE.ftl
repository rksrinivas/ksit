<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3 class="text-center">Higher Studies & GRE</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote>
                    
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <p> Placement Department regularly organizes talks by various professionals from different universities for pursuing higher education in abroad. Trainers conduct special seminars and talks for taking competitive exams like CAT, GMAT, Aptitude etc.</p>
					
					<p>Several students from different branches are pursuing the higher studies across the globe.</p>
					
					<p><span style="color:#000;font-size:18px;">Byju's</span> - The Learning App, India's leading ed-tech company is providing following free resources for our students to help you in higher education competitive exams and campus recruitment training.
					 Click on the following link to learn more</p>
					 
					 <ul class="list" style="color:#000;">
					 	<li><a href="https://byjus.com/free-gre-prep/" target="_blank">GRE Free Prep</a></li>
					 	<li><a href="https://byjus.com/free-gmat-prep/" target="_blank">GMAT Free Prep</a></li>
					 	<li><a href="https://play.google.com/store/apps/details?id=com.byjus.thelearningapp&hl=en" target="_blank">CRT 7 Days Training</a></li>
					 </ul>

                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<!--table-->
		 			<div class="col-md-12 col-lg-12 col-sm-12">
								  
				                      <span class="fa fa-quote-left"></span>
				                      
				                      <#list yearSet as year>
			  		 					<#setting number_format="0" />
				                      
									  <p>Higher Education Details of the year ${year?c!}</p>
				                    
								  
								  		 <table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.NO. </th>
												<th>Name of the Program</th>
												<th>Semester</th>
												<th>Batch</th>
												<th>Company</th>
												<th>Date</th>
										
											  </tr>
											</thead>
											<tbody>
											
											<#assign count = 1>
											<#list placementHigherStudiesList as placementHigherStudies>
												<#if placementHigherStudies.year == year>
												
												
												  <tr>        
													<td>${count!}</td>
													<td>${placementHigherStudies.program!}</td>	
													<td>${placementHigherStudies.semester!}</td>
													<td>${placementHigherStudies.year!}</td>
													<td>${placementHigherStudies.company!}</td>
													<td>${placementHigherStudies.schedule!}</td>
													
													<#assign count = count + 1>
													
												</tr>	
												</#if>
											
											</#list>
											</tbody>
										  </table>
								  		</#list> 
										
						</div>
			
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>