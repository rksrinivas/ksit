<@page>
<section id="about">
   <div class="container">
   <div class="news_area home-4">
      <div class="row">
         <div class="col-md-12">
            <h3>Souvenir</h3>
            <div class="aboutus_area wow fadeInLeft">
            
            	<p>There is certain nostalgia and romance in a place you left. Human's natural tendency is to wish to relive and enhance fond memories. Souvenir is one such expression of grateful thanks to those who have labored and contributed towards the Institution.
            	</p>
            	<p>Souvenir has many stories unfold from the four years of a student's life in college and many have common threads running through them. . Our souvenir has such warm memories and rich tributes to Kammavari Sangham Institute of Technology
            	</p>
            	
            	
            
               <#list souvenirPageSouvenirList as souvenirPageSouvenir>
               <p>${souvenirPageSouvenir.heading!}  <a href="${souvenirPageSouvenir.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
               </#list>
            </div>
         </div>
      </div>
   </div>
</section>
</@page>