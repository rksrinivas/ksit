<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive_admin">
      <div class="container">
	   
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">

			  <!-- start blog archive  -->
              <div class="row">
                <!-- start single blog archive -->
                <div class="col-lg-6 col-6 col-sm-8">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/administration/directorAcademics.jpg">
                      </a>
                    </div>
                    <h3 class="blog_title" style="text-align:center;"><span style="color: darkblue;">Dr. Sangappa S.B</span></h3>
					<h4 class="blog_title" style="text-align:center;"><span style="color: red;">Director - ADM & PRO</span></h4>
                    <!--<p class="blog_summary">Duis erat purus, tincidunt vel ullamcorper ut, consequat tempus nibh. Proin condimentum risus ligula, dignissim mollis tortor hendrerit vel. Aliquam...</p>
                    <a class="blog_readmore" href="events-single.html">Read More</a>-->
                  </div>
                </div>
                <!-- End single blog archive -->
                
                <div class="container-fluid">
				<p>Hearty Congratulations for having decided to join Kammavari Sangham Institute of Technology (KSIT) that has
				 successfully shaped and molded the professional careers of many successful engineers serving the society at large
				  in various capacities. Let me welcome you all to this great temple of learning which is committed to give both quality
				   technical education strengthened ethical values and honed with training.</p>
				<p>	At KSIT, we believe in the holistic development of Engineering, Software and other professionals by imbibing
				 both hard skills as well as soft skills through our well trained and dedicated teachers as well as equally competent 
				 training partners. We prepare our graduates to take on multidimensional challenges and effectively address the
				  societal problems. The institution has now embarked on the process of getting itself accredited by national 
				  agencies like NAAC and NBA.</p>
				<p>	KSIT is affiliated to Visvesvaraya Technological University, Belgaum,  and offers Bachelor's degree in AI&ML,
				 CS&E, E&C and Mechanical Engineering and Post Graduate degrees in Computer Science, Digital Electronics & Communication
				  and Machine Design. There are four Research Centres where the Scholars pursue their Ph.D. program.</p><br/>
				<p>	Being located on the Kanakpura Main Road the institution is approachable by road and also the METRO. The college
				 also offers  good bus facilities from various locations to enable the staff and students to reach the campus comfortably.
				  The college offers multiple student support systems like Mentoring System, National Social Service, sports, 
				  and career guidance that enable the students to have an all round development.</p>
				<p>	KSIT has a very enviable learning environment with a very good staff- student relationship along with plenty
				 of opportunities to develop through professional bodies and clubs. These opportunities bring out the best of talent 
				 in each of the student and create opportunities to pursue their passion and interests. These activities harp on the
				  development of personality and character of each of our students. Our academic standards continue to stay tuned 
				   with the improvements made by our affiliating university that ensures the success of our graduates in an ever 
				   changing world.</p>
				<p>	This institution is governed by a group of dedicated philanthropists who give a considerable amount of their 
				valuable time to run this institution. The Management is ably supported by an Academic Advisory Board that consists 
				of former Vice Chancellors, reputed academicians and industrialists. This Board advices the Management on all academic 
				issues and systems. The Board has introduced a concept of bringing people from the industry to the academia to 
				develop programming and logic skills, experiential learning and do socially relevant projects through design
				 and implementation. The institution has a philosophy of mentoring and nurturing students on a one to one concept
				  where the students get more personal attention and guidance.</p>
				<p>	KSIT is committed to provide holistic education and believes in the overall development of its students who
				 are transformed into ethical professionals with a flair for serving the society. It imbibes in its students
				  certain value systems that will take them high in their professional careers. These values include hard work, 
				  perseverance, accepting challenges, adapting to changes, never give up attitude, helping one another, social
				   responsibility, sense of belonging, mutual respect and so on. KSIT is a place to be proud of and our students 
				   are our ambassadors and are expected to take the name of the institution to very high places and make both KSIT
				    and their parents very proud. KSIT is committed to prepare its students to become global citizens</p>
			</div>
        </div>
      </div>
    </section>
<!--=========== END COURSE BANNER SECTION ================-->
</@page>	