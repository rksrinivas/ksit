<@page>
<br/>
    <!--content -->
	<section id="courseArchive" style="margin-top:0px;">
      <div class="container">
	  <div class="row">
					
		<div class="col-lg-12 col-md-12 col-sm-12">
				<h1 class="titile text-center" style="margin-bottom:40px;">Academic Advisory Board</h1>
		
			<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="">
						<img class="img-responsive" src="${img_path!}/administration/khincha.jpg" alt="image" />
					</div>
													
					<p class="image_caption"><span style="color:darkblue;font-weight:500;">Dr. H.P Khincha</span></p>
					<p class="image_caption">Former Vice Chancellor VTU and Professor IISc</p>
					<p class="image_caption"><span style="color:red;">Chairman</span></p>
													
			</div>
												
			<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="">
						<img class="img-responsive" src="${img_path!}/administration/murthy.jpg" alt="image" />
					</div>
													
					<p class="image_caption"><span style="color:darkblue;font-weight:500;">Dr. K.N.B. Murthy</span></p>
					<p class="image_caption">Vice Chancellor of PES University </p>
					<p class="image_caption"><span style="color:red;">Member</span></p>
													
			</div>									
		</div>
	<!-- members-->
</section>
<br/>
</@page>