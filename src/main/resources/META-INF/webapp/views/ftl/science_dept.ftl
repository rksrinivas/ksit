<@page>

<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/banner/bs_s1.jpg" alt="">
                       
                        </article>
                        <article class="item">
                           <img class="animated bounceIn" src="${img_path!}/science/slider/s1.jpg" alt="">										
                        
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/science/slider/s2.jpg" alt="">
                         
                        </article>
                         <article class="item">
                           <img class="animated rollIn" src="${img_path!}/science/slider/s3.jpg" alt="">
                          
                        </article>
                        
                         <article class="item">
                           <img class="animated rollIn" src="${img_path!}/science/slider/s4.jpg" alt="">                         
                        </article>
                        
                         <article class="item">
                           <img class="animated rollIn" src="${img_path!}/science/slider/s5.jpg" alt="">                         
                        </article>

                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 


    
<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To Applied Science And Humanities</h1>
   <p class="welcome-text">The Applied Sciences department comprises of the three branches of Mathematics, Physics and Chemistry. They play a vital role in shaping the engineering knowledge acquisition and learning process. </p>
</div>
<!-- welcome -->
    
    
    <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12 padding">
			            <div class="vertical-tab" role="tabpanel">
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
			                    <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab">Vision & Mission</a></li> 
			                    <li role="presentation"><a href="#mathematics" aria-controls="mathematics" role="tab" data-toggle="tab">Mathematics</a></li>
			                    <li role="presentation"><a href="#physics" aria-controls="physics" role="tab" data-toggle="tab">Physics</a></li>
			                    <li role="presentation"><a href="#chemistry" aria-controls="chemistry" role="tab" data-toggle="tab">chemistry</a></li>
						        <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
			        	       <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li>
			                    
			                    
			                    <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>
				               
				                <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
				                <li role="presentation"><a href="#pedagogy" aria-controls="pedagogy" role="tab" data-toggle="tab" class="dept-cart">Pedagogy</a></li>               
				               
				               
				                <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Time Table</a></li>

				               <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>
			               
				               <!-- <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li> -->
				                <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
				                <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
				                <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
				           
				           		
			                    
			                    <!--<li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Section 3</a></li>
			                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>-->
			                </ul>
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                    
			                    <!-- vision and mission -->
			                    <div role="tabpanel" class="tab-pane fade in active" id="profile">	
			                    	<div class="container-fluid">		                    			                    
			                          <div class="aboutus_area wow fadeInLeft">
		  								 <h3>Profile</h3>
										 <p>Basic Science is an idea, a culmination of ideas that constitute everything in our daily life. 
										 Department of Science and Humanities encompasses Mathematics, Physics, Chemistry, English, Kannada, 
										 Professional ethics and cyber laws and Indian constitution courses being offered to all engineering
										  students. An engineer in particular, keeps coming back to basic Physics and Chemistry in his career.
										   Mathematics, Physics and Chemistry will forever remain the 3 cardinal factors responsible for Earth's
										    existence and Humanities will always be an explanation of the difficulties faced by them in its continuous
										     journey through the eras. The department has remained true to the vision on which it was founded.</p>
									<p>The basic science department has a total 10 teaching faculty members with various specialization, 
									out of which 2 are Ph.D. holders and 6 are pursuing Ph.D. The faculties in the department understands 
									and are extremely helpful to the students in all their endeavours and make sure they receive excellent
									 care and immense motivation and the department has strong R & D centre. Laboratories and Research departments 
									 are fully equipped that serves as a platform to quench the thirst of the students who are practical hunters.</p>
									
									<p>Every year department releases its newsletter Highlighting academic, Co-curricular activities of staff and students. 
									Various workshops on programming skills, carrier guidance, cloud, Big Data Analytics, Artificial Intelligence, 
									IoT and Networks remain active throughout the year to enhance the skills of staff and students. Our department has 
									several MOU with industries, R&D and training centers to give guidance, assistance and internships facilities for UG
									 and PG students as well as collaborative research for PhD students.</p>	
						<h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <img class="hod-img img-responsive" src="${img_path!}/science/hod/basic_science_and_humanities_hod.jpeg" alt="image" />	
                         <div class="qualification_info">
	                        <h3 class="qual-text">Dr.JALAJA P</h3>
	                        <h4 class="qual-text">Professor & Head, Applied science and humanities</h4>
	                        <h3 class="qual-text">M.Sc, M.Phil, Ph.D</h3>
                        </div>	
                       
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>Head, department of BS&H, has total of 20 years of teaching experience.
                        Prof Jalaja P obtained master's degree in mathematics in Sri padmavathi visva vidyalayam,Thirupathi University 
                        and completed her doctoral degree from Visvesvaraya technological University,Belagalvi. 
                        Area of research is Fluid Dynamics.</p>
                     </div>
              
                  </div>
											
											
									  </div>
									</div>
			                    </div>

			                    <!-- vision and mission -->
			                    
			                      <!-- vision and mission -->
			                    <div role="tabpanel" class="tab-pane fade in" id="vision">	
			                    	<div class="container-fluid">		                    			                    
			                          <div class="aboutus_area wow fadeInLeft">
		  								  <h3>Vision</h3>
						                     <ul class="list">
						                        <li>To create a conducive environment to impart sound fundamentals and problem solving 
						                         skills among the students and prepare them for higher learning.</li>
						                     </ul>
						                     <h3>Mission</h3>
						                     <ul class="list">
						                        <li>To continuously improve the teaching-learning process and maintain effective pedagogy.</li>
						                        <li>To inculcate ethical and professional values among students.</li>
						                        <li>To create institute-industry interaction to promote research.</li>
						                     </ul>
						                     
						                     <h3>PROGRAM OUTCOMES</h3>
                    						  <p><span>PO1 Engineering knowledge:  </span>Apply the knowledge of mathematics, science, engineering fundamentals, and an engineering specialization to the solution of complex engineering problems.</p>
                    						  <p><span> PO2 Problem analysis: </span>Identify, formulate, research literature, and analyze complex engineering problems reaching substantiated conclusions using first principles of mathematics, natural sciences, and engineering sciences.
                    						  </p>
                    						  <p><span>PO3 Design/development of solutions: </span>Design solutions for complex engineering problems and design system components or processes that meet the specified needs with appropriate consideration for the public health and safety, and the cultural, societal, and environmental considerations.
                    						  </p>
                    						  <p><span>PO4 Conduct investigations of complex problems: </span>Use research-based knowledge and research methods including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions.
                    						  </p>
                    						  <p><span>PO5 Modern tool usage: </span>Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations.
                    						  </p>
                    						  <p><span>PO6 The engineer and society: </span>Apply reasoning informed by the contextual knowledge to assess societal, health, safety, legal and cultural issues and the consequent responsibilities relevant to the professional engineering practice.
                    						  </p>
                    						  <p><span>PO7 Environment and sustainability: </span>Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development.
                    						  </p>
                    						  <p><span>PO8 Ethics: </span>Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice.
                    						  </p>
                    						  <p><span>PO9 Individual and team work: </span>Function effectively as an individual, and as a member or leader in diverse teams, and in multidisciplinary settings.
                    						  </p>
                    						  <p><span>PO10 Communication: </span>Communicate effectively on complex engineering activities with the engineering community and with the society at large, such as, being able to comprehend and write effective reports and design documentation, make effective presentations, and give and receive clear instructions.
                    						  </p>
                    						  <p><span>PO11 Project management and finance: </span>Demonstrate knowledge understanding of the engineering and 
                    						  management principles and apply these to one's own work, as a member and leader in a team, to manage projects 
                    						  and in multidisciplinary environments.
                    						  </p>
                    						  <p><span>PO12 Life-long learning: </span>Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change.
                    						  </p>
                    						  
						                     
										
									  </div>
									</div>
			                    </div>

			                    <!-- vision and mission -->
			                    
			 <!-- Research and development -->
               <div role="tabpanel" class="tab-pane fade" id="research">               
                  <div class="container-fluid">
                  <h3>Research and Development</h3>
                  
                  
				 <h3>DEPARTMENT OF CHEMISTRY- CENTER FOR NANOSCIENCE</h3>

					<p>Chemistry R&D is a central facility available to students and faculty of KSIT College of Engineering to work on Chemical processes and Nano research areas and applications from all branches of engineering. The mission of this facility is to support research and educational objectives of KSIT and offer state of the art fabrication and characterization facilities and services to novel and technologically relevant research. Its is a matter of Proudness to announce that The Department has been recognized as R & D Centre by Visvesvaraya Technological University for carrying out Research activities leading to M. Sc. (Engg.) and Ph.D. degrees. Established in the year 2007-08 CENTER FOR NANOSCIENCE and envisions growth to meet the future demands of materials and chemical processes.</p> 

					<h3>Major objectives of the CENTER FOR NANOSCIENCE are:</h3>
					<p>To create indigenous capability in the emerging areas like Nano Materials, composites, Synthesis of Nanoparticles, characterization and application towards energy and environment, Adsorption studies Electrodeposition, Kinetics of Biomolecules Photocatalysis, Electrode Modification & Sensors etc. Promotion of research among faculties by conducting programs on research methodology, research possibilities in science and technology. CENTER FOR NANOSCIENCE facilities provide an environment for the overall career development of researchers, students and faculty .</p>
                  

                  <div class="container-fluid">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     		
                        <h3>R & D Activities</h3>
                        
                        <#list scienceResearchList as scienceResearch>
                        	<p>${scienceResearch.heading!}  <a href="${scienceResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        
                        <#list scienceSponsoredProjectsList as scienceSponsoredProjects>
                        	<p>${scienceSponsoredProjects.heading!}  <a href="${scienceSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        
                        <#list sciencePhdPursuingList as sciencePhdPursuing>
                        	<p>${sciencePhdPursuing.heading!}  <a href="${sciencePhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        
                        <h3>Ph.D Awardees</h3>
                        
                         <#list sciencePhdAwardeesList as sciencePhdAwardees>
                        	<p>${sciencePhdAwardees.heading!}  <a href="${sciencePhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                       
                     		
                     		
                     		<!--<p> <a href="${img_path!}/science/chemistry_events/conference.pdf" target="_blank">Conference</a></p>
							
							<p> <a href="${img_path!}/science/chemistry_events/journals.pdf" target="_blank">Journals</a></p>
							
							<p> <a href="${img_path!}/science/chemistry_events/Chemistry R&D details.pdf" target="_blank">Chemistry R&D details</a></p>
							
							<p> <a href="${img_path!}/science/chemistry_events/publications.pdf" target="_blank">National and International List of Publications </a></p> -->
							
							
                     </div>
                  </div>
                  </div>
               </div>
               <!-- Research and development -->
               
               
               
                 <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="container-fluid">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>Instructional Materials</h3>
                        
                          <#list scienceInstructionalMaterialsList as scienceInstructionalMaterials>
	                    
	                        <p>${scienceInstructionalMaterials.heading!}  
	                      	    <a href="//${scienceInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                          <h3>Lab Manuals</h3>
                        
                          <#list scienceLabManualList as scienceLabManual>
	                    
	                        <p>${scienceLabManual.heading!}  
	                      	    <a href="//${scienceLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                    
                       											
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
               <!-- pedagogy -->

                <div role="tabpanel" class="tab-pane fade" id="pedagogy">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     	  <h3>Pedagogical Activities</h3>
                        
                          <#list sciencePedagogicalActivitiesList as sciencePedagogicalActivities>
	                    
	                        <p>${sciencePedagogicalActivities.heading!}  
	                      	    <a href="//${sciencePedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogy Review Form</h3>
                        
                          <#list sciencePedagogyReviewFormList as sciencePedagogyReviewForm>
	                    
	                        <p>${sciencePedagogyReviewForm.heading!}  
	                      	    <a href="//${sciencePedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>                     	
                       											
                     </div>
                  </div>
               </div>
              
                <!-- pedagogy -->
               
               
               
               
               
               <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="container-fluid">
                     <h3>Time Table</h3>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list scienceClassTimeTableList as scienceClassTimeTable>              
                        	<p>${scienceClassTimeTable.heading!}  <a href="${scienceClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list scienceTestTimeTableList as scienceTestTimeTable>
                         <p>${scienceTestTimeTable.heading!}  <a href="${scienceTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list scienceCalanderList as scienceCalander>              
                         <p>${scienceCalander.heading!}  <a href="${scienceCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--Time Table  -->
               
               
                      <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="container-fluid">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>Physics</h3>
                        
                        <#list physicsSyllabusList as physicsSyllabus>
                        <p>${physicsSyllabus.heading!}  <a href="${physicsSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                        <h3>Chemistry</h3>
                        
                        <#list chemistrySyllabusList as chemistrySyllabus>
                        <p>${chemistrySyllabus.heading!}  <a href="${chemistrySyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                      
                        <h3>Mathematics</h3>
                        
                        <#list mathematicsSyllabusList as mathematicsSyllabus>
                        <p>${mathematicsSyllabus.heading!}  <a href="${mathematicsSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                      
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
             
             
               
               <!--News Letter  -->
               <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="container-fluid">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>News letter</h3>
                        <#list scienceNewsLetterList as scienceNewsLetter>
                          <p>${scienceNewsLetter.heading!}  <a href="${scienceNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--News Letter  -->
               
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                    <div class="container-fluid">
                    <h3>Events</h3>
                    
                    <#list scienceEventsList[0..*3] as scienceEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${scienceEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h1>${scienceEvent.heading!}</h1>	                      	 
	                       	 <p>"${scienceEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${scienceEvent.eventDate!}  </span></p>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
                    <a class="btn btn-primary" href="science_events.html">View more</a>                    
  						
                    </div>    
                  </div>
                  <!-- Events  -->
                  
                         <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="container-fluid">
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
		                  <#list scienceLabList as scienceLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${scienceLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${scienceLab.name!}</h3>	                      	 
			                       	 <p>${scienceLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${scienceLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
                   <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="container-fluid">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        
                          <#list scienceEresourcesList as scienceEresources>
	                    
	                        <p>${scienceEresources.heading!}  
	                      	    <a href="//${scienceEresources.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
                  <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="container-fluid">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list scienceFdpList as scienceFdp>
                        <p>${scienceFdp.heading!}  <a href="${scienceFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
                
               
               
			                    
			                    <!-- Mathematics-->
			                    <div role="tabpanel" class="tab-pane fade" id="mathematics">
			                      <div class="container-fluid">		                    			                    
			                          <div class="aboutus_area wow fadeInLeft">
			                       		 <h3>Mathematics</h3>
			                       		 <p>Mathematics is the basic tool of engineering which is the QUEEN of all sciences. Department of
			                       		  Mathematics came into existence with the establishment of the institution KSIT in the year 1999.
			                       		   Mathematics is taught to engineering students for first four semesters. The basic aim of the department 
			                       		   is in pursuit of academic excellence and better understanding of Mathematical skills from fundamental 
			                       		   level to its applications which is essential to every engineer.</p>

										<p>The department has five well qualified, experienced and dedicated faculty members with one professor 
										and four Assistant Professors Faculty members are also actively involved in research. The faculty members
										 together have published more than 25 research articles in national and international journals and
										  51 in national and international conferences. Faculty members constantly enrich their knowledge by 
										  innovative teaching methods, research and participating actively in seminars, conferences and workshops
										   at national and international level. The faculty of Mathematics department actively participate
										    both in curricular and co curricular activities of the college.</p>

										<p>Department has brought out student resources to motivate and help students in academics. The student
										 resource material consists of thoroughly prepared Notes, Question Bank and Assignments which are
										  distributed to them. Special classes are conducted for Diploma Students from lateral entry and 
										  slow learners. The graph of Mathematics result is always on the upper trend from 90 to 100 %.</p>
										 
										 										 
										  
										  <h3>Vision</h3>
			 
									 <ul class="list">
									 	<li>To impart quality technical education with ethical values, 
										 employable skills and  research to achieve excellence</li>
									 </ul>
									 
									  <h3>Program Specific Outcomes (PSO's)</h3>
									  
									  	<ul class="list">
									  		<li><strong>PSO1: </strong>Ability to apply Mathematical concepts in engineering to design a          
				            					system or a process.</li>
									  		<li><strong>PSO2: </strong>Ability to develop effective communication, team work and computational skills.</li>
									  	</ul>
									  
									  	
									    <h3>Mathematics Courses</h3>
									    
										<p>SEMESTER I</p>
										
										<p>Course Name &amp; Course&nbsp;code:</p>
										
										<p>Mathematics-I&nbsp;for CSE Stream&nbsp;-&nbsp;BMATS101</p>
										
										<p>Mathematics-I&nbsp;for EEE Stream&nbsp;-&nbsp;BMATE101</p>
										
										<p>Mathematics-I&nbsp;for ME Stream&nbsp;- &nbsp;&nbsp;BMATM101</p>
										
										<p>&nbsp;</p>
										
										<p>SEMESTER II</p>
										
										<p>Course Name &amp; Course&nbsp;code:</p>
										
										<p>Mathematics-II&nbsp;for CSE Stream&nbsp;-&nbsp;BMATS201</p>
										
										<p>Mathematics-II&nbsp;for EEE Stream&nbsp;-&nbsp;BMATE201</p>
										
										<p>Mathematics-II&nbsp;for ME Stream&nbsp;-&nbsp;BMATM201</p>
										
										<p>&nbsp;</p>
										
										<p>SEMESTER III</p>
										
										<p>Course Name &amp; Course&nbsp;code:</p>
										
										<p>COMPUTER&nbsp;SCIENCE &amp; ENGINEERING AND IT&rsquo;S ALLIED BRANCHES&nbsp;&nbsp; (CSD/AIML/IOT)</p>
										
										<p>&bull;&nbsp;Mathematics for Computer Science- BCS301</p>
										
										<p>COMPUTER &amp; COMMUNICATION ENGINEERING</p>
										
										<p>&bull;&nbsp;Mathematics for Computer &amp; Communication Engineering&nbsp;-BCM301</p>
										
										<p>ELECTRONICS &amp; COMMUNICATION ENGINEERING</p>
										
										<p>&bull;&nbsp;AV Mathematics-III for EC Engineering- BMATEC301</p>	  
										  
										  
			                         </div>
			                      </div>
			                    </div>
			                    <!--Mathematics -->
			                    
			                     <!-- Physics-->
			                    <div role="tabpanel" class="tab-pane fade" id="physics">
			                      <div class="container-fluid">		                    			                    
			                          <div class="aboutus_area wow fadeInLeft">
			                       		 <h3>Physics</h3>
			                       		  <p>Engineering physics is a common subject in the first year for all branches of engineering
											under Visvesvaraya Technological University. Department has very good infrastructure, well
											equipped and spacious laboratory. The department teaches Engineering Physics theory as
											well as laboratory experiments for all the Engineering branches. Faculties are well qualified,
											experienced and highly motivated and also involved in interdisciplinary research activities.
											The Physics Department has enthusiastic and academically experienced teaching and
											technical staff. Currently, the Department has one doctorate holder and the other faculty
											members have registered for their Ph.D. programme. The Faculties are actively involved in
											research activities in the areas of Photophysics, Material Science, Spectroscopy, NMR and IR
											Spectroscopy and Nanotechnology.</p>
											<p>The Department aims in equipping the future engineers with various aspects of
											Physics. The Engineering Physics Lab is well furnished with the latest equipment�s and all
											the necessary facilities. The Department also encourage Research activities for both students
											and faculty members to pursue experimental researches�in the field of engineering. </p>
														                       		  
									   <h3>Program Specific Outcomes (PSO's)</h3>
								 
								 		<ul class="list">
								 			<li><strong>PSO1: </strong>Ability to apply the concepts of physics to design a process to address        
					            				the real world challenges.</li>
								 			<li><strong> PSO2: </strong>Ability to develop effective communication team work and 
					             				computational skills</li>
								 		</ul>
								 
								 		 <h3>Physics Courses</h3>
								 		 										
										<p>FIRST YEAR&nbsp;(PHYSICS CYCLE)</p>
										
										<p>SEMESTER I&amp;II</p>
										
										<p>COMPUTER SCIENCE &amp; ENGINEERING AND IT&rsquo;S ALLIED BRANCHES(CSD/AIML/CCE/IOT)</p>
										
										<p>Course Name:&nbsp;APPLIED&nbsp;PHYSICS FOR CSE STREAM</p>
										
										<p>Course Code: BPHYS102/202</p>
										
										<p>&nbsp;</p>
										
										<p>ELECTRONICS AND COMMUNICATION ENGINEERING</p>
										
										<p>Course Name:&nbsp;APPLIED&nbsp;PHYSICS FOR&nbsp;EEE STREAM</p>
										
										<p>Course Code: BPHYE102/202</p>
										
										<p>&nbsp;</p>
										
										<p>MECHANICAL ENGINEERING</p>
										
										<p>Course Name:&nbsp;APPLIED&nbsp;PHYSICS FOR&nbsp;ME STREAM</p>
										
										<p>Course Code: BPHYM102/202</p>

						
			                       		 
			                         </div>
			                      </div>
			                    </div>
			                    <!--Physics -->
			                    
			                     <!-- Chemistry-->
			                    <div role="tabpanel" class="tab-pane fade" id="chemistry">
			                      <div class="container-fluid">		                    			                    
			                          <div class="aboutus_area wow fadeInLeft">
			                        	<h3>Chemistry</h3>
			                        	 <p>The Department of Chemistry continued its contribution to teaching and research to instil in our students, a sense of independence to acquire knowledge through curriculum and at the same time acquire sufficient awareness and exposure to support the curriculum. The faculty explicitly tried hard to motivate students towards attending seminars and conferences, so as to exhibit their developed models, present their creative ideas and innovative concepts in various fields of thrust interest to society. </p>
			                        	 
                 
										  <h3>Program Specific Outcomes (PSO's)</h3>
										  
										  <ul class="list">
										  	<li><strong>PSO1:</strong> Ability to apply concept of Chemistry to design a system, to address a       
									            real world challenges.</li>
											<li><strong>PSO2:</strong> Ability to develop effective communication, team work and 
									             computational skills.</li>
						
										  
										  </ul>
										  
										  	<h3>Chemitry Courses</h3>
										  
											<p>FIRST YEAR&nbsp;(CHEMISTRY&nbsp;CYCLE)</p>
											
											<p>SEMESTER I&amp;II</p>
											
											<p>COMPUTER SCIENCE &amp; ENGINEERING AND IT&rsquo;S ALLIED BRANCHES(CS&amp;D/AI&amp;ML/CCE/IOT)</p>
											
											<p>Course Name:&nbsp;APPLIED&nbsp;CHEMISTRY&nbsp;FOR CSE STREAM</p>
											
											<p>Course Code:&nbsp;BCHES102/202&nbsp;</p>
											
											<p>&nbsp;</p>
											
											<p>ELECTRONICS AND COMMUNICATION ENGINEERING</p>
											
											<p>Course Name:&nbsp;APPLIED&nbsp;CHEMISTRY&nbsp;FOR&nbsp;EEE STREAM</p>
											
											<p>Course Code: BCHEE102/202</p>
											
											<p>&nbsp;</p>
											
											<p>MECHANICAL ENGINEERING</p>
											
											<p>Course Name:&nbsp;APPLIED&nbsp;CHEMISTRY&nbsp;FOR&nbsp;ME STREAM</p>
											
											<p>Course Code: BCHEM102/202</p>

												
			                         </div>
			                      </div>
			                    </div>
			                    <!--Chemistry -->
			                    
			                    
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="container-fluid">
                  
                     <h3>Mathematics Faculty</h3>
                     
                     <!--Mathematics Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list mathsFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Mathematics Teaching faculty-->
                     
                     <h3>Physics Faculty</h3>
                     
                     <!--Physics Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list physicsFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Physics Teaching faculty-->
                     
                     <h3>Chemistry Faculty</h3>
                     
                     <!--Chemistry Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list chemistryFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Chemistry Teaching faculty-->
                     
                     <h3>Humanities Faculty</h3>
                     
                     <!--Humanities Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list humanitiesFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Humanities Teaching faculty-->
                     
                     
                     <h3>Supporting Staff Faculty</h3>
                     
                     <!--Supporting Staff Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Supporting Staff faculty-->

                  </div>
               </div>
               <!--Faculty  -->
  
			                    <div role="tabpanel" class="tab-pane fade" id="Section3">
			                                      <div class="container-fluid">
			                    
			                        <h3>Section 3</h3>
			                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
			                    </div>
			                    </div>
			
			
			                    <div role="tabpanel" class="tab-pane fade" id="Section4">
			                      <div class="container-fluid">
			                    
			                        <h3>Section 4</h3>
			                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
			                    </div>
			                    </div>
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
    
</@page>