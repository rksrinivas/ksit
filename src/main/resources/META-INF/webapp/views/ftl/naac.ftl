<@page>
<h1 style="text-align: center;">NAAC-AQAR</h1>
<section id="vertical-tabs" style="min-height: 600px;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="vertical-tab padding" role="tabpanel">
					<ul class="nav nav-tabs" role="tablist" id="myTab">
						<#list yearList as year>
						
							<li role="${year?c}" <#if year?index==0>class="active"</#if>><a href="#${year?c}" aria-controls="${year?c}" role="tab" data-toggle="tab" class="dept-cart">${year?c}-${(year+1)?c}</a></li>
						</#list>					
					</ul>
					<div class="tab-content tabs">
						<#list yearList as year>
							<div role="tabpanel" class="tab-pane fade in <#if year?index==0>active</#if>" id="${year?c}">
								<h2 style="text-align: center;">${year?c}-${(year+1)?c}</h2>
								<table style="width: 100%">
									<#list yearMap?values as criteriaMap>
										<#list criteriaMap?values as naacList>
											<#if naacList[0].year == year>
												<tr>
													<ul>
														<li>
															${naacList[0].criteria.desc}
														</li>
														<li>
															<ul>
																<#list naacList as  naac>
																	<li style="padding: 20px"> - ${naac.heading} <a href="${naac.link}" target="_blank">view here</a></li>												
																</#list>
															</ul>
														</li>
													</ul>
												</tr>
											</#if>
										</#list>
									</#list>
								</table>
							</div>
						</#list>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

</@page>