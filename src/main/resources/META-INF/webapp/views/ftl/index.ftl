<@page>
	 <!--=========== slider  ================-->
      <section id="home_slider">
      <div class="container" style="width:100%;">
      

        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="home_slider_content">  
			
              <div class="row">
			  		<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			              <!--    <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="https://twitter.com/KSIT_official" target="_blank"><i class="fa fa-twitter"></i></a></li> -->
			                  <li><a data-toggle="tooltip" data-placement="top" title="Instagram" class="soc_tooltip insta"  href="https://www.instagram.com/ksit.official/" target="_blank"><i class="fa fa-instagram"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  		
			  		<!-- quick link slider-->
			  			<div id="quick_links_slider">
			  				<ul>
			  				<!--	<li><a href="img/about/ksgi-brochure 2022-23.pdf" target="_blank">Download Brochure</a></li> -->
								<li><a href="img/about/ksit-nisp-policy.pdf" target="_blank">NISP Policy</a></li>
			  					<!--<li>
			  					<#if homePageTimeTable?has_content>
								<a href="${homePageTimeTable.imageURL!}" target="_blank">Time Table</a>								 	
								</#if>
								</li> -->
								<li><a href="newsletter.html">NewsLetter</a></li>
			  					<li><a href="souvenir.html">Souvenir</a></li>
			  					<li><a href="${img_path!}/IICRating.pdf" target="_blank">IIC Rating</a></li>
			  				<li><a href="${img_path!}/naac/7.2/bestpractices_12_24.pdf" target="_blank">Best Practices</a></li>
							<!--	<li>
			  					<#if homePageCircular?has_content>
								<a href="${homePageCircular.imageURL!}" target="_blank">Circular</a>								 	 
								</#if> -->
			  					</li>
								<li><a href="${img_path!}/naac/ARI-C-1283-certificate.pdf" target="_blank">ARIIA 2021</a></li>
								<li><a href="${img_path!}/banner/NIRF_Engineering_Innovation_2025.pdf" target="_blank">NIRF</a></li>
								<#--  <li><a href="https://easypay.axisbank.co.in/easyPay/makePayment?mid=MjYyODM%3D" target="_blank">Online Fee Payment</a></li>  -->
			  				<!--	<li><a href="coursesOffered.html">Admission Enquiry</a></li> -->
			  				</ul>
			  			</div>
			  		<!-- quick links slider-->
			  		
				 	<div id="myCarousel" class="carousel slide">
						<div class="carousel-inner">
							<#if homePageSliderImageList?has_content>
	                     		<#list homePageSliderImageList as homePageSliderImage>
	                     			<#if homePageSliderImage?index  == 0>
	                     				<article class="item active">
	                     			<#else>
	                     				<article class="item">
	                     			</#if>
		                           		<img class="animated slideInLeft" src="${homePageSliderImage.imageURL}" alt="">
										<div class="carousel-caption" style="background:none;">
											<p class="slideInLeft">${homePageSliderImage.heading}</p>
											<p class="slideInRight">${homePageSliderImage.content}</p>
										</div>                           
		                        	</article>
	                     		</#list>
	                     	<#else>
								<article class="item active">									
									<img class="animated slideInLeft" src="${img_path!}/home/s1.jpg" alt="">
										<div class="carousel-caption" style="background:none;">
											<p class="slideInLeft">Accredited by NBA , NAAC &amp; IEI</p>
											<p class="slideInRight">Approved by AICTE, New Delhi</p>
										</div>
								</article>				
											
								<article class="item">						
									<img class="animated bounceIn" src="${img_path!}/home/s2.jpg" alt="">										
										<div class="carousel-caption" style="background:none;">									
											<p class="slideInLeft">Come! Join us for the bright future.</p>
										    <p class="slideInRight">Together we make future bright!</p>
										</div>										
								</article>
								
								<article class="item">									
									<img class="animated rollIn" src="${img_path!}/home/s3.jpg" alt="">
										<div class="carousel-caption" style="background:none;">
											<p class="slideInLeft">Ranked as one of the</p>
											<p class="slideInRight">Premier Institute in ARIIA</p>
										</div>
								</article>	
								<article class="item">						
									<img class="animated bounceInLeft" src="${img_path!}/home/s4.jpg" alt="">										
										<div class="carousel-caption" style="background:none;">									
											<p class="slideInLeft">Come! Join us for the bright future.</p>
										    <p class="slideInRight">Together we make future bright!</p>
										</div>										
								</article>
								
								<article class="item">									
									<img class="animated rollIn" src="${img_path!}/home/s5.jpg" alt="">
										<div class="carousel-caption" style="background:none;">
											<p class="slideInLeft">Accredited by NBA , NAAC &amp; IEI</p>
											<p class="slideInRight">Approved by AICTE, New Delhi</p>
										</div>
								</article>	
								
								<article class="item">						
									<img class="animated bounceInRight" src="${img_path!}/home/s6.jpg" alt="">										
										<div class="carousel-caption" style="background:none;">									
											<p class="slideInLeft">Come! Join us for the bright future.</p>
										    <p class="slideInRight">Together we make future bright!</p>
										</div>										
								</article>
								
								<article class="item">									
									<img class="animated rollIn" src="${img_path!}/home/s7.jpg" alt="">
										<div class="carousel-caption" style="background:none;">
											<p class="slideInLeft">Accredited by NBA , NAAC &amp; IEI</p>
											<p class="slideInRight">Approved by AICTE, New Delhi</p>
										</div>
								</article>	
								
								<article class="item">						
									<img class="animated bounceInLeft" src="${img_path!}/home/s8.jpg" alt="">										
										<div class="carousel-caption" style="background:none;">									
											<p class="slideInLeft">Come! Join us for the bright future.</p>
										    <p class="slideInRight">Together we make future bright!</p>
										</div>										
								</article>
								<article class="item ">									
									 <img class="animated slideInLeft" src="${img_path!}/slider/Slide1.jpg" alt="img">
								</article>
								<article class="item ">									
									 <img class="animated slideInLeft" src="${img_path!}/slider/Slide2.jpg" alt="img">
								</article>	
								<article class="item ">									
									 <img class="animated slideInLeft" src="${img_path!}/slider/Slide3.jpg" alt="img">
								</article>	
								<article class="item ">									
									 <img class="animated slideInLeft" src="${img_path!}/slider/Slide4.jpg" alt="img">
								</article>	
								<article class="item ">									
									 <img class="animated slideInLeft" src="${img_path!}/slider/Slide5.jpg" alt="img">
								</article>	
								<article class="item ">									
									 <img class="animated slideInLeft" src="${img_path!}/slider/Slide6.jpg" alt="img">
								</article>	
								<article class="item ">									
									 <img class="animated slideInLeft" src="${img_path!}/slider/Slide7.jpg" alt="img">
								</article>
							</#if>
				
						</div>
								<!-- Indicators -->
									<ol class="carousel-indicators">
											<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
											<li data-target="#myCarousel" data-slide-to="1"></li>
											<li data-target="#myCarousel" data-slide-to="2"></li>
											<li data-target="#myCarousel" data-slide-to="3"></li>
											<li data-target="#myCarousel" data-slide-to="4"></li>
											<li data-target="#myCarousel" data-slide-to="5"></li>
											<li data-target="#myCarousel" data-slide-to="6"></li>
											<li data-target="#myCarousel" data-slide-to="7"></li>
											<li data-target="#myCarousel" data-slide-to="8"></li>
											<li data-target="#myCarousel" data-slide-to="9"></li>
											<li data-target="#myCarousel" data-slide-to="10"></li>
											<li data-target="#myCarousel" data-slide-to="11"></li>
											<li data-target="#myCarousel" data-slide-to="12"></li>
											<li data-target="#myCarousel" data-slide-to="13"></li>
											<li data-target="#myCarousel" data-slide-to="14"></li>
											<li data-target="#myCarousel" data-slide-to="15"></li>
											<li data-target="#myCarousel" data-slide-to="16"></li>
											<li data-target="#myCarousel" data-slide-to="17"></li>
											<li data-target="#myCarousel" data-slide-to="18"></li>
											<li data-target="#myCarousel" data-slide-to="19"></li>
											<li data-target="#myCarousel" data-slide-to="20"></li>
											<li data-target="#myCarousel" data-slide-to="21"></li>
									</ol>
										<!-- Controls -->
									<div class="carousel-controls">
											<a class="carousel-control left" href="#myCarousel" data-slide="prev">
											<!--<span class="glyphicon glyphicon-backward"></span>-->
											</a>
											<a class="carousel-control right" href="#myCarousel" data-slide="next">
											<!--<span class="glyphicon glyphicon-forward"></span>-->
											</a>
								</div>
							</div>
		      </div>
            </div>
			<!--</marquee>-->
		</div>
		</div>
        <!-- End Our courses content -->
      </div>
    </section>
    <!--=========== Slider End ================--> 
    
    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about">
	<div class="container" >
	<div class="news_area home-4">
		<div class="row">
			<div class="col-md-12">
			<h1 class="titile text-center">Welcome to K.S. Institute Of Technology</h1>
			<p class="welcome-text text-center color-blue"><b>Imparting quality technical education with ethical values, employable skills and research to achieve excellence.</b></p>
				<div class=" col-lg-7 col-md-6 col-sm-12">
					<div class="aboutus_area wow fadeInLeft">
						<p>The Kammavari Sangham, a multi-activity non-profit oriented voluntary service organization, was established in the year 1952 with the sole objective of providing charitable service to community and society. </p>
						<p>The Sangham has diversified its activities since its establishment over five decades ago. With a firm belief that quality and meaningful education only can lay the strong foundation for bringing about economic and social changes to the lives of thousand, the Sangham went about establishing educational institutions, starting with <a href="https://ksgi.edu.in/best-diploma-college-ksp" class="font-weight-600" target="_blank">K.S. Polytechnic</a> in 1992.</p>				
						<p>Enthused with this success of its foray into technical education, the Sangham moved forward by starting the <a href="https://ksgi.edu.in/best-engineering-college-ksit" class="font-weight-600" target="_blank">K.S Institute of Technology (KSIT) </a> Its Engineering College in the year 1999. In the years that followed, KSIT quickly earned an outstanding reputation for academic excellence. The Sangham's collective group of institutions, known as <a href="https://ksgi.edu.in/" class="font-weight-600" target="_blank"> Kammavari Sangham Group of Institutions (KSGI)</a>, embodies its commitment to comprehensive education.</p>
						
					</div>
				</div>

				<div class="col-lg-5 col-md-6 col-sm-12">         	      
            		<img class="home-img img-responsive" src="${img_path!}/about/25_years_logo.jpg" alt="image" />	
				</div>
				<div class=" col-lg-12 col-md-12 col-sm-12">
          			<div class="aboutus_area wow fadeInLeft">
						
						
						<p>The commitment to fostering education continued with the inception of <a href="https://ksgi.edu.in/best-engineering-college-kssem" class="font-weight-600" target="_blank">K.S. School of Engineering and Management (KSSEM) </a> in 2010, further solidifying the Sangham's dedication to providing advanced technical and management education. Additionally, the <a href="https://ksgi.edu.in/best-college-of-architecture-kssa" class="font-weight-600" target="_blank">K.S. School of Architecture (KSSA) </a>  was established in 2015, offering specialized education in architectural studies.</p>
						<p>To cater to pre-university students, the Sangham founded <a href="https://ksgi.edu.in/best-pu-college-kspuc" class="font-weight-600" target="_blank">K.S. Pre-University College</a>, which has been instrumental in preparing students for higher education and future careers.</p>
						<p>All of these institutions have established themselves as centers of learning and have contributed significantly to the community by offering scholarships and free hostel accommodations to deserving students, further underscoring the Sangham's unwavering commitment to education and social upliftment.</p>
					</div>
				</div>
			
        </div>
		</div>
		</div>
	</section>
    <!--=========== END ABOUT US SECTION ================--> 

	<!--=========== Start Vision Mission Section ================-->
	
	<section class="quick_facts_news bg-light">
	<div class="container " >
	<div class="news_area home-4">
				
			<div class="row">
				<div class="col-md-12">
					<div class="row">
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						
							<h2 class="outline text-blue">Quality Policy</h2>
								<!-- marquee start-->
									<ul class="list">
										<li>We, at KSIT, are committed to pursue global standards of excellence in teaching as well as research & remain accountable to our stakeholders</li>
									</ul>	
							
								<h2 class="outline text-blue"> Vision</h2>
								<!-- marquee start-->
									<ul class="list">
										<li>To impart quality technical education with ethical values, employable skills and research to achieve excellence.</li>
									</ul>

								<h2 class="outline  text-blue"></i> Mission</h2>
								<ul class="list">
									<li>To attract and retain highly qualified, experienced and committed faculty.</li>
									<li>To create relevant infrastructure</li>
									<li>Network with industry and premier institutions to encourage emergence of new ideas by providing research and
									development facilities to strive for academic excellence</li>
									<li>To inculcate the professional and ethical values among young students with employable skills and knowledge 
									acquired to transform the society</li>
								</ul>
								<!-- marquee end-->
						
											
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						
							
							
								<h2 class="outline  text-blue"> Objectives</h2>
								<!-- marquee start-->
									<ul class="list">
										<li>To provide quality education by employing highly qualified, experienced and committed faculties.</li>
										<li>To keep all the infrastructure updated and relevant at any given point of time.</li>
										<li>To provide practical exposes through industry connect and research.</li>
										<li>To provide holistic education be exploring the students through ethical values and provide employable skills to take-up responsibilities for a better society.</li>
									</ul>

								<h2 class="outline  text-blue"></i> Core Values</h2>
								<ul class="list">
									<li>Quality Education</li>
									<li>Honesty and Integrity</li>
									<li>Research Work and self Improvement</li>
									<li>Leadership and Entrepreneurship</li>
									<li>Social Service</li>
								</ul>
								<!-- marquee end-->
						
											
					</div>
					<!-- end single news-->
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
	
<!--=========== End Vision Mission Section ================-->

	<!--=========== START Programs SECTION ================-->
	<section id="quick_facts_life">
	
            <div class="title_area">
              <h2 class="titile text-center yellow" title="KSIT®">Programmes At KSIT</h2>
			  
            </div>
			
         
	<div class="container-program">
	
    
    <div class="program-content-wrapper">
      <!-- Grid Section -->
      <div class="program-grid">
        <div class="program-grid-item" onclick="updateDetails('Artificial Intelligence & Machine Learning', 'img/about/ksit_aiml.jpg', 'At AI & ML, we blend hands-on learning with transformative research to shape future-ready minds.','aiml_dept.html')">
          <p>Department of Artificial Intelligence & Machine Learning</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('Computer Science & Design', 'img/about/ksit_csd.jpg', 'At CSD, we foster practical skills and innovative thinking to drive technological excellence.','csd_dept.html')">
          <p>Department of Computer Science & Design</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('Computer & Communication Engineering', 'img/about/ksit_cce.jpg', 'At CCE, we empower creative problem-solvers with skills in computing and communication technologies.','cce_dept.html')">
          <p>Department of Computer & Communication Engineering</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('Computer Science & Engineering', 'img/about/ksit_cse.jpg', 'At CSE, we nurture technical expertise and innovation to build future tech leaders.','cse_dept.html')">
          <p>Department of Computer Science & Engineering</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('Computer Science & Engineering (IOT & Cyber Security including Block chain Technology)', 'img/about/ksit_cseicb.jpg', 'At CSE ICB, we specialize in IoT, Cybersecurity, and Blockchain technologies to shape the future of digital innovation.','csicb_dept.html')">
          <p>Department of Computer Science & Engineering (ICB)</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('Electronics & Communication Engineering', 'img/about/ksit_ece.jpg', 'At ECE, we blend electronics, communication, and innovation to shape the future of technology.','ece_dept.html')">
          <p>Department of Electronics & Communication Engineering</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('Mechanical Engineering', 'img/about/ksit_me.jpg', 'At Mechanical, we inspire innovation and problem-solving to drive advancements in engineering and technology.','mech_dept.html')">
          <p>Department of Mechanical Engineering</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('Science & Humanities', 'img/about/ksit_sh.jpg', 'At Science and Humanities, we cultivate critical thinking and a deep understanding of the world to shape well-rounded individuals.','science_dept.html')">
            <p>Department of Science & Humanities</p>
        </div>
        <div class="program-grid-item" onclick="updateDetails('M.Tech in CSE', 'img/about/ksit_mcse.jpg', 'At M.Tech in CSE, we foster advanced research, innovation, and technical expertise to develop the next generation of computer science leaders.','cse_dept.html')">
          <p>Master of Technology in CSE</p>
      </div>
      <div class="program-grid-item" onclick="updateDetails('Master of Computer Application', 'img/about/ksit_mca.jpg', 'At MCA, we empower students with advanced knowledge and skills in computer applications to excel in the digital world.','mca_dept.html')">
        <p>Master of Computer Application</p>
    </div>
     <div class="program-grid-item" onclick="updateDetails('Training & Placements', 'img/about/ksit_tpd.jpg', 'At Training and Placements, we bridge the gap between education and industry, preparing students for successful careers with hands-on training and placement support.','about_placement.html')">
        <p>Department of Training & Placements</p>
    </div>
    <div class="program-grid-item" onclick="updateDetails('K S Research & Innovation Foundation', 'img/about/ksit_rif.jpg', 'At K S Research and Innovation Foundation, we drive cutting-edge research and foster innovation to create solutions that shape the future.','about_ksrif.html')">
      <p>K S Research & Innovation Foundation (KSRIF)</p>
  </div>

   
        <!-- Add more grid items here -->
      </div>

      <!-- Details Section -->
      <div class="program-details-section">
        <div class="program-details-image">
            <img id="program-details-image" src="img/about/ksit_aiml.jpg" alt="AI & ML">
          </div>
        <div class="program-details-text">
          <h2 id="program-details-title">Artificial Intelligence & Machine Learning</h2>
          <p id="program-details-description"> At AI & ML, we blend hands-on learning with transformative research to shape future-ready minds.</p>
          
		 <button id="program-details-button" class="button">Read More</button>
        </div>
      </div>
    </div>
  </div>

	</section>
	
	<!--=========== END Programs SECTION ================-->

	

    <!--=========== Upcoming and Latest News Section ===========-->
	<!--start news  area -->	
	<section class="quick_facts_news bg-light">
	<div class="container " >
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea text-center">
							 Latest at KSIT
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-12">
					<div class="row">
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options' style="height:220px;">
										<@eventDisplayMacro eventList=homePageLatestEventsList/>
									</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								<!-- marquee start-->
								<div class='marquee-with-options' style="height:220px;">
								<@eventDisplayMacro eventList=homePageUpcomingEventsList/>
								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!--end news  area -->
<!-- upcoming and latest news list-->
<!-- testing comment -->

  
  <!--=========== BEGIN STUDENTS Life at KSIT SECTION ================-->
    <section id="quick_facts_life" class="margin-top0">

	<div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="titile text-center yellow" title="KSIT®">Life At KSIT</h2>			  
            </div>			
    </div>
    	
    	<div class="container">
    
      	<div class="row">
			<div class="col-md-6 pb-5 mt-5 pt-4 title_area">
				<h2 class="title_two pb-3 mb-3">Campus Life</h2>
				<p class="content_KSSEM text-justify text-white px-3 my-3 font-weight-lighter">
					Kammavari Sangham Institute of Technology enlightens the dream of each and every student and accomplishes it by grooming the students in a most professional
				  way. KSIT has hi-tech wifi enabled infrastructure, completely equipped with advanced lab facilities , 
				  seminar halls, a world class placement cell and in-house canteen facility. KSIT has a two story library filled with resources and
				   also has a digital library facility for enhancing knowledge of students. KSIT Advances in Sports and extra curricular activities in empoworing
				   the creative talents of students.  
				</p>
            </div>
            <div class="col-md-6 py-5">
            	<div>
					<ul id="KSSEMTabs" class="nav nav-pills d-flex flex-row" role="tablist">
					    <li role="presentation" class="active"><a class="rounded-0" href="#home" aria-controls="home" role="tab" data-toggle="tab">
					    	<img class="img-fluid" src="${img_path!}/campus1.png" alt="img" />
					    </a></li>
					    <li role="presentation"><a class="rounded-0" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
					    	<img class="img-fluid" src="${img_path!}/library1.png" alt="img" />
					    </a></li>
					    <li role="presentation"><a class="rounded-0" href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
					    	<img class="img-fluid" src="${img_path!}/sports1.png" alt="img" />
					    </a></li>
					  </ul>
					
					  <!-- Tab panes -->
					  <div class="tabcontent">
						  <div id="KSSEMTabsContent" class="tab-content text-white">
						    <div role="tabpanel" class="tab-pane active" id="home">
						    	<div class="pt-5 text-justify">
						    		<h5 class="tabtitle">Hostel</h5>
						    		<p class="px-5 tabpadding">
						    			Separate hostels are provided for boys and girls situated within walking distance from the college for convenience. 
						    			The hostels are prepared to be an extended home. 
						    			The hostel facilitates completely support students to carry out serious study and to grow physically, intellectually and psychology. 
						    			The college tradition and discipline is also extended to the hostels.
						    		</p>
						    	</div>
						    	<div class="text-right">
						    		<a href="hostel">
								    	<button class="btn btn-lg rounded-0">
								    		Read More
								    	</button>
							    	</a>
						    	</div>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="profile">
						    	<div class="pt-5 text-justify">
						    		<h5 class="tabtitle">Library</h5>
						    		<p class="px-5 tabpadding">
						    			Library occupied a place of Pride in K.S. Institute of Technology and is Essential component of the Institute outstanding educational activities. It is a resource centre for teaching, learning and research. Being the heart of the academic centre, it is home for all the information services.
						    		</p>
						    	</div>
						    	<div class="text-right">
						    		<a href="about_library">
								    	<button class="btn btn-lg rounded-0">
								    		<b>Read More</b>
								    	</button>
								    </a>
						    	</div>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="messages">
						    	<div class="pt-5 text-justify">
						    		<h5 class="tabtitle">Sports</h5>
						    		<p class="px-5 tabpadding">
							    		Sport includes all forms of competitive physical activity or games which, through casual
									    or organized participation, at least in part aim to use, maintain or improve physical ability and skills while
									     providing enjoyment to participants, and in some cases, entertainment for spectators.
						    		</p>
						    	</div>
						    	<div class="text-right">
						    		<a href="sports">
								    	<button class="btn btn-lg rounded-0">
								    		Read More
								    	</button>
								    </a>
						    	</div>
						    </div>
						</div>
					</div>
            	</div>
            </div>
		</div>
    </div>
        <!-- End Our courses titile -->
    </section>
    <!--=========== END STUDENTS life at KSIT SECTION ================-->

   

    <!--=========== BEGIN STUDENTS testimonial SECTION ================-->
    <section id="quick_facts_achievers">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
              <div class="row">

				 <div class="col-md-12" data-wow-delay="0.2s">
				 	<h2 class="titile text-center" title="KSIT®">ACHIEVERS AND ACHIEVEMENTS</h2>
					
				 		
				 		<!--achievers slider-->
				 			<div class="container">
							    <div class="row">
							        <div class="col-md-offset-1 col-md-10">
							            <div id="testimonial-slider" class="owl-carousel">
							            <#list achieversList as achievers>
							                <div class="testimonial">
							                    <div class="pic">
							                        <img src="${achievers.imageURL!}" alt="">
							                    </div>
							                    <p class="description">
													${achievers.content!}	
							                    </p>
							                    <h3 class="testimonial-title">
							                        ${achievers.heading!}
							                        <!--<small class="post">Web Developer</small>-->
							                    </h3>
							                </div>
							 			</#list> 
							            </div>
							        </div>
							    </div>
							</div>
				 		<!--achievers slider-->
                    </div>
		      </div>
            </div>
		<!--</marquee>-->
		</div>
		</div>
    <!-- End Our courses content -->
    </div>
    </section>
    <!-- achievers-->

	 <!--=========== BEGIN STUDENTS TESTIMONIAL SECTION ================-->	
   <!-- <section id="quick_facts_life" class="margin-top0">
      <div class="container"> -->
       <!-- Our courses titile -->
    <!--    <div class="row">
        
        <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="titile text-center yellow" title="KSIT®">Life At KSIT</h2>
			  <p class="content_ksit " >Kammavari Sangham Institute of Technology
				 enlightens the dream of each and every student and accomplishes it by grooming the students in a most professional
				  way. KSIT has hi-tech wifi enabled infrastructure, completely equipped with advanced lab facilities , 
				  seminar halls, a world class placement cell and in-house canteen facility. KSIT has a two story library filled with resources and
				   also has a digital library facility for enhancing knowledge of students. KSIT Advances in Sports and extra curricular activities in empoworing
				   the creative talents of students. </p>
            </div>
			
         </div>
        
         <div class="col-lg-6 col-md-6 col-sm-12"> 
            <div class="title_area margin-top0">
            
				
				  
				  	<h2 class="titile yellow">VISION</h2>
					<ul class="list">
					<li>To impart quality technical education with ethical values, employable skills and research to achieve excellence.</li>
					</ul>

					<h2 class="titile yellow">MISSION</h2>
					<ul class="list">
					<li>To attract and retain highly qualified, experienced and committed faculty.</li>
					<li>To create relevant infrastructure</li>
					<li>Network with industry and premier institutions to encourage emergence of new ideas by providing research and
					 development facilities to strive for academic excellence</li>
					<li>To inculcate the professional and ethical values among young students with employable skills and knowledge 
					acquired to transform the society</li>
					</ul>
					

					<h2 class="titile yellow">OBJECTIVES</h2>
					<ul class="list">
					<li>To provide quality education by employing highly qualified, experienced and committed faculties.</li>
					<li>To keep all the infrastructure updated and relevant at any given point of time.</li>
					<li>To provide practical exposes through industry connect and research.</li>
					<li>To provide holistic education be exploring the students through ethical values and provide employable skills to take-up responsibilities for a better society.</li>
					</ul>

					<h2 class="titile yellow">CORE VALUES</h2>
					<ul class="list">
					<li>Quality Education</li>
					<li>Honesty and Integrity</li>
					<li>Research Work and self Improvement</li>
					<li>Leadership and Entrepreneurship</li>
					<li>Social Service</li>
					</ul>
            </div>
          </div>
        
        <div class="col-lg-6 col-md-6 col-sm-12"> 
        	 <div class="youtube_content"> -->
			<!-- YouTube Link -->      
            <!--	<iframe width="100%" height="287" src="${youtubeLink!}" 
            	frameborder="1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
            	allowfullscreen>
				</iframe>
				
				<iframe src="https://www.easytourz.com/BT-EmabedTour/all/06dbb5b6c54213e1" width="100%"
			  		 height="287" frameborder="1" style="border:1;margin-bottom:10px;"
			    	 webkitAllowFullScreen mozallowfullscreen allowFullScreen >Your browser does not support iframes.
				</iframe>
            </div>

			<div class="quick_facts_content"  style="padding-top:40px;">
              <div class="row">-->
                <!-- start single student testimonial -->
            <!--    <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">               
                      <div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/campus1.png" alt="img" />
						<a href="hostel.html"><img class="msgbox_img2" src="${img_path!}/campus2.png" alt="img" /></a>
					  </div>
                    </div>
					<p><a class="yellow" href="hostel.html">Hostel</a></p>
                  </div>
                </div> -->
                <!-- End single student testimonial -->
				
                <!-- start single student testimonial -->
            <!--    <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">
					  <div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/library1.png" alt="img" />
						<a href="about_library.html"><img class="msgbox_img2" src="${img_path!}/library2.png" alt="img" /></a>
					  </div>
                    </div>
					<p><a class="yellow" href="about_library.html">Library</a></p>
                  </div>
                </div> -->
				<!-- End single student testimonial -->
				
				<!-- start single student testimonial -->
            <!--    <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">
						<div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/sports1.png" alt="img" />
						<a href="sports.html"><img class="msgbox_img2" src="${img_path!}/sports2.png" alt="img" /></a>
					  </div>
                    </div>
					<p><a class="yellow" href="sports.html">Sports</a></p>
                  </div>
                </div> -->
				<!-- End single student testimonial -->
				
				 <!-- start single student testimonial -->
              <!--  <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">
                       <div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/ananya1.png" alt="img" />
						<a href="ananya.html"><img class="msgbox_img2" src="${img_path!}/ananya2.png" alt="img" /></a>
	
					</div>
                    </div>
					<p><a class="yellow" href="ananya.html">Ananya</a></p>
                  </div>
                </div> -->
				<!-- End single student testimonial -->
           <!--   </div>
            </div>
          </div>
        </div> -->
        <!-- End Our courses content -->
		<!--<div class="col-lg-12 col-md-12 col-sm-12"> 
            <div class="title_area margin-top0">					  
				  	<h2 class="titile yellow">Quality Policy</h2>
					<ul class="list">
					<li>We, at KSIT, are committed to pursue global standards of excellence in teaching as well as research & remain accountable to our stakeholders</li>
					</ul>				
            </div>
          </div>

        </div> 
        </div> -->

		

		
        <!-- End Our courses titile -->
        <!-- Start Our courses content -->
       <!-- <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="quick_facts_content"  style="padding-top:40px;">
              <div class="row"> -->

                <!-- start single student testimonial -->

            <!--    <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">               
                      <div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/campus1.png" alt="img" />
						<a href="hostel.html"><img class="msgbox_img2" src="${img_path!}/campus2.png" alt="img" /></a>
					  </div>
                    </div>
					<p><a class="yellow" href="hostel.html">Hostel</a></p>
                  </div>
                </div> -->

                <!-- End single student testimonial -->
				
                <!-- start single student testimonial -->

            <!--    <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">
					  <div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/library1.png" alt="img" />
						<a href="about_library.html"><img class="msgbox_img2" src="${img_path!}/library2.png" alt="img" /></a>
					  </div>
                    </div>
					<p><a class="yellow" href="about_library.html">Library</a></p>
                  </div>
                </div> -->

				<!-- End single student testimonial -->
				
				<!-- start single student testimonial -->

            <!--    <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">
						<div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/sports1.png" alt="img" />
						<a href="sports.html"><img class="msgbox_img2" src="${img_path!}/sports2.png" alt="img" /></a>
					  </div>
                    </div>
					<p><a class="yellow" href="sports.html">Sports</a></p>
                  </div>
                </div> -->

				<!-- End single student testimonial -->
				
				 <!-- start single student testimonial -->

               <!-- <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_quick_facts wow fadeInUp">
                    <div class="quick_facts_content">
                       <div class="quick_facts_msgbox">
						<img class="msgbox_img1" src="${img_path!}/ananya1.png" alt="img" />
						<a href="ananya.html"><img class="msgbox_img2" src="${img_path!}/ananya2.png" alt="img" /></a>
	
					</div>
                    </div>
					<p><a class="yellow" href="ananya.html">Ananya</a></p>
                  </div>
                </div> -->

				<!-- End single student testimonial -->
          <!--    </div>
            </div>
          </div>
        </div> -->
        <!-- End Our courses content -->
		 
   <!--   </div>

	  
    </section> -->
    <!--=========== END STUDENTS life at ksit SECTION ================-->



<!--=========== Start Explore KSIT  SECTION ================-->
		<section id="quick_facts_life" class="margin-top0">
		<div class="container">
       <!-- Our courses titile -->
	   <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="titile text-center yellow" title="KSIT®">Explore KSIT</h2>
			  
            </div>
			
         </div>
       
        <div class="row">
         <div class="col-md-6 col-sm-12 pl-0 title_area">
			<div class="youtube_content">
				
				
	            	<iframe width="100%" height="385" src="${youtubeLink!}" 
	            	frameborder="1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
	            	allowfullscreen></iframe>
	            	<h2 class="title_two"><span class="material-icons" id="span-icon">video_library</span>Campus Video</h2>
            	</div>
         </div>
         <div class="col-md-6 col-sm-12 pl-0 title_area">
			<div class="virtual_tour_content">              
             	
			  
			  <iframe src="https://www.easytourz.com/BT-EmabedTour/all/06dbb5b6c54213e1" width="100%"
			  		 height="385" frameborder="0" style="border:0;margin-bottom:10px;"
			    	 webkitAllowFullScreen mozallowfullscreen allowFullScreen ><span class="material-icons">error</span>Your browser does not support iframes.</iframe>
			    	 <h2 class="title_two"><span class="material-icons" id="span-icon">view_in_ar</span>Virtual Tour</h2>
              	</div>
          	</div>  
        </div>
	</section>
	
    <!--=========== BEGIN OUR COURSES SECTION ================-->

<!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_block" class="block_bgImg" style="background:url('${img_path!}/ananya/ananyabg.jpg');">
      <div class="container">
        <!-- Start Our courses content -->
        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
			<!--<marquee>-->
            <div class="quick_facts_content">  
              <div class="row">
				 <div class="col-md-12" data-wow-delay="0.2s">
				 
				 	<h1 class="center yellow">ANANYA</h1>
				 	<p class="center">With every year it's a new team, a new family, but ever strong roots..<br/>
				 		A pride within itself,<br/>
				 		An ever fascinating stage of talents here at KSIT!
				 	</p>
				 	<p class="button"><a href="ananya.html">Read more</a></p>

				 </div>
				</div>
			</div>
			</div>
			</div>
		</div>

	</section>

	<!--content SECTION-->

<!--=========== BEGIN OUR COURSES SECTION ================-->
    <!--<section id="ourCourses">
      <div class="container">-->
       <!-- Our courses titile -->
       <!-- <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Message from honourables</h2>
              <span></span> 
            </div>
          </div>
        </div>-->
        <!-- End Our courses titile -->
        <!-- Start Our courses content -->
        <!--<div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="ourCourse_content">
              <ul class="course_nav">
                <li>
                  <div class="single_course">
                    <div class="singCourse_imgarea">
                      <img src="${img_path!}/about/RamachandraNaidu(President).jpg" />
                      <div class="mask">     
							<p>To provide competent and responsible professionals in the field of Computer Science and Engineering with knowledge and skills required for the country in its quest for development.</p>
                      </div>
                    </div> 
                  </div>
                </li>-->

                <!--<li>
                  <div class="single_course">
                    <div class="singCourse_imgarea">
                      <img src="${img_path!}/about/VenkateshNaidu(Secretary).jpg" />
                      <div class="mask"> 
					  <p>Knowledge for Excellence is the vision of the department, who work with a motive to prepare highly committed and dedicated professionals ingrained in creativity and adaptability</p>-->
                    <!--  </div>
                    </div>
                  </div>
                </li>--> 
               	<!-- <li>
                  <div class="single_course">
                    <div class="singCourse_imgarea">
                      <img src="${img_path!}/about/Rukmangada(Treasrer).jpg" />
                      <div class="mask">
						<p> To groom the incumbent to be able to compete with the best in the Mechanical Engineering profession and to get recognized by peers as one of the best learning centers.</p><br/>-->
                        <!--<a href="#" class="course_more">View Course</a>-->
                      <!--</div>
                    </div>
                  </div>
                </li>          
              </ul>
            </div>
          </div>
        </div>-->
    <!-- End Our courses content -->
    <!-- </div>
    </section>-->
<!--=========== END OUR COURSES SECTION ================-->  
<!--=========== BEGIN virtual tour SECTION ================-->
 <!--   <section id="virtual_tour">
      <div class="container">
        
       	<div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="virtual_tour_content">              
             	<h2 class="title_two" title="360° View"><span class="fa fa-cube"></span> Virtual Tour</h2>
			
			  	<iframe src="https://www.easytourz.com/BT-EmabedTour/all/06dbb5b6c54213e1" width="100%"
			  		 height="575" frameborder="0" style="border:0;margin-bottom:10px;"
			    	 webkitAllowFullScreen mozallowfullscreen allowFullScreen >Your browser does not support iframes.
				</iframe>
            </div>
          </div>
        </div>
    </div>
        
    </div>
    </section>  -->
    <!--=========== END  virtual tour SECTION ================-->   
    
    
    <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_block" class="block_bgImg" style="background:url('${img_path!}/nss/nss_bg.jpg');">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
            <div class="quick_facts_content">  
              <div class="row">
				 <div class="col-md-12" data-wow-delay="0.2s">
				 
				 	<h1 class="center yellow">NSS</h1>
				 	<p class="center">At KSIT NSS was officially launched in the year 2017 aimed to develop students personality through social service. 
					VTU sanctioned one unit (100 students) of NSS to our college in the year 2018.</p>
				 	<p  class="button"><a href="nss.html">Read more</a></p>
				 	
				 </div>
				</div>

			</div>
			</div>
			</div>
		</div>

		</section>
	<!--content SECTION-->        
    <!-- youtube link-->      
   
    <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_block" class="block_bgImg" style="background:url('${img_path!}/nss/redcross_bg.jpg');">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
				<div class="col-md-12" data-wow-delay="0.2s">
				 
				 	<h1 class="center yellow">YOUTH RED CROSS (YRC) : KSIT</h1>
				 	<p class="center">YRC unit of KSIT in association with Indian red cross, Lions club, Rotery Bangalore, is regularly conducting Blood donation camps in every academic year. 
				 	</p>
				 	<p  class="button"><a href="redcross.html">Read more</a></p>

				</div>
				</div>

			</div>
			</div>
			</div>
		</div>
		</section>

	<!--content SECTION-->

<!--=========== BEGIN STUDENTS placement SECTION ================-->
    <section id="quick_facts_placement">
      <div class="container">
       <!-- Our courses titile -->
        <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
			
              <a class="header_link" href="about_placement.html" title="100% Placements"><h2 class="titile text-center">Placements At KSIT</h2></a>
				<p class="text-center text-muted">Placement is an integral part of K. S. Institutions of Technology.</p>
            </div>
          </div>
        </div>
        <!-- End Our courses titile -->

        <!-- Start Our courses content -->
		
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				<div class="c-carousel">
					<div class="c-carousel__arrow c-carousel__arrow--left">&nbsp;&nbsp;<i class="fa fa-angle-left" style="font-size:36px"></i></div>
							<div class="c-carousel__arrow c-carousel__arrow--right">&nbsp;&nbsp;<i class="fa fa-angle-right" style="font-size:36px"></i></div>
								<ul class="c-carousel__slides">
									<li><img src="${img_path!}/placements/2.png" /></li>
									<li><img src="${img_path!}/placements/3.png" /></li>
									<li><img src="${img_path!}/placements/4.png" /></li>
									<li><img src="${img_path!}/placements/6.png" /></li>
									<li><img src="${img_path!}/placements/10.png" /></li>
									<li><img src="${img_path!}/placements/11.png" /></li>
									<li><img src="${img_path!}/placements/13.png" /></li>
									<li><img src="${img_path!}/placements/14.png" /></li>
									<li><img src="${img_path!}/placements/15.png" /></li>
									<li><img src="${img_path!}/placements/16.png" /></li>
									<li><img src="${img_path!}/placements/17.png" /></li>
									<li><img src="${img_path!}/placements/18.png" /></li>
									<li><img src="${img_path!}/placements/20m.png" /></li>
									<li><img src="${img_path!}/placements/21.png" /></li>
									<li><img src="${img_path!}/placements/22.png" /></li>
									<li><img src="${img_path!}/placements/23.png" /></li>
									<li><img src="${img_path!}/placements/24.png" /></li>
									<li><img src="${img_path!}/placements/25.png" /></li>
								</ul>
							</div>
		        </div>
            </div>
			<!--</marquee>-->
		</div>
		</div>	
    <!-- End Our courses content -->
      </div>
	  
    </section>
    <!--=========== END STUDENTS placement SECTION ================--> 
    
    
    <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_block" class="block_bgImg" style="background:url('${img_path!}/Slide33.JPG');">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">
				 
				 	<h1 class="center yellow">KSIT CAMPUS</h1>
				 	<p class="center">K S Institute of technology has a best in class high-fi infrastructure completely surrounded by CCTV cameras. Seminar halls, lab facilities and placement cells are equipped with world class amenities. Campus is completely wifi enabled and easily accessible by each and every students for the betterment of their studies.</p>
				 	<p  class="button"><!--<a href="campus.html">read more</a>--></p>
				 	
				 </div> 
				</div>
			</div>
				</div>
			</div>
		</div>

		</section>

	<!--content SECTION-->
	
	<!--=========== BEGIN STUDENTS testimonial SECTION ================-->
    <section id="quick_facts_testimonials">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">
				 
				 		<h2 class="titile text-center yellow" title="KSIT®">Testimonials</h2>
				 		
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <!-- Bottom Carousel Indicators -->
                            <ol class="carousel-indicators">
                            
                            	<#list testimonialList as testimonial>
                            		<li data-target="#quote-carousel" data-slide-to="${testimonial?index}"  <#if testimonial?is_first>class="active"</#if>><img class="img-responsive " src="${testimonial.imageURL!}" alt="">
                                	</li>
                            	</#list>
                            </ol>

                            <!-- Carousel Slides / Quotes -->
                            <div class="carousel-inner text-center">

								<#list testimonialList as testimonial>
	                                <!-- Quote 1 -->
	                                <div class="item <#if testimonial?is_first>active</#if>">
	                                    <blockquote>
	                                        <div class="row">
	                                            <div class="col-sm-8 col-sm-offset-2">
	                                                <p>${testimonial.content!}</p>
	                                                <small>${testimonial.heading!}</small>
	                                            </div>
	                                        </div>
	                                    </blockquote>
	                                </div>
                                </#list>
                            </div>

                            <!-- Carousel Buttons Next/Prev -->
                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
		      </div>
            </div>
			<!--</marquee>-->
		</div>
		</div>
        <!-- End Our courses content -->
    </div> 




    <!-- message box modal-->
      
    <!-- message box -->

        <#if messageBox?has_content>
	        <div id="myModal" class="modal fade" role="dialog" data-backdrop="static">
			  <div class="modal-dialog">
			
			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">${messageBox.heading!}</h4>
			      </div>
			      <div class="modal-body-1">
			        <a href="${messageBox.content!}" target="_blank"><img src="${messageBox.imageURL!}" alt="img" class="message_box_img"/></a>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div>
		 </#if>
        <!-- message box -->
    </section>
    <!--=========== END STUDENTS testimonial SECTION ================--> 

	<!--=========== START Affiliations and Accreditations SECTION ================-->

	
	
	<!--=========== END Affiliations and Accreditations SECTION ================-->
</@page>
