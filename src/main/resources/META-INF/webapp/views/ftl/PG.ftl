<@page>
<br/>
<div class="container" style="background-color:white;min-height:600px;margin-top:35px;">
	<table class="table table-striped table-bordered">
		<tr>
			<th>Department</th>
			<th>Syllabus & Time Table</th>
		</tr>
		<#list programmeList as programme>
			<tr>
				<td>
					${programme.department!}
				</td>
				<td>
					<#list programmeDataTypes as programmeDataType>
						${programmeDataType.desc}
						<br />
						<ul>
						<#list programme.programmeDataSet as programmeData>
							<#if programmeData.programmeDataType == programmeDataType>
								<span>
									<a href="${programmeData.data}">${programmeData.name}</a>
								</span>
							</#if>
						</#list>
						</ul>
					</#list>
				</td>
			</tr>
		</#list>
	</table>
</div>
</@page>
