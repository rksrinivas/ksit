<div class="modal fade" id="feedbackAlumniModal" role="dialog" style="display: none;">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Feedback</h4>
        </div>
        <form action="/feedback/alumni" method="post">
        	<div class="modal-body" style="max-height: calc(100vh - 175px); overflow-y: auto;"> 
        		<input type="hidden" name="page" id="feedbackAlumniPage" />
        		<div class="form-group">
          			<label>Student Name :</label>
          			<input class="form-control" type="text" name="studentName">
          		</div>
          		<div class="form-group">
          			<label>Year Of Graduation:</label> 
          			<input class="form-control" type="text" name="yearOfGraduation">
          		</div>
          		<div class="form-group">
          			<label>Position in Graduation:</label> 
          			<input class="form-control" type="text" name="positionInGraduation">
          		</div>
          		<div class="form-group">
          			<label>USN:</label> 
          			<input class="form-control" type="text" name="usn">
          		</div>
          		<div class="form-group">
          			<label>Email Id:</label> 
          			<input class="form-control" type="text" name="emailId">
          		</div>
          		<div class="form-group">
          			<label>Address:</label> 
          			<input class="form-control" type="text" name="address">
          		</div>
          		<div class="form-group">
          			<label>Mobile Number:</label> 
          			<input class="form-control" type="text" name="mobileNumber">
          		</div>
          		<div class="form-group">
          			<label>Current Organization:</label> 
          			<input class="form-control" type="text" name="currentOrganisation">
          		</div>
          		<div class="form-group">
          			<label>Designation:</label> 
          			<input class="form-control" type="text" name="designation">
          		</div>
        		<div class="form-group">
					<label>Whether the <span name="departmentName"></span> Department is moving towards right path to achieve its Vision &amp; Mission?</label>
	        		<div class="form-check form-check-inline">
		          		<input class="form-check-input" type="radio" name="visionMission" value="Yes" checked>
		          		<label class="form-check-label" for="visionMissionYes">Yes</label>
		          	</div>
		          	<div class="form-check form-check-inline">
		          		<input class="form-check-input" type="radio" name="visionMission" value="No"/>
		          		<label class="form-check-label" for="visionMissionNo">No</label>
	          		</div>
	          	</div>
          		<div class="form-group">
          			<label>Do you agree our Vision statement captures where we should be heading as a Department?</label>
	          		<div class="form-check form-check-inline">
		          		<input class="form-check-input" type="radio" name="visionAgree" value="agree" checked>
		          		<label class="form-check-label" for="visionAgree">Agree</label>
		          	</div>
		          	<div class="form-check form-check-inline">
		          		<input class="form-check-input" type="radio" name="visionAgree" value="disagree"/>
		          		<label class="form-check-label" for="visionDisagree">Disagree</label>
	          		</div>
          		</div>
          		<div class="form-group">
          			<label>Do you agree that our Mission statements will enable us to achieve our Vision?</label>
	          		<div class="form-check form-check-inline">
		          		<input class="form-check-input" type="radio" name="missionAgree" value="agree" checked>
		          		<label class="form-check-label" for="missionAgree">Agree</label>
		          	</div>
		          	<div class="form-check form-check-inline">
		          		<input class="form-check-input" type="radio" name="missionAgree" value="disagree"/>
		          		<label class="form-check-label" for="missionDisagree">Disagree</label>
	          		</div>
          		</div>	
          		<div class="form-group">
          			<label>Suggestions about Vision</label>
          			<textarea class="form-control" name="visionSuggestion" cols="70" rows="2"></textarea>
          		</div>
          		<div class="form-group">
          			<label>Suggestions about Mission</label>
					<textarea class="form-control" name="missionSuggestion" cols="70" rows="2"></textarea>
				</div>
				<div class="form-group">
					<label>Suggestion About Program Educational Objectives (PEO)</label>
          			<textarea class="form-control" name="peoSuggestion" cols="70" rows="2"></textarea>
          		</div>
          		<div class="form-group">
	          		<label>Please select the appropriate answer for every statement.</label>
	          		<h3><b>Knowledge</b></h3>
	          		<#list knowledgeFeedbacks as knowledgeFeedback>
	          			<div class="form-group">
	          				<label>${knowledgeFeedback?index+1}. ${knowledgeFeedback}</label>
	          				<br />
	          				<select class="wp-form-control" name="knowledgeResponse${knowledgeFeedback?index+1}">
	          					<#list responses as response>
	          						<option value="${response}">${response}</option>
	          					</#list>
	          				</select>
	          			</div>
	          		</#list>
	          		<h3><b>Communication Skills</b></h3>
	          		<#list communicationFeedbacks as communicationFeedback>
	          			<div class="form-group">
	          				<label>${communicationFeedback?index+1}. ${communicationFeedback}</label>
	          				<br />
	          				<select class="wp-form-control" name="communicationResponse${communicationFeedback?index+1}">
	          					<#list responses as response>
	          						<option value="${response}">${response}</option>
	          					</#list>
	          				</select>
	          			</div>
	          		</#list>
	          		<h3><b>Interpersonal Skills</b></h3>
	          		<#list interPersonalFeedbacks as interPersonalFeedback>
	          			<div class="form-group">
	          				<label>${interPersonalFeedback?index+1}. ${interPersonalFeedback}</label>
	          				<br />
	          				<select class="wp-form-control" name="interPersonalResponse${interPersonalFeedback?index+1}">
	          					<#list responses as response>
	          						<option value="${response}">${response}</option>
	          					</#list>
	          				</select>
	          			</div>
	          		</#list>
	          		<h3><b>Management / leadership Skills</b></h3>
	          		<#list managementFeedbacks as managementFeedback>
	          			<div class="form-group">
	          				<label>${managementFeedback?index+1}. ${managementFeedback}</label>
	          				<br />
	          				<select class="wp-form-control" name="managementResponse${managementFeedback?index+1}">
	          					<#list responses as response>
	          						<option value="${response}">${response}</option>
	          					</#list>
	          				</select>
	          			</div>
	          		</#list>
	          	</div>
          		<div class="form-group">
          			<label>Any other suggestions/comments:</label>
          			<textarea class="form-control" name="response" rows="4" cols="50"></textarea>
          		</div>
         	</div>
        	<div class="modal-footer">
        		<button class="btn btn-primary" type="submit" value="Submit">Submit</button>
        	</div>
        </form>
 	  </div>
	</div>
</div>