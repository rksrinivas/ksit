<#include "feedbackParentModal.ftl">
<#include "feedbackStudentModal.ftl">
<#include "feedbackAlumniModal.ftl">
<#include "feedbackEmployerModal.ftl">

<link href="${css_path!}/form.css" rel="stylesheet">
<div class="modal fade" id="feedbackListModal" role="dialog" style="display: none;">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Feedback</h4>
        </div>
        <div class="modal-body"">          	      		
	  		<h5>Select Category</h5>
	  		<input type="hidden" id="feedbackPage" />
	  		<select class="wp-form-control wpcf7-text" id="feedbackList" required>
	  			<option value="">Select</option>
	  			<option value="PARENT">Parent</option>
	  			<option value="STUDENT">Student</option>
	  			<option value="ALUMNI">Alumni</option>
	  			<option value="EMPLOYER">Employer / User System</option>
	  		</select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
 	  </div>
	</div>
</div>