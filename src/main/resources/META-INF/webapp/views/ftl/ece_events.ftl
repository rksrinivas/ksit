<@page>
    
            <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <h3>Electronics and Communication Engineering Events</h3>
                    </blockquote>
                    
				
				
				
					 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                
                <#list eceEventsList as eceEvent>
                    
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
                    
                    	
					
						<h2 class="singCourse_title">
						
								"${eceEvent.heading!}"
						
						</h2>
						
						<p>	 <img alt="img" src="${eceEvent.imageURL!}" class="media-object">	</p>
						
						
						<p>
						
							"${eceEvent.content!}"
						
						</p>
						
						
						<p>
						
							"${eceEvent.eventDate!}"
						
						</p>
						
                    </div>
                   
                  </div>
                  
                  </#list>
                  
                </div>
                <!-- End single course -->
				
				
				
				
              
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title"></h2>
						<p>                          Project Exhibition 2016-17<img alt="img" src="img\ece\ece_events\inaug.jpg" class="media-object"></p>
						
						<p>Inauguration of Project Exhibition 2016-17</p>
						
						<p><span class="feed_date"> 2017-05-12</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">ECE Newsletter PRERANA 2017 </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture new l.jpg" class="media-object"></p>
					   <p>Releasing of ECE Newsletter PRERANA 2017</p>
						
						
						<p>                       <span class="feed_date">2017-05-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference on Recent Trends & Applications in Electrical and Electronics Engineering</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\abc.jpg" class="media-object"></p>
						
						   <p>A three days National conference-paper presentation was held by Electronics and Communication department under the collaboration of IEEE KSIT Student Chapter and ISTE, 10th-12th May 2017.</p>
						
						<p><span class="feed_date">2017-05-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture on Opportunities for Engineers,2017</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Pictureh.png" class="media-object"></p>
						
						   <p>Mr.Akhil Bhardwaj and Mr.Sameera Bhardwaj delivered the talk on Opportunities for Engineers . Both of them are Alumni of our institute</p>					
						
						<p>                       <span class="feed_date">2017-03-25</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> InnovacEEEa-2K17</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\p.png" class="media-object"></p>
						
						<p>One day semi technical event InnovacEEEa-2K17 was organized by IEEE KSIT on 11th March, 2017 at KSIT, Bangalore.This event included the students in a team of three, and four rounds</p>
						
						<p>					                          <span class="feed_date"> 2017-03-11</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IEEE student branch-Women's Day Celebrations</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\w.png" class="media-object"></p>
						
						   <p>IEEE WIE KSIT celebrated womens day on 8th March 2017.The chief guest for the womens day celebrations was Ms.Soumya Bhat, Gender activist.</p>					
						
						<p>                       <span class="feed_date">2017-03-08</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial Visit to IISC,Bangalore</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\iv.png" class="media-object"></p>
						
						   <p>March 4th is celebrated as IISc open day.All the students of EC Department visited IISC campus and explored the various projects exhibited.</p>					
						
						<p>                       <span class="feed_date">2017-03-04</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture on FIELD THEORY AND ITS APPLICATIONS IN ANTENNA DESIGN</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\ft.png" class="media-object"></p>
						
					   <p>Guest Lecture by Dr. Chaitanya Kumar MV, Principal, HKBK College of Engineering, Bangalore on FIELD THEORY AND ITS APPLICATIONS IN ANTENNA DESIGN was organized for IV Semester ECE & TCE students.</p>						
						
						<p>                       <span class="feed_date">2017-02-20</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk on Latest Trends in Wireless Communications</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\phoj1.jpg" class="media-object"></p>
						
					   <p>Technical Talk on Latest Trends in Wireless Communications was organized on for Six Sem students. Mr Devadas Pai,CEO & Founder Nano Cell Technologies Bangalore, India was the Resource Person.</p>						
						
						<p>                       <span class="feed_date">2017-02-13</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Raspberry Pi and its Applications in IoT</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\pppppppppppp.png" class="media-object"></p>
						
					   <p>A Four day Faculty Development Program on Raspberry Pi and its Applications in IoT,conducted by Department of Electronics and Communication Engineering,Computer Science&Engineering and Telecommunication Engineering, in association with IEEE, ISTE, and CSI-India, from 18th to 21st Jan 2017, under the banner of IETE .</p>						
						
						<p><span class="feed_date"> 2017-01-18</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar for 5th semester students on Entrepreneurship and leadership</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\IMG_20161103_144851268_HDR.jpg" class="media-object"></p>
						
					   <p>A half-day seminar on 3rd November 2016 Seminar for 5th semester students on Entrepreneurship and leadership</p>						
						
						<p><span class="feed_date">2016-11-03</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day Workshop on Software testing by M/S Uniqval </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\software testing.JPG" class="media-object"></p>
						
					   <p>Two day Workshop on Software testing Conducted in Micro processor Lab by Mr.Anantha Somayaji,CEO,M/S Uniqval.</p>						
						
						<p><span class="feed_date">2016-11-05</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">1 st IEEE Day celebrations in KSIT was held on 4th October 2016</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\pp.JPG" class="media-object"></p>
						
					   <p>The First IEEE Day celebrations in KSIT was held on 4th October 2016</p>	
						
						<p><span class="feed_date">2016-10-04</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day workshop on Raspberry Pie by Viswa jyothi Technologies Private by Viswa Jyothi Technologie</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\Rasp1.JPG" class="media-object"></p>
						
						   <p>Two day Workshop on Raspberry Pie was organised in EC Department at On Line Lab.</p>
						
						<p>                       <span class="feed_date">2016-10-23</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title"> A three day workshop on RC Aircraftwith Skyfi Education Labs Pvt.Ltd</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\RC.jpeg" class="media-object"></p>
						
					   <p>A three day workshop on RC Aircraft was organized under IEEE in collaboration with Skyfi Education Labs Pvt.Ltd. on 30th September,1st,2nd October 2016 at KSIT, Bangalore.</p>	
						
						<p>                       <span class="feed_date">2016-10-01</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title"> Industrial visit was conducted by KSIT under the collaboration of IEEE .</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\Rasp1 (1).JPG" class="media-object"></p>
						
					<p>India International Innovation Fair held at Bangalore International Exhibition Centre (BIEC).An industrial visit was conducted by KSIT under the collaboration of IEEE .</p>	
						
						<p><span class="feed_date">2016-09-11</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day IETE workshop on ARM CORTEX by TENET TECHNETRONICS  </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\workshop.JPG" class="media-object"></p>
						
						   <p>The Two day IETE workshop on ARM CORTEX by TENET TECHNETRONICS was jointly organized by TCE &ECE Department of K.S.I.T on 27th and 28th of August 2016 in Online Lab,K.S. Institute of Technology, Bangalore.</p>
						
						<p><span class="feed_date">2016-08-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture on Applications of Image Processing organized for VII semester ECE students</h2>
						<p><img alt="img" src="img\ece\ece_events\gest lec.JPG" class="media-object"></p>
						
					   <p>Guest Lecture on Applications of Image Processing was organized on 20 August 2016 for final year students.Dr.Debabrata Samanta Associate professor Dept. of Computer Applications, Dayananda Sagar College of Arts, Science and Commerce, Bangalore, India was the Guest speaker.</p>	
						
						<p><span class="feed_date">2016-08-20</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A visit to an orphanage was organized under the collaboration of IEEE STB Branch of KSIT</h2>
						<p><img alt="img" src="img\ece\ece_events\orphan.JPG" class="media-object"></p>
						
				   <p>A visit to an orphanage on 31st July was organized under the collaboration of IEEE STB Branch of KSIT.</p>		
						
						<p><span class="feed_date">2016-07-31</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition 2015-2016</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\pro1 (10).jpg" class="media-object"></p>
						
						   <p>Department of Electronics & Communication Engineering Project Exhibition 2015-2016</p>
						
						<p>                       <span class="feed_date"> 2016-05-14</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Annual Valedatory Function </h2>
						<p>  <img alt="img" src="img\ece\ece_events\vali.png" class="media-object"></p>
						
				<p>On 29th of April,2016 Annual IEEE Valedictory function was organized by the IEEE STB and WIE affinity group to felicitate students on successful completion of their tenure as office bearers of IEEE and WIE. Dr. DeepaShenoy, IEEE Bangalore Section SAC Chair had presided over the function as Chief Guest.</p>		
						
						<p><span class="feed_date">2016-04-29</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A two day workshop on MSP430 microcolntroller was organized under ISTE in collaboration with JYOTI E</h2>
						<p><img alt="img" src="img\ece\ece_events\New Pic (8).png" class="media-object"></p>
						
					   <p>A two day workshop on MSP430 microcolntroller was organized under ISTE in collaboration with JYOTI EMBEDDED LABS by electronics and communication department on 16th and 17th of March, 2016 at KSIT, Bangalore. This workshop was targeted for all 4th semester ECE students.</p>	
						
						<p><span class="feed_date">2016-03-17</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A one day Technical Seminar on Embedded System Design An Introduction to Microchip and ARM Microc</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Pi (7).png" class="media-object"></p>
						
				   <p>A one day Technical Seminar on Embedded System Design An Introduction to Microchip and ARM Microcontroller was organized on 24 February 2016 for students of Sixth Semester ECE students at 2.00 PM in ECE Seminar Hall. Mr. N Nithin Avasthi, VP Technical, APSIS Solutions, Bangalore was the Guest speaker.</p>		
						
						<p>                       <span class="feed_date">2016-02-24</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day IETE workshop on 'Wireless Embedded Systems' by TENET TECHNETRONICS was jointly organized by </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Pice (6).png" class="media-object"></p>
						
					   <p>The Two day IETE workshop on 'Wireless Embedded Systems' by TENET TECHNETRONICS was jointly organized by TCE &ECE Department of K.S.I.T on 26th and 27th of February 2016 under the banner of IETE Bangalore section in Online Lab.</p>			
						
						<p>                       <span class="feed_date">2016-02-26</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> A two day workshop on Quadrotor was organized under IEEE in collaboration with Skyfi Education Labs</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New P (5).png" class="media-object"></p>
						
				   <p>A two day workshop on Quadrotor was organized under IEEE in collaboration with Skyfi Education Labs Pvt.Ltd. on 27th and 28th of February,2016 at Electronics and Communication Dept of KSIT, Bangalore.</p>		
						
						<p>                       <span class="feed_date">2016-02-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial visit to Centre for Development of Advanced Computing (C-DAC) on February, 2016</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New (4).png" class="media-object"></p>
						
					<p>The IEEE Student Branch in collaboration with the Department of Electronics and Communication, K.S.I.T hadorganized an industrial visit to Centre for Development of Advanced Computing (C-DAC)on 19th February, 2016 for IEEE Student Members from Electronics &Communication</p>	
						
						<p><span class="feed_date">2016-02-19</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial visit to Elecrama-2016 on February, 2016</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\elec.png" class="media-object"></p>
						
					   <p>An Industrial visit to Elecrama-2016 on 16th February, 2016 for 4 th Sem students of ECE Dept</p>	
						
						<p>					                       <span class="feed_date">2016-02-16</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A one day Faculty Development Program on Writing research proposals was organized on February 2016 </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\frb.png" class="media-object"></p>
						
					   <p>A one day Faculty Development Program on Writing research proposals was organized on 1st February 2016 for faculties of various departments at 10:00 AM in ECE Seminar Hall. Dr. S. Viswanadha Raju, Professor, JNTUH, Hyderabad, Telangana was the Guest speaker</p>	
						
						<p>					                       <span class="feed_date">2016-02-01</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Motivational Talk on Developing Soft Skills conducted for 4 th Sem B.E. students Janaury 2016</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\mot.png" class="media-object"></p>
						
					<p>A one day Motivational talk on Developing Soft Skills was organized on 25 Janaury 2016 for students of Fourth Semister ECE students at 10:30 AM in ECE Seminar Hall. Mr. VidyaNag, CEO, VIdyaNag Training Guru Pvt Ltd , Bangalore was the Guest speaker.</p>	
						
						<p>					                          <span class="feed_date">2016-01-25</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> Industrial visit to All India Radio, Bengaluru was organized to fifth semester students in October</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture10.png" class="media-object"></p>
						
					   <p>An industrial visit to All India Radio, Bengaluru was organized to fifth semester students in October,2015. Mrs. Sangeetha V, Asst. Prof, Mrs. Nithya Kumari, Asst. Prof and Mr. Sudarshan B, Asst. Prof accompanied the students</p>	
						
						<p><span class="feed_date">2015-10-31</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">                       Industrial visit to Tata Power Solar Systems Limited, Bangalore October 2015</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture (9).png" class="media-object"></p>
						
				   <p>Industrial visit to Tata Power Solar Systems Limited, Bangalore October 2015 for VII Semester students</p>		
						
						<p>					                       <span class="feed_date">2015-10-31</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial visit to Hindustan Aeronautical Limited (HAL), Bengaluru October 2015</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture (8).png" class="media-object"></p>
						
					   <p>An industrial visit to Hindustan Aeronautical Limited (HAL), Bengaluru was organized to third semester students .</p>	
						
						<p>					                       <span class="feed_date">2015-10-06</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">                      Two day workshop on Robotics was organized by IEEEStudent chapter</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture (7).png" class="media-object"></p>
						
					   <p>A two day workshop on Robotics was organized by IEEE Student chapter and WIE affinity group in association with Robominions</p>	
						
						<p>                       <span class="feed_date">2015-10-31</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A guest lecture on Dr.Kalam, Rockets and Satellites October 2015</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture (5).png" class="media-object"></p>
						
					<p>A guest lecture on Dr.Kalam, Rockets and Satellites by.Mr.Nagaraj Ananth from ISRO on 15-10-2015</p>	
						
						<p><span class="feed_date">2015-10-15</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> A seminar on Future of the world was organized on 28th July 2015</h2>
						<p><img alt="img" src="img\ece\ece_events\New Picture (6).png" class="media-object"></p>
						
					   <p>A seminar on Future of the world was organized on 28th July 2015 by Mr.Anson Ben ,CEO Global intelnet Services ,Bangalore</p>	
						
						<p><span class="feed_date">2015-07-28</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical talk on Embeded Systems July 2015</h2>
						<p><img alt="img" src="img\ece\ece_events\New Picture (4).png" class="media-object"></p>
						
					   <p>Technical talk on Embeded Systems by Mr.Bhanuprashant HOD & Professor BNMIT</p>	
						
						<p>                       <span class="feed_date"> 2015-07-24</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Faculty development program on VLSI Design from 22-06-2015 to 26-06-2015 </h2>
						<p><img alt="img" src="img\ece\ece_events\New Picture (3).png" class="media-object"></p>
						
					   <p>Five days Faculty development program on VLSI Design Using Cadence tools</p>	
						
						<p>                       <span class="feed_date">2015-06-22</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> Technical Talk on Trends in Mobile Communications and networks in April 2015</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture (2).png" class="media-object"></p>
						
					   <p>A Technical Talk on Trends in Mobile Communications and networks by Mr.Prashanth Wali from IIITB Bangalore</p>	
						
						<p><span class="feed_date"> 2015-04-17</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk on Mobile Sensor networks April 2015</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture.png" class="media-object"></p>
						
					   <p>A Technical Talk on Mobile Sensor networks By Dr.Sunil manvi from Reva</p>	
						
						<p>                       <span class="feed_date">2015-04-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">FDP on VLSI and Cadence Tools from june 29 -july 3 2015</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\Optimized-Picture 029.jpg" class="media-object"></p>
						
					   <p>Technical talk on Opearting System concept by Mrs.Dept.Of ECE organizing 5 days FDP on VLSI and Cadence Tools from june 29 -july 3 2015 Nalini Kannan on Aprill 2015</p>	
						
						<p><span class="feed_date"> 2015-06-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop on Arm Cortex</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\New Picture (9) (1).png" class="media-object"></p>
						
					   <p>Dept of ECE.. conducting two days workshop on Arm Cortex</p>	
						
						<p>                       <span class="feed_date">2015-04-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop on Fibre optics</h2>
						<p><img alt="img" src="img\ece\ece_events\New 1 Picture.png" class="media-object"></p>
						
					   <p>Dept of ECE.. Conducting Two days workshop on Fibre optics..</p>	
						
						<p><span class="feed_date">2015-04-02</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk on "Trends in mobile communication &Networking  </h2>
						<p> <img alt="img" src="img\ece\ece_events\IMG_0569edit.jpg" class="media-object"></p>
						
					   <p>Dept OF ECE is conducting a Technical Talk on "Trends in mobile communication &Networking on 02-03-15.</p>	
						
						<p><span class="feed_date">2015-03-02</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Dept OF ECE &TCE are Jointly organizing an IETE Student Event Forum</h2>
						<p><img alt="img" src="img\ece\ece_events\Picture 095edit.jpg" class="media-object"></p>
						
					<p>Dept OF ECE &TCE are Jointly organizing an IETE Student Event Forum on 12-03-2015.</p>	
						
						<p><span class="feed_date">2015-03-12</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture on Data Communication &Innovation</h2>
						<p><img alt="img" src="img\ece\ece_events\" class="media-object"></p>
						
					   <p>Dept Of ECE..conducting a "Guest Lecture on Data Communication &Innovation" on 11-02-15.</p>	
						
						<p>                       <span class="feed_date">2015-02-11</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Career Guidance on the international education scene &Advantage of Research</h2>
						<p><img alt="img" src="img\ece\ece_events\Optimized-Picture 089.jpg" class="media-object"></p>
						
					<p>Dept Of ECE &TCE Jointly Organized Seminar on "Career Guidance on the international education scene &Advantage of Research" on 09/03/15.</p>	
						
						<p><span class="feed_date">2015-03-09</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industry specific training on Embedded system design </h2>
						<p><img alt="img" src="img\ece\ece_events\IMG_3474.jpg" class="media-object"></p>
						
					<p>Industry specific training on Embedded system design, Mission VTU Empower 10000 initiative is started on 5th july 2014..</p>	
						
						<p>                          <span class="feed_date">2014-07-05</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">OPTIMUS</h2>
						<p><img alt="img" src="img\ece\ece_events\" class="media-object"></p>
						
					<p>IEEE Event on 26th April 2014 Saturday ULTIMATE ENGINEER 1) Team of Max 4 persons 2) Total of 4 rounds 3) 10 teams enter the 3rd Round 4) Only teams entering the 3rd Round and after will be given IEEE participant Certificates Entry Fee Non-IEEE Members - 200 IEEE Members - 100 Timings : 9 am - 4 pm</p>	
						
						<p><span class="feed_date">2014-04-26</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">MATAPP 2013</h2>
						<p><img alt="img" src="img\ece\ece_events\" class="media-object"></p>
						
					   <p>Dept of ECE is conducting a Workshop on Matlab Tool and Its Application on MATAPP 28th February 2013</p>	
						
						<p><span class="feed_date">2013-02-28</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference NCRTEC13</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\" class="media-object"></p>
						
					   <p>Dept of ECE is conducting a National Conference on Advanced Trends in Electronics and communication Engineering NCRTEC13 on 30th August 2013</p>	
						
						<p>                       <span class="feed_date">2013-08-19</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">MSP 430</h2>
						<p><img alt="img" src="img\ece\ece_events\phpCUD9gLAM.jpg" class="media-object"></p>
						
					   <p>MSP 430 workshop</p>	
						
						<p>                       <span class="feed_date"> 2014-03-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">MATLAB WORKSHOP  </h2>
						<p><img alt="img" src="img\ece\ece_events\IMG_2460.jpg" class="media-object"></p>
						
					   <p>Workshop on Matalb Tools and Its Applications is conducted on 13th and 14th March 2014.</p>	
						
						<p>                       <span class="feed_date">2014-03-13</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Placement Details of 2013,2012,2011 Batch</h2>
						<p><img alt="img" src="img\ece\ece_events\" class="media-object"></p>
						
					<p>Placement Details of 2013,2012,2011 Batch</p>	
						
						<p>                          <span class="feed_date">2014-02-01</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Details for final year ECE  </h2>
						<p><img alt="img" src="img\ece\ece_events\" class="media-object"></p>
						
					   <p>Project details and guidelines for ECE Final YEAR 2014..</p>	
						
						<p><span class="feed_date">2014-01-29</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">KSIT STUDENT CHAPTER BRANCH</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\ksit Event poster .jpg" class="media-object"></p>
						
					   <p>KSIT STUDENT CHAPTER BRANCH</p>	
						
						<p><span class="feed_date">2013-12-09</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">KSIT "ELEMENTS" Technical Event</h2>
						<p><img alt="img" src="img\ece\ece_events\" class="media-object"></p>
						
					   <p>On 26th October 2013 Saturday <br/>Timings: 9AM -2PM<br/>Venue :Conference Hall  KSIT, Raghuvanhalli Bangalore-62 <br/>3 Events 1) CRANIUM (General Quiz)  2) RANDOM n TANDOM (Puzzle Solving) 3) CUBIX
					   </p>
	
						
						<p><span class="feed_date">2013-10-26</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Inaugural Function of IETE-ISF (The Institution of Electronics & Telecommunication Engineers- Instit</h2>
						<p><img alt="img" src="img\ece\ece_events\iete invitation.JPG" class="media-object"></p>
						
					   <p>Inaugural Function of IETE-ISF (The Institution of Electronics & Telecommunication Engineers- Institution Students Forum) on 9th April 2012</p>	
						
						<p><span class="feed_date"> 2012-04-09</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two Day International Conference on VLSI and Signal Provessing</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\ICVSP Brocher front1.jpg" class="media-object"></p>
						
					   <p>Two Day International Conference on VLSI and Signal Provessing on May 4th and 5th,2012 jointly organized by department of ECE, KSIT,In Association with ISTE,IETE,VTU,ISRO,cadence and National Instruments Bangalore. for more details click on http://icvsp.org/index.html</p>	
						
						<p><span class="feed_date">2012-05-04</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Three Day National Level Workshop on Matlab Tools & Its Applications</h2>
						<p><img alt="img" src="img\ece\ece_events\Matlab Poster.jpg" class="media-object"></p>
						
					   <p>Three Day National Level Workshop on Matlab Tools & Its Applications on 28th Feb to 2nd March Jointly Organized Dept. of Electronics & Communication Engg. & Dept. of Telecommunication Engg.Download the Registration form at KSIT website.</p>	
						
					   <p><span class="feed_date">2013-02-28</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day State Level Workshop on ETHICAL HACKING & CYBER FORENSICS</h2>
						<p><img alt="img" src="img\ece\ece_events\workshop-EH.JPG" class="media-object"></p>
						
					<p>Dept. of Computer Science & Engg., K S Institute of Technology conducted two days State Level workshop on  ETHICAL HACKING & CYBER FORENSICS 0n 22-02-13 & 23-02-13.</p>	
						
					   <p>                          <span class="feed_date">2013-02-22</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Idea-2012</h2>
						<p><img alt="img" src="img\ece\ece_events\idea post resized.jpg" class="media-object"></p>
						
					<p>A State Level Technical Contest, Idea-2012 </p>	
						
					   <p><span class="feed_date">2012-09-15</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NCCSC-12 </h2>
						<p><img alt="img" src="img\ece\ece_events\Agust_9__NCCSC_Invitationp.jpg" class="media-object"></p>
						
					   <p>NCCSC</p>	
						
					   <p>                       <span class="feed_date">2012-08-09</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NCCSC-2012  </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\Agust_9__NCCSC_Invitation.jpg" class="media-object"></p>
						
					<p>National Conference on Communication & Soft Computing</p>	
						
					   <p><span class="feed_date">2012-08-09</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference on Communication and Soft Computing</h2>
						<p><img alt="img" src="img\ece\ece_events\DSC_0283.JPG" class="media-object"></p>
						
					   <p>National Conference on Communication and Soft Computing on 9th August 2012</p>	
						
					   <p><span class="feed_date">National Conference on Communication and Soft Computing</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day Workshop on Low power embedded systems using TI MSP430 microcontroller</h2>
						<p> <img alt="img" src="img\ece\ece_events\New Picture.jpg" class="media-object"></p>
						
					   <p>Dept. of ECE, K S Institute of Technology conducted two days workshop on Low power embedded systems using  TI MSP430, in association with TI university program and Cranes software private limited on 21-22 feb 2012.</p>	
						
					   <p><span class="feed_date">2012-02-21</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Demo training program on Embeded C</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\DSC_0283 (1).JPG" class="media-object"></p>
						
					   <p>The department of ECE organized a demo training program on Embedded C for 6th semester students, delivered by Suhas Gokulae & team of IDEME on 29.02.2012.</p>	
						
					   <p><span class="feed_date">  2012-02-29</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">LABVIEW & NI HARDWARE Workshop </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\DSC_0283 (2).JPG" class="media-object"></p>
						
					<p>Two Day Workshop on LABVIEW & NI HARDWARE is conducted on Sept 2nd and 3rd 2011, jointly organized by  Department of EC, KSIT & National Instruments, Bangalore</p>	
						
					   <p>                          <span class="feed_date">2011-09-02</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IDEA - 2011, A State Level Technical Contest</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\DSC_0283 (3).JPG" class="media-object"></p>
						
					<p>IDEA - 2011, A State Level Technical Contest on Sep 15th  2011, to celebrate 151st Birth Anniversary of Sir M.Visvesvaraya, organized by Department of ECE, KSIT. , in association with IEEE student chapter</p>	
						
					   <p>                       <span class="feed_date">2011-09-15</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">workshop on Entrepreneurship Awareness Camp  </h2>
						<p>                          <img alt="img" src="img\ece\ece_events\DSC_0283 (4).JPG" class="media-object"></p>
						
					   <p>Department of Electronics and Commuication conducted 3- days workshop on Entrepreneurship Awareness in co-ordination with Visvesvaraya technological University, Belgaum.</p>	
						
					   <p>                       <span class="feed_date"> 2010-04-27</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop on PLD\'s FPGA design Flow</h2>
						<p>                          <img alt="img" src="img\ece\ece_events\DSC_0283 (5).JPG" class="media-object"></p>
						
					<p>EC department conducted workshop on PLD\'s FPGA design Flow, sponsored by IMAPS India chapter and CG Corel on 20th and 21st june 2008.</p>	
						
					   <p><span class="feed_date">2008-06-20</span></p>	
						
					</div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
							
				
				
				 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
    
    </@page>