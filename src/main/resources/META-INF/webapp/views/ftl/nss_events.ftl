 <@page>
	 
    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <h3>NSS Events</h3>
                    </blockquote>
                    
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                
                  <#list nssEventsList as nssEvent>
                  <div class="single_course wow fadeInUp">
                  
                    <div class="singCourse_content">
                    
                    	
						<h2 class="singCourse_title">
						
								"${nssEvent.heading!}"
						
						</h2>
						
						<p>	 <img alt="img" src="${nssEvent.imageURL!}" class="media-object">	</p>
						
						
						<p>
						
							"${nssEvent.content!}"
						
						</p>
						
						<p>
						
							"${nssEvent.eventDate!}"
						
						</p>
						
                    </div>
                    
                  </div>
                  
                  </#list>
                  
                </div>
                <!-- End single course -->
						 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
	
</@page>