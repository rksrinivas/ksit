<@page>
		 <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-8 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <!-- start blog archive  -->
              <div class="row">
                <!-- start single blog archive -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
					<h2 class="blog_title">It's not a complement, it's harrassment</h2>
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="img\about\anti ragging.jpg">
                      </a>
                    </div>
                    
                    
                  </div>
                </div>
                <!-- start single blog archive -->
                
               
              </div>
              <!-- end blog archive  -->
              
            </div>
          </div>
          <!-- End course content -->

          <!-- start course archive sidebar -->
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="courseArchive_sidebar">
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Highlights <span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
				<marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();"  direction="up">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                      
					  <p>1) A separate team is formed to undetake Anti-Ragging measures as well as Anti-sexual Harrasment </p>
					  
					  <br/>
					  
					  <p>2) The team members are highly energetic and dynamic</p>
					  
					  <br/>
					  
					  <p>3) Each and every student in the campus is been monitered</p>
					  
					  <br/>
					  
					  <p>4) The campus is equipped with CCTV cameras.</p>
					  
					  <p></p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                     
                      <div class="media-body">
                      
					  <p> </p>
					  
                      </div>
                    </div>
                  </li>
                  
				</marquee>
                </ul>
              </div>
              <!-- End single sidebar -->
             
			  
            </div>
          </div>
          <!-- start course archive sidebar -->
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
	

    
   	
	 <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs" style="margin-top:0%;">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="newsfeed_area wow fadeInRight">
            <ul class="nav nav-tabs feed_tabs" id="myTab2">
              <li class="active"><a href="#news" data-toggle="tab">ANTI RAGGING MEASURES</a></li>
              <li><a href="#notice" data-toggle="tab">Anti-Sexual Harassment</a></li>
              <li><a href="#events" data-toggle="tab">Tobacco Free Campus</a></li>         
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
			
			
              <!-- Start news tab content -->
              <div class="tab-pane fade in active" id="news">                
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
													
							<div class="col-lg-12 col-md-12 col-sm-12">
								<p id="news"></p>
								 <p>KSIT has taken utmost care to avoid ragging in the campus and hostels. KSIT is successful in evading ragging in the campus and hostels through its unique approach of Educating and Mentoring both junior and senior students and strict vigilance on any such incidences.</p>
								<blockquote>
								  <span class="fa fa-quote-left"></span>
								 <p> <b>Committees have been formed to carryout enquiry of any such complaints, and to carryout surprise visits to hostels and key places in the campus. Please refer to the end of this document. Affected students and parents can contact the members through mail or phone.</b></p>
								</blockquote>
								

								
								<h3>What constitutes Ragging ? </h3>
								<p>Ragging constitutes one or more of any of the following acts:</p>

								<ul class="black">
								  <li><span class="fa fa-angle-double-right"></span> any conduct by any student or students whether by words spoken or written or by an act which has the effect of teasing, treating or handling with rudeness a fresher or any other student;</li>
								  <li><span class="fa fa-angle-double-right"></span> indulging in rowdy or undisciplined activities by any student or students which causes or is likely to cause annoyance, hardship, physical or psychological harm or to raise fear or apprehension there of in any fresher or any other student;</li>
								  <li><span class="fa fa-angle-double-right"></span> asking any student to do any act which such student will not in the ordinary course do and which has the effect of causing or generating a sense of shame, or torment or embarrassment so as to adversely affect the physique or psyche of such fresher or any other student;</li>
								  <li><span class="fa fa-angle-double-right"></span> any act by a senior student that prevents, disrupts or disturbs the regular academic activity of any other student or a fresher;</li>
								  <li><span class="fa fa-angle-double-right"></span> exploiting the services of a fresher or any other student for completing the academic tasks assigned to an individual or a group of students.</li>
								  <li><span class="fa fa-angle-double-right"></span> any act of financial extortion or forceful expenditure burden put on a fresher or any other student by students;</li>
								  <li><span class="fa fa-angle-double-right"></span> any act of physical abuse including all variants of it: sexual abuse, homosexual assaults, stripping, forcing obscene and lewd acts, gestures, causing bodily harm or any other danger to health or person;</li>
								  <li><span class="fa fa-angle-double-right"></span> any act or abuse by spoken words, emails, posts, public insults which would also include deriving perverted pleasure, vicarious or sadistic thrill from actively or passively participating in the discomfiture to fresher or any other student;</li>
								  <li><span class="fa fa-angle-double-right"></span> any act that affects the mental health and self-confidence of a fresher or any other student with or without an intent to derive a sadistic pleasure or showing off power, authority or superiority by a student over any fresher or any other student.</li>
								  
								</ul>
								
								<h4>Actions to be taken against students for indulging and abetting ragging in technical institutions Universities including Deemed to be University imparting technical education</h4>
								
								<p>The punishment to be meted out to the persons indulged in ragging has to be exemplary and justifiably harsh to act as a deterrent against recurrence of such incidents.</p>
								
								<p>Every single incident of ragging a First Information Report (FIR) must be filed without exception by the institutional authorities with the local police authorities.</p>
								
								<h4>The Anti-Ragging Committee of the institution shall take an appropriate decision, with regard to punishment or otherwise, depending on the facts of each incident of ragging and nature and gravity of the incident of ragging.</h4>
								<p>Depending upon the nature and gravity of the offence as established the possible punishments for those found guilty of ragging at the institution level shall be any one or any combination of the following</p>
								
								<ul class="black">
								  <li><span class="fa fa-angle-double-right"></span>  Cancellation of admission</li>
								
								 <li><span class="fa fa-angle-double-right"></span>  Suspension from attending classes</li>
								
								
								 <li><span class="fa fa-angle-double-right"></span> Withholding/withdrawing scholarship/fellowship and other benefits</li>
								
								
								 <li><span class="fa fa-angle-double-right"></span> Debarring from appearing in any test/examination or other evaluation process</li>
								
								 <li><span class="fa fa-angle-double-right"></span>  Withholding results</li>
								
								 <li><span class="fa fa-angle-double-right"></span>   Debarring from representing the institution in any regional, national or international meet, tournament, youth festival, etc.</li>
								
								 <li><span class="fa fa-angle-double-right"></span> Suspension/expulsion from the hostel</li>
								
								 <li><span class="fa fa-angle-double-right"></span> Rustication from the institution for period ranging from 1 to 4 semesters</li>
								
								 <li><span class="fa fa-angle-double-right"></span> Expulsion from the institution and consequent debarring from admission to any other institution.</li>
								
								</ul>
								
							
											
							
							</div>
							
							
							<!-- Why us top titile -->
								  <div class="row">
									<div class="col-lg-12 col-md-12"> 
									  <div class="title_area">
										<h2 class="title_two">ANTI-RAGGING SQUAD</h2>
										<span></span> 
									  </div>
									</div>
								  </div>
						    <!-- End Why us top titile -->
							
							<div class="row">
							
								<#list facultyList as faculty>
									<div class="col-md-3 col-lg-3 col-sm-6">
										<div class="single_teacher_item">
												<div class="teacher_thumb">
													<img src="${faculty.imageURL!}" alt="" />
													<div class="thumb_text">
														<h2>${faculty.name}</h2>
														<p>${faculty.designation}</p>
													</div>
												</div>
												<div class="teacher_content">
													<h2>${faculty.name}</h2>
													<span>${faculty.designation}</span>
													<p>${faculty.qualification}</p>
													<span>${faculty.department}</span>
													<p><a href=""${faculty.profileURL!}" target="_blank">view profile</a></p>
													<div class="social_icons">
														<a href="#" class="tw"><i class="fa fa-twitter"></i></a>
														<a href="#" class="fb"><i class="fa fa-facebook"></i></a>
														<a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
														<a href="#" class="go"><i class="fa fa-google-plus"></i></a>
													</div>
												</div>
											</div>
										</div>
										
									</#list>	
				
														
								
								
								
							</div>	
			
			
							
						
						</div>
                    </div>                    
                  </li>
                  
                  
                </ul>                
               
              </div>
			  
			  
              <!-- Start notice tab content -->  
              <div class="tab-pane fade " id="notice">
                <div class="single_notice_pane">
                  <ul class="news_tab">
                    <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							
						
							<div class="col-lg-12 col-md-12 col-sm-12">
								<p id="news"></p>
								<p>KSIT has created a friendly ambience for all staff members and students in the campus. Issues related to work, human relation, harassment (both Men and Women) and injustice in any other form will all be addressed by two committees, viz., Grievance Committee and Women Cell. Women Cell will deal with Sexual Harassment cases referred by female staff members and students.</p>
								
								
								<a href="img\administration\ASHC.pdf" target="_blank"> WOMEN SAFETY AND SELF DEFENSE </a>

							</div>
							
													
						
								
							
							
							<!-- Why us top titile -->
								  <div class="row" style="margin-left:10px;">
									<div class="col-lg-12 col-md-12"> 
									  <div class="title_area">
										<h2 class="title_two">ANTI-SEXUAL HARASSMENT COMMITTEE</h2>
										
											<h2>Co-Ordinator:</h2>
											<p>Ms. Chanda V. Reddy   HOD,TCE</p>
											
											<h2>Members:</h2>


											
											
												<table class="table table-striped course_table" style="text-align:left !important;">

												<tr>
													<th>Sl. no</th>
													<th>Name</th>
												</tr>
												
												
												<tr>
													<td>1.</td>
													<td>Ms.L.Nirmala,MED</td>
												</tr>
												
												<tr>
													<td>2.</td>
													<td>Ms. Sreesudha, MED</td>
												</tr>
												
												<tr>
													<td>3.</td>
													<td>Ms.Sangeetha.V, ECE	</td>
												</tr>
												
												<tr>
													<td>4.</td>
													<td>Ms. Vijayalakshmi, CSE</td>
												</tr>
												
												<tr>
													<td>5.</td>
													<td>Ms.Rekha.N,TCE</td>
												</tr>
												
												<tr>
													<td>6.</td>
													<td>Ms. Sridevi B.R , BS &H</td>
												</tr>
												
												<tr>
													<td>7.</td>
													<td>Ms.Vasantha,Librarian</td>
												</tr>
												
											</table>
										
										
										<span></span> 
									  </div>
									</div>
								  </div>
						    <!-- End Why us top titile -->

						
						<!-- content-->
							
								<div class="col-md-12 col-lg-12 col-sm-12">
						
									<h3>POLICY STATEMENT</h3>
										<ul class="list">
										<li>KSIT is committed to providing an environment free from sexual harassment.</li>
										<li>KSIT believes that you should be afforded the opportunity to work in an environment free of sexual harassment. Sexual harassment is a form of misconduct that undermines the employment relationship. No employee, either male or female, should be subjected verbally or physically to unsolicited and unwelcome sexual overtures or conduct. Sexual harassment refers to behavior that is not welcome, that is personally offensive, that debilitates morale and, therefore, interferes with work effectiveness. Behavior that amounts to sexual harassment may result in disciplinary action, up to and including dismissal.</li>
										<li>Any and all complaints or allegations of sexual harassment will be investigated promptly. Appropriate, corrective action will be implemented based upon the results of the investigation in the event harassment in is found to have taken place.</li>
										<li>In response to the Supreme Court Guidelines in Visakha Judgment has developed Policyand Procedures designed to prevent sexual harassment, and to deal with any complaints which may arise.</li>
										<li>Whereas Sexual Harassment infringes the fundamental right of a woman to gender equality under Article 14 of the Constitution of India and her right to life and live with dignity under Article 21 of the Constitution which includes a right to a safe environment free from sexual harassment.</li>
										<li>And whereas the right to protection from sexual harassment and the right to work with dignity are recognized as universal human rights by international conventions and instruments such as Convention on the Elimination of all Forms of Discrimination against Women (CEDAW), which has been ratified by the Government of India.</li>

										</ul>
										
										
									
									<h3>DEFINITION OF SEXUAL HARASSMENT</h3>
										<blockquote>
										  <span class="fa fa-quote-left"></span>
										  "Sexual Harassment includes such unwelcome sexually determined behavior as physical contact and advances, sexually coloured remarks, showing pornography and sexual demand, whether by words or actions. Such conduct can be humiliating and may constitute a health and safety problem; it is discriminatory when the woman has reasonable grounds to believe that her objection would disadvantage her in connection with her employment, including recruitment or promotion, or when it creates a hostile working environment.


										</blockquote>
								
									
									<p>There are usually three kinds of sexual harassment and the following examples are not exhaustive. Sexual harassment can be perpetrated upon members of the opposite gender or one's own gender.</p>
									
									
									<ul>
									
									<h5>Non-Verbal</h5>
									<li><span class="fa fa-angle-double-right"></span> Gestures</li>
									<li><span class="fa fa-angle-double-right"></span> Staring / leering</li>
									<li><span class="fa fa-angle-double-right"></span> Invading personal space</li>
									<li><span class="fa fa-angle-double-right"></span> Pin-ups</li>
									<li><span class="fa fa-angle-double-right"></span>  Offensive publications</li>
									<li><span class="fa fa-angle-double-right"></span> Offensive letters / memos</li>
									<li><span class="fa fa-angle-double-right"></span> Unsolicited and unwanted gifts</li>
									
									
									<h5>Verbal</h5>
									<li><span class="fa fa-angle-double-right"></span>  Language of a suggestive or explicit nature</li>
									<li><span class="fa fa-angle-double-right"></span> Unwanted propositions</li>
									<li><span class="fa fa-angle-double-right"></span>  Jokes of a sexual or explicit nature</li>
									<li><span class="fa fa-angle-double-right"></span> Use of affectionate names</li>
									<li><span class="fa fa-angle-double-right"></span> Questions or comments of a personal nature</li>
									
									<h5>Physical</h5>
									<li><span class="fa fa-angle-double-right"></span> Deliberate body contact</li>
									<li><span class="fa fa-angle-double-right"></span> Indecent exposure</li>
									<li><span class="fa fa-angle-double-right"></span> Groping / fondling / kissing</li>
									<li><span class="fa fa-angle-double-right"></span> Coerced sexual contact</li>
									
									
									</ul>
									
									<h3>SEXUAL HARASSMENT IS UNLAWFUL</h3>
									<p>Every employee shall have a right to be free from Sexual Harassment and the Right to work in an environment free from any form of Sexual Harassment.</p>
									<p>No employer or any person who is a part of the management or ownership, a supervisor or a co-employee of the KSIT shall, sexually harass an employee whether male or female, where he or she is employed; whether the harassment occurs in / at the workplace, or at a place where the said persons have gone in connection with the work or the workplace, or at any place whatsoever Sexual Harassment will amount to misconduct in employment and the staff service rules / regulations governing employment shall govern such misconduct, in addition to the provisions of this Act;</p>
									<p>KSIT will take all necessary and reasonable steps to prevent and ensure that no staff employed in the KSIT is subject to sexual harassment by any third party during the course of employment.</p>
									<p>Where any such Sexual Harassment occurs, the employer shall take all necessary steps to assist the aggrieved man/ woman to redress the act of Sexual harassment. No employee of KSIT shall sexually harass an outsider who visits the KSIT Society for a legitimate Purpose.</p>
									<p>No person shall sexually harass another person in the course of providing or offering to provide goods or services to that other person.</p>
							
							
									<h3>PREVENTIVE STEPS</h3>
									<p>Consistent with the existing law under Vishaka, KSIT shall take all reasonable steps to ensure prevention of sexual harassment at work. Such steps shall include:</p>
									<p>Circulation of KSIT's policy in English/Hindi/vernacular in regional offices on sexual harassment to all persons employed by or in any way acting in connection with the work and/or functioning of KSIT.</p>
									<p>Sexual harassment will be affirmatively discussed at monthly meetings, workshops etc, Conduct or cause to carry out in-house training on sexual harassment and addressing complaints to staff as well as members of Women Cell.</p>
									<p>Guidelines will be prominently displayed to create awareness of the rights of female employees.</p>
									<p>Widely publicize that the Sexual Harassment is a crime and will not be tolerated. The employer will assist persons affected in cases of sexual harassment by outsiders Names and contact numbers of members of the internal complaint's committee will be prominently displayed in all the offices/projects.</p>
							
							
									<h3>IF YOU ARE BEING HARASSED</h3>
									<p>Tell the harasser his/her behaviour is unwelcome and ask him/her to stop.</p>
									<p>Keep a record of incidents (date, time, locations, possible witnesses, what happened, your response). You do not have to have a record of events in order to file a complaint, but a record can strengthen your case and help you remember details over time.</p>
									
									
									<h3>REPORTING A COMPLAINT</h3>
									<p>If an individual believes he or she is the victim of sexual harassment or retaliation, s/he is encouraged to report such complaint immediately. The Principal has designated the Women Cell to receive all complaints, verbal or written, of harassment on behalf of the organization.</p>
									<p>The Women Cell constituted will be appointed for a period of three years. A complaint under this Act may be lodged with ICC at the earliest point of time and in any case within 15 days of occurrence of the alleged incident. The complaint shall contain all the material and relevant details concerning the alleged Sexual Harassment.</p>
									<p>If the complainant feels that he or she cannot disclose his/ her identity for any reason, the complainant shall address the complaint to the Principal/Director and hand over the same in person or in a sealed cover.</p>

									<p>The Principal/Director shall retain the original complaint with him/her and send to the Women Cell for further enquiry.</p>
									
									
									<!-- Why us top titile -->
								  <div class="row">
									<div class="col-lg-12 col-md-12"> 
									  <div class="title_area">
										<h2 class="title_two">GUIDELINES FOR ENQUIRY</h2>
										<span></span> 
									  </div>
									</div>
								  </div>
						    <!-- End Why us top titile -->

								<h3>DISPUTE RESOLUTION PRIOR TO ENQUIRY</h3>	
								<p>The Women Cell may if, and only if so requested by the aggrieved person/woman, try to resolve the matter informally by intervening and thereby permitting the parties to resolve the matter mutually before the commencement of the formal enquiry proceedings. The person to carry out the Dispute Resolution Process shall be chosen from the Internal Complaints Committee by the aggrieved person/woman. The Officer shall carry out the Dispute Resolution Process on a Local Complaints Committee.</p>
							
								<h3>ORAL COMPLAINTS TO BE REDUCED IN WRITING</h3>	
								<p>It shall be the duty of the Women Cell before whom an oral complaint is made under this Act to reduce the said complaint in writing and read out the complaint to the complainant in the language requested by the complainant and obtain the signature of the complainant.</p>
							
								<h3>CONFIDENTIALITY OF COMPLAINTS</h3>	
								<p>It shall be the duty of all the persons and authorities designated under this Act to ensure that all complaints lodged under this chapter shall be strictly confidential. The name of the aggrieved person/woman shall not be referred to in any records of proceedings, or any orders or Judgments given under this Act; The name of neither the aggrieved person/ woman nor her identity shall be revealed by the press / media or any other persons whilst reporting any proceedings, case, order or Judgment under this Act.</p>
							
								<h3>IMPROPER COMPLAINTS</h3>	
								<p>This policy shall not be used to bring frivolous or malicious complaints against anyone. Making a knowingly false complaint subjects the complaint to disciplinary or corrective action. However, failure to prove a claim of sexual harassment does not constitute proof of a false and / or malicious accusation.</p>
							
							
								<h3>PROHIBITION OF VICTIMISATION</h3>
								
								<ol type="1">
									<li>No person shall be victimized for anything said or done in relation to any complaints or proceeding under this act.</li>
									<li>A person victimizes another person if the person subjects the other person or threatens to subject the other person to any detriment in connection with employment or recruitment or promotion because such person
										
										<ol type="1)">
											<li>Has brought proceedings under this Act against any person.</li>
											<li>The other person associates with the complainant.</li>
											<li>Has given evidence or information or produced a document, in connection with any proceedings under this Act.</li>
											<li>Has otherwise done anything in accordance with this Act in relation to any person.</li>
											<li>Has alleged that any person has contravened a provision of this act</li>
										
										</ol>
									
									</li>
								
								</ol>
							
								
								<h3>CONDUCTING OF ENQUIRY</h3>
								<p>Where no Dispute Resolution process has been requested by the aggrieved woman, or if requested and carried out, has not been successful, the Women Cell shall within a period of two weeks from the completion of any mediation process held, or if not held, within two weeks of its receipt of the complaint, proceed to conduct a full enquiry into the allegation of sexual harassment, in accordance with the rules and regulations governing misconduct for KSIT and as per the procedures set out below.</p>
								<p>Both the complainant and the alleged harasser will be interviewed, as will any individuals who may be able to provide relevant information. All information will be kept in confidence. The employer / KSIT shall hand over to the convenor of the Committee a copy of the charge sheet issued to the defendant and reply/explanation (if any) of the defendant prior to the commencement of the enquiry, and the Committee shall hand over copies of the same to the complainant by hand delivery duly acknowledged or by Regd. A/D post within 3 days of its receipt of the same and prior to the commencement of the enquiry;</p>
								<p>The Committee shall give 7 days notice by hand delivery duly acknowledged or by regd A, D Post to the complainant and the defendant to appear for the first date of the enquiry which shall be specified.</p>
								<p>The notice shall state that the complainant and defendant shall be given an opportunity of producing evidence, examining witnesses etc if any. At the commencement of the enquiry the committee shall explain to both the complainant and defendant the procedure which will be followed in the enquiry.</p>
								<p>The Committee shall see that every reasonable opportunity is extended to the complainant and to the defendant, for putting forward and defending their case. The venue of the enquiry should be as per the convenience of the complainant.</p>



								<h3>ENQUIRY TO BE COMPLETED WITHIN 90 DAYS</h3>
								<p>Notwithstanding anything contained in any law for the time being in force an enquiry under this chapter shall be completed, including the submission of the Enquiry Report, within a period of 90 days from the date on which the enquiry is commenced. Any delay in completion shall be done for reasons given in writing.</p>
								<br/>
								<p>(Thanks to http://leprasociety.org/pdf/EHB_SH.pdf for sharing their documents related to handling of sexual harassment cases)</p>
							
								</div>
						
						<!--end content-->
							
							
							
							
						
						</div>
                    </div>                    
                  </li>                         
                  </ul>
                  
                </div>               
              </div>
			  
			  
              <!-- Start events tab content -->
              <div class="tab-pane fade " id="events">
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							
						
							<div class="col-lg-12 col-md-12 col-sm-12">
								
								<p>Enacting the tobacco free policy will greatly improve the campus atmosphere and create a healthier environment and lifestyle for all the members. KSIT has taken utmost care to avoid tobacco consumption in the campus and hostels.</p>
								
								<p>The committee has been formed to make the campus tobacco free.</p>
								
								
								
							
							</div>
						
						</div>
                    </div>                    
                  </li>      
                </ul>
                
              </div>
			  
			  
			  
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>
    <!--=========== END ABOUT US SECTION ================-->
</@page>