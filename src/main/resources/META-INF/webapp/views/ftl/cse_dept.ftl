<@page>
<#include "feedback/feedback_list_modal.ftl">
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#if csePageSliderImageList?has_content>
                     		<#list csePageSliderImageList as csePageSliderImage>
                     			<#if csePageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${csePageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">${csePageSliderImage.heading}</p>
										<p class="slideInRight">${csePageSliderImage.content}</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>
	                        <article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/cse/slider/s11.jpg" alt="">                           
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/cse/slider/s12.jpg" alt="">										
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/cse/slider/s13.jpg" alt="">
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/cse/slider/s14.jpg" alt="">
	                        </article>
						</#if>
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                        <li data-target="#myCarousel" data-slide-to="6"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 



<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 



    <!-- upcoming and latest news list-->
      
	<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at CSE
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
					
				<div class="col-md-12">
					<div class="row">
					
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=csePageLatestEventsList/>
									</div>
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
								<@eventDisplayMacro eventList=csePageUpcomingEventsList/>		

								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->
    
   

<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To Computer Science And Engineering</h1>
   <p class="welcome-text">Preparing students to find Computer Solutions for the society through research and entrepreneurship with professional ethics.</p>
</div>
<!-- welcome -->



<!--dept tabs -->
<section id="vertical-tabs">
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
         <div class="vertical-tab padding" role="tabpanel">
          
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
               <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="dept-cart">Profile</a></li>
               <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision & Mission, PEO, PSO & PO</a></li>
               
                            
            <!--   <li role="presentation"><a href="#csd" aria-controls="csd" role="tab" data-toggle="tab" class="dept-cart">Computer Science and Design</a></li> 
               <li role="presentation"><a href="#cce" aria-controls="cce" role="tab" data-toggle="tab" class="dept-cart">Computers and Communication Engineering</a></li>
               <li role="presentation"><a href="#csiot" aria-controls="csiot" role="tab" data-toggle="tab" class="dept-cart">Computer Science(IoT, Cyber Security including Block Chain)</a></li> -->
               
               <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>
               
               <li role="presentation"><a href="#professional_bodies" aria-controls="professional_bodies" role="tab" data-toggle="tab" class="dept-cart">Professional Bodies</a></li>
               <li role="presentation"><a href="#professional_links" aria-controls="professional_links" role="tab" data-toggle="tab" class="dept-cart">Professional Links</a></li>
               <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
			   <li role="presentation"><a href="#pedagogy" aria-controls="pedagogy" role="tab" data-toggle="tab" class="dept-cart">Pedagogy</a></li>
               <li role="presentation"><a href="#content-beyond-syllabus" aria-controls="content-beyond-syllabus" role="tab" data-toggle="tab" class="dept-cart">Content Beyond Syllabus</a></li>
               
               <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Time Table & Calendar of Events</a></li>
               
               <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>
               
               
               
               <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li>
               <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
              
               <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li>
              
              
               <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab" class="dept-cart">Achievers</a></li>
               <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
               <li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
               <li role="presentation"><a href="#library_dept" aria-controls="library" role="tab" data-toggle="tab" class="dept-cart">Departmental Library</a></li>
               <li role="presentation"><a href="#placement_dept" aria-controls="placement" role="tab" data-toggle="tab" class="dept-cart">Placement Details</a></li>
               
               <li role="presentation"><a href="#higher_education" aria-controls="higher_education" role="tab" data-toggle="tab" class="dept-cart">Higher Education & entrepreneurship</a></li>
               
               <li role="presentation"><a href="#student_club" aria-controls="student_club" role="tab" data-toggle="tab" class="dept-cart">Student Club</a></li>
               
               
               <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab" class="dept-cart">Internships & Projects </a></li>
               
               <li role="presentation"><a href="#industrial_visit" aria-controls="industrial_visit" role="tab" data-toggle="tab" class="dept-cart">Industrial Visit</a></li>
             
               
               <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
               <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
               
               <li role="presentation"><a href="#project_exhibition" aria-controls="project_exhibition" role="tab" data-toggle="tab" class="dept-cart">Project Exhibition</a></li>
               <li role="presentation"><a href="#mou-signed" aria-controls="mou-signed" role="tab" data-toggle="tab" class="dept-cart">MOUs Signed</a></li>
               <li role="presentation"><a href="#alumni-association" aria-controls="alumni-association" role="tab" data-toggle="tab" class="dept-cart">Alumni Association</a></li>
               <li role="presentation"><a href="#nba" aria-controls="nba" role="tab" data-toggle="tab" class="dept-cart">NBA</a></li>
                                          
               <li role="presentation"><a href="#social_activities" aria-controls="social_activities" role="tab" data-toggle="tab" class="dept-cart">Social Activities</a></li>
               <li role="presentation"><a href="#other_details" aria-controls="other_details" role="tab" data-toggle="tab" class="dept-cart">Other Details</a></li>
               <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="tab" class="dept-cart">Testimonials</a></li>
                             
            
               <!--<li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>-->
            </ul>
            <!-- Nav tabs -->
            
            <!-- Tab panes -->
            <div class="tab-content tabs">
            	
               <!-- profile -->
               <div role="tabpanel" class="tab-pane fade in active" id="profile">
                  <div class="row">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Profile</h3>
                  <p>The Department of <a href="https://ksgi.edu.in/Best-College-For-Computer-Science-and-Engineering" class="font-weight-600" target="_blank">Computer Science and Engineering</a> is established in the year 1999.</p> 
						<p>The current intake of UG program is 180, PG with 18 and department has a PhD Program. The main focus of the department is to produce graduate, post graduates and PhDs with the expertise in Computer Science and Engineer domain. The outcome-based education helps students to achieve this goal. The college is accredited by National Assessment and Accreditation Council (NAAC). The Department of Computer Science and Engineering is accredited by NBA (2021-2024). The Department offers various programs namely Bachelor of Engineering (B.E), Master of Technology (M.Tech) in CSE and PhD. The department has received grants from various prestigious funding agencies like VGST (KFIST Level-I), IEI, VTU and KSCST.</p>
						
						<p>The department has qualified, experienced & committed teaching faculty members with various specializations. Faculties with Ph.D. degree and those who are pursuing Ph.D. provide qualitative industry level education. Due to the quality teaching, the students of the department are performing well in the examination. Totally 3 ranks in UG and one rank in PG are secured by students at the University level. </p>
						
						<p>The Department has well equipped laboratory with state of art equipment and software tools. A computer center with over 150 computers caters to the student needs along with the strong R&D culture. Active professional bodies like CSI, BITES, ISTE, IEI and IEEE help to conduct Workshops, FDPs, STTPs, ATPs to comprehend and upgrade latest technologies. Department student club Firefox organizes technical and non-technical evets for the students to exhibit their team spirit. The biannual department newsletter “CS -Gyanbytes” disseminates the details of the events and achievements of the students and faculties.</p>
						
						<p>Activities such as technical conferences, student conventions, Hackathon, Coding Contests, Seminars and Workshops are carried out for the students to boost their technical skills and knowledge.</p>
						<p>The department student teams have won MHRD organized Smart India Hackathon (SIH) for the years 2016-17 (1 Prize), 2017-18 (2 Prizes), 2018-19(2 Prizes), 2022-23(3 Prizes). In 2020-21 two teams won Toycothan awards and in the year 2019-2020 one team secured 2nd runner up place in TCS EngiNX competition. In the year 2021-2022 one team secured second runner up in Aadhar hackathon. </p>
						
						<p>The Department has Memorandum of Understandings (MOUs) with various Organizations which supports internships and projects to develop skills and professional ethics required for IT Industry.</p>
                     
                  </div>
                  <h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <img class="hod-img img-responsive" src="${img_path!}/cse/cse-hod-new.jpg" alt="image" />	
                        <div class="qualification_info">
	                        <h3 class="qual-text">Dr. Rekha B Venkatapur</h3>
	                        <h4 class="qual-text">Professor & Head</h4>
	                        <h3 class="qual-text">B.E, M.Tech, Ph.D</h3>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>The Department of Computer Science & Engineering offers B.E and MTech in CSE. The department has well qualified, capable, Knowledgeable and Dedicated 
                           faculty to provide qualitative, Industry tuned education to students with Unreserved support from the Principal 
                           and HOD. The Department Infrastructure accomplishes and ropes learning skills on the latest technology in the industry. 
                           The students are shaped with multi-dimensional skills covering all the required graduate qualities. Our mission is 
                           to inculcate strong theoretical and practical knowledge for continuous learning, prepare students to find Computer 
                           Solutions for the society through research and entrepreneurship with Professional ethics, encourage 
                           team work in inter-disciplines and evolve as leaders with social concerns. Department staff and students are members 
                           of Professional bodies like ISTE, IEI and CSI where students learn to do project, present technical papers 
                           and work together which imbibes teamwork and leadership qualities. Department has Firefox club and GLUG to encourage 
                           students talent in programming, debugging. 
                        </p>
                     </div>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     	<p> Every year department releases its newsletter Highlighting academic, Co-curricular activities of staff and students. Various workshops on programming skills, carrier guidance, cloud, Big Data Analytics, Artificial Intelligence, IoT and Networks remain active throughout the year to enhance the
                           skills of staff and students.  Our department has several MOU with industries, R&D and training centers to give guidance, assistance and internships facilities for UG and PG students as well as collaborative research for PhD students.
                        </p>                    
                        
                     </div>
                  </div>
                  <div class="aboutus_area wow fadeInLeft">
                  	<h3>Highlights of department</h3>
                  	<ul class="list">
                  		<li>Young, dynamic and energetic faculty.</li>
                  		<li>State of the art facilities for carrying out research in Computer Science & Engineering and allied fields.</li>
                  		<li>Excellent facilities to faculty like latest computer, laptop, laser printer, scanner, high speed internet connection etc.</li>
                  		<li>Excellent infrastructure: building, space, class-rooms, seating arrangements, etc.</li>
                  		<li>Adequate teaching aids like black/white boards, LCD projectors, etc.</li>
                  		<li>Excellent results.</li>
                  		<li>Students highly sought by top class industries with good emoluments.</li>
                  		<li>Excellent facilities for imparting training/courses to engineering & other faculty, students, community, etc.</li>
                  		<li>Involvement of faculty members in institutional development work (automation, video conferencing, networking, etc.).</li>
                  		<li>Transparency in academic and administrative processes.</li>
                  		<li>Excellent teaching learning environment got 3 VTU ranks.</li>
                  		<li>The department student teams have won MHRD organized Smart 
                  		   India Hackathon (SIH) for 3 consecutive years</li>
                  	</ul>
                  
                 
                  </div>
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Academic programs offered:</h3>
                     <h4>UG Program:</h4>
                     <p>Bachelor of Engineering (B.E.) in Computer Science and Engineering (CSE) with
                        approved intake of 120 students and a lateral entry of 20% from 2nd year.
                     </p>
                     <h4>PG Program:</h4>
                     <p>M.Tech Programme in Computer Science and Engineering with approved intake of 18 students</p>
                     <h4>RESEARCH PROGRAM:</h4>
                     <p>Ph.D</p>
                  </div>
                  </div>
               </div>
               <!-- profile -->
               
               <!-- vision and mission -->
               <div role="tabpanel" class="tab-pane fade in" id="vision">
               	 <div class="row">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Vision</h3>
                     <ul class="list">
                        <li>To create competent professionals in Computer Science and Engineering with adequate skills to drive the IT industry</li>
                     </ul>
					<button type="button" style="float:right;" class="btn btn-info btn-sm" data-backgrop="static" data-toggle="modal" data-page="cse_page" data-target="#feedbackListModal">Feedback</button>               
                     <h3>Mission</h3>
                     <ul class="list">
                        <li>  Impart sound technical knowledge and quest for continuous learning.</li>
                        <li>	To equip students to furnish Computer Applications for the society through 
                           experiential learning and research with professional ethics.
                        </li>
                        <li>Encourage team work through inter-disciplinary project and evolve as leaders with social concerns.</li>
                     </ul>
                     
                    
                     <h3>PROGRAM EDUCATIONAL OBJECTIVES (PEOs)</h3>
                     <p>PEO1:   Excel  in  professional career by acquiring  knowledge in cutting  edge 
                        Technology  and  contribute to the society as an excellent employee or
                        as  an entrepreneur  in  the field of   Computer Science &  Engineering.
                     </p>
                     <p>PEO2:  Continuously enhance their knowledge on par with the  development in
                        IT industry and pursue higher studies in computer science &engineering.
                     </p>
                     <p>PEO3 :  Exhibit  professionalism,  cultural  awareness,  team work,  ethics,
                        and  effective communication  skills with their knowledge in solving 
                        social and environmental problems by applying computer technology.
                     </p>
                     <h3>Programme Specific Outcomes(PSOs)</h3>
                     <p>PSO1 :  Ability to understand, analyze problems and  implement solutions in 
                        Programming  languages, as well  to apply  concepts in core areas of Computer Science in association  with
                        professional bodies and clubs.
                     </p>
                     <p>PSO2 :  Ability to use computational skills and  apply software knowledge to
                        develop effective solutions and data to  address real world challenges. 
                     </p>

                      <h3>PROGRAM OUTCOMES</h3>
					 <p>1. Engineering knowledge: Apply the knowledge of mathematics, science, engineering fundamentals, and engg. specialization to the solution of complex engineering problems.</p>
					 <p>2. Problem analysis: Identify, formulate, research literature, and analyze engineering problems to arrive at substantiated conclusions using first principles of mathematics, natural, and engineering sciences. </p>
					 <p>3. Design/development of solutions: Design solutions for complex engineering problems and design system components, processes to meet the specifications with consideration for the public health and safety, and the cultural, societal, and environmental considerations.</p>
					 <p>4. Conduct investigations of complex problems: Use research-based knowledge including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions.</p>
					 <p>5. Modern tool usage: Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations.</p>
					 <p>6. The engineer and society: Apply reasoning informed by the contextual knowledge to assess societal, health, safety, legal, and cultural issues and the consequent responsibilities relevant to the professional engineering practice.</p>
					 <p>7. Environment and sustainability: Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development.</p>
					 <p>8. Ethics: Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice.</p>
					 <p>9. Individual and team work: Function effectively as an individual, and as a member or leader in teams, and in multidisciplinary settings. </p>
					 <p>10. Communication: Communicate effectively with the engineering community and with society at large. Be able to comprehend and write effective reports documentation. Make effective presentations, and give and receive clear instructions.</p>
					 <p>11. Project management and finance: Demonstrate knowledge and understanding of engineering and management principles and apply these to one�s own work, as a member and leader in a team. Manage projects in multidisciplinary environments.</p>
					 <p>12. Life-long learning: Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change.</p>
					
                  </div>
                  
                  </div>                  
               </div>
               <!-- vision and mission -->
                              
               <!-- Research and development -->
               <div role="tabpanel" class="tab-pane fade" id="research">               
                  <div class="row">
                  <h3>Research and Development</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <p>Computer Science & Engineering Research and Development Center is established in the year 2014-15. The Management of the institution has supported financially to start the research activities. R&D is established to motivate Faculty and students to participate in various technical events such as conferences, technical quizzes and workshops to enhance their technical and soft skills. The CSE Department R & D center has 1 Doctorate guiding students towards Ph.D. in various areas. There are 03 research scholars pursuing their research in this center. We have dedicated R & D LAB, IEEE Access and Library facilities for all research scholars and research students. R & D Center has well qualified and experienced team of faculties and students involved in various research activities. The knowledge is shared and transfered to the industry through teaching, paper publications, collaboration, entrepreneurship, etc. By integrating engineering disciplines into one R&D department, we can address major challenges and develop complete solutions, serving as an international hub for engineering excellence. Faculty and students are involved in research activities and setup CLOUD in the R&D centre. </p>
                        <h2>Funding Projects @ KSIT, R & D </h2>
                        <h3>VGST-K-FIST </h3>
                        <p>Financial grant of Rs.20.00 lakhs has been sanctioned for the Project titled Hybrid Real Time Intrusion Detection System under high Infrastructure Strengthening in Science and Technology (K-FIST)  Level  I scheme by VGST for the year 2013-14 under the guidance of Dr.Naveen N C.   </p>
                        <h3>IEI </h3>
                        <p>Financial grant of Rs. 15,000/- has been sanctioned for the Student Project Titled Secure and precise data in a cloud for rural health care monitoring with mobility, for the year 2014-2015 under the guidance of Dr. N C Naveen. </p>
                        <h3>KSCST</h3>
                        <p>Financial grant has been sanctioned for the Student Project titled Video Stitching and Object tracking , by KSCST 39th Series SPP for the year 2015-16 under the Guidance of Rekha B Venkatapur. </p>
                        <p>Financial grant has been sanctioned for the Student Project titled Predictive Analysis of diabetes using classification Techniques, by KSCST 39th series SPP for the Year 2015-2016  under the guidance of  Pradeep K R,. </p>
                        <p>Financial grant has been sanctioned for the Student Project titled A PERSONALIZED MOBILE SERACH ENGINE (PMSE) , by KSCST, under UG student project proposal for the year 2013 2014 under the guidance of Mrs. NAGA MANJULA RANI. </p>
                        
                       
                        <h3>R & D Activities</h3>
                        
                        <#list cseResearchList as cseResearch>
                        	<p>${cseResearch.heading!}  <a href="${cseResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        <p>Every year the department encourages faculty & students to apply for project funding agencies like AICTE, 
                        MHRD, VGST, KSCST, IEI and VTU FOSS, etc. The department has received consultancy from funding agency such 
                        as upGrad Education and One97 Communication Limited (Paytm).</p>
                        
                        <#list cseSponsoredProjectsList as cseSponsoredProjects>
                        	<p>${cseSponsoredProjects.heading!}  <a href="${cseSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        
                        <#list csePhdPursuingList as csePhdPursuing>
                        	<p>${csePhdPursuing.heading!}  <a href="${csePhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        
                        <h3>Ph.D Awardees</h3>
                        
                         <#list csePhdAwardeesList as csePhdAwardees>
                        	<p>${csePhdAwardees.heading!}  <a href="${csePhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                       

                        <!--<p><a href="${img_path!}/cse/cse_links/Calender of Events Odd Sem 2017-18.jpg" target="_blank">Calender of Events Odd Sem 2017-18</a></p>
                        <p><a href="${img_path!}/cse/cse_links/About CSE R& D.pdf" target="_blank">About CSE R& D</a></p>
                        <p><a href="${img_path!}/cse/cse_links/KSIT R&D research scholar details -2017-May.pdf" target="_blank">KSIT R&D research scholar details -2017-May</a></p>
                        <p><a href="${img_path!}/cse/cse_links/FACULTY PUBLICATION LIST.pdf" target="_blank">FACULTY PUBLICATION LIST</a></p>
                        <p><a href="${img_path!}/cse/cse_links/Student publication list.pdf" target="_blank">Student publication list</a></p>
                        <p><a href="${img_path!}/cse/cse_links/LIST OF STAFF PURSUING PH.D- may2017.pdf" target="_blank">LIST OF STAFF PURSUING PH.D- may2017</a></p>
                        <p><a href="${img_path!}/cse/cse_links/IEI , ISTE & CSI.docx" target="_blank">IEI , ISTE & CSI</a></p>
                        <p><a href="${img_path!}/cse/cse_links/funds-received-applied-till2016.pdf" target="_blank">funds-received-applied-till2016</a></p>-->
                     </div>
                  </div>
                  </div>
               </div>
               <!-- Research and development -->
               
               <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="row">
                     <h3>Time Table</h3>
                     <p> A planned schedule to provide students a healthy window for preparations, which includes Learners Teachers Rooms Time slots.</p>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list cseClassTimeTableList as cseClassTimeTable>              
                        <p>${cseClassTimeTable.heading!}  <a href="${cseClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list cseTestTimeTableList as cseTestTimeTable>
                        <p>${cseTestTimeTable.heading!}  <a href="${cseTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list cseCalanderList as cseCalander>              
                        <p>${cseCalander.heading!}  <a href="${cseCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--Time Table  -->
               
                <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>UG (B.E in CSE)</h3>
                        
                        <#list cseUgSyllabusList as cseUgSyllabus>
                        <p>${cseUgSyllabus.heading!}  <a href="${cseUgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                         <h3>PG (M.Tech in CSE)</h3>
                        
                        <#list csePgSyllabusList as csePgSyllabus>
                        <p>${csePgSyllabus.heading!}  <a href="${csePgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
              
               
               
               <!--News Letter  -->
               <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>News letter</h3>
                        <p>CS-GYANBYTES is the technical magazine which publishes details about paper publications, 
                        100% attendance results, Guest lectures given by the faculties outside and guest faculty being conducted 
                        within the department, Events attended by the faculties such as FDPs, Workshops, and Seminars, achievements
                         by the students and staffs.</p>
                        <#list cseNewsLetterList as cseNewsLetter>
                        <p>${cseNewsLetter.heading!}  <a href="${cseNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--News Letter  -->
               
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="row">
                     <h3>Faculty</h3>
                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Faculty</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id!}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               

				  <!--testimonials  -->
               <div role="tabpanel" class="tab-pane fade" id="testimonials">
                  <!--slider start-->
                  <div class="row">
                     <div class="col-sm-8">
                        <h3>Testimonials</h3>
                        <p>Heartfelt words from our students.</p>
                        <!--<div class="seprator"></div>-->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          
                           <!-- Wrapper for slides -->
                           <div class="carousel-inner">
                              <#list testimonialList as testimonial>
                              <div class="item <#if testimonial?is_first>active</#if>">
                                 <div class="row" style="padding: 20px">
                                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                    <p class="testimonial_para">${testimonial.content!}</p>
                                    <br>
                                    <div class="row">
                                       <div class="col-sm-2">
                                          <img src="${testimonial.imageURL!}" class="img-responsive" style="width: 80px">
                                       </div>
                                       <div class="col-sm-10">
                                          <h4><strong>${testimonial.heading!}</strong></h4>
                                          <!--<p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                             <span>Officeal All Star Cafe</span>-->
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </#list>  
                           </div>
                           <div class="controls testimonial_control pull-right">									            	
                              <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--slider end -->
               </div>
               <!--testimonials  -->
               
                   <!-- Achievers -->
               <div role="tabpanel" class="tab-pane fade" id="achievers">
               	<div class="row">
               		 <h3>Department Achievers</h3>
                     <!--Department Achievers-->
		                  <#list cseDepartmentAchieversList as cseDepartmentAchievers>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cseDepartmentAchievers.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${cseDepartmentAchievers.heading!}</h3>	                      	 
			                       	 <p>${cseDepartmentAchievers.eventDate!} </p>
			                       	 <p> ${cseDepartmentAchievers.content!}</p>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
		                    
		              <h3>List of FCD Students</h3>
		              <#list cseFcdStudentsList as cseFcdStudents>
                        <p>${cseFcdStudents.heading!}  <a href="${cseFcdStudents.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                      </#list>
                    
               	</div>
               </div>
               <!-- Achievers -->


             
               
               
               <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="row">
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
                     <p>Computer Science and Engineering Department has fully furnished State of Art laboratories.
                      All laboratories are well-equipped to run the academic laboratories courses. Highly experienced Technical 
                      staff are available in all the laboratories to assist the students to carry out their lab sessions.</p>
                      
                      <p>The Department has</p>
                      <ul class="list">
                      	<li>Dedicated six laboratories, all are well-equipped with latest computers installed with required software
                      	 for a students to carry out lab sessions, LAN connectivity, WiFi and uninterrupted power supply. Projectors 
                      	 are available for effective explanation of concepts and programs related to academic laboratories courses,
                      	  technical seminar presentation, internship presentation and project phase reviews. Analog and Digital 
                      	  Electronics Laboratory/Microcontroller and Embedded Systems Laboratory And System Software and Operating
                      	   System Laboratory equipments like CRO, IC trainer kits, ARM7 LPC 2148 Microcontroller kit, ALS - SDA - ARM7 
                      	   are available.</li>
                      	<li>Project labs in the department are available for students to carry out academic projects, mini 
                      	projects and to conduct any other organizational events like CSI, Sentinal Hack.</li>
                      	<li>Research and Development lab is well-equipped with latest computers, laptops, servers,
                      	 Network storage, interactive panel, Barcode Scanner, softwares like Anaconda, MongoDB, Wireshark, 
                      	 JAVA, Oracle etc. It is utilized by research scholars, students    to carry out academic projects, 
                      	 project for various competitions like Smart India Hackathon, assignment projects, Hobby projects etc.
                      	  For the benefit of the students carrying out the project works, a small library 109 books, 461
                      	   reference journal are made available. Further, project reports of few years (departmental copy)
                      	    are also placed in this library. The lab is well equipped with LAN connectivity, WiFi and uninterrupted
                      	     power supply. Dedicated projectors are available for use. With enough output power points, so that the 
                      	     students can bring their own computing devices to work hassle free. </li>
                      	<li>Seven class rooms with a seating capacity of seventy and one fully furnished Seminar hall with seating capacity of 120.</li>
                      </ul>
                      
                      
                      
		                  <#list cseLabList as cseLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cseLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${cseLab.name!}</h3>	                      	 
			                       	 <p>${cseLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${cseLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
                <!--phd pursuing  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdPursuing">
               	<div class="row">
               		 <h3>Ph.D. Pursuing</h3>
		                  <#list csePhdPursuingList as csePhdPursuing>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csePhdPursuing.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${csePhdPursuing.name!}</h3>	                      	 
			                       	 <p>Description: ${csePhdPursuing.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csePhdPursuing.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd pursuing -->
               
                 <!--phd awardees  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdAwardees">
               	<div class="row">
               		 <h3>Ph.D. Awardees</h3>
		                  <#list csePhdAwardeesList as csePhdAwardees>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csePhdAwardees.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${csePhdAwardees.name!}</h3>	                      	 
			                       	 <p>Description: ${csePhdAwardees.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csePhdAwardees.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd awardees -->
               
               <!-- Gallery  -->
               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
               	<div class="row">
                  <h3>Gallery</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="gallerySLide" class="gallery_area">
                           <#list cseGalleryList as cseGallery>	
                           <a href="${cseGallery.imageURL!}" target="_blank">
                           <img class="gallery_img" src="${cseGallery.imageURL!}" alt="" />					                    			                   
                           <span class="view_btn">${cseGallery.heading!}</span>
                           </a>
                           </#list>
                        </div>
                     </div>
                  </div>
                  </div>
               </div>
               <!-- Gallery -->
               
               <!-- Dept Library  -->
               <div role="tabpanel" class="tab-pane fade" id="library_dept">
                 <div class="row">
                  <h3>Departmental Library</h3>
                  <p>Libraries are established for the systematic collection, organization, preservation and dissemination of knowledge and information. It is very important for person to preserve and maintain the valuable knowledge and information contained in the books and documents because we want to preserve our knowledge and wisdom for the coming generations. By preserving the documents in a library this knowledge can be made available to others so that they can benefit from it.</p>
					<p>Libraries play a vital role in the development of any society by enhancing the cause of education and academic research. They cater to the information needs of thousands of peoples.</p>

                  
                  <h3>Collection Of books</h3>
                  <table class="table table-striped course_table">
                     <#list cseLibraryBooksList as libraryBooks>
                     <tr>
                        <th style="width:50%">${libraryBooks.heading!}</th>
                        <td>${libraryBooks.content!}</td>
                     </tr>
                     </#list>
                  </table>
                  </div>
               </div>
               <!--  Dept Library   -->
               
               <!-- Department placements -->
               <div role="tabpanel" class="tab-pane fade" id="placement_dept">
                <div class="row">
                  <h3>Placement Details</h3>
                   <p>The purpose of the placement and training is to guide students to choose right career and to give knowledge,
                    skill, and aptitude and meet the manpower requirements of the Industry. The Placement and Training department
                     conducts various training activities like Campus Induction for the First Year Students, Soft skill training 
                     for the second, third and fourth year students to help students grow their technical skills. Also the campus 
                     induction programs to make students prepared for the company interviews. The placement department in 
                     assistance with the department of CSE faculties coordinates and provides logistic supports to the companies
                      while visiting the college. The outcome of this brings students play a major role in in shaping up the career 
                      goals of students.</p>
                  
                  <#list csePlacementList as csePlacement>              
                  <p>${csePlacement.heading!}  <a href="${csePlacement.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
                  <!-- Department placements -->
                  
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                    <div class="row">
                    <h3>Events</h3>
                    
                    <#list cseEventsList[0..*3] as cseEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${cseEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${cseEvent.heading!}</h3>	                      	 
	                       	 <p>"${cseEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${cseEvent.eventDate!}  </span></p>
	                       	 <a class="btn btn-primary" href="cse_events.html">View more</a>
	                       	 <#if (cseEvent.reportURL)??>
	                       	 	<a class="title-red" href="${cseEvent.reportURL!}" target="_blank">Report</a>
	                       	 </#if>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
  						
                    </div>    
                  </div>
                  <!-- Events  -->
                  
               <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        <p>A large number of Electronic Resources are available for all the subjects. </p>
                        
                          <#list cseEresourcesList as cseEresources>
	                    
	                        <p>${cseEresources.heading!}  
	                      	    <a href="//${cseEresources.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                     <p>All the users of library resources of KSIT must adhere to copyright laws of publishers and consortia.</p> 

						<p>No copyrighted work may be copied, published, disseminated, displayed, performed, without permission of the copyright holder except in accordance with fair use or licensed agreement. This includes DVDs/CDs and other copyrighted material. Using these resources for commercial purpose, systematic downloading, copying or distributing of information is prohibited. KSIT may terminate the online access of users who are found to have infringed copyright.</p>
	                     
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
                <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Teaching And Learning</h3>

						<p>The faculty of CSE department attends various MOOCS and training programs on advanced topics, update
						 their knowledge and skills, and gives additional inputs in the classes. Further, the faculty conducts 
						 various innovative teaching and learning activities inside and outside the classrooms to engage the students
						  effectively and efficiently. The teaching and learning activities conducted by the faculty for the 
						  improvement of student learning includes: </p>
						  <ul class="list">
						  	<li>Teaching with working models, simulations and animated videos</li>
						  	<li>Assignments which include Miniprojects and Case studies</li>
						  	<li>Conduction of online and classroom quizzes, surprise class tests, open book assignments,  
     						 group discussions, seminars etc.</li>
						  	<li>Usage of ICT, Pupilpod and Google classrooms for posting assignments and lecture materials</li>
						  	<li>Usage of Google forms and Kahoot for online interaction, assessment and evaluation</li>
						  
						  </ul>

						<p>The instructional materials and pedagogical activities are uploaded in Google drive and the associated 
						links are made available on institutional website for student and faculty access. </p>
						

                     
                        <h3>Instructional Materials</h3>
                        
                        <p>Instructional Materials are made available for public access.</p>
                        
                          <#list cseInstructionalMaterialsList as cseInstructionalMaterials>
	                    
	                        <p>${cseInstructionalMaterials.heading!}  
	                      	    <a href="//${cseInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                        
                          <h3>Lab Manuals</h3>
                        
                          <#list cseLabManualList as cseLabManual>
	                    
	                        <p>${cseLabManual.heading!}  
	                      	    <a href="//${cseLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                    
                       											
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
               <!-- Pedagogy-->              
               <div role="tabpanel" class="tab-pane fade" id="pedagogy">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
					  <h3>Pedagogical Activities</h3>
                      <p>Pedagogy is the method and practice of teaching an academic subject or theoretical concept. 
                      Pedagogical skills involves being able to convey knowledge and skills in ways that students can understand, 
                      remember and apply. Pedagogies involve a range of techniques, including whole-class and structured group
                       work, guided learning and individual activity. Pedagogies focus on developing higher order thinking.</p>
                       
                         <#list csePedagogicalActivitiesList as csePedagogicalActivities>
                    
                        <p>${csePedagogicalActivities.heading!}  
                      	    <a href="//${csePedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
                        </p>
                     
                      </#list>
                     
                      <h3>Pedagogy Review Form</h3>
                      <p>The Peer review forms associated with pedagogical activities are made available for public access,
                       peer review, critique and further development. </p>
                       
                         <#list csePedagogyReviewFormList as csePedagogyReviewForm>
                    
                        <p>${csePedagogyReviewForm.heading!}  
                      	    <a href="//${csePedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
                        </p>
                     
                     </#list>					
                     </div>
                  </div>
               </div>

                <!-- content beyond syllabus -->

                <div role="tabpanel" class="tab-pane fade" id="content-beyond-syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Content Beyond Syllabus</h3>						
                        
                         <#list csecontentbeyondsyllabusList as csecontentbeyondsyllabus>
                        <p>${csecontentbeyondsyllabus.heading!}  <a href="${csecontentbeyondsyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	                 
                       											
                     </div>
                  </div>
               </div>
              
                <!-- content beyond syllabus -->
               
                 <!-- Project Exhibition -->
               <!--<div role="tabpanel" class="tab-pane fade" id="sponsored-projects">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
		                  <#list cseSponsoredProjectsList as cseSponsoredProjects>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cseSponsoredProjects.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${cseSponsoredProjects.name!}</h3>	                      	 
			                       	 <p>Description: ${cseSponsoredProjects.designation!}</p>
			                       	 <a class="btn btn-primary" href="${cseSponsoredProjects.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
                 <!-- Project Exhibition -->
               
               <!-- MOU's signed -->
               <div role="tabpanel" class="tab-pane fade" id="mou-signed">
                <div class="row">
                  <h3>MOUs Signed</h3>
                  
                 <p> The Memorandum of Understanding details  modalities and general conditions regarding collaboration between
                  the INDUSTRY and KSIT. Also facilititate for enhancing and encouraging   students for interactions between
                   the Industry Experts, Scientists, Research fellows and Faculty members  for knowledge sharing with the following
                    objectives:-</p>
                    
                    <ul class="list">
                    	<li>Collaboration in conduction of conferences, workshops, seminars and hackthons.</li>
                    	<li>Provides industry exposure to KSIT students.</li>
                    	<li>Mutually agreed guidance for student's projects and scholar's reserarch works through  internship and industrial training.</li>
                    	<li>Provision given for the usage of institute infrastructure by industry like library, internet and computational facilities etc.</li>
                    </ul>
         
                  
                  <#list cseMouSignedList as cseMouSigned>              
                  <p>${cseMouSigned.heading!}  <a href="${cseMouSigned.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- MOU's signed -->
               
                <!-- Alumni Association -->
               <div role="tabpanel" class="tab-pane fade" id="alumni-association">
                <div class="row">
                  <h3>Alumni Association</h3>
                  <p>An alumni association is an association of graduates or, more broadly, former students of the college.the purpose of association is to foster a spirit of laws and to promote the general welfare of the institution.</p>
					<p>Associations exist to support the institution's goal, and strengthen the ties between alumini, the community and the institution.
                  </p>
                  <#list cseAlumniAssociationList as cseAlumniAssociation>              
                  <p>${cseAlumniAssociation.heading!}  <a href="${cseAlumniAssociation.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- Alumni Association -->
               
               
               <!--  nba -->
                <div role="tabpanel" class="tab-pane fade" id="nba">
	                <div class="row">
						<div class="col-lg-6">
							<h3>NBA Links</h3>
							<table style="width: 100%">
								<#list criteriaMap?values as nbaList>
									<tr>
										<td>
											<ul>
												<li>
													${nbaList[0].criteria.desc}
												</li>
												<li>
													<ul>
														<#list nbaList as  nba>
															<li style="padding: 20px"> - ${nba.heading} <a href="${nba.link}" target="_blank">view here</a></li>												
														</#list>
													</ul>
												</li>
											</ul>
										</td>
									</tr>
								</#list>
							</table>
						</div>	
	                </div>
                </div>								
              <!--  nba --> 
               
                <!-- Professional Bodies  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_bodies">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Bodies</h3>
                        <p>Department of Computer Science and engineering of KSIT has various professional bodies to help students to
                         take-up innovative projects, present technical papers and organize various technical events thereby receiving a
                          broader perspective on the professional front by providing opportunities for teamwork, leadership and service.</p>
                          
                          <ul class="list">
                          	<li><strong>Computer Society of India (CSI)</strong> is the first and largest body of computer professionals in India. It was started on 6 March 1965 by a few computer professionals and has now grown to be the national body representing computer professionals. The college has CSI Academic membership since 2014 and KSIT CSI Student branch was inaugurated in the year 2015. The 3rd year and 4th year Undergraduate students of CSE Department are the members of CSI. Workshops, Seminars, Guest Lectures, Student Convention, MiniProject Exhibitions are regularly conducted for students under CSI Student chapter. The event details are published regularly in CSI Communications and also in college magazine. Faculty Development Programs and workshops are also conducted for the faculties under CSI chapter.</li>
                          	<li><strong>ISTE</strong> formulates & generates goals & responsibilities of technical education. Seminars, workshops, conferences are organized on the topic of relevance to technical education for engineering students as well as teachers.</li>
                          	<li><strong>The Institution of Engineers (IEI)</strong> is the national organization of engineers in India. IEI in KSIT promotes an environment to enable ethical pursuits for professional excellence.</li>
                          	<li><strong>The Board for IT Education Standards (BITES)</strong> is an autonomous body and a non-profit society promoted by the Government of Karnataka, in association with educational institutions and IT Industries in Karnataka, in order to enhance the quality of IT education and help build quality manpower for the IT industry. Set up in June 2000, BITES is an ISO 9001:2015 certified organization.</li>
                          	<li><strong>ALUMNI</strong> K.S. Institute of Technology has shaped many educationists, software engineers, entrepreneurs, teachers etc. The Alumni Association brings all these people together on a single platform and develops synergistic plans to aid and improve Institutional endeavours. KSIT works to strengthen ties between the Institutions and the alumni, so that the alumni can participate in various activities including cultural and social activities. The Alumni Association conducts regular meetings wherein the members freely and closely interact with each other on issues pertaining to development of the college and also their role and contribution. Their ideas and suggestions are duly recognized and implemented by the college administration. The HODs also interact with the members of Alumni Association especially on the matters of curriculum design and development of value � added programs.</li>
                          </ul>
                          
                          
                          
                        <#list cseProfessionalBodiesList as cseProfessionalBodies>
                        <p>${cseProfessionalBodies.heading!}  <a href="${cseProfessionalBodies.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Professional Bodies   -->
           
           
           	<!-- Professional Links  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_links">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Links</h3>
                        
                       		    <h3>BITES</h3>
								<p>The Board for IT Education Standards is a non-profit Society set up by the Karnataka Government,
								 in association with IT industries and educational institutions, in order to enhance the quality of 
								 IT education and help build quality manpower for the IT industry. The institute is registered institutional
								  member of BITES since January 2004. Workshops, Seminars, Guest Lectures, are regularly conducted for 
								  students. Faculty development programs are conducted for faculties. The event details are published 
								  regularly in BITES Communications and also in college magazine.</p>
								
								 <h3>ISTE</h3>
								<p>The Indian Society for Technical Education (ISTE) is the leading National Professional non-profit
								 making Society for the Technical Education System. The institute established KSIT-ISTE Student Chapter
								  in March 2014. Various events such as student Convention, Faculty convention, Seminars, Guest Lectures 
								  are conducted under ISTE Chapter.</p>
								
								 <h3>IEI</h3>
								<p>The Institution of Engineers (India) [IEI] is a professional body to promote and advance the engineering
								 and technology. It was established in 1920.The institute established IEI Institute Life membership in Dec
								  2014. Various events such as Faculty development programs, workshops, Seminars, Guest Lectures are 
								  conducted under IEI.</p>
								                        
                        
                        
                         <#list cseProfessionalLinksList as cseProfessionalLinks>
	                    
	                        <p>${cseProfessionalLinks.heading!}  
	                      	    <a href="//${cseProfessionalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                												
                     </div>
                  </div>
               </div>
               <!-- Professional Links   -->
           
                 <!-- Higher Education  -->
               <div role="tabpanel" class="tab-pane fade" id="higher_education">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Higher Education & Entrepreneurship</h3>
                        <p>The department conduct higher studies awareness program in order to motivate the students to do higher
                         studies both within the country and abroad. The department in association with training and placement 
                         department helps students in understanding the importance of attempting various competitive exams like 
                         GATE, IELTS, GRE, TOEFL etc. </p>
                        <#list cseHigherEducationList as cseHigherEducation>
                        <p>${cseHigherEducation.heading!}  <a href="${cseHigherEducation.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Higher Education   -->
           
              
              
                 <!-- Students Club  -->
               <div role="tabpanel" class="tab-pane fade" id="student_club">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Fire Fox</h3>
                     
                     
                        <h3>Club Activities</h3>
                        
                        <p>Firefox was created in 2002, under the name �Phoenix� by the Mozilla community members who wanted a 
                        standalone browser rather than the Mozilla Application Suite bundle. Even during its beta phase, Firefox 
                        proved to be popular with its testers and was praised for its speed, security, and add-ons compared to
                         Microsoft's then - dominant Internet Explorer 6. Firefox was released in November 2004, and was highly
                          successful with 60 million downloads within nine months.</p>
                        
                        <#list cseClubActivitiesList as cseClubActivities>
                        <p>${cseClubActivities.heading!}  <a href="${cseClubActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>External Links (Club)</h3>	
                        
                         <#list cseClubExternalLinksList as cseClubExternalLinks>
	                    
	                        <p>${cseClubExternalLinks.heading!}  
	                      	    <a href="//${cseClubExternalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                        			
                        
                        							
                     </div>
                  </div>
               </div>
               <!-- Students Club   -->
           
               
                 <!-- Internship  -->
               <div role="tabpanel" class="tab-pane fade" id="internship">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Internships</h3>
                        <p>Internships are foundational in preparing students for the workforce and providing opportunities after graduation. Most employers seek career-ready college graduates who have been equipped with prior experiences and skills in a given field. An internship is important because it can present you with new skills and opportunities that student would not receive otherwise. Interns not only gain technical knowledge within the industry of their choice, but they also learn how to interact with professionals in a workplace setting, and develop essential soft skills like time management, organization, adaptability, problem-solving and teamwork. An internship is the best way to gain work experience as a student. Internships give you the valuable practical experience needed along with theoretical knowledge. Employers often note that there is a disconnect between the requirements of the industry and the academics. This gap can be bridged by an internship and pitch you as an ideal candidate.</p>
                        <#list cseInternshipList as cseInternship>
                        <p>${cseInternship.heading!}  <a href="${cseInternship.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Projects</h3>
                        <p>Final year project is one of the most important aspects of Engineering degree. Final year project will be the first exposure to the full rigour of Engineering practice. It is essential that students learn from this exposure and practise all of the engineering methodologies involved. It is particularly important that students learn not just to apply what they know, but to apply it with judgement.</p>
                        <#list cseProjectsList as cseProjects>
                        <p>${cseProjects.heading!}  <a href="${cseProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Mini-Projects</h3>
                        <p>Mini projects for engineering students give an edge over the race of recruitment to work hard to ensure a good career. By taking up mini projects, students can gain practical knowledge.  The technical skills can only be improved by taking up mini projects. Students become more productive.</p>                       
                        <#list cseMiniProjectsList as cseMiniProjects>
                        <p>${cseMiniProjects.heading!}  <a href="${cseMiniProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        											
                     </div>
                  </div>
               </div>
               <!-- Internship   -->
               
               <!-- Social Activities  -->
               <div role="tabpanel" class="tab-pane fade" id="social_activities">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Social Activities</h3>
                        <#list cseSocialActivitiesList as cseSocialActivities>
                        <p>${cseSocialActivities.heading!}  <a href="${cseSocialActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Social Activities  -->   
               
                <!-- Other Details  -->
               <div role="tabpanel" class="tab-pane fade" id="other_details">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Other Details</h3>
                        <#list cseOtherDetailsList as cseOtherDetails>
                        <p>${cseOtherDetails.heading!}  <a href="${cseOtherDetails.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Other Details  --> 
               
                  <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list cseFdpList as cseFdp>
                        <p>${cseFdp.heading!}  <a href="${cseFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
           
           
             <!-- Industrial Visit -->
               <div role="tabpanel" class="tab-pane fade" id="industrial_visit">
               	<div class="row">
               		 <h3>Industrial Visit </h3>
               		 
               		 <p>Objectives of industrial visit is to provide students an insight regarding internal working of companies.
               		  Students know, theoretical knowledge which is not enough for making a good professional career. With an aim 
               		  to go beyond academics, the department conducts industrial visit provides student a practical perspective on
               		   the world of work. The department also provides the visit to premier institutions like IISc and IIIT during
               		    Open Day and Open House respective events to give students awareness to newer technologies that help them
               		     in deciding the project topics during their final year.</p>
               		 
		                  <#list cseIndustrialVisitList as cseIndustrialVisit>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cseIndustrialVisit.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${cseIndustrialVisit.name!}</h3>	                      	 
			                       	 <p>${cseIndustrialVisit.designation!}</p>
			                       	 <a class="btn btn-primary" href="${cseIndustrialVisit.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Industrial Visit -->
           
           
            <!-- Project Exhibition -->
               <div role="tabpanel" class="tab-pane fade" id="project_exhibition">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
               		 
               		 <p>Project Presentation is organized to evaluate the projects by experts from Academic Institutions and/Industry.
               		   The students demonstrate the projects to the jury. The evaluation is done and 2 prizes are awarded
               		    for the best projects.</p>
               		 
               		 
		                  <#list cseProjectExhibitionList as cseProjectExhibition>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cseProjectExhibition.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${cseProjectExhibition.name!}</h3>	                      	 
			                       	 <p>Description: ${cseProjectExhibition.designation!}</p>
			                       	 <a class="btn btn-primary" href="${cseProjectExhibition.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Project Exhibition -->
               
               
                
               
               <!-- CS&D  -->
               <div role="tabpanel" class="tab-pane fade" id="csd">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                       <h3>COMPUTER SCIENCE & DESIGN (CSD) as an engineering discipline</h3>
                       <p>In the internet economy, all businesses are focused on engaging the user. A successful user engagement leads to increased revenue and profits. 
                       Any distraction results in business loss and bad reputation, often accompanied by an onslaught on social media. 
                       <a href="https://ksgi.edu.in/Best-College-For-Computer-Science-and-Design" class="font-weight-600" target="_blank">The Computer Science &amp; Design</a> (CSD) course contains two themes that address these two aspects, i.e., User Interface (UI) design 
                       and User Experience (UX) design.</p>
                       
                       <p>Most CS graduates today study Machine Learning and majority of final semester projects makes use of machine learning. 
                       However, this is often disconnected from the end user/business manager's perspective. 
                       For them, it is important to see the business intelligence and patterns from Data mining. 
                       This needs to be in an easy and intuitive visual form. The popular adage "A Picture is worth a 1000 words" is very apt in this context. 
                       Results need to be depicted in a form such that the end user can understand these results easily and apply it to improve the business. 
                       Thus, Data visualizations is a crucial aspect to truthfully and succinctly present the result to end user. 
                       The CSD course equips the student to fulfil this important business need.</p>
                       
                       <p>India and some part of the work has a large percentage of young people in their population. 
                       This young generation is a big lover of games, especially digital games. Irrespective of the economy of any country, every parent today obliges their children when it comes to digital games. 
                       Thus, a career in game design and development is considered recession proof. The gaming technology inherently makes user of AR/VR (Augmented Reality/Virtual Reality) as well as animation technologies. 
                       The CSD curriculum is framed to include courses on these topics, thereby ensuring that upcoming graduates are able to fulfil these underlying engineering needs.</p>
                       
                       
                       <p>Keeping with currents trend of IoT devices proliferating, it is imperative that new engineers are fully conversant with Robotics, automation and IoT communication. 
                       CSD curriculum in higher semesters addresses these needs as optional electives for interested students.</p>
                       
                       <p>Thus, CSD though shares about 75% of its course with CSE, but provides a distinct perspective and specialization in emerging technology fields. 
                       This course is designed in a way such that engineering graduates are equally well versed with Multimedia technologies and design approach. 
                       This is in addition to general computing approaches, tools and technologies.  
                       This course is in conformity with the objectives set out in AICTE model curriculum of courses at UG level in Emerging areas 
                       i.e., "To cope up with the upcoming emerging industrial demands, the technical institutes are to be tuned to educate and train their students to meet the upcoming requirements of the industrial revolution".</p>
                       
                       <h3>Placement and career growth</h3>
                       
                       <p>A computer science and design graduate will have manifold and multi-level opportunities in terms of career growth. 
                       This course will equip them to work in IT industry in general like other CS graduates. 
                       In addition, they have exciting opportunities to explore the careers in many segments of digital media industry such as Gaming, animation, Augmented/Virtual reality etc. 
                       As students will get general exposure to all the encompassing aspects of digital media technologies, 
                       it will open the doors for them to pursue higher studies in various disciplines of Computer Science, Information Technologies, and Digital Design.</p>                       
                       
                       
                       <p><a class="btn btn-primary" href="coursesOffered.html" target="_blank">Apply Now</a></p>
						 
                     </div>
                   </div>
               </div>
               <!-- CS & D  -->  

               <!-- C and CE  -->
               <div role="tabpanel" class="tab-pane fade" id="cce">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Computers and Communication Engineering</h3>
                        
                        <p>Over the last decade, Internet has amalgamated with every part of our daily life. Every activity we perform today is touched upon by Internet in one way or other. Internet as a technology consists of two key components, namely, Computers and Communication systems. At the undergraduate level, these two subjects have generally been taught separately as two different engineering streams; a) Computer Science b) Communication Engineering. Study of Computers started about 50+ years ago whereas communication systems has evolved over 150 years period. With the onset of Internet in 1980's, Computer Networks as a subject to introduce Communication Engineering is being taught in Computer Science courses.</p>
						<p>Study of Computers and Communication Engineering represents the amalgamation of these two branches bring out the core aspects of both these disciplines. In this course, a student will study basic and advance topics related both the branches, such as, Data Structure and Algorithms, Computer system organization, cloud computing, communication networks including wired, wireless and cellular telephony networks.</p>
						 <p>When students complete this Computer and Communication Engineer ing(CCE) course, they will acquire a strong foundation, and will have ample opportunities for their career path. For these students, placement opportunities are going to be available in Software Development, Computer Communication related industry, IoT based application industry, Sensor technology, Drones technology and many other areas. These students are likely to have multiple offers and can choose the right one that would interest them the most to follow their dreams. </p>
						 
						 
						 <p><a class="btn btn-primary" href="coursesOffered.html" target="_blank">Apply Now</a></p>
						 
                     </div>
                   </div>
               </div>
               <!-- C and CE  -->  

                <!-- Cs IOT  -->
               <div role="tabpanel" class="tab-pane fade" id="csiot">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Computer Science(IoT, Cyber Security including Block Chain)</h3>
                        
                        <p>Internet started as a research project connecting universities and educational institutes over a distributed network. As its initial operating domain was within academia, security of information communication was not a concern. With the onset of web technologies in mid 90's, when business started using the internet and web, secure communication became need of the hour. Further, most of the devices we use today in our daily life are connected to network and generate data all the times. Today, internet is being more used by devices (called Internet of Things - IoT) than human beings. The enormity of such data is at much higher scale than generated by human usage of internet.</p>
						<p>To keep up with evolving needs and new technologies, AICTE has recommended several courses in emerging technology areas, such as, Internet of Things (IoT), Cyber security, Blockchain, Robotics, Quantum Computing etc. It is imperative that new engineers are fully conversant with these newer technologies with basic knowledge of core computer science and engineering. Further, with all pervasive use of digital transactions, security and privacy of data being exchanged becomes critical. We should all become knowledgeable about securing and preserving data and expect that related tools and technologies become common place. Majority of transactions being digital, their secure preservation in distributed ledger has to become an underlying and implicit requirement, which is addressed by technologies such Blockchain. The course Computer Science & Engineering (IoT & Cyber security, including Block chain technology) is designed to meet these emerging needs. </p>
						 <p>Realizing that our engineers should be future ready, KSIT is offering B.E. degree in "CSE (IoT, Cyber Security and Blockchain)". This course will empower our young engineering graduates with the required knowledge of computer science as well as tools and technologies of these emerging areas of Internet of (Every) Thing, Drones, Cyber security, Ransomware, Blockchain and Smart contract et al. Considering the future careers, these students will be well equipped to perform well in these areas as well as play a key role in nation building and its security. These students will not only get wonderful placement opportunities but may also choose entrepreneurial path and chart their own course. </p>
						 
						 
						 <p><a class="btn btn-primary" href="coursesOffered.html" target="_blank">Apply Now</a></p>
						 
                     </div>
                   </div>
               </div>
               <!-- Cs IOT  -->  
               
               
               
               
                  
                  <!--  -->
                  <div role="tabpanel" class="tab-pane fade" id="Section4">
                   <div class="row">
                     <h3>Section 4</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
                  </div>
                  </div>
                  <!--  -->
                  <!-- Tab panes -->
                  
                  
               </div>
            </div>
         </div>
      </div>
</section>
<!--dept tabs -->
<!--about us carousal end -->


<!--<section class="container">
	<h3>Department Labs</h3>
	
	<table class="table table-bordered">
		<tr>
			<th>SI. No.</th>
			<th>Name of the Laboratory</th>
			<th>Configuration of the systems	</th>
			<th>Number of Systems	</th>
			<th>Purpose of the laboratory (Conduction of following laboratories)
			</th>
			<th>image</th>
		</tr>
		<#list labDataList as labData>
			<tr>
				<td>${labData.serialNo!}.</td>
				<td>${labData.name!}</td>
				<td>
					<ul class="ul-list">
						<#list labData.configurations as configuration>
							<li>${configuration}</li>
						</#list>
					</ul>
				</td>	
				<td>
					<ul class="ul-list">
						<#list labData.noOfSystems as noOfSystem>
							<li>${noOfSystem}</li>
						</#list>
					</ul>
				</td>
				<td>
					<ul class="ul-list">
						<#list labData.purposes as purpose>
							<li>${purpose}</li>
						</#list>
					</ul>
				</td>
				<td>
					<#if labData.imageURL?has_content>
						<td><img class="img-table" width=100 height=100 src="${labData.imageURL}" alt="" /></td>
					</#if>
				</td>
			</tr>
		</#list>
	</table>

</section>
-->



<script>


</script>

</@page>