<@page>
<#include "feedback/feedback_list_modal.ftl">
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  	<!--		<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>  -->
			  		<!--social media icons  -->

                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#if csicbPageSliderImageList?has_content>
                     		<#list csicbPageSliderImageList as csicbPageSliderImage>
                     			<#if csicbPageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${csicbPageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">${csicbPageSliderImage.heading}</p>
										<p class="slideInRight">${csicbPageSliderImage.content}</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>
	                        <article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/csicb/slider/ibs1.JPG" alt="">                       
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/csicb/slider/ibs2.JPG" alt="">                          
	                        </article>
	                        
	                         <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/csicb/slider/ibs3.JPG" alt="">                          
	                        </article>
						</#if>
                    
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 

<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 

<!-- upcoming and latest news list-->
      
<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at CSICB 
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
					
				<div class="col-md-12">
					<div class="row">

					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=csicbPageLatestEventsList/>
									</div>
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
									<@eventDisplayMacro eventList=csicbPageUpcomingEventsList/>
								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->
	
	<!-- welcome -->
	<div class="dept-title">
	   <h1>Welcome To Computer Science & Engineering <br>(IoT and Cybersecurity including Blockchain Technology)</h1>
	   <p class="welcome-text">An innovative hub of technological exploration, cutting edge research and transformative education.</p>
	</div>
	<!-- welcome -->
	


<!--dept tabs -->
<section id="vertical-tabs">
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
         <div class="vertical-tab padding" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
               <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="dept-cart">Profile</a></li>
               <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision & Mission, PEO, PSO & PO</a></li>
        <!--     <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>  -->
              <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
              
                <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Calendar of Events & Time Table</a></li>
                 <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>
                   <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
               <li role="presentation"><a href="#pedagogy" aria-controls="pedagogy" role="tab" data-toggle="tab" class="dept-cart">Pedagogy</a></li>                                                                
               <li role="presentation"><a href="#content-beyond-syllabus" aria-controls="content-beyond-syllabus" role="tab" data-toggle="tab" class="dept-cart">Content Beyond Syllabus</a></li> 
                                         
            <!--   <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li>
               
               <li role="presentation"><a href="#library_dept" aria-controls="library" role="tab" data-toggle="tab" class="dept-cart">Departmental Library</a></li>
               <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li> -->
              
              
               <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab" class="dept-cart">Achievers</a></li>
               <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
               <li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
              
        <!--       <li role="presentation"><a href="#placement_dept" aria-controls="placement" role="tab" data-toggle="tab" class="dept-cart">Placement Details</a></li> 
               
               <li role="presentation"><a href="#higher_education" aria-controls="higher_education" role="tab" data-toggle="tab" class="dept-cart">Higher Education</a></li>  -->

           <li role="presentation"><a href="#student_club" aria-controls="student_club" role="tab" data-toggle="tab" class="dept-cart">Student Club</a></li>


              <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab" class="dept-cart">Internships & Projects </a></li>  
               
               <li role="presentation"><a href="#industrial_visit" aria-controls="industrial_visit" role="tab" data-toggle="tab" class="dept-cart">Industrial Visit</a></li>
             
             
               
               <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
               <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
               
               
             <li role="presentation"><a href="#professional_bodies" aria-controls="professional_bodies" role="tab" data-toggle="tab" class="dept-cart">Professional Bodies</a></li>
               <li role="presentation"><a href="#professional_links" aria-controls="professional_links" role="tab" data-toggle="tab" class="dept-cart">Professional Links</a></li>
  
            <!--   <li role="presentation"><a href="#project_exhibition" aria-controls="project_exhibition" role="tab" data-toggle="tab" class="dept-cart">Project Exhibition</a></li> -->
               
               <li role="presentation"><a href="#mou-signed" aria-controls="mou-signed" role="tab" data-toggle="tab" class="dept-cart">MOUs Signed</a></li>
        <!--       <li role="presentation"><a href="#alumni-association" aria-controls="alumni-association" role="tab" data-toggle="tab" class="dept-cart">Alumni Association</a></li>  
               
               <li role="presentation"><a href="#social_activities" aria-controls="social_activities" role="tab" data-toggle="tab" class="dept-cart">Social Activities</a></li>
               
               <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="tab" class="dept-cart">Testimonials</a></li>  -->
                                 
               
               <!-- <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li> -->
            </ul>
            <!-- Nav tabs -->
            <!-- Tab panes -->
            <div class="tab-content tabs">
            
                  	
            	
               <!-- profile -->
               <div role="tabpanel" class="tab-pane fade in active" id="profile">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Profile</h3>
                  <!--   <p>Department of <a href="https://ksgi.edu.in/Best-College-For-Artificial-Intelligence-and-Machine-Learning" class="font-weight-600" target="_blank">Artificial Intelligence &amp; Machine Learning</a> (csicb) is established in the year 2020.</p> -->
						
						<p>The world today is inundated with massive technological transformations and witnessing the
adaptation of fast-paced digitalization in every walk of life.
                  </p>
						
						<p>Computer Science has become a beehive of constantly evolving disruptive digital technologies and
has outgrown simple data capture, processing, storage, and transactional computing. IoT (Internet of
Things), Blockchain and Cybersecurity are three distinct but interconnected fields in the realm of
digitalization. This technology triplet has just begun to create an orbit shift in digital space. The
convergence of these technologies mandates interdisciplinary strategies for securing interconnected
devices, data and systems, creating innovative opportunities coupled with exciting challenges for
the curious next generation of technophiles. 
                  </p>

						<p>The first batch of Bachelor of Engineering in CSE (ICB) students at K.S. Institute of Technology started
in 2022 with an intake of 60. The department is a dynamic blend of visionary thinkers, skilled
educators, and dedicated researchers, all united by a common goal: to shape the future of
technology and empower the next generation of digital pioneers.
                  </p>

                  
			       </div>
                  <h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
						<img class="hod-img img-responsive" src="/img/csicb/cseiot_hod.jpg" alt="image">          
						<div class="qualification_info">
	                        <h3 class="qual-text">Dr. Ganga Holi</h3>
	                        <h4 class="qual-text"> Professor & Head</h4>
	                        <h3 class="qual-text">PhD., MCSI, MISTE</h3>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>Welcome to the beginning of an innovative journey to be at the forefront of ever-evolving technology. Intrigued by the
intricate world of IoT, fascinated by the potential of blockchain, and committed to safeguarding the digital realm
through cybersecurity, we invite you to join us in shaping the future. Here at KSIT, the students of CSE (IoT &amp; Cyber
Security including Blockchain Technology) embark on a transformative journey beyond textbooks and lectures. The
principal objective of the department is to elevate the learning experience of the next generation, who are in pursuit of the
destination that paves the way for limitless possibilities. The students will be prepared with best-of-breed technology
training, improved content delivery systems, state-of-the-art research driven approaches and collaborative learning
methods.
                     </p>
                     <p>&nbsp; &nbsp; &nbsp; &nbsp;I wish every student to be part of our vibrant community,
discover their potential, ignite their imagination and embark
on a path that promises to revolutionize the way we live, work and connect.</p>
						
                     </div>
                     
              <!--       <div class="col-lg-12 col-md-12 col-sm-12">
                     	<p>The department continues to engage with industry in many ways. We do provide consultancy
as well sign Memorandum of Understanding (MoU). The consultancy mechanism provides
an opportunity to faculty to work on current real-life situations that required efficient
technology solutions. The MoUs would enable our faculty and students to collaborate with
industry to develop prototypes and engage in research to find practical workable solutions.
Department has the students club "Aenfinity". Students are actively involved in organizing
technical and non-technical activities under the club.
                     </p>
                     </div> -->
                  </div>
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>UG Program : </h3>
                     <p>
                        &nbsp; &nbsp; Bachelor of Engineering (B.E) in Computer Science & Engineering (IoT and Cybersecurity including Blockchain Technology) with approved intake of 60 students and a lateral entry of 10% from 2nd year.
                     </p>

                     <h3>Highlights : </h3>
                     <p>&nbsp; &nbsp; In an era where technology is seamlessly woven into the fabric of our lives, the Department of Computer Science & Engineering  (IoT and
 Cybersecurity including blockchain technology) takes center stage in preparing students to harness the potential of
the digital revolution. Our department stands at the intersection of multiple disciplines, focusing on
the following core pillars:</p>
                      <ul class="list">
                       		<li><b>Internet of Things (IoT): </b> Discover the realm where the physical and digital worlds converge. Our IoT
program unravels the art of connecting everyday objects to the internet, enabling them to
communicate, collect data, and make intelligent decisions. Students delve into sensor networks,
data analytics, smart devices, and the transformative impact of IoT across industries.</li>
                       		<li><b>Blockchain Technology:</b> Dive into the innovative landscape of blockchain, the decentralized,
transparent, and secure ledger that underpins cryptocurrencies and beyond. Our curriculum
demystifies blockchain&#39;s cryptographic foundations, explores its applications in finance, supply chain,
healthcare, and more and equips students with the skills to create and implement their own
blockchain solutions.</li>
                       		<li><b>Cybersecurity:</b> In an increasingly interconnected world, safeguarding digital assets is paramount. Our
cybersecurity program arms students with the knowledge and tools to thwart cyber threats, protect
sensitive data, and fortify digital infrastructure. From ethical hacking to risk management, students
gain insights into defending against evolving cyber challenges.</li>
                       		
                       		
                       </ul>  
                   <h3>What Sets Us Apart: : </h3>
                  <!--   <p>A great career catalyst for professionals.</p> -->
                   <ul class="list">
                   <li><b>Experiential Learning:</b> Learning comes alive through hands-on projects, workshops, and real-world
simulations. Our students don&#39;t just learn theories; they build working IoT devices, experiment with
blockchain protocols, and develop practical cybersecurity strategies.</li>
                   <li><b>Industry Collaborations:</b> We bridge academia and industry by partnering with tech giants, startups,
and research institutions. Students engage in internships, collaborative projects, and guest lectures
from industry experts, gaining valuable insights and forging meaningful connections.</li>
                   <li><b>Cutting-edge Research:</b> Our faculty members are at the forefront of research, driving innovation in
IoT, blockchain, and cybersecurity. From exploring AI-driven IoT applications to advancing blockchain
scalability, our research contributes to the evolution of these dynamic fields.</li>
                   <li><b>Global Perspective:</b> Technology knows no boundaries, and neither do we. Our department fosters a
global mindset by encouraging international collaborations, study abroad opportunities, and
exposure to diverse perspectives.</li>
                   </ul>
                    
                  </div>
   
               </div>
               <!-- profile -->
               
               <!-- vision and mission -->
               <div role="tabpanel" class="tab-pane fade in" id="vision">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Vision</h3>
				
						<ul class="list">
							<li>To imbibe cutting edge technological capabilities supported by pioneering
research and industry collaboration encompassed with ethical principles.
</li>
						</ul>
				<!--		<button type="button" style="float:right;" class="btn btn-info btn-sm" data-backgrop="static" data-toggle="modal" data-page="tele_page" data-target="#feedbackListModal">Feedback</button>  -->
						
				 		 <h3>Mission</h3>
						
						<ul class="list">
						<li>To equip students with the knowledge and skills needed to drive technological advancements.
</li>
						<li> To provide a dynamic learning environment that empowers students to become adept, ethical, and innovative technologists.
</li>
						<li>To train in the digital security space and contribute positively to society through transformative education.
</li>
						</ul>
					
			
					
						
					
						
						<h3>PROGRAM EDUCATIONAL OBJECTIVES (PEOs)</h3>	
						<p><b>PEO1:</b> Graduates will excel in computer science and engineering principles, specializing in IoT, cyber security, and blockchain technology, and will apply their knowledge to inventive problem-solving.
</p>
						<p><b>PEO2:</b>  Graduates will apply their technical skills to tackle real-world issues, exhibit ethical leadership, and make positive societal contributions by advancing secure, privacy-focused, and socially responsible technology.
</p>
						<p><b>PEO3:</b>  Graduates will adopt continuous learning, collaborate across fields, communicate proficiently, and propel innovation.
 </p>
					
						<h3>PROGRAM SPECIFIC OUTCOMES (PSOs)</h3>
						<p><b>PSO1:</b> Graduates will demonstrate the technical skills to address real-world challenges in IoT, cyber security, and blockchain technology while upholding ethical considerations and social responsibility.
</p>
						<p><b>PSO2:</b> Graduates will engage in continuous learning, collaborate cross-functionally, and adeptly create and implement inventive solutions for societal benefit while ensuring digital security and privacy.
</p>

	<h3>PROGRAM OUTCOMES (POs)</h3>	
                  <p><b>PO1 : </b>Engineering Knowledge:  Apply knowledge of mathematics and science, with fundamentals of Computer Science & Engineering to be able to solve complex engineering problems related to CSE.  </p>
                  <p><b>PO2 : </b>Problem Analysis : Identify, Formulate, review research literature, and analyse complex engineering problems related to CSE and reaching substantiated conclusions using first principles of mathematics, natural sciences and engineering sciences. 
</p>
                  <p><b>PO3 : </b>Design/Development of solutions : Design solutions for complex engineering problems related to CSE and design system components or processes that meet the specified needs with appropriate consideration for the public health and safety and the cultural societal and environmental considerations. 
</p>
                  <p><b>PO4 : </b>Conduct Investigations of Complex problems :  Use research-based knowledge and research methods including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions. 
</p>
                  <p><b>PO5 : </b>Modern tool usage : Create, Select, and apply appropriate techniques, resources and modern engineering and IT tools including prediction and modelling to computer science related complex engineering activities with an understanding of the limitations. 
</p>
                  <p><b>PO6 : </b>The Engineer and Society : Apply Reasoning informed by the contextual knowledge to assess societal, health, safety, legal and cultural issues, and the consequent responsibilities relevant to the CSE professional engineering practice.
</p>
                  <p><b>PO7 : </b>Environment and Sustainability : Understand the impact of the CSE professional engineering solutions in societal and environmental contexts and demonstrate the knowledge of and need for sustainable development.
</p>
                  <p><b>PO8 : </b>Ethics : Apply Ethical Principles and commit to professional ethics and responsibilities and norms of the engineering practice. 
</p>
                  <p><b>PO9 : </b>Individual and team work  : Function effectively as an individual and as a member or leader in diverse teams and in multidisciplinary Settings.
</p>
                  <p><b>PO10 : </b>Communication : Communicate effectively on complex engineering activities with the engineering community and with society at large such as able to comprehend and with write effective reports and design documentation, make effective presentations, and give and receive clear instructions. 
</p>
                  <p><b>PO11 : </b>Project Management and Finance : Demonstrate knowledge and understanding of the engineering management principles and apply these to one's own work, as a member and leader in a team, to manage projects and in multi-disciplinary environments. 
</p>
                  <p><b>PO12 : </b>Lifelong learning : Recognize the need for and have the preparation and ability to engage in independent and life-long learning the broadest context of technological change.
</p>
												  
                  </div>
               </div> 
               <!-- vision and mission -->
				               
               
               <!-- Research and development -->
      <!--         <div role="tabpanel" class="tab-pane fade" id="research">
                  <h3>Research and Development</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
               						 <h3>R & D Activities</h3>
                        
                        <#list csicbResearchList as csicbResearch>
                        	<p>${csicbResearch.heading!}  <a href="${csicbResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        <p>Renowned projects have been awarded with cash prizes by prestigious government and private bodies to encourage students.</p>
                        
                        <#list csicbSponsoredProjectsList as csicbSponsoredProjects>
                        	<p>${csicbSponsoredProjects.heading!}  <a href="${csicbSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        
                        <#list csicbPhdPursuingList as csicbPhdPursuing>
                        	<p>${csicbPhdPursuing.heading!}  <a href="${csicbPhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        
                        <h3>Ph.D Awardees</h3>
                        
                         <#list csicbPhdAwardeesList as csicbPhdAwardees>
                        	<p>${csicbPhdAwardees.heading!}  <a href="${csicbPhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                    
					
                     	
                     
                     </div>
                  </div>
               </div>   -->
               <!-- Research and development -->
               
               
               <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="row">
                     <h3>Calendar of Events & Time Table</h3>
                      <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list csicbCalanderList as csicbCalander>              
                        <p>${csicbCalander.heading!}  <a href="${csicbCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>                     
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list csicbClassTimeTableList as csicbClassTimeTable>              
                        <p>${csicbClassTimeTable.heading!}  <a href="${csicbClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list csicbTestTimeTableList as csicbTestTimeTable>
                        <p>${csicbTestTimeTable.heading!}  <a href="${csicbTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                    
                  </div>
               </div>
               <!--Time Table  -->
               
            <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>Syllabus</h3>
                     
                        <h3>B.E in Computer Science & Engineering (IoT and Cybersecurity including Blockchain Technology) </h3>
                        
                        <#list csicbUgSyllabusList as csicbUgSyllabus>
                        <p>${csicbUgSyllabus.heading!}  <a href="${csicbUgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                        
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
             
               
               
               
               <!--News Letter  -->
              <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Newsletter</h3>
                        
                        <p>The main purpose of newsletter is to encourage students and faculties to involve themselves in overall development. Newsletter includes the vision-mission of college and department along with encouraging words from Principal & HOD.
                        </p>
						<p>News letter mainly projects the events conducted and attended by students & Staff. It also throws light on the latest technology and views of Alumni about the department.</p>
                        
                        
                        <#list csicbNewsLetterList as csicbNewsLetter>
                        <p>${csicbNewsLetter.heading!}  <a href="${csicbNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--News Letter  -->
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="row">
                     <h3>Faculty</h3>
                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Staff</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name}</p>
                                 <p>${faculty.designation}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id!}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               <!--testimonials  -->
           <!--    <div role="tabpanel" class="tab-pane fade" id="testimonials">  -->
                  <!--slider start-->
             <!--     <div class="row">
                     <div class="col-sm-8">
                        <h3><strong>Testimonials</strong></h3>  -->
                        <!--<div class="seprator"></div>-->
                  <!--      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">  -->
                          
                           <!-- Wrapper for slides -->
                     <!--      <div class="carousel-inner">
                              <#list testimonialList as testimonial>
                              <div class="item <#if testimonial?is_first>active</#if>">
                                 <div class="row" style="padding: 20px">
                                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                    <p class="testimonial_para">${testimonial.content!}</p>
                                    <br>
                                    <div class="row">
                                       <div class="col-sm-2">
                                          <img src="${testimonial.imageURL!}" class="img-responsive" style="width: 80px">
                                       </div>
                                       <div class="col-sm-10">
                                          <h4><strong>${testimonial.heading!}</strong></h4>  -->
                                          <!--<p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                             <span>Officeal All Star Cafe</span>-->
                            <!--              </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </#list>  
                           </div>
                           <div class="controls testimonial_control pull-right">									            	
                              <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>    -->
                  <!--slider end -->
          <!--     </div>   -->
               <!--testimonials  -->
 
 
 			 <!-- Achievers -->
               <div role="tabpanel" class="tab-pane fade" id="achievers">
               	<div class="row">
               		 <h3>Department Achievers</h3>
                     <!--Department Achievers-->
		                  <#list csicbDepartmentAchieversList as csicbDepartmentAchievers>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csicbDepartmentAchievers.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${csicbDepartmentAchievers.heading!}</h3>	                      	 
			                       	 <p>${csicbDepartmentAchievers.eventDate!} </p>
			                       	 <p> ${csicbDepartmentAchievers.content!}</p>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list> 
		                    
		              <h3>List of FCD Students</h3>
		              <#list csicbFcdStudentsList as csicbFcdStudentsList>
                        <p>${csicbFcdStudentsList.heading!}  <a href="${csicbFcdStudentsList.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                      </#list>  
                    
               	</div>
               </div>
               <!-- Achievers -->

 				
 
               
                      <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="row">
               		
               		<h3>Infrastructure / Facilities</h3>
               		 <p>  
                           Artificial Intelligence and Machine Learning Department has fully furnished State of Art
                           laboratories. All laboratories are well-equipped to run the academic laboratories courses.
                           Highly experienced Technical staff are available in all the laboratories to assist the
                           students to carry out their lab sessions.
                     </p>
                     <p><b>The Department has :</b></p>
                     <ul class="list">
                     <li>  Dedicated two laboratories, all are well-equipped with latest computers installed
                           with required software for a students to carry out lab sessions, LAN connectivity,
                           WiFi and uninterrupted power supply. Projectors are available for effective
                           explanation of concepts and programs related to academic laboratories courses,
                           technical seminar presentation, internship presentation and project phase reviews.
                           Analog and Digital Electronics Laboratory/Microcontroller and Embedded Systems
                           Laboratory equipment's like CRO, IC trainer kits, ARM7 LPC 2148 Microcontroller
                           kit, ALS - SDA - ARM7 are available.</li>
                     <li>  Two class rooms with a seating capacity of seventy and one fully furnished
                           Seminar hall with seating capacity of 120.</li>
                     
                     </ul>
               	
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
		                  <#list csicbLabList as csicbLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csicbLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${csicbLab.name!}</h3>	                      	 
			                       	 <p>${csicbLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csicbLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
               
               
               
              <!-- Gallery  -->
               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
                  <h3>Gallery</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="gallerySLide" class="gallery_area">
                           <#list csicbGalleryList as csicbGallery>	
                           <a href="${csicbGallery.imageURL!}" target="_blank">
                           <img class="gallery_img" src="${csicbGallery.imageURL!}" alt="" />					                    			                   
                           <span class="view_btn">${csicbGallery.heading!}</span>
                           </a>
                           </#list>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Gallery -->
               
                <!-- Dept Library  -->
               <div role="tabpanel" class="tab-pane fade" id="library_dept">
                  <h3>Departmental Library</h3>
                  <p>
                     Libraries play a vital role in the development of any society by enhancing the cause of education and academic research. They cater to the information needs of thousands of people.
                  </p>
                  <p>
                     The department has its own library which has more than 500 text books and reference
                     book catering to the needs of students as well as Teaching Staffs. The library preserves
                     previous year's project, internship reports, journals; dissertation and seminar reports and
                     the books contributed by Alumnis and these are available for students and staffs. Every
                     year 50 to 100 books are being added to the stock. The students and Staffs borrow
                     books from the library periodically. The department maintains separate register for
                     borrowing books.
                  </p>
                  <h2 class="titile">Collection Of books</h2>
                  <table class="table table-striped course_table">
                     <#list csicbLibraryBooksList as libraryBooks>
                     <tr>
                        <th style="width:50%">${libraryBooks.heading!}</th>
                        <td>${libraryBooks.content!}</td>
                     </tr>
                     </#list>
                  </table>
               </div>
               <!--  Dept Library   -->
               
               <!-- Department placements -->
               <div role="tabpanel" class="tab-pane fade" id="placement_dept">
                  <h3>Placement Details</h3>
                  
                  <p>Placement Training will be provided to all semester students at the beginning of the semester.
                  </p>
                  <p>Placement Drives will be arranged for final year students from August. Company Specific
                  </p>
                  <p>Trainings will be arranged for final year students to grab more offers in their dream Companies.
                  </p>
                  <p> Technical training sessions will be provided to Pre-final Year students.
                  </p>
                  
                  
                  
                  
                  <#list csicbPlacementList as csicbPlacement>              
                  <p>${csicbPlacement.heading!}  <a href="${csicbPlacement.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>											
                  <!-- Department placements -->
                  
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                  
                    <h3>Events</h3>
                    
                    <#list csicbEventsList[0..*3] as csicbEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${csicbEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${csicbEvent.heading!}</h3>	                      	 
	                       	 <p>"${csicbEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${csicbEvent.eventDate!}  </span></p>
	                       	 <a class="btn btn-primary" href="csicb_events.html">View more</a>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
  						
                        
                  </div>
                  <!-- Events  -->
                  
                      <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        
                          <#list csicbEresourcesList as csicbEresources>
	                    
	                        <p>${csicbEresources.heading!}  
	                      	    <a href="//${csicbEresources.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
                              
               <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     <h3>Teaching and Learning</h3>
                     <p>
                     The faculty of CSICB department attends various MOOCS and training programs on
                     advanced topics, update their knowledge and skills, and gives additional inputs in the
                     classes. Further, the faculty conducts various innovative teaching and learning activities
                     inside and outside the classrooms to engage the students effectively and efficiently. The
                     teaching and learning activities conducted by the faculty for the improvement of student
                     learning includes:
                     </p>
                     <ul class="list">
                     <li>Teaching with simulations and animated videos</li>
                     <li>Assignments include Conduction of Poster Presentations, online and classroom
                           quizzes, surprise class tests, group discussions, seminars Miniprojects etc.</li>
                     <li>Usage of ICT and Google classrooms for posting assignments and lecture materials.</li>
                     <li>Usage of Google forms and Kahoot for online interaction, assessment and evaluation.</li>
                     </ul>
                     <p>
                        The instructional materials and pedagogical activities are uploaded in Google drive and
                        the associated links are made available on institutional website for student and faculty
                        access.
                     </p>
                     
                        <h3>Instructional Materials</h3>
                        <p>
                           Instructional Materials are made available for public access.
                        </p>
                        
                          <#list csicbInstructionalMaterialsList as csicbInstructionalMaterials>
	                    
	                        <p>${csicbInstructionalMaterials.heading!}  
	                      	    <a href="//${csicbInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Lab Manuals</h3>
                        
                          <#list csicbLabManualList as csicbLabManual>
	                    
	                        <p>${csicbLabManual.heading!}  
	                      	    <a href="//${csicbLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                    
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
               <!-- pedagogy -->

                <div role="tabpanel" class="tab-pane fade" id="pedagogy">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Pedagogical Activities</h3>
                         <p>
                           Pedagogy is the method and practice of teaching an academic subject or theoretical
                           concept. Pedagogical skills involve being able to convey knowledge and skills in ways
                           that students can understand, remember and apply. Pedagogies involve a range of
                           techniques, including whole-class and structured group work, guided learning and
                           individual activity. Pedagogies focus on developing higher order thinking.
                         </p>
                        
                          <#list csicbPedagogicalActivitiesList as csicbPedagogicalActivities>
	                    
	                        <p>${csicbPedagogicalActivities.heading!}  
	                      	    <a href="//${csicbPedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogy Review Form</h3>
                         <p>
                           The Peer review forms associated with pedagogical activities are made available for
                           public access, peer review, critique and further development.
                         </p>
                        
                          <#list csicbPedagogyReviewFormList as csicbPedagogyReviewForm>
	                    
	                        <p>${csicbPedagogyReviewForm.heading!}  
	                      	    <a href="//${csicbPedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>       
                       											
                     </div>
                  </div>
               </div>
              
                <!-- pedagogy -->

                 <!-- content beyond syllabus -->

                <div role="tabpanel" class="tab-pane fade" id="content-beyond-syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Content Beyond Syllabus</h3>						
                        
                         <#list csicbcontentbeyondsyllabusList as csicbcontentbeyondsyllabus>
                        <p>${csicbcontentbeyondsyllabus.heading!}  <a href="${csicbcontentbeyondsyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	                 
                       											
                     </div>
                  </div>
               </div>
              
                <!-- content beyond syllabus -->
               
               
               
               <!-- MOU's signed -->
               <div role="tabpanel" class="tab-pane fade" id="mou-signed">
                <div class="row">
                  <h3>MOUs Signed</h3>
                  <p>
                     The Memorandum of Understanding details modalities and general conditions regarding
                     collaboration between the INDUSTRY and KSIT. Also facilititate for enhancing and
                     encouraging students for interactions between the Industry Experts, Scientists,
                     Research fellows and Faculty members for knowledge sharing with the following
                     objectives :
                  </p>
                  <ul class="list">
                  <li>Collaboration in conduction of conferences, workshops, seminars.</li>
                  <li>Provides industry exposure to KSIT students.</li>
                  <li>Mutually agreed guidance for student&#39;s projects works through internship and industrial training.</li>
                  <li>Provision given for the usage of institute infrastructure by industry like library, internet and computational facilities etc.</li>

                  </ul>
                  <#list csicbMouSignedList as csicbMouSigned>              
                  <p>${csicbMouSigned.heading!}  <a href="${csicbMouSigned.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- MOU's signed -->
               
                <!-- Alumni Association -->
               <div role="tabpanel" class="tab-pane fade" id="alumni-association">
                <div class="row">
                  <h3>Alumni Association</h3>
                  <p>CHIRANTHANA with a tagline of EVERLASTING FOREVER is the name given to KSIT ALUMNI ASSOCIATION, with a motto of uniting all the alumni members under the umbrella of KSIT and growing together forever.KSIT alumni association was started in the year 2014.The association provides a platform to their alumni's to share their knowledge & experience with the budding engineers through technical talks, seminars & workshops.</p>
                  <#list csicbAlumniAssociationList as csicbAlumniAssociation>              
                  <p>${csicbAlumniAssociation.heading!}  <a href="${csicbAlumniAssociation.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- Alumni Association -->
               
               
               <!-- Professional Bodies  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_bodies">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Bodies</h3>
                        <p>Department of Artificial Intelligence and Machine Learning of KSIT has various professional bodies. Many workshops, technical talks, Guest lectures are organized in
association with this professional bodies. This gives opportunity to students to enhance their knowledge on cutting edge technologies.</p>
                       <ul class="list">
                        <li><b>ISTE : </b> formulates &amp; generates goals &amp; responsibilities of technical education.
Seminars, workshops, conferences are organized on the topic of relevance to
technical education for engineering students as well as teachers. KSIT ISTE
Chapter was established on 22 nd March 2014.</li>
                        <li><b>The Institution of Engineers (IEI) : </b>is the national organization of engineers in
India. IEI in KSIT promotes an environment to enable ethical pursuits for
professional excellence. KSIT established IEI Life membership in Dec 2014</li>
                        <li><b>The Board for IT Education Standards (BITES) : </b>is an autonomous body and a
non-profit society promoted by the Government of Karnataka, in association with
educational institutions and IT Industries in Karnataka, in order to enhance the
quality of IT education and help build quality manpower for the IT industry. Set up
in June 2000, BITES is an ISO 9001:2015 certified organization.</li>
                     </ul>
                        <#list csicbProfessionalBodiesList as csicbProfessionalBodies>
                        <p>${csicbProfessionalBodies.heading!}  <a href="${csicbProfessionalBodies.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Professional Bodies   -->
           
           
           	<!-- Professional Links  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_links">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Links</h3>
                        <ul class="list">
                        <li><b>BITES : </b>The Board for IT Education Standards is a non-profit Society set up by the Karnataka
Government, in association with IT industries and educational institutions, in order to
enhance the quality of IT education and help build quality manpower for the IT industry.
The institute is registered institutional member of BITES since January 2004.
Workshops, Seminars, Guest Lectures, are regularly conducted for students. Faculty
development programs are conducted for faculties. The event details are published
regularly in BITES Communications and also in college magazine.</li>
                        <li><b>ISTE: </b> The Indian Society for Technical Education (ISTE) is the leading National Professional
non-profit making Society for the Technical Education System. The institute established
KSIT-ISTE Student Chapter in March 2014. Various events such as student Convention,
Faculty convention, Seminars, Guest Lectures are conducted under ISTE Chapter.</li>
                        <li><b>IEI : </b> The Institution of Engineers (India) [IEI] is a professional body to promote and advance
the engineering and technology. It was established in 1920.The institute established IEI
Institute Life membership in Dec 2014. Various events such as Faculty development
programs, workshops, Seminars, Guest Lectures are conducted under IEI.</li>
                        </ul>
                        
                         <#list csicbProfessionalLinksList as csicbProfessionalLinks>
	                    
	                        <p>${csicbProfessionalLinks.heading!}  
	                      	    <a href="//${csicbProfessionalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                												
                     </div>
                  </div>
               </div>
               <!-- Professional Links   -->
           
                 <!-- Higher Education  -->
               <div role="tabpanel" class="tab-pane fade" id="higher_education">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Higher Education</h3>
                        <#list csicbHigherEducationList as csicbHigherEducation>
                        <p>${csicbHigherEducation.heading!}  <a href="${csicbHigherEducation.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Higher Education   -->
           
              
              
                 <!-- Students Club  -->
               <div role="tabpanel" class="tab-pane fade" id="student_club">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     <!--	<h3>Aenfinity</h3> -->
                     
                     
                        <h3>Club Activities</h3>
                        <#list csicbClubActivitiesList as csicbClubActivities>
                        <p>${csicbClubActivities.heading!}  <a href="${csicbClubActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>External Links (Club)</h3>	
                        
                         <#list csicbClubExternalLinksList as csicbClubExternalLinks>
	                    
	                        <p>${csicbClubExternalLinks.heading!}  
	                      	    <a href="//${csicbClubExternalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                        			
                        
                        							
                     </div>
                  </div>
               </div>
               <!-- Students Club   -->
           
               
                 <!-- Internship  -->
               <div role="tabpanel" class="tab-pane fade" id="internship">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Internship</h3>
                        <p>
                           Students are facilitated by providing list of companies /Industries, where they can
                           undergo 4 weeks of Internship/ Professional practice. College takes initiative to organize
                           in house internship by calling resource persons from industry. Student cumulates their
                           learning in a report on their internship/ professional practice and gives a presentation in
                           front of internship coordinator and the guide.
                        </p>
                        <#list csicbInternshipList as csicbInternship>
                        <p>${csicbInternship.heading!}  <a href="${csicbInternship.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                  
             			<h3>Projects</h3>
                        <#list csicbProjectsList as csicbProjects>
                        <p>${csicbProjects.heading!}  <a href="${csicbProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Mini-Projects</h3>                       
                        <#list csicbMiniProjectsList as csicbMiniProjects>
                        <p>${csicbMiniProjects.heading!}  <a href="${csicbMiniProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        											
                     </div>
                  </div>
               </div>
               <!-- Internship   -->
               
               <!-- Social Activities  -->
               <div role="tabpanel" class="tab-pane fade" id="social_activities">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Social Activities</h3>
                        <#list csicbSocialActivitiesList as csicbSocialActivities>
                        <p>${csicbSocialActivities.heading!}  <a href="${csicbSocialActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Social Activities  -->   
               
                 <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list csicbFdpList as csicbFdp>
                        <p>${csicbFdp.heading!}  <a href="${csicbFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
           
           
             <!-- Industrial Visit -->
               <div role="tabpanel" class="tab-pane fade" id="industrial_visit">
               	<div class="row">
               		 <h3>Industrial Visit </h3>
               	<!--	 	<p>
                           The department strives to offer a great source of practical knowledge to students by
                           exposing them to real working environment through Industrial visits. Regularly Industrial
                           visits are arranged to organizations like Indian Space Research Organization, Karnataka
                           State Load Dispatch Centre, All India Radio. Visits are arranged for students to view
                           project exhibitions Organized by Indian Institute of Science, Visvesvaraya Industrial and
                           Technological Museum, EMMA-Expo 2014 and so on. Also visits are arranged to places
                           like HAL Heritage Centre and Aerospace Museum.
							   </p>  -->
		                  <#list csicbIndustrialVisitList as csicbIndustrialVisit>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csicbIndustrialVisit.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${csicbIndustrialVisit.name!}</h3>	                      	 
			                       	 <p>${csicbIndustrialVisit.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csicbIndustrialVisit.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Industrial Visit -->
           
           
            <!-- Project Exhibition -->
               <div role="tabpanel" class="tab-pane fade" id="project_exhibition">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
               		 <p>The department conducts the project exhibition in the even semester of every academic year. Students are encouraged to present their projects and mini projects to the invited guests and evaluators. The best innovative projects are selected and are awarded.</p>
		                  <#list csicbProjectExhibitionList as csicbProjectExhibition>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csicbProjectExhibition.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${csicbProjectExhibition.name!}</h3>	                      	 
			                       	 <p>Description: ${csicbProjectExhibition.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csicbProjectExhibition.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Project Exhibition -->
           
               
                  
                  
                  <!--  -->
                  <div role="tabpanel" class="tab-pane fade" id="Section4">
                     <h3>Section 4</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
                  </div>
                  <!--  -->
                  
               </div>
            </div>
         </div>
      </div>
</section>
<!--dept tabs -->
	


	
   
   
   
    	
	 
</@page>