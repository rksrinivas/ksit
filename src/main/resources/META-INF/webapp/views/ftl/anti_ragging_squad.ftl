<@page>
    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about">
	<div class="container">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">

				<div class="dept-title">
   				<h1>Anti Ragging Cell</h1>   
				</div> 

          <div class="aboutus_area wow fadeInLeft">
			
    				<p>
					Ragging which is a social evil can cause an ill effect on the student community. This may have a psychological and physical effect on a student. There is no Ragging at KSIT campus as the administration has taken several steps to curb ragging & indiscipline. With respect to the Reference to clause (g) of sub - section (I) of Section 26 of the University Grants Commission Act, 1956, and AICTE notification vide curricular no. F. No. 37-3/legal /AICTE/2009 dated 25/03/2009, all regulations will be followed strictly in the institute to root out ragging in all its forms.
					</p>
					
					<p>Dear students please fill in the online Anti Ragging Affidavit by clicking the link: <a href="https://antiragging.in/affidavit_university_form.php"  id="antiragging_link" target="_blank">&#9656; https://antiragging.in/affidavit_university_form.php </a> </p>
					
					<h3 style="color:red;">Nodal office Details:</h3>
					<p><b>Name: </b>Prof. Sunil KumarN</p>
					<p><b>Email ID: </b> sunilkumarn@ksit.edu.in</p>
					
					
					
					<h3 style="color:red;">Ragging constitutes of following acts:</h3>
					<ul class="list">
					
						<li>Any conduct by any student or students whether by words spoken or written or by an act which has the effect of teasing, treating or handling with rudeness a fresher or any other student.</li>
						
						<li>Indulging in rowdy or undisciplined activities by any student or students which causes or is likely to cause annoyance, hardship, physical or psychological harm or to raise fear or apprehension thereof in any fresher or any other student.</li>
						
						<li>Asking any student to do any act which such student will not in the ordinary course do and which has the effect of causing or generating a sense of shame, or torment or embarrassment so as to adversely affect the physique or psyche of such fresher or any other student. </li>
						
						<li>Any act by a senior student that prevents, disrupts or disturbs the regular academic activity of any other student or a fresher. </li>
						
						<li>Exploiting the services of a fresher or any other student for completing the academic tasks assigned to an individual or a group of students. </li>

						<li>Any act of financial extortion or forceful expenditure burden put on a fresher or any other student by students.</li>

						<li>Any act of physical abuse including all variants of it: sexual abuse, homosexual assaults, stripping, forcing obscene and lewd acts, gestures, causing bodily harm or any other danger to health or person. </li>

						<li>Any act or abuse by spoken words, emails, posts, public insults which would also include deriving perverted pleasure, vicarious or sadistic thrill from actively or passively participating in the discomfiture to fresher or any other student. </li>

						<li>Any act that affects the mental health and self-confidence of a fresher or any other student with or without an intent to derive a sadistic pleasure or showing off power, authority or superiority by a student over any fresher or any other student.</li>
					</ul>	

					<h3 style="color:red;">Actions to be taken on those students who indulge in ragging:</h3>
					<p><b>1.</b> 	Every single incident of ragging a First Information Report (FIR) will be filed without exception by the institutional authorities with the local police authorities. </p>
					<p><b>2.</b>   Depending upon the nature and the gravity of the offence as established the possible punishments for those found guilty of ragging shall be any one or any combination of the following:</p>
					<ul class="list">
					<li>Cancellation of admission.</li>
					<li>Suspension from attending the classes.</li>
					<li>Debarring from appearing in any test/examination or other evaluation process.</li>
					<li>Suspension/Expulsion from the hostel.</li>
					<li>Rustication from the institution for period from 1 to 4 semesters.</li>
					<li>Expulsion from the institution and consequent debarring from admission to any other institution.</li>
					<li><b>Collective punishment:</b> when the persons committing or abetting the crime of ragging are not identified, the institution shall report to collective punishment as a deterrent to ensure community pressure on the potential raggers.</li>
					</ul>
			
		  </div>
        </div>
		
		</div>
		</div>
	</section>

<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<!--<h3>Grievance</h3>-->
					<!--  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote> -->
                    
				<div class="dept-title">
   				<h1>Anti Ragging Squad</h1>   
				</div>            
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<!--	<h3 class="color_red">GRIEVANCE REDRESSAL COMMITTEE</h3>
	                    	<p>As per the directions of the Registrar, VTU, Belgaum and in accordance
	                    	 with the regulations of AICTE, a <strong>Grievance Redressal Committee</strong> has been established in our 
	                    	 college with the following --->
	                    	 <!--<a href="${img_path!}/pdf/KSIT_Grievance_Cell.pdf" target="_blank">members</a>
	                    	 .</p>-->
	                    <!--	<p>All aggrieved students, their parents & staff may approach the committee for assistance and solve
	                    	 their problems related to academics, resources and personal grievances, ifany.</p> -->
	                    	 
	                    	 	<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name</th>
												<th>Department</th>
												<!--<th>Mobile</th> -->
												<th>Designation</th>
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>1.</td>
												<td>Mr. Sunil Kumar. N </td>
												<td>Dept of AS & H</td>
												<!--<td>9916915517 </td> -->
												<td>Co-ordinator</td>
											  </tr>
											  
											   <tr>          
												<td>2.</td>
												<td>Mr. K. Prasad</td>
												<td>Dept of ME </td>
												<td>Member</td>
											  </tr>
											  
											   <tr>          
												<td>3.</td>
												<td>Mr. Ranganath. N</td>
												<td>Dept of ME</td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>4.</td>
												<td>Dr. Devika. B	</td>
												<td>Dept of ECE </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>5.</td>
												<td>Ms. Ramya. K.R</td>
												<td>Dept of ECE </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>6.</td>
												<td>Mr. Christo Jain	</td>
												<td>Dept of ECE	 </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>7.</td>
												<td>Mr. Satish Kumar. B</td>
												<td>Dept of ECE </td>
												<td>Member</td>
											  </tr>
											  
											<!--   <tr>          
												<td>8.</td>
												<td>Mr. Harshavardhan. J. R  </td>
												<td>Dept. of CSE </td>
												<td>Member </td>
											  </tr> -->
											  
											  <tr>          
												<td>8.</td>
												<td>Mr. Kumar. K  </td>
												<td>Dept. of CSE </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>9.</td>
												<td>Mr. Somashekar. T  </td>
												<td>Dept. of CSE </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>10.</td>
												<td>Ms. Rashmi. H</td>
												<td>Dept. of CSE </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>11.</td>
												<td>Ms. Pallavi. R </td>
												<td>Dept. of CSE </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>12.</td>
												<td>Ms. Lakshmi. K. K </td>
												<td>Dept. of AI&ML</td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>13.</td>
												<td>Mr. Nagabhushan Pandarinath  </td>
												<td>Dept. of AI&ML </td>
												<td>Member</td>
											  </tr>

                                              <tr>          
												<td>14.</td>
												<td>Ms. Sathya Sheela. D  </td>
												<td>Dept. of CSD </td>
												<td>Member</td>
											  </tr>

                                              <tr>          
												<td>15.</td>
												<td>Ms. Rachana. V. Murthy  </td>
												<td>Dept. of CSE-ICB </td>
												<td>Member</td>
											  </tr>

                                              <tr>          
												<td>16.</td>
												<td>Dr. Venkataramana. B. S </td>
												<td>Dept. of AS&H </td>
												<td>Member</td>
											  </tr>

                                              <tr>          
												<td>17.</td>
												<td>Mr. Naveen. v  </td>
												<td>Dept. of AS&H </td>
												<td>Member</td>
											  </tr>

                                              <tr>          
												<td>18.</td>
												<td>Ms. Sneha.G. Kulkarni </td>
												<td>Dept. of AS&H </td>
												<td>Member</td>
											  </tr>

                                              <tr>          
												<td>19.</td>
												<td>Ms. Mamatha. N </td>
												<td>Dept. of AS&H </td>
												<td>Member</td>
											  </tr>

                                              <tr>          
												<td>20.</td>
												<td>Mr. Shivaprakash. K. M  </td>
												<td>PED </td>
												<td>Member</td>
											  </tr>
											
											 </tbody>
											 
													 
											 
											  
											  
											  
								 </table>
	                    	 
						                    
                    </div>
                    
                    
                    
                    
                   
                  </div>
                </div>
                <!-- End single course -->
				
					
				
				 
            </div>
          </div>
          <!-- End course content -->
          
         
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>