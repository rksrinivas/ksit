<@page>
	        <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact">
     <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="titile text-center">K.S. Institute of Technology </h2>
              <span></span> 
            <!--  <p>The Kammavari Sangham, a multi-activity non-profit oriented voluntary service organization, was established in the year 1952 with the sole objective of providing charitable service to community and society. The Sangham has diversified its activities since its establishment over five decades ago.</p>-->
            </div>
          </div>
       </div>
       <div class="row">
         <div class="col-lg-8 col-md-8 col-sm-8">
         
           <div class="contact_form wow fadeInLeft">
           
           	  <form action="" class="submitphoto_form" method="post">
                <input type="text" class="wp-form-control wpcf7-text" name="name" placeholder="Your name" required>
                <input type="email" class="wp-form-control wpcf7-email" name="email" placeholder="Email address" required>
				<input type="phone" class="wp-form-control wpcf7-phone" name="phone" placeholder="Phone No" required>                    
                <input type="text" class="wp-form-control wpcf7-text" name="subject" placeholder="Subject" required>
                <textarea class="wp-form-control wpcf7-textarea" name="description" cols="30" rows="10" placeholder="What would you like to tell us" required></textarea>
                <input type="submit" value="Submit" class="wpcf7-submit">
              </form>
              <br/>
           </div>
           
           
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4">
           <div class="contact_address wow fadeInRight">
             <h3>Address</h3>
             <div class="address_group">
               <b style="color:#ff0000">&#9656; K.S. Institute of Technology</b>
        <br/>
				<p><i class="fa fa-map-marker"></i>&nbsp;No.14, Raghuvanahalli, Kanakapura Main Road, Bengaluru - 560109</p>
		<!--		<p><i class="fa fa-phone"></i>&nbsp;Phone : 080-28435722, 080-28435724</p>
				 <p><i class="fa fa-fax"></i>&nbsp;Fax : 080-28435723</p>  -->
         <p><i class="fa fa-phone"></i>&nbsp;Phone : 080-28427162, 080-28427164</p>
				<p><i class="fa fa-globe"></i>&nbsp;Website : www.ksit.ac.in</p>
				<p><i class="fa fa-envelope"></i>&nbsp;Email: principal@ksit.edu.in</p>
             </div>
           <!--  <div class="address_group">
              <ul class="footer_social">
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>
                </ul>
             </div> -->
           </div>
         </div>
       </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->

    
	<!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact" style="margin-top:0;background:#f1f3f4;">
     <div class="container">
       
       <div class="row">
         <div class="col-lg-8 col-md-8 col-sm-8">
           
		   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2891.4346423537013!2d77.54324704939891!3d12.879259011723011!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3ff4059b97c1%3A0x9ae119628bd2e43d!2sK.+S.+Institute+of+Technology!5e0!3m2!1sen!2sin!4v1490240642842" width="100%" height="400" frameborder="0" style="border:0;margin:auto;" allowfullscreen></iframe>
		   
		   
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4">
           <div class="contact_address wow fadeInRight" style="margin:1px;background:#ebe8de;height:auto;padding:3px;border:0.5px solid #fff;">
             <!--<h3>BMTC Bus Numbers</h3>-->
             <div class="address_group">
					<p class="route_list">Buses From Mejestic/Kempegowda Bus Station</p>
					<p class="route_list_details">211B, 212D, 212F,211J,213N, KBS-5H</p>
					<p class="route_list">From K.R.Market</p>
					<p class="route_list_details">211, 211E, 211M, 211T, 213T, 214B, 211D, 211DA, 211G, 211H, 211K, 211N, 211Q, 211R, 212, 213H, 213J, 213K, 213L, 211A, 211C, 212C, 213, 213D, 213DA</p>
				  
				  <p class="route_list">From Corporation Circle</p>
				  <p class="route_list_details">211B, 214A, 212D, 212F, 211J, KBS-5H, 213N, CR213 </p>

				  <p class="route_list">Raghuvanahalli stop, Kanakapura main road</p>
          <p class="route_list_details">11A and 19C, 211B, 211F, 211J, 211L, 212D, 212F, 213Z, 214F, 217C </p>
            </div>
           </div>
         </div>
       </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->
	
	
	<!--=========== BEGIN GOOGLE MAP SECTION ================-->
    <!--<section id="googleMap">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2891.4346423537013!2d77.54324704939891!3d12.879259011723011!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3ff4059b97c1%3A0x9ae119628bd2e43d!2sK.+S.+Institute+of+Technology!5e0!3m2!1sen!2sin!4v1490240642842" width="100%" height="600" frameborder="0" style="border:0;margin:auto;" allowfullscreen></iframe>
	</section>-->
    <!--=========== END GOOGLE MAP SECTION ================-->
	
</@page>