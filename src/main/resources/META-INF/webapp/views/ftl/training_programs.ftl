<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3 class="text-center">Training Programs Organized by Placement Division</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>We believe that to be leaders in the real world, students must develop the intellectual depth & breadth to handle the challenges of increasing complexity, globalization and rapid change. The Student Development Program focuses on every aspect of student personality, helping them develop interpersonal, technical and business skills.</p>
                    </blockquote>
                    
					
				<!-- start single course -->
				<#list placementTrainingList as placementTraining>
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	${placementTraining.program!}</h3>
					<h3 class="singCourse_title"><span>Semester: </span> ${placementTraining.semester!}</h3>
					<h3 class="singCourse_title"><span>Year: </span> ${placementTraining.year?c!}</h3>
					<h3 class="singCourse_title"><span>Company: </span>	${placementTraining.company!}</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> ${placementTraining.schedule!} </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                </#list>
                <!-- End single course -->
					
					
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Personality Development Program (LifeSkill )</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 01-08-2017 to 12-08-2017 </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Personality Development Program (LifeSkill )</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 5th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Sevent Sense</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 18/8/2017 to 22/8/2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Personality Development Program (LifeSkill )</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 3rd</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 04-09-2017 to 04-09-2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Campus Induction Program</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 1st</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 07-08-2017 to 08-08-2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	HireMee Online Assessment Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	HireMee</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 29/8/2017 to 29/8/2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Freshersworld.Com- Online Assessment Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Freshersworld</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 01-09-2017 to 01-09-2017 </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	First Naukri.Com-Online  Assessment Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Freshersworld</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 01-09-2017 to 01-09-2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Infosys Company Specific Training</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 07-09-2017 to 09-09-2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Aspiring Minds-  AMCAT Assessment Test </h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Aspiring Mind</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 11-09-2017 to 11-09-2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
									
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	NTT DATA Company Specific Training</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	NTT DATA</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 18/09/2017 to 18/09/2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				
				
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Cocubes Online Assessment Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Cocubes</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 25/09/2017 to 25/09/2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
								
					
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Workshop on Recent advance in IT Industry by ABC Technology Program</h3>
					<h3 class="singCourse_title"><span>Semester: </span> 7th</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2017</h3>
					<h3 class="singCourse_title"><span>Company: </span>	</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> 04-10-2017 to 04-10-2017</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
								
			  
                <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Great Mind Challenge Competition</h3>
					<h3 class="singCourse_title"><span>Semester: </span>2nd,4th & 6th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2016</h3>
					<h3 class="singCourse_title"><span>Company: </span>	IBM</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>4/3/2016 to 03/04/2016</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program:</span>	Seminor on GRE/GMAT & International Studies	</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2016</h3>
					<h3 class="singCourse_title"><span>Company: </span>Byju's</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>03/01/2016 to 03/01/2016 </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  
				
				
				
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Seminor on GRE, TOFEL& IELTS & International Studies</h3>
					<h3 class="singCourse_title"><span>Semester </span>6th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2016</h3>
					<h3 class="singCourse_title"><span>Company: </span>Manhattan Review</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>12/04/2016 to 12/04/2016</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp  placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Personality Development Program (LifeSkill )</h3>
					<h3 class="singCourse_title"><span>Semester: </span>4th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2016</h3>
					<h3 class="singCourse_title"><span>Company: </span>Seventh sense</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>02/02/2016 to 9/2/2016 (Except 6,7)</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Personality Development Program (LifeSkill )</h3>
					<h3 class="singCourse_title"><span>Semester: </span>6th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2016</h3>
					<h3 class="singCourse_title"><span>Company: </span>Seventh sense</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>25/1/2016 to 02/01/2016</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Cocubes Pre Assessment Test II</h3>
					<h3 class="singCourse_title"><span>Semester: </span>8th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2016</h3>
					<h3 class="singCourse_title"><span>Company: </span>Co-cubes</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>21/01/2016 to 21/01/2016</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	AMCAT Assessment Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Aspiringminds</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>29/10/2015 to 29/10/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Tech Mahindra Company Specific Training</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>	Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>16/10/2015	 to 16/10/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>L & T Infotech Company Specific Training</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>10/05/2015 to 10/06/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Infosys Company Specific Training</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th Sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>uantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>18/9/2015 to 20/9/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Online Assess ment Test - E Practo</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Formac</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>18/8/2015 to 18/8/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Technical Training Program- By Dr.Padma Reddy , Mangining Director,Sai Vidya college of Engineering Bangalore</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>08/01/2015 to 18/8/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Cocubes Pre Assessment Test I</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Co-cubes</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>20/8/2015 to 20/8/2015 </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Freshers World.Com Assessment Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Freshers World.Com</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>19/8/2015 to 19/8/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Cocubes Carrier Diagnostic Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Co-cubes</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>08/11/2015 to 13/8/2015 </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Seventh Sense Online Assessment Test</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7th sem</h3>
					<h3 class="singCourse_title"><span>Year: </span>2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Seventh sense</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>08/10/2015 to 08/10/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  
				
				<!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Campus Induction Program	</h3>
					<h3 class="singCourse_title"><span>Semester: </span>1rd sem</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>04/08/2015 to 06/08/2015 </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	Personality Development Program (LifeSkill)</h3>
					<h3 class="singCourse_title"><span>Semester: </span>3rd sem</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>17/08/2015 to 21/08/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Personality Development Program (LifeSkill)</h3>
					<h3 class="singCourse_title"><span>Semester: </span>5rd sem</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>10/08/2015 to 15/08/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
               
              <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>Personality Development Program (LifeSkill)</h3>
					<h3 class="singCourse_title"><span>Semester: </span>7rd sem</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2015</h3>
					<h3 class="singCourse_title"><span>Company: </span>Quantech Origin</h3>
					<h3 class="singCourse_title"><span>Schedule: </span>027/08/2015 to 02/08/2015</h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
            </div>
          </div>
          <!-- End course content -->
		
		<h3>Interpersonal Skill Development</h3>
		<p>Creativity, lateral thinking and communication people management skills are essential components for progress in any sphere. Students are encouraged to develop these through goal setting exercises, group discussions, mock interviews and presentations.</p>
		
		<h3>Technical Skill Development</h3>
		<p>The depth of technical skills that students develop, depend to a great extent on the course they have chosen. However, all students are given a conceptual grounding in core skills and application orientation through real time projects to ensure their skills are concurrent with market needs.</p>
		
		<h3>Faculty development programme</h3>
		<p>Our faculty is attending Wipro Mission 10X faculty development programme organized by Wipro Technologies. </p>

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>