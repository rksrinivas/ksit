<@page>
	 
	 <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs_course">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="aboutus_area wow fadeInLeft">
            <h2 class="titile"> E-Resources</h2>
				
				<p>Online resources are subscribed to all the programmes Undergraduate, Postgraduate through VTU E-resource Consortium.</p>
				
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="col-lg-12 col-md-12 col-sm-12">
					 <div clas="row">
							
							<table class="table table-striped course_table">

												<tr>
													<th>SL No. </th>
													<th>Publishers</th>
													<th>E-Portal</th>
													<th>No. of e- Resources</th>
												</tr>
												
												<#assign count = 1>
											<#list libraryEresourcesList as libraryEresource>	
												<tr>
													<td class="col_eresource">${count!}</td>
													<td class="col_eresource"><img class="eresource_logo" alt="img" src="${libraryEresource.imageURL!}"></td>
													<td class="col_eresource">${libraryEresource.heading!}</td>
													<td class="col_eresource">${libraryEresource.content!}</td>
													
													<#assign count = count + 1>
												</tr>
											</#list>
												
											
												
												
								</table>				
					</div>
					
						
					</div>

				
						
				</div>
				
				<h3>Copyright Restrictions</h3>
					<p>All the users of library resources of KSIT must adhere to copyright laws of publishers
					 and consortia. No copyrighted work may be copied, published, disseminated, displayed,
					  performed, without permission of the copyright holder except in accordance with fair
					   use or licensed agreement. This includes DVDs/CDs and other copyrighted material. 
					   Using these resources for commercial purpose, systematic downloading, copying or 
					   distributing of information is prohibited. KSIT may terminate the online access of 
					   users who are found to have infringed copyright.</p>
					   
				<div class="col-lg-12 col-md-12 col-sm-12">
						
						<table class="table table-striped course_table">
							<thead>
							  <tr>          
								<th>Do's</th>
								<th>Don'ts</th>
								
								
							  </tr>
							</thead>
							
							<tbody>
							  <tr>          
								<td>Can make limited print/ electronic copies.</td>
								<td>Cannot share resources with non KSIT community.</td>								
							  </tr>
							  
							  <tr>
								<td>Can use for teaching, research, discussion, and other academic purposes.</td>          
								<td>Cannot do systematic or substantial downloading, copying, printing etc.</td>                    
								
							  </tr>
							  
							  <tr>
								<td>Can share with professional community.</td>          
								<td>Cannot publish in other website/blogs/forums.</td>                    
								
							  </tr>
							  
							  
							</tbody>
						  </table>
					</div>
					
					<!-- <h3>BEST PRACTICES</h3>
							
					<ul class="list">
                    	<li>Discussion room is provided for group learning.</li>
                    	<li>The library provides Digital Library where students can access institutional 
                    	repository, electronic resources, Guest Lectures and video lectures.</li>
                    	<li>Book Exhibition will be organized once in a year.</li>
                    	<li>Extended library opening hours as per the students' requirements.</li>
                    	<li>The information literacy program is conducted every year for our students.</li>
                    	<li>To receive an online feedback from students with regard to facilities, resources, and services.</li>
                    	<li>24/7 Access to Electronic Resources through remote access.</li>
                    	<li>Web OPAC is available for our students and faculty members to check the availability of 
                    	resources and they can also login to their personal account to see which books are borrowed 
                    	from the library and its due date.</li>
                    	<li>One more book is issued to top 10 Branch Toppers every Semester</li>
                     </ul>
					-->			
			
			</div>
        </div>
        
	  
			   
          </div>
        </div>
	  
	 
	
	
	  
	  </div>
	  </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 
	 
	 
	 
</@page>