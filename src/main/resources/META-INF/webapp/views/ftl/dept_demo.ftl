<@page>


<style>
#header {
	display: none;
}

#footer{
	display: none;
}

*{ margin: 0; padding: 0; }

body { height: 3200px; font-size: 16px; font-family: 'Exo 2', sans-serif; background: #efefef; color: #555; }

.header { text-align: center; }

.header a { padding: 30px 0 0; display: block; font-size: 48px; text-decoration: none; color: #555; }
.header p { margin: 10px 0 40px 0; font-size: 18px; }
.container { max-width: 1200px; margin: 0 auto; }



</style>






<div class="header">
		<!--<a href="#"></a>-->
	
	<div class="stellarnav">
		<ul class="parent-menu">
		
			<li><a href="index.html">Home</a></li>
			
			<li><a href="">About</a>
		    	<ul class="child-menu">
					<li><a href="about.html">History</a></li>
                        <li><a href="administration.html">Administration</a></li>
                        <li><a href="academic_advisory_board.html">Academic Advisory Board</a></li>
                        <li><a href="ceo.html">CEO</a></li>
                        <li><a href="principal.html">Principal</a></li>
                        <li><a href="academic_governing_council.html">Academic Governing Council</a></li>
                        <li><a href="office_administration.html">Office Administration</a></li>
                        <li><a href="right_to_information.html">Right To Information</a></li>
                        <li><a href="${img_path!}/about/Mandatory disclosure 2020 - 21.pdf">Mandatory Disclosure</a></li>	
		    	</ul>
		    </li>		
		    
		    <li><a href="">Programs</a>
		    	<ul>
						
		    		<li><a href="#">UG Program</a>
		    			<ul>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
		    			</ul>
		    		</li>
		    		<li><a href="#">PG program</a>
		    			<ul>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
		    			</ul>
		    		</li>
		    	</ul>
		    </li>
		    
			<li>
		    	<ul>
					<li><a href="coursesOffered.html">Admission Details </a></li>
                     <li><a href="fees_structure.html">Fee Structure</a></li>							    		
		    	</ul>
		    </li>	
		    
		    <li><a href="">Facilities</a>
		    	<ul>
					<li><a href="about_library.html">Library</a></li>
					 <li><a href="sports.html">Sports</a></li>
                     <li><a href="hostel.html">Hostel</a></li>
                     <!-- <li><a href="transport.html">Transport</a></li> -->
                     <li><a href="canteen.html">Canteen</a></li>						    		
		    	</ul>
		    </li>		
		    	
		    <li><a href="">NAAC</a>
		    	<ul>
						<li><a href="#">How deep?</a>
		    			<ul>
				    		<li><a href="#">Deep</a>
				    			<ul>
				    				<li><a href="#">Even deeper</a>
				    					<ul>
				    						<li><a href="#">Item</a></li>
				    						<li><a href="#">Item</a></li>
				    						<li><a href="#">Item</a></li>
				    					</ul>
				    				</li>
				    				<li><a href="#">Item</a></li>
				    				<li><a href="#">Item</a></li>
				    				<li><a href="#">Item</a></li>
				    			</ul>
				    		</li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
		    			</ul>
		    		</li>
		    		<li><a href="#">Item</a>
		    			<ul>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
		    			</ul>
		    		</li>
		    		<li><a href="#">Item</a>
		    			<ul>
		    				<li><a href="#">Deeper</a>
		    					<ul>
		    						<li><a href="#">Item</a></li>
		    						<li><a href="#">Item</a></li>
		    						<li><a href="#">Item</a></li>
		    					</ul>
		    				</li>
		    				<li><a href="#">Item</a></li>
		    				<li><a href="#">Item</a></li>
		    				<li><a href="#">Item</a></li>
		    			</ul>
		    		</li>
		    		<li><a href="#">Here's a very long item.</a>
		    			<ul>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
				    		<li><a href="#">Item</a></li>
		    			</ul>
		    		</li>
		    	</ul>
		    </li>
		    
		    <li><a href="">IQAC</a>
		    	<ul>
					<li><a href="img/naac/IQAC Composition 2016.17 & 18.pdf" target="_blank">IQAC Composition 2016.17 &amp; 18</a></li>
                    <li><a href="img/naac/IQAC details.pdf" target="_blank">IQAC details</a></li>
                    <li><a href="img/naac/IQAC Minutes of the meeting.pdf" target="_blank">IQAC Minutes of the meeting</a></li>		    		
		    	</ul>
		    </li>	
		    
		    <li><a href="">Gallery</a>
		    	<ul>
					<li><a href="cse_gallery.html">Computer Science & Engineering</a></li>
                        <li><a href="ece_gallery.html">Electronics & Communication Engineering</a></li>
                        <li><a href="mech_gallery.html">Mechanical Engineering </a></li>
                        <li><a href="tele_gallery.html">Electronics & Telecommunication Engineering</a></li>
                        <li><a href="science_gallery.html">Science and Humanities</a></li>
                        <li><a href="sports_gallery.html">Sports</a></li>
                        <li><a href="placement_gallery.html">Placement</a></li>
                        <li><a href="library_gallery.html">Library</a></li>
                        <li><a href="nss_gallery.html">NSS</a></li>
                        <li><a href="redcross_gallery.html">Red Cross</a></li>
                        <li><a href="alumni_gallery.html">Alumni</a></li>
                        <li><a href="https://drive.google.com/drive/folders/1oQAdmBUuZ_vcGUWtuXm1-KkHFm82Dvvy?usp=sharing" target="_blank">Graduation Day</a></li>
                     	    		
		    	</ul>
		    </li>		
		    	
		     <li><a href="">Life at KSIT</a>
		    	<ul>
					<li><a href="alumni.html">Alumni</a></li>
                        <li><a href="clubs.html">Student Club</a></li>
                        <li><a href="ananya.html">Ananya</a></li>
                        <li><a href="nss.html">NSS</a></li>
                        <li><a href="redcross.html">Red Cross</a></li>	    		
		    	</ul>
		    </li>	
		    
		     <li><a href="">Placements</a>
		    	<ul>
					<li><a href="about_placement.html">About Placement</a></li>
                        <li><a href="training_programs.html">Training Programs</a></li>
                        <li><a href="internship.html">Internship</a></li>
                        <li><a href="companies.html">Companies</a></li>
                        <li><a href="placement_events.html">Events</a></li>
                        <li><a href="yearwise_placement.html">Yearwise</a></li>
                        <li><a href="higher_studiesGRE.html">Higher studies/GRE</a></li>	    		
		    	</ul>
		    </li>	
		    
		     <li><a href="">Achievements</a>
		    	<ul>
					<li><a href="vtu_ranks.html">Vtu Rankings</a></li>	    		
		    	</ul>
		    </li>	
		    
		     <li><a href="">Calendar</a>
		    	<ul>
					<li><a href="academic_calender.html">Academic Calendar</a></li>
                     <li><a href="http://vtu.ac.in/time-table-of-ug-pg-for-the-examination-june-july-2016/">Exam Time-table</a></li>		    		
		    	</ul>
		    </li>	
		    
			
		    <li><a href="contact.html">Contact</a></li>
		    
		    
		</ul>
	</div><!-- .stellarnav -->
</div>



  	
	 <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="vertical-tab padding" role="tabpanel">
			               
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab">Vision & Mission</a></li>
			                    <li role="presentation"><a href="#highlights" aria-controls="profile" role="tab" data-toggle="tab">Highlights</a></li>
			                    <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Section 3</a></li>
			                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>
			                </ul>
			                <!-- Nav tabs -->
			               
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                    
			                    <!-- vision and mission -->
			                    <div role="tabpanel" class="tab-pane fade in active" id="vision">
			                    
			                        <div class="aboutus_area wow fadeInLeft">
		  
										 <h3>COMPUTER Science AND Engineering</h3>
										 <p>Prepare students to find Computer Solutions for the society through research and entrepreneurship with professional ethics.</p>
										 
										 
										
				
											 
											 
									  </div>
			
			                    </div>
			                    <!-- vision and mission -->
			                    
			                    <!-- highlights -->
			                    
			                    <div role="tabpanel" class="tab-pane fade" id="highlights">
			                      
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>Highlights of department</h3>
				
									
								  </div>
			                    
			                    </div>
			                    
			                    <!-- highlights -->
			                    
			                    
			                    <div role="tabpanel" class="tab-pane fade" id="Section3">
			                        <h3>Section 3</h3>
			                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
			                    </div>
			
			
			                    <div role="tabpanel" class="tab-pane fade" id="Section4">
			                        <h3>Section 4</h3>
			                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
			                    </div>
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
  


    	
	 <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="vertical-tab padding" role="tabpanel">
			               
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab">Vision & Mission</a></li>
			                    <li role="presentation"><a href="#highlights" aria-controls="profile" role="tab" data-toggle="tab">Highlights</a></li>
			                    <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Section 3</a></li>
			                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>
			                </ul>
			                <!-- Nav tabs -->
			               
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                    
			                    <!-- vision and mission -->
			                    <div role="tabpanel" class="tab-pane fade in active" id="vision">
			                    
			                        <div class="aboutus_area wow fadeInLeft">
		  
										 <h3>COMPUTER Science AND Engineering</h3>
										 <p>Prepare students to find Computer Solutions for the society through research and entrepreneurship with professional ethics.</p>
										 
										 
										
				
											 
											 
									  </div>
			
			                    </div>
			                    <!-- vision and mission -->
			                    
			                    <!-- highlights -->
			                    
			                    <div role="tabpanel" class="tab-pane fade" id="highlights">
			                      
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>Highlights of department</h3>
				
									
								  </div>
			                    
			                    </div>
			                    
			                    <!-- highlights -->
			                    
			                    
			                    <div role="tabpanel" class="tab-pane fade" id="Section3">
			                        <h3>Section 3</h3>
			                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
			                    </div>
			
			
			                    <div role="tabpanel" class="tab-pane fade" id="Section4">
			                        <h3>Section 4</h3>
			                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
			                    </div>
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
  


  
	
</@page>