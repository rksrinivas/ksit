<@page>
	<!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
             <#list mechGalleryList as mechGallery>
				
					<a href="${mechGallery.imageURL!}" title="">
	                    <img class="gallery_img" src="${mechGallery.imageURL!}" alt="img" />
	                	<span class="view_btn">View</span>
	                </a>
				
				</#list>
            
            
                <a href="${img_path!}/mech/m1.jpg" title="This is Title">
                  <img class="gallery_img" src="${img_path!}/mech/m1.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m2.jpg" title="This is Title2">
                  <img class="gallery_img" src="${img_path!}/mech/m2.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m3.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/mech/m3.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m4.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/mech/m4.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m5.jpg" title="This is Title5">
                  <img class="gallery_img" src="${img_path!}/mech/m6.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m6.jpg">
                  <img class="gallery_img" src="${img_path!}/mech/m6.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m7.jpg">
                  <img class="gallery_img" src="${img_path!}/mech/m7.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m8.jpg">
                  <img class="gallery_img" src="${img_path!}/mech/m8.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                 <a href="${img_path!}/mech/m9.jpg" title="This is Title">
                  <img class="gallery_img" src="${img_path!}/mech/m9.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m10.jpg" title="This is Title2">
                  <img class="gallery_img" src="${img_path!}/mech/m10.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m11.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/mech/m11.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m12.jpg" title="This is Title4">
                  <img class="gallery_img" src="${img_path!}/mech/m12.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m13.jpg" title="This is Title5">
                  <img class="gallery_img" src="${img_path!}/mech/m13.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m14.jpg">
                  <img class="gallery_img" src="${img_path!}/mech/m14.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m15.jpg">
                  <img class="gallery_img" src="${img_path!}/mech/m15.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/mech/m16.jpg">
                  <img class="gallery_img" src="${img_path!}/mech/m16.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>