<@page>
	 <!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
            
             <#list placementGalleryList as placementGallery>
				
					<a href="${placementGallery.imageURL!}" title="">
	                    <img class="gallery_img" src="${placementGallery.imageURL!}" alt="img" />
	                	<span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
	                </a>
				
				</#list>
			
				
                <a href="${img_path!}/placements/placement_gallery/1.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/1.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/2.png" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/2.png" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/3.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/3.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/4.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/4.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/5.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/5.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/6.jpg">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/6.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/7.jpg">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/7.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/8.jpg">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/8.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                 <a href="${img_path!}/placements/placement_gallery/9.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/9.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/10.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/10.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/11.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/11.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/12.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/12.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/13.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/13.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/14.jpg">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/14.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/15.jpg">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/15.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/placements/placement_gallery/16.jpg">
                  <img class="gallery_img" src="${img_path!}/placements/placement_gallery/16.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>