<@page>
<#include "feedback/feedback_list_modal.ftl">
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  	<!--		<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>   -->
			  		<!--social media icons  -->

                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#if ccePageSliderImageList?has_content>
                     		<#list ccePageSliderImageList as ccePageSliderImage>
                     			<#if ccePageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${ccePageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">${ccePageSliderImage.heading}</p>
										<p class="slideInRight">${ccePageSliderImage.content}</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>
	                        <article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/cce/slider/cces1.JPG" alt="">                       
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/cce/slider/cces2.JPG" alt="">                          
	                        </article>
	                        
	                         <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/cce/slider/cces3.JPG" alt="">                          
	                        </article>

                           <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/cce/slider/cces4.JPG" alt="">                          
	                        </article>
						</#if>
                    
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 

<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 

<!-- upcoming and latest news list-->
      
<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at CCE
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
					
				<div class="col-md-12">
					<div class="row">

					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=ccePageLatestEventsList/>
									</div>
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
									<@eventDisplayMacro eventList=ccePageUpcomingEventsList/>
								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->
	
	<!-- welcome -->
	<div class="dept-title">
	   <h1>Welcome To Computer and Communication Engineering</h1>
	 <!--  <p class="welcome-text">Artificial intelligence (AI) is wide-ranging branch of computer science concerned with building smart machines capable of performing tasks that typically require human intelligence. Machine learning focuses on the development of computer programs that can access data and use it learn for themselves.</p> -->
	</div>
	<!-- welcome -->
	


<!--dept tabs -->
<section id="vertical-tabs">
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
         <div class="vertical-tab padding" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
               <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="dept-cart">Profile</a></li>
              <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision & Mission, PEO, PSO & PO</a></li>
         <!--   <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>  -->
              
                <li role="presentation"><a href="#professional_bodies" aria-controls="professional_bodies" role="tab" data-toggle="tab" class="dept-cart">Professional Bodies</a></li>
               <li role="presentation"><a href="#professional_links" aria-controls="professional_links" role="tab" data-toggle="tab" class="dept-cart">Professional Links</a></li>
              
              
               <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
               <li role="presentation"><a href="#pedagogy" aria-controls="pedagogy" role="tab" data-toggle="tab" class="dept-cart">Pedagogy</a></li>                                             
               <li role="presentation"><a href="#content-beyond-syllabus" aria-controls="content-beyond-syllabus" role="tab" data-toggle="tab" class="dept-cart">Content Beyond Syllabus</a></li>               
               
               <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Time Table</a></li>
               
               <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>
               
                              
             <!--  <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li> -->
               <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
              
               <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li>
              
              
               <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab" class="dept-cart">Achievers</a></li>
               <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
               <li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
             <!--  <li role="presentation"><a href="#library_dept" aria-controls="library" role="tab" data-toggle="tab" class="dept-cart">Departmental Library</a></li> -->
        <!--     <li role="presentation"><a href="#placement_dept" aria-controls="placement" role="tab" data-toggle="tab" class="dept-cart">Placement Details</a></li> 
               
               <li role="presentation"><a href="#higher_education" aria-controls="higher_education" role="tab" data-toggle="tab" class="dept-cart">Higher Education</a></li>  

           <li role="presentation"><a href="#student_club" aria-controls="student_club" role="tab" data-toggle="tab" class="dept-cart">Student Club</a></li> -->


              <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab" class="dept-cart">Internships & Projects </a></li>  
               
               <li role="presentation"><a href="#industrial_visit" aria-controls="industrial_visit" role="tab" data-toggle="tab" class="dept-cart">Industrial Visit</a></li>
             
             
               
               <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
               <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
               
               
         
  
              <li role="presentation"><a href="#project_exhibition" aria-controls="project_exhibition" role="tab" data-toggle="tab" class="dept-cart">Project Exhibition</a></li> 
               
               <li role="presentation"><a href="#mou-signed" aria-controls="mou-signed" role="tab" data-toggle="tab" class="dept-cart">MOUs Signed</a></li>
          <!--    <li role="presentation"><a href="#alumni-association" aria-controls="alumni-association" role="tab" data-toggle="tab" class="dept-cart">Alumni Association</a></li> --> 
               
               <li role="presentation"><a href="#social_activities" aria-controls="social_activities" role="tab" data-toggle="tab" class="dept-cart">Social Activities</a></li>
               
          <!--     <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="tab" class="dept-cart">Testimonials</a></li>  -->
                                 
               
               <!-- <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li> -->
            </ul>
            <!-- Nav tabs -->
            <!-- Tab panes -->
            <div class="tab-content tabs">
            
                  	
            	
               <!-- profile -->
               <div role="tabpanel" class="tab-pane fade in active" id="profile">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Profile</h3>
                   <!--  <p>Department of <a href="https://ksgi.edu.in/Best-College-For-Artificial-Intelligence-and-Machine-Learning" class="font-weight-600" target="_blank">Artificial Intelligence &amp; Machine Learning</a> (cce) is established in the year 2020.</p>  -->
						
						<p>Over the last decade, Internet has amalgamated with every part of our daily life. Every activity we perform today is touched upon by Internet in one way or other. Internet as a technology consists of two key components, namely, Computer and Communication systems. At the undergraduate level, these two subjects have generally been taught separately as two different engineering streams; a. Computer Science b. Communication Engineering. Study of Computers started about 50+ years ago whereas communication systems has evolved over 150 years period. With the onset of Internet in 1980's, Computer Networks as a subject to introduce Communication Engineering is being taught in Computer Science courses.
                  </p>
						
						<p>Study of Computer and Communication Engineering represents the amalgamation of these two branches bring out the core aspects of both these disciplines. In this course, a student will study basic and advance topics related both the branches, such as, Data Structure and Algorithms, Computer system organization, cloud computing, communication networks including wired, wireless and cellular telephony networks.
                  </p>

						<p>When students complete this Computer and Communication Engineer ing(CCE) course, they will acquire a strong foundation, and will have ample opportunities for their career path. For these students, placement opportunities are going to be available in Software Development, Computer Communication related industry, IoT based application industry, Sensor technology, Drones technology and many other areas. These students are likely to have multiple offers and can choose the right one that would interest them the most to follow their dreams.
                  </p>

                  
			       </div>
                  <h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
						<img class="hod-img img-responsive" src="/img/cce/cce_hod.jpg" alt="image">          
						<div class="qualification_info">
	                        <h3 class="qual-text">Dr. Chanda V Reddy</h3>
	                        <h4 class="qual-text">Professor & Head</h4>
	                        <h3 class="qual-text">B.E, M. Tech, Ph.D</h3>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>Computers are critical for communication and are becoming the focus mantle of information technology. Communication technology refers to all equipment and programs that are used to process and communicate information. 
                     </p>
						<p>The Department Infrastructure accomplishes and ropes in learning skills on the latest technology in the industry. The students are shaped with multi-dimensional skills covering all the required graduate attributes. The department encourages students to participate in inter college events also conduct technical and non- technical events to enhance their teamwork, Hardskills, and Softskills.
                  </p>
                  <p>Professional bodies like CSI, ISTE, IEI, and IEEE offers opportunities to acquire knowledge & expertise on state of art technology. The department provides numerous events for students to interact with Industry professionals and hone up their skills. This enables students to analyse real life problems and develop solutions for the same.  
                  </p>
                     </div>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     	<p>The department continues to engage with industry in many ways. We do provide consultancy
as well sign Memorandum of Understanding (MoU). The consultancy mechanism provides
an opportunity to faculty to work on current real-life situations that required efficient
technology solutions. The MoUs would enable our faculty and students to collaborate with
industry to develop prototypes and engage in research to find practical workable solutions.
                     </p>
                     </div>
                  </div>
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>UG Program : </h3>
                     <p>
                        Bachelor of Engineering (B.E.) in Computer & Communication Engineering (CCE) with an approved intake of 60 students and a lateral entry of 10% from 2nd year. </p>

                     <h3>Highlights : </h3>
                      <ul class="list">
                       		<li>Overall development of students and faculty through conduction of various activities.</li>
                       		<li>Interaction with Industry and Academia experts.</li>
                       		<li>Excellent teaching and learning environment.</li>
                       		<li>Experienced and dedicated faculty members.</li>
                       		<li>Adequate lab infrastructure and teaching aids.</li>
                       		<li>Effective mentoring.</li>
                       		
                       </ul>  
               <!--    <h3>Career Opportunities : </h3>
                     <p>A great career catalyst for professionals.</p>
                     <p>Opportunities in many areas, such as, Retail, Logistics, Robotics, Consumer services
(chatbots), Healthcare, Transport, Education, DÃ©fense, Drones, Cybersecurity, Sports, etc.</p>  -->
                     
                  </div> 
   
               </div>  
               <!-- profile -->
               
               <!-- vision and mission -->
               <div role="tabpanel" class="tab-pane fade in" id="vision">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Vision</h3>
				
						<ul class="list">
							<li>Harness students' creativity and self-belief to be successful global professionals with human touch
</li>
						</ul>
				<!--		<button type="button" style="float:right;" class="btn btn-info btn-sm" data-backgrop="static" data-toggle="modal" data-page="tele_page" data-target="#feedbackListModal">Feedback</button>  -->
						
				 		 <h3>Mission</h3>
						
						<ul class="list">
						<li>Use experiential learning approach to empower students with fundamentals of theory and practice.
</li>
						<li> Enable students to solve real life problems both at national and international level.
</li>
						<li>To promote entrepreneurship among students so as to be job creators.
</li>
						</ul>
					
			
					
						
					
						
						<h3>PROGRAM EDUCATIONAL OBJECTIVES (PEOs)</h3>	
						<p><b>PEO1:</b> To use technical tools for building solutions to complex engineering problems.
                  </p>
						<p><b>PEO2:</b> To promote collaborative learning and spirit of team work through multidisciplinary projects and diverse professional activities.
                  </p>
						<p><b>PEO3:</b> To enable graduates to pursue research, or have a successful career in academia or industries including entrepreneurship associated with Computer and Communication Engineering.
                  </p>
					
						<h3>PROGRAM SPECIFIC OUTCOMES (PSOs)</h3>
						<p><b>PSO1:</b> To understand and apply the concepts to design and develop solutions in computer and communication Engineering.
</p>
						<p><b>PSO2:</b> To use the inculcated experiential learning for research and develop inventive solutions for societal benefit while ensuring security with moral values and ethics.
</p>
							<h3>PROGRAM OUTCOMES (POs)</h3>	
                  <p><b>PO1 : </b>Engg Knowledge : Apply the knowledge of mathematics, science, engineering fundamentals, and engg. specialization to the solution of complex engineering problems. </p>
                  <p><b>PO2 : </b>Problem Analysis : Identify, formulate, research literature, and analyze engineering problems to arrive at substantiated conclusions using first principles of mathematics, natural, and engineering sciences. 
</p>
                  <p><b>PO3 : </b>Design/Development : Design solutions for complex engineering problems and design system components, processes to meet the specifications with consideration for the public health and safety, and the cultural, societal, and environmental considerations. 
</p>
                  <p><b>PO4 : </b>Conduct Investigations : Use research-based knowledge including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions. 
</p>
                  <p><b>PO5 : </b>Modern tool usage : Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations. 
</p>
                  <p><b>PO6 : </b>Engineer and Society : Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations. 
</p>
                  <p><b>PO7 : </b>Environment : Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development .
</p>
                  <p><b>PO8 : </b>Ethics : Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice .
</p>
                  <p><b>PO9 : </b>Individual and team work  : Function effectively as an individual, and as a member or leader in teams, and in multidisciplinary settings. 
</p>
                  <p><b>PO10 : </b>Communication : Communicate effectively with the engineering community and with society at large. Be able to comprehend and write effective reports documentation. Make effective presentations, and give and receive clear instructions. 
</p>
                  <p><b>PO11 : </b>Project Mgmt : Demonstrate knowledge and understanding of engineering and management principles and apply these to one's own work, as a member and leader in a team. Manage projects in multidisciplinary environments. 
</p>
                  <p><b>PO12 : </b>Lifelong learning : Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change. 
</p>						  
                  </div>
               </div>
               <!-- vision and mission -->
				               
               
               <!-- Research and development -->
      <!--         <div role="tabpanel" class="tab-pane fade" id="research">
                  <h3>Research and Development</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
               						 <h3>R & D Activities</h3>
                        
                        <#list cceResearchList as cceResearch>
                        	<p>${cceResearch.heading!}  <a href="${cceResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        <p>Renowned projects have been awarded with cash prizes by prestigious government and private bodies to encourage students.</p>
                        
                        <#list cceSponsoredProjectsList as cceSponsoredProjects>
                        	<p>${cceSponsoredProjects.heading!}  <a href="${cceSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        
                        <#list ccePhdPursuingList as ccePhdPursuing>
                        	<p>${ccePhdPursuing.heading!}  <a href="${ccePhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        
                        <h3>Ph.D Awardees</h3>
                        
                         <#list ccePhdAwardeesList as ccePhdAwardees>
                        	<p>${ccePhdAwardees.heading!}  <a href="${ccePhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                    
					
                     	
                     
                     </div>
                  </div>
               </div>   -->
               <!-- Research and development -->
               
               
               <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="row">
                     <h3>Time Table</h3>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list cceClassTimeTableList as cceClassTimeTable>              
                        <p>${cceClassTimeTable.heading!}  <a href="${cceClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list cceTestTimeTableList as cceTestTimeTable>
                        <p>${cceTestTimeTable.heading!}  <a href="${cceTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list cceCalanderList as cceCalander>              
                        <p>${cceCalander.heading!}  <a href="${cceCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--Time Table  -->
               
            <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>Syllabus</h3>
                     
                        <h3>B.E in CCE</h3>
                        
                        <#list cceUgSyllabusList as cceUgSyllabus>
                        <p>${cceUgSyllabus.heading!}  <a href="${cceUgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                        
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
             
               
               
               
               <!--News Letter  -->
              <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Newsletter</h3>
                        
                        <p>The main purpose of newsletter is to encourage students and faculties to involve themselves in overall development. Newsletter includes the vision-mission of college and department along with encouraging words from Principal & HOD.
                        </p>
						<p>News letter mainly projects the events conducted and attended by students & Staff. It also throws light on the latest technology and views of Alumni about the department.</p>
                        
                        
                        <#list cceNewsLetterList as cceNewsLetter>
                        <p>${cceNewsLetter.heading!}  <a href="${cceNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--News Letter  -->
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="row">
                     <h3>Faculty</h3>
                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Faculty</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name}</p>
                                 <p>${faculty.designation}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id!}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               <!--testimonials  -->
           <!--    <div role="tabpanel" class="tab-pane fade" id="testimonials">  -->
                  <!--slider start-->
             <!--     <div class="row">
                     <div class="col-sm-8">
                        <h3><strong>Testimonials</strong></h3>  -->
                        <!--<div class="seprator"></div>-->
                  <!--      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">  -->
                          
                           <!-- Wrapper for slides -->
                     <!--      <div class="carousel-inner">
                              <#list testimonialList as testimonial>
                              <div class="item <#if testimonial?is_first>active</#if>">
                                 <div class="row" style="padding: 20px">
                                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                    <p class="testimonial_para">${testimonial.content!}</p>
                                    <br>
                                    <div class="row">
                                       <div class="col-sm-2">
                                          <img src="${testimonial.imageURL!}" class="img-responsive" style="width: 80px">
                                       </div>
                                       <div class="col-sm-10">
                                          <h4><strong>${testimonial.heading!}</strong></h4>  -->
                                          <!--<p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                             <span>Officeal All Star Cafe</span>-->
                            <!--              </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </#list>  
                           </div>
                           <div class="controls testimonial_control pull-right">									            	
                              <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>    -->
                  <!--slider end -->
          <!--     </div>   -->
               <!--testimonials  -->
 
 
 			 <!-- Achievers -->
               <div role="tabpanel" class="tab-pane fade" id="achievers">
               	<div class="row">
               		 <h3>Department Achievers</h3>
                     <!--Department Achievers-->
		                  <#list cceDepartmentAchieversList as cceDepartmentAchievers>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cceDepartmentAchievers.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${cceDepartmentAchievers.heading!}</h3>	                      	 
			                       	 <p>${cceDepartmentAchievers.eventDate!} </p>
			                       	 <p> ${cceDepartmentAchievers.content!}</p>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list> 
		                    
		              <h3>List of FCD Students</h3>
		              <#list cceFcdStudentsList as cceFcdStudentsList>
                        <p>${cceFcdStudentsList.heading!}  <a href="${cceFcdStudentsList.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                      </#list>  
                    
               	</div>
               </div>
               <!-- Achievers -->

 				
 
               
                      <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="row">
               		
               		<h3>Infrastructure / Facilities</h3>
               		 <p>  
                           Computer and Communication Engineering Department has fully furnished State of Art
                           laboratories. All laboratories are well-equipped to run the academic laboratories courses.
                           Highly experienced Technical staff are available in all the laboratories to assist the
                           students to carry out their lab sessions.
                     </p>
                     <p><b>The Department has :</b></p>
                     <ul class="list">
                     <li>  Dedicated two laboratories, all are well-equipped with latest computers installed
                           with required software for a students to carry out lab sessions, LAN connectivity,
                           WiFi and uninterrupted power supply. Projectors are available for effective
                           explanation of concepts and programs related to academic laboratories courses,
                           technical seminar presentation, internship presentation and project phase reviews.
                           Analog and Digital Electronics Laboratory/Microcontroller and Embedded Systems
                           Laboratory equipment's like CRO, IC trainer kits, ARM7 LPC 2148 Microcontroller
                           kit, ALS - SDA - ARM7 are available.</li>
                     <li>  Two class rooms with a seating capacity of seventy and one fully furnished
                           Seminar hall with seating capacity of 120.</li>
                     
                     </ul>
               	
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
		                  <#list cceLabList as cceLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cceLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${cceLab.name!}</h3>	                      	 
			                       	 <p>${cceLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${cceLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
               
               
               
              <!-- Gallery  -->
               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
                  <h3>Gallery</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="gallerySLide" class="gallery_area">
                           <#list cceGalleryList as cceGallery>	
                           <a href="${cceGallery.imageURL!}" target="_blank">
                           <img class="gallery_img" src="${cceGallery.imageURL!}" alt="" />					                    			                   
                           <span class="view_btn">${cceGallery.heading!}</span>
                           </a>
                           </#list>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Gallery -->
               
                <!-- Dept Library  -->
               <div role="tabpanel" class="tab-pane fade" id="library_dept">
                  <h3>Departmental Library</h3>
                  <p>
                     Libraries play a vital role in the development of any society by enhancing the cause of education and academic research. They cater to the information needs of thousands of people.
                  </p>
                  <p>
                     The department has its own library which has more than 500 text books and reference
                     book catering to the needs of students as well as Teaching Staffs. The library preserves
                     previous year's project, internship reports, journals; dissertation and seminar reports and
                     the books contributed by Alumnis and these are available for students and staffs. Every
                     year 50 to 100 books are being added to the stock. The students and Staffs borrow
                     books from the library periodically. The department maintains separate register for
                     borrowing books.
                  </p>
                  <h2 class="titile">Collection Of books</h2>
                  <table class="table table-striped course_table">
                     <#list cceLibraryBooksList as libraryBooks>
                     <tr>
                        <th style="width:50%">${libraryBooks.heading!}</th>
                        <td>${libraryBooks.content!}</td>
                     </tr>
                     </#list>
                  </table>
               </div>
               <!--  Dept Library   -->
               
               <!-- Department placements -->
               <div role="tabpanel" class="tab-pane fade" id="placement_dept">
                  <h3>Placement Details</h3>
                  
                  <p>Placement Training will be provided to all semester students at the beginning of the semester.
                  </p>
                  <p>Placement Drives will be arranged for final year students from August. Company Specific
                  </p>
                  <p>Trainings will be arranged for final year students to grab more offers in their dream Companies.
                  </p>
                  <p> Technical training sessions will be provided to Pre-final Year students.
                  </p>
                  
                  
                  
                  
                  <#list ccePlacementList as ccePlacement>              
                  <p>${ccePlacement.heading!}  <a href="${ccePlacement.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>											
                  <!-- Department placements -->
                  
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                  
                    <h3>Events</h3>
                    
                    <#list cceEventsList[0..*3] as cceEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${cceEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${cceEvent.heading!}</h3>	                      	 
	                       	 <p>"${cceEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${cceEvent.eventDate!}  </span></p>
	                       	 <a class="btn btn-primary" href="cce_events.html">View more</a>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
  						
                        
                  </div>
                  <!-- Events  -->
                  
                      <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        
                          <#list cceEresourcesList as cceEresources>
	                    
	                        <p>${cceEresources.heading!}  
	                      	    <a href="//${cceEresources.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
                              
               <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     <h3>Teaching and Learning</h3>
                     <p>
                     The faculty of CCE department attends various MOOCS and training programs on
                     advanced topics, update their knowledge and skills, and gives additional inputs in the
                     classes. Further, the faculty conducts various innovative teaching and learning activities
                     inside and outside the classrooms to engage the students effectively and efficiently. The
                     teaching and learning activities conducted by the faculty for the improvement of student
                     learning includes:
                     </p>
                     <ul class="list">
                     <li>Teaching with simulations and animated videos</li>
                     <li>Assignments include Conduction of Poster Presentations, online and classroom
                           quizzes, surprise class tests, group discussions, seminars Miniprojects etc.</li>
                     <li>Usage of ICT and Google classrooms for posting assignments and lecture materials.</li>
                     <li>Usage of Google forms and Kahoot for online interaction, assessment and evaluation.</li>
                     </ul>
                     <p>
                        The instructional materials and pedagogical activities are uploaded in Google drive and
                        the associated links are made available on institutional website for student and faculty
                        access.
                     </p>
                     
                        <h3>Instructional Materials</h3>
                        <p>
                           Instructional Materials are made available for public access.
                        </p>
                        
                          <#list cceInstructionalMaterialsList as cceInstructionalMaterials>
	                    
	                        <p>${cceInstructionalMaterials.heading!}  
	                      	    <a href="//${cceInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                      
	                     
	                      <h3>Lab Manuals</h3>
                        
                          <#list cceLabManualList as cceLabManual>
	                    
	                        <p>${cceLabManual.heading!}  
	                      	    <a href="//${cceLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                       											
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
               <!-- pedagogy -->

                <div role="tabpanel" class="tab-pane fade" id="pedagogy">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Pedagogical Activities</h3>
                         <p>
                           Pedagogy is the method and practice of teaching an academic subject or theoretical
                           concept. Pedagogical skills involve being able to convey knowledge and skills in ways
                           that students can understand, remember and apply. Pedagogies involve a range of
                           techniques, including whole-class and structured group work, guided learning and
                           individual activity. Pedagogies focus on developing higher order thinking.
                         </p>
                        
                          <#list ccePedagogicalActivitiesList as ccePedagogicalActivities>
	                    
	                        <p>${ccePedagogicalActivities.heading!}  
	                      	    <a href="//${ccePedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogy Review Form</h3>
                         <p>
                           The Peer review forms associated with pedagogical activities are made available for
                           public access, peer review, critique and further development.
                         </p>
                        
                          <#list ccePedagogyReviewFormList as ccePedagogyReviewForm>
	                    
	                        <p>${ccePedagogyReviewForm.heading!}  
	                      	    <a href="//${ccePedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>        
                       											
                     </div>
                  </div>
               </div>
              
                <!-- pedagogy -->

                 <!-- content beyond syllabus -->

                <div role="tabpanel" class="tab-pane fade" id="content-beyond-syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Content Beyond Syllabus</h3>						
                        
                         <#list ccecontentbeyondsyllabusList as ccecontentbeyondsyllabus>
                        <p>${ccecontentbeyondsyllabus.heading!}  <a href="${ccecontentbeyondsyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	                 
                       											
                     </div>
                  </div>
               </div>
              
                <!-- content beyond syllabus -->
               
               
               
               <!-- MOU's signed -->
               <div role="tabpanel" class="tab-pane fade" id="mou-signed">
                <div class="row">
                  <h3>MOUs Signed</h3>
                  <p>
                     The Memorandum of Understanding details modalities and general conditions regarding
                     collaboration between the INDUSTRY and KSIT. Also facilititate for enhancing and
                     encouraging students for interactions between the Industry Experts, Scientists,
                     Research fellows and Faculty members for knowledge sharing with the following
                     objectives :
                  </p>
                  <ul class="list">
                  <li>Collaboration in conduction of conferences, workshops, seminars.</li>
                  <li>Provides industry exposure to KSIT students.</li>
                  <li>Mutually agreed guidance for student&#39;s projects works through internship and industrial training.</li>
                  <li>Provision given for the usage of institute infrastructure by industry like library, internet and computational facilities etc.</li>

                  </ul>
                  <#list cceMouSignedList as cceMouSigned>              
                  <p>${cceMouSigned.heading!}  <a href="${cceMouSigned.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- MOU's signed -->
               
                <!-- Alumni Association -->
               <div role="tabpanel" class="tab-pane fade" id="alumni-association">
                <div class="row">
                  <h3>Alumni Association</h3>
                  <p>CHIRANTHANA with a tagline of EVERLASTING FOREVER is the name given to KSIT ALUMNI ASSOCIATION, with a motto of uniting all the alumni members under the umbrella of KSIT and growing together forever.KSIT alumni association was started in the year 2014.The association provides a platform to their alumniï¿½s to share their knowledge & experience with the budding engineers through technical talks, seminars & workshops.</p>
                  <#list cceAlumniAssociationList as cceAlumniAssociation>              
                  <p>${cceAlumniAssociation.heading!}  <a href="${cceAlumniAssociation.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- Alumni Association -->
               
               
               <!-- Professional Bodies  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_bodies">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Bodies</h3>
                        <p>Department of Artificial Intelligence and Machine Learning of KSIT has various professional bodies. Many workshops, technical talks, Guest lectures are organized in
association with this professional bodies. This gives opportunity to students to enhance their knowledge on cutting edge technologies.</p>
                       <ul class="list">
                        <li><b>ISTE : </b> formulates &amp; generates goals &amp; responsibilities of technical education.
Seminars, workshops, conferences are organized on the topic of relevance to
technical education for engineering students as well as teachers. KSIT ISTE
Chapter was established on 22 nd March 2014.</li>
                        <li><b>The Institution of Engineers (IEI) : </b>is the national organization of engineers in
India. IEI in KSIT promotes an environment to enable ethical pursuits for
professional excellence. KSIT established IEI Life membership in Dec 2014</li>
                        <li><b>The Board for IT Education Standards (BITES) : </b>is an autonomous body and a
non-profit society promoted by the Government of Karnataka, in association with
educational institutions and IT Industries in Karnataka, in order to enhance the
quality of IT education and help build quality manpower for the IT industry. Set up
in June 2000, BITES is an ISO 9001:2015 certified organization.</li>
                     </ul>
                        <#list cceProfessionalBodiesList as cceProfessionalBodies>
                        <p>${cceProfessionalBodies.heading!}  <a href="${cceProfessionalBodies.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Professional Bodies   -->
           
           
           	<!-- Professional Links  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_links">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Links</h3>
                        <ul class="list">
                        <li><b>BITES : </b>The Board for IT Education Standards is a non-profit Society set up by the Karnataka
Government, in association with IT industries and educational institutions, in order to
enhance the quality of IT education and help build quality manpower for the IT industry.
The institute is registered institutional member of BITES since January 2004.
Workshops, Seminars, Guest Lectures, are regularly conducted for students. Faculty
development programs are conducted for faculties. The event details are published
regularly in BITES Communications and also in college magazine.</li>
                        <li><b>ISTE: </b> The Indian Society for Technical Education (ISTE) is the leading National Professional
non-profit making Society for the Technical Education System. The institute established
KSIT-ISTE Student Chapter in March 2014. Various events such as student Convention,
Faculty convention, Seminars, Guest Lectures are conducted under ISTE Chapter.</li>
                        <li><b>IEI : </b> The Institution of Engineers (India) [IEI] is a professional body to promote and advance
the engineering and technology. It was established in 1920.The institute established IEI
Institute Life membership in Dec 2014. Various events such as Faculty development
programs, workshops, Seminars, Guest Lectures are conducted under IEI.</li>
                        </ul>
                        
                         <#list cceProfessionalLinksList as cceProfessionalLinks>
	                    
	                        <p>${cceProfessionalLinks.heading!}  
	                      	    <a href="//${cceProfessionalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                												
                     </div>
                  </div>
               </div>
               <!-- Professional Links   -->
           
                 <!-- Higher Education  -->
               <div role="tabpanel" class="tab-pane fade" id="higher_education">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Higher Education</h3>
                        <#list cceHigherEducationList as cceHigherEducation>
                        <p>${cceHigherEducation.heading!}  <a href="${cceHigherEducation.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Higher Education   -->
           
              
              
                 <!-- Students Club  -->
               <div role="tabpanel" class="tab-pane fade" id="student_club">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Aenfinity</h3>
                     
                     
                        <h3>Club Activities</h3>
                        <#list cceClubActivitiesList as cceClubActivities>
                        <p>${cceClubActivities.heading!}  <a href="${cceClubActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>External Links (Club)</h3>	
                        
                         <#list cceClubExternalLinksList as cceClubExternalLinks>
	                    
	                        <p>${cceClubExternalLinks.heading!}  
	                      	    <a href="//${cceClubExternalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                        			
                        
                        							
                     </div>
                  </div>
               </div>
               <!-- Students Club   -->
           
               
                 <!-- Internship  -->
               <div role="tabpanel" class="tab-pane fade" id="internship">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Internship</h3>
                        <p>
                           Students are facilitated by providing list of companies /Industries, where they can
                           undergo 4 weeks of Internship/ Professional practice. College takes initiative to organize
                           in house internship by calling resource persons from industry. Student cumulates their
                           learning in a report on their internship/ professional practice and gives a presentation in
                           front of internship coordinator and the guide.
                        </p>
                        <#list cceInternshipList as cceInternship>
                        <p>${cceInternship.heading!}  <a href="${cceInternship.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                  
             			<h3>Projects</h3>
                        <#list cceProjectsList as cceProjects>
                        <p>${cceProjects.heading!}  <a href="${cceProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Mini-Projects</h3>                       
                        <#list cceMiniProjectsList as cceMiniProjects>
                        <p>${cceMiniProjects.heading!}  <a href="${cceMiniProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        											
                     </div>
                  </div>
               </div>
               <!-- Internship   -->
               
               <!-- Social Activities  -->
               <div role="tabpanel" class="tab-pane fade" id="social_activities">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Social Activities</h3>
                        <#list cceSocialActivitiesList as cceSocialActivities>
                        <p>${cceSocialActivities.heading!}  <a href="${cceSocialActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Social Activities  -->   
               
                 <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list cceFdpList as cceFdp>
                        <p>${cceFdp.heading!}  <a href="${cceFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
           
           
             <!-- Industrial Visit -->
               <div role="tabpanel" class="tab-pane fade" id="industrial_visit">
               	<div class="row">
               		 <h3>Industrial Visit </h3>
               		 	<p>
                           The department strives to offer a great source of practical knowledge to students by
                           exposing them to real working environment through Industrial visits. Regularly Industrial
                           visits are arranged to organizations like Indian Space Research Organization, Karnataka
                           State Load Dispatch Centre, All India Radio. Visits are arranged for students to view
                           project exhibitions Organized by Indian Institute of Science, Visvesvaraya Industrial and
                           Technological Museum, EMMA-Expo 2014 and so on. Also visits are arranged to places
                           like HAL Heritage Centre and Aerospace Museum.
							   </p>
		                  <#list cceIndustrialVisitList as cceIndustrialVisit>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cceIndustrialVisit.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${cceIndustrialVisit.name!}</h3>	                      	 
			                       	 <p>${cceIndustrialVisit.designation!}</p>
			                       	 <a class="btn btn-primary" href="${cceIndustrialVisit.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Industrial Visit -->
           
           
            <!-- Project Exhibition -->
               <div role="tabpanel" class="tab-pane fade" id="project_exhibition">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
               		 <p>The department conducts the project exhibition in the even semester of every academic year. Students are encouraged to present their projects and mini projects to the invited guests and evaluators. The best innovative projects are selected and are awarded.</p>
		                  <#list cceProjectExhibitionList as cceProjectExhibition>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${cceProjectExhibition.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${cceProjectExhibition.name!}</h3>	                      	 
			                       	 <p>Description: ${cceProjectExhibition.designation!}</p>
			                       	 <a class="btn btn-primary" href="${cceProjectExhibition.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Project Exhibition -->
           
               
                  
                  
                  <!--  -->
                  <div role="tabpanel" class="tab-pane fade" id="Section4">
                     <h3>Section 4</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
                  </div>
                  <!--  -->
                  
               </div>
            </div>
         </div>
      </div>
</section>
<!--dept tabs -->
	


	
   
   
   
    	
	 
</@page>