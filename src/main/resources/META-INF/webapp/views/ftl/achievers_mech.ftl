<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <!-- start blog archive  -->
              <div class="row">
              
               <!-- start single blog archive -->
              	 
              	 <#list mechDepartmentAchieversList as mechDepartmentAchievers>
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${mechDepartmentAchievers.imageURL!}">
                      </a>
                    </div>
                    <h2 class="blog_title">${mechDepartmentAchievers.heading!}</h2>
					
                    <div class="blog_commentbox">
                      <p>${mechDepartmentAchievers.eventDate!}</p>
                                            
                    </div>
					
                    <p class="blog_summary">${mechDepartmentAchievers.content!}</p>
					
					<!--<a class="blog_readmore" href="#">Read More</a>-->
                    
                  </div>
                </div>
                
                </#list>
                <!-- start single blog archive -->
			  
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/mech/mech_staff/mechanical.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Dr. K Rama Narasimha </h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					Dr. K Rama Narasimha was nominated by VTU as Board of Examination member in Mechanical Engineering during the year 2016-2017.
					
					</p>
                    <br/>
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
                
				
				
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/mech/best project award 2017.jpeg">
                      </a>
                    </div>
                    <h2 class="blog_title">Optimization of Tesla Turbine</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					The project titled �Optimization of Tesla Turbine� guided by Mr. M Umashankar, Associate Professor, was awarded as Best Project during the year 2016-17. The project team members are Abhijith Magal, Anirudh.V, Hazaifuddin, Kushaal P.
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				  <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/mech/kushal.jpeg">
                      </a>
                    </div>
                    <h2 class="blog_title">Best ISTE Student</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					Mr. Kushal K Bharadwaj of 7th semester was awarded as �Best ISTE Student� for the year 2017.
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				
				
				
              </div>
              <!-- end blog archive  -->
             
            </div>
          </div>
          <!-- End course content -->

         
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>