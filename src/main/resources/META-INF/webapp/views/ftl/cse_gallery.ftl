<@page>
	     <!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
				 <#list cseGalleryList as cseGallery>
				
					<a href="${cseGallery.imageURL!}">
					  <img class="gallery_img" src="${cseGallery.imageURL!}" alt="" />					                    			                   
					  <span class="view_btn"><#if cseGallery.heading?has_content>${cseGallery.heading}</#if></span>
					</a>
				
				</#list>
				
				
				
                <a href="${img_path!}/cse/p1.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p1.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p2.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p2.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p3.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p3.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p4.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p4.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p5.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p5.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p6.jpg">
                  <img class="gallery_img" src="${img_path!}/cse/p6.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p7.jpg">
                  <img class="gallery_img" src="${img_path!}/cse/p7.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p8.jpg">
                  <img class="gallery_img" src="${img_path!}/cse/p8.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                 <a href="${img_path!}/cse/p9.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p9.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p10.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p10.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p11.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p11.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p12.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p12.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p13.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/cse/p13.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p14.jpg">
                  <img class="gallery_img" src="${img_path!}/cse/p14.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p15.jpg">
                  <img class="gallery_img" src="${img_path!}/cse/p15.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/cse/p16.jpg">
                  <img class="gallery_img" src="${img_path!}/cse/p16.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>