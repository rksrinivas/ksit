<@page>
	    <!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
             <#list eceGalleryList as eceGallery>
				
					<a href="${eceGallery.imageURL!}" title="">
	                    <img class="gallery_img" src="${eceGallery.imageURL!}" alt="img" />
	                	<span class="view_btn">View</span>
	                </a>
				
				</#list>
            
            
                <a href="${img_path!}/ece/e1.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/ece/e1.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e2.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/ece/e2.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e3.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/ece/e3.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e4.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/ece/e4.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e5.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/ece/e5.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e6.jpg">
                  <img class="gallery_img" src="${img_path!}/ece/e6.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e7.jpg">
                  <img class="gallery_img" src="${img_path!}/ece/e7.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e8.jpg">
                  <img class="gallery_img" src="${img_path!}/ece/e8.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                 <a href="${img_path!}/ece/e9.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/ece/e9.jpg" alt="img" />
                <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e10.jpg" title="">
                  <img class="gallery_img" src="${img_path!}/ece/e10.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e11.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/ece/e12.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e12.jpg" title="This is Title4">
                  <img class="gallery_img" src="${img_path!}/ece/e12.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e13.jpg" title="This is Title5">
                  <img class="gallery_img" src="${img_path!}/ece/e13.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e14.jpg">
                  <img class="gallery_img" src="${img_path!}/ece/e14.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e15.jpg">
                  <img class="gallery_img" src="${img_path!}/ece/e15.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
                <a href="${img_path!}/ece/e16.jpg">
                  <img class="gallery_img" src="${img_path!}/ece/e16.jpg" alt="img" />
                  <span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
                </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>