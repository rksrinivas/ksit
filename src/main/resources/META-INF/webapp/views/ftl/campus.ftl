<@page>



     <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about_campus">
		
		
		
      <div class="container">
	  
        <div class="row">
		
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
		 <h2 class="titile">Campus</h2>
          <div class="aboutus_area wow fadeInLeft">
			
            
            <p>K S Institute of Technology has a best in class high-fi infrastructure completely surrounded by CCTV cameras. Seminar halls, lab facilities and placement cells are equipped with world class amenities. Campus is completely wifi enabled and easily accessible by each and every students for the betterment of their studies.</p>


            <p>K S Institute of Technology believes in preparing students through the Choice Based Credit System (CBCS). The CBCS pattern of education has been introduced in all programs to facilitate students to opt for subjects of their choice in addition to the core subjects of the study and prepare them with required skills. It also provides opportunities for students to earn more credits and there by acquiring additional Proficiency Certificates.</p>
            <p>Highly qualified, experienced faculty members and scholars along with industrial and business experts have contributed towards mentoring of each and every student individually. K S Institute of Technology strives to strengthen its team of committed faculty members and staff who ensure that this journey of enlightenment progresses smoothly for every student.The ever evolving collection of books, journals and digital content in the library and the latest IT infrastructure, ensure that students have information in every form at their disposal to explore. K S Institute of Technology  is renowned for its well equipped laboratories that are well equipped which aid students in their learning and research. The custom-built teaching facilities and classrooms, the indoor and outdoor sports and cultural facilities, the multi-cuisine food court, and campus stores provide students an-encouraging learning environment.</p>
			
			
			
		  
		  </div>
        </div>
		
		</div>
		</div>
	</section>
	
	
    <!--=========== END ABOUT US SECTION ================--> 
	
	
	
	
   
 
     <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_classroom"  style="background:url('${img_path!}/slider/classroom.jpg');background-attachment:fixed;background-repeat:no-repeat;background-size:100% 100%;">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">

				 	<h1 class="center">Class Rooms</h1>
				 	<!-- <p  class="button"><a href="#">read more</a></p> -->
				 	
				 	

				 </div>
				</div>

			</div>

				</div>

			</div>
		</div>

		</section>

		<!--content SECTION-->

  
	
		  <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about_classroom">
		
		
		
      <div class="container">
	  
        <div class="row">
		
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
		 <!-- <h2 class="titile">Class Room</h2> -->
          <div class="aboutus_area wow fadeInLeft">
			   
      <p></p>

		  </div>
        </div>
		
		</div>
		</div>
	</section>
	
  <!--=========== END ABOUT US SECTION ================--> 

    <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_labs" style="background:url('${img_path!}/slider/labs.jpg');background-attachment:fixed;background-repeat:no-repeat;background-size:100% 100%;">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">

				 	<h1 class="center">Labs</h1>
				 	<!-- <p  class="button"><a href="#">read more</a></p> -->
				 	
				 	

				 </div>
				</div>

			</div>

				</div>

			</div>
		</div>

		</section>

		<!--content SECTION-->

  
	
		  <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about_labs">
		
		
		
      <div class="container">
	  
        <div class="row">
		
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
		 <!-- <h2 class="titile">Labs</h2> -->
          <div class="aboutus_area wow fadeInLeft">
			
            
            <p></p>
			
			
			
		  
		  </div>
        </div>
		
		</div>
		</div>
	</section>
	
	
    <!--=========== END ABOUT US SECTION ================--> 



	
       <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_seminarhalls" style="background:url('${img_path!}/slider/seminar.jpg');background-attachment:fixed;background-repeat:no-repeat;background-size:100% 100%;">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">

				 	<h1 class="center">Seminar Halls</h1>
				 	<!-- <p  class="button"><a href="#">read more</a></p> -->
				 	
				 	

				 </div>
				</div>

			</div>

				</div>

			</div>
		</div>

		</section>

		<!--content SECTION-->

  
	
		  <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about_seminarhalls">
		
		
		
      <div class="container">
	  
        <div class="row">
		
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
		 <!-- <h2 class="titile">Seminar Halls</h2> -->
          <div class="aboutus_area wow fadeInLeft">
			
            
            <p></p>
			
			
			
		  
		  </div>
        </div>
		
		</div>
		</div>
	</section>
	
	
    <!--=========== END ABOUT US SECTION ================--> 


         <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_placementcell" style="background:url('${img_path!}/slider/placements.jpg');background-attachment:fixed;background-repeat:no-repeat;background-size:100% 100%;">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">

				 	<h1 class="center">Placement cell</h1>
				 	<!-- <p  class="button"><a href="#">read more</a></p> -->
				 	
				 	

				 </div>
				</div>

			</div>

				</div>

			</div>
		</div>

		</section>

		<!--content SECTION-->

  
	
		  <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about_placementcell">
		
		
		
      <div class="container">
	  
        <div class="row">
		
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
		 <!-- <h2 class="titile">Placement Cell</h2> -->
          <div class="aboutus_area wow fadeInLeft">
			
            
            <p></p>
			
			
			
		  
		  </div>
        </div>
		
		</div>
		</div>
	</section>
	
	
    <!--=========== END ABOUT US SECTION ================--> 


     <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_sports"  style="background:url('${img_path!}/slider/sports.jpg');background-attachment:fixed;background-repeat:no-repeat;background-size:100% 100%;">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">

				 	<h1 class="center">Sports</h1>
				 	<!-- <p  class="button"><a href="#">read more</a></p> -->
				 	
				 	

				 </div>
				</div>

			</div>

				</div>

			</div>
		</div>

		</section>

		<!--content SECTION-->

  
	
		  <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about_sports">
		
		
		
      <div class="container">
	  
        <div class="row">
		
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
		 <!-- <h2 class="titile">Sports</h2> -->
          <div class="aboutus_area wow fadeInLeft">
			
            
            <p></p>
			
			
			
		  
		  </div>
        </div>
		
		</div>
		</div>
	</section>
	
	
    <!--=========== END ABOUT US SECTION ================--> 


   <!--=========== BEGIN STUDENTS content SECTION ================-->
    <section id="quick_facts_library"  style="background:url('${img_path!}/slider/library.jpg');background-attachment:fixed;background-repeat:no-repeat;background-size:100% 100%;">
      <div class="container">
       
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
		  
			<!--<marquee>-->
            <div class="quick_facts_content">  
			
              <div class="row">
			  
				 <div class="col-md-12" data-wow-delay="0.2s">

				 	<h1 class="center">Library</h1>
				 	<!-- <p  class="button"><a href="#">read more</a></p> -->
				 	
				 	

				 </div>
				</div>

			</div>

				</div>

			</div>
		</div>

		</section>

		<!--content SECTION-->

  
	
		  <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about_library">
		
		
		
      <div class="container">
	  
        <div class="row">
		
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
		<!--  <h2 class="titile">Library</h2> -->
          <div class="aboutus_area wow fadeInLeft">
			
            
            <p></p>
			
			
			
		  
		  </div>
        </div>
		
		</div>
		</div>
	</section>
	
	
    <!--=========== END ABOUT US SECTION ================--> 


















</@page>