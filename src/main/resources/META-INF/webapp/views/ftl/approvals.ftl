<@page>
<section id="about">
	<div class="container-fluid">
		<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<h3>Affiliations and Approvals</h3>
					<div class="aboutus_area wow fadeInLeft">
						<#list approvalsMouSignedList as approvalsMouSigned>
							<p>${approvalsMouSigned.heading!}  <a href="${approvalsMouSigned.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
						</#list>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</@page>