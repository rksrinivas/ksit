<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive_admin">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  <!-- start blog archive  -->
              <div class="row">
                <!-- start single blog archive -->
                <div class="col-lg-6 col-6 col-sm-8">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/administration/principal.jpg">
                      </a>
                    </div>
                    <h3 class="blog_title" style="text-align:center;">Dr. K.V.A. Balaji</h3>
					<h4 class="blog_title" style="text-align:center;">Chief Executive Officer</h4>
					<h5 class="blog_title" style="text-align:center;">K S Group of Institutions</h5>
                    <!--<p class="blog_summary">Duis erat purus, tincidunt vel ullamcorper ut, consequat tempus nibh. Proin condimentum risus ligula, dignissim mollis tortor hendrerit vel. Aliquam...</p>
                    <a class="blog_readmore" href="events-single.html">Read More</a>-->
                  </div>
                </div>
                <!-- End single blog archive -->
				<div class="container-fluid">
				<p>Dear Students,</p>
				<p>Kammavari Sangham (1952) Group of Institutions, having its presence in Bengaluru, aims to serve the cause of quality 
				technical education and is dedicated to impart sound education and training in a few popular branches of engineering. 
				The group strives to develop a holistic professional who is equipped to face the modern day challenges both in his professional 
				as well as personal fronts. The group has created an ambience among its institutions that nurture all round development of personality
				 and character with excellent staff - student bondage that instills self-confidence and harbors the joy of 
				learning, experiencing and doing things that focus on social issues.</p>
				<br/>
				
				<p>I welcome all our students, parents and other stakeholders to join this movement of training and nurturing 
				quality engineering professionals with a human touch to man our organizations and make our society a much refined 
				place to live.</p>
				<br/>
				
				<p>I wish everyone in our system the very best in their efforts to achieve the goals of the group.</p>
			</div>
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
	<!--content -->
	<!--<section id="courseArchive" style="margin-top:0px;">
      <div class="container">
	  
	  <div class="row">
					
					<div class="col-lg-12 col-md-12 col-sm-12">
					
					<h1>Profile</h1>
					
                    <blockquote>
					
                      <span class="fa fa-quote-left"></span>
					  
					  <h3 class="course_heading">Name:  <span> Dr. T.V.Govindaraju </span></h3>

						<h3  class="course_heading">Designation: <span>Principal and Director</span></h3>

						<h3 class="course_heading">Qualification </h3> 
							<ul class="list">
							<li></li>
							</ul>
						
						</h3>
                        
						<h3 class="course_heading">Area of Specialization  </h3>
						<ul class="list">
							<li>Machine Design</li>
						</ul>
						
                    </blockquote>
                    
						<div class="single_course wow fadeInUp">
						
						<h3 class="course_heading">Work Experience</h3>
                    
							<div class="singCourse_content">
							
								<p class="course_content"><span class="fa fa-angle-double-right"></p>
												
							</div>
					   
					  </div>
					  
					</div>
					</div>
	  </div>
	  
	  </section>-->
	  
	  <!--content-->
	  
	<!--  <section id="courseArchive" style="margin-top:0px;">
      <div class="container">
	  
			<div class="row">
					
					<div class="col-lg-12 col-md-12 col-sm-12">
					
                    <blockquote>
					
                      <span class="fa fa-quote-left"></span>
					  
					  <h3>Academic Excellence</h3>
					  
					  <ul class="list">
						<li>Guiding 3 Ph.D students (VTU)</li>
					  </ul>
					  
					  <h3>Publications</h3>
					  <ul class="list">
						<li>2 International Journal Publications</li>
						<li>4 International Conference Presentations</li>
						<li>5 National Conference Presentations</li>
					  </ul>
					  
					  <h3>Membership</h3>
					  <ul class="list">
						<li>Institution of Engineers (India) � FIE-018233-4</li>
						<li>Indian Society for Technical Education � Life Member � 8505</li>
					  </ul>

					</blockquote>
			</div>
		</div>
	 </section>-->

</@page>	