<@page>

<style>
	.modal{
		top:0;
		right:0;
		bottom:0;
		left:0;
		margin:auto;
		z-index:9999999;
		height:auto;
		width:100%;
	
	}
	
	
	.modal-backdrop{
		display:none;
		
	
	}
	
	.modal-header span{
	    top:3px;
		color:#000;
	
	}
	
	.career_form{
	
		
	}
	
	.modal-content{
		height:600px;
	}
	

</style>

<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3>Career</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote>
                    
				            
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
						<h3 class="title">Career at KSIT</h3>
	                    <p>We delightfully welcome you to join and be a part of us in our family. Send your updated resumes to 
						
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#careerModal">principal@ksit.edu.in</button></p>
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					
				
				 
            </div>
          </div>
          <!-- End course content -->
          
          
          <!-- boostrap modal-->
          
		<!-- Modal -->
			<div class="modal fade" id="careerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
			 aria-hidden="true">
			    <div class="modal-dialog" role="dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <h5 class="modal-title" id="exampleModalLabel">Submit Your resume</h5>
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                    <span aria-hidden="true">&times;</span>
			                </button>
			            </div>
			            <div class="modal-body">
			            
			            
			            		 <div class="career_form wow fadeInLeft">
           
					           	  <form action="" class="submitphoto_form" method="post">
					           	  
					                <input type="text" class="wp-form-control wpcf7-text" name="name" placeholder="Your name" required>
					                <input type="email" class="wp-form-control wpcf7-email" name="email" placeholder="Email address" required>          
					                <input type="text" class="wp-form-control wpcf7-text" name="subject" placeholder="Subject" required>
					                <textarea class="wp-form-control wpcf7-textarea" name="description" cols="30" rows="10" placeholder="What would you like to tell us" required></textarea>
					               	<input type="file" class="wp-form-control wpcf7-file" name="resume" placeholder="Upload your resume" required>

					                
					                
					                
					              </form>
					              
					              
					           </div>
			            	
			            
			            </div>
			            <div class="modal-footer">
			            	<input type="submit" value="Submit" class="wpcf7-submit">
			                <button type="button" class="btn btn-secondary wpcf7-submit" data-dismiss="modal">Close</button>
			               
			            </div>
			        </div>
			    </div>
			</div>

         <!-- boostrap modal--> 
          
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->




</@page>