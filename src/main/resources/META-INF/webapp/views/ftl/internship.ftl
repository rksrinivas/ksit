<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3 class="text-center">Internship Details</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                    </blockquote>
                    
					
				<!-- start single course -->
				<#list placementInternshipList as internship>
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp placement_training">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Program: </span>	${internship.program!}</h3>
					<h3 class="singCourse_title"><span>Semester: </span> ${internship.semester!}</h3>
					<h3 class="singCourse_title"><span>Year: </span> ${internship.year?c!}</h3>
					<h3 class="singCourse_title"><span>Company: </span>	${internship.company!}</h3>
					<h3 class="singCourse_title"><span>Schedule: </span> ${internship.schedule!} </h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                </#list>
                <!-- End single course -->
					
				
			

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>