<@page>
	 
	<!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs_course">
      <div class="container">
        <div class="row courseArchive_content">
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="aboutus_area wow fadeInLeft">
            <h1> Right to Information</h1>
            
            <p>Right to Information Act (RTI) is an Act of the Parliament of India "to provide for setting 
            out the practical regime of right to information for citizens". An Act to provide for setting out 
            the practical regime of right to information for citizens to secure access to information under 
            the control of public authorities, in order to promote transparency and accountability in the working 
            of every public authority, the constitution of a Central Information Commission and State Information 
            Commissions and for matters connected therewith or incidental thereto.</p>
            
            <p>For Right to Information Act Guide: <a href="${img_path!}/about/rti_guide-1.pdf" target="_blank"> rti_guide.pdf</a></p>
            
            <p>For Application: <a href="${img_path!}/about/rti_application.doc" target="_blank">rti.application.pdf</a></p>
            
            <p>A person, who desires to obtain any information under RTI Act 2005, can submit prescribed
             application form. Application can be downloaded from college website and complete in all respects.</p>
			
			<p>The institute will make an endeavor to provide information within 30 days on payment of requisite fee of Rs.250/-. The time limit for providing information concerned to life and liberty of a person is 48 hours
			In case the information pertains to a third party, the time limit is 45 days.</p>
				
				<address>Dr. Sangappa 
					Public Relation Officer,
					Professor
					Department of Electronics and Communication Engineering,
					K.S. Institute of Technology, 
					Raghuvanahalli, Kanakapura Main Road
					Bengaluru-560109
					Phone: +91 9845238866
					Email: rti@ksit.edu.in
				</address>
				
				
			  
				
			   
			   
			    
			  
				
           

				
			</div>
        </div>
         
		
	  
	  </div>
	  </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 

 
	 
	 
</@page>