<@page>

   <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about">
	<div class="container">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">

				<div class="dept-title">
   				<h1>Anti Sexual Harassment Cell</h1>   
				</div> 

          <div class="aboutus_area wow fadeInLeft">
			
    				<p>
					KSIT has created a friendly ambience for all staff members and students in the campus. Issues related to work, human relation, harassment (both Men and Women) and injustice in any other form will all be addressed by Anti Sexual Harassment Cell. 
					</p>
					
				<!--	<p>Dear students please fill in the online Anti Ragging Affidavit by clicking the link: <a href="https://antiragging.in/affidavit_university_form.php"  id="antiragging_link" target="_blank">&#9656; https://antiragging.in/affidavit_university_form.php </a> </p> -->
					
					<h3 style="color:red;">Nodal officer Details:</h3>
					<p><b>Name: </b>Dr. Vijayalaxmi Mekali</p>
					<p><b>Email ID: </b> vijayalaxmi@ksit.edu.in</p>
					
					
					
					<h3 style="color:red;">POLICY STATEMENT</h3>
					<ul class="list">
					
						<li>KSIT is committed to providing an environment free from sexual harassment.</li>
						
						<li>KSIT believes that you should be afforded the opportunity to work in an environment free of sexual harassment. Sexual harassment is a form of misconduct that undermines the employment relationship. No employee, either male or female, should be subjected verbally or physically to unsolicited and unwelcome sexual overtures or conduct. Sexual harassment refers to behavior that is not welcome, that is personally offensive, that debilitates morale and, therefore, interferes with work effectiveness. Behavior that amounts to sexual harassment may result in disciplinary action, up to and including dismissal.</li>
						
						<li>Any and all complaints or allegations of sexual harassment will be investigated promptly. Appropriate, corrective action will be implemented based upon the results of the investigation in the event harassment in is found to have taken place. </li>
						
						<li>In response to the Supreme Court Guidelines in Visakha Judgment has developed Policyand Procedures designed to prevent sexual harassment, and to deal with any complaints which may arise. </li>

						<li>Whereas Sexual Harassment infringes the fundamental right of a woman to gender equality under Article 14 of the Constitution of India and her right to life and live with dignity under Article 21 of the Constitution which includes a right to a safe environment free from sexual harassment. </li>

						<li>And whereas the right to protection from sexual harassment and the right to work with dignity are recognized as universal human rights by international conventions and instruments such as Convention on the Elimination of all Forms of Discrimination against Women (CEDAW), which has been ratified by the Government of India. </li>
					
					</ul>	

					<h3 style="color:red;">DEFINITION OF SEXUAL HARASSMENT</h3>
					<blockquote>
					<span class="fa fa-quote-left"></span>
					Sexual Harassment includes such unwelcome sexually determined behavior as physical contact and advances, sexually coloured remarks, showing pornography and sexual demand, whether by words or actions. Such conduct can be humiliating and may constitute a health and safety problem; it is discriminatory when the woman has reasonable grounds to believe that her objection would disadvantage her in connection with her employment, including recruitment or promotion, or when it creates a hostile working environment.
					</blockquote>

					<p>There are usually three kinds of sexual harassment and the following examples are not exhaustive. Sexual harassment can be perpetrated upon members of the opposite gender or one's own gender.</p>
					<ul class="list">
					
					<h5 style="color:red;">Non-Verbal</h5>
					<li> Gestures</li>
					<li> Staring / leering</li>
					<li> Invading personal space</li>
					<li> Pin-ups</li>
					<li> Offensive publications</li>
					<li> Offensive letters / memos</li>
					<li> Unsolicited and unwanted gifts</li>

					<h5 style="color:red;">Verbal</h5>
					<li> Language of a suggestive or explicit nature</li>
					<li>Unwanted propositions</li>
					<li> Jokes of a sexual or explicit nature</li>
					<li> Use of affectionate names</li>
					<li> Questions or comments of a personal nature</li>

					<h5 style="color:red;">Physical</h5>
					<li> Deliberate body contact</li>
					<li> Indecent exposure</li>
					<li> Groping / fondling / kissing</li>
					<li> Coerced sexual contact</li>


					</ul>

					

					<h3 style="color:red;">SEXUAL HARASSMENT IS UNLAWFUL</h3>
				<p>Every employee shall have a right to be free from Sexual Harassment and the Right to work in an environment free from any form of Sexual Harassment.</p>
				<p>No employer or any person who is a part of the management or ownership, a supervisor or a co-employee of the KSIT shall, sexually harass an employee whether male or female, where he or she is employed; whether the harassment occurs in / at the workplace, or at a place where the said persons have gone in connection with the work or the workplace, or at any place whatsoever Sexual Harassment will amount to misconduct in employment and the staff service rules / regulations governing employment shall govern such misconduct, in addition to the provisions of this Act;</p>
				<p>KSIT will take all necessary and reasonable steps to prevent and ensure that no staff employed in the KSIT is subject to sexual harassment by any third party during the course of employment.</p>
				<p>Where any such Sexual Harassment occurs, the employer shall take all necessary steps to assist the aggrieved man/ woman to redress the act of Sexual harassment. No employee of KSIT shall sexually harass an outsider who visits the KSIT Society for a legitimate Purpose.</p>
				<p>No person shall sexually harass another person in the course of providing or offering to provide goods or services to that other person.</p>
				
				<h3 style="color:red;">PREVENTIVE STEPS</h3>
				<p>Consistent with the existing law under Vishaka, KSIT shall take all reasonable steps to ensure prevention of sexual harassment at work. Such steps shall include:</p>
				<p>Circulation of KSIT's policy in English/Hindi/vernacular in regional offices on sexual harassment to all persons employed by or in any way acting in connection with the work and/or functioning of KSIT.</p>
				<p>Sexual harassment will be affirmatively discussed at monthly meetings, workshops etc, Conduct or cause to carry out in-house training on sexual harassment and addressing complaints to staff as well as members of Women Cell.</p>
				<p>Guidelines will be prominently displayed to create awareness of the rights of female employees.</p>
				<p>Widely publicize that the Sexual Harassment is a crime and will not be tolerated. The employer will assist persons affected in cases of sexual harassment by outsiders Names and contact numbers of members of the internal complaint's committee will be prominently displayed in all the offices/projects.</p>
				
				<h3 style="color:red;">IF YOU ARE BEING HARASSED</h3>
				<p>Tell the harasser his/her behaviour is unwelcome and ask him/her to stop.</p>
				<p>Keep a record of incidents (date, time, locations, possible witnesses, what happened, your response). You do not have to have a record of events in order to file a complaint, but a record can strengthen your case and help you remember details over time.</p>
				
				<h3 style="color:red;">REPORTING A COMPLAINT</h3>
				<p>If an individual believes he or she is the victim of sexual harassment or retaliation, s/he is encouraged to report such complaint immediately. The Principal has designated the Women Cell to receive all complaints, verbal or written, of harassment on behalf of the organization.</p>
				<p>The Women Cell constituted will be appointed for a period of three years. A complaint under this Act may be lodged with ICC at the earliest point of time and in any case within 15 days of occurrence of the alleged incident. The complaint shall contain all the material and relevant details concerning the alleged Sexual Harassment.</p>
				<p>If the complainant feels that he or she cannot disclose his/ her identity for any reason, the complainant shall address the complaint to the Principal/Director and hand over the same in person or in a sealed cover.</p>
				<p>The Principal/Director shall retain the original complaint with him/her and send to the Women Cell for further enquiry.</p>
			
		  </div>
        </div>
		
		</div>
		</div>
	</section>


<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<!--<h3>Grievance</h3>-->
					<!--  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote> -->
                    
				<div class="dept-title">
   				<h1>Anti Sexual Harassment Committee</h1>   
				</div>            
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<!--	<h3 class="color_red">GRIEVANCE REDRESSAL COMMITTEE</h3>
	                    	<p>As per the directions of the Registrar, VTU, Belgaum and in accordance
	                    	 with the regulations of AICTE, a <strong>Grievance Redressal Committee</strong> has been established in our 
	                    	 college with the following --->
	                    	 <!--<a href="${img_path!}/pdf/KSIT_Grievance_Cell.pdf" target="_blank">members</a>
	                    	 .</p>-->
	                    <!--	<p>All aggrieved students, their parents & staff may approach the committee for assistance and solve
	                    	 their problems related to academics, resources and personal grievances, ifany.</p> -->
	                    	 
	                    	 	<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name</th>
												<th>Designation</th>
												<!--<th>Mobile</th> -->
												<th>Position in CICC</th>
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>1.</td>
												<td>Dr. Vijayalaxmi Mekali </td>
												<td>Prof & Head, AI&ML</td>
												<!--<td>9916915517 </td> -->
												<td>Co-ordinator</td>
											  </tr>
											  
											   <tr>          
												<td>2.</td>
												<td>Dr. Girish. T. R</td>
												<td>Dept of ME </td>
												<td>Member</td>
											  </tr>
											  
											   <tr>          
												<td>3.</td>
												<td>Ms. Sangeetha. V</td>
												<td>Dept Of ECE</td>
												<td>Member </td>
											  </tr>
											  
											 <!--  <tr>          
												<td>4.</td>
												<td>Mr. Harshavardhan. J. R	</td>
												<td>Dept Of CSE </td>
												<td>Member</td>
											  </tr> -->
											  
											   <tr>          
												<td>4.</td>
												<td>Ms. Preethi Kamath</td>
												<td>Dept of AI&ML </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>5.</td>
												<td>Ms. Ammu Bhuvana. D	</td>
												<td>Dept Of CSD </td>
												<td>Member</td>
											  </tr>
											  
											   <tr>          
												<td>6.</td>
												<td>Ms. Rajashree Byalal</td>
												<td>Dept of CSE-ICB</td>
												<td>Member</td>
											  </tr>
											  
											   <tr>          
												<td>7.</td>
												<td>Mr. Naveen. V </td>
												<td>Dept Of AS&H</td>
												<td>Member</td>
											  </tr>
											  
											  <tr>          
												<td>8.</td>
												<td>Students Representatives  </td>
												<td>One Per Department</td>
												<td>Members</td>
											  </tr>

											  
											
											 </tbody>
											 
													 
											 
											  
											  
											  
								 </table>
	                    	 
						                    
                    </div>
                    
                    
                    
                    
                   
                  </div>
                </div>
                <!-- End single course -->
				
					
				
				 
            </div>
          </div>
          <!-- End course content -->
          
         
		

        </div>
      </div>
    </section>
    
     <section id="reports">
		<div class="container">
        	<div class="row">
				<!-- start course content -->
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="dept-title">
   						<h1>Reports</h1>   
					</div>        
					
					<#list reportsList as report>
	                  <div class="row aboutus_area wow fadeInLeft">
	                  	<#if report.imageURL?has_content>
	                     	<div class="col-lg-6 col-md-6 col-sm-6">
	                        	<img class="img-responsive" src="${report.imageURL!}" alt="image" />	                       
	                     	</div>
	                     </#if>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${report.heading!}</h3>	                      	 
	                       	 <p>${report.content!}</p>
	                       	 <p><span class="events-feed-date">${report.eventDate!}  </span></p>
	                       	 <#if (report.reportURL)??>
	                       	 	<a class="title-red" href="${report.reportURL!}" target="_blank">Report</a>
	                       	 </#if>
	                     </div>
	                  </div>
	                  <hr />  
                    </#list>   
				</div>
          		<!-- End course content -->
			</div>
		</div>
	</section>  
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>