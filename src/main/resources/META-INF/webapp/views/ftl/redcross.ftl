<@page>

<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
           	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/red_cross/slider/s1.jpg" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated bounceIn" src="${img_path!}/red_cross/slider/s2.jpg" alt="">										
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/red_cross/slider/s3.jpg" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/red_cross/slider/s4.jpg" alt="">                        
                        </article>
                  
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 
		 
<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To YOUTH RED CROSS (YRC)</h1>
   <p class="welcome-text">K.S.I.T has been affiliated to the Youth Red Cross Society (Karnataka state Branch) Bengaluru in terms
      of Government of karnataka,VTU Order No:IRCS/YRC/VTU/BLRU/0009/2011-2012 dated 23/01/2012.</p>
</div>
<!-- welcome -->		 

	 <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="vertical-tab padding" role="tabpanel">
			               
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
			                    <li role="presentation"><a href="#guidelines" aria-controls="guidelines" role="tab" data-toggle="tab">Guidelines</a></li>
			                    <li role="presentation"><a href="#committee" aria-controls="committee" role="tab" data-toggle="tab">Committee</a></li>
			                    <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>
			                    
			                    <li role="presentation"><a href="#achievements" aria-controls="vision" role="tab" data-toggle="tab">Achievements</a></li>			                    
			                    <li role="presentation"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab">Reports</a></li>			                    
								<li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
			                    			                    
			                    
			                    
			                    <!--<li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Section 3</a></li>
			                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>-->
			                </ul>
			                <!-- Nav tabs -->
			               
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                	
			                	<!-- profile -->
			                	 <div role="tabpanel" class="tab-pane fade in active" id="profile">
			                	  <div class="row">
			                        <!--<h3>Profile</h3>-->
			                            <h3>YRC Programme Officer's Desk</h3>
						                  <div class="aboutus_area wow fadeInLeft">
						                     <div class="col-lg-6 col-md-6 col-sm-6">
						                        <img class="hod-img img-responsive" src="${img_path!}/nss/nss_head.jpg" alt="image" />	
						                       <div class="qualification_info">
						                       		<h3 class="qual-text">Naveen V</h3>
							                        <h4 class="qual-text">YRC Programme Officer</h4>
							                        <h3 class="qual-text">M.Sc</h3>
						                        </div>
						                    
						                     </div>
						                     <div class="col-lg-6 col-md-6 col-sm-6">	
						                     		<h3>POLICY</h3>												
													<p>The youth red cross is the most important component of Indian Red cross society 
													established under central act XV of 1920.YRC aims at inculcating the 7 important 
													fundamental principles of the Red cross movement </p>
												   <p>viz., Humanity, Impartiality, Neutrality, Independence, Voluntary service, Unity and University. The YRC is organized with a view to develop and deploy student youth resource for working towards the principles.
														</p>	
														
												
												 <p>It also aims at inculcating in Youth the ideals and practice of service specially in relation to:
						                      </p>
						                      <ul class="list">
						                     	<li>Taking care of their own health and that of others.</li> 
						                     	<li>Understanding and accepting civic responsibility.</li>
						                     	<li>Promotion of national and international understanding and goodwill by using their international link of the movement.</li>
						                     	<li>Promotion of volunteerism.</li>
						                      </ul>
														
																																 
						                     </div>
						                     
						                      <div class="col-lg-12 col-md-12 col-sm-12">	
						                     <h3>AFFILIATION</h3>
						                      <p>K.S.I.T has been affiliated to the Youth Red Cross Society (Karnataka state Branch) Bengaluru in terms
      									of Government of karnataka,VTU Order No:IRCS/YRC/VTU/BLRU/0009/2011-2012 dated 23/01/2012.</p>
					                     																		 
					                    	 </div>
						                     
						                   </div>
			                            
			                            
			                      </div>
			                    </div>
			                    <!-- profile -->
			                    
							 <!-- Events -->
			                  <div role="tabpanel" class="tab-pane fade" id="events">
			                    <div class="row">
			                    <h3>Events</h3>
			                    
			                    <#list redcrossEventList[0..*3] as redcrossEvent>
				                  <div class="row aboutus_area wow fadeInLeft">
				                     <div class="col-lg-6 col-md-6 col-sm-6">
				                        <img class="img-responsive" src="${redcrossEvent.imageURL!}" alt="image" />	                       
				                     </div>
				                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
				                     	 <h3>${redcrossEvent.heading!}</h3>	                      	 
				                       	 <p>"${redcrossEvent.content!}"</p>
				                       	 <p><span class="events-feed-date">${redcrossEvent.eventDate!}  </span></p>
				                       	 <a class="btn btn-primary" href="redcross_events.html">View more</a>
				                     </div>
				                  </div>
			                  
			                           <hr />  
			                    </#list>   
			  						
			                    </div>    
			                  </div>
			                  <!-- Events  -->
			                  
			                  
			                  <!-- Achievements -->
				               <div role="tabpanel" class="tab-pane fade" id="achievements">
				               	<div class="row">
				               		 <h3>Achievements</h3>
				                     <!--Department Achievers-->
						                  <#list redcrossAchievementsList as redcrossAchievements>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${redcrossAchievements.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h3>${redcrossAchievements.heading!}</h3>	                      	 
							                       	 <p>${redcrossAchievements.eventDate!} </p>
							                       	 <p> ${redcrossAchievements.content!}</p>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>  	                    
						          
				                    
				               	</div>
				               </div>
				               <!-- Achievements -->
		
			                  
			                      <!-- Gallery  -->
				               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
				               	<div class="row">
				                  <h3>Gallery</h3>
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <div id="gallerySLide" class="gallery_area">
				                           <#list redcrossGalleryList as redcrossGallery>	
				                           <a href="${redcrossGallery.imageURL!}" target="_blank">
				                           <img class="gallery_img" src="${redcrossGallery.imageURL!}" alt="" />					                    			                   
				                           <span class="view_btn">${redcrossGallery.heading!}</span>
				                           </a>
				                           </#list>
				                        </div>
				                     </div>
				                  </div>
				                  </div>
				               </div>
				               <!-- Gallery -->
				                    
	
				     
			                  
			                   <!-- objectives -->
			                    <div role="tabpanel" class="tab-pane fade" id="guidelines">
			                    	<div class="row">
			                           <h3>PROGRAMME GUIDELINES </h3>
										<p>Participation of students in Red Cross activities promotes understanding and accepting of civic responsibility and maintaining a spirit of friendliness.</p>
								                     <ul class="list">
								                        <li>Volunteer Orientation.</li>
								                        <li>Training on first- aid.</li>
								                        <li>Training on disaster preparedness and management.</li>
								                        <li>The training on minimum competencies.</li>
								                        <li>Motivation of blood donors.</li>
								                        <li>Observation of important days.</li>
								                        <li>YRC unit record.</li>
								                     </ul>	
								                     						                     
			                        </div>  
			                    </div>
			                  <!-- objectives -->
			                  
			                  <!--committee -->
			                    <div role="tabpanel" class="tab-pane fade" id="committee">
			                    	<div class="row">
			                			               <h3>YRC COMMITTEE MEMBERS </h3>
			
											   <table class="table table-striped course_table" style="text-align:left !important;">

												<tr>
													<th>Name</th>
													<th>Branch</th>
													
												</tr>
												<tr>
													<td>Mr.Umesh.S</td>
													<td>PED</td>													
												</tr>
												
												<tr>
													<td>Mr.MANJUNATH B R</td>
													<td>MED</td>													
												</tr>
												
												<tr>
													<td>Mr.SALEEM.S.TEVARAMANI</td>
													<td>ECE</td>													
												</tr>
												
												<tr>
													<td>Mr.HARSHAVARDHAN J R</td>
													<td>CSE</td>													
												</tr>
												
												<tr>
													<td>Mr.MANOJ KUMAR S</td>
													<td>ECE</td>													
												</tr>
												
												<tr>
													<td>MS.SHYLAJA.K.R</td>
													<td>BS&H</td>													
												</tr>
												
												<tr>
													<td colSpan="2">And Students  Representatives</td>													
												</tr>
												
											</table>
				
			                        </div     
			                    </div>
			                  <!--committee --> 
			                    
			                    
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
		 
	
</@page>