<@page>
		 
<!--=========== slider  ================-->
<!--<section id="home_slider">
   <div class="container" style="width:100%;"> -->
      <!-- Start Our courses content -->
   <!--   <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
    
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/nss/slider/nss1.JPG" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated bounceIn" src="${img_path!}/nss/slider/nss2.JPG" alt="">										
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss3.JPG" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss4.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss5.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss6.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss7.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss8.JPG" alt="">                        
                        </article>
                  
                     </div>  -->
                     <!-- Indicators -->
                <!--     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
						<li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                        <li data-target="#myCarousel" data-slide-to="6"></li>
						<li data-target="#myCarousel" data-slide-to="7"></li>
                        
                    </ol> -->
                     <!-- Indicators -->
                     
            <!--      </div>
               </div>
            </div>
            
         </div>
      </div>
   
   </div>
</section>  -->
<!--=========== slider end ================--> 
		 
<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To Internal Quality Assurance Cell (IQAC)</h1>
 <!--  <h3>Objective (As per NAAC)</h3>
   <p class="welcome-text">The primary aim of  IQAC is :</p> -->
   
</div>
<!-- welcome -->		 

	 <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="vertical-tab padding" role="tabpanel">
			               
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                <!--    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li> -->
			                    <li role="presentation" class="active"><a href="#objectives" aria-controls="objectives" role="tab" data-toggle="tab">Objectives</a></li>
			                    <li role="presentation"><a href="#committee" aria-controls="committee" role="tab" data-toggle="tab">Committee</a></li>
								<li role="presentation"><a href="#roles" aria-controls="roles" role="tab" data-toggle="tab">Roles and Responsibilities</a></li>
			                    <li role="presentation"><a href="#composition" aria-controls="composition" role="tab" data-toggle="tab">Composition</a></li>
								<li role="presentation"><a href="#minutesofmeeting" aria-controls="minutesofmeeting" role="tab" data-toggle="tab">Minutes Of Meeting</a></li>
								<li role="presentation"><a href="#bestpractices" aria-controls="bestpractices" role="tab" data-toggle="tab">Best Practices</a></li>
			                    <li role="presentation"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab">Annual Reports</a></li>
								<li role="presentation"><a href="#others" aria-controls="others" role="tab" data-toggle="tab">Other Reports</a></li>
								
							<!--	
								<li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>
								<li role="presentation"><a href="#achievements" aria-controls="vision" role="tab" data-toggle="tab">Achievements</a></li>			                  
			                    <li role="presentation"><a href="#gallery_dept" aria-controls="gallery_dept" role="tab" data-toggle="tab">Gallery</a></li>			                    
			                    			                    
			                    
			                    
			                    <li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Section 3</a></li>
			                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>-->
			                </ul>
			                <!-- Nav tabs -->
			               
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                	
			                	<!-- profile -->
			                <!--	 <div role="tabpanel" class="tab-pane fade in active" id="profile">
			                	  <div class="row">
			                        <h3>Profile</h3>
			                        <p>At KSIT NSS was officially launched in the year 2017 aimed to develop students personality through 
			                        social service. V T U sanctioned one unit (100 students )  to our college in the year 2018.</p>
			                            
			                            <h3>NSS Programme Officer's Desk</h3>
						                  <div class="aboutus_area wow fadeInLeft">
						                     <div class="col-lg-6 col-md-6 col-sm-6">
						                        <img class="hod-img img-responsive" src="${img_path!}/nss/nss_head.jpg" alt="image" />	
						                       <div class="qualification_info">
							                        <h3 class="qual-text">Naveen V</h3>
							                        <h4 class="qual-text">NSS Programme Officer</h4>
							                        <h3 class="qual-text">M.Sc</h3>
						                        </div>
						                    
						                     </div>
						                     <div class="col-lg-6 col-md-6 col-sm-6">	
						                     		<h3>MOTTO</h3>												
													<p>The Motto of NSS "Not Me But You", reflects the essence of democratic living and
													 upholds the need for self-less service. NSS helps the students development & 
													 appreciation to other person's point of view and also show consideration 
													 towards other living beings. The philosophy of the NSS is a good doctrine in
													  this motto, which underlines on the belief that the welfare of an individual
													   is ultimately dependent on the welfare of the society as a whole and therefore, 
													   the NSS volunteers shall strive for the well-being of the society.</p>																			 
						                     </div>
						                     
						                      <div class="col-lg-12 col-md-12 col-sm-12">													
													
								                     
								             
								                     
								                     																		 
								                     </div>
						                     
						                   </div>
			                            
			                            
			                      </div>
			                    </div> -->
			                    <!-- profile -->
			                    
							 <!-- Events -->
			                  <div role="tabpanel" class="tab-pane fade" id="events">
			                    <div class="row">
			                    <h3>Events</h3>
			                    
			                    <#list nssEventList[0..*3] as nssEvent>
				                  <div class="row aboutus_area wow fadeInLeft">
				                     <div class="col-lg-6 col-md-6 col-sm-6">
				                        <img class="img-responsive" src="${nssEvent.imageURL!}" alt="image" />	                       
				                     </div>
				                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
				                     	 <h3>${nssEvent.heading!}</h3>	                      	 
				                       	 <p>"${nssEvent.content!}"</p>
				                       	 <p><span class="events-feed-date">${nssEvent.eventDate!}  </span></p>
				                       	<a class="btn btn-primary" href="nss_events.html">View more</a>
				                     </div>
				                  </div>
			                  
			                           <hr />  
			                    </#list>   
			  						
			                    </div>    
			                  </div>
			                  <!-- Events  -->
			                  
			                  
			                  <!-- Achievements -->
				               <div role="tabpanel" class="tab-pane fade" id="achievements">
				               	<div class="row">
				               		 <h3>Achievements</h3>
				                     <!--Department Achievers-->
						                  <#list nssAchievementsList as nssAchievements>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${nssAchievements.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h3>${nssAchievements.heading!}</h3>	                      	 
							                       	 <p>${nssAchievements.eventDate!} </p>
							                       	 <p> ${nssAchievements.content!}</p>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>  	                    
						          
				                    
				               	</div>
				               </div>
				               <!-- Achievements -->
		
			                  
			                      <!-- Gallery  -->
				               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
				               	<div class="row">
				                  <h3>Gallery</h3>
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <div id="gallerySLide" class="gallery_area">
				                           <#list nssGalleryList as nssGallery>	
				                           <a href="${nssGallery.imageURL!}" target="_blank">
				                           <img class="gallery_img" src="${nssGallery.imageURL!}" alt="" />					                    			                   
				                           <span class="view_btn">${nssGallery.heading!}</span>
				                           </a>
				                           </#list>
				                        </div>
				                     </div>
				                  </div>
				                  </div>
				               </div>
				               <!-- Gallery -->
				               

							    <!-- Composition  -->
				               <div role="tabpanel" class="tab-pane fade" id="composition">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Annual Composition</h3>
				                        <#list annualCompositionList as annualComposition>
				                        	<p>${annualComposition.heading!}  <a href="${annualComposition.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				               <!-- Composition -->   

							    <!-- Minutes Of Meeting  -->
				               <div role="tabpanel" class="tab-pane fade" id="minutesofmeeting">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Minutes Of Meeting</h3>
				                        <#list minutesOfMeetingList as minutesOfMeeting>
				                        	<p>${minutesOfMeeting.heading!}  <a href="${minutesOfMeeting.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				               <!-- Minutes Of Meeting  -->  

							<!-- Best Practices  -->
				               <div role="tabpanel" class="tab-pane fade" id="bestpractices">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Best Practices</h3>
				                        <#list bestPractiseList as bestPractise>
				                        	<p>${bestPractise.heading!}  <a href="${bestPractise.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				            <!-- Best Practices  -->  
				                      
					         <!-- Reports  -->
				               <div role="tabpanel" class="tab-pane fade" id="reports">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Annual Reports</h3>
				                        <#list annualReportsList as annualReports>
				                        	<p>${annualReports.heading!}  <a href="${annualReports.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				               <!-- Reports  -->   

							<!-- Others  -->
				               <div role="tabpanel" class="tab-pane fade" id="others">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Other Reports</h3>
				                        <#list otherReportsList as otherReports>
				                        	<p>${otherReports.heading!}  <a href="${otherReports.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				            <!-- Others  -->   
			                    
				                    
	
				     
			                  
			                   <!-- objectives -->
			                    <div role="tabpanel" class="tab-pane fade in active" id="objectives">
			                    	<div class="row">
			                           <h3>Objective (As per NAAC) </h3>
													 <p>The primary aim of  IQAC is :</p>
								                     <ul class="list">
								                        <li>To develop a system for conscious, consistent, and catalytic action to improve the academic and administrative performance of the institution.</li>
								                        <li>To promote measures for institutional functioning towards quality enhancement through internalization of quality culture and institutionalization of best practices.</li>
								                     </ul>	
			                        </div>  
			                    </div>
			                  <!-- objectives -->
			                  
			                  <!--committee -->
			                    <div role="tabpanel" class="tab-pane fade" id="committee">
			                    	<div class="container-fluid">
			                			               <h3>IQAC COMMITTEE MEMBERS</h3>
			
											   <table class="table table-striped course_table" style="text-align:left !important;">
											   <thead>
											   <tr>
											   		<th>Sl. No.</th>
													<th>Name</th>
													<th>Designation</th>
													<th>Role</th>
													
												</tr>
												</thead>
												
												<#list facultyList as faculty>
													<tr>
														<td>${faculty?index+1}</td>
														<td>${faculty.name!}</td>
														<td>${faculty.designation!}</td>
														<td>${faculty.qualification!}</td>													
													</tr>
												</#list>
											</table>
											
				
			                        </div>     
			                    </div>
			                  <!--committee --> 


							  <!-- Roles and Responsibilities -->
			                    <div role="tabpanel" class="tab-pane fade" id="roles">
			                    	<div class="row">
			                           <h3>Strategies (As per NAAC)</h3>
													 <p>IQAC shall evolve mechanisms and procedures for </p>
								                     <ul class="list">
								                        <li>Ensuring timely, efficient, and progressive performance of academic, administrative, and financial tasks.</li>
								                        <li>The relevance and quality of academic and research programmes.</li>
														<li>Equitable access to and affordability of academic programmes for various sections of society.</li>
														<li>Optimization and integration of modern methods of teaching and learning.</li>
														<li>The credibility of evaluation procedures.</li>
														<li>Ensuring the adequacy, maintenance and proper allocation of support structure and services.</li>
														<li>Sharing of research findings and networking with other institutions in India and abroad.</li>
								                     </ul>	

										<h3>Functions (As per NAAC)</h3>
													 <p>Some of the functions expected of the IQAC are:</p>
								                     <ul class="list">
								                        <li>Development and application of quality benchmarks/parameters for various academic and administrative activities of the institution.</li>
														<li>Facilitating the creation of a learner-centric environment conducive to quality education and faculty maturation to adopt the required knowledge and technology for participatory teaching and learning process.</li>
														<li>Arrangement for feedback response from students, parents, and other stakeholders on quality-related institutional processes.</li>
														<li>Dissemination of information on various quality parameters of higher education.</li>
														<li>Organization of inter and intra-institutional workshops, seminars on quality related themes, and promotion of quality circles.</li>
														<li>Documentation of the various programs/activities leading to quality improvement.</li>
														<li>Acting as a nodal agency of the Institution for coordinating quality-related activities, including adoption and dissemination of best practices.</li>
														<li>Development and maintenance of institutional database through MIS for the purpose of maintaining /enhancing the institutional quality.</li>
														<li>Development of Quality Culture in the institution.</li>
														<li>Preparation of the Annual Quality Assurance Report (AQAR) as per guidelines and parameters of NAAC, to be submitted to NAAC.</li>
													</ul>	

										<h3>Benefits</h3>
													 <p>IQAC will facilitate / contribute</p>
								                     <ul class="list">
								                        <li>Ensure heightened level of clarity and focus in institutional functioning towards quality enhancement.</li>
														<li>Ensure internalization of the quality culture.</li>
														<li>Ensure enhancement and coordination among  various activities of the institution and institutionalize all good practices.</li>
														<li>Provide a sound basis for decision-making to improve institutional functioning.</li>
														<li>Act as a dynamic system for quality changes in HEIs.</li>
														<li>Build an organized methodology of documentation and internal communication.</li>
													</ul>

													<br>

										<p><b>Frequency of Meeting:</b> The IQAC should meet at least once every quarter.</p>
										<p><b>Membership duration:</b> The membership of nominated members shall be for a period of two years.</p>	
										<p><b>Quorum:</b> The quorum for the meeting shall be two-third of the total number of members. The agenda, minutes, and Action Taken Reports are to be documented with official signatures and maintained electronically in a retrievable format.</p>
										
										<h3>The role of coordinator (As per NAAC):</h3>
													 <p>The role of the coordinator of the IQAC is crucial in ensuring the effective functioning of all the members. The coordinator of the IQAC may be a senior person with expertise in quality aspects. She/he may be a full-time functionary or, to start with, she/he may be a senior academic /administrator entrusted with the IQAC as an additional responsibility. <b>Secretarial assistance</b> may be facilitated by the administration. It is preferable that the coordinator may have sound knowledge about the computer, its various functions, and its usage for effective communication.</p>

										<h3>Operational Features of the IQAC (As Per NAAC)</h3>
													 <p>Quality assurance is a by-product of ongoing efforts to define the objectives of an institution, to have a work plan to achieve them, and to specify the checks and balances to evaluate the degree to which each of the tasks is fulfilled. 
													 	Hence devotion and commitment to improvement rather than mere institutional control is the basis for devising procedures and instruments for assuring quality. The right balance between the health and growth of an institution needs to be struck. The IQAC has to ensure that whatever is done in the institution for education is done efficiently and effectively with high standards.
													  <b>In order to do this, the IQAC will have to first establish procedures and modalities to collect data and information on various aspects of institutional functioning</b>.</p>
													  <p>The coordinator of the IQAC and the secretary will have a major role in implementing these functions. The IQAC may derive major support from the already existing units and mechanisms that contribute to the functions listed above. The operational features and functions discussed so far are broad-based to facilitate institutions towards academic excellence and institutions may adapt them to their specific needs.</p>

										<h3>Monitoring Mechanism (As per NAAC)</h3>
												<p>The institutions need to submit yearly the Annual Quality Assurance Report (AQAR) to NAAC. A functional Internal Quality Assurance Cell (IQAC) and timely submission of Annual Quality Assurance Reports (AQARs) are the Minimum Institutional Requirements (MIR) to volunteer for second, third, or subsequent cycle accreditation.  During the institutional visit, the NAAC peer teams will interact with the IQACs to know the progress, functioning as well quality sustenance initiatives undertaken by them.</p>
												<p>The Annual Quality Assurance Reports (AQAR) may be part of the Annual Report. The AQAR shall be approved by the statutory bodies of the HEIs (such as Syndicate, Governing Council/Board) for the follow-up action for necessary quality enhancement measures.</p>
												<p>The Higher Education Institutions (HEI) shall submit the AQAR regularly to NAAC. The IQACs may create its exclusive window on its institutional website and regularly upload/ report on its activities, as well as for hosting the AQAR.</p>

			                        </div>  
			                    </div>
			                  <!-- Roles and Responsibilities -->
			                  
			           
			                    
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
  



	
</@page>