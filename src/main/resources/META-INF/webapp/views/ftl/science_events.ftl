<@page>
	<#list scienceEventsList as scienceEvent>
		<div class="row aboutus_area wow fadeInLeft">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<img class="img-responsive" src="${scienceEvent.imageURL!}" alt="image" />	                       
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h1>${scienceEvent.heading!}</h1>
				<p>"${scienceEvent.content!}"</p>
				<p><span class="events-feed-date">${scienceEvent.eventDate!}  </span></p>
			</div>
		</div>
		<hr />
	</#list>   
</@page>