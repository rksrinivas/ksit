<@page>
		 
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
    
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/nss/slider/nss1.JPG" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated bounceIn" src="${img_path!}/nss/slider/nss2.JPG" alt="">										
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss3.JPG" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss4.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss5.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss6.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss7.JPG" alt="">                        
                        </article>
						 <article class="item">
                           <img class="animated rollIn" src="${img_path!}/nss/slider/nss8.JPG" alt="">                        
                        </article>
                  
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
						<li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                        <li data-target="#myCarousel" data-slide-to="6"></li>
						<li data-target="#myCarousel" data-slide-to="7"></li>
                        
                    </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 
		 
<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To National Service Scheme (NSS)</h1>
   <p class="welcome-text">The National Service Scheme (NSS) is an Indian government-sponsored public service program conducted by 
   the Ministry of Youth Affairs and Sports of the Government of India. Popularly known as NSS, the scheme was launched in Gandhiji's
    Centenary year in 1969. Aimed at developing student's personality through community service, NSS is a voluntary association of
     young people in Colleges, Universities and at +2 level working for a campus-community (esp. Villages) linkage.</p>
</div>
<!-- welcome -->		 

	 <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="vertical-tab padding" role="tabpanel">
			               
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
			                    <li role="presentation"><a href="#objectives" aria-controls="objectives" role="tab" data-toggle="tab">Objectives</a></li>
			                    <li role="presentation"><a href="#committee" aria-controls="committee" role="tab" data-toggle="tab">Committee</a></li>
			                    <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>
			                    
			                    <li role="presentation"><a href="#achievements" aria-controls="vision" role="tab" data-toggle="tab">Achievements</a></li>			                  
			                    
			                     <li role="presentation"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab">Reports</a></li>			                    
								
			                    <li role="presentation"><a href="#gallery_dept" aria-controls="gallery_dept" role="tab" data-toggle="tab">Gallery</a></li>			                    
			                    			                    
			                    
			                    
			                    <!--<li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Section 3</a></li>
			                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>-->
			                </ul>
			                <!-- Nav tabs -->
			               
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                	
			                	<!-- profile -->
			                	 <div role="tabpanel" class="tab-pane fade in active" id="profile">
			                	  <div class="row">
			                        <h3>Profile</h3>
			                        <p>At KSIT NSS was officially launched in the year 2017 aimed to develop students personality through 
			                        social service. V T U sanctioned one unit (100 students )  to our college in the year 2018.</p>
			                            
			                            <h3>NSS Programme Officer's Desk</h3>
						                  <div class="aboutus_area wow fadeInLeft">
						                     <div class="col-lg-6 col-md-6 col-sm-6">
						                        <img class="hod-img img-responsive" src="${img_path!}/nss/nss_head.jpg" alt="image" />	
						                       <div class="qualification_info">
							                        <h3 class="qual-text">Naveen V</h3>
							                        <h4 class="qual-text">NSS Programme Officer</h4>
							                        <h3 class="qual-text">M.Sc</h3>
						                        </div>
						                    
						                     </div>
						                     <div class="col-lg-6 col-md-6 col-sm-6">	
						                     		<h3>MOTTO</h3>												
													<p>The Motto of NSS "Not Me But You", reflects the essence of democratic living and
													 upholds the need for self-less service. NSS helps the students development & 
													 appreciation to other person's point of view and also show consideration 
													 towards other living beings. The philosophy of the NSS is a good doctrine in
													  this motto, which underlines on the belief that the welfare of an individual
													   is ultimately dependent on the welfare of the society as a whole and therefore, 
													   the NSS volunteers shall strive for the well-being of the society.</p>																			 
						                     </div>
						                     
						                      <div class="col-lg-12 col-md-12 col-sm-12">													
													
								                     
								             
								                     
								                     																		 
								                     </div>
						                     
						                   </div>
			                            
			                            
			                      </div>
			                    </div>
			                    <!-- profile -->
			                    
							 <!-- Events -->
			                  <div role="tabpanel" class="tab-pane fade" id="events">
			                    <div class="row">
			                    <h3>Events</h3>
			                    
			                    <#list nssEventList[0..*3] as nssEvent>
				                  <div class="row aboutus_area wow fadeInLeft">
				                     <div class="col-lg-6 col-md-6 col-sm-6">
				                        <img class="img-responsive" src="${nssEvent.imageURL!}" alt="image" />	                       
				                     </div>
				                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
				                     	 <h3>${nssEvent.heading!}</h3>	                      	 
				                       	 <p>"${nssEvent.content!}"</p>
				                       	 <p><span class="events-feed-date">${nssEvent.eventDate!}  </span></p>
				                       	<a class="btn btn-primary" href="nss_events.html">View more</a>
				                     </div>
				                  </div>
			                  
			                           <hr />  
			                    </#list>   
			  						
			                    </div>    
			                  </div>
			                  <!-- Events  -->
			                  
			                  
			                  <!-- Achievements -->
				               <div role="tabpanel" class="tab-pane fade" id="achievements">
				               	<div class="row">
				               		 <h3>Achievements</h3>
				                     <!--Department Achievers-->
						                  <#list nssAchievementsList as nssAchievements>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${nssAchievements.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h3>${nssAchievements.heading!}</h3>	                      	 
							                       	 <p>${nssAchievements.eventDate!} </p>
							                       	 <p> ${nssAchievements.content!}</p>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>  	                    
						          
				                    
				               	</div>
				               </div>
				               <!-- Achievements -->
		
			                  
			                      <!-- Gallery  -->
				               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
				               	<div class="row">
				                  <h3>Gallery</h3>
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <div id="gallerySLide" class="gallery_area">
				                           <#list nssGalleryList as nssGallery>	
				                           <a href="${nssGallery.imageURL!}" target="_blank">
				                           <img class="gallery_img" src="${nssGallery.imageURL!}" alt="" />					                    			                   
				                           <span class="view_btn">${nssGallery.heading!}</span>
				                           </a>
				                           </#list>
				                        </div>
				                     </div>
				                  </div>
				                  </div>
				               </div>
				               <!-- Gallery -->
				               
				                      
					         <!-- Reports  -->
				               <div role="tabpanel" class="tab-pane fade" id="reports">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Reports</h3>
				                        <#list nssReportsList as nssReports>
				                        <p>${nssReports.heading!}  <a href="${nssReports.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				               <!-- Reports  -->   
			                    
				                    
	
				     
			                  
			                   <!-- objectives -->
			                    <div role="tabpanel" class="tab-pane fade" id="objectives">
			                    	<div class="row">
			                           <h3>OBJECTIVES OF NSS </h3>
													 <p>The main objectives of National Service Scheme (NSS) are :</p>
								                     <ul class="list">
								                        <li>Understand the community in which they work</li>
								                        <li>Understand themselves in relation to their community</li>
								                        <li>Identify the needs and problems of the community and involve them in problem-solving</li>
								                        <li>Develop among themselves a sense of social and civic responsibility</li>
								                        <li>Utilise their knowledge in finding practical solutions to individual and community problems</li>
								                        <li>Develop competence required for group-living and sharing of responsibilities</li>
								                        <li>Gain skills in mobilising community participation</li>
								                        <li>Acquire leadership qualities and democratic attitudes</li>
								                        <li>Develop capacity to meet emergencies and natural disasters</li>
								                        <li>Practise national integration and social harmony</li>
								                     </ul>	
			                        </div>  
			                    </div>
			                  <!-- objectives -->
			                  
			                  <!--committee -->
			                    <div role="tabpanel" class="tab-pane fade" id="committee">
			                    	<div class="container-fluid">
			                			               <h3>NSS COMMITTEE MEMBERS</h3>
			
											   <table class="table table-striped course_table" style="text-align:left !important;">

												<tr>
													<th>Name</th>
													<th>Branch</th>
													
												</tr>
												
												
												<tr>
													<td>Mr.Umesh.S</td>
													<td>PED</td>													
												</tr>
												
												<tr>
													<td>Mr.MANJUNATH B R</td>
													<td>MED</td>													
												</tr>
												
												<tr>
													<td>Mr.SALEEM.S.TEVARAMANI</td>
													<td>ECE</td>													
												</tr>
												
											<!--	<tr>
													<td>Mr.HARSHAVARDHAN J R</td>
													<td>CSE</td>													
												</tr>  -->
												
												<tr>
													<td>Mr.MANOJ KUMAR S</td>
													<td>ECE</td>													
												</tr>
												
												<tr>
													<td>MS.SHYLAJA.K.R</td>
													<td>BS&H</td>													
												</tr>
												
												<tr>
													<td colSpan="2">And Students  Representatives</td>													
												</tr>
												
											
												
												
												
											</table>
											
				
			                        </div     
			                    </div>
			                  <!--committee --> 
			                  
			           
			                    
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
  



	
</@page>