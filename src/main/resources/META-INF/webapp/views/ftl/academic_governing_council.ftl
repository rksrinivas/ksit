<@page>
    		
	 <!--=========== BEGIN WHY US SECTION ================-->
    <section id="whyUs_admin"  style="background-color:#686464;">
      <!-- Start why us top -->
      <div class="row">        
        <div class="col-lg-12 col-sm-12">
          <div class="whyus_top">
            <div class="container">

        <!-- Start Why us top content  -->
    	<div class="row">		    
		<!-- faculty -->			  
		<div class="container">
			<div class="row">

      <!-- Why us top titile -->
              <div class="row">
                <div class="col-lg-12 col-md-12"> 
                  <div class="title_area">
                    <h2 class="titile text-center">Academic Governing Council</h2>
                    <span></span> 
                  </div>
                </div>
              </div>
              <!-- End Why us top titile -->
    

			<!--faculty-->
			<#list academicGoverningCouncilFacultyList as faculty>
			<div class="col-md-3 col-lg-3 col-sm-6">
			<div class="container-fluid">
				<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${faculty.imageURL!}" alt="" />
							<div class="thumb_text">
								<h2>${faculty.name}</h2>
								<p>${faculty.designation}</p>
							</div>
						</div>
						<div class="teacher_content">
							<h2>${faculty.name}</h2>
							<span>${faculty.designation}</span>
							<p>${faculty.qualification}</p>
							<span>${faculty.department}</span>
							<p><a href="${faculty.profileURL!}" target="_blank">view profile</a></p>
							<!--<div class="social_icons">
								<a href="#" class="tw"><i class="fa fa-twitter"></i></a>
								<a href="#" class="fb"><i class="fa fa-facebook"></i></a>
								<a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
								<a href="#" class="go"><i class="fa fa-google-plus"></i></a>
							</div>-->
						</div>
					</div>
				</div>
				</div>
				</#list>
			</div>
		</div>
	</div>	
	<!--end teacher  area -->

	<!-- end of teacher details-->
    </div>
            <!-- End Why us top content  -->
        </div>
        </div>
    </div>        
    </div>
    <!-- End why us top -->		
    </div>
            </div>            
          </div>
        </div>        
      </div>
    <!-- End why us bottom -->
    </section>
<!--=========== END WHY US SECTION ================-->

</@page>