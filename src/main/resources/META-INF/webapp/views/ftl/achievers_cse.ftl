<@page>	
	<!-- =========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <!-- start blog archive  -->
              <div class="row">
              
              
              	 <!-- start single blog archive -->
              	 
              	 <#list cseDepartmentAchieversList as cseDepartmentAchievers>
                <div class="col-lg-6 col-12 col-sm-12">
                 
                 
                 
                 
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${cseDepartmentAchievers.imageURL!}">
                      </a>
                    </div>
                    <h2 class="blog_title">${cseDepartmentAchievers.heading!}</h2>
					
                    <div class="blog_commentbox">
                      <p>${cseDepartmentAchievers.eventDate!}</p>
                                            
                    </div>
					
                    <p class="blog_summary">${cseDepartmentAchievers.content!}</p>
					
					<!--<a class="blog_readmore" href="#">Read More</a>-->
                    
                  </div>
                </div>
                
                </#list>
                <!-- start single blog archive -->
              
			  
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/cse/rekha.JPG">
                      </a>
                    </div>
                    <h2 class="blog_title">Dr Rekha B Venkatapur, Professor and Head</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					Distinguished Women in Engineering 2017  � Venus International Foundation, India
					</p>
					
					<!--<a class="blog_readmore" href="#">Read More</a>-->
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/cse/sangeetha.JPG">
                      </a>
                    </div>
                    <h2 class="blog_title">Mrs. Sangeetha V, Associate Professor</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					Young Woman Educator & Researcher Award  on 8th March 2017 by National Foundation for Entrepreneurship Development (NFED), Coimbatore.
					</p>
                    <!--<a class="blog_readmore" href="#">Read More</a>-->
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/cse/priyanga.JPG">
                      </a>
                    </div>
                    <h2 class="blog_title">Mrs. Priyanga P, Assistant Professor</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					I2OR Promising Researcher Award 2017 � International Institute of Organized Research (I2OR) An organized Research Plat form.

					</p>
                    <!--<a class="blog_readmore" href="#">Read More</a>-->
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				 <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/cse/pradeep.JPG">
                      </a>
                    </div>
                    <h2 class="blog_title">Mr. Pradeep K R, Assistant Professor</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					I2OR Promising Researcher Award 2017 � International Institute of Organized Research (I2OR) An organized Research Plat form.

					</p>
                    <!--<a class="blog_readmore" href="#">Read More</a>-->
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
					
				 <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/cse/bithackers.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">SMART INDIA HACKATHON 2017</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					Our students secured 1st Prize of 1 Lakh  in 36 hours National Level Non stop Coding Competition termed to be Worlds Biggest Hackathon  �SMART INDIA HACKATHON 2017  conducted by Ministry of HRD Govt of India in Jaipur on 1st and 2nd April 2017. 

					</p>
                    <!--<a class="blog_readmore " href="#">Read More</a>-->
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				
				
              </div>
              <!-- end blog archive  -->
             
            </div>
          </div>
          <!-- End course content -->

         
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>