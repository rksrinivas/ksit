<div class="mymodal fade" id="addProgrammeDataModal" data-backdrop="false" data-keyboard="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content flat-style">
          <div class="modal-header" style ="background-color: #fd3753;">
            <button type="button" class="close" data-dismiss="modal" style="margin-top: 10px;"><i class="icon icon-remove-circle"></i>x</button>
            <div class="row">
              <div class="col-xs-9" style="margin-top: 4px;margin-left: 13px;">
                <h4 class="modal-title">Add Programme Data</h4>
              </div>
            </div>
          </div><!-- /.modal-header -->
          <div class="modal-body">
            <input type="hidden" id="programmeIdProgrammeDataData" value=""/>
            
            <div>
				<h3>Programme Type</h3>
				<select id="programmeDataType">
					<#list programmeDataTypes as programmeDataType>
						<option value="${programmeDataType}">${programmeDataType}</option>
					</#list>
				</select>
			</div>
                      
			<div>
				<h3>Semester</h3>
				<select id="semesterSyllabusData">
					<#list 1..8 as semester>
						<option value="${semester}">${semester}</option>
					</#list>
				</select>
			</div>
			
			<div>
				<h3>Start Year</h3>
				<select id="startYearProgrammeDataData">
					<#list 2015..2029 as startYear>
						<option value="${startYear}">${startYear}</option>
					</#list>
				</select>
			</div>
			
			<div>
				<h3>End Year</h3>
				<select id="endYearProgrammeDataData">
					<#list 2016..2030 as endYear>
						<option value="${endYear}">${endYear}</option>
					</#list>
				</select>
			</div>
  
			<div>
			    <h3>Syllabus</h3>
			    <input type="file" id="programmeDataData" />
			</div>
			
            <div>
              <button type="button" class="btn btn-primary" id="cancelAddProgrammeDataData">Cancel</button>
              <button type="submit" class="btn btn-success" id="saveAddProgrammeDataData">Save</button>
            </div>            
              
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>