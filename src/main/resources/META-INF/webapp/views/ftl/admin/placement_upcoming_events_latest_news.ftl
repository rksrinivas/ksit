<@pageAdmin>
	<div class="container" style="background-color:white;min-height:600px">

		<h3>Latest Events</h3>
		<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${placementLatestEventsFieldName!}>
			Add Latest Event
		</a>
		<@eventMacro eventList=placementLatestEventsList/>
		
		<h3>Upcoming Events</h3>
		<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${placementUpcomingEventsFieldName!}>
			Add Upcoming Event
		</a>
		<@eventMacro eventList=placementUpcomingEventsList/>
	</div>
</@pageAdmin>
