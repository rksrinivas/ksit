<@pageAdmin title="Feedback" showFooter=false>
<div id="file_content">
	<table>
		<tbody>
			<tr>
				<th>
					Student Name:
				</th>
				<td>
				 	${feedbackAlumni.studentName}
				 </td>
			</tr>
			<tr>
				<th>
					Year of Graduation
				</th>
				<td>
					${feedbackAlumni.yearOfGraduation}
				</td>
			</tr>
			<tr>
				<th>
					Position in Graduation
				</th>
				<td>
					${feedbackAlumni.positionInGraduation}
				</td>
			</tr>
			<tr>
				<th>			
					USN
				</th>
				<td>
					${feedbackAlumni.usn}
				</td>
			</tr>
			<tr>
				<th>			
					Email Id
				</th>
				<td>
					${feedbackAlumni.emailId}
				</td>
			</tr>
			<tr>
				<th>			
					Address
				</th>
				<td>
					${feedbackAlumni.address}
				</td>
			</tr>
			<tr>
				<th>			
					Mobile Number
				</th>
				<td>
					${feedbackAlumni.mobileNumber}
				</td>
			</tr>
			<tr>
				<th>			
					Current Organisation
				</th>
				<td>
					${feedbackAlumni.currentOrganisation}
				</td>
			</tr>
			<tr>
				<th>			
					Designation
				</th>
				<td>
					${feedbackAlumni.designation}
				</td>
			</tr>	
			<tr>
				<th>
					Suggestion About Vision:
				</th>
				<td>
					${feedbackAlumni.visionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Mission:
				</th>
				<td>
					${feedbackAlumni.missionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Program Educational Objectives (PEO):
				</th>
				<td>
					${feedbackAlumni.peoSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Whether the Department is moving towards right path to achieve its vision &amp; mission?
				</th>
				<td>
					${feedbackAlumni.visionMission}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree our vision statement captures where we should be heading as a department?
				</th>
				<td>
					${feedbackAlumni.visionAgree}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree that our mission statements will enable us to achieve our vision?
				</th>
				<td>
					${feedbackAlumni.missionAgree}
				</td>
			</tr>
			<#list knowledgeFeedbacks as knowledgeFeedback>
				<tr>
					<th>
						${knowledgeFeedback?index+1}. ${knowledgeFeedback}
					</th>
					<td>
						<#assign key = "knowledgeResponse${knowledgeFeedback?index+1}">
						${feedbackAlumni[key].desc}
					</td>
				</tr>
			</#list>
			<#list communicationFeedbacks as communicationFeedback>
				<tr>
					<th>
						${communicationFeedback?index+1}. ${communicationFeedback}
					</th>
					<td>
						<#assign key = "communicationResponse${communicationFeedback?index+1}">
						${feedbackAlumni[key].desc}
					</td>
				</tr>
			</#list>
			<#list interPersonalFeedbacks as interPersonalFeedback>
				<tr>
					<th>
						${interPersonalFeedback?index+1}. ${interPersonalFeedback}
					</th>
					<td>
						<#assign key = "interPersonalResponse${interPersonalFeedback?index+1}">
						${feedbackAlumni[key].desc}
					</td>
				</tr>
			</#list>
			<#list managementFeedbacks as managementFeedback>
				<tr>
					<th>
						${managementFeedback?index+1}. ${managementFeedback}
					</th>
					<td>
						<#assign key = "managementResponse${managementFeedback?index+1}">
						${feedbackAlumni[key].desc}
					</td>
				</tr>
			</#list>
			<tr>
				<th>
					Any other suggestions/comments:
				</th>
				<td>
					${feedbackAlumni.response}
				</td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	$(document).ready(function(){
	    var doc = new jsPDF('p','pt', 'a4');
	    var specialElementHandlers = {
	        '#editor': function (element, renderer) {
	            return true;
	        }
	    };
	    var download = getParameterByName('download');
	    if(download){
	    	let options = {
	            'width': 500,
	            'elementHandlers': specialElementHandlers
	        };
	        let x = 15;
	        let y = 15;
	        pageHeight= doc.internal.pageSize.height;
	    	$('table > tbody  > tr').each(function(index, tr) {
	    		if (y >= pageHeight){
	    			doc.addPage();
	    			y = 0;
	    		} 
	    		doc.fromHTML($(tr).html(), x, y, options);
	    		y += $(tr).height() * 2;
			});
	        doc.save('feedback_alumni_${feedbackAlumni.id}.pdf');
	    }
    });
</script>
</@pageAdmin>
