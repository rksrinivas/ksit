<div class="mymodal fade" id="addProgrammeModal" data-backdrop="false" data-keyboard="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content flat-style">
          <div class="modal-header" style ="background-color: #fd3753;">
            <button type="button" class="close" data-dismiss="modal" style="margin-top: 10px;"><i class="icon icon-remove-circle"></i>x</button>
            <div class="row">
              <div class="col-xs-9" style="margin-top: 4px;margin-left: 13px;">
                <h4 class="modal-title">Add Programme</h4>
              </div>
            </div>
          </div><!-- /.modal-header -->
          <div class="modal-body">
            <input type="hidden" id="programmeTypeProgrammeData" />          
			<div>
				<h3>Active</h3>
				<select id="departmentProgrammeData">
					<#if programmeDataType?has_content>
						<#if programmeDataType == "UG">
						  <option value="Artificial intelligence and machine learning">Artificial intelligence and machine learning</option>
						  <option value="Computer Science & Design">Computer Science & Design</option>
						  <option value="Computer Science & Engineering">Computer Science & Engineering</option>
						  <option value="Electronics & Communication Engineering">Electronics & Communication Engineering</option>
						  <option value="Mechanical Engineering">Mechanical Engineering</option>
						  <option value="Electronics & Telecommunication Engineering">Electronics & Telecommunication Engineering</option>
						  <option value="Science and Humanities">Science and Humanities</option>
						 <#elseif programmeDataType == "PG">
						 	<option value="M.tech in computer science and engineering">M.tech in computer science and engineering</option>
						 	<option value="M.tech in Machine Design">M. Tech in Machine Design</option>
						 	<option value="M.tech in Digital Electronics and communication">M.tech in Digital Electronics and communication</option>
						 </#if>
					</#if>
				</select>
			</div>
			
			<br />
			<br />
			
            <div>
              <button type="button" class="btn btn-primary" id="cancelAddProgrammeData">Cancel</button>
              <button type="submit" class="btn btn-success" id="saveAddProgrammeData">Save</button>
            </div>            
              
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>