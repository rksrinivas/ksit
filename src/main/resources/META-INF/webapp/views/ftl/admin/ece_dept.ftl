<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	
	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${ecePageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=ecePageSliderImageList/>
	
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${eceLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=ecePageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${eceUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=ecePageUpcomingEventsList/>
	
	

	
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eceEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=eceEventList/>
	
	<h3>Testimonials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${testimonialFieldName!}>
		Add Testimonial
	</a>
	<@testimonialMacro testimonialList=testimonialList/>
	
	<!--<h3>Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${achieverFieldName!}>
		Add Achievers
	</a>
	<@achieverMacro achieverList=achieverList/> -->
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	<h3>Supporting Staff Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Gallery
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	<h3>Department Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=eceDepartmentAchieversList/>
	
	<h3>FCD Students List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceFcdStudentsFieldName!}>
		Add Students List
	</a>
	<@galleryMacro galleryList=eceFcdStudentsList/>
	
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${eceClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@achieverMacro achieverList=eceClassTimeTableList/>
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${eceTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@achieverMacro achieverList=eceTestTimeTableList/>
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${eceCalanderFieldName!}>
		Add Calendar
	</a>
	<@achieverMacro achieverList=eceCalanderList/>
	
	
	
	<h3>UG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceUgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceUgSyllabusList/>
	
	<h3>PG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${ecePgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=ecePgSyllabusList/>
	
	
	
	<h3>News Letter</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${eceNewsLetterFieldName!}>
		Add newsletter
	</a>
	<@achieverMacro achieverList=eceNewsLetterList/>
	
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${eceLibraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=eceLibraryBooksList/>
	
	<h3>Placement List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${ecePlacementFieldName!}>
		Add Placement List
	</a>
	<@galleryMacro galleryList=ecePlacementList/>
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${eceLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=eceLabList/>
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${eceEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=eceEresourcesList/>
	
	
	<h3>Research and Development</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceResearchFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=eceResearchList/>
	
	<h3>Sponsored Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceSponsoredProjectsFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=eceSponsoredProjectsList/>
	
	
	<h3>Phd Pursuing</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${ecePhdPursuingFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=ecePhdPursuingList/>
	
	<h3>Phd Awardees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${ecePhdAwardeesFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=ecePhdAwardeesList/>
	
	
	
	
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${eceInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=eceInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${ecePedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=ecePedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${ecePedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=ecePedagogyReviewFormList/>

	<h3>Content Beyond Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${ececontentbeyondsyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=ececontentbeyondsyllabusList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${eceLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=eceLabManualList/>
	
	
	<h3>MOU's Signed</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceMouSignedFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceMouSignedList/>
	
	<h3>Alumni Association</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceAlumniAssociationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceAlumniAssociationList/>
	
	
	
	<h3>Professional Bodies</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceProfessionalBodiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceProfessionalBodiesList/>
	
	<h3>Professional Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${eceProfessionalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=eceProfessionalLinksList/>
	
	<h3>Higher Education</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceHigherEducationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceHigherEducationList/>
	
	<h3>Student Club Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceClubActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceClubActivitiesList/>
	
	<h3>Student Club External Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${eceClubLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=eceClubExternalLinksList/>
	
	<h3>Internships</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceInternshipFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceInternshipList/>
	
	<h3>Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceProjectsList/>
	
	<h3>Mini Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceMiniProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceMiniProjectsList/>
	
	<h3>Social Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceSocialActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceSocialActivitiesList/>
	
	<h3>Industrial Visit</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${eceIndustrialVisitFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=eceIndustrialVisitList/>
	
	<h3>Project Exhibition</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${eceProjectExhibitionFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=eceProjectExhibitionList/>
	
	<h3>Other Details</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceOtherDetailsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceOtherDetailsList/>
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${eceFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=eceFdpList/>
	
	
	
	
	<!--<h3>Project Exhibition</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${eceSponsoredProjectsFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=eceSponsoredProjectsList/> -->

	<h3>Feedback By Alumni</h3>
	<@feedbackAlumniMacro feedbackAlumniList=feedbackAlumniList/>
	
	<h3>Feedback By Student</h3>
	<@feedbackStudentMacro feedbackStudentList=feedbackStudentList/>
	
	<h3>Feedback By Parent</h3>
	<@feedbackParentMacro feedbackParentList=feedbackParentList/>
	
	<h3>Feedback By Employer</h3>
	<@feedbackEmployerMacro feedbackEmployerList=feedbackEmployerList/>
</div>	
</@pageAdmin>