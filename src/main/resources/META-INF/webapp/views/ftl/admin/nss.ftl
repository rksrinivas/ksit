<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>

	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${nssEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=nssEventList/>
	
	<h3>Achievements</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${nssAchievementsFieldName!}>
		Add 
	</a>
	<@eventMacro eventList=nssAchievementsList/>
	
	<h3>Reports</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${nssReportsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=nssReportsList/>
	
</div>
</@pageAdmin>