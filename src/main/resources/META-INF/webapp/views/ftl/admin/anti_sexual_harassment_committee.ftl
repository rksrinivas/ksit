<@pageAdmin>
<div class="container-fluid" style="background-color:white;min-height:600px">
	<h3>Reports</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${reportsFieldName!}>
		Add Report
	</a>
	<@eventMacro eventList=reportsList/>

    </div>
</@pageAdmin>