<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Feedback</h3>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th style="width:20%">Name</th>
			<th style="width:20%">Email</th>
			<th style="width:20%">Phone</th>
			<th style="width:20%">Subject</th>
			<th style="width:20%">Description</th>
		</thead>
		<tbody>
			<#if feedbackList?has_content>
				<#list feedbackList as feedback>
					<tr>
						<td>
							${feedback.name!}
						</td>
						<td>
							${feedback.email!}
						</td>
						<td>
							${feedback.phone!}
						</td>
						<td>
							${feedback.subject!}
						</td>
						<td>
							${feedback.description!}  <button onclick="deleteFeedback('${feedback.id!}');">Delete</button>
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
	<ul class="pager divInRight ">
		<#if hasPrev?has_content && hasPrev>
			<li><a href="feedback?page=${prevPage!}">Prev</a></li>
		</#if>
		<#if hasNext?has_content && hasNext>
			<li><a href="feedback?page=${nextPage!}">Next</a></li>
		</#if>
	</ul>
</div> 
</@pageAdmin>