<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<p>
		<#if loggedInUser?has_content>
			<h3>
				Hi ${loggedInUser.username!},
			</h3>
		</#if> 
	</p>
	<p>
		<h4 class="error">
			You do not have access to that page. Please Contact Administrator!!!
		</h4>
	</p>
</div>
</@pageAdmin>