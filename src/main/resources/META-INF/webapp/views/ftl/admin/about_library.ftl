<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">

	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${libraryPageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=libraryPageSliderImageList/>
	
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Gallery
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${libraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=libraryBooksList/>

	<h3>New Arrivals</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${newArrivalListFieldName!}>
		Add new books
	</a>
	<@testimonialMacro testimonialList=newArrivalList/>
	
	<h3>Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${libraryActivityListFieldName!}>
		Add new activities
	</a>
	<@testimonialMacro testimonialList=libraryActivityList/>
	
	<h3>Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${libraryLinksFieldName!}>
		Add links
	</a>
	<@achieverMacro achieverList=libraryLinksList/>
	
	<h3>E-resources </h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${libraryEresourcesListFieldName!}>
		Add new e-resources
	</a>
	<@testimonialMacro testimonialList=libraryEresourcesList/>
	
	<h3>Other Details</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${libraryOtherDetailsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=libraryOtherDetailsList/>
	
	
	
	
</div>





</@pageAdmin>