<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<div class="row">
		<div class="col-lg-4">
			<form action="/admin/institutionalGovernance" method="post" enctype="multipart/form-data">
				<div class="wpcf7-text"> 
					Heading <input type="text" name="heading">
				</div> 
				File <input type="file" name="file"/>
				<br />
				<br />
				<button type="submit" class="btn btn-primary" value="Submit">Save</button>
			</form>
		</div>
	</div>
	<br />
	<h3>Saved Details</h3>
	<div class="row">
		<div class="col-lg-6">
			<table class="table table-striped table-bordered" style="width="100%">
				<tr>
					<th>
						Heading
					</th>
					<th>
						Link
					</th>
					<th>
						Delete
					</th>
				</tr>
				<#list institutionalGovernanceList as institutionalGovernance>
					<tr>
						<td>${institutionalGovernance.heading}</td>
						<td><a href="${institutionalGovernance.link}" target="_blank">View</a></td>
						<td>
							<form action="/admin/institutionalGovernance/delete/${institutionalGovernance.id}" method="post">
								<button type="submit" value="Submit">Delete</button>						
							</form>
						</td>
					</tr>
				</#list>
			</table>
		</div>
	</div>
</div>
</@pageAdmin>