<@pageAdmin>
	<div class="container" style="background-color:white;min-height:600px">

		<h3>Events</h3>
		<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${placementStatisticsFieldName!}>
			Add Placement Statistics
		</a>
		<@eventMacro eventList=placementStatisticsList/>
	</div>
</@pageAdmin>
