<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<div class="row">
		<div class="col-lg-4">
			<form action="/admin/nba" method="post">
				Page
				<select class="wp-form-control wpcf7-text" name="page" required>
					<option value="cse_page">Computer Science and Engineering</option>
					<option value="ece_page">Electronics and Communication Engineering</option>
					<option value="mech_page">Mechanical Engineering</option>						
				</select>
				Criteria
				<select class="wp-form-control wpcf7-text" name="criteria" required>
					<#list criteriaList as criteria>
						<option value="${criteria}">${criteria.desc}</option>
					</#list>
				</select>
				<div class="wpcf7-text"> 
					Heading <input type="text" name="heading">
				</div> 
				<div class="wpcf7-text"> 
					Link  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="link">
				</div>
				<button type="submit" value="Submit">Save</button>
			</form>
		</div>
	</div>
	<br />
	<h3>Saved NBA Details</h3>
	<div class="row">
		<div class="col-lg-6">
			<table class="table table-striped table-bordered" style="width="100%">
				<tr>
					<th>
						Page
					</th>
					<th>
						Criteria
					</th>
					<th>
						Heading
					</th>
					<th>
						Link
					</th>
					<th>
						Delete
					</th>
				</tr>
				<#list nbaList as nba>
					<tr>
						<td>${nba.page}</td>
						<td>${nba.criteria.desc}</td>
						<td>${nba.heading}</td>
						<td>${nba.link}</td>
						<td>
							<form action="/admin/nba/delete/${nba.id}" method="post">
								<button type="submit" value="Submit">Delete</button>						
							</form>
							
						</td>
					</tr>
				</#list>
			</table>
		</div>
	</div>
</div>
</@pageAdmin>