<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<form action="/alumni_list" method="get">
		<table class="table table-striped table-bordered" style="width="100%">
			<thead style="width:100%">
				<th>Name</th>
				<th>Email</th>
				<th>USN</th>
				<th>Year of Passing</th>
				<th>Mobile Number</th>
				<th>Course</th>
				<th>Branch</th>
				<th>Company</th>
				<th>Career Details</th>
			</thead>
			<tbody>
				<#if alumniList?has_content>
					<#list alumniList as alumni>
						<tr>
							<td>
								${alumni.name!}
							</td>
							<td>
								${alumni.emailId!}
							</td>
							
							<td>
								${alumni.usn!}
							</td>
							
							<td>
								${alumni.yearOfPassing!}
							</td>
							<td>
								${alumni.mobileNumber!}
							</td>
							<td>
								${alumni.course!}
							</td>
							<td>
								${alumni.branch!}
							</td>
							<td>
								${alumni.companyName!}
							</td>
							<td>
								${alumni.careerDetails!}
							</td>
							
						</tr>
					</#list>
				</#if>
			</tbody>
			<tfoot style="width:100%">
				<th>Name</th>
				<th>Email</th>
				<th>USN</th>
				<th>Year of Passing</th>
				<th>Mobile Number</th>
				<th>Course</th>
				<th>Branch</th>
				<th>Company</th>
				<th>Career Details</th>
			</tfoot>
		</table>
		<ul class="pager divInRight ">
			<#if hasPrev?has_content && hasPrev>
				<li><a href="alumni_list?page=${prevPage!}">Prev</a></li>
			</#if>
			<#if hasNext?has_content && hasNext>
				<li><a href="alumni_list?page=${nextPage!}">Next</a></li>
			</#if>
		</ul>
	</form>
</div> 
</@pageAdmin>