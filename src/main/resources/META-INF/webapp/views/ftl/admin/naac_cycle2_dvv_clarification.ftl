<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Naac Cycle 2 DVV Clarification</h3>
	<div class="row">
		<div class="col-lg-4">
			<form action="/admin/naac_cycle2_dvv_clarification" method="post" enctype="multipart/form-data">
				Serial No
				<input type="number" name="year" min="1">
				<br />
				Criteria
				<select class="wp-form-control wpcf7-text" name="criteria" required>
					<#list criteriaList as criteria>
						<option value="${criteria}">${criteria.desc}</option>
					</#list>
				</select>
				<br />
				<div class="wpcf7-text"> 
					Heading <input type="text" name="heading">
				</div> 
				File <input type="file" name="file"/>
				<br />
				<br />
				<button type="submit" class="btn btn-primary" value="Submit">Save</button>
			</form>
		</div>
	</div>
	<br />
	<h3>Saved NAAC Details</h3>
	<div class="row">
		<div class="col-lg-6">
			<table class="table table-striped table-bordered" style="width="100%">
				<tr>
					<th>
						Serial No
					</th>
					<th>
						Criteria
					</th>
					<th>
						Heading
					</th>
					<th>
						Link
					</th>
					<th>
						Delete
					</th>
				</tr>
				<#list naacList as naac>
					<tr>
						<td>${naac.year}</td>
						<td>${naac.criteria.desc}</td>
						<td>${naac.heading}</td>
						<td><a href="${naac.link}" target="_blank">View</a></td>
						<td>
							<form action="/admin/naac/delete/${naac.id}" method="post">
								<button type="submit" value="Submit">Delete</button>						
							</form>
						</td>
					</tr>
				</#list>
			</table>
		</div>
	</div>
</div>
</@pageAdmin>