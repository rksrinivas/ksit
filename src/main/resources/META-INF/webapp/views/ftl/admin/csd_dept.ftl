<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${csdPageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=csdPageSliderImageList/>
		
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${csdLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=csdPageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${csdUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=csdPageUpcomingEventsList/>
	
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${csdEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=csdEventList/>
	
<!--	<h3>Testimonials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${testimonialFieldName!}>
		Add Testimonial
	</a>
	<@testimonialMacro testimonialList=testimonialList/>  -->
	
	<!--<h3>Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${achieverFieldName!}>
		Add Achievers
	</a>
	<@achieverMacro achieverList=achieverList/> -->
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	<h3>Supporting Staff</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>
		
	
	<h3>Department Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=csdDepartmentAchieversList/>
	
	<h3>FCD Students List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdFcdStudentsFieldName!}>
		Add Students List
	</a>
	<@galleryMacro galleryList=csdFcdStudentsList/>
	
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${csdClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@achieverMacro achieverList=csdClassTimeTableList/>
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${csdTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@achieverMacro achieverList=csdTestTimeTableList/>
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${csdCalanderFieldName!}>
		Add Calendar
	</a>
	<@achieverMacro achieverList=csdCalanderList/>
	
	<h3>UG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdUgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdUgSyllabusList/>
	
	
	
<!--	<h3>News Letter</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${csdNewsLetterFieldName!}>
		Add newsletter
	</a>
	<@achieverMacro achieverList=csdNewsLetterList/>  -->
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdLibraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=csdLibraryBooksList/>
	
<!--	<h3>Placement List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdPlacementFieldName!}>
		Add Placement List
	</a>
	<@galleryMacro galleryList=csdPlacementList/>  -->
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${csdLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=csdLabList/>
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csdEresourcesList/>
	
	
	
	
<!--	<h3>Research and Development</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdResearchFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=csdResearchList/>
	
	<h3>Sponsored Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdSponsoredProjectsFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=csdSponsoredProjectsList/>
	
	
	<h3>Phd Pursuing</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdPhdPursuingFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=csdPhdPursuingList/>
	
	<h3>Phd Awardees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdPhdAwardeesFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=csdPhdAwardeesList/>  -->
	
	
	
	
		
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csdInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdPedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csdPedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdPedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csdPedagogyReviewFormList/>

	<h3>Content Beyond Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdcontentbeyondsyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdcontentbeyondsyllabusList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csdLabManualList/>
	
	
	<h3>MOU's Signed</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdMouSignedFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdMouSignedList/>
	
<!--	<h3>Alumni Association</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdAlumniAssociationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdAlumniAssociationList/>  -->
	
	
	
	
	<h3>Professional Bodies</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdProfessionalBodiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdProfessionalBodiesList/>
	
	<h3>Professional Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdProfessionalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csdProfessionalLinksList/>
	
<!--	<h3>Higher Education</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdHigherEducationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdHigherEducationList/> -->
	
	<h3>Student Club Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdClubActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdClubActivitiesList/>
	
	<h3>Student Club External Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csdClubLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csdClubExternalLinksList/>
	
	<h3>Internship</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdInternshipFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdInternshipList/>
	
	<h3>Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdProjectsList/>
	
	<h3>Mini Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdMiniProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdMiniProjectsList/>
	
<!--	<h3>Social Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdSocialActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdSocialActivitiesList/>
	
	<h3>Industrial Visit</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${csdIndustrialVisitFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=csdIndustrialVisitList/>

	<h3>Project Exhibition</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${csdProjectExhibitionFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=csdProjectExhibitionList/>    -->
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdFdpList/>
	
	<h3>Other Details</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csdOtherDetailsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csdOtherDetailsList/>
	

<!--	<h3>Feedback By Alumni</h3>
	<@feedbackAlumniMacro feedbackAlumniList=feedbackAlumniList/>
	
	<h3>Feedback By Student</h3>
	<@feedbackStudentMacro feedbackStudentList=feedbackStudentList/>
	
	<h3>Feedback By Parent</h3>
	<@feedbackParentMacro feedbackParentList=feedbackParentList/>
	
	<h3>Feedback By Employer</h3>
	<@feedbackEmployerMacro feedbackEmployerList=feedbackEmployerList/>  -->
		
</div>
</@pageAdmin>