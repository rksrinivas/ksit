<@pageAdmin title="Feedback" showFooter=false>
<div id="file_content">
	<table>
		<tbody>
			<tr>
				<th>
					Name
				</th>
				<td>
				 	${feedbackEmployer.name}
				 </td>
			</tr>
			<tr>
				<th>
					Company Name
				</th>
				<td>
					${feedbackEmployer.companyName}
				</td>
			</tr>
			<tr>
				<th>
					Designation
				</th>
				<td>
					${feedbackEmployer.designation}
				</td>
			</tr>
			<tr>
				<th>
					Email Id
				</th>
				<td>
					${feedbackEmployer.emailId}
				</td>
			</tr>
			<tr>
				<th>
					Phone
				</th>
				<td>
					${feedbackEmployer.mobileNumber}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Vision:
				</th>
				<td>
					${feedbackEmployer.visionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Mission:
				</th>
				<td>
					${feedbackEmployer.missionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Program Educational Objectives (PEO):
				</th>
				<td>
					${feedbackEmployer.peoSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Whether the Department is moving towards right path to achieve its vision &amp; mission?
				</th>
				<td>
					${feedbackEmployer.visionMission}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree our vision statement captures where we should be heading as a department?
				</th>
				<td>
					${feedbackEmployer.visionAgree}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree that our mission statements will enable us to achieve our vision?
				</th>
				<td>
					${feedbackEmployer.missionAgree}
				</td>
			</tr>
			<#list employerFeedbacks as employerFeedback>
				<tr>
					<th>
						${employerFeedback?index+1}. ${employerFeedback}
					</th>
					<td>
						<#assign key = "response${employerFeedback?index+1}">
						${feedbackEmployer[key].desc}
					</td>
				</tr>
			</#list>
			<tr>
				<th>
					Any other suggestions/comments:
				</th>
				<td>
					${feedbackEmployer.response}
				</td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	$(document).ready(function(){
	    var doc = new jsPDF('p','pt', 'a4');
	    var specialElementHandlers = {
	        '#editor': function (element, renderer) {
	            return true;
	        }
	    };
	    var download = getParameterByName('download');
	    if(download){
	    	let options = {
	            'width': 500,
	            'elementHandlers': specialElementHandlers
	        };
	        let x = 15;
	        let y = 15;
	        pageHeight= doc.internal.pageSize.height;
	    	$('table > tbody  > tr').each(function(index, tr) {
	    		if (y >= pageHeight){
	    			doc.addPage();
	    			y = 0;
	    		} 
	    		doc.fromHTML($(tr).html(), x, y, options);
	    		y += $(tr).height() * 2;
			});
	        doc.save('feedback_employer_${feedbackEmployer.id}.pdf');
	    }
    });
</script>
</@pageAdmin>
