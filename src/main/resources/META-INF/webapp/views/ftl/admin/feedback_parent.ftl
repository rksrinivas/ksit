<@pageAdmin title="Feedback" showFooter=false>
<div id="file_content">
	<table>
		<tbody>
			<tr>
				<th>
					Parent Name:
				</th>
				<td>
				 	${feedbackParent.nameOfTheParent}
				 </td>
			</tr>
			<tr>
				<th>
					Email:
				</th>
				<td>
					${feedbackParent.emailId}
				</td>
			</tr>
			<tr>
				<th>
					Phone No:
				</th>
				<td>
					${feedbackParent.phoneNo}
				</td>
			</tr>
			<tr>
				<th>
					Occupation
				</th>
				<td>
					${feedbackParent.occupation}
				</td>
			</tr>
			<tr>
				<th>
					Student Name:
				</th>
				<td>
				 	${feedbackParent.studentName}
				 </td>
			</tr>
			<tr>
				<th>
					Semester & Section:
				</th>
				<td>
					${feedbackParent.semesterAndSection}
				</td>
			</tr>
			<tr>
				<th>
					Academic Year:
				</th>
				<td>
					${feedbackParent.academicYear}
				</td>
			</tr>
			<tr>
				<th>			
					USN:
				</th>
				<td>
					${feedbackParent.usn}
				</td>
			</tr>
			<tr>
				<th>			
					Relationship:
				</th>
				<td>
					${feedbackParent.relationship}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Vision:
				</th>
				<td>
					${feedbackParent.visionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Mission:
				</th>
				<td>
					${feedbackParent.missionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Program Educational Objectives (PEO):
				</th>
				<td>
					${feedbackParent.peoSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Whether the Department is moving towards right path to achieve its vision &amp; mission?
				</th>
				<td>
					${feedbackParent.visionMission}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree our vision statement captures where we should be heading as a department?
				</th>
				<td>
					${feedbackParent.visionAgree}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree that our mission statements will enable us to achieve our vision?
				</th>
				<td>
					${feedbackParent.missionAgree}
				</td>
			</tr>
			<#list parentFeedbacks as parentFeedback>
				<tr>
					<th>
						${parentFeedback?index+1}. ${parentFeedback}
					</th>
					<td>
						<#assign key = "response${parentFeedback?index+1}">
						${feedbackParent[key].desc}
					</td>
				</tr>
			</#list>
			<tr>
				<th>
					Any other suggestions/comments:
				</th>
				<td>
					${feedbackParent.response}
				</td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	$(document).ready(function(){
	    var doc = new jsPDF('p','pt', 'a4');
	    var specialElementHandlers = {
	        '#editor': function (element, renderer) {
	            return true;
	        }
	    };
	    var download = getParameterByName('download');
	    if(download){
	    	let options = {
	            'width': 500,
	            'elementHandlers': specialElementHandlers
	        };
	        let x = 15;
	        let y = 15;
	        pageHeight= doc.internal.pageSize.height;
	    	$('table > tbody  > tr').each(function(index, tr) {
	    		if (y >= pageHeight){
	    			doc.addPage();
	    			y = 0;
	    		} 
	    		doc.fromHTML($(tr).html(), x, y, options);
	    		y += $(tr).height() * 2;
			});
	        doc.save('feedback_parent_${feedbackParent.id}.pdf');
	    }
    });
</script>
</@pageAdmin>
