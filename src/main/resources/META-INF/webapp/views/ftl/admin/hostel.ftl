<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Hostel Enrollment</h3>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th style="width:20%">College Name</th>
			<th style="width:20%">First Name</th>
			<th style="width:20%">Last Name</th>
			<th style="width:20%">Year</th>
			<th style="width:20%">Sem</th>
			<th style="width:20%">Phone</th>
			<th style="width:20%">Email</th>
			<th style="width:20%">Father Name</th>
			<th style="width:20%">Father Phone</th>
			<th style="width:20%">Mother Name</th>
			<th style="width:20%">Mother Phone</th>
			<th style="width:20%">Address</th>
			<th style="width:20%">Delete</th>									
		</thead>
		<tbody>
			<#if hostelEnrollmentList?has_content>
				<#list hostelEnrollmentList as hostelEnrollment>
					<tr>
						<td>
							${hostelEnrollment.collegeName!}
						</td>
						<td>
							${hostelEnrollment.firstName!}
						</td>
						<td>
							${hostelEnrollment.lastName!}
						</td>
						<td>
							${hostelEnrollment.year!}
						</td>
						<td>
							${hostelEnrollment.sem!}
						</td>
						<td>
							${hostelEnrollment.phone!}
						</td>
						<td>
							${hostelEnrollment.email!}
						</td>
						<td>
							${hostelEnrollment.fatherName!}
						</td>
						<td>
							${hostelEnrollment.fatherPhone!}
						</td>
						<td>
							${hostelEnrollment.motherName!}
						</td>
						<td>
							${hostelEnrollment.motherPhone!}
						</td>
						<td>
							${hostelEnrollment.address!}
						</td>
						<td> 
							<button onclick="deleteHostelEnrollment('${hostelEnrollment.id!}');">Delete</button>
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
	<ul class="pager divInRight ">
		<#if hasPrev?has_content && hasPrev>
			<li><a href="hostel?page=${prevPage!}">Prev</a></li>
		</#if>
		<#if hasNext?has_content && hasNext>
			<li><a href="hostel?page=${nextPage!}">Next</a></li>
		</#if>
	</ul>
</div> 
</@pageAdmin>