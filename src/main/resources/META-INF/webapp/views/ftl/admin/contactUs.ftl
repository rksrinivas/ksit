<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Contact Us</h3>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th style="width:20%">Name</th>
			<th style="width:20%">Email</th>
			<th style="width:20%">Phone</th>
			<th style="width:20%">Subject</th>
			<th style="width:20%">Description</th>
		</thead>
		<tbody>
			<#if contactUsList?has_content>
				<#list contactUsList as contactUs>
					<tr>
						<td>
							${contactUs.name!}
						</td>
						<td>
							${contactUs.email!}
						</td>
						<td>
							${contactUs.phone!}
						</td>
						<td>
							${contactUs.subject!}
						</td>
						<td>
							${contactUs.description!}  <button onclick="deleteContactUs('${contactUs.id!}');">Delete</button>
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
	<ul class="pager divInRight ">
		<#if hasPrev?has_content && hasPrev>
			<li><a href="contactUs?page=${prevPage!}">Prev</a></li>
		</#if>
		<#if hasNext?has_content && hasNext>
			<li><a href="contactUs?page=${nextPage!}">Next</a></li>
		</#if>
	</ul>
</div> 
</@pageAdmin>