<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${telePageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=telePageSliderImageList/>
		
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${teleLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=telePageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${teleUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=telePageUpcomingEventsList/>
	
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${teleEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=teleEventList/>
	
	<h3>Testimonials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${testimonialFieldName!}>
		Add Testimonial
	</a>
	<@testimonialMacro testimonialList=testimonialList/>
	
	<!--<h3>Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${achieverFieldName!}>
		Add Achievers
	</a>
	<@achieverMacro achieverList=achieverList/> -->
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	<h3>Supporting Staff</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>
		
	
	<h3>Department Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=teleDepartmentAchieversList/>
	
	<h3>FCD Students List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleFcdStudentsFieldName!}>
		Add Students List
	</a>
	<@galleryMacro galleryList=teleFcdStudentsList/>
	
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${teleClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@achieverMacro achieverList=teleClassTimeTableList/>
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${teleTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@achieverMacro achieverList=teleTestTimeTableList/>
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${teleCalanderFieldName!}>
		Add Calendar
	</a>
	<@achieverMacro achieverList=teleCalanderList/>
	
	<h3>UG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleUgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleUgSyllabusList/>
	
	
	
	<h3>News Letter</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${teleNewsLetterFieldName!}>
		Add newsletter
	</a>
	<@achieverMacro achieverList=teleNewsLetterList/>
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${teleLibraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=teleLibraryBooksList/>
	
	<h3>Placement List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${telePlacementFieldName!}>
		Add Placement List
	</a>
	<@galleryMacro galleryList=telePlacementList/>
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${teleLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=teleLabList/>
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${teleEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=teleEresourcesList/>
	
	
	
	
	<h3>Research and Development</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleResearchFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=teleResearchList/>
	
	<h3>Sponsored Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleSponsoredProjectsFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=teleSponsoredProjectsList/>
	
	
	<h3>Phd Pursuing</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${telePhdPursuingFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=telePhdPursuingList/>
	
	<h3>Phd Awardees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${telePhdAwardeesFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=telePhdAwardeesList/>
	
	
	
	
		
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${teleInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=teleInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${telePedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=telePedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${telePedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=telePedagogyReviewFormList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${teleLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=teleLabManualList/>
	
	
	<h3>MOU's Signed</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleMouSignedFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleMouSignedList/>
	
	<h3>Alumni Association</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleAlumniAssociationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleAlumniAssociationList/>
	
	
	
	
	<h3>Professional Bodies</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleProfessionalBodiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleProfessionalBodiesList/>
	
	<h3>Professional Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${teleProfessionalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=teleProfessionalLinksList/>
	
	<h3>Higher Education</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleHigherEducationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleHigherEducationList/>
	
	<h3>Student Club Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleClubActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleClubActivitiesList/>
	
	<h3>Student Club External Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${teleClubLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=teleClubExternalLinksList/>
	
	<h3>Internship</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleInternshipFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleInternshipList/>
	
	<h3>Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleProjectsList/>
	
	<h3>Mini Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleMiniProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleMiniProjectsList/>
	
	<h3>Social Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleSocialActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleSocialActivitiesList/>
	
	<h3>Industrial Visit</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${teleIndustrialVisitFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=teleIndustrialVisitList/>

	<h3>Project Exhibition</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${teleProjectExhibitionFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=teleProjectExhibitionList/>
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${teleFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=teleFdpList/>
	

	<h3>Feedback By Alumni</h3>
	<@feedbackAlumniMacro feedbackAlumniList=feedbackAlumniList/>
	
	<h3>Feedback By Student</h3>
	<@feedbackStudentMacro feedbackStudentList=feedbackStudentList/>
	
	<h3>Feedback By Parent</h3>
	<@feedbackParentMacro feedbackParentList=feedbackParentList/>
	
	<h3>Feedback By Employer</h3>
	<@feedbackEmployerMacro feedbackEmployerList=feedbackEmployerList/>
		
</div>
</@pageAdmin>