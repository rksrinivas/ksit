<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${csicbPageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=csicbPageSliderImageList/>
		
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${csicbLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=csicbPageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${csicbUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=csicbPageUpcomingEventsList/>
	
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${csicbEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=csicbEventList/>
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	<h3>Supporting Staff</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>
		
	
	<h3>Department Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=csicbDepartmentAchieversList/>
	
	<h3>FCD Students List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbFcdStudentsFieldName!}>
		Add Students List
	</a>
	<@galleryMacro galleryList=csicbFcdStudentsList/>
	
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${csicbClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@achieverMacro achieverList=csicbClassTimeTableList/>
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${csicbTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@achieverMacro achieverList=csicbTestTimeTableList/>
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${csicbCalanderFieldName!}>
		Add Calendar
	</a>
	<@achieverMacro achieverList=csicbCalanderList/>
	
	<h3>UG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbUgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbUgSyllabusList/>
	
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbLibraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=csicbLibraryBooksList/>
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${csicbLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=csicbLabList/>
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csicbEresourcesList/>

		
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csicbInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbPedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csicbPedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbPedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csicbPedagogyReviewFormList/>

	<h3>Content Beyond Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbcontentbeyondsyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbcontentbeyondsyllabusList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csicbLabManualList/>
	
	
	<h3>MOU's Signed</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbMouSignedFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbMouSignedList/>
	
	
	<h3>Professional Bodies</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbProfessionalBodiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbProfessionalBodiesList/>
	
	<h3>Professional Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbProfessionalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csicbProfessionalLinksList/>
	
	<h3>Student Club Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbClubActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbClubActivitiesList/>
	
	<h3>Student Club External Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csicbClubLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csicbClubExternalLinksList/>
	
	<h3>Internship</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbInternshipFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbInternshipList/>
	
	<h3>Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbProjectsList/>
	
	<h3>Mini Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbMiniProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbMiniProjectsList/>
	
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csicbFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csicbFdpList/>
		
</div>
</@pageAdmin>