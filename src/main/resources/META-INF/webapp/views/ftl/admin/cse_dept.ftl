<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">

	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${csePageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=csePageSliderImageList/>
	
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${cseLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=csePageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${cseUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=csePageUpcomingEventsList/>
		
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	<h3>FCD Students List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseFcdStudentsFieldName!}>
		Add Students List
	</a>
	<@galleryMacro galleryList=cseFcdStudentsList/>
	
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${cseEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=cseEventList/>
	
	
	<h3>Testimonials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${testimonialFieldName!}>
		Add Testimonial
	</a>
	<@testimonialMacro testimonialList=testimonialList/>
	
	
	<!--<h3>Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${achieverFieldName!}>
		Add Achievers
	</a>
	<@achieverMacro achieverList=achieverList/> -->
	
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
		
	
	<h3>Supporting Staff</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Gallery
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	
	
	<h3>Department Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=cseDepartmentAchieversList/>
	
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@galleryMacro galleryList=cseClassTimeTableList/>
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@galleryMacro galleryList=cseTestTimeTableList/>
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseCalanderFieldName!}>
		Add Calendar
	</a>
	<@galleryMacro galleryList=cseCalanderList/>
	
	
	
	<h3>UG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseUgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseUgSyllabusList/>
	
	<h3>PG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csePgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csePgSyllabusList/>
	
	
	
	<h3>News Letter</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseNewsLetterFieldName!}>
		Add newsletter
	</a>
	<@galleryMacro galleryList=cseNewsLetterList/>
	
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cseLibraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=cseLibraryBooksList/>
	
	<h3>Placement List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csePlacementFieldName!}>
		Add Placement List
	</a>
	<@galleryMacro galleryList=csePlacementList/>
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${cseLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=cseLabList/>
	
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cseEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cseEresourcesList/>
	
	<h3>Research and Development</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseResearchFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=cseResearchList/>
	
	<h3>Sponsored Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseSponsoredProjectsFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=cseSponsoredProjectsList/>
	
	
	<h3>Phd Pursuing</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csePhdPursuingFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=csePhdPursuingList/>
	
	<h3>Phd Awardees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csePhdAwardeesFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=csePhdAwardeesList/>
	
	
	<!-- <h3>Phd Pursuing</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${csePhdPursuingFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=csePhdPursuingList/>
	
	<h3>Phd Awardees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${csePhdAwardeesFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=csePhdAwardeesList/>
	
	
	 -->
	
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cseInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cseInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csePedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csePedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${csePedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=csePedagogyReviewFormList/>

	<h3>Content Beyond Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${csecontentbeyondsyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=csecontentbeyondsyllabusList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cseLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cseLabManualList/>
	
	
	<h3>MOU's Signed</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseMouSignedFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseMouSignedList/>
	
	<h3>Alumni Association</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseAlumniAssociationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseAlumniAssociationList/>
	
	
	
	
	
	<h3>Professional Bodies</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseProfessionalBodiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseProfessionalBodiesList/>
	
	<h3>Professional Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cseProfessionalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cseProfessionalLinksList/>
	
	<h3>Higher Education</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseHigherEducationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseHigherEducationList/>
	
	<h3>Student Club Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseClubActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseClubActivitiesList/>
	
	<h3>Student Club External Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cseClubLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cseClubExternalLinksList/>
	
	<h3>Internships</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseInternshipFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseInternshipList/>
	
	<h3>Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseProjectsList/>
	
	<h3>Mini Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseMiniProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseMiniProjectsList/>
	
	<h3>Social Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseSocialActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseSocialActivitiesList/>
	
	<h3>Industrial Visit</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${cseIndustrialVisitFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=cseIndustrialVisitList/>
	
	
	
	<h3>Project Exhibition</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${cseProjectExhibitionFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=cseProjectExhibitionList/>
	
	<h3>Other Details</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseOtherDetailsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseOtherDetailsList/>
	
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cseFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cseFdpList/>
	
	
	
	
	
	
	
	
	<!--<h3>Project Exhibition</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${cseSponsoredProjectsFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=cseSponsoredProjectsList/>-->
	
	
	
	
	<!--<h3>Department Labs</h3>
	
	<table class="table table-striped table-bordered">
		<tr>
			<th>SI. No.</th>
			<th>Name of the Laboratory</th>
			<th>Configuration of the systems	</th>
			<th>Number of Systems	</th>
			<th>Purpose of the laboratory (Conduction of following laboratories)
			</th>
			<th>image</th>
			<th>Delete</th>
		</tr>
		<#list labDataList as labData>
			<tr>
				<td>${labData.serialNo!}.</td>
				<td>${labData.name!}</td>
				<td>
					<ul class="ul-list">
						<#list labData.configurations as configuration>
							<li>${configuration}</li>
						</#list>
					</ul>
				</td>	
				<td>
					<ul class="ul-list">
						<#list labData.noOfSystems as noOfSystem>
							<li>${noOfSystem}</li>
						</#list>
					</ul>
				</td>
				<td>
					<ul class="ul-list">
						<#list labData.purposes as purpose>
							<li>${purpose}</li>
						</#list>
					</ul>
				</td>
				<td>
					<#if labData.imageURL?has_content>
						<td><img class="img-table" width=100 height=100 src="${labData.imageURL}" alt="" /></td>
					</#if>
				</td>
				<td>
					<button type="submit" class="btn btn-danger" onclick="deleteLabData('${labData.id}');">Delete</button>
				</td>
			</tr>
		</#list>
	</table>
	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addLabDataModal">Add Lab Data</button> -->
	
	<h3>Feedback By Alumni</h3>
	<@feedbackAlumniMacro feedbackAlumniList=feedbackAlumniList/>
	
	<h3>Feedback By Student</h3>
	<@feedbackStudentMacro feedbackStudentList=feedbackStudentList/>
	
	<h3>Feedback By Parent</h3>
	<@feedbackParentMacro feedbackParentList=feedbackParentList/>
	
	<h3>Feedback By Employer</h3>
	<@feedbackEmployerMacro feedbackEmployerList=feedbackEmployerList/>
	
</div>
</@pageAdmin>