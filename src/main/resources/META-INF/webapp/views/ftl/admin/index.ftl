<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Scrolling Text Top</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextTopFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextTopList/>
	
	<h3>Advertisement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${messageBoxFieldName!}>
		Add Image
	</a>
	<@testimonialMacro testimonialList=messageBoxList/>

	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${homePageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=homePageSliderImageList/>
	
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${latestEventsHomeFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=homePageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${upcomingEventsHomeFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=homePageUpcomingEventsList/>
	
	<h3>Youtube Link</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${youtubeLinkFieldName!}>
		Add link
	</a>
	<@dataMacro dataList=youtubeLinkList/>

	<h3>Achievers Home Page</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${achieversHomeFieldName!}>
		Add achievers
	</a>
	<@testimonialMacro testimonialList=achieversList/>

	<h3>Testimonials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${testimonialFieldName!}>
		Add Testimonial
	</a>
	<@testimonialMacro testimonialList=testimonialList/>
	
	<h3>Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${homepagetimetableFieldName!}>
		Add Time Table
	</a>
	<@galleryMacro galleryList=homepagetimetableList/>
	
	<h3>Circular</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${homepagecircularFieldName!}>
		Add Time Table
	</a>
	<@galleryMacro galleryList=homepagecircularList/>
	
	<h3>Academic Calander</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${academicCalanderFieldName!}>
		Add Calander
	</a>
	<@achieverMacro achieverList=academicCalanderList/>
	
	<h3>News Letter</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${newsletterPageNewsLetterFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=newsletterPageNewsLetterList/>
	
	<h3>Souvenir</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${souvenirPageSouvenirFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=souvenirPageSouvenirList/>
	
	<h3>Hostel Fees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${hostelFeesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=hostelFeesList/>
	
</div>
</@pageAdmin>