<@pageAdmin>
<style>
table, tr, td {
  border: 1px solid black;
}
</style>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Online Admissions</h3>
	<table class="table table-striped table-bordered" style="display:block; overflow-x:auto">
		<tbody>
			<#if onlineAdmissionList?has_content>
				<#list onlineAdmissionList as onlineAdmission>
					<tr>
						<td>
							Branch ${onlineAdmission.admittedBranch!}
							<br />
							CET Rank ${onlineAdmission.cetRank!}
							<br />
							Photo ${onlineAdmission.photoUrl!}
							<br />
							Candidate Name ${onlineAdmission.candidateName!}
							<br />
							DOB ${onlineAdmission.dob!}
							<br />
							Gender ${onlineAdmission.gender!}
							<br />
							Caste ${onlineAdmission.caste!}
							<br />
							Category ${onlineAdmission.category!}
							<br />
							Religion ${onlineAdmission.religion!}
							<br />
							Nationality ${onlineAdmission.nationality!}
							<br />
							Mobile No ${onlineAdmission.mobileNo!}
							<br />
							Email ${onlineAdmission.email!}
							<br />
						</td>
						<td>
							Father's Name ${onlineAdmission.fathersName!}
							<br />
							Father's Occupation ${onlineAdmission.fathersOccupation!}
							<br />
							Father's Annual Income ${onlineAdmission.fathersAnnualIncome!}
							<br />
							Father's Mobile No ${onlineAdmission.fathersMobileNo!}
							<br />
							Father's Email ${onlineAdmission.fathersEmail!}
							<br />
						</td>
						<td>
							Mother's Name ${onlineAdmission.mothersName!}
							<br />
							Mother's Occupation ${onlineAdmission.mothersOccupation!}
							<br />
							Mother's Annual Income ${onlineAdmission.mothersAnnualIncome!}
							<br />
							Mother's Mobile No ${onlineAdmission.mothersMobileNo!}
							<br />
							Mother's Email ${onlineAdmission.mothersEmail!}
							<br />
						</td>
						<td>
							Guardian's Name: ${onlineAdmission.guardiansName!}
							<br />
							Guardian's Mobile No: ${onlineAdmission.guardiansMobileNo!}
							<br /> 
						</td>
						<td>
							Address for Correspondance: ${onlineAdmission.correspondenceAddress!}
							<br />
							${onlineAdmission.correspondencePinCode!}
							<br />
							${onlineAdmission.permanentAddress!}
							<br />
							${onlineAdmission.permanentPinCode!}
							<br />
							Details of Previous Institution attended: ${onlineAdmission.prevInstitution!}
							<br />
							Board: ${onlineAdmission.prevInstitutionBoard!}
							<br /> 
							Date of Leaving: ${onlineAdmission.prevDateOfLeaving!}
							<br />
							Register Number of Equivalent Exam: ${onlineAdmission.prevRegisterNo!}
							<br />
							Year of Passing: ${onlineAdmission.prevPassingYear!}
							<br />
						</td>
						<td>
							Physics ${onlineAdmission.physicsMarks!}/${onlineAdmission.physicsMax!}
							<br />
							Mathematics ${onlineAdmission.mathsMarks!}/${onlineAdmission.mathsMax!}
							<br />
							Chemistry/Bio./Elct./Comp./Etc ${onlineAdmission.otherMarks!}/${onlineAdmission.otherMax!}
							<br />
							Total  ${onlineAdmission.totalMarks!}/${onlineAdmission.totalMax!} ${onlineAdmission.percentage!}
						</td>
						<td>
							<b>KEA / CET</b>
							Year: ${onlineAdmission.keaYear!}
							Number: ${onlineAdmission.keaRegNo!}
							Rank ${onlineAdmission.keaRank!}
							<br />
							<b>COMED-K</b>
							Year: ${onlineAdmission.comedYear!}
							Number: ${onlineAdmission.comedRegNo!}
							Rank ${onlineAdmission.comedRank!}
							<br />
							<b>JEE</b>
							Year: ${onlineAdmission.jeeYear!}
							Number: ${onlineAdmission.jeeRegNo!}
							Rank ${onlineAdmission.jeeRank!}
							<br />
							<b>Others</b>
							Year: ${onlineAdmission.othersYear!}
							Number: ${onlineAdmission.othersRegNo!}
							Rank ${onlineAdmission.othersRank!}
							<br />
							Sports & Extra Curricular Activites: 
							${onlineAdmission.extraActivites!}
						</td>
						<td>
							SSLC / X Standard Marks Card 
							<img class="img-table" width=100 height=100 src="${onlineAdmission.sslcMarksCardUrl!}" alt="" />
							<br />
							
							PUC / XII Standard Marks Card
							<img class="img-table" width=100 height=100 src="${onlineAdmission.pucMarksCardUrl!}" alt="" />
							<br />
							
							CET / COMED-K / JEE/ UGET Rank
							<img class="img-table" width=100 height=100 src="${onlineAdmission.rankCertificateUrl!}" alt="" />							
							<br />
							
							Transfer Certificate
							<img class="img-table" width=100 height=100 src="${onlineAdmission.transferCertificateUrl!}" alt="" />							
							<br />
							
							Migration Certificate
							<img class="img-table" width=100 height=100 src="${onlineAdmission.migrationCertificateUrl!}" alt="" />							
							<br />
							
							Physical Fitness Certificate
							<img class="img-table" width=100 height=100 src="${onlineAdmission.physicalFitnessCertificateUrl!}" alt="" />							
							<br />
							
							Student VISA, Clearance from Ministry of HRD,
							<br /> 
							AIU Equivalance, Passport & attested copy in case of foreign students only
							<img class="img-table" width=100 height=100 src="${onlineAdmission.foreignStudentsCertificateUrl!}" alt="" />
						</td>
						<td>
							Transaction No ${onlineAdmission.transactionNo!}
							<br />
							<img class="img-table" width=100 height=100 src="${onlineAdmission.transactionNo!}" alt="" />
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
	<ul class="pager divInRight ">
		<#if hasPrev?has_content && hasPrev>
			<li><a href="onlineadmissions?page=${prevPage!}">Prev</a></li>
		</#if>
		<#if hasNext?has_content && hasNext>
			<li><a href="onlineadmissions?page=${nextPage!}">Next</a></li>
		</#if>
	</ul>
</div> 
</@pageAdmin>