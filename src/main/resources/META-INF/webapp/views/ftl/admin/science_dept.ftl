<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=eventList/>
	
	<h3>Physics Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${physicsFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=physicsFacultyList/>
	
	<h3>Chemistry Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${chemistryFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=chemistryFacultyList/>
	
	<h3>Maths Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${mathsFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=mathsFacultyList/>
	
	<h3>Humanities Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${humanitiesFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=humanitiesFacultyList/>
	
	<h3>Supporting Staff Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${scienceClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@achieverMacro achieverList=scienceClassTimeTableList/>
	
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${scienceTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@achieverMacro achieverList=scienceTestTimeTableList/>
	
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${scienceCalanderFieldName!}>
		Add Calendar
	</a>
	<@achieverMacro achieverList=scienceCalanderList/>
	
	
	
	<h3>Physics Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${physicsSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=physicsSyllabusList/>
	
	<h3>Chemistry Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${chemistrySyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=chemistrySyllabusList/>
	
	<h3>Mathematics Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mathematicsSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mathematicsSyllabusList/>
	
	
	
	
	<!-- <h3>News Letter</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${scienceNewsLetterFieldName!}>
		Add newsletter
	</a>
	<@achieverMacro achieverList=scienceNewsLetterList/> -->
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${scienceLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=scienceLabList/>
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${scienceEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=scienceEresourcesList/>
	
	<h3>Research and Development</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${scienceResearchFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=scienceResearchList/>
	
	<h3>Sponsored Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${scienceSponsoredProjectsFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=scienceSponsoredProjectsList/>
	
	
	<h3>Phd Pursuing</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${sciencePhdPursuingFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=sciencePhdPursuingList/>
	
	<h3>Phd Awardees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${sciencePhdAwardeesFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=sciencePhdAwardeesList/>
	
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${scienceInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=scienceInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${sciencePedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=sciencePedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${sciencePedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=sciencePedagogyReviewFormList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${scienceLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=scienceLabManualList/>
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${scienceFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=scienceFdpList/>
	
	
</div>
</@pageAdmin>