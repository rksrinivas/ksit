<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${ccePageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=ccePageSliderImageList/>
		
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${cceLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=ccePageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${cceUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=ccePageUpcomingEventsList/>
	
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${cceEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=cceEventList/>
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	<h3>Supporting Staff</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>
		
	
	<h3>Department Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=cceDepartmentAchieversList/>
	
	<h3>FCD Students List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceFcdStudentsFieldName!}>
		Add Students List
	</a>
	<@galleryMacro galleryList=cceFcdStudentsList/>
	
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${cceClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@achieverMacro achieverList=cceClassTimeTableList/>
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${cceTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@achieverMacro achieverList=cceTestTimeTableList/>
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${cceCalanderFieldName!}>
		Add Calendar
	</a>
	<@achieverMacro achieverList=cceCalanderList/>
	
	<h3>UG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceUgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceUgSyllabusList/>
	
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cceLibraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=cceLibraryBooksList/>
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${cceLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=cceLabList/>
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cceEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cceEresourcesList/>

		
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cceInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cceInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${ccePedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=ccePedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${ccePedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=ccePedagogyReviewFormList/>

	<h3>Content Beyond Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${ccecontentbeyondsyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=ccecontentbeyondsyllabusList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cceLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cceLabManualList/>
	
	
	<h3>MOU's Signed</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceMouSignedFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceMouSignedList/>
	
	
	<h3>Professional Bodies</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceProfessionalBodiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceProfessionalBodiesList/>
	
	<h3>Professional Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cceProfessionalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cceProfessionalLinksList/>
	
	<h3>Student Club Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceClubActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceClubActivitiesList/>
	
	<h3>Student Club External Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${cceClubLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=cceClubExternalLinksList/>
	
	<h3>Internship</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceInternshipFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceInternshipList/>
	
	<h3>Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceProjectsList/>
	
	<h3>Mini Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceMiniProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceMiniProjectsList/>
	
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${cceFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=cceFdpList/>
		
</div>
</@pageAdmin>