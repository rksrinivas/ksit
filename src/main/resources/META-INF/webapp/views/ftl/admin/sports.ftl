<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${sportsActivitiesFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=sportsActivitiesList/>
	
    <h3>College Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${sportsAchieversFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=sportsAchieversList/>
	
	 <h3>University Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${universityAchieversFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=universityAchieversList/>
	
	 <h3>State Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${stateAchieversFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=stateAchieversList/>
	
	 <h3>National Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${nationalAchieversFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=nationalAchieversList/>
	
	<h3>Reports</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${sportsReportsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=sportsReportsList/>
	
	
</div>
</@pageAdmin>