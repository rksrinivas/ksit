<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Admission Enquiries</h3>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Course</th>
			<th>Address</th>
			<th>SSLC</th>
			<th>PUC</th>
			<th>Date</th>
		</thead>
		<tbody>
			<#if courseList?has_content>
				<#list courseList as course>
					<tr>
						<td>
							${course.name!}
						</td>
						<td>
							${course.email!}
						</td>
						<td>
							${course.phone!}
						</td>
						<td>
							${course.course!}
						</td>
						<td>
							${course.address!}
						</td>
						<td>
							${course.sslc!}
						</td>
						<td>
							${course.puc!}
						</td>
						<td>
							${course.createdOn!}
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
	<ul class="pager divInRight ">
		<#if hasPrev?has_content && hasPrev>
			<li><a href="course?page=${prevPage!}">Prev</a></li>
		</#if>
		<#if hasNext?has_content && hasNext>
			<li><a href="course?page=${nextPage!}">Next</a></li>
		</#if>
	</ul>
</div> 
</@pageAdmin>