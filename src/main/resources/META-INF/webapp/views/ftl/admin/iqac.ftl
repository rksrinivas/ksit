<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Members</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Member
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	
	<h3>Annual Composition Data</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${annualCompositionFieldName!}>
		Add Annual Composition Data
	</a>
	<@eventMacro eventList=annualCompositionList/>
	
	<h3>Minutes Of Meeting</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${minutesOfMeetingFieldName!}>
		Add Minutes Of Meeting Data
	</a>
	<@eventMacro eventList=minutesOfMeetingList/>
	
	<h3>Best Practices</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${bestPractiseFieldName!}>
		Add Best Practice
	</a>
	<@eventMacro eventList=bestPractiseList/>
	
	<h3>Annual Reports</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${annualReportsFieldName!}>
		Add Annual Report
	</a>
	<@eventMacro eventList=annualReportsList/>
	
	<h3>Other Reports</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${annualReportsFieldName!}>
		Add Other Reports
	</a>
	<@eventMacro eventList=otherReportsList/>
</div>
</@pageAdmin>