<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	<h3>Meets and Events </h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${alumniMeetsFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=alumniMeetsList/>
	
	<h3>CSE Members</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${cseAlumniMembersFieldName!}>
		Add CSE Members
	</a>
	<@achieverMacro achieverList=cseAlumniMembersList/>
	
	
	<h3>ECE Members</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${eceAlumniMembersFieldName!}>
		Add ECE Members
	</a>
	<@achieverMacro achieverList=eceAlumniMembersList/>
	
	
	<h3>ME Members</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${mechAlumniMembersFieldName!}>
		Add ME Members
	</a>
	<@achieverMacro achieverList=mechAlumniMembersList/>
	
	<h3>TCE Members</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${teleAlumniMembersFieldName!}>
		Add TCE Members
	</a>
	<@achieverMacro achieverList=teleAlumniMembersList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${alumniGalleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=alumniGalleryList/>
	
		
</div>

</@pageAdmin>