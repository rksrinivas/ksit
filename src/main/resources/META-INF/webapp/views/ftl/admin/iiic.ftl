<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>MoU's signed till date</h3>

	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${mouFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=mouSignedList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Gallery
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	<h3>Members</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Member
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	<h3>Activities Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${activitiesFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=activitiesEventsList/>
</div>
</@pageAdmin>