<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Contact Us</h3>
	<form action="/contactUs" method="get">
		<table class="table table-striped table-bordered" style="width:80%">
			<thead style="width:100%">
				<th>Grievance Details</th>
				<th>Subject</th>
				<th>Description</th>
			</thead>
			<tbody>
				<#if grievanceList?has_content>
					<#list grievanceList as grievance>				
					
						<tr>
							<td>
								<p>Name: ${grievance.name!}</p>
								<p>Category: ${grievance.category!}</p>
								<p>Course: ${grievance.course!}</p>
								<p>Branch: ${grievance.branch!}</p>
								<p>Usn: ${grievance.usn!}</p>
								<p>Email: ${grievance.email!}</p>
								<p>Mobile: ${grievance.mobileNumber!}</p>
								
								
								
							</td>
							
							
							<td>
								${grievance.subject!}
							</td>
							<td>
								${grievance.description!}
							</td>
						</tr>
					</#list>
				</#if>
			</tbody>
			<tfoot style="width:100%">
				<th>Grievance Details</th>
				<th>Subject</th>
				<th>Description</th>
			</tfoot>
		</table>
		
				
		<ul class="pager divInRight ">
			<#if hasPrev?has_content && hasPrev>
				<li><a href="contactUs?page=${prevPage!}">Prev</a></li>
			</#if>
			<#if hasNext?has_content && hasNext>
				<li><a href="contactUs?page=${nextPage!}">Next</a></li>
			</#if>
		</ul>
	</form>
</div> 
</@pageAdmin>