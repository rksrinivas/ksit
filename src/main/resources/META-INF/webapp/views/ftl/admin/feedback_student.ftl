<@pageAdmin title=Feedback showFooter=false>
<div id="file_content">
	<table>
		<tbody>
			<tr>
				<th>
					Student Name:
				</th>
				<td>
				 	${feedbackStudent.nameOfTheStudent}
				 </td>
			</tr>
			<tr>
				<th>
					Semester & Section:
				</th>
				<td>
					${feedbackStudent.semesterAndSection}
				</td>
			</tr>
			<tr>
				<th>
					Academic Year:
				</th>
				<td>
					${feedbackStudent.academicYear}
				</td>
			</tr>
			<tr>
				<th>			
					USN:
				</th>
				<td>
					${feedbackStudent.usn}
				</td>
			</tr>
			<tr>
				<th>
					Email:
				</th>
				<td>
					${feedbackStudent.emailId}
				</td>
			</tr>
			<tr>
				<th>
					Phone No:
				</th>
				<td>
					${feedbackStudent.phoneNo}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Vision:
				</th>
				<td>
					${feedbackStudent.visionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Mission:
				</th>
				<td>
					${feedbackStudent.missionSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Suggestion About Program Educational Objectives (PEO):
				</th>
				<td>
					${feedbackStudent.peoSuggestion}
				</td>
			</tr>
			<tr>
				<th>
					Whether the Department is moving towards right path to achieve its vision &amp; mission?
				</th>
				<td>
					${feedbackStudent.visionMission}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree our vision statement captures where we should be heading as a department?
				</th>
				<td>
					${feedbackStudent.visionAgree}
				</td>
			</tr>
			<tr>
				<th>
					Do you agree that our mission statements will enable us to achieve our vision?
				</th>
				<td>
					${feedbackStudent.missionAgree}
				</td>
			</tr>
			<#list studentFeedbacks as studentFeedback>
				<tr>
					<th>
						${studentFeedback?index+1}. ${studentFeedback}
					</th>
					<td>
						<#assign key = "response${studentFeedback?index+1}">
						${feedbackStudent[key].desc}
					</td>
				</tr>
			</#list>
			<tr>
				<th>
					Any other suggestions/comments:
				</th>
				<td>
					${feedbackStudent.response}
				</td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	$(document).ready(function(){
	    var doc = new jsPDF('p','pt', 'a4');
	    var specialElementHandlers = {
	        '#editor': function (element, renderer) {
	            return true;
	        }
	    };
	    var download = getParameterByName('download');
	    if(download){
	    	let options = {
	            'width': 500,
	            'elementHandlers': specialElementHandlers
	        };
	        let x = 15;
	        let y = 15;
	        pageHeight= doc.internal.pageSize.height;
	    	$('table > tbody  > tr').each(function(index, tr) {
	    		if (y >= pageHeight){
	    			doc.addPage();
	    			y = 0;
	    		} 
	    		doc.fromHTML($(tr).html(), x, y, options);
	    		y += $(tr).height() * 2;
			});
	        doc.save('feedback_student_${feedbackStudent.id}.pdf');
	    }
    });
</script>
</@pageAdmin>
