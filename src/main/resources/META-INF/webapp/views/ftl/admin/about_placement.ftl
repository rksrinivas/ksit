<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	<!--<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${placementLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=placementPageLatestEventsList/> -->
	
	<h3>Placement News</h3> <!-- <h3>upcoming Events </h3>-->
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${placementUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=placementPageUpcomingEventsList/>
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=eventList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Gallery
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	<h3>Yearwise Companies List / Companies </h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addPlacementModal" data-page=${pageName!} data-field_name=${placementCompanyFieldName!}>
		Add company details
	</a>
	<@placementCompanyMacro placementCompanyList=placementCompanyList/>
	
	<h3>Placement Training Programs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addPlacementTrainingModal" data-page=${pageName!} data-field_name=${placementTrainingFieldName!}>
	   Add training details
	</a>
	<@placementTrainingMacro placementTrainingList=placementTrainingList/>
	
	
	<h3>Higher Studies/GRE</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addPlacementTrainingModal" data-page=${pageName!} data-field_name=${placementHigherStudiesFieldName!}>
	   Add Higher Studies details
	</a>
	<@placementTrainingMacro placementTrainingList=placementHigherStudiesList/>
	
	<h3>Internship Programs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addPlacementTrainingModal" data-page=${pageName!} data-field_name=${placementInternshipFieldName!}>
	   Add internship details
	</a>
	<@placementTrainingMacro placementTrainingList=placementInternshipList/>
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
		
	
	<h3>Supporting Staff</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	
	
</div>
</@pageAdmin>