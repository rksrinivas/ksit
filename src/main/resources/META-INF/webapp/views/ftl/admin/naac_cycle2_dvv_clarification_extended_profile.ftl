<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	<h3>Naac Cycle 2 DVV Clarification Extended Profiles</h3>
	<div class="row">
		<div class="col-lg-4">
			<form action="/admin/naac_cycle2_dvv_clarification_extended_profile" method="post" enctype="multipart/form-data">
				Serial No
				<input type="number" name="year" min="1" required>
				<div class="wpcf7-text"> 
					Heading <input type="text" name="heading" required>
				</div> 
				File <input type="file" name="file" required/>
				<br />
				<br />
				<button type="submit" class="btn btn-primary" value="Submit">Save</button>
			</form>
		</div>
	</div>
	<br />
	<h3>Saved NAAC Details</h3>
	<div class="row">
		<div class="col-lg-6">
			<table class="table table-striped table-bordered" style="width="100%">
				<tr>
					<th>
						Serial No
					</th>
					<th>
						Heading
					</th>
					<th>
						Link
					</th>
					<th>
						Delete
					</th>
				</tr>
				<#list naacList as naac>
					<tr>
						<td>${naac.year}</td>
						<td>${naac.heading}</td>
						<td><a href="${naac.link}" target="_blank">View</a></td>
						<td>
							<form action="/admin/naac/delete/${naac.id}" method="post">
								<button type="submit" value="Submit">Delete</button>						
							</form>
						</td>
					</tr>
				</#list>
			</table>
		</div>
	</div>
</div>
</@pageAdmin>