<style>
	.labcontainer input[type=text] {
		padding:5px 0px;
		margin:5px 5px 5px 0px;
	}

	.add_configuration_lab_data, .add_no_of_system_lab_data, .add_purpose_lab_data 
	{
	    background-color: #1c97f3;
	    border: none;
	    color: white;
	    padding: 8px 32px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;
	    margin: 4px 2px;
	    cursor: pointer;
	    border:1px solid #186dad;
	}
	
	.lab_input{
	    border: 1px solid #1c97f3;
	    width: 260px;
	    height: 40px;
	 	margin-bottom:14px;
	}
	
	.delete{
	    background-color: #fd1200;
	    border: none;
	    color: white;
	    padding: 5px 15px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 14px;
	    margin: 4px 2px;
	    cursor: pointer;
	}
</style>

<div class="mymodal fade" id="addLabDataModal" data-backdrop="false" data-keyboard="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content flat-style">
          <div class="modal-header" style ="background-color: #fd3753;">
            <button type="button" class="close" data-dismiss="modal" style="margin-top: 10px;"><i class="icon icon-remove-circle"></i>x</button>
            <div class="row">
              <div class="col-xs-9" style="margin-top: 4px;margin-left: 13px;">
                <h4 class="modal-title">Add Lab Data</h4>
              </div>
            </div>
          </div><!-- /.modal-header -->
          <div class="modal-body">
            
            <input type="hidden" name="page" id="pageAddValueLabData" value="cse_page" />
            <input type="hidden" name="fieldName" id="fieldNameAddValueLabData" value="cse_page_lab" />
            
            <#setting number_format="0" />
			<div>
                <h3>Serial No</h3>
                <input type="number" min="1" id="serialNoAddValueLabData" name="serialNo" value="1"/>
           	</div>
           	
            <div>
            	<h3>Name</h3>
            	<input type="text" name="name" id="nameAddValueLabData" />
            </div>
            
            <div>
	            <h3>Image</h3>
	            <input type="file" name="image" id="imageAddValueLabData" />
           	</div>
           	
           	<br />
           	<div class="labcontainer">
				<button class="add_configuration_lab_data">Add Configuration of the systems</button>
				<div>
					<textarea rows="2" cols="50" class = "lab_input" name="configurationsLabData[]"></textarea>
				</div>
			</div>
			
			<br />
			<div class="labcontainer">
				<button class="add_no_of_system_lab_data">Add No. of Systems</button>
				<div>
				   <input type="number" min="1" name="noOfSystemsLabData[]" value="1"/>
				</div>
			</div>
           	
           	<br />
           	<div class="labcontainer">
				<button class="add_purpose_lab_data">Add Purpose</button>
				<div>
					<textarea rows="2" cols="50" class = "lab_input" name="purposesLabData[]"></textarea>
				</div>
			</div>
           	
          <div>
            <h3>Active</h3>
            <select id="activeAddValueLabData" name="active">
              <option value="true">True</option>
              <option value="false">False</option>
            </select>
          </div>
          
      	  <#setting number_format="0" />
          <div>
            <h3>Rank</h3>
            <input type="number" min="1" id="rankAddValueLabData" name="rank" value="1"/>
          </div>     
            
            <div>
              <button type="button" class="btn btn-primary" id="cancelAddLabData">Cancel</button>
              <button type="submit" class="btn btn-success" id="saveAddLabData" >Save</button>
            </div>            
              
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->