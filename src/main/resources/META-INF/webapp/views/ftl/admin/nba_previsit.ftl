<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">
	
	<@addNBAMacro/>
	<br />
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addNBADataModal">
		Add NBA Detail
	</a>
	<h3>Saved NBA Details</h3>
	<div class="row">
		<@nbaMacro nbaPrevisitList=nbaPrevisitList/>
	</div>
</div>
</@pageAdmin>