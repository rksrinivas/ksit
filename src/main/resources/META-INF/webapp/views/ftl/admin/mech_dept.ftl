<@pageAdmin>
<div class="container" style="background-color:white;min-height:600px">

	<h3>Slider</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addSliderModal" data-page=${pageName!} data-field_name=${mechPageSliderImageFieldName!}>
		Add Slider
	</a>
	<@sliderMacro sliderImageList=mechPageSliderImageList/>
	
	<h3>Latest Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${mechLatestEventsFieldName!}>
		Add Latest Events
	</a>
	<@testimonialMacro testimonialList=mechPageLatestEventsList/>
	
	<h3>Upcoming Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${mechUpcomingEventsFieldName!}>
		Add Upcoming Events
	</a>
	<@testimonialMacro testimonialList=mechPageUpcomingEventsList/>
	
	
	
	<h3>Departmentwise Announcement</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDataModal" data-page=${pageName!} data-field_name=${scrollingTextBottomFieldName!}>
		Add Data
	</a>
	<@dataMacro dataList=scrollingTextBottomList/>
	
	<h3>Events</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${mechEventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=mechEventList/>
	
	<h3>Testimonials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addTestimonialModal" data-page=${pageName!} data-field_name=${testimonialFieldName!}>
		Add Testimonial
	</a>
	<@testimonialMacro testimonialList=testimonialList/>
	
	<!--<h3>Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${achieverFieldName!}>
		Add Achievers
	</a>
	<@achieverMacro achieverList=achieverList/> -->
	
	<h3>Faculty</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${facultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=facultyList/>
	
	
	<h3>Supporting Staff</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addFacultyModal" data-page=${pageName!} data-field_name=${supportingStaffFacultyFieldName!}>
		Add Faculty
	</a>
	<@facultyMacro facultyList=supportingStaffFacultyList/>
	
	<h3>Gallery</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${galleryFieldName!}>
		Add Event
	</a>
	<@galleryMacro galleryList=galleryList/>
	
	
	
	<h3>Department Achievers</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addEventModal" data-page=${pageName!} data-field_name=${eventFieldName!}>
		Add Event
	</a>
	<@eventMacro eventList=mechDepartmentAchieversList/>
	
	<h3>FCD Students List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechFcdStudentsFieldName!}>
		Add Students List
	</a>
	<@galleryMacro galleryList=mechFcdStudentsList/>
	
	
	<h3>Class Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${mechClassTimeTableFieldName!}>
		Add Class Time Table
	</a>
	<@achieverMacro achieverList=mechClassTimeTableList/>
	
	<h3>Test Time Table</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${mechTestTimeTableFieldName!}>
		Add Test Time Table
	</a>
	<@achieverMacro achieverList=mechTestTimeTableList/>
	
	
	<h3>Calendar</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addAchieversModal" data-page=${pageName!} data-field_name=${mechCalanderFieldName!}>
		Add Calendar
	</a>
	<@achieverMacro achieverList=mechCalanderList/>
	
	
	<h3>UG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechUgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechUgSyllabusList/>
	
	<h3>PG Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechPgSyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechPgSyllabusList/>
	
	
	
	<h3>News Letter</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechNewsLetterFieldName!}>
		Add newsletter
	</a>
	<@achieverMacro achieverList=mechNewsLetterList/>
	
	<h3>Collection of books</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechLibraryBooksFieldName!}>
		Add 
	</a>
	<@libraryMacro libraryList=mechLibraryBooksList/>
	
	<h3>Placement List</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechPlacementFieldName!}>
		Add Placement List
	</a>
	<@galleryMacro galleryList=mechPlacementList/>
	
	<h3>Infrastructure / Labs</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${mechLabFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=mechLabList/>
	
	<h3>E-resources</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechEresourcesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=mechEresourcesList/>
	
	
	<h3>Research and Development</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechResearchFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=mechResearchList/>
	
	<h3>Sponsored Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechSponsoredProjectsFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=mechSponsoredProjectsList/>
	
	
	<h3>Phd Pursuing</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechPhdPursuingFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=mechPhdPursuingList/>
	
	<h3>Phd Awardees</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechPhdAwardeesFieldName!}>
		Add 
	</a>
	<@galleryMacro galleryList=mechPhdAwardeesList/>
	
	
	
		
	<h3>Instructional Materials</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechInstructionalMaterialsFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=mechInstructionalMaterialsList/>
	
	<h3>Pedagogical Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechPedagogicalActivitiesFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=mechPedagogicalActivitiesList/>
	
	<h3>Pedagogy Review Form</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechPedagogyReviewFormFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=mechPedagogyReviewFormList/>

	<h3>Content Beyond Syllabus</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechcontentbeyondsyllabusFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechcontentbeyondsyllabusList/>
	
	<h3>Lab Manual</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechLabManualFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=mechLabManualList/>
	
	
	<h3>MOU's Signed</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechMouSignedFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechMouSignedList/>
	
	<h3>Alumni Association</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechAlumniAssociationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechAlumniAssociationList/>
	
	
	
		
	
	<h3>Professional Bodies</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechProfessionalBodiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechProfessionalBodiesList/>
	
	<h3>Professional Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechProfessionalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=mechProfessionalLinksList/>
	
	<h3>Higher Education</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechHigherEducationFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechHigherEducationList/>
	
	<h3>Student Club Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechClubActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechClubActivitiesList/>
	
	<h3>Student Club External Links</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addLibraryModal" data-page=${pageName!} data-field_name=${mechClubExternalLinksFieldName!}>
		Add (Add link in content area)
	</a>
	<@libraryMacro libraryList=mechClubExternalLinksList/>
	
	<h3>Internship</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechInternshipFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechInternshipList/>
	
	<h3>Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechProjectsList/>
	
	<h3>Mini Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechMiniProjectsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechMiniProjectsList/>
	
	<h3>Social Activities</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechSocialActivitiesFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechSocialActivitiesList/>
	
	<h3>Industrial Visit</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${mechIndustrialVisitFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=mechIndustrialVisitList/>

	
	<h3>Project Exhibition</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${mechProjectExhibitionFieldName!}>
		Add
	</a>
	<@deptLabMacro deptLabList=mechProjectExhibitionList/>
	
	<h3>Other Details</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechOtherDetailsFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechOtherDetailsList/>
	
	<h3>Faculty Development Program</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addGalleryModal" data-page=${pageName!} data-field_name=${mechFdpFieldName!}>
		Add
	</a>
	<@galleryMacro galleryList=mechFdpList/>
	

	<!--<h3>Sponsored Projects</h3>
	<a href="#" class="tooltips" data-toggle="modal" data-target="#addDeptLabModal" data-page=${pageName!} data-field_name=${mechSponsoredProjectsFieldName!}>
		Add Lab
	</a>
	<@deptLabMacro deptLabList=mechSponsoredProjectsList/>-->

	<h3>Feedback By Alumni</h3>
	<@feedbackAlumniMacro feedbackAlumniList=feedbackAlumniList/>
	
	<h3>Feedback By Student</h3>
	<@feedbackStudentMacro feedbackStudentList=feedbackStudentList/>
	
	<h3>Feedback By Parent</h3>
	<@feedbackParentMacro feedbackParentList=feedbackParentList/>
	
	<h3>Feedback By Employer</h3>
	<@feedbackEmployerMacro feedbackEmployerList=feedbackEmployerList/>
	
</div>
</@pageAdmin>