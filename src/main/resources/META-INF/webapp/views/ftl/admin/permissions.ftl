<@pageAdmin>
<style>
input { font-size: 18px; }
table
{
	width:500px;
}
#select1, #select2
{
	width: 200px;
   height: 150px;
}
#new
{
	float:left;
 	text-align: center;
  	margin: 10px;
}
#add
{
	display: block;
	border: 1px solid #aaa;
	text-decoration: none;
	background-color: #fafafa;
	color: #123456;
	margin: 2px;
	clear:both;
}
#remove
{
	display: block;
	border: 1px solid #aaa;
	text-decoration: none;
	background-color: #fafafa;
	color: #123456;
	margin: 2px;
	clear:both;
}
</style>
<div class="container" style="background-color:white;min-height:425px">
	<div class="row listing-container-row" >
		<#assign notVerified = "icon icon-warning-sign red">
		<#assign verified = "icon icon-large icon-ok green">
		<div class="col-md-3" style="padding-left:10px;padding-top:10px">
			<div class="listing-content-heading-border hidden-xs">
				<div class=""><strong class="text-highlight"> Manage Users Roles</strong></div>
				</div>
				<nav class="navbar navbar-default" role="navigation" style="background: none; border:0">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#4steps">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
     						</button>
     						<strong class="text-highlight visible-xs" style="margin-top: 15px; margin-right: 25px;"> Manage your Account </strong>
     					</div>
     				
     					<div class="collapse navbar-collapse" id="4steps" style="padding-left: 0; padding-right: 0">
     						<ul id="userProfile" class="nav nav-pills nav-stacked" style="padding-top:10px;" >
     							<li class="<#if tab=='changerole'>active</#if>"><a href="#changerole" id="changeroleTab" data-toggle="pill">Grant/Revoke Roles</a></li>		  
     							<li class="<#if tab=='viewusers'>active</#if>"><a href="#viewusers" id="viewuserTab" data-toggle="pill">View Users</a></li>		  
     						</ul>
     					</div>
     				</nav>
    		</div>
    		<div class="col-md-9 listing-content" style="padding:10px">
				<div class="tab-content row">
					<div class="tab-pane <#if tab=='changerole'>active</#if>" id="changerole" style="">
						<div class="col-md-12" >
							<div class="dotted-border-bottom listing-content-heading-border"></div>
								<form action="" method="post">
									<table border="1">
										<tr>
											<td>
												Enter UserName
											</td>
											<td>
												<input type="text" id="username" value="" size="25"/>
												<button class="btn btn-primary" value onclick="loadExisting(); return false;">Show</button>										
											</td>
										</tr>
									</table>
								</form>
							</div>
							<br />
							<br />
							<div id="new" >
								Remaining Roles <br />
								<select multiple id="select1" size=6></select>
							<a href="#" id="add">add &gt;&gt;</a>
						</div>
						<div id="new">
							Current Roles <br />
							<select multiple id="select2" size=6></select>
							<a href="#" id="remove">&lt;&lt; remove</a>
						</div>
						<br />
						<div class="input-group col-sm-6 col-xs-8" >
						<button class="btn btn-primary" id="grantrole" onclick="grantRole(); return false;">Save</button>
					</div> 	
					
					
		  		</div>
		   		<div class="tab-pane <#if tab=='viewusers'>active</#if>" id="viewusers" style="">
					<div class="col-md-12" >
						<div class="dotted-border-bottom listing-content-heading-border"> <strong>List of users in different roles</strong></div>
						</div>
						<div class="form-group" >
							<div class="input-group col-sm-8 col-xs-8">
								<form action="" method="post">
									Select Page:
									<select name="page" id="page">
										<#if pageTypeList?? && pageTypeList?has_content>
											<#list pageTypeList as pageType>
												<option value="${pageType}">${pageType}</option>
											</#list>
										</#if>
									</select>
									<button href="" onclick="viewUsersRole(); return false;" class="btn btn-primary" id="viewUsers">View Users</button>								
								</form>
								<table id="displayUsers" border="1">
								</table>
							</div>    
						</div>
		 			</div>
					</div>
			</div>
		</div>
	</div>
</div>
</@pageAdmin>