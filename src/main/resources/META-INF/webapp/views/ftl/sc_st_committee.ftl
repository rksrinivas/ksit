<@page>
<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<!--<h3>Grievance</h3>-->
					<!--  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote> -->
                    
				<div class="dept-title">
   				<h1>SC / ST Committee</h1>   
				</div>            
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<!--	<h3 class="color_red">GRIEVANCE REDRESSAL COMMITTEE</h3>
	                    	<p>As per the directions of the Registrar, VTU, Belgaum and in accordance
	                    	 with the regulations of AICTE, a <strong>Grievance Redressal Committee</strong> has been established in our 
	                    	 college with the following --->
	                    	 <!--<a href="${img_path!}/pdf/KSIT_Grievance_Cell.pdf" target="_blank">members</a>
	                    	 .</p>-->
	                    <!--	<p>All aggrieved students, their parents & staff may approach the committee for assistance and solve
	                    	 their problems related to academics, resources and personal grievances, ifany.</p> -->
	                    	 
	                    	 	<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name</th>
												<th>Department</th>
												<!--<th>Mobile</th> -->
												<th>Designation</th>
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>1.</td>
												<td>Dr. Dilip Kumar. K</td>
												<td>Principal/Director</td>
												
												<td>Chair Person</td>
											  </tr> 

											  <tr>          
												<td>2.</td>
												<td>Mr. Ranganath. N</td>
												<td>Mechanical Engineering</td>
												
												<td>Coordinator</td>
											  </tr> 

											  <tr>          
												<td>3.</td>
												<td>Dr. R. Senkamalavalli</td>
												<td>Computer Science and Engineering</td>
												
												<td>Member</td>
											  </tr> 

											  <tr>          
												<td>4.</td>
												<td>Mrs. Ammu Bhuvana</td>
												<td>Computer Science and Design</td>
												
												<td>Member</td>
											  </tr> 

											  <tr>          
												<td>5.</td>
												<td>Dr. Rekha. N</td>
												<td>Electronics and Communication Engineering</td>
												
												<td>Member</td>
											  </tr> 

											  <tr>          
												<td>6.</td>
												<td>Mr. Prasad. K</td>
												<td>Mechanical Engineering</td>
												
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>7.</td>
												<td>Mr. Rajesh. P</td>
												<td>Office</td>
												
												<td>Member</td>
											  </tr>  
											  
											   

											  
											
											 </tbody>
											 
													 
											 
											  
											  
											  
								 </table>
	                    	 
						                    
                    </div>
                    
                    
                    
                    
                   
                  </div>
                </div>
                <!-- End single course -->
				
					
				
				 
            </div>
          </div>
          <!-- End course content -->
          
         
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>