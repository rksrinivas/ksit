<@page>
		<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
             
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">

                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an1.JPG" alt="">                           
                        </article>   

						 <article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an2.JPG" alt="">                           
                        </article>   

						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an3.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an4.JPG" alt="">                           
                        </article>
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an8.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an9.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an10.JPG" alt="">                           
                        </article>     
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an11.JPG" alt="">                           
                        </article>   
						 <article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an12.JPG" alt="">                           
                        </article>   
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an13.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an14.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an15.JPG" alt="">                           
                        </article>     
						 <article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an16.JPG" alt="">                           
                        </article>   
						 <article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an16.JPG" alt="">                           
                        </article>   
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an17.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an18.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an19.JPG" alt="">                           
                        </article>     
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an20.JPG" alt="">                           
                        </article>        
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an21.JPG" alt="">                           
                        </article>  
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an22.JPG" alt="">                           
                        </article>     
						<article class="item">
                           <img class="animated slideInLeft" src="${img_path!}/ananya/an23.JPG" alt="">                           
                        </article>         
                     </div>
					 
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
						<li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                        <li data-target="#myCarousel" data-slide-to="6"></li>
						 <li data-target="#myCarousel" data-slide-to="7"></li>
                        <li data-target="#myCarousel" data-slide-to="8"></li>
                        <li data-target="#myCarousel" data-slide-to="9"></li>
						<li data-target="#myCarousel" data-slide-to="10"></li>
                        <li data-target="#myCarousel" data-slide-to="11"></li>
                        <li data-target="#myCarousel" data-slide-to="12"></li>
						<li data-target="#myCarousel" data-slide-to="13"></li>
						<li data-target="#myCarousel" data-slide-to="14"></li>
                        <li data-target="#myCarousel" data-slide-to="15"></li>
                        <li data-target="#myCarousel" data-slide-to="16"></li>
						<li data-target="#myCarousel" data-slide-to="17"></li>
                        <li data-target="#myCarousel" data-slide-to="18"></li>
                        <li data-target="#myCarousel" data-slide-to="19"></li>
						<li data-target="#myCarousel" data-slide-to="20"></li>
                        <li data-target="#myCarousel" data-slide-to="21"></li>
                        <li data-target="#myCarousel" data-slide-to="22"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 
	
 <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
		<div class="row">
			<div class="col-md-12">
			<h1 class="titile text-center">ANANYA</h1>
		<h3 class="welcome-text text-center" style="color:darkblue;"><b>The Ultimate College Fest Experience</b></h3>
				<div class=" col-lg-12 col-md-12 col-sm-12">
					<div class="text-center">
						<p><b>Welcome to Ananya, our annual college fest that promises an unforgettable celebration of creativity, talent, and camaraderie. Ananya, meaning "unique" in Sanskrit, perfectly embodies the spirit of our festival. It is a vibrant platform where students from various disciplines come together to showcase their skills, share ideas, and make lasting memories. </b></p>
						
					</div>
				</div>

				
				
          
			
        </div>
		</div>
		</div>
	</section>
    <!--=========== END ABOUT US SECTION ================--> 
   

    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="newsfeed_area wow fadeInRight"><br/>
           <!-- <ul class="nav nav-tabs feed_tabs" id="myTab2">
              <li class="active"><a href="#news" data-toggle="tab">Ananya</a></li> -->
			  
              <!--<li><a href="#notice" data-toggle="tab">Members</a></li>
              <li><a href="#events" data-toggle="tab">Meets and Events</a></li>  -->
			  
           <!-- </ul> -->

            <!-- Tab panes -->
            <div class="tab-content">
			
			
              <!-- Start news tab content -->
              <div class="tab-pane fade in active" id="news">                
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="${img_path!}/ananya/ananya-24.jpeg" alt="image" />
								</div>
							</div>
						
							<div class="col-lg-6 col-md-6 col-sm-6">
							
							<!--<h3 class="titile text-center ">Ananya<h3>
							<h3 class ="text-center"> The Ultimate College Fest Experience</h3>-->
								<p><b><i>With every year it's a new team <br/>A new family but ever strong roots.. <br/>A pride within itself,<br/>
									An ever fascinating stage of talents here at KSIT!</b></p>
									<!--	<h3 class="welcome-text" style="color:darkblue;"><b>Why Ananya?</b></h3>-->
								<p>Ananya is not just a fest; it's an experience. It's about breaking the monotony of academic life and diving into a world of fun, learning, and creativity. It's where friendships are forged, talents are discovered, and memories are made. Join us for Ananya and be a part of a unique celebration that will leave you inspired and invigorated.</p>
								<p>Mark your calendars and get ready to be a part of Ananya - where every moment is a celebration and every participant is a star. We look forward to welcoming you to an event like no other!</p>
									<p><b>With all the excitement and thrill we bring to You <i>ANANYA 2k24</i> titled as <i>Vaibhava - Intense Celebration</i>! <br/>An event for and by you!</b></i></p>
									
								<p><b>Ananya 2024</b> is set up with a theme <b>"VAIBHAVA"</b>.</p>
								 
								<p>You can get updates and happenings of entire ANANYA on our students made <b>Facebook and instagram page</b>.</p>
								<p>Click here for facebook <a href="https://www.facebook.com/ananyaksit/"  id="ananya_link" target="_blank">&#9656; ANANYA FB </a> </p>
								<p>Click here for Instagram <a href="https://instagram.com/_ksitcultura_?igshid=MzRlODBiNWFlZA=="  id="ananya_link" target="_blank">&#9656; ANANYA Insta </a> </p>
							
								

							</div>

							<div class="col-lg-12 col-md-12 col-sm-12">
							
								<p></p>
								
							</div>
						
						</div>
                    </div>                    
                  </li>
                  
                </ul>                
               
              </div>
			  
			  
              <!-- Start notice tab content -->  
              <div class="tab-pane fade " id="notice">
                <div class="single_notice_pane">
                  <ul class="news_tab">
                    <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<!--<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="img/about/sangham.jpg" alt="image" />
								</div>
							</div>-->
						
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--								
								
								<p><a href="#" target="_blank"></a></p>
								<p><a href="#" target="_blank"></a></p>	
								<p><a href="#" target="_blank"></a></p>-->
								
							<h3>Alumni Members</h3>	
								
								 <table class="table table-striped course_table table_alumni">
							<thead>
							
							  <tr>          
								<th>Designation</th>
								<th>Name</th>
								<th>Email Id</th>
								<th class="mobile_number">Mobile No:</th>
							  </tr>
							  
							</thead>
							
							<tbody>
							
							  <tr>          
								<td>DIRECTOR</td>
								<td>PRINCIPAL</td>
								<td>principal@ksit.edu.in</td>
								<td class="mobile_number">9663778001</td>
							  </tr>
							  
							   <tr>          
								<td>PRESIDENT</td>
								<td>ANIL KUMAR</td>
								<td>anilkumar.ani789@gmail.com</td>
								<td class="mobile_number">8197475168</td>
							  </tr>
							 
							  
							   <tr>          
								<td>VICE-PRESIDENT</td>
								<td>CHAITANYA  C</td>
								<td>cchaitanya3@gmail.com</td>
								<td class="mobile_number">9663592998</td>
							  </tr>
							  
							   <tr>          
								<td>VICE-PRESIDENT</td>
								<td>ASHWIK PRABHU</td>
								<td>ashwik.prabhu@gmail.com</td>
								<td class="mobile_number">7259164891</td>
							  </tr>
							  
							   <tr>          
								<td>INTERNAL SECRETARY</td>
								<td>SWATHI  K</td>
								<td>k.swathi980@gmail.com</td>
								<td class="mobile_number">9742569970</td>
							  </tr>
							  
							   <tr>          
								<td>EXTERNAL SECRETARY</td>
								<td>G  NITIN</td>
								<td>nithin369ruls@gmail.com</td>
								<td class="mobile_number">8147707717</td>
							  </tr>
							  
							   <tr>          
								<td>TREASURER</td>
								<td>SUBRAMANYA  K S</td>
								<td>Subrahmanyaks.91@gmail.com</td>
								<td class="mobile_number">9886345054</td>
							  </tr>
							  
							  
							</tbody>
						  </table>
								
							<p><a href="${img_path!}/alumni/office bearers list.pdf" target="_blank">view more</a></p>
								
							</div>
						
						</div>
                    </div>                    
                  </li>                         
                  </ul>
                  
                </div>               
              </div>
			  
			  
              <!-- Start events tab content -->
              <div class="tab-pane fade " id="events">
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<!--<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="img/about/sangham.jpg" alt="image" />
								</div>
							</div>-->
						
							<div class="col-lg-12 col-md-12 col-sm-12">
								
								<p>The Guest Lecture on Opportunities for Engineers <a href="${img_path!}/alumni/Report2_alumni.doc" target="_blank"> Click here</a></p>
								
								<p>Technical Talk by Alumni <a href="${img_path!}/alumni/Technical Talk by Alumni.doc" target="_blank">click here </a></p>
								
								<p>Technical Talk report by Alumni Abhiroop april 30 2016 <a href="${img_path!}/alumni/Technical Talk report by Alumni Abhiroop april302016.doc" target="_blank"> click here </a></p>
								
								
							</div>
						
						</div>
                    </div>                    
                  </li>      
                </ul>
                
              </div>
			  
			  
			  
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 
   
   
	
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive" style="margin-top:0%;">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3 class="text-center" style="color:red;">Glimpses of ANANYA</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Entire journey of Ananya has been captured and showcased here.. </p>
										  				  
                    </blockquote>
                    
							
                 <section id="gallery">
				      <div class="container">
				        <div class="row">
				          <div class="col-lg-12 col-md-12 col-sm-12">
				            <div id="gallerySLide" class="gallery_area">
				            
								 <#list ananyaGalleryList as ananyaGallery>
								
									<a href="${ananyaGallery.imageURL!}" title="">
					                    <img class="gallery_img" src="${ananyaGallery.imageURL!}" alt="img" />
					                	<span class="view_btn">View</span>
					                </a>
								
								</#list>
								</div>
							</div>
							</div>
							</div>
					</section>
				
	
				
				
				
				
			

			</div>
		 </div>
		</div>
	</div>
  </div>
</section>  
				
</@page>