<@page>
<link href="${css_path!}/onlineadmissions.css" rel="stylesheet">
<link href="${css_path!}/stepper_form.css" type="text/css" rel="stylesheet">
    
<div class="container" style="margin-top: 3px;margin-bottom: 30px;">
   <div class="row container-fluid">
     <div class="col-12">
     	<form id="oaForm" action="/onlineadmissions" method="post" enctype="multipart/form-data">
     		
     		<div style="text-align:center;margin-top:40px;">
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			  <span class="step"></span>
			</div>
			<div class="tab">
				<div class="form-group">
					<label>Branch to which candidate is admitted:</label>
					<select class="wp-form-control" name="admittedBranch">
		              <option value="Artificial Intelligence and machine learning Engineering">Artificial Intelligence and machine learning Engineering</option>
		              <option value="Computer Science and Design">Computer Science and Design</option>		              
		              <option value="Computer science Engineering">Computer science Engineering</option>
		              <option value="Electronics and Communication Engineering">Electronics and Communication Engineering</option>
		              <option value="Mechanical Engineering">Mechanical Engineering</option>
		            </select>
	          	</div>
	          	<div class="form-group">
					<label>CET Rank:</label>
	          		<input type="text" class="wp-form-control" name="cetRank"/>
	          	</div>
	          	<div class="form-group">
					<label>Upload your recent passport size photograph:</label>
	          		<input type="file" class="form-control-file" name="photo"/>
	          	</div>
				<div class="form-group">
					<label>Name of the Candidate:</label>
	          		<input type="text" class="wp-form-control" name="candidateName"/>
	          	</div>
	          	<div class="form-group">
					<label>Date of Birth:</label>
	          		<input type="date" class="wp-form-control" name="dob"/>
	          	</div>
	          	<div class="form-group">
					<label>Gender:</label>
					<select class="wp-form-control" name="gender">
		              <option value="male">Male</option>
		              <option value="female">Female</option>
		              <option value="others">Others</option>
		            </select>
	          	</div>
	          	<div class="form-group">
					<label>Caste:</label>
	          		<input type="text" name="caste"/>
	          	</div>
	          	<div class="form-group">
					<label>Category:</label>
	          		<input type="text" name="category"/>
	          	</div>
	          	<div class="form-group">
					<label>Religion:</label>
	          		<input type="text" name="religion"/>
	          	</div>
	          	<div class="form-group">
					<label>Nationality:</label>
	          		<input type="text" name="nationality"/>
	          	</div>
	          	<div class="form-group">
					<label>Mobile Number:</label>
	          		<input type="text" class="wp-form-control" name="mobileNo"/>
	          	</div>
	          	<div class="form-group">
					<label>Email ID:</label>
	          		<input type="email" class="wp-form-control" name="email"/>
	          	</div>
			</div>
			<div class="tab">
				<div class="form-group">
					<label>Father's Name:</label>
	          		<input type="text" class="wp-form-control" name="fathersName"/>
	          	</div>
	          	<div class="form-group">
					<label>Occupation:</label>
	          		<input type="text" class="wp-form-control" name="fathersOccupation"/>
	          	</div>
	          	<div class="form-group">
					<label>Annual Income:</label>
	          		<input type="text" class="wp-form-control" name="fathersAnnualIncome"/>
	          	</div>
	          	<div class="form-group">
					<label>Phone Number:</label>
	          		<input type="text" class="wp-form-control" name="fathersMobileNo"/>
	          	</div>
	          	<div class="form-group">
					<label>Email ID:</label>
	          		<input type="email" class="wp-form-control" name="fathersEmail"/>
	          	</div>
			</div>
			
			<div class="tab">
				<div class="form-group">
					<label>Mothers's Name:</label>
	          		<input type="text" class="wp-form-control" name="mothersName"/>
	          	</div>
	          	<div class="form-group">
					<label>Occupation:</label>
	          		<input type="text" class="wp-form-control" name="mothersOccupation"/>
	          	</div>
	          	<div class="form-group">
					<label>Annual Income:</label>
	          		<input type="text" class="wp-form-control" name="mothersAnnualIncome"/>
	          	</div>
	          	<div class="form-group">
					<label>Phone Number:</label>
	          		<input type="text" class="wp-form-control" name="mothersMobileNo"/>
	          	</div>
	          	<div class="form-group">
					<label>Email ID:</label>
	          		<input type="email" class="wp-form-control" name="mothersEmail"/>
	          	</div>
			</div>
			
			<div class="tab">
				<div class="form-group">
					<label>Name of the Local Guardian:</label>
	          		<input type="text" name="guardiansName"/>
	          	</div>
	          	<div class="form-group">
					<label>Phone Number:</label>
	          		<input type="text" name="guardiansMobileNo"/>
	          	</div>
			</div>
			
			<div class="tab">
				<div class="form-group">
					<label>Address for Correspondance:</label>
	          		<textarea class="wp-form-control" name="correspondenceAddress" rows="4" cols="50"></textarea>
	          	</div>
	          	<div class="form-group">
					<label>Pin Code:</label>
	          		<input type="number" class="wp-form-control" name="correspondencePinCode"/>
	          	</div>
	          	<hr/>
	          	<div class="form-group">
					<label>Permanent Address:</label>
	          		<textarea class="wp-form-control" name="permanentAddress" rows="4" cols="50"></textarea>
	          	</div>
	          	<div class="form-group">
					<label>Pin Code:</label>
	          		<input type="number" class="wp-form-control" name="permanentPinCode"/>
	          	</div>
	          	<hr/>
	          	<div class="form-group">
					<label>Details of Previous Institution attended:</label>
	          		<input type="text" class="wp-form-control" name="prevInstitution"/>
	          	</div>
	          	<div class="form-group">
					<label>Board:</label>
	          		<input type="text" class="wp-form-control" name="prevInstitutionBoard"/>
	          	</div>
	          	<div class="form-group">
					<label>Date of Leaving:</label>
	          		<input type="date" class="wp-form-control" name="prevDateOfLeaving"/>
	          	</div>
	          	<div class="form-group">
					<label>Register Number of Equivalent Exam:</label>
	          		<input type="text" class="wp-form-control" name="prevRegisterNo"/>
	          	</div>
	          	<div class="form-group">
					<label>Year of Passing:</label>
	          		<input type="number" class="wp-form-control" name="prevPassingYear">
	          	</div>
			</div>
			
			<div class="tab">
				<h6>
					Marks secured in PCM / XII / +2 / PUC Equivalent:
				</h6>
				<table class="table">
					<tr>
						<th>Subjects</th>
						<th>Maximum Marks</th>
						<th>Marks Obtained</th>
						<th>Percentage</th>
					</tr>
					<tr>
						<td>Physics</td>
						<td>
							<input type="number" class="wp-form-control" name="physicsMax" />
						</td>
						<td>
							<input type="number" class="wp-form-control" name="physicsMarks" />
						</td>
					</tr>
					<tr>
						<td>Mathematics</td>
						<td>
							<input type="number" class="wp-form-control" name="mathsMax" />
						</td>
						<td>
							<input type="number" class="form-control" name="mathsMarks" />
						</td>
					</tr>
					<tr>
						<td>Chemistry/Bio./Elct./Comp. etc</td>
						<td>
							<input type="number" class="wp-form-control" name="otherMax" />
						</td>
						<td>
							<input type="number" class="wp-form-control" name="otherMarks" />
						</td>
					</tr>
					<tr>
						<th>Total</th>
						<td>
							<input type="number" class="wp-form-control" name="totalMax" />
						</td>
						<td>
							<input type="number" class="wp-form-control" name="totalMarks" />
						</td>
						<td>
							<input type="number" step="0.01" class="wp-form-control" name="percentage" />
						</td>
					</tr>
				</table>
			</div>
			<div class="tab">
				<h6>
					Details of Entrance Test Appeared:
				</h6>
				<table class="table">
					<tr>
						<th>Test</th>
						<th>Year</th>
						<th>Registration Number</th>
						<th>Rank</th>
					</tr>
					<tr>
						<td>KEA / CET</td>
						<td>
							<input type="number" class="form-control" name="keaYear" />
						</td>
						<td>
							<input type="text" class="wp-form-control" name="keaRegNo" />
						</td>
						<td>
							<input type="number" class="wp-form-control" name="keaRank" />
						</td>
					</tr>
					<tr>
						<td>COMED-K</td>
						<td>
							<input type="text" class="form-control" name="comedYear" />
						</td>
						<td>
							<input type="text" class="form-control" name="comedRegNo" />
						</td>
						<td>
							<input type="text" class="form-control" name="comedRank" />
						</td>
					</tr>
					<tr>
						<td>JEE</td>
						<td>
							<input type="text" class="form-control" name="jeeYear" />
						</td>
						<td>
							<input type="text" class="form-control" name="jeeRegNo" />
						</td>
						<td>
							<input type="text" class="form-control" name="jeeRank" />
						</td>
					</tr>
					<tr>
						<td>Others</td>
						<td>
							<input type="text" class="form-control" name="othersYear" />
						</td>
						<td>
							<input type="text" class="form-control" name="othersRegNo" />
						</td>
						<td>
							<input type="text" class="form-control" name="othersRank" />
						</td>
					</tr>
				</table>
				<div class="form-group">
					<label>Sports & Extra Curricular Activites:</label>
	          		<input type="text" name="extraActivites"/>
	          	</div>
			</div>
			<div class="tab">
				<h6>Details of certificates / enclosures to be enclosed (originals & two sets of photocopies)</h6>
				<table class="table">
					<tr>
						<td>SSLC / X Standard Marks Card</td>
						<td><input type="file" class="form-control-file" name="sslcMarksCard" /></td>
					</tr>
					<tr>
						<td>PUC / XII Standard Marks Card</td>
						<td><input type="file" class="form-control-file" name="pucMarksCard" /></td>
					</tr>
					<tr>
						<td>CET / COMED-K / JEE/ UGET Rank</td>
						<td><input type="file" class="form-control-file" name="rankCertificate" /></td>
					</tr>
					<tr>
						<td>Transfer Certificate</td>
						<td><input type="file" name="transferCertificate" /></td>
					</tr>
					<tr>
						<td>Migration Certificate</td>
						<td><input type="file" name="migrationCertificate" /></td>
					</tr>
					<tr>
						<td>Physical Fitness Certificate from Registered Medical Practitioner</td>
						<td><input type="file" name="physicalFitnessCertificate" /></td>
					</tr>
					<tr>
						<td>Student VISA, Clearance from Ministry of HRD, AIU Equivalance, Passport & attested copy in case of foreign students only</td>
						<td><input type="file" name="foreignStudentsCertificate" /></td>
					</tr>	
				</table>
			</div>
			<div class="tab">
				<h6>
					Payment Details
				</h6>
				<span>
					K.S INSTITUTE OF TECHNOLOGY, BANGALORE.
					<br />
					AXIS BANK, JP NAGAR BRANCH, BANGALORE.
					<br />
					SB A/C NO:  912010014093916
					<br />
					IFS CODE NO: UTIB0001513
					<br />
				</span>
				<table class="table">
					<tr>
						<td>Transaction No</td>
						<td><input type="text" class="form-control" name="transactionNo" /></td>
					</tr>
					<tr>
						<td>Upload Screenshot of transaction</td>
						<td><input type="file" class="form-control" name="screenshot" /></td>
					</tr>
				</table>
			</div>
			<div style="overflow:auto;">
			  <div style="float:right;">
			    <button type="button" class="btn btn-primary btn-lg" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
			    <button type="button" class="btn btn-primary btn-lg" id="nextBtn" onclick="nextPrev(1)">Next</button>
			  </div>
			</div>
		</form>
      </div>
   </div>
  </div>
</div>
<script src="${js_path!}/onlineadmissions${timestamp!}.js"></script>
</@page>