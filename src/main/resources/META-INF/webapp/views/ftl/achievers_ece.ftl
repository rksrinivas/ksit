<@page>	
		
	
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <!-- start blog archive  -->
              <div class="row">
              
               <!-- start single blog archive -->
              	 
              	 <#list eceDepartmentAchieversList as eceDepartmentAchievers>
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${eceDepartmentAchievers.imageURL!}">
                      </a>
                    </div>
                    <h2 class="blog_title">${eceDepartmentAchievers.heading!}</h2>
					
                    <div class="blog_commentbox">
                      <p>${eceDepartmentAchievers.eventDate!}</p>
                                            
                    </div>
					
                    <p class="blog_summary">${eceDepartmentAchievers.content!}</p>
					
					<!--<a class="blog_readmore" href="#">Read More</a>-->
                    
                  </div>
                </div>
                
                </#list>
                <!-- start single blog archive -->
			  
			  
			    <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/ece/devika.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">P Devika </h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					P Devika of 7th semester has won 2 State Awards from Karnataka Govt for,1st place in national level competition �Gopinath Das Nyasa�
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
			  
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/ece/akash.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Aakash P S </h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
						Aakash P S of the 7th  semester secured Gold medal in the National Martial Art Extravaganza held at Sree Kanteerava Indoor Stadium, Bengaluru. 
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
              
				
				
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/Achievers/ece/smartbin.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Smart Bin</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					Harshit R, Nagabhushana Bhat and Abhishek S Gowda of 5th semester have secured 1st place for the project �Smart Bin� in the mini project exhibition conducted by IEEE KSIT Student Branch on 4th of October. 
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				 <!-- start single blog archive -->
               <!-- <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="${img_path!}/blog.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Name</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					
					</p>
                    
                  </div>
                </div>-->
                <!-- start single blog archive -->
				
				
				
				
				
				
              </div>
              <!-- end blog archive  -->
             
            </div>
          </div>
          <!-- End course content -->

         
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>