<@page>
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
             
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/canteen/c1.jpg" alt="">                           
                        </article>                     
                     </div>
                     <!-- Indicators -->
                     <!--<ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                     </ol>-->
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 



    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about">
	<div class="container">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
          <div class="aboutus_area wow fadeInLeft">
			
    				<p>KSIT College Cafeteria has a charming ambience and spacious area with innovative dining solutions that provides a wide range of Vegetarian hygienic food. The topographical food and beverages is taken care by the canteen. There are huge variety of food items including Chinese cuisine to South Indian breakfast and meals at affordable prices for students, faculty, staff, and visitors to the college.</p>
					
					
					<p>College canteen is not just a place for students to grab a bite, but it's also a place to hang out where they get all the entertainment to beat their blues away and so it is a popular hub on the campus both for physical nourishment and for student bonding.</p>
					
					
					<p>Our Canteen, an important part of college life, It tends to be the most popular place on campus , not only for physical refreshment but also for students chatting , gossiping, discussion and even work on assignments and projects.</p>
					
					<p>The  Canteen  is a small building with a seating capacity of  hundred members have  separate area for the  staff and students. A  variety of refreshments and meals are available here.</p>
					
                      
					<p>By promoting better student health the College hopes to inspire our students to be healthy and happy about themselves. In keeping with the College's Healthy Foods Policy, the canteen provides healthy food options for the wide range of diverse cultures represented at College.</p>
					
				<p>By promoting better student health the College hopes to inspire our students to be healthy and happy about themselves. In keeping with the College's Healthy Foods Policy, the canteen provides healthy food options for the wide range of diverse cultures represented at College.</p>
					
					
					<ul class="list">
					
						<li>Clean, Healthy and Hygienic food</li>
						
						<li>Separate Dining area for Students and Faculty</li>
						
						<li>Wide Variety of food menu ranging from Chinese to south Indian.</li>
						
						<li>Well maintained and advanced kitchen </li>
						
						<li>Well trained canteen staff</li>
					
					</ul>	
					
			
		  </div>
        </div>
		
		</div>
		</div>
	</section>


</@page>    