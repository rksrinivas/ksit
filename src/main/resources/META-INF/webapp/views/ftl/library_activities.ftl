<@page>	
	<!-- =========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <!-- start blog archive  -->
              <div class="row">
              <h2 class="blog_title">LIBRARY ACTIVITIES</h2>
              
              	 <!-- start single blog archive -->
              	 
              <#list libraryActivityList as libraryActivity>
                <div class="col-lg-4 col-md-6 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="${libraryActivity.imageURL!}" target="_blank" class="blog_img">
                        <img alt="img" src="${libraryActivity.imageURL!}">
                      </a>
                    </div>
					
                    <div class="blog_commentbox">
                      
                      <h2>${libraryActivity.heading!}</h2>
                                            
                    </div>
					
                    <p class="blog_summary">${libraryActivity.content!}</p>					
				                    
                  </div>
                </div>
                
               </#list>
                <!-- start single blog archive -->
              
			 				
				
				
				
              </div>
              <!-- end blog archive  -->
             
            </div>
          </div>
          <!-- End course content -->

         
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>