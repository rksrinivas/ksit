		 <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <!--<section id="imgBanner_cse">
      <h2></h2>
    </section>-->
    <!--=========== END COURSE BANNER SECTION ================-->
	
	
	 <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
         
		<h2 class="titile">Sports</h2>
          <!-- start course archive sidebar -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_sidebar">
			
              <!-- start single sidebar -->
              <div class="single_sidebar">
			  
                <h2> VTU Assignments<span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
				
                   <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4></h4>
					   <p>* 	Manager for VTU Net Ball Team which participated in the All India Inter-University Net Ball Tournament-2012. Held at Guru Ghasidas University, Bilaspur, (CG).    </p>
					   <p>* 	Coach for VTU Hand Ball Team which participated in South zone Inter-University Hand ball Tournament 2013, held at Periyar University, Salem (TN). </p>
					   <p>* 	Selection committee member for VTU Bangalore North Zone   Cricket Selection trails. Selection will be on 10th & 11th September 2013 at REVA ITM, Bangalore.</p>
					   <p>* 	Coach for VTU Kabaddi  Team which participated in South zone Inter-University Kabaddi Tournament 2013, held at Acharya Nagarjuna University, Guntur  (AP) From 4th to 8th December 2013.</p>
					   <p>* 	Selection committee member for VTU Bangalore North Zone   Cricket Selection trails. Selection will be on 22nd & 23rd September 2014 at REVA ITM, Bangalore.</p>
					   <p>* 	Coach for VTU Net Ball Team (Women) which participated in the All India Inter-University Net Ball Tournament-2015. Held at Annamalai University, Chidambaram (TN) From 19th February 2015 to 24th February 2015</p>
					   <p>* 	Selection committee member for VTU Handball (Men & Women) Team Selection trails. Selection will be on 23rd to 25th March 2015 at GAT, Bangalore.</p>
					   <p>* 	Selection committee member for VTU Kabaddi (Men & Women) Team Selection trails. Selection will be on 27rd to 29th April 2015 at APSCE, Bangalore.</p>
					   <p>* 	Selection committee member for VTU Basket Ball (Men) Team Selection trails. Selection will be on 14th to 16th May 2015 at BLDECE, Vijayapura.</p>
					   <p>* 	Selection committee member for VTU Bengaluru South Zone Cricket Team Selection trails. Selection will be on 28th to 29th September 2015 at SJBIT, Bangalore.</p>
					   <p>* Selection committee member for VTU Archery (Men & Women) Team Selection trails. Selection will be on 26th October 2015 at Acharya IT, Bangalore.</p>
					   <p>* Selection committee member for VTU Kho-Kho(Men ) Team Selection trails. Selection will be on 11th  to 13th March 2016 at NCET, Devanahalli.</p>
					   <p>* Selection committee member for VTU Handball (Men & Women) Team Selection trails. Selection will be on 18th  to 20th March 2016 at JNNCE, Shivamoga.</p>
					   
                       
                      </div>
                    </div>
                  </li>  
				  
				   
				                                
                </ul>
              </div>
              <!-- End single sidebar -->
			  
			 
			  
			  
			 
             
			  
            </div>
          </div>
          <!-- start course archive sidebar -->
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->