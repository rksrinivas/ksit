<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
                        <h3 class="text-center">Placement Events</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                    </blockquote>

				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                
                 <#list placementEventsList as placementEvent>
                	
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">
						
							"${placementEvent.heading!}"
						
						</h2>
					
						<p>"${placementEvent.content!}"</p>
						
						<img class="placement_images" src="${placementEvent.imageURL!}" alt="image" style="height:200px;width:200px;" />
						
						<!--<a href="${placementEvent.imageURL!}" target="_blank">click here to download</a>-->
						
						<p>"${placementEvent.eventDate!}"</p>
					
                    </div>
                   
                  </div>
                  
                  </#list>
                  
                </div>
                <!-- End single course -->
				
				
              
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                
                	
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Campus Induction Training Program</h2>
					
						<p>Seating arrangements of campus Induction Training Program 1st Semester</p>
						<a href="img\placements\placement_events\administration-news-New Doc 2017-08-06.pdf" target="_blank">Click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Personality Development Program</h2>
					
						<p>All 7th Semester students must attend the Personality Development Program from 1st Aug 2017 without fail @ KSIT, Principal, KSIT</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">DATRI Blood Stem Cell Donors Registry Camp on 15th Mar 2017</h2>
					
						<p>DATRI Blood Stem Cell Donors Registry Camp on 15th Mar 2017 @KSIT Bangalore</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulations for the students got placed in Arteck Infoteck and Qspiders</h2>
					
						<p>Heartiest Congratulations for the students got placed in Arteck Infoteck and Qspiders campus drive held at KSIT Bangalore for 2017 batch</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">1</h2>
					
						<p>Heartiest congratulations for the students for all 8 students got placed in Arteck Infoteck and 11 got placed in Qsiders campus drive held at KSIT Bangalore</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Placement training for 6th sem, 4th sem, 2nd sem students </h2>
					
						<p>Placement Department has planned to conduct placement training for 6th sem students from 6.2.2017 to 9.2.2017, 4th sem students from 13.2.2017 to 16.2.2017 and 2nd sem students from 20.2.2017 to 23.2.2017. Mandatory to attend the sessions.</p>
						<a href="img\placements\placement_events\administration-news-New Doc 1.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Personality Devlopment Program for 6th Sem</h2>
					
						<p>Personality Devlopment Program for 6th Sem is commencing from 6th Feb 2017 to 9th Feb 2017. It is mandatory and compulsory to attend the program.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Global automation campus Recruitment drive on 31st Jan 2017</h2>
					
						<p>Global automation Campus Recruitment Drive for 2017 batch students on 31/1/2017</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Congratualtions for all 8 students got placed in Amazon 2017 batch</h2>
					
						<p>Congratualtions for all 8 students got placed in Amazon 2017 batch</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Pre-Assessment test of cocoubes for 7th sem </h2>
					
						<p>Placement department has planned to conduct pre-Assessment test of cocoubes for 7th sem students on 10th, 13th, 14th, 16th September 2016. It is mandatory to attend. For more details please find the attachment or contact placement cell.</p>
						<a href="img\placements\placement_events\administration-news-7th sem cocubes test.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Personality development program for 5th sem 'LIFESKILL @ KSIT'</h2>
					
						<p>Placement department has planned to conduct personality development program "LIFESKILL @ KSIT" for 5th semester students from 14th September 2016 to 18th September 2016. It is mandatory to attend the program.</p>
						<a href="img\placements\placement_events\administration-news-personality development program 5th sem.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">KATALYST Presents a unique program that supports meritorious girls of I Year</h2>
						
						<a href="img\placements\placement_events\administration-news-placement1.jpg" target="_blank">Click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Virtual Recruitment Drive</h2>
					
						<p>Placement Department has planned to conduct "Virtual Recruitment Drive" for Final Year students from 27th August 2016 to 28th August 2016. For more information Contact placement cell.</p>
						<a href="img\placements\placement_events\administration-news-New_Doc_19_2.PDF" target="_blank">Click here to download</a>
														
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">LIFESKILL @ KSIT for 5th and 3rd sem students</h2>
					
						<p>Placement department has planned to conduct personality development program "LIFESKILL @ KSIT" for 5th semester students from 9th September 2016 to 14th September 2016 and for 3rd sem students from 16th September 2016 to 21st September 2016 . It is mandatory to attend the program.</p>
						<a href="img\placements\placement_events\administration-news-New_Doc_19_1.PDF" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">M/s. Amagi Media Labs Recruitment Pool Campus Drive</h2>
					
						<p>M/s. Amagi Media Labs Recruitment Pool Campus Drive is on 15/2/2016 for 2016 batch students.</p>
						<a href="img\placements\placement_events\administration-news-02-11_07-27-15.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Hp Enterprize drive on 1st Feb 2016 and 2nd Feb 2016</h2>
					
						<p>Hp Enterprize drive on 1st Feb 2016 and 2nd Feb 2016 for 2016 batch students</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Cease Fire Campus Recruitment Drive on 4th Feb 2016</h2>
					
						<p>Cease Fire Campus Recruitment Drive for Mech Students on 4th Feb 2016</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Cognizant Campus Recruitment Drive on 3rd Feb 2016 and 4th Feb 2016 </h2>
					
						<p>Cognizant Campus Recruitment Drive is on 3rd Feb 2016 and 4th Feb 2016 for 2016 batch students</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">1</h2>
					
						<p>Cognizant Campus Drive is on 3rd and 4th Feb 2016 for 2016 batch students</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Placement Training for 4th sem students</h2>
					
						<p>Personality Development Training Program Life skill @KSIT for 4th sem students will commence from 2nd Feb 2016 to 9th Feb 2016.It is mandatory to attend the same.</p>
						<a href="img\placements\placement_events\administration-news-Placement.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Personality Development Program "LifeSkill@KSIT"</h2>
					
						<p>Personality Development Training Program Life Skill at KSIT for 4sem and 6th sem students from 1st Feb 2016 to 6th Feb 2016 and 8th Feb 2016 to 15th Feb 2016 Respectively . For more details refer the circular attached and contact placement cell.</p>
						<a href="img\placements\placement_events\administration-news-20151222_085640.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical training on C,C++,OOPS,Core JAVA AND UPCOMING TRENDS IN tECHNOLOGIES for 8th sem students </h2>
					
						<p>Placement department had planned to conduct technical training on above mentioned topics for 8th semester students from 10th Jan 2016 to 20th Jan 2016. Please find the attachment for more details or contact placement department.</p>
						<a href="img\placements\placement_events\administration-news-20151218_121231.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop on Industrial Automation for 4th and 6th sem students from 10th Jan 2016 to 20th Jan 2016 </h2>
					
						<p>Placement department is planning to conduct workshop on Industrial Automation for 4th and 6th sem students from 10th Jan 2016 to 16th Jan 2016. For more details refer the circular attach and contact placement cell.</p>
						
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical training on Core Java from 10th Jan 2016 to 20th Jan 2016</h2>
					
						<p>Placement department had planned to conduct the Technical Training on Core Java course in association with Q-spiders from 10th Jan 2016 to 20th Jan 2016. For more details refer the circular attach and contact placement cell.</p>
						<a href="img\placements\placement_events\administration-news-TECH Training.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">HP Campus drive on 19th Nov 2015 for 2016 batch</h2>
					
						<p>HP Campus Drive on 19th /11/2015 and 20th Nov 2015 for our 2016 batch students.All the Best Student</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Capgemini Campus Interview and HP Campus Interview </h2>
					
						<p>Capgemini Final Round of selection on 18/11/2015 for 2016 batch student.All the Best students</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulations</h2>
					
						<p>Heartiest Congratulations Students for getting placed in Infosys from 2016 Batch</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Cocubes Carrier Diagnostic Test</h2>
					
						<p>CoCubes Carrier Diagnostic Test is schedule on 11/8/2015,12/8/2015 and 13/8/2015 for 7th sem and Pre Assess Test I is on 22nd Aug 2015. It is mandatory to attend the progranm</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Training Program details are as follows</h2>
					
						<p>Dear Students Training Program details are as follows- 1. Placement Department had organize Personality Development Training Program from 27th July 2015 to 1st Aug 2015 for all final year students of all branch.It is Mandatory and Compulsory for every one to attend the same.Student need not to pay any fee for this. 2.We are planning to organize Technical Training Program from 2nd July 2015 to 12th July 2015 for all final year students of all branch. For this purpose interested students had to pay Rs.3100/- to the office and register for the same on or before 10th June 2015 to their respected departments. This technical program will be delivered from technical experts of Industry like CISCO, DELL, Schnieder Electricals, Oracle, TCS, Pearson etc which will help you to prepare for technical interview and placments. Also you will get the certificate after completing the course. For more details refer the circular attach and contact placement cell.</p>
						<a href="img\placements\placement_events\administration-news-training.rar" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Personality Development and Technical Training Program</h2>
					
						<p>Personality Development Training Program for all semester students is organized. Dates are as follows- 1. 27th July 2015 to 1st Aug 2015 for 7th sem 2015 2. 3rd Aug 2015 to 8th Aug 2015 for 5th sem 2015. 3.17th Aug 2015 to 22nd Aug 2015 for 3th sem. Technical Training for 7th sem 2015 on 2nd July 2015 to 12th July 2015</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IITJEE EXAM on 10th and 11th April 2015</h2>
					
						<p>IITJEE EXAM Online Entrance Exam on 10th and 11th April 2015.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulations for the student got placed in EQUIFAX and MICROLAND CAMPUS DRIVE</h2>
					
						<p>Heartiest Congratulations for the 3 students got placed in EQUIFAX and 4 students in MICROLAND CAMPUS DRIVE</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulations</h2>
					
						<p>Heartiest Congratulations for the students got placed in Equifax from KSIT on the campus drive held on 17/3/2105 from CS deptt.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulations for student placed in various companies </h2>
					
						<p>Heartiest Congratulations for Student got placed in HP, Sonata Software, Mercedez Benz,Mphasis from 2015 batch</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">campus drive of HIve minds Innovative Market Solutions</h2>
					
						<p>HiveMinds Innovative Market Solutions Pvt. Ltd drive on 28th and 29th Jan 2015 for 2015 batch students</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Pool Campus Drive for 2015 Batch</h2>
					
						<p>Vtiger Pool Campus Recruitment Drive on 22/1/2015 ,Nokia Network Siemens Pool campus Drive on 24/1/2015, Artech Infotech Pool Campus Drive on 27/1/2015 and HP pool Campus Drive on 3/2/2105 for 2015 batch students</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GATE 2015 Exam</h2>
					
						<p>GATE 2015 Online examination on 31/1/2015, 1/2/2015 and 7/2/2015 in association with IISC Bangalore and TCS ION.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">College will reopen on 21st Jan 2015 </h2>
					
						<p>College will reopen on 21st Jan 2015 for all semester students.Personality Development Training Program is commencing from 27/1/2015 to 2/2/2015 for 6th sem,2/2/15 to 7/2/15 for 4th Sem and 9/2/15 to 14/2/15 for II sem.It is mandatory for each and every students to attend the program.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Personality Development Program</h2>
					
						<p>Personality development Training Program for IV Sem on 27/1/2015 sem to 2/2/2015, for IV sem on 2/2/2015 to 7/2/14 and for II sem on 9/2/2015 to 14/2/14</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heatiest Congratulations for the students placed in L and T Infotech </h2>
					
						<p>Heartiest Congratulations for the all the Students Placed in L and T Infotech and IBM and Razor Think</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Congratulations for all students Placed in Infosys,Techmahindra and Nttdata</h2>
					
						<p>Heartiest Congratulations for all students placed in Infosys,Techmahindra and Nttdata,Mphasis and Cegedam</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Aspiring Mind Pre Assess Test AMCAT</h2>
					
						<p>Aspiring Minds Pre Assess Test on 2Oth Sept 2014</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">COCUBES DIAGNOSTIC TEST </h2>
					
						<p>If you have a great idea,were here to listen. Compete with the best and brightest minds from across the country in Indias biggest online technical project contest. For more information, <br/><a href="http://www.app3.unisys.com/common/about_unisys/Cloud20_20V6/index.html Cloud 20/20 Version 6.0 A Clear Vision for the Cloud" target="_blank">Please visit us at:</a> <u>http://www.app3.unisys.com/common/about_unisys/Cloud20_20V6/index.html Cloud 20/20 Version 6.0 A Clear Vision for the Cloud</u></p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Aptitude and Soft skill training for V sem students </h2>
					
						<p>scheduled from 22nd July 2014 to 28th July 2014.</p>
						<a href="img\placements\placement_events\administration-news-soft skill traning.jpg" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Flipkart Pool Campus Recruitment Drive</h2>
					
						<p>KSIT Heartily Welcomes Students for Pool Campus Recruitment Drive of Flipkart for 2014 Batch students.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">COGNIZANT OFF CAMPUS RECRUITMENT DRIVE</h2>
					
						<p>FOR 2012 AND 2013 BATCH THROUGH VTU IS AT K.S.INSTITUTE OF TECHNOLOGY -BANGALORE ON 3RD APRIL 2014.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulations- 11 students got placed in Cognizant Technology Drive of 2014 batch </h2>
					
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">CBSE- JEE (AIEEE Exams) On 9th April 2014, 11th April 2014, 12th& 19th April 2014</h2>
					
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NIMHANS PG Entrance Exam on 30th March 2014</h2>
					
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">personality development training through Quantech </h2>
					
						<p>Find the enclosed attachment of procedure for using TRACK POSITIONS. Detailed procedure is enclosed for further details contact Placement Cell.</p>
						<a href="img\placements\placement_events\administration-news-Track positions.pptx" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulation</h2>
					
						<p>24 students got placed in Infosys Campus Recruitment Drive at K.S.Institute of Technology,Bangalore on 18th March 2014 and 19th March 2014. from 2014 batch. Health Asyst Campus Recruitment Drive is on 22nd March 2014 for CS and EC students of 2014 batch Aryaka Networks Campus Recruitment Drive on 21st March 2014 for our final year students.</p>
						<a href="img\placements\placement_events\administration-news-Final_Selects_KSIT.pdf" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Heartiest Congratulations</h2>
					
						<p>Heartiest Congratulations- Our eleven students got placed in Cognizant Technology Drive from 2014 batch</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">JEE (AIEEE Exams)</h2>
					
						<p>On 9th April 2014, 11th April 2014, 12th& 19th April 2014 CBSE- JEE (AIEEE Exams)</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NIMHANS PG Entrance Exam</h2>
					
						<p>on 30th March 2014</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Recruitment Drive of TATA CONSULTANCY SERVICES & INFOSYS</h2>
					
						<p>K.S.Institute of Technology Heartily Welcomes Aspiring Students for Off Campus Recruitment Drive of TATA CONSULTANCY SERVICES on 8th March 2014 and 9th March 2014 for 2014 batch students...K.S.Institute of Technology Heartily Welcomes Aspiring Students for On Campus Recruitment Drive of on 18th March 2014 and 19th March 2014 for 2014 Batch students.</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Kirloskar Toyota Textile Machinery Pvt.Ltd. Bangalore</h2>
					
						<p>Kirloskar Toyota Textile Machinery Pvt.Ltd. Bangalore. Final round of Interview is on 10/3/2014, 11/3/2014 and 12/3/2014 for our shortlisted students of Final Year students</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Placement Details of 2013,2012,2011 Batch</h2>
					
						<p>Placement Details of 2013,2012,2011 Batch</p>
						<a href="img\placements\placement_events\administration-news-3 yrs placement list.docx" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Student List For Placement Training </h2>
					
						<p>Student List For Placement Training Jan 2014. KSIT ECE Dept...</p>
						<a href="img\placements\placement_events\administration-news-placement.zip" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">TECHMAHINDRA POOL CAMPUS RECRUITMENT DRIVE</h2>
					
						<p>K.S.INSTITUTE OF TECHNOLOGY WELCOMES STUDENTS FROM VARIOUS PART OF INDIA FOR TECHMAHINDRA POOL CAMPUS RECRUITMENT DRIVE FOR 2013 BATCH PASSED OUT STUDENTS on 17th Nov 2013 and 18th Nov 2013</p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">M/s Tata Consultancy is conducting the CodeVita</h2>
					
						<p>Greetings from KSIT Blr..<br/>M/s Tata   Consultancy is conducting the  CodeVita  -  A  nationwide Coding contest for all students of batch 2014,2015,2016,2017 i.e right from 1st year to 4th year, The contest is open to all the accredited institutes, group institutes and non accredited institutes across India.Last date for registration is 10th August 2013.</p>
						<a href="img\placements\placement_events\administration-news-Attachments_201388.zip" target="_blank">Click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
			
				
				
				 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>