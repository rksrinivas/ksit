<@page>
		<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <h3>AI & ML Engineering Events</h3>
                    </blockquote>
                    
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                
                
                 <#list aimlEventsList as aimlEvent>
                    
					
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
                    
						<h2 class="singCourse_title">
						
								"${aimlEvent.heading!}"
						
						</h2>
						
						<p>	 <img alt="img" src="${aimlEvent.imageURL!}" class="media-object">	</p>
						
						
						<p>
						
							"${aimlEvent.content!}"
						
						</p>
						
						
						<p>
						
							"${aimlEvent.eventDate!}"
						
						</p>
						
                    </div>
                   
                  </div>
                  
						
			  </#list>
						
                  
                </div>
                <!-- End single course -->
				
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NATIONAL CONFERENCE AND PROJECT EXHIIBITION</h2>
						<p><img alt="img" src="img\tele\tele_events\2.jpg" class="media-object"></p>
						
						<p>National Conference was organised in the Dept on 10th and 11th May 2017 We had paper presentation in various sessions and project exhibition was also conducted on 12th May 2017 to show case the best project and Students were given a platform to exhibit their talent.</p>
						
						<p><span class="feed_date">2017-05-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">TECHNICAL TALK ON SPACE TECHNOLOGY IN INDIA</h2>
						<p><img alt="img" src="img\tele\tele_events\n4 (1).jpg" class="media-object"></p>
						
						   <p>TECHNICAL TALK ON SPACE TECHNOLOGY IN INDIA WAS HELD ON 25TH MARCH 2017</p>
						
						<p>                       <span class="feed_date">2017-03-25</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">WORK SHOP ON INTRO ARDUINO</h2>
						<p><img alt="img" src="img\tele\tele_events\ws1.jpg" class="media-object"></p>
						
						<p>Two day IETE workshop on INTROARDUINO by INVERSA TECHNOSOFT PVT LTD</p>
						
						<p>                          <span class="feed_date">2017-03-03</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">VISIT TO IISC</h2>
						<p><img alt="img" src="img\tele\tele_events\IISC.jpg" class="media-object"></p>
						
						<p>On the account of National Science Day,the Indian Institute of Science IISC, Bengaluru, hosts an Open Day event every year to spread knowledge on various innovations and research to the public. Open Day 2017 was held on the 4th of March, TCE Dept faculties and students visited IISC on this Open Day</p>
						
						<p><span class="feed_date">2017-03-04</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">INDUSTRIAL VISIT TO ISRO 2017</h2>
						<p><img alt="img" src="img\tele\tele_events\6-1.jpg" class="media-object"></p>
						
						   <p>Industrial visit to ISRO was conducted for 6th semester students of TCE Dept</p>
						
						<p><span class="feed_date">2017-03-09</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">INDUSTRIAL VISIT </h2>
						<p><img alt="img" src="img\tele\tele_events\phpZLCfvVAM.jpg" class="media-object"></p>
						
						<p>Industrial visit to ISRO was conducted for 4th semester students of TCE Dept</p>
						
						<p><span class="feed_date"> 2017-03-09</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">INDUSTRIAL VISIT TO IMTEX 2017</h2>
						<p><img alt="img" src="img\tele\tele_events\3M (1).jpg" class="media-object"></p>
						
						<p>Faculties of TCE Dept attended the IMTEX 2017 and the concurrent Tooltech 2017 exhibition which was organized by Indian Machine Tool Manufacturers Association from 26th Jan to 1st Feb 2017 at the Bangalore International Exhibition Centre</p>
						
						<p>                          <span class="feed_date">2017-01-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">FACULTY DEVELOPMENT PROGRAM</h2>
						<p><img alt="img" src="img\tele\tele_events\php3QiYN8AM.jpg" class="media-object"></p>
						
						   <p>Dept of TCE along with Dept of ECE and Dept of CSE had organized Faculty Development Program on 'Raspberry Pi and its Applications in IoT' from 18th - 21st Jan 2017</p>
						
						<p><span class="feed_date">2017-01-18</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">VANQUISH-16</h2>
						<p>                          <img alt="img" src="img\tele\tele_events\1.JPG" class="media-object"></p>
						
						   <p>Dept of Telecommunication Engg, organised a technical event VANQUISH-16 on the eve of Engineers day,the inspiration was drawn from Dr.C.N.Rao, and this occasion was graced by Narayanaswamy .H.P, TCS,Bangalore</p>
						
						<p><span class="feed_date">2016-09-15</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
              
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">TECHNICAL TALK </h2>
						<p><img alt="img" src="img\tele\tele_events\isro.jpg" class="media-object"></p>
						
						   <p>Dept of TCE Organised a Technical Talk on Space Technology in INDIA by Shri.Srinivasa,Scientist, ISRO,Bengaluru.</p>
						
						<p><span class="feed_date">2016-09-23</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IETE Workshop</h2>
						<p><img alt="img" src="img\tele\tele_events\4.jpg" class="media-object"></p>
						
						   <p>The Two day IETE workshop on ARM CORTEX by TENET TECHNETRONICS was jointly organized by TCE & ECE Departments of K.S.I.T on 27th and 28th of August 2016.</p>
						
						<p>                       <span class="feed_date">2016-08-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Industrial Visit  </h2>
						<p><img alt="img" src="img\tele\tele_events\hal16-17.jpg" class="media-object"></p>
						
						<p>Industrial Visit to HAL Heritage Center and Aerospace Museum was organised for 3rd Semester students of TCE Dept on 20th August 2016</p>
						
						<p><span class="feed_date">2016-08-20</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PROJECT EXIHIBITION & PAPER PRESENTATION CONTEST</h2>
						<p><img alt="img" src="img\tele\tele_events\" class="media-object"></p>
						
						<p>The project exhibition was conducted to show case the best project and exhibit the same. Students were given a platform to exhibit their talent. We also had paper presentation contest on the same day.</p>
						
						<p><span class="feed_date"> 2016-05-14</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">technical talk</h2>
						<p><img alt="img" src="img\tele\tele_events\phpR4eDIJPM.jpg" class="media-object"></p>
						
						   <p>Technical Talk on Multimedia: Fundamentals, Algorithms and Applications by Dr.H.S.Prasantha , NITTE Meenakshi Institute of Technology, was organised by Dept of TCE</p>
						
						<p><span class="feed_date"> 2016-04-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">technical talk   </h2>
						<p><img alt="img" src="img\tele\tele_events\Optimized-IMG_20160405_110120 (1).jpg" class="media-object"></p>
						
						   <p>Mr Akhil ,Alumini of KSIT Telecom department had given a technical talk on "industrial trend and personality devolopement skills"</p>
						
						<p><span class="feed_date">2016-04-04</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">wireless embedded system workshop </h2>
						<p><img alt="img" src="img\tele\tele_events\39320.jpg" class="media-object"></p>
						
						<p>A two day IETE Workshop on "Wireless Embedded System " by Tenet Technetronics was jointly organised by Dept of TCE and ECE on 26th and 27th February 2016.</p>
						
						<p><span class="feed_date">2016-02-26</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Seminar on DIGITAL RADIO MAONDIALE </h2>
						<p><img alt="img" src="img\tele\tele_events\php7roVtfPM.jpg" class="media-object"></p>
						
						<p>Technical Seminar on DIGITAL RADIO MAONDIALE was organized in the department and was delivered by Mr.V.G.Malagi,Dy.Director All India Radio. The evolution of Radio Communication and DRM were briefed in the talk.</p>
						
						<p><span class="feed_date">2015-04-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar on Challenges in Vector Control</h2>
						<p><img alt="img" src="img\tele\tele_events\2 (1).jpg" class="media-object"></p>
						
						   <p>Seminar on "Challenges in Vector Control" by Dr.S.K.Ghosh, Scientist-G, NIMR Field Unit Bangalore, was organised for IV, VI,VIII semester students of Telecommunication Engg Dept on 8th April 2015</p>
						
						<p><span class="feed_date">2015-04-08</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IETE STUDENTS FORUM INAUGURATION </h2>
						<p><img alt="img" src="img\tele\tele_events\2 (2).jpg" class="media-object"></p>
						
						   <p>Conducted an event of IETE student forum inauguration (ISF) Jointly organized with Department of Telecommunication Engineering and Department of Electronics and Communication Engineering on 12th March 2015. Chief guest Mr.D.C.Pande, chairman, IETE, Bangalore keynote speaker Mr.M.H.Kori, TPC Chairman, IETE HQ.</p>
						
						<p><span class="feed_date">2015-03-12</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">SEMINAR ON CAREER GUIDANCE ON THE INTERNATIONAL EDUCATION SCENE & ADVANTAGES OF RESEARCH</h2>
						<p><img alt="img" src="img\tele\tele_events\SEMINAR1.jpg" class="media-object"></p>
						
						   <p>Seminar on Career Guidance on the international education scene & advantages of research Delivered by Ms. Sreelatha.T, Mr. Sreejit Narayan Jointly organized by Department of Telecommunication Engineering and Electronics & Communication Engineering on 09/03/2015.</p>
						
						<p><span class="feed_date">2015-03-09</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">INDUSTRIAL VISIT </h2>
						<p><img alt="img" src="img\tele\tele_events\1-hal.jpg" class="media-object"></p>
						
						   <p>An Industrial Visit to HAL Heritage Centre and Museum was Organised for IV semester students of TCE Department on 14 February 2015</p>
						
						<p><span class="feed_date">2015-02-14</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE </h2>
						<p><img alt="img" src="img\tele\tele_events\viji.jpg" class="media-object"></p>
						
						<p>GUEST LECTURE ON "VEDIC APPROACH TO MODERN ENGINEERING " by Mr.VIJAY.V,Asst.Prof, Telecommunication Engg Dept, K.S.I.T, on 27.09.2014</p>
						
						<p><span class="feed_date"> 2014-09-27</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE</h2>
						<p><img alt="img" src="img\tele\tele_events\php6Xu5BzPM.jpg" class="media-object"></p>
						
						<p>GUEST LECTURE ON "DATA COMMUNICATION AND INNOVATION" By Mr.AKHIL.B ,on 23.09.2014</p>
						
						<p>                          <span class="feed_date">2014-09-23</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">VANQUISH-2014</h2>
						<p><img alt="img" src="img\tele\tele_events\vanguish.jpg" class="media-object"></p>
						
						<p>VANQUISH-2014 WAS AN EVENT ORGANISED TO MARK ENGINEERS DAY.ITS A CLUBBED EVENT OF BOTH TECHNICAL AND NON-TECHNICAL ROUNDS CONSISTING OF ROUNDS LIKE QUIZ,BOUNTY HUNT,PIC-RASH AND JACK OF ALL</p>
						
						<p><span class="feed_date">2014-09-12</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Hands On Experience</h2>
						<p><img alt="img" src="img\tele\tele_events\phpNHLBQYPM.jpg" class="media-object"></p>
						
						<p>Hands on Experience on " RASPBERRY PI " by Tenet Technetronics was organised on 17th May 2014</p>
						
						<p><span class="feed_date">Hands On Experience</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PROJECT EXHIBITION AND PAPER PRESENTATION</h2>
						<p><img alt="img" src="img\tele\tele_events\phpwVHY93PM.jpg" class="media-object"></p>
						
						   <p>PROJECT EXHIBITION AND PAPER PRESENTATION CONTEST WAS HELD ON 10th MAY 2014</p>
						
						<p><span class="feed_date">2014-05-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE</h2>
						<p><img alt="img" src="img\tele\tele_events\phpWyIZVXPM.jpg" class="media-object"></p>
						
						<p>Guest lecture was organised on 8th April 2014 on the topic " CURING THE INCURABLE IN ACADEMIC ENVIRONMENT "</p>
						
						<p><span class="feed_date">2014-04-08</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE</h2>
						<p><img alt="img" src="img\tele\tele_events\php1GnPqzAM.jpg" class="media-object"></p>
						
						   <p>Technical talk on OSS/BSS TELECOM NETWORKS delivered by Mr.Girish.K.S & Mr.Ravindra Sharma wireless division Reliance</p>
						
						<p><span class="feed_date">2014-03-22</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">2 Days Workshop On " Matlab Tools & Simulink" </h2>
						<p><img alt="img" src="img\tele\tele_events\phpExrSNPAM.jpg" class="media-object"></p>
						
						<p>2 Days Workshop On " Matlab Tools & Simulink" was held on 13/03/2014 and 14/03/2014. Jointly organised by Dept of Telecommunication Engg & Dept of Electronic and Communication Engg</p>
						
						<p><span class="feed_date">2014-03-13</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PERSONALITY DEVELOPMENT TRAINING </h2>
						<p><img alt="img" src="img\tele\tele_events\" class="media-object"></p>
						
						<p>Personality development programme is being conducted from 25 jan 2014 to 31st Jan 2014 in ksit. Thirty seven students from 4th semester and forty three students from 6th semester Telecommunication Engineering are attending this programme.</p>
						
						<p><span class="feed_date"> 2014-01-25</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National conference </h2>
						<p><img alt="img" src="img\tele\tele_events\phpWaosDoAM.jpg" class="media-object"></p>
						
						   <p>National conference on Advancement in Communication and Computation</p>
						
						<p><span class="feed_date">2013-09-28 </span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE </h2>
						<p><img alt="img" src="img\tele\tele_events\phppycLGTAM.jpg" class="media-object"></p>
						
						<p>GUEST LECTURE ON "PHOTON JOURNEY ENCOMPASS TELECOMMUNICATION" By DR.T.G.K.Murthy ,on 08.04.2013</p>
						
						<p><span class="feed_date">2013-04-08 </span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture </h2>
						<p><img alt="img" src="img\tele\tele_events\phpxsjOBWAM.jpg" class="media-object"></p>
						
						<p>GUEST LECTURE ON "PRACTICAL SCENARIOS AND EXAMPLE OF COMMUNICATION WITH SATELLITE" ,on 15th April 2013</p>
						
						<p><span class="feed_date">  2013-04-15</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">THREE DAY NATIONAL LEVEL WORKSHOP ON MATLAB TOOLS & ITS APPLICATIONS  </h2>
						<p><img alt="img" src="img\tele\tele_events\phpKsjgVgAM.jpg" class="media-object"></p>
						
						<p>This workshop was jointly organized by Electronics and Telecommunication departments for three days from Feb 28th to Mar 2nd 2013.Seventy delegates from in and around Bangalore attended this workshop. This workshop was divided into three sessions with regress hands on training. The resource person for the first day was Dr. K. Uma Rao, R.V.C.E, Bangalore, for the second day we had Dr. Andhe Pallavi, Head I.T department R.N.S.I.T, Bangalore and for the third day we had resource person from the industry.</p>
						
						<p><span class="feed_date"> 2013-03-02</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">3 DAYS WORKSHOP ON WIRELESS SENSOR NETWORKS  </h2>
						<p><img alt="img" src="img\tele\tele_events\resize_workshop.jpg" class="media-object"></p>
						
						   <p>Wireless sensor network workshop was organized in the department and was delivered by Mr.Gurujith Singh Managing Director Gill Instruments pvt. Ltd. In this workshop students were made to design and work with sensors using the microcontrollers.</p>
						
						<p><span class="feed_date">2012-10-13 </span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NATIONAL CONFERENCE ON COMMUNICATIONS & COMPUTATIONS</h2>
						<p><img alt="img" src="img\tele\tele_events\resize_conference.jpg" class="media-object"></p>
						
						   <p>The key note speaker for the event was Sri V. Ramachandra (Director LVPO, ISRO) and the guest of honor for the event was Mrs.Vanishree.P.Acharya(M.D,Kromaspects).In this conference we had six sessions where in 60 different papers were presented by the delegates from different states. The best paper for each session was awarded and the best paper for the day was awarded with cash prize of Rs.5000/.</p>
						
						<p><span class="feed_date">2012-09-29 </span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">TECHNICAL SEMINAR ON WIRELESS SENSOR NETWORKS </h2>
						<p><img alt="img" src="img\tele\tele_events\resize_seminar.jpg" class="media-object"></p>
						
						   <p>Technical seminar on wireless sensor networks was organized in the department and was delivered by Mr.Gurujith Singh Managing Director Gill Instruments pvt.Ltd. The design and uses of wireless sensors was briefed in the talk.</p>
						
						<p><span class="feed_date"> 2012-08-08</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PROJECT EXIHIBITION & PAPER PRESENTATION CONTEST</h2>
						<p><img alt="img" src="img\tele\tele_events\php9bp1jAPM.jpg" class="media-object"></p>
						
						<p>The project exhibition was conducted to show case the best project and exhibit the same. Students were given a platform to exhibit their talent. We also had paper presentation contest on the same day.We had Prof Anand .B.Gudi head ECE department J.E.C Bangalore</p>
						
						<p><span class="feed_date"> 2012-05-17</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">ENGINEERS DAY </h2>
						<p><img alt="img" src="img\tele\tele_events\engineers day.jpg" class="media-object"></p>
						
						<p>On the eve of engineers day we conducted paper presentation contest circuit debugging and technical quiz.</p>
						
						<p><span class="feed_date">2011-09-11 </span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 		
				
				
				 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>