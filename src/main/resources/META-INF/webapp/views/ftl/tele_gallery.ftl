<@page>
	<!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
            
             <#list teleGalleryList as teleGallery>
				
					<a href="${teleGallery.imageURL!}" title="">
	                    <img class="gallery_img" src="${teleGallery.imageURL!}" alt="img" />
	                	<span class="view_btn">View</span>
	                </a>
				
				</#list>
            
                <a href="${img_path!}/tele/t1.jpg" title="This is Title">
                  <img class="gallery_img" src="${img_path!}/tele/t1.jpg" alt="img" />
                <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t2.jpg" title="This is Title2">
                  <img class="gallery_img" src="${img_path!}/tele/t2.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t3.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/tele/t3.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/te4.jpg" title="This is Title4">
                  <img class="gallery_img" src="${img_path!}/tele/t4.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t5.jpg" title="This is Title5">
                  <img class="gallery_img" src="${img_path!}/tele/t5.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t6.jpg">
                  <img class="gallery_img" src="${img_path!}/tele/t6.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t7.jpg">
                  <img class="gallery_img" src="${img_path!}/tele/t7.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t8.jpg">
                  <img class="gallery_img" src="${img_path!}/tele/t8.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                 <a href="${img_path!}/tele/t9.jpg" title="This is Title">
                  <img class="gallery_img" src="${img_path!}/tele/t9.jpg" alt="img" />
                <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t10.jpg" title="This is Title2">
                  <img class="gallery_img" src="${img_path!}/tele/t10.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t11.jpg" title="This is Title3">
                  <img class="gallery_img" src="${img_path!}/tele/t11.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t12.jpg" title="This is Title4">
                  <img class="gallery_img" src="${img_path!}/tele/t12.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t13.jpg" title="This is Title5">
                  <img class="gallery_img" src="${img_path!}/tele/t13.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t14.jpg">
                  <img class="gallery_img" src="${img_path!}/tele/t14.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t15.jpg">
                  <img class="gallery_img" src="${img_path!}/tele/t15.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
                <a href="${img_path!}/tele/t16.jpg">
                  <img class="gallery_img" src="${img_path!}/tele/t16.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>