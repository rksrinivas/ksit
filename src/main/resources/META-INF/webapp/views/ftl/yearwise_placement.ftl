<@page>
	        <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
          
      <div class="courseArchive_content">
			  
			  <div clas="row">
			  	<#list yearSet as year>
			  		 <#setting number_format="0" />
			  		<h3 class="text-center">Placement Data for year ${year?c!}</h3>
            <hr/>
				  	<table class="table table-striped course_table">
						<thead>
							<tr>          
								<th>SL.NO. </th>
								<th>Company</th>
								<th>TOTAL STUDENTS PLACED</th>											
							</tr>
						</thead>
						<tbody>
							<#assign count = 1>
							
							<#list placementCompanyList as placementCompany>
								<#if placementCompany.year == year>
								
									<tr>
										<td>${count!}</td>
										<td>${placementCompany.company!}</td>
										<td>${placementCompany.noOfStudents!}</td>
										<#assign count = count + 1>
									</tr>
								</#if>
							</#list>
						</tbody>
					</table>
			  	</#list>
				
			</div>
      <hr/>
          <div class="row">
					<h3 class="text-center">Yearwise Placements</h3>

					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>The Statistics and Placement details for the Batch 2016, 2015, 2014, 2013, 2012, 2009 and 2008 is as listed below</p>
					  
					  <p>LIST OF STUDENTS PLACED IN 2016 BATCH AS ON 24/04/2016 </p>
                    </blockquote>

                <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	Indian Navy </h3>
					<h3 class="singCourse_title"><span>EC: </span>11 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 3</h3>
					<h3 class="singCourse_title"><span>CS: </span>	3 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 9</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>26 (S) </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> 	INFOSYS </h3>
					<h3 class="singCourse_title"><span>EC: </span>12 </h3>
					<h3 class="singCourse_title"><span>TC: </span>6 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	14 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 8</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>40 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>NTT DATA </h3>
					<h3 class="singCourse_title"><span>EC: </span> 9</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span> 10	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 19 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	L&T Infotech </h3>
					<h3 class="singCourse_title"><span>EC: </span>6 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 6 </h3>
					<h3 class="singCourse_title"><span>CS: </span> 8	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>20 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>ALPHA 9 MARINE </h3>
					<h3 class="singCourse_title"><span>EC: </span>15 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 2</h3>
					<h3 class="singCourse_title"><span>CS: </span>	10 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>17 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>44 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> 	Tech Mahindra</h3>
					<h3 class="singCourse_title"><span>EC: </span>9 </h3>
					<h3 class="singCourse_title"><span>TC: </span>8</h3>
					<h3 class="singCourse_title"><span>CS: </span>	6 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 4</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>27 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	IBM</h3>
					<h3 class="singCourse_title"><span>EC: </span> 3</h3>
					<h3 class="singCourse_title"><span>TC: </span> 2</h3>
					<h3 class="singCourse_title"><span>CS: </span>	5 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 11</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>WEST LINE SHIP MANAGEMENT PVT.LTD </h3>
					<h3 class="singCourse_title"><span>EC: </span>8 </h3>
					<h3 class="singCourse_title"><span>TC: </span>2 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	4 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>6 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 20</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	V-TIGER </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span>	2 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 2</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> CAPGEMINI</h3>
					<h3 class="singCourse_title"><span>EC: </span>5 </h3>
					<h3 class="singCourse_title"><span>TC: </span>6 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	6 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 18</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> 	VIRTUSA</h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>CS: </span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	HP (INC) </h3>
					<h3 class="singCourse_title"><span>EC: </span>4</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>5 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	HP (ENT) </h3>
					<h3 class="singCourse_title"><span>EC: </span> 2</h3>
					<h3 class="singCourse_title"><span>TC: </span> 2</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 4</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>CEASE FIRE </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>2 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 2</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> COGNIZANT</h3>
					<h3 class="singCourse_title"><span>EC: </span>2 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>CS: </span>	2 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>4 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	AMAZON </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> 1</h3>
					<h3 class="singCourse_title"><span>CS: </span>1	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>2 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>4 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>6D Technology	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1 (S)</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>M phasis </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span> 2	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	Phoenix Sea Services Pvt.Ltd </h3>
					<h3 class="singCourse_title"><span>EC: </span> 5</h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 5 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>Hero MotoCorp </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA </h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>  </h3>
					<h3 class="singCourse_title"><span>EC: </span>  NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA </h3>
					<h3 class="singCourse_title"><span>CS: </span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> HCL TALENT CARE</h3>
					<h3 class="singCourse_title"><span>EC: </span>2 </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span>	7 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 9 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> ICTI</h3>
					<h3 class="singCourse_title"><span>EC: </span> NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>1 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> Rothenberger </h3>
					<h3 class="singCourse_title"><span>EC: </span>  NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA </h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1 (S)</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>SISNETIC CLOUD SOLUTION PVT LTD </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>CS: </span>	2 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>ADECO </h3>
					<h3 class="singCourse_title"><span>EC: </span>1</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>CS: </span>	2 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>3 (S) </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<p>Total number of students placed : 282</p>
				<p><span style="color:red;">*</span>S - ShortListed</p>
				
				
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                       <p>LIST OF STUDENTS PLACED IN 2015 </p>
                 </blockquote>
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> INFOSYS</h3>
					<h3 class="singCourse_title"><span>EC: </span> 19</h3>
					<h3 class="singCourse_title"><span>TC: </span> 7 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	18 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 18 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>62 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> 	NTTDATA</h3>
					<h3 class="singCourse_title"><span>EC: </span>8 </h3>
					<h3 class="singCourse_title"><span>TC: </span>6 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	11 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 25</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>MPHASIS </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>CS: </span>	6 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>6 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>TECH MAHINDRA </h3>
					<h3 class="singCourse_title"><span>EC: </span>9 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 7</h3>
					<h3 class="singCourse_title"><span>CS: </span>7	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>2 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>25 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>L & T INFOTECH </h3>
					<h3 class="singCourse_title"><span>EC: </span>10 </h3>
					<h3 class="singCourse_title"><span>TC: </span>8 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	9 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>27 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>	IBM </h3>
					<h3 class="singCourse_title"><span>EC: </span>4 </h3>
					<h3 class="singCourse_title"><span>TC: </span>1 </h3>
					<h3 class="singCourse_title"><span>CS: </span>2	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>7 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span> 	HP	</h3>
					<h3 class="singCourse_title"><span>EC: </span> 2</h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>CS: </span>7	 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>2 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 11</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company: </span>MINDTREE </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 1</h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> CAPGEMINI</h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span> 3</h3>
					<h3 class="singCourse_title"><span>CS:</span>2	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 5</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>WEST LINE  SHIPPING PVT LTD </h3>
					<h3 class="singCourse_title"><span>EC:</span>19 </h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	4 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 34</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>57 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>ARICENT </h3>
					<h3 class="singCourse_title"><span>EC:</span> 3</h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>4	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 7</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>SONATA SOFTWARE </h3>
					<h3 class="singCourse_title"><span>EC:</span>4 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span> 0	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 4</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>TOYOTA KIRLOSKAR TEXTILES PVT LTD </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>0	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 2</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> MERCEDES  BENZ</h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	2 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>TRIVENI TURBINES </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>EQUIFAX </h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	3 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>3 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> NOKIA</h3>
					<h3 class="singCourse_title"><span>EC:</span> 1</h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> BRAKES INDIA</h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>0	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>MICROLAND </h3>
					<h3 class="singCourse_title"><span>EC:</span> 1</h3>
					<h3 class="singCourse_title"><span>TC:</span>2 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 4</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>HEALTH ASYST</h3>
					<h3 class="singCourse_title"><span>EC:</span> 1</h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>0	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>CMC TATA </h3>
					<h3 class="singCourse_title"><span>EC:</span> 4</h3>
					<h3 class="singCourse_title"><span>TC:</span> 8</h3>
					<h3 class="singCourse_title"><span>CS:</span>	3 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>2 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>17 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> SANDVIK ASIA PVT LTD</h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>METRIC STREAM	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> ZOVI</h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span>1</h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> CLUSOINFO LINK PVT LTD</h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>FIRST AMERICAN </h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span>2 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 2</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> TCS</h3>
					<h3 class="singCourse_title"><span>EC:</span> 1</h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>FIRSTSOURCE SOLUTIONS LTD </h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>INTEGRA TECHNOLOGIES </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>HASHEDIN TECHNOLOGIES </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> e-EMPHASYS TECHNOLOGIES</h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>3	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 3</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>RAZOR THINK </h3>
					<h3 class="singCourse_title"><span>EC:</span>1 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> NOVELL SOFTWARE</h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>1	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> GE TRANSPORTATION</h3>
					<h3 class="singCourse_title"><span>EC:</span>1 </h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>BOSCH </h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>12 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>12 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>V-TIGER </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>1	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>6D TECHNOLOGIES </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span>1 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> YAHOO</h3>
					<h3 class="singCourse_title"><span>EC:</span> 1</h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> BERRY PLASTIC</h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 1</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> RARE MAIL TECHNOLOGIES</h3>
					<h3 class="singCourse_title"><span>EC:</span> 0</h3>
					<h3 class="singCourse_title"><span>TC:</span> 0</h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	LOWE'S </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span> NA</h3>
					<h3 class="singCourse_title"><span>CS:</span>1	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 2</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					
                <!-- End single course -->
				
				
					<p>Total number of students placed : 303</p>
				    <p><span style="color:red;">*</span>S - ShortListed</p>
				
				
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                       <p>LIST OF STUDENTS PLACED IN 2014 </p>
                 </blockquote>
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Infosys </h3>
					<h3 class="singCourse_title"><span>EC:</span> 10</h3>
					<h3 class="singCourse_title"><span>TC:</span> 03</h3>
					<h3 class="singCourse_title"><span>CS:</span>	05 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 06</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>24 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Tata Consultancy Services </h3>
					<h3 class="singCourse_title"><span>EC:</span> 03</h3>
					<h3 class="singCourse_title"><span>TC:</span> 00</h3>
					<h3 class="singCourse_title"><span>CS:</span>	06 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 10</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> L and T Info tech</h3>
					<h3 class="singCourse_title"><span>EC:</span> 05</h3>
					<h3 class="singCourse_title"><span>TC:</span>04 </h3>
					<h3 class="singCourse_title"><span>CS:</span> 11 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 03</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>23 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Cognizant Technologies</h3>
					<h3 class="singCourse_title"><span>EC:</span>	03 </h3>
					<h3 class="singCourse_title"><span>TC:</span>00 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	08 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 	00</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>11 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Tech Mahindra</h3>
					<h3 class="singCourse_title"><span>EC:</span> 	05</h3>
					<h3 class="singCourse_title"><span>TC:</span>	06 </h3>
					<h3 class="singCourse_title"><span>CS:</span>		15 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>02 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>	28 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Volvo IT </h3>
					<h3 class="singCourse_title"><span>EC:</span>07 </h3>
					<h3 class="singCourse_title"><span>TC:</span>	NA</h3>
					<h3 class="singCourse_title"><span>CS:</span>		04 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 	NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 11</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	NTTDATA </h3>
					<h3 class="singCourse_title"><span>EC:</span>10 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 	03</h3>
					<h3 class="singCourse_title"><span>CS:</span>	13 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 26</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Razor Think </h3>
					<h3 class="singCourse_title"><span>EC:</span>	NA </h3>
					<h3 class="singCourse_title"><span>TC:</span> 	NA</h3>
					<h3 class="singCourse_title"><span>CS:</span>	01	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 1(Shortlisted)</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Hp </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 01</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Knowx Innovations </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Retail  On</h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Racks & Rollers </h3>
					<h3 class="singCourse_title"><span>EC:</span> NA</h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>O </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Rare Mile Technologies</h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Aptean </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 0</h3>
				
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 	Mphasis</h3>
					<h3 class="singCourse_title"><span>EC:</span> 00</h3>
					<h3 class="singCourse_title"><span>TC:</span> 00</h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>00 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Hive Minds </h3>
					<h3 class="singCourse_title"><span>EC:</span>00 </h3>
					<h3 class="singCourse_title"><span>TC:</span>00 </h3>
					<h3 class="singCourse_title"><span>CS:</span>00	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>00 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>00 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Common Floor</h3>
					<h3 class="singCourse_title"><span>EC:</span> 00</h3>
					<h3 class="singCourse_title"><span>TC:</span> NA</h3>
					<h3 class="singCourse_title"><span>CS:</span>02	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>2(shortlisted) </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>united Spirits </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span> NA</h3>
					<h3 class="singCourse_title"><span>CS:</span>NA	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>23 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Cegedium </h3>
					<h3 class="singCourse_title"><span>EC:</span>00 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 00</h3>
					<h3 class="singCourse_title"><span>CS:</span>00	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>00 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>00 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Sap Labs </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 2</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>CMC - TATA </h3>
					<h3 class="singCourse_title"><span>EC:</span>07 </h3>
					<h3 class="singCourse_title"><span>TC:</span>00 </h3>
					<h3 class="singCourse_title"><span>CS:</span>00	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 00</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 07</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Unisys </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span>00 </h3>
					<h3 class="singCourse_title"><span>CS:</span>03	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>00 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>05 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Kriloskar Toyota Textile Pvt.Ltd </h3>
					<h3 class="singCourse_title"><span>EC:</span> NA</h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>NA	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>02 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Tech Mahindra </h3>
					<h3 class="singCourse_title"><span>EC:</span>05 </h3>
					<h3 class="singCourse_title"><span>TC:</span>06 </h3>
					<h3 class="singCourse_title"><span>CS:</span>15	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>02 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>28 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Health Asyst </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>00 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>00 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>GE India Pvt.Ltd </h3>
					<h3 class="singCourse_title"><span>EC:</span> 00</h3>
					<h3 class="singCourse_title"><span>TC:</span>00 </h3>
					<h3 class="singCourse_title"><span>CS:</span>00	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>00 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>00 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	VTIGER </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Everest India Pvt. Ltd </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>NA	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01(SL) </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Triveniturbines Pvt. Ltd </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>NA	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 00</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>00 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Mega Engineering Infrastructure Limited </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Accord Software </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>CS:</span>0	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Acropetal  Technologies Limited </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Arayaka Networks </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Amazon </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>02	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>HCL Technology </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 	Bhash Software labs</h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Alackrity Consols Pvt Ltd </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 04</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>04 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Hauwei Technology Pvt Ltd</h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Bosch </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Allgo Embedded system Pvt Ltd </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> RA</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Open Text </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>S2C Consultant Pvt Ltd </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Saint Gobin </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01(S) </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Flipkart </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 01</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> CMC Limited</h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> SkillBrew P3 Infotech</h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Sevon India	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Toshiba  Transitions & Distributed Systems </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> RA</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Odessa Technology </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> RA</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>UTC Aerospace Systems </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Rinac India Pvt Ltd</h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span>-</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Alcatel-Lucent </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Just Dial Limited </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Vembu Technologies </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>6D Telecom </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Robosoft </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> RA</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Hawkinfomatics </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> RA</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Global Edge </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>02	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Sonus Networks </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> RA</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Mind Prosoft</h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>RA </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 
				
				
				<p>Total number of students placed/selected : 219</p>
				<p><span style="color:red;">*</span>S - ShortListed</p>
				
				
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                       <p>LIST OF STUDENTS PLACED IN 2013 </p>
                 </blockquote>
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Sasken </h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>03 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 	L and T Info tech</h3>
					<h3 class="singCourse_title"><span>CS:</span>21	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 11</h3>
					<h3 class="singCourse_title"><span>TC:</span> 10</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 08</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>50 </h3>
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Indian Navy </h3>
					<h3 class="singCourse_title"><span>CS:</span>	01 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Quinnox Consultancy services </h3>
					<h3 class="singCourse_title"><span>CS:</span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>02 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> United Spirits Limited</h3>
					<h3 class="singCourse_title"><span>CS:</span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span> NA</h3>
					<h3 class="singCourse_title"><span>MECH:</span>02 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Microsoft India</h3>
					<h3 class="singCourse_title"><span>CS:</span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> NA</h3>
					<h3 class="singCourse_title"><span>TC:</span> 07(S)</h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>07(S) </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Sap Labs	</h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> 03</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>05 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Unisys Technology Pvt.Ltd.</h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 03</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 01</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>07 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Volvo IT	</h3>
					<h3 class="singCourse_title"><span>CS:</span>04	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>03 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 07</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Triveni Turbines Pvt.Ltd </h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Virtusa </h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 03</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>LJosiah Technology Pvt.Ltd </h3>
					<h3 class="singCourse_title"><span>CS:</span>10	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> 04</h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 15</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 	Xchanging Sol. Pvt.Ltd.</h3>
					<h3 class="singCourse_title"><span>CS:</span>	03 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span>05 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>10 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Razor Think	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> NA</h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> VIT Infotech</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 02</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 01</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Enzen	</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Vital Labs </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Subex </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>NRN </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Hp	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>VTIGER	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 	NTTDATA</h3>
					<h3 class="singCourse_title"><span>CS:</span>06	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 01</h3>
					<h3 class="singCourse_title"><span>TC:</span> 05</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 02</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>14 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Ola Cabs	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Gill Instruments	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	01 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 01</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>CMC Ltd (A TATA Enterprises) </h3>
					<h3 class="singCourse_title"><span>CS:</span>	09 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span>07 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>18 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Health Asyst </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 01</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	IBM </h3>
					<h3 class="singCourse_title"><span>CS:</span>	05 </h3>
					<h3 class="singCourse_title"><span>EC:</span>03 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 02</h3>
					<h3 class="singCourse_title"><span>MECH:</span>03 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>13 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>6D Technologies	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 03</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Global Automation	</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> STS Info Systems (CISCO)</h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Retail On </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>02 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>CISCO </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Kirloskar Toyoda Textile Pvt. Ltd </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>10 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>10 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Ericsson </h3>
					<h3 class="singCourse_title"><span>CS:</span>	01 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Bosch Campus Recruitment Drive </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 07</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 07</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Huawei Campus Drive </h3>
					<h3 class="singCourse_title"><span>CS:</span>	03 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>02 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>06 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Artech Info Systems	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Zyne Solutions	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Global Edge Software</h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 01</h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 03</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 3i Infotech</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 03</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	V Works Software Pvt.Ltd </h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 01</h3>
					<h3 class="singCourse_title"><span>TC:</span> 02</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 05</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> GENPACT</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 02</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Ingersoll Rand</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 06</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 06</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Accenture (Development) </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Accenture (Tech Support) </h3>
					<h3 class="singCourse_title"><span>CS:</span>03	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>03 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 02</h3>
					<h3 class="singCourse_title"><span>MECH:</span>03 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>11 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Net Cracker </h3>
					<h3 class="singCourse_title"><span>CS:</span>	01 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>03 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Vital Labs</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>CTS </h3>
					<h3 class="singCourse_title"><span>CS:</span>03	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 03</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 08</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Tech Mahindra</h3>
					<h3 class="singCourse_title"><span>CS:</span>	07 </h3>
					<h3 class="singCourse_title"><span>EC:</span>07 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 03</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 08</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>25 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>- </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>02</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 03</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<p>Total number of students placed : 270</p>
				    <p><span style="color:red;">*</span>S - ShortListed</p>
				
				
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                       <p>LIST OF STUDENTS PLACED IN 2012 </p>
                 </blockquote>
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Infosys Technology	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	25 </h3>
					<h3 class="singCourse_title"><span>EC:</span>11 </h3>
					<h3 class="singCourse_title"><span>TC:</span>28 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 17</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>81 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 	Tech Mahindra &  Mahindra Satyam</h3>
					<h3 class="singCourse_title"><span>CS:</span>	04 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 02</h3>
					<h3 class="singCourse_title"><span>TC:</span> 06</h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 12</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>L and T Info tech </h3>
					<h3 class="singCourse_title"><span>CS:</span>	06 </h3>
					<h3 class="singCourse_title"><span>EC:</span>04 </h3>
					<h3 class="singCourse_title"><span>TC:</span>03 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>03 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>16 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Robert Bosch </h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 05</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Oracle Financial Services</h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>EC:</span> NA</h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Qunniox Consulting Services	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span>03 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 03</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> 	Exialant Technology</h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 03</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>07 </h3>	
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Piaggaio	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span> NA</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 04</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>04 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Robosoft</h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>03 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Symphony </h3>
					<h3 class="singCourse_title"><span>CS:</span>	02 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> 01</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 03</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Keane India </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>04 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 04</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Unisys	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span>02 </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 02</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>06 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	IBM </h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 01</h3>
					<h3 class="singCourse_title"><span>TC:</span> 05</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 07</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Global Edge Software</h3>
					<h3 class="singCourse_title"><span>CS:</span>	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 01</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Sasken Communication </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 01</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Creative Genius and Digital Juice Animation </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> T.D.Power Systems	</h3>
					<h3 class="singCourse_title"><span>CS:</span>	NA </h3>
					<h3 class="singCourse_title"><span>EC:</span>NA </h3>
					<h3 class="singCourse_title"><span>TC:</span> NA</h3>
					<h3 class="singCourse_title"><span>MECH:</span>02(s)	 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02(s)	</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Global Edge Software	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 01</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Carrer Xperts </h3>
					<h3 class="singCourse_title"><span>CS:</span>	01 </h3>
					<h3 class="singCourse_title"><span>EC:</span>01 </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Microsoft Indi</h3>
					<h3 class="singCourse_title"><span>CS:</span>	 </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02(s)</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Captronics </h3>
					<h3 class="singCourse_title"><span>CS:</span>	04 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>04 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Toyoda Kirloskar Pvt.Ltd </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span>06 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 06</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> NETTURE Technical Training foundation (NTTE)</h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span>01 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 01</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Continental Automotive Components (India) Pvt Ltd	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span>01 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>01 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Accenture (Tech support) </h3>
					<h3 class="singCourse_title"><span>CS:</span>	03 </h3>
					<h3 class="singCourse_title"><span>EC:</span>02 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 02</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 03</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>10 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Toyota Kirloskar Motor Pvt Ltd	 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span>- </h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span>07 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>07 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Convergys (Tech Support) </h3>
					<h3 class="singCourse_title"><span>CS:</span>03	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 05</h3>
					<h3 class="singCourse_title"><span>TC:</span> 02</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 10</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>IBM (Development) </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 01</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 01</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Craft Silicon Pvt Ltd</h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>HP 24X7 (Tech Support) </h3>
					<h3 class="singCourse_title"><span>CS:</span>-	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> 02</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span> Honey well Technology</h3>
					<h3 class="singCourse_title"><span>CS:</span>01	 </h3>
					<h3 class="singCourse_title"><span>EC:</span> 01</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>02 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>Kirloskar Toyoda Textile Pvt Ltd </h3>
					<h3 class="singCourse_title"><span>CS:</span>	- </h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span> -</h3>
					<h3 class="singCourse_title"><span>MECH:</span> 07</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 07</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company:</span>	Siemens </h3>
					<h3 class="singCourse_title"><span>CS:</span>	 -</h3>
					<h3 class="singCourse_title"><span>EC:</span> -</h3>
					<h3 class="singCourse_title"><span>TC:</span>- </h3>
					<h3 class="singCourse_title"><span>MECH:</span> 02</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 02</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<p>Total number of students placed : 209</p>
				    <p><span style="color:red;">*</span>S - ShortListed</p>
				
				
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                       <p>Department Wise Placement Details for 2009 Batch( From 1-9-2008 to 30-11-2009) </p>
                 </blockquote>
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year:</span> Feb 08</h3>
                    <h3 class="singCourse_title"><span>Company:</span> Keane</h3>
					<h3 class="singCourse_title"><span>Batch:</span>2009 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	21 </h3>
					<h3 class="singCourse_title"><span>EC:</span>13 </h3>
					<h3 class="singCourse_title"><span>TC:</span> 7</h3>
					<h3 class="singCourse_title"><span>MECH:</span>8 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>49 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year:</span>Feb 08</h3>
                    <h3 class="singCourse_title"><span>Company:</span>LandT Infotech </h3>
					<h3 class="singCourse_title"><span>Batch:</span>2009 </h3>
					<h3 class="singCourse_title"><span>CS:</span>	1 </h3>
					<h3 class="singCourse_title"><span>EC:</span>3 </h3>
					<h3 class="singCourse_title"><span>TC:</span>0 </h3>
					<h3 class="singCourse_title"><span>MECH:</span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span> 4</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year:</span>Feb 08 </h3>
                    <h3 class="singCourse_title"><span>Company:</span>Sonata SW </h3>
					<h3 class="singCourse_title"><span>Batch:</span> 2009</h3>
					<h3 class="singCourse_title"><span>CS:</span>	3 </h3>
					<h3 class="singCourse_title"><span>EC:</span>0 </h3>
					<h3 class="singCourse_title"><span>TC:</span>NA </h3>
					<h3 class="singCourse_title"><span>MECH:</span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed:</span>3 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Feb 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Infosys</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Tech Mahindra </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>4 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>4 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Mar 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>LandT Eng Services</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Subex</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 0</h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
	                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>May 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Perot Systems</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 1</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>May 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>SASKEN </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 0</h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Aug 08</h3>
                    <h3 class="singCourse_title"><span>Company: </span> Balley Technologies</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 0</h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Aug 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Convergent Tree </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Sep 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Maveric Systems</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Oct 08</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Cap Gemini </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>	Oct 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Aspiring Minds</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Nov 08</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Javagal Tooling Co </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Feb 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Assurgent </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	1 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 0</h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Feb 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>IBM BPO </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>	Feb 09</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Manipal E Commerce </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>	Feb 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>VIT Infotech </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> 	Indian Air force</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	4 </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span> 1</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>5 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> eLitmus</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Nanocell </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> CMC ( Tata )</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	7 </h3>
					<h3 class="singCourse_title"><span>EC: </span>6 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 5</h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 18</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> May 09</h3>
                    <h3 class="singCourse_title"><span>Company: </span>TWB </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	2 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 9</h3>
					<h3 class="singCourse_title"><span>TC: </span> 1</h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>12 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> May 09</h3>
                    <h3 class="singCourse_title"><span>Company: </span>NR Switch </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span> 7</h3>
					<h3 class="singCourse_title"><span>TC: </span> 5</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>12 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Jun 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Tayana Software </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Nov 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Honeywell</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	2 </h3>
					<h3 class="singCourse_title"><span>EC: </span>- </h3>
					<h3 class="singCourse_title"><span>TC: </span>- </h3>
					<h3 class="singCourse_title"><span>MECH: </span> -</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Nov 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Exilant </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	Result Not Reported </h3>
					<h3 class="singCourse_title"><span>EC: </span> Result Not Reported</h3>
					<h3 class="singCourse_title"><span>TC: </span> Result Not Reported</h3>
					<h3 class="singCourse_title"><span>MECH: </span> Result Not Reported</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Nov 09 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Microland </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>Result Not Reported	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>Result Not Reported </h3>
					<h3 class="singCourse_title"><span>TC: </span>Result Not Reported </h3>
					<h3 class="singCourse_title"><span>MECH: </span>Result Not Reported </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Jan 2010 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Accenture </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2009 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	2 </h3>
					<h3 class="singCourse_title"><span>EC: </span>- </h3>
					<h3 class="singCourse_title"><span>TC: </span>- </h3>
					<h3 class="singCourse_title"><span>MECH: </span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Jan 2010 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Igate</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2009</h3>
					<h3 class="singCourse_title"><span>CS: </span>1	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>- </h3>
					<h3 class="singCourse_title"><span>TC: </span>- </h3>
					<h3 class="singCourse_title"><span>MECH: </span>- </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<p>Total number of students placed : 113</p>
				    <p><span style="color:red;">*</span>S - ShortListed</p>
				
				
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                       <p>Department Wise Placement Details for 2008 Batch (From 1-3-2007 to 31-3-2008) </p>
                 </blockquote>
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Caritor </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>	14 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 5</h3>
					<h3 class="singCourse_title"><span>TC: </span> 5</h3>
					<h3 class="singCourse_title"><span>MECH: </span>5 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>29 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>	Mar 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Tech Mahindra </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span> 3 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 8 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 3 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 5 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 19 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Mar 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Perot Systems </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span> 4 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 4</h3>
					<h3 class="singCourse_title"><span>TC: </span> 0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 3 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 11 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Mar 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> LandT Infotech </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span> 2 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 2</h3>
					<h3 class="singCourse_title"><span>TC: </span> 1</h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 5 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Mar 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> iGATE </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span> 0	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 5 </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 5 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Mar 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>NIIT Technologies </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Satyam Computers </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>2 </h3>
					<h3 class="singCourse_title"><span>EC: </span>3 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 2</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>7 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Subex Systems </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>SonataSW </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span> 0</h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Sapient </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0</h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>iflex </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>1 </h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Siemens IS </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>0</h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Infosys </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>2 </h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 4</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Accord </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>May 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Celstream </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 1</h3>
					<h3 class="singCourse_title"><span>TC: </span> 1</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Jun 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> ADITI</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>2 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Aug 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Global Edge SW </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 0</h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Aug 07</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Bluestar Infotech </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	1 </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 1</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Aug 07 </h3>
          <h3 class="singCourse_title"><span>Company: </span>Aris Global </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Aug 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Kasura Tech </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2007/8</h3>
					<h3 class="singCourse_title"><span>CS: </span>1 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Sep 07</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Bistle Cone </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 1</h3>
					<h3 class="singCourse_title"><span>TC: </span> 1</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Sep 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Cap Gemini </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>2</h3>
					<h3 class="singCourse_title"><span>EC: </span>7 </h3>
					<h3 class="singCourse_title"><span>TC: </span>4 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 13</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Oct 07</h3>
                    <h3 class="singCourse_title"><span>Company: </span>JSW Steel Works </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>NA</h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Oct 07</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Covansys </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 0</h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Oct 07 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Microsoft</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0</h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Feb 2008</h3>
                    <h3 class="singCourse_title"><span>Company: </span>GXS </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Feb 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>eLitmus </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>NA</h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Triveni</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>NA</h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span>3 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>3 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> IAF</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0</h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Microsoft</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Mar 2008</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Elvista </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Lcube </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Infotech Enterprises </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>2 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>2 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Mar 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Wipro BPO </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span> 3</h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 4</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>7 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Apr 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Dell Fides </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>May 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Honeywell </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span>2 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>3 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>May 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Parak technologies </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span> 0</h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>May 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Robert Bosch </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Jul 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> IBM ISL</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>1 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Jul 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>SAP Labs </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA</h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Jul 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>IBM Inf </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>NA</h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Aug 2008</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Magna Infotech </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	1 </h3>
					<h3 class="singCourse_title"><span>EC: </span>4 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>5 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Aug 2008</h3>
                    <h3 class="singCourse_title"><span>Company: </span>Javagal tooling Co </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA </h3>
					<h3 class="singCourse_title"><span>EC: </span> NA</h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span>3 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 3</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Aug 2008 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Super val </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Sep 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Achir Communications </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span> 0</h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span> 0</h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Sep 08</h3>
                    <h3 class="singCourse_title"><span>Company: </span>IBM ISL </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>2	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>1 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>NA </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>3 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Oct 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>Potential technologies </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Nov 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>IBM BPO </h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>0	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Nov 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> HP INF</h3>
					<h3 class="singCourse_title"><span>Batch: </span> 2008</h3>
					<h3 class="singCourse_title"><span>CS: </span>NA	 </h3>
					<h3 class="singCourse_title"><span>EC: </span>2 </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span>1 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>3 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Nov 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span>IBM GSS </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>0 </h3>
					<h3 class="singCourse_title"><span>EC: </span>0 </h3>
					<h3 class="singCourse_title"><span>TC: </span>0 </h3>
					<h3 class="singCourse_title"><span>MECH: </span>0 </h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span>Dec 08 </h3>
                    <h3 class="singCourse_title"><span>Company: </span> Open Stream</h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 cl
					ass="singCourse_title"><span>CS: </span>1 </h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span> NA</h3>
					<h3 class="singCourse_title"><span>MECH: </span> NA</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>1 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
										
					<h3 class="singCourse_title"><span>Month & Year: </span> Dec 08</h3>
                    <h3 class="singCourse_title"><span>Company: </span>MHB Filters </h3>
					<h3 class="singCourse_title"><span>Batch: </span>2008 </h3>
					<h3 class="singCourse_title"><span>CS: </span>	NA</h3>
					<h3 class="singCourse_title"><span>EC: </span>NA </h3>
					<h3 class="singCourse_title"><span>TC: </span>NA </h3>
					<h3 class="singCourse_title"><span>MECH: </span> 0</h3>
					<h3 class="singCourse_title"><span>No. of Students Placed: </span>0 </h3>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<p>Total number of students placed : 137</p>
				    <p><span style="color:red;">*</span>S - ShortListed</p>
				
				
				 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>