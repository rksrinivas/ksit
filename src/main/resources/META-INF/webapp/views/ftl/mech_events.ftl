<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <h3>Mechanical Engineering Events</h3>
                    </blockquote>
                    
					
					 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                
                
                 <#list mechEventsList as mechEvent>
                    
					
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
                    
						<h2 class="singCourse_title">
						
								"${mechEvent.heading!}"
						
						</h2>
						
						<p>	 <img alt="img" src="${mechEvent.imageURL!}" class="media-object">	</p>
						
						
						<p>
						
							"${mechEvent.content!}"
						
						</p>
						
						
						<p>
						
							"${mechEvent.eventDate!}"
						
						</p>
						
                    </div>
                   
                  </div>
                  
                  </#list>
                  
                </div>
                <!-- End single course -->
				
					
					
					
					 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Redline racing</h2>
						<p>  The college SAE-Baja team - Redline racing of K S Institute of Technology had organized Autodynamics-2017, a three day workshop under the banner of SAEINDIA (Bengaluru section)and in association with Cadd Focus which attracted many automobile enthusiasts. Over 200 students from various engineering colleges across Karnataka attended the workshop that was conducted on 13th, 14th & 15th of October 2017 at KSIT Campus.</p>
						
					   <p>An inaugural function was held on first day, which was preceded by K V Murthy, the vice president of SAEINDIA Bengaluru section. The workshop, which was conducted across three days, included two sessions on each day. On the first day, session 1 was on Automobile basics followed by session 2 on automobile design philosophy and vehicle dynamics. Day 2 started with an industrial visit to Avasarala technologies pvt Ltd. and followed by hands-on session for dissembling and assembling an engine, gearbox then a visit to automobile laboratory. Third day session 1 was on designing and drafting of industrial components using solidworks and last session on introduction to FEA and its scope in automotive industries and concluded with a valedictory function</p>	
						
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
					
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference on Current Development and Its Applications in Mechanical Engineering</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\NCCDAME03.JPG" class="media-object"></p>
						
					   <p>NCCDAME</p>	
						
						<p><span class="feed_date">2017-05-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference on Current Development and Its Applications in Mechanical Engineering     </h2>
						<p>                          <img alt="img" src="img\mech\mech_events\NCCDAME05.JPG" class="media-object"></p>
						
					   <p>Inauguration of NCCDAME</p>	
						
						<p><span class="feed_date">2017-05-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference on Current Development and Its Applications in Mechanical Engineering</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\NCCDAME02.JPG" class="media-object"></p>
						
					   <p>Inauguration of NCCDAME</p>	
						
						<p><span class="feed_date">2017-05-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Ananya 2017</h2>
						<p><img alt="img" src="img\mech\mech_events\Ananya05-2017.JPG" class="media-object"></p>
						
						<p>Ananya 2017</p>
						
						<p><span class="feed_date">2017-04-07</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition 2017</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\Project4.JPG" class="media-object"></p>
						
						<p>project Exhibition 2017</p>
						
						<p><span class="feed_date">2017-05-12</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GO-KART 2017</h2>
						<p><img alt="img" src="img\mech\mech_events\IMG-Go -Kart-WA0002.JPG" class="media-object"></p>
						
						<p>National level Go-Kart computation held on Feb-2017 Under guidance of BALAJI B</p>
						
						<p><span class="feed_date">2017-02-23</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">UNVEILING OF BAJA CAR</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\Redline2.JPG" class="media-object"></p>
						
						<p>160 days of work, 27 members, 1 goal</p>
						
						<p><span class="feed_date">2017-02-06</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\CFD01.JPG" class="media-object"></p>
						
						   <p>Workshop on computational fluid dynamics for final year students of KSIT and KSSEM</p>
						
						<p>                       <span class="feed_date">2017-02-08</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">POLIO FREE WORLD RUN</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\20161118_124200.jpg" class="media-object"></p>
						
						   <p>ROTARY 10K RUN HAS BEEN ORGANISED FOR POLIO FREE WORLD ON 20 NOV 2016 AT 6 AM VENUE YMCA NRUPATUNGA ROAD BANGALORE 01 CONTACT 8861532581</p>
						
						<p>                       <span class="feed_date">2016-11-20</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop on "Non-Conventional Energy Resources and its Application for the Society"</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\BROUCHERweb.JPG" class="media-object"></p>
						
						   <p>The prime objective of workshop is to educate the participants with recent developments in Non Conventional Energy Resources and its Applications for the Society. scheduled from 20th-22nd Oct. 2016</p>
						
						<p>                       <span class="feed_date">2016-10-20</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">FAREWELL 2016</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\DSC_8289.jpg" class="media-object"></p>
						
					<p>FAREWELL WAS CONDUCTED FOR THE OUTGOING STUDENTS OF THE ACADEMIC YEAR 2015-2016 ON 21/05/2016</p>	
						
						<p>                          <span class="feed_date">2016-05-21</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PROJECT EXHIBITION 2016</h2>
						<p><img alt="img" src="img\mech\mech_events\PROJECT EXHIBITION.JPG" class="media-object"></p>
						
						<p>Our department has organised & invited experienced scholars from reputed institutions to judge the students & to give guidelines for students to improve their engineering skills.</p>
						
						<p>                          <span class="feed_date">2016-05-14</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Go - kart </h2>
						<p>  <img alt="img" src="img\mech\mech_events\kp.jpeg" class="media-object"></p>
						
						   <p>Fifth sem Mechanical Engg. Students are participating in national level Go-karting Competition. under the guidance of Mr. K Prasad</p>
						
						<p>                       <span class="feed_date">2016-02-01</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
              
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Gokarting</h2>
						<p><img alt="img" src="img\mech\mech_events\image1.JPG" class="media-object"></p>
						
						   <p>Fifth sem Mechanical Engg. Students are participating in national level Go-karting Competition. under the guidance of Mr. Balaji B</p>
						
						<p>                       <span class="feed_date">2016-02-01</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">SAE Redline Racing </h2>
						<p><img alt="img" src="img\mech\mech_events\2007.jpg" class="media-object"></p>
						
						   <p>Team redline racing from K.S.Institute of Technology,Bangalore has successfully designed and fabricated an off road buggy for a competition conducted by BAJA SAEINDIA - 2016</p>
						
						<p><span class="feed_date">2016-02-08</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">FDP on R & D Funding Opportunities and IPR</h2>
						<p><img alt="img" src="img\mech\mech_events\2006.jpg" class="media-object"></p>
						
						   <p>Three days Faculty Development Program on R And D funding opportunities and intellectual property rights FDP-2016 was successfully conducted at K.S.Institue of Technology. The FDP received an overwhelming response with 86 participants. The main aim of FDP was 1. Emphasize the need for research culture in teaching 2. to familiarize faculty with different funding opportunities 3. Educate faculty about research project proposal writing and methods to get project funding 4. to familiarize with the rules and laws of patenting 5. to discuss case studies of successful patenting procedure 6. to discuss tools and techniques to write a good research proposal or thesis or a paper.</p>
						
						<p><span class="feed_date">  2016-03-28</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk on Scope in Automotive Styling </h2>
						<p><img alt="img" src="img\mech\mech_events\" class="media-object"></p>
						
						   <p>Technical Talk on Scope in Automotive Styling by Mr. M. Sugumaran, Business Manager, Institute of Industrial Design, Chennai</p>
						
						<p>                       <span class="feed_date">2016-02-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk on App Design Prototyping Apple</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\" class="media-object"></p>
						
						<p>Technical Talk on App Design Prototyping Apple by Mr. Mohit R Hegde, Director, Creative INFOTECH, Authorised Training Centre, Bengaluru</p>
						
						<p><span class="feed_date">2016-02-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition   </h2>
						<p>                          <img alt="img" src="img\mech\mech_events\" class="media-object"></p>
						
						   <p>Exhibition: Mechanical (Exclusive) 22 Team of all semester participated, KSIT. Judge by BVS Murthy, Dr Girish K Ramaiah, MMM Patnaik</p>
						
						<p>                       <span class="feed_date">2015-09-17</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk: Fabrication of BAJA off road vehicle</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\2004.jpg" class="media-object"></p>
						
					   <p>Technical Talk: Fabrication of BAJA off road vehicle by Purushotham, SAE Co-ordinator</p>	
						
						<p><span class="feed_date">2015-09-17</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar on car Design </h2>
						<p> <img alt="img" src="img\mech\mech_events\2003.jpg" class="media-object"></p>
						
						<p>Seminar on car Design by speaker Sugath Das, Country manager, IED (Institute Europeo Di Design)</p>
						
						<p><span class="feed_date"> 2015-09-30</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Seminar on Introduction Session- FEA & CFD</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\2002.jpg" class="media-object"></p>
						
					   <p>Seminar on Introduction Session- FEA & CFD by Sathyak Sunder Padhy,Sugath Das, Technology Need Manager</p>	
						
						<p><span class="feed_date">2015-09-11</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">> WORK SHOP on Automobile Basic & Vehicle Dynamics</h2>
						<p>  <img alt="img" src="img\mech\mech_events\2001.jpg" class="media-object"></p>
						
						<p>WORK SHOP on Automobile Basic & Vehicle Dynamics by the speaker:Kushal Singh bithu, Formal Marketing & Research associate, Elite techno groups & SAE</p>
						
						<p>                          <span class="feed_date"> 2015-04-10</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Five Day Faculty Development Programme </h2>
						<p><img alt="img" src="img\mech\mech_events\" class="media-object"></p>
						
						   <p>5 Day Faculty Development Programme on Ansys Solutions For General Mechanical Engineering Problems by Dr.Ganapathi Manickam Vice President Head design Validation Engg Solutions Tech Mahindra Bangalore</p>
						
						<p><span class="feed_date">2015-02-23</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Three Day Faculty Development Programme</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\2011.jpg" class="media-object"></p>
						
						<p>Faculty Development Programme on Design Engineering lab I</p>
						
						<p>                          <span class="feed_date"> 2014-10-16</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk</h2>
						<p> <img alt="img" src="img\mech\mech_events\2012.jpg" class="media-object"></p>
						
						   <p>Technical Talk on Stem Robotics Aeromodelling Renewable Energy by Mr. Mohan Nimbalkar Zonal Manager K-12 Solutions Procyon Infotech Ptv Ltd Bangalore.</p>
						
						<p>                       <span class="feed_date">2014-09-09</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop</h2>
						<p><img alt="img" src="img\mech\mech_events\IMG_0183.JPG" class="media-object"></p>
						
						<p>Workshop on Glider Modelling conducted under the banner S A E club under the guidance Mr. Kumar V, Design Engineer, QuEST Global.</p>
						
						<p><span class="feed_date">2014-04-13</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Sketching Competition</h2>
						<p>                          <img alt="img" src="img\mech\mech_events\IMG_0045.JPG" class="media-object"></p>
						
						   <p>Sketching Competition under S A E Club for motivating students to actively participate in club. Dr. H K srinivas, YDIT, Bangalore was invited as Judge for the competition.</p>
						
						<p><span class="feed_date">2014-04-04</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Launch of News Letter EMANATION</h2>
						<p><img alt="img" src="img\mech\mech_events\2010.jpg" class="media-object"></p>
						
						<p>The department as launched News letter EMANATION on March 17th 2014, which provides information about the progress, recent achievements and important forthcoming activities of the department.</p>
						
						<p><span class="feed_date"> 2014-03-17</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture </h2>
						<p>                          <img alt="img" src="img\mech\mech_events\spk.jpg" class="media-object"></p>
						
						<p>1.Guest Lecture on "Challenges of Gas Turbine Engines "By R.K. Mishra, (DRDO),B'lore.</p>
						
						<p><span class="feed_date"> 2013-02-19</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Guest Lecture</h2>
						<p><img alt="img" src="img\mech\mech_events\DSC_0283.JPG" class="media-object"></p>
						
						<p>"Energy Systems"By Dr. B.V.Anand of Allianz University,Bangalore.</p>
						
						<p>                          <span class="feed_date">2012-11-19</span></p>
						
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				
				
							
				
				
				 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>