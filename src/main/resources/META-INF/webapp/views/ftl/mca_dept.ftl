<@page>
   
 <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about">
	<div class="container" >
	<div class="news_area home-4">
		<div class="row">
			<div class="col-md-12">
			<h1 class="titile text-center">Welcome to Master Of Computer Applications</h1>
			<p class="welcome-text text-center"><b>Imparting quality technical education with ethical values, employable skills and research to achieve excellence.</b></p>
				<div class=" col-lg-7 col-md-6 col-sm-12">
					<div class="aboutus_area wow fadeInLeft">
						<p>Welcome to the Master of Computer Applications (MCA) program at KSIT, a place where aspiring tech enthusiasts transform into industry-ready professionals. Our MCA program is more than just a degree - it's a journey that blends knowledge, innovation, and practical experience to prepare students for the ever-evolving IT industry. </p>
						<p>The MCA curriculum at KSIT is meticulously designed to cover the latest trends and technologies, including software development, data analytics, artificial intelligence, machine learning, cybersecurity, and cloud computing. Beyond classroom learning, students actively participate in hands-on projects, workshops, and industry collaborations, ensuring they gain real-world insights and problem-solving skills.</p>				
						<p>Our dedicated and experienced faculty members not only deliver world-class education but also mentor students to unlock their full potential. The program is supported by state-of-the-art infrastructure, including advanced labs and high-performance computing resources, which provide the perfect platform for innovation and experimentation.</p>
						<p>At KSIT, we strongly believe in holistic development. Through extracurricular activities, leadership programs, and industry interactions, students are encouraged to build their technical expertise, communication skills, and confidence. With opportunities to work on live projects, internships, and hackathons, our MCA graduates leave with more than just a degree - they gain the experience, exposure, and skills required to excel in the IT sector.</p>
						

					</div>
				</div>

				<div class="col-lg-5 col-md-6 col-sm-12">         	      
            		<img class="home-img img-responsive" src="${img_path!}/mca/mca_admissions_banner.jpeg" alt="image" />	
				</div>
				<div class=" col-lg-12 col-md-12 col-sm-12">
          			<div class="aboutus_area wow fadeInLeft">
						
					<p>Join the MCA program at KSIT and take the first step towards a future where you not only adapt to change but lead it. Be part of a vibrant community where learning meets innovation, and dreams turn into reality.</p>	
					  <p><a class="btn btn-primary" href="coursesOffered.html" target="_blank">Apply Now</a></p>	
						
					</div>
				</div>
			
        </div>
		</div>
		</div>
	</section>
    <!--=========== END ABOUT US SECTION ================--> 		   
							

			
			  
			  
             
			  
			  
             

	
	
	
</@page>