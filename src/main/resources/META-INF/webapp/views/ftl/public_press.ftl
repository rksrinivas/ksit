<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <div class="row">
              
              <!-- start single course -->
              <#list publicPressList as publicPress>
              
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="single_course wow fadeInUp">
                    <div class="singCourse_imgarea">
                      <img src="${publicPress.imageURL!}" />
                      <div class="mask">  
						<p>${publicPress.content!}</p>					  
                        	
						</a>
                      </div>
                    </div>
                    
					<div class="singCourse_content">
                    <h3 class="singCourse_title">${publicPress.heading!} [${publicPress.eventDate!}]</h3>
                    
                    </div>
					
                   </div>
                </div>
                
                </#list>
                <!-- End single course -->
              
			  
                <!-- start single course -->
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="single_course wow fadeInUp">
                    <div class="singCourse_imgarea">
                      <img src="img/press/scan.jpg" />
                      <div class="mask">  
						<p>The inauguration ceremony of the 18th batch of first year.</p>					  
                        <!--<a href="course-single.html" class="course_more">View-->
						</a>
                      </div>
                    </div>
                    
					<div class="singCourse_content">
                    <h3 class="singCourse_title">The inauguration ceremony</h3>
                    
                    </div>
					
                   </div>
                </div>
                <!-- End single course -->
				
				<!-- start single course -->
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="single_course wow fadeInUp">
                    <div class="singCourse_imgarea">
                      <img src="img/press/campus.jpg" />
                      <div class="mask">  
											  
                        <!--<a href="course-single.html" class="course_more">View-->
						</a>
                      </div>
                    </div>
                    
					<div class="singCourse_content">
                    <h3 class="singCourse_title">Campus Capsule</h3>
                    
                    </div>
					
                   </div>
                </div>
                <!-- End single course -->
				
				<!-- start single course -->
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="single_course wow fadeInUp">
                    <div class="singCourse_imgarea">
                      <img src="img/press/device.jpg" />
                      <div class="mask">  
							<p>Engineering students devise kit to rescue babies from borewells</p>				  
                        <!--<a href="course-single.html" class="course_more">View-->
						</a>
                      </div>
                    </div>
                    
					<div class="singCourse_content">
                    <h3 class="singCourse_title">Device Kit</h3>
                    
                    </div>
					
                   </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="single_course wow fadeInUp">
                    <div class="singCourse_imgarea">
                      <img src="img/press/color.jpg" />
                      <div class="mask">  
											  
                        <!--<a href="course-single.html" class="course_more">View-->
						</a>
                      </div>
                    </div>
                    
					<div class="singCourse_content">
                    <h3 class="singCourse_title">Education Corner</h3>
                    
                    </div>
					
                   </div>
                </div>
                <!-- End single course -->
				
				
                
              </div>
              
             
            </div>
          </div>
          <!-- End course content -->

                </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>