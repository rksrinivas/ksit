<@page>
	   
     
    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3 class="text-center">Clubs</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote>
                    
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
					
					<p>S A E INDIA KSIT COLLEGIATE CLUB for more information please visit <br/><a href="https://www.facebook.com/ksitsaeindia" target="_blank">https://www.facebook.com/ksitsaeindia</a></p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
			
			 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
					
					<p>Equinox at KSIT <br/><a href="https://www.facebook.com/EquinoxAtKSIT/" target="_blank">https://www.facebook.com/EquinoxAtKSIT/</a></p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
			
			
			 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
					<h3>&#9656; YOUTH RED CROSS(YRC) KSIT</h3>
						<p><br/><a href="#" target="_blank"></a></p>
						<p >"The youth Red Cross " is the most important constituent of its mother organization,Indian Red Cross Society.It is a group moment organized for students in colleges.</p>
					
					<h3>&#9656; AIMS OF YOUTH RED CROSS</h3>
						<p>The youth Red Cross is organized - for the purpose of inculcation in Indian Youth the ideal and practice of service especially in relation to</p>
						
						<ul class="list list_color">
						
							<li>Taking care of their own health and that of others</li>
							
							<li>Understanding and accepting of civic responsibility</li>
							
							<li>Maintaining a spirit of friendliness and helpfulness towards other youths .</li>
							
						
						</ul>
						
						<h3>&#9656; YRC PLEDGE</h3>
						<p>" I pledge myself to take care of my own health and that of others,to help the sick and suffering,especially children and to look up on all over the world as my friends"</p>
						
						<h3>&#9656; YRC ACTIVITIES</h3>
						
						<ul class="list list_color">
						
							<li>Promotion of Health & Hygiene</li>
							
							<li>AIDS awareness</li>
							
							<li>National Integration</li>
							
							<li>Blood Donation Camp</li>
							
							<li>Community Service</li>
							
							<li>Literacy Campaign</li>
							
							<li>Relief work during emergencies like flood,fire and other natural calamities.</li>
							
						
						</ul>
						
						<p>YRC KSIT  organized Blood Donation Camp in association with  India Red Cross Society and Rotary Bengaluru for the academic year 2016-2017.</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->

            </div>
          </div>
          <!-- End course content -->

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>