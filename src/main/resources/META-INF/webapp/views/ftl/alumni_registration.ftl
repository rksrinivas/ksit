<@page>
 <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="alumni">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Alumni Registration Form</h2>
              <span></span> 
              
            </div>
          </div>
       </div>
       <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
           <div class="alumni_form wow fadeInLeft">
		   
			<!--<h3>Personal Details</h3>-->
				<form action="" class="submitphoto_form" method="post">
			
			  
				 <div class="col-lg-6 col-md-6 col-sm-12">
					
               <input type="text" class="wp-form-control wpcf7-text" name="name" placeholder="Your name" required>
								
				<select class="wp-form-control wpcf7-text" name="course" required>
				  <option>Select Course</option>
				  <option value="BE">BE</option>
				  <option value="M.Tech">M.tech</option>				  				  
				</select>
				
				  <select class="wp-form-control wpcf7-text" name="branch" required>
				  <option>Select Branch</option>
				  <option value="CSE">CSE</option>
				  <option value="ECE">ECE</option>
				  <option value="ME">ME</option>
				  <option value="TCE">TCE</option>
				  <option value="M.Tech">M.Tech(Computer Science)</option>
				  <option value="M.Tech">M.Tech(Machine design)</option>
				  <option value="M.Tech">M.Tech(Digital Electronics & Communication)</option>

				</select>
				
				<input type="text" pattern="^1ks[0-9]{2}[a-z]{2}[0-9]{3}$" placeholder="Enter USN" class="wp-form-control wpcf7-text" name="usn" placeholder="Your USN" required>
				
				<input type="year" placeholder="Year of Passing" class="wp-form-control wpcf7-text" name="yearOfPassing" placeholder="Enter the year of graduation" required >
								
				
				<input type="number" placeholder="Enter Mobile Number" class="wp-form-control wpcf7-text" name="mobileNumber" placeholder="Enter the Mobile number" required>
				
                <input type="mail" placeholder="Enter email" class="wp-form-control wpcf7-email" name="emailId" placeholder="Email address" required> 
				
               			
				
				</div>
				
				<!--<h3>Career and Employment</h3>-->
				
				 <div class="col-lg-6 col-md-6 col-sm-12">
					
				
				<input type="text" class="wp-form-control wpcf7-text" name="companyName" placeholder="Company / Organization name" required>
				
				<textarea class="wp-form-control wpcf7-textarea" cols="30" rows="10" name="careerDetails" placeholder="Career Details"></textarea>
				
                <input type="submit" value="Submit" class="wpcf7-submit" name="submit_button">
				
				</div>
				
              </form>
           </div>
         </div>
		 
		 
		<!--<div class="col-lg-6 col-md-6 col-sm-12">
           <div class="contact_address wow fadeInRight">
             <h3>Address</h3>
             <div class="address_group">
               <p>200 Lincoln Ave, Salinas, CA 93901</p>
               <p>Phone: 662-807-9585</p>
               <p>Email:contact@wpfdegree.com</p>
             </div>
             <div class="address_group">
              <ul class="footer_social">
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>
                </ul>
             </div>
           </div>
         </div>-->
		 
		 
       </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->
</@page>