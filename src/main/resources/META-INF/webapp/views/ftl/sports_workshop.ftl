<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <!--<section id="imgBanner_cse">
      <h2></h2>
    </section>-->
    <!--=========== END COURSE BANNER SECTION ================-->
	
	
	 <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
         
		<h2 class="titile">Sports</h2>
          <!-- start course archive sidebar -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_sidebar">
			
              <!-- start single sidebar -->
              <div class="single_sidebar">
			  
                <h2> Conference, Workshops, Seminars<span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
				
				
                   <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>National Seminar on Fitness and Well-Being through Physical Activity * Government College for Women *Mandya* on 20th November 2008</h4>
					   <p>UGC Sponsored National level Conference at the Department of Physical Education.</p>
                       
                      </div>
                    </div>
                  </li>  
				  
				  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4> Seminar on Introduction of physical education as a compulsory subject in primary and secondary schools in Karnataka.YMCA College of physical education .Bangalore. On 27th &28th November 2008.</h4>
					   <p></p>
                       
                      </div>
                    </div>
                  </li>  
				  
				  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>International Conference on Talent Identification and Development in Physical Education and Sports * Mangalore University *Mangalore * on 29th &30th January 2010.</h4>
					   <p>UGC-SAP Sponsored International Level Seminar Jointly organized by College of Physical Education Teachers Association and Brahmavara Sports Club.</p>
                       
                      </div>
                    </div>
                  </li>  
				  
				  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>Emergence of Excellence in indigenous Sports at Grass Root Level * Department of Physical Education *Gulbarga University* on 29th &30th January 2011</h4>
					   <p>UGC-SAP Sponsored National Level Seminar.</p>
                       
                      </div>
                    </div>
                  </li>  
				  
				  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>Current Trends in Physical Education and Sports Science * Government First Grade College *Mandya* 13th & 14th  May 2011</h4>
					   <p>UGC Sponsored National level Conference at the Department of Physical Education.</p>
                       
                      </div>
                    </div>
                  </li>  
				  
				  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>National Seminar on Role of Colleges and Universities of Sports in India * Government First Grade College *Bangalore * on 18th & 19th March 2011</h4>
					   <p>UGC Sponsored National Level Seminar at the Department of Physical Education.</p>
                       
                      </div>
                    </div>
                  </li>  
								  
                </ul>
				

				
              </div>
              <!-- End single sidebar -->
			  </div>
			  </div>
			  
			  
		<div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_sidebar">
			   <!-- start single sidebar -->
              <div class="single_sidebar">
			  
                <h2>Esteemed Positions Held<span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
				
				
                   <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>Working President of Karnataka State Physical Education Graduates Association * Karnataka *Bangalore * May 2010</h4>
					   <p>Karnataka State Physical Education Graduates Association is a consortium of Physical Education Graduates and conducts Sports Events and Re-Creation Camps all over the state.</p>
                       
                      </div>
                    </div>
                  </li> 
				  
				  
				   <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>Member of National Association of Physical Education and Sports Sciences * NAPESS* India *May 2015</h4>
					   <p>Karnataka State Physical Education Graduates Association is a consortium of Physical Education Graduates and conducts Sports Events and Re-Creation Camps all over the state.</p>
                       
                      </div>
                    </div>
                  </li> 
				  
				   <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <h4>Qualified State Referee in Sports (Throw ball) * Karnataka *Bangalore *</h4>
					   <p>I had the honor to be the referee in several University, State and National Level Sporting events</p>
                       
                      </div>
                    </div>
                  </li> 

				</ul>
			  </div>	
			 
			  
			  
			 
             
			  
            </div>
          </div>
          <!-- start course archive sidebar -->
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>