<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			<!--<h3>Site Map</h3>-->
              <div class="row">
			  
					
			  
                <!-- start single course -->
                <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2>Menu</h2>
                    <h3 class="singCourse_title"><a href="index.html" target="_blank">Home</a></h3>
					<h3 class="singCourse_title"><a href="about.html" target="_blank">About</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">Careers</a></h3>
					
					<h3 class="singCourse_title"><a href="contact.html" target="_blank">Contact</a></h3>
					
					<h3 class="singCourse_title"><a href="http://ksit.pupilpod.in/" target="_blank">Login</a></h3>
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
							
				
				
				
                <div class="col-lg-3 col-md-3 col-sm-3">
				
					
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2>Courses</h2>
                    <h3 class="singCourse_title"><a href="cse_dept.html" target="_blank">Computer Science</a></h3>
					<h3 class="singCourse_title"><a href="ece_dept.html" target="_blank">Electronics and Communication</a></h3>
					<h3 class="singCourse_title"><a href="mech_dept.html" target="_blank">Mechanical</a></h3>
					<h3 class="singCourse_title"><a href="tele_dept.html" target="_blank">Telecommunication</a></h3>
					<h3 class="singCourse_title"><a href="science_dept.html" target="_blank">Science and Humanities</a></h3>
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2>Facilities</h2>
					
					<h3 class="singCourse_title"><a href="library" target="_blank">Library</a></h3>
					
                    <h3 class="singCourse_title"><a href="hostel.html" target="_blank">Hostel</a></h3>
					
					<h3 class="singCourse_title"><a href="transport.html" target="_blank">Transportation</a></h3>
					
					<h3 class="singCourse_title"><a href="sports.html" target="_blank">Sports</a></h3>
					
					<h3 class="singCourse_title"><a href="#" target="_blank">...</a></h3>
					
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
                
				<div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2>Calendar</h2>
                    <h3 class="singCourse_title"><a href="#" target="_blank">Academic Calender</a></h3>
					
					<h3 class="singCourse_title"><a href="#" target="_blank">Exam Time-table</a></h3>
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2></h2>
                    <h3 class="singCourse_title"><a href="#" target="_blank">Anti - Ragging Measures</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">Enquiry</a></h3>
					
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				<div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2></h2>
                    <h3 class="singCourse_title"><a href="#" target="_blank">AICTE</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">VTU</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">Ananya KSIT</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">Sitemap</a></h3>
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				<div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2></h2>
                    <h3 class="singCourse_title"><a href="#" target="_blank">Email</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">Online Fee Payment</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">Public Relations and Press</a></h3>
					<h3 class="singCourse_title"><a href="#" target="_blank">Grievance Redressal Cell</a></h3>
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				
				<div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2>Gallery</h2>
                    <h3 class="singCourse_title"><a href="#" target="_blank">Computer Science</a></h3>
					
					<h3 class="singCourse_title"><a href="#" target="_blank">Electronics</a></h3>
					
					<h3 class="singCourse_title"><a href="#" target="_blank">Mechanical</a></h3>
					
					<h3 class="singCourse_title"><a href="#" target="_blank">Telecommunication</a></h3>
					
					<h3 class="singCourse_title"><a href="#" target="_blank">Science And Humanities</a></h3>
					
					<h3 class="singCourse_title"><a href="#" target="_blank">Sports</a></h3>
                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->            


              </div>
              
            </div>
          </div>
          <!-- End course content -->


        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>