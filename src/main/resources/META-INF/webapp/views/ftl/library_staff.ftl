<@page>
	 
	 <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs_course">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
         
		 
		 <div class="newsfeed_area wow fadeInRight">
            <ul class="nav nav-tabs feed_tabs" id="myTab2">
              <li class="active"><a href="#news" data-toggle="tab">Message From Chief Librarian</a></li>
              <!--<li><a href="#notice" data-toggle="tab">Course Content</a></li>
              <li><a href="#events" data-toggle="tab">Research & Development</a></li> -->        
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
			
			
              <!-- Start news tab content -->
              <div class="tab-pane fade in active" id="news">                
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="">
								<img class="img-responsive" src="${img_path!}/library/library_staff/library.jpg" alt="image" />
							</div>
							</div>
						
					   <div class="col-lg-6 col-md-6 col-sm-6">
					   <p id="news"> </p>
					   <p class="media_body_content"> We are dedicated to delivering high quality, innovative information services, which contribute to the successful learning, teaching and research of the institute and the community. </p>
						<p class="media_body_content">We try to follow the standards in keeping our library with good facilities, full of books of repute and provide E-learning through           e-books and e-journals. We provide our students with the resources and study environment they need, to support them during the course of their studies.   </p>
						
						<p class="media_body_content">I Welcome you all to step into our library and explore the intellectual resources. </p>
						
						</div>
						
						</div>
                    </div>                    
                  </li>
                  
                  
                </ul>                
               
              </div>
			  
			  
              <!-- Start notice tab content -->  
              <div class="tab-pane fade " id="notice">
                <div class="single_notice_pane">
                  <ul class="news_tab">
                    <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="${img_path!}/about/sangham.jpg" alt="image" />
								</div>
							</div>
						
							<div class="col-lg-6 col-md-6 col-sm-6">
								<p id="news"></p>
								<p>course content here</p>
								<p></p>
							</div>
						
						</div>
                    </div>                    
                  </li>                         
                  </ul>
                  
                </div>               
              </div>
			  
			  
              <!-- Start events tab content -->
              <div class="tab-pane fade " id="events">
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="${img_path!}/about/sangham.jpg" alt="image" />
								</div>
							</div>
						
							<div class="col-lg-6 col-md-6 col-sm-6">
								<p id="news"></p>
								<p>R& D here</p>
								<p></p>
							</div>
						
						</div>
                    </div>                    
                  </li>      
                </ul>
                
              </div>
			  
			  
			  
            </div>
          </div>
        </div>
		 
		 
		 
        </div>
        
	  
			   
          </div>
        </div>
	  
	 
	
	
	  
	  </div>
	  </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 

	 <!--=========== BEGIN WHY US SECTION ================-->
    <section id="whyUs_dept">
      <!-- Start why us top -->
      <div class="row">        
        <div class="col-lg-12 col-sm-12">
          <div class="whyus_top">
            <div class="container">
              <!-- Why us top titile -->
              <div class="row">
                <div class="col-lg-12 col-md-12"> 
                  <div class="title_area">
                    <h2 class="title_two">Staff</h2>
                    <span></span> 
                  </div>
                </div>
              </div>
              <!-- End Why us top titile -->
              <!-- Start Why us top content  -->
              <div class="row">
               
                  
				  
				  
	<!-- faculty -->			  
				  
	<div class="teacher_area home-2">
		<div class="container">
			<div class="row">		
			
					  
			<!--faculty-->
				<#list facultyList as faculty>
			<div class="col-md-3 col-lg-3 col-sm-6">
				<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${faculty.imageURL!}" alt="" />
							<div class="thumb_text">
								<h2>${faculty.name}</h2>
								<p>${faculty.designation}</p>
							</div>
						</div>
						<div class="teacher_content">
							<h2>${faculty.name}</h2>
							<span>${faculty.designation}</span>
							<p>${faculty.qualification}</p>
							<span>${faculty.department}</span>
							<p><a href=""${faculty.profileURL!}" target="_blank">view profile</a></p>
							<!-- <div class="social_icons">
                                          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                                          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                                          <a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
                                          <a href="#" class="go"><i class="fa fa-google-plus"></i></a>
                              </div>-->
						</div>
					</div>
				</div>
				
				</#list>
			
			<!--end-->  
		
				<!--start teacher single  item -->
				<div class="col-md-12 col-lg-12 col-sm-12">
					<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${img_path!}/library/bharati.jpg" alt="" />
							<div class="thumb_text">
								<h2>Dr. V. Bharathi</h2>
								<p>Chief Librarian</p>
							</div>
						</div>
						<div class="teacher_content">
							<h2>Dr. V. Bharathi</h2>
							<span>Chief Librarian</span>
							<p>M.L.I.Sc, M.Phil, Ph.D</p>
							<span>bharathivirla@yahoo.co.in</span>
							<!-- <div class="social_icons">
                                          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                                          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                                          <a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
                                          <a href="#" class="go"><i class="fa fa-google-plus"></i></a>
                             </div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
				
				<!-- teachers details-->
				
				
				<!--start ads  area -->
	<div class="teacher_area home-2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-12">
					<div class="title">
						<h3 class="module-title">
							   <span></span>
						</h3>
					</div>
				</div>
			</div>	
		</div>	
		
		<div class="container">
			<div class="row">
				<!--start teacher single  item -->
				<div class="col-md-3 col-lg-3 col-sm-6">
					<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${img_path!}/library/vasantha.jpg" alt="" />
							<div class="thumb_text">
								<h2>M. Vasantha</h2>
								<p>Assistant Librarian</p>
							</div>
						</div>
						<div class="teacher_content">
							<h3>M. Vasantha</h3>
							<span>Assistant Librarian</span>
							<p>M. L.I.Sc.</p>
							<span>vasanthabalaji2010@gmail.com</span>
							<!-- <div class="social_icons">
                                          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                                          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                                          <a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
                                          <a href="#" class="go"><i class="fa fa-google-plus"></i></a>
                             </div>-->
						</div>
					</div>
				</div>
				<!--End teacher single  item -->
				<!--start teacher single  item -->
				<div class="col-md-3 col-lg-3 col-sm-6">
					<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${img_path!}/library/kiran.jpg" alt="" />
							<div class="thumb_text">
								<h2>G. Kiran Kumar</h2>
								<p>Technician</p>
							</div>
						</div>
						<div class="teacher_content">
							<h3>G. Kiran Kumar</h3>
							<span>Technician</span>
							<p>D. C.S.</p>
							<p>kirangksit@gmail.com</p>
							<!-- <div class="social_icons">
                                          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                                          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                                          <a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
                                          <a href="#" class="go"><i class="fa fa-google-plus"></i></a>
                             </div>-->
						</div>
					</div>
				</div>
				<!--End teacher single  item -->
				<!--start teacher single  item -->
				<div class="col-md-3 col-lg-3 col-sm-6">
					<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${img_path!}/library/venugopal.jpg" alt="" />
							<div class="thumb_text">
								<h2>S. Venugopal</h2>
								<p>Attender</p>
							</div>
						</div>
						<div class="teacher_content">
							<h3>S. Venugopal Naidu</h3>
							<span>Attender</span>
							<p>S.S.L.C.</p>
							<p></p>
							<!-- <div class="social_icons">
                                          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                                          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                                          <a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
                                          <a href="#" class="go"><i class="fa fa-google-plus"></i></a>
                            </div>-->
						</div>
					</div>
				</div>
				<!--End teacher single  item -->
				<!--start teacher single  item -->
				<div class="col-md-3 col-lg-3 col-sm-6">
					<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${img_path!}/library/rekha.jpg" alt="" />
							<div class="thumb_text">
								<h2>S. Rekha</h2>
								<p>	Attender</p>
							</div>
						</div>
						<div class="teacher_content">
							<h3>S. Rekha</h3>
							<span>Attender</span>
							<p>S.S.L.C.	</p>
							<p></p>
							<!-- <div class="social_icons">
                                          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                                          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                                          <a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
                                          <a href="#" class="go"><i class="fa fa-google-plus"></i></a>
                             </div>-->
						</div>
					</div>
				</div>
				<!--End teacher single  item -->
				
				
			</div>
		</div>
	</div>	
	<!--end teacher  area -->
				
				
				
				
				<!-- end of teacher details-->
				
				
              </div>
              <!-- End Why us top content  -->
            </div>
          </div>
        </div>        
      </div>
      <!-- End why us top -->

     
				

				
              </div>
            </div>            
          </div>
        </div>        
      </div>
      <!-- End why us bottom -->
    </section>
    <!--=========== END WHY US SECTION ================-->
	
	 
	 
</@page>