<@page>
<!--=========== BEGIN COURSE BANNER SECTION ================-->
	<section id="courseArchive">
		<div class="container">
		<div class="row">
			<!-- start course content -->
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="courseArchive_content">
					<div class="row">
						<h4 class="text-center titile">Fee Structure</h4>
						<blockquote>
							<span class="fa fa-quote-left"></span>
						</blockquote>
						<!-- start single course -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="single_course wow fadeInUp">
								<div class="singCourse_content">
									<#list feeStructureList as feeStructure>
										<p>${feeStructure.heading!} <br/><a href="${feeStructure.imageURL!}" target="_blank"> click here to download Fee Structure</a> </p>
									</#list>
								</div>
							</div>
						</div>
						<!-- End single course -->
					</div>
				</div>
				<!-- End course content -->
			</div>
		</div>

		  <div class="col-lg-12 col-md-6 col-sm-12">
		   	<div class="aboutus_area wow fadeInLeft">
			<h4 class="titile">Bank Account Details :</h4>	
          <p>K. S. INSTITUTE OF TECHNOLOGY, BANGALORE</p>
          <P>AXIS BANK, JP NAGAR BRANCH, BANGALORE</P>
          <P>SB A/C NO. 912010014093916</P>
          <P>IFS CODE NO. UTIB0001513</P>
			</div>
			</div>
	</section>
<!--=========== END COURSE BANNER SECTION ================-->
</@page>