<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3 class="text-center">Recruitment Partners</h3>
					 <!-- <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>SALARY PAY PACKAGE AS ON 24/04/2016</p>
                    </blockquote>-->
                    
			  	
			  	 <!-- start single course -->

			  	 	<#list placementCompanyList as placementCompany>
		                <div class="col-lg-6 col-md-12 col-sm-12">
		                
		                  <div class="single_course wow fadeInUp single_course_placement">
		                    
		                    <div class="singCourse_content">
							
		                    <h3 class="singCourse_title"><span>Company : </span>${placementCompany.company!}</h3>
							<h3 class="singCourse_title"><img src="${placementCompany.imageURL!}" alt="img" /></h3>
							<h3 class="singCourse_title"><span>Salary Package : </span>${placementCompany.salary!}</h3>
							<h3 class="singCourse_title"><span>Number of Students placed : </span>${placementCompany.noOfStudents!}</h3>
							                    
		                    </div>
		                   
		                  </div>
		                </div>
                 	</#list>

                               
			  
			  
                <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>WEST LINE SHIPPING PVT LTD</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/26.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>24 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>20</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>INDIAN NAVY</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/18.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>16 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>26 (S)</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Phoenix Sea Services Pvt.Ltd</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/22.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>16 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>5</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>ALPHA 9 MARINE</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/9.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>12 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>44</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Huawei Technologies</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/16.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>6.80 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Hero MotoCorp</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/15.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>5.5 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>CEASE FIRE</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/12.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>4 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>2</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>HP (ENT)</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/3.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.60 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>4</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>V-TIGER</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/25.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.50 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>2</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>VIRTUSA</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/24.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.40 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>COGNIZANT</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/13.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.35 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>4</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>INFOSYS</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/2.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.25 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>40</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Tech Mahindra</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/5.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.25 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>27</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>CAPGEMINI</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/11.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.15 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>18</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>NTT DATA</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/21.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.00 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>19</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>L&T Infotech</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/4.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.00 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>20</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>AMAZON</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/10.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.00 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>4</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>IBM</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/17.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.60 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>11</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
									
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>6D Technology</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/1.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.60 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1 (S)</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>M phasis</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/20.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.50 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>2</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>HP (INC)</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/3.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.40 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>5</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>ICTI</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/19.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.10 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>HCL TALENT CARE</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/14.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.00 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>9</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Rothenberger</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/23.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.00 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1 (S)</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>SISNETIC CLOUD SOLUTION PVT LTD</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/7.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.00 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>2 (S)</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>ADECCO</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/8.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>2.00 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>3 (S)</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>HIGHEST PAY PACKAGE DETAILS TILL:27/04/2016</p>
                    </blockquote>
				 
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>WEST LINE SHIPPING PVT LTD</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/26.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>24 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>20</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>INDIAN NAVY</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/18.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>16 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>26 (S)</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Phoenix Sea Services Pvt.Ltd</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/22.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>16 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>5</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>ALPHA 9 MARINE</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/9.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>12 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>44</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Huawei Technologies</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/16.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>6.80 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>Hero MotoCorp</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/15.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>5.5 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>CEASE FIRE</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/12.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>4 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>2</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>HP (ENT)</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/3.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.60 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>4</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>V-TIGER</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/25.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.50 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>2</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Company : </span>VIRTUSA</h3>
					<h3 class="singCourse_title"><img src="${img_path!}/placements/24.png" alt="img" /></h3>
					<h3 class="singCourse_title"><span>Salary Package : </span>3.40 LPA</h3>
					<h3 class="singCourse_title"><span>Number of Students placed : </span>1</h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
							 <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>The list of the companies visited our campus</p>
                    </blockquote>
				 
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title">1. Infosys Limited </h3>
					<h3 class="singCourse_title">2. L and T Integrated Services</h3>
					<h3 class="singCourse_title">3. Accenture</h3>
					<h3 class="singCourse_title">4. Tech Mahindra & Mahindra Satyam </h3>
					<h3 class="singCourse_title">5. Wipro Technologies </h3>
					<h3 class="singCourse_title">6. Tata Consultancy Services </h3>
					<h3 class="singCourse_title">7. IBM</h3>
					<h3 class="singCourse_title">8. Cognizant Technologies Solutions </h3>
					<h3 class="singCourse_title">9. Keane India </h3>
					<h3 class="singCourse_title">10. GE Motors </h3>
					<h3 class="singCourse_title">11. Honeywell </h3>
					<h3 class="singCourse_title">12. GE Capital </h3>
					<h3 class="singCourse_title">13. HDFC </h3>
					<h3 class="singCourse_title">14. HSBC </h3>
					<h3 class="singCourse_title">15. Hindustan Lever Ltd </h3>
					<h3 class="singCourse_title">16. Hewlett Packard </h3>
					<h3 class="singCourse_title">17. L and T Infotech </h3>
					<h3 class="singCourse_title">18. Toyota Kirloskar Pvt.Ltd </h3>
					<h3 class="singCourse_title">19. Toyoda Textile Pvt.LTd </h3>
					<h3 class="singCourse_title">20. Saint Gobain </h3>
					<h3 class="singCourse_title">21. Unisys </h3>
					<h3 class="singCourse_title">22. Piaggio </h3>
					<h3 class="singCourse_title">23. Indian Navy </h3>
					<h3 class="singCourse_title">24. Indian Army </h3>
					<h3 class="singCourse_title">25. Robert Bosch </h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <!-- start single course -->
                <div class="col-lg-6 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title">26. I-Flex </h3>
					<h3 class="singCourse_title">27. ITC </h3>
					<h3 class="singCourse_title">28. Tesco </h3>
					<h3 class="singCourse_title">29. Motorola </h3>
					<h3 class="singCourse_title">30. Oracle Financials Services </h3>
					<h3 class="singCourse_title">31. Ranbaxy </h3>
					<h3 class="singCourse_title">32. Rallis India </h3>
					<h3 class="singCourse_title">33. Reliance </h3>
					<h3 class="singCourse_title">34. Siemens </h3>
					<h3 class="singCourse_title">35. Standard Chartered </h3>
					<h3 class="singCourse_title">36. Perot System </h3>
					<h3 class="singCourse_title">37. Kasura Technologies </h3>
					<h3 class="singCourse_title">38. TVS Motors </h3>
					<h3 class="singCourse_title">39. HCL Technologies </h3>
					<h3 class="singCourse_title">40. Telco </h3>
					<h3 class="singCourse_title">41. Tata Elxsi </h3>
					<h3 class="singCourse_title">42. Quinox </h3>
					<h3 class="singCourse_title">43. Symphony </h3>
					<h3 class="singCourse_title">44. Exiltant Technology </h3>
					<h3 class="singCourse_title">45. Yokogawa IA Technologies India Pvt.Ltd </h3>
					<h3 class="singCourse_title">46. Microsoft India </h3>
					<h3 class="singCourse_title">47. T D Power System </h3>
					<h3 class="singCourse_title">48. TISCO </h3>
					<h3 class="singCourse_title">49. Global Edge Software </h3>
					<h3 class="singCourse_title">50. Fire Pro Sytems </h3>
					                    
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
               
              
            </div>
          </div>
          <!-- End course content -->


        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>