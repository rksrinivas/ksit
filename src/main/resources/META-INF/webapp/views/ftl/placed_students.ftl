<@page>
	<div class="container" style="background-color:white;min-height:600px">

		<div class="row">
			<#list placedStudentsList as placedStudent>
				<div class="col-lg-4 col-md-4 col-sm-4">
					${placedStudent.heading!}
					<img src="${placedStudent.imageURL!}" width="100%"/>
  				</div>
  			</#list>
  		</div>
	</div>
</@page>