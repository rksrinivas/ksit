<@page>
<style>

.row_nba{
	text-align: center;
}
.tile {
	border-style: solid;
	padding: 50px 14px 14px 14px;
	border-width: 3px;
	color: #ffffff;
	display: inline-block;
	height: 130px;
	list-style-type: none;
	margin: 10px 40px 10px 20px;
	position: relative;
	text-align: center;
	width: 270px;
	border-radius: 25px;
	box-shadow: 10px 10px 5px #888888;
	background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #CCF11B), color-stop(1, #3874FF));
	background-image: linear-gradient(-28deg, #000000 0%, #171973 100%);
    vertical-align: top;
	text-align: center;
}

.tile_cs {
	border-style: solid;
	padding: 35px 14px 14px 14px;
	border-width: 3px;
	color: #ffffff;
	display: inline-block;
	height: 130px;
	list-style-type: none;
	margin: 10px 40px 10px 20px;
	position: relative;
	text-align: center;
	width: 270px;
	border-radius: 25px;
	box-shadow: 10px 10px 5px #888888;
	background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #CCF11B), color-stop(1, #3874FF));
	background-image: linear-gradient(-28deg, #000000 0%, #171973 100%);
    vertical-align: top;
	text-align: center;
}

.tile_ec {
	border-style: solid;
	padding: 25px 14px 14px 14px;
	border-width: 3px;
	color: #ffffff;
	display: inline-block;
	height: 130px;
	list-style-type: none;
	margin: 10px 40px 10px 20px;
	position: relative;
	text-align: center;
	width: 270px;
	border-radius: 25px;
	box-shadow: 10px 10px 5px #888888;
	background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #CCF11B), color-stop(1, #3874FF));
	background-image: linear-gradient(-28deg, #000000 0%, #171973 100%);
    vertical-align: top;
	text-align: center;
}

.tile_description {
	color: yellow;
	font-size: 1.5em;
    font-weight: 900;
}

.heading {
	text-align: center;
	color: red;
	font-size: 2em;
	font-weight:900;
	margin-bottom: 30px;
}
</style>
<br/>
<br/>
<div class="container" style="background-color:white;min-height:600px">
	<div class="row_nba">
		<h1 class="heading">NBA Documents Uploaded Department Wise</h1>
		<div>
			<a href="/nba_previsit_dept_cse">
				<div class="tile_cs">
					<p class="tile_description">Computer Science & Engineering</p>
				</div>
			</a>
			<a href="/nba_previsit_dept_ece">
				<div class="tile_ec">
					<p class="tile_description">Electronics & Communication Engineering</p>
				</div>
			</a>
			<a href="/nba_previsit_dept_mech">
				<div class="tile">
					<p class="tile_description">Mechanical Engineering</p>
				</div>
			</a>
		</div>
	</div>
</div>
</@page>