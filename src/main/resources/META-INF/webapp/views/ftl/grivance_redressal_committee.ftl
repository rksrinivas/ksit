<@page>
<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<!--<h3>Grievance</h3>-->
					<!--  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote> -->
                    
				<div class="dept-title">
   				<h1>Grievance Redressal Committee</h1>   
				</div>            
				
				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<!--	<h3 class="color_red">GRIEVANCE REDRESSAL COMMITTEE</h3>
	                    	<p>As per the directions of the Registrar, VTU, Belgaum and in accordance
	                    	 with the regulations of AICTE, a <strong>Grievance Redressal Committee</strong> has been established in our 
	                    	 college with the following --->
	                    	 <!--<a href="${img_path!}/pdf/KSIT_Grievance_Cell.pdf" target="_blank">members</a>
	                    	 .</p>-->
	                    <!--	<p>All aggrieved students, their parents & staff may approach the committee for assistance and solve
	                    	 their problems related to academics, resources and personal grievances, ifany.</p> -->
	                    	 
	                    	 		<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name</th>
												<th>Department</th>
												<!--<th>Mobile</th> -->
												<th>Designation</th>
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>1.</td>
												<td>Dr. Dilip Kumar. K </td>
												<td>Principal / Director</td>
												<!--<td>9916915517 </td> -->
												<td>Chair Person</td>
											  </tr>
											  
											   <tr>          
												<td>2.</td>
												<td>Dr. Girish. T. R</td>
												<td>Prof & HOD, Dept of ME </td>
												<td>Member</td>
											  </tr>
											  
											   <tr>          
												<td>3.</td>
												<td>Mr. Kumar. K</td>
												<td>Asst. Prof, Dept Of CSE </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>4.</td>
												<td>Dr. Devika. B	</td>
												<td>Asso Prof, Dept of ECE </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>5.</td>
												<td>Dr. Amulyashree. S </td>
												<td>Asso Prof. Dept of AIML </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>6.</td>
												<td>Dr. Venkataramana B. S	</td>
												<td>Asst Prof, Dept of BS&H	 </td>
												<td>Member </td>
											  </tr>
											  
											   <tr>          
												<td>7.</td>
												<td>Ms. Gagana</td>
												<td>Student, Dept of CSE </td>
												<td>Member</td>
											  </tr>
											  
											   <tr>          
												<td>8.</td>
												<td>Ms. Aditi  </td>
												<td>Student, Dept. of ECE </td>
												<td>Member </td>
											  </tr>
											  
											  <tr>          
												<td>9.</td>
												<td>Mr. Haryank. K  </td>
												<td>Student, Dept. of ME </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>10.</td>
												<td>Mr. Madhu. S. S  </td>
												<td>Student, Dept. of AI&ML </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>11.</td>
												<td>Mr. Dhanush  </td>
												<td>Student, Dept. of CS&D </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>12.</td>
												<td>Ms. Hima. S  </td>
												<td>Student, Dept. of CSE (1st Year) </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>13.</td>
												<td>Mr. Havavadan Madhwaraj  </td>
												<td>Student, Dept. of AI&ML (1st Year) </td>
												<td>Member</td>
											  </tr>

											  <tr>          
												<td>14.</td>
												<td>Ms. Trupthi G. B  </td>
												<td>Student, Dept. of CSE (Sports) </td>
												<td>Member</td>
											  </tr>
											
											 </tbody>
											 
													 
											 
											  
											  
											  
								 </table>
	                    	 
						                    
                    </div>
                    
                    
                    
                    
                   
                  </div>
                </div>
                <!-- End single course -->
				
					
				
				 
            </div>
          </div>
          <!-- End course content -->
          
         
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>