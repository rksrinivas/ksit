<@page>  
<style>
/* CSS for Radio Consent */
.container-text {
  display: block;
  position: relative;
  padding: 0px 4px 0px 30px;
  margin-bottom: 25px;
  cursor: pointer;
  font-size: 10px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-weight:400;
  text-align: justify;
}

/* Hide the browser's default checkbox */
.container-text input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #fff;
  border:1px solid #78adc9;
}

/* On mouse-over, add a grey background color */
.container-text:hover input ~ .checkmark {
  background-color: #fff;
  box-shadow:inset 0px 0px 2.5px #78adc9;
}

/* When the checkbox is checked, add a blue background */
.container-text input:checked ~ .checkmark {
  background-color: #78adc9;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container-text input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container-text .checkmark:after {
  left: 9px;
  top: 4px;
  width: 7px;
  height: 13px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>   
<section id="contact">
     <div class="container">
       
       <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
         
          <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="aboutus_area wow fadeInLeft">

           <div class="grievance_form wow fadeInLeft">
                       <h2 class="titile text-center">Admission Enquiry</h2>

           	  <form action="/course/add" class="submitphoto_form" method="POST">
           	  
           	  		<div class="col-lg-12 col-md-12 col-sm-12">
           	  
		                 <input type="text" class="wp-form-control wpcf7-text" name="name" placeholder="Your name" required>
		                 
		                 <input type="email" class="wp-form-control wpcf7-email" name="email" placeholder="Email address" required> 
			                
			             <input type="tel" class="wp-form-control wpcf7-text" name="mobileNumber" placeholder="Phone Number" required>   
			                       
		                 		
							<select class="wp-form-control wpcf7-text" name="course" required>
								<#list courseList as course>
									<option value="${course}">${course.desc}</option>
								</#list>		  				  
							</select>
				
				           <input type="number" step="0.01" class="wp-form-control wpcf7-text" name="SSLC" placeholder="% of Marks in SSLC / 10th (Equivalent)" required>   
				          
				           <input type="number" step="0.01" class="wp-form-control wpcf7-text" name="PUC" placeholder="% of Marks in PUC / 12th / +2 (Equivalent)" required>   

			            	<textarea class="wp-form-control wpcf7-textarea" name="address" cols="30" rows="10" placeholder="Please enter your address" required></textarea><br/>
			            <label class="container-text">I authorize K S Institute of Technology and its representatives to contact me with updates and notification via Email, SMS, WhatsApp &amp; Call. This will override the registry on DND / NDNC.
  							<input type="checkbox" checked="checked" required>
  							<span class="checkmark"></span>
						</label>
		                <input type="submit" value="Submit" class="wpcf7-submit">
	                </div>
              	</form>
              	</div>
			    <hr/>
           </div>
           </div>
           <!-- content -->
            <div class="col-lg-6 col-md-6 col-sm-12">
            	<div class="aboutus_area wow fadeInLeft">
            <h2 class="titile">Admission Details</h2>
				
				<p>K S Institute of Technology is affiliated to Visveswaraya Technological University, Accredited by NAAC, recognized by Government of Karnataka and approved by AICTE New Delhi.  </p>
			
			   
			   <h3>&#9656; UG COURSE</h3>
			   <h2>Admission process</h2>
			   <p>Admission to UG courses is through Common Entrance Test (UGCET)  conducted by Karnataka Examination Authority (KEA) & Under Graduate Entrance Test (UGET) conducted by Consortium of Medical, Engineering and Dental Colleges of Karnataka (COMEDK). </p>
			   
			   <p>Admission to Management Quota seats is based on merit and rank obtained in any of the Entrance Test : UGCET , UGET or JEE.   </p>
			   
			   <h2>Eligibility Criteria :</h2>
			   
			   <p>The candidates who have passed the qualifying examination, second PUC or 10+2 Higher Secondary or equivalent 
			   examination recognized by State / Central Government; the last two years of study shall comprise of Physics,
			    Chemistry and Mathematics and should have passed these subjects individually along with English as a compulsory subject. </p>
			   
			   <p>Candidates must have obtained a minimum of 45% marks in aggregate wherein Physics and Mathematics are 
			   compulsory subjects along with Chemistry or Bio- Technology or Biology or Computer Science or Electronics as one 
			   of the optional subjects (40% marks in respect of SC, ST and OBC candidates of Karnataka State). However, changes,
			    if any, in the eligibility criteria by the AICTE will be adopted. </p>
			   
			 
			</div>
            </div>
           <!-- content -->
           
           <div class="col-lg-12 col-md-6 col-sm-12">
		   	<div class="aboutus_area wow fadeInLeft">
             <h3>&#9656; PG COURSE</h3>
			   <h2>Admission process</h2>
			   
			   <p>Admission to PG courses is through Post Graduate Common Entrance Test (PGCET) conducted by Karnataka 
			   Examination Authority (KEA).</p>
			   
			   <p>Admission to Management Quota seats is based on merit and rank obtained in the Entrance Test : PGCET or valid GATE SCORE    </p>
			   
			   <h2>Eligibility Criteria :</h2>
			   
			   <p>A candidate who has passed qualifying examination or equivalent examination as prescribed by the Competent Authority and obtained an aggregate minimum of 50% marks taken together in all the subjects of all the                   years / semesters of the Degree Examination is eligible for admission. (45% of marks in Q. E. in case of SC, ST and Category-I candidates and reservation is applicable only for Karnataka Candidates)</p>

          <h2>Bank Account Details :</h2>	
          <p>K. S. INSTITUTE OF TECHNOLOGY, BANGALORE</p>
          <P>AXIS BANK, JP NAGAR BRANCH, BANGALORE</P>
          <P>SB A/C NO. 912010014093916</P>
          <P>IFS CODE NO. UTIB0001513</P>
           </div>
           </div>
           
         </div>
         
       </div>
      </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->
    
    
    
</@page>