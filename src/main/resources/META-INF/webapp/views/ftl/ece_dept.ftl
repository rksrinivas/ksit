<@page>
<#include "feedback/feedback_list_modal.ftl">
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#if ecePageSliderImageList?has_content>
                     		<#list ecePageSliderImageList as ecePageSliderImage>
                     			<#if ecePageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${ecePageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">${ecePageSliderImage.heading}</p>
										<p class="slideInRight">${ecePageSliderImage.content}</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>
	                     	<article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/ece/slider/s4.jpg" alt="">
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated slideInLeft" src="${img_path!}/ece/slider/s1.jpg" alt=""> 
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/ece/slider/s2.jpg" alt="">										 
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/ece/slider/s3.jpg" alt=""> 
	                        </article>
						</#if>
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 

<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 



    <!-- upcoming and latest news list-->
      
	<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at ECE
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
					
				<div class="col-md-12">
					<div class="row">
					
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=ecePageLatestEventsList/>
									</div>
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
									<@eventDisplayMacro eventList=ecePageUpcomingEventsList/>
								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->
	
	<!-- welcome -->
	<div class="dept-title">
	   <h1>Welcome To Electronics And Communication Engineering</h1>
	   <p class="welcome-text">The Electronics and Communication Engineering opens up great career prospects for the students in manufacturing industries and service organizations such as broadcasting, consulting, data communication, entertainment, research and development; and system support.</p>
	</div>
	<!-- welcome -->



<!--dept tabs -->
<section id="vertical-tabs">
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
      
         <div class="vertical-tab padding" role="tabpanel">
         
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
               <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="dept-cart">Profile</a></li>
               <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision & Mission, PEO, PSO & PO</a></li>
               <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>
              
               <li role="presentation"><a href="#professional_bodies" aria-controls="professional_bodies" role="tab" data-toggle="tab" class="dept-cart">Professional Bodies</a></li>
               <li role="presentation"><a href="#professional_links" aria-controls="professional_links" role="tab" data-toggle="tab" class="dept-cart">Professional Links</a></li>
               <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
               <li role="presentation"><a href="#pedagogy" aria-controls="pedagogy" role="tab" data-toggle="tab" class="dept-cart">Pedagogy</a></li>               
               <li role="presentation"><a href="#content-beyond-syllabus" aria-controls="content-beyond-syllabus" role="tab" data-toggle="tab" class="dept-cart">Content Beyond Syllabus</a></li>
              
               <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Time Table & Calendar of Events</a></li>
            
               <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>

            
               <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li>
               <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
              
               <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li>
              	
              
               <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab" class="dept-cart">Achievers</a></li>
               <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
               <li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
               <li role="presentation"><a href="#library_dept" aria-controls="library" role="tab" data-toggle="tab" class="dept-cart">Departmental Library</a></li>
               <li role="presentation"><a href="#placement_dept" aria-controls="placement" role="tab" data-toggle="tab" class="dept-cart">Placement Details</a></li>
               
               <li role="presentation"><a href="#higher_education" aria-controls="higher_education" role="tab" data-toggle="tab" class="dept-cart">Higher Education & Entrepreneurship</a></li>
              
               <li role="presentation"><a href="#student_club" aria-controls="student_club" role="tab" data-toggle="tab" class="dept-cart">Student Club</a></li>

               <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab" class="dept-cart">Internships & Projects</a></li>
               <li role="presentation"><a href="#industrial_visit" aria-controls="industrial_visit" role="tab" data-toggle="tab" class="dept-cart">Industrial Visit</a></li>
             
               
               <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
               <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
            
  
               <li role="presentation"><a href="#project_exhibition" aria-controls="project_exhibition" role="tab" data-toggle="tab" class="dept-cart">Project Exhibition</a></li>
               <li role="presentation"><a href="#mou-signed" aria-controls="mou-signed" role="tab" data-toggle="tab" class="dept-cart">MOUs Signed</a></li>
               <li role="presentation"><a href="#alumni-association" aria-controls="alumni-association" role="tab" data-toggle="tab" class="dept-cart">Alumni Association</a></li>
			   <li role="presentation"><a href="#nba" aria-controls="nba" role="tab" data-toggle="tab" class="dept-cart">NBA</a></li>
                                         
              <li role="presentation"><a href="#social_activities" aria-controls="social_activities" role="tab" data-toggle="tab" class="dept-cart">Social Activities</a></li>
               <li role="presentation"><a href="#other_details" aria-controls="other_details" role="tab" data-toggle="tab" class="dept-cart">Other Details</a></li>
          
             <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="tab" class="dept-cart">Testimonials</a></li>
            
               
               
		       <!--<li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>-->
            </ul>
            <!-- Nav tabs -->
            
	            <!-- Tab panes -->
	            <div class="tab-content tabs"> 
	            	
	            	 <!-- profile -->
               <div role="tabpanel" class="tab-pane fade in active" id="profile">
                <div class="row">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Profile</h3>
					 <p>The Department of <a href="https://ksgi.edu.in/Best-College-For-Electronics-and-Communication-Engineering" class="font-weight-600" target="_blank">Electronics and Communication Engineering</a> (ECE) was
established in the year 1999 offering UG and Ph.D under the affiliation of VTU, Belagavi. The course is structured with an Outcome-Based Education to help students to excel
in academics and to achieve their goals. The department is accredited by NBA, NAAC and IEI. </p>
						
						<p>The main aim of the department is to work towards overall development of the students and make them industry ready engineers. The Department provides excellent training
                     with strong fundamental knowledge with activity-based learning in Electronics and Communication Engineering. The department has Garut Aeromodelling club &amp; Satellite
                     club to promote experiential learning. Department of ECE believes in overall development of students and created platform through active professional bodies like IEEE, ISTE, IEDC, IETE and IEI. These forums help students to take-up innovative
                     projects, present technical papers and organize various technical events &amp; non- technical events thereby receiving a broader perspective on the professional front by providing opportunities for teamwork, leadership and service. The department
                     provides ample opportunities to students to participate &amp; also organise in non-technical and cultural activities. These forums will provide platform to showcase their talents.
                     Our students have brought laurels to the department by representing in sports and cultural events at various Universities, State level and National level.</p>

                  </div>
                  <h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <img class="hod-img img-responsive" src="${img_path!}/ece/ece-hod-new.jpg" alt="image" />
                        <div class="qualification_info">
	                        <h3 class="qual-text">Dr. P. N. Sudha</h3>
	                        <h4 class="qual-text">Professor & Head</h4>
	                        <h3 class="qual-text">B.E, M.Tech, Ph.D</h3>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                    			
						<p>The ECE Department provides a holistic environment for the overall development of the
                     student. We have highly dedicated, committed and experienced team of teaching &amp; supporting staff. Our team of faculty members continuously strive hard and leave no
                     stone unturned for the all-round development of the students through personal guidance, mentoring &amp; Counselling beyond academics. Apart from providing a friendly
                     atmosphere for learning, we also constantly strive to improve the quality of Engineering through various programs like Technical Talk, Workshops, guest lectures and FDP.
                     Department organises regular industrial visits for all the students.</p>								
                        <p>Students are nurtured by enhancing their confidence to bring out best in them. The
                           course is structured with an Outcome based education to achieve academic excellence. During the course of study students are encouraged to take-up mini projects, activity
                           based learning &amp; case studies to supplement theoretical knowledge with practical experience. They are motivated to select projects benefiting society or dealing with local
                           problems.</p>
                     </div>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     	
						<p> These projects enable them to understand the relevance of working in a group and also
help them to realize the finer aspects of teamwork. We also encourage students to organize and participate in curricular and extracurricular activities with social
relevance. Our students have participated in many events and brought laurels to the dept.</p>
                     	<p>With valued based education ingrained in them we find our budding engineers
hardworking, ready to learn and committed. Department has maintained quality placement record till date. We are proud of the long and sustained placement records of
our students in different multinational companies, few of our students are pursuing higher education at different top-class universities after qualifying GRE/TOEFL &amp; IELS
and few of students have become budding entrepreneurs. Department believe that “Our students are our brand ambassadors"</p>
						<p>Thankyou</p>
                     </div>
                  </div>
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Highlights of department</h3>
                     <ul class="list">
                     	<li>Highly experienced and passionate faculty members.</li>
                     	<li>State of Art Class rooms &amp; laboratory facilities.</li>
                     	<li>Excellent teaching and learning environment.</li>
                     	<li>Activity based experiential learning.</li>
                     	<li>Exclusive department library for quick &amp; ready reference.</li>
                     	<li>Adequate teaching facilities with Online videos /Presentations.</li>
                     	<li>All round development of students through conduction of various activities.</li>
                     	<li>Active departmental clubs to kindle experiential learning</li>
                        <li>Dynamic and active professional bodies like IEEE, ISTE, IETE &amp; IEI</li>
                        <li>Excellent placement record.</li>
                        <li>Effective mentoring</li>
                     </ul>
                   
                   
										  
					
					<h3> Course Opportunities</h3>
					<p>The students graduated from our department have become successful professional engineers working for both core and software companies such as Robert Bosch, Wipro
R&amp;D, Infosys, Cognizant Technology Solutions, HCL Technologies, TCS, Mahindra Satyam, iGate and MSMEs etc. Some of them are serving in government organizations
and few of them have become successful entrepreneurs. Our students are pursuing higher education like MTech, MS, MBA and PhD in various universities in India and
abroad. Few of our graduates have become job providers by becoming entrepreneurs. </p>					
                     
                  </div>
                </div>
               </div>
               <!-- profile -->
               
                <!-- vision and mission -->
               <div role="tabpanel" class="tab-pane fade in" id="vision">
                 <div class="row">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Vision</h3>
                      <ul class="list">
						 <li>To achieve excellence in academics and research in Electronics & Communication     Engineering  to  meet  societal  need.</li>
						 </ul>
						<button type="button" style="float:right;" class="btn btn-info btn-sm" data-backgrop="static" data-toggle="modal" data-page="ece_page" data-target="#feedbackListModal">Feedback</button>                 
						 
						 
						 <h3>Mission</h3>
						 
						 <ul class="list">
						 <li>To impart quality technical education with the relevant technologies to produce industry ready engineers
						  with ethical values.</li>
						  
						  <li>To enrich experiential learning through active involvement in professional clubs & societies.</li>
						  
						  <li>To promote industry-institute collaborations for research & development.</li>
					 </ul>
                    		
						
						
						<h3>PROGRAM EDUCATIONAL OBJECTIVES (PEOs) </h3>
						
						<p>PEO1 : To Excel in professional career by acquiring domain knowledge.</p>
						<p>PEO2 : To pursue higher Education and research by adopting 
			    					technological innovations by continuous learning through professional 
			   						 bodies and clubs. </p>
						<p>PEO3 : To inculcate effective communication skills, team work, ethics, entrepreneurship skills and
		     					leadership qualities. </p>
		     					
						<h3>PROGRAM SPECIFIC OUTCOMES (PSOs)</h3>
						
						<p>PSO1: Graduate should be able to understand the fundamentals in the field of 
						  Electronics and Communication and apply the same to various areas like 
						  Signal processing, embedded systems, Communication & Semiconductor 		  technology.</p>
						  
						<p>PSO2: Graduate will demonstrate the ability to design, develop solutions for 
								  Problems in Electronics and Communication Engineering using hardware and software tools with social concerns.</p>
						
                    <h3>PROGRAM OUTCOMES</h3>
                  <p>1. Engineering knowledge: Apply the knowledge of mathematics, science, engineering fundamentals, and engg. specialization to the solution of complex engineering problems.</p>
						<p>2. Problem analysis: Identify, formulate, research literature, and analyze engineering problems to arrive at substantiated conclusions using first principles of mathematics, natural, and engineering sciences. </p>
						<p>3. Design/development of solutions: Design solutions for complex engineering problems and design system components, processes to meet the specifications with consideration for the public health and safety, and the cultural, societal, and environmental considerations.</p>
						<p>4. Conduct investigations of complex problems: Use research-based knowledge including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions.</p>
						<p>5. Modern tool usage: Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations.</p>
						<p>6. The engineer and society: Apply reasoning informed by the contextual knowledge to assess societal, health, safety, legal, and cultural issues and the consequent responsibilities relevant to the professional engineering practice.</p>
						<p>7. Environment and sustainability: Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development.</p>
						<p>8. Ethics: Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice.</p>
						<p>9. Individual and team work: Function effectively as an individual, and as a member or leader in teams, and in multidisciplinary settings. </p>
						<p>10. Communication: Communicate effectively with the engineering community and with society at large. Be able to comprehend and write effective reports documentation. Make effective presentations, and give and receive clear instructions.</p>
						<p>11. Project management and finance: Demonstrate knowledge and understanding of engineering and management principles and apply these to one�s own work, as a member and leader in a team. Manage projects in multidisciplinary environments.</p>
						<p>12. Life-long learning: Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change.</p>
				 
                  </div>                  
                  </div>
               </div>
               <!-- vision and mission -->
                              
               
                <!-- Research and development -->
               <div role="tabpanel" class="tab-pane fade" id="research">
                  <div class="row">
                      <h3>Research and Development</h3> 
                      <div class="col-lg-12 col-md-12 col-sm-12">
				 		<p>
				 			<h1>Funding & Consulting Projects @ KSIT, R & D</h1>
				 		</p>
				 		<p>
				 			Research and Development Centre of Electronics and Communication Engineering Department of KSIT was started in the year of 2014-15 and is encouraging PhD research scholars & 
				 			students to carryout research and development in the specialized areas like Image processing, Communication Neural networks etc. The Centre is affiliated to VTU. Presently, 
				 			the Centre has five research guides with 20 research scholars pursuing PhD under these guides. The Centre is well equipped with software and hardware tools required to carry out research and development.
				 			Faculty members of the department are collaborating with organizations like Tata elxsi, Mitara consulting services, Acsia Technologies (P) Ltd., and many more for consultancy works. 
				 			The research and consultancy projects worth Rs. 30.75 Lakhs are either completed or ongoing.
				 		</p>
				 		<p>
				 			The research projects worth Rs. 8.8 Lakhs are either completed or ongoing. Some of these are funded by Government of Karnataka, Vision Group on Science and Technology (VGST), 
				 			Karnataka State Council for Science and Technology (KSCST), Visveswaraya Technological University (VTU) and Institution of Engineers IE(I).
				 		</p>
				 		<p>
				 			The faculty of the ECE department is actively engaged in research in the areas of Electronics and Communication Engineering, Signal Processing, Cryptography, Machine Learning and VLSI Technologies. 
				 			A total of 188 research articles were published and presented by the faculty in various technical journals and conferences in the past 3 years, with more than 750 citations.
				 		</p>
				 	</div>
				 </div>
				  <div class="row">     
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     
                        <h3>R & D Activities</h3>
                        
                        <#list eceResearchList as eceResearch>
                        	<p>${eceResearch.heading!}  <a href="${eceResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        
                        <p>Every year the department encourages faculty & students to apply for project funding to funding agencies like VGST, KSCST, DST etc. Following are the details of the sponsored projects.</p>
                        
                        <#list eceSponsoredProjectsList as eceSponsoredProjects>
                        	<p>${eceSponsoredProjects.heading!}  <a href="${eceSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        
                        <#list ecePhdPursuingList as ecePhdPursuing>
                        	<p>${ecePhdPursuing.heading!}  <a href="${ecePhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        
                        <h3>Ph.D Awardees</h3>
                        
                         <#list ecePhdAwardeesList as ecePhdAwardees>
                        	<p>${ecePhdAwardees.heading!}  <a href="${ecePhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                       
                     
                     
                     
                     			<!--<p>Funds and projects</p>
								<p><a href="${img_path!}/ece/ece_links/updated-R-AND-D _2020.pdf" target="_blank">Funds and Projects for 2017-18</a></p>
								<p><a href="${img_path!}/ece/ece_links/Funded_projects_2017-18.pdf" target="_blank">Funds and Projects for 2017-18</a></p>
								<p><a href="${img_path!}/ece/ece_links/Funded_projects_2016-17.pdf" target="_blank">Funds and Projects for 2016-17</a></p>
								
								<p><a href="${img_path!}/ece/ece_links/Funds and Projects.pdf" target="_blank">Funds and projects</a></p>
								
								<p>Publications</p>
								
                                 

								<p><a href="${img_path!}/ece/ece_links/Publications_2018-19.pdf" target="_blank">Publications for 2018-19</a></p>
								<p><a href="${img_path!}/ece/ece_links/Publications_2017-18.pdf" target="_blank">Publications for 2017-18</a></p>
								
								<p><a href="${img_path!}/ece/ece_links/Publications 2016-17.pdf" target="_blank">Publications 2016-17</a></p>
								<p><a href="${img_path!}/ece/ece_links/Publications 2015-16.pdf" target="_blank">Publications 2015-16</a></p>
								<p><a href="${img_path!}/ece/ece_links/Publications 2014-15.pdf" target="_blank">Publications 2014-15</a></p>
								<p><a href="${img_path!}/ece/ece_links/Publications 2013-14.pdf" target="_blank">Publications 2013-14</a></p>
								
								
								
								<p>Faculty pursuing PhD</p>
								<p><a href="${img_path!}/ece/ece_links/Phd Scholars.pdf" target="_blank">Faculty pursuing PhD</a></p>-->
								
                     
                      </div>
                  </div>
               </div>
               <!-- Research and development -->
               
                <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="row">
                     <h3>Time Table</h3>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list eceClassTimeTableList as eceClassTimeTable>              
                        <p>${eceClassTimeTable.heading!}  <a href="${eceClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list eceTestTimeTableList as eceTestTimeTable>
                        <p>${eceTestTimeTable.heading!}  <a href="${eceTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list eceCalanderList as eceCalander>              
                        <p>${eceCalander.heading!}  <a href="${eceCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--Time Table  -->
               
               
                  <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>UG (B.E in ECE)</h3>
                        
                        <#list eceUgSyllabusList as eceUgSyllabus>
                        <p>${eceUgSyllabus.heading!}  <a href="${eceUgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                         <h3>PG (M.Tech in Digital Electronics & Communication)</h3>
                        
                        <#list ecePgSyllabusList as ecePgSyllabus>
                        <p>${ecePgSyllabus.heading!}  <a href="${ecePgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
             
               
               
                <!--News Letter  -->
               <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                                               
                        <h3>Department News letter- PRERANA</h3>
					    <p>Department of Electronics and Communication engineering showcases the departmental activities through News letter PRERANA every year. The editorial team of news letter summarizes the talent, achievements and mile stones achieved by the staff and students through department magazine. The main goal of news letter is to provide overall development for our students through academic excellence, innovations and creativity. Our experienced and well learned faculties help us to empower a diverse community of students to nurture their capabilities, transform their lives and find success through high quality teaching and learning. We encourage our students to participate in curricular and extracurricular activities to shape them as leaders of tomorrow and to be industry ready engineers to face competitive world.</p> 
                        
                        <h3>IEEE Newsletter-MOMENTUM:</h3>
						<p>IEEE Newsletter is released annually that showcases technical events, humanitarian events conducted under IEEE KSIT Student branch for students. The editorial team of IEEE Newsletter constitutes both faculty and students IEEE members.</p>
                        
                        
                        
                        <#list eceNewsLetterList as eceNewsLetter>
                        <p>${eceNewsLetter.heading!}  <a href="${eceNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--News Letter  -->
               
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="row">
					
					  <h3>Faculty</h3>
					<p>The ECE department has highly dedicated, motivated and experienced faculty members with passion for teaching. The faculty members in the department are highly accomplished with post graduation and doctoral degrees to provide a conducive learning environment to achieve excellence in education. They have specializations in the areas of Communications, Microwaves, VLSI, Image Processing, Digital Signal Processing etc. The department is committed to achieve academic excellence by promoting innovative activities.</p>
					<p>The department is headed by Dr. P. N. Sudha and has good experience in teaching, research activities and administration.</p>
					
					                     



                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Faculty</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name}</p>
                                 <p>${faculty.designation}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id!}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               
                    <!--testimonials  -->
               <div role="tabpanel" class="tab-pane fade" id="testimonials">
                  <!--slider start-->
                  <div class="row">
                     <div class="col-sm-8">
                        <h3><strong>Testimonials</strong></h3>
                        <!--<div class="seprator"></div>-->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          
                           <!-- Wrapper for slides -->
                           <div class="carousel-inner">
                              <#list testimonialList as testimonial>
                              <div class="item <#if testimonial?is_first>active</#if>">
                                 <div class="row" style="padding: 20px">
                                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                    <p class="testimonial_para">${testimonial.content!}</p>
                                    <br>
                                    <div class="row">
                                       <div class="col-sm-2">
                                          <img src="${testimonial.imageURL!}" class="img-responsive" style="width: 80px">
                                       </div>
                                       <div class="col-sm-10">
                                          <h4><strong>${testimonial.heading!}</strong></h4>
                                          <!--<p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                             <span>Officeal All Star Cafe</span>-->
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </#list>  
                           </div>
                           <div class="controls testimonial_control pull-right">									            	
                              <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--slider end -->
               </div>
               <!--testimonials  -->
               
                   <!-- Achievers -->
               <div role="tabpanel" class="tab-pane fade" id="achievers">
               	<div class="row">
               		 <h3>Department Achievers</h3>
                     <!--Department Achievers-->
		                  <#list eceDepartmentAchieversList as eceDepartmentAchievers>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${eceDepartmentAchievers.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${eceDepartmentAchievers.heading!}</h3>	                      	 
			                       	 <p>${eceDepartmentAchievers.eventDate!} </p>
			                       	 <p>${eceDepartmentAchievers.content!}</p>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>  
		                    
		              <h3>List of FCD Students</h3>
		              <#list eceFcdStudentsList as eceFcdStudents>
                        <p>${eceFcdStudents.heading!}  <a href="${eceFcdStudents.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                      </#list> 
                    
               	</div>
               </div>
               <!-- Achievers -->
               
                     <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="row">
               	
               		 <h3>Infrastructure / Facilities</h3>
					<p>All laboratories are modernized, well equipped and established with state-of-the-art facilities. Well-qualified and highly experienced instructors are available to assist students to carry out their experiments. </p>
					
					<p>The class rooms, labs and seminar hall of Electronics and Communication Department are spacious, well ventilated, has comfortable seating arrangement with White/ Black/ Glass boards, LCD projectors & Internet facility. All laboratories are modernized, well equipped and established with state-of-the-art facilities.</p>

					<ul class="list">
						<li>Department of ECE has 9 Class Rooms each of 70 Seating capacity and 1 Seminar Hall of 120 Seating capacity with good Ambience. All the class rooms are equipped with ICTs.</li>
						<li>Departmental library has 720 books with reading room having of seating capacity of 8.</li>
						<li>9 Labs with equipment like CRO, Power Supply, Function Generators, Basic electrical lab modules, Embedded Microcontroller kits, IC trainer kits, Microwave integrated circuit kits, Microwave benches, Antennas, DPSK &QPSK kits, OFC kits, Power Electronics modules, FPGA kits, 8051 and Arm kits, 8086 kits, Interfacing kits.</li>
						<li>110 high end computers are available in ECE Department with LAN and Internet facility.</li>
						<li>Labs are equipped with licensed softwares like Cadence Tool , Labview, Matlab & Keil software etc.</li>
						<li>Department has also a set up for R&D Lab which has high end PCs with licensed softwares.</li>
					
					</ul>
					
               	
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
		                  <#list eceLabList as eceLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${eceLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${eceLab.name!}</h3>	                      	 
			                       	 <p>${eceLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${eceLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
                  <!--phd pursuing  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdPursuing">
               	<div class="row">
               		 <h3>Ph.D. Pursuing</h3>
		                  <#list ecePhdPursuingList as ecePhdPursuing>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${ecePhdPursuing.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${ecePhdPursuing.name!}</h3>	                      	 
			                       	 <p>Description: ${ecePhdPursuing.designation!}</p>
			                       	 <a class="btn btn-primary" href="${ecePhdPursuing.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd pursuing -->
               
                 <!--phd awardees  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdAwardees">
               	<div class="row">
               		 <h3>Ph.D. Awardees</h3>
		                  <#list ecePhdAwardeesList as ecePhdAwardees>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${ecePhdAwardees.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${ecePhdAwardees.name!}</h3>	                      	 
			                       	 <p>Description: ${ecePhdAwardees.designation!}</p>
			                       	 <a class="btn btn-primary" href="${ecePhdAwardees.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd awardees -->
	            
	               <!-- Gallery  -->
	               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
	                  <div class="row">
	                     <h3>Gallery</h3>
	                    
	                     <div class="col-lg-12 col-md-12 col-sm-12">
	                        <div id="gallerySLide" class="gallery_area">
	                           <#list eceGalleryList as eceGallery>	
	                           <a href="${eceGallery.imageURL!}" target="_blank">
	                           <img class="gallery_img" src="${eceGallery.imageURL!}" alt="" />					                    			                   
	                           <span class="view_btn">${eceGallery.heading!}</span>
	                           </a>
	                           </#list>
	                        </div>
	                     </div>
	                  </div>
	               </div>
	               <!-- Gallery -->
	               
	                 <!-- Dept Library  -->
		               <div role="tabpanel" class="tab-pane fade" id="library_dept">
		                 <div class="row">
		                  <h3>Departmental Library</h3>
		                  <p>The Department Library occupies a unique place in academic and research activities of the Department. The Library has around 720 books catering to the needs of UG (B.E) and PG (M. Tech) students as well as Teaching Staffs .It covers all domains of Electronics and Communication Engineering. The Library maintains an excellent collection of data books, occasional papers, technical magazines, kannada literary books and other documents/materials. The library preserves previous year's project, dissertation, old university question papers and seminar reports. All students and faculties of the department are encouraged to avail this facility.
		                  </p>
		                  
		                  <p>"Libraries are full of ideas perhaps the most dangerous and powerful of all weapons."   </p> 
    						<p> - Sarah J. Maas</p>
		                  
		                  
		                  <h2 class="titile">Collection Of books</h2>
		                  <table class="table table-striped course_table">
		                     <#list eceLibraryBooksList as libraryBooks>
		                     <tr>
		                        <th style="width:50%">${libraryBooks.heading!}</th>
		                        <td>${libraryBooks.content!}</td>
		                     </tr>
		                     </#list>
		                  </table>
		                  </div>
		               </div>
		               <!--  Dept Library   -->
               
               <!-- Department placements -->
               <div role="tabpanel" class="tab-pane fade" id="placement_dept">
                 <div class="row">
                  <h3>Placement Details</h3>
                  <p>The Department of Placement & Training of KSIT with the assistance of EC department faculty coordinators facilitates  logistic support to companies that visit the College for placements by assisting them in conduction of Presentation, testing, Group Discussions and Interviews. The department of Placement & Training provided job opportunities to students through On-Campus, Off-Campus, Centralized Campus, State level Placement Programme and Job-fairs etc. Placement Office also co-ordinates and helps the students to get project work , arranging in-plant training, industrial visits, projects, guest lecturers and other industry institute activity.</p>
                  <#list ecePlacementList as ecePlacement>              
                  <p>${ecePlacement.heading!}  <a href="${ecePlacement.imageURL!}" class="time_table_link" targrt="_blank"> [view here] </a></p>
                  </#list>	
                  </div>
                </div>											
                  <!-- Department placements -->
                  
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                    <div class="row">
                    <h3>Events</h3>
                    
                    <#list eceEventsList[0..*3] as eceEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${eceEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${eceEvent.heading!}</h3>	                      	 
	                       	 <p>"${eceEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${eceEvent.eventDate!}  </span></p>
	                       	 <a class="btn btn-primary" href="ece_events.html">View more</a>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
  						
                   </div>      
                  </div>
                  <!-- Events  -->
                  
                   <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        
                          <#list eceEresourcesList as eceEresources>
	                    
	                        <p>${eceEresources.heading!}  
	                      	    <a href="//${eceEresources.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
                   <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                       <h3>Teaching and Learning</h3>
                       <p>The faculty of ECE department attends various MOOCS and training programs on advanced topics, update their knowledge and skills, and gives additional inputs in the classes. Further, the faculty conducts various innovative teaching and learning activities inside and outside the classrooms to engage the students effectively and efficiently. The teaching and learning activities conducted by the faculty for the improvement of student learning includes:</p>

						<ul class="list">
							<li>Teaching with working models, simulations, and animated videos.</li>
							<li>Teaching with concept maps, model demo, charts etc.</li>
							<li>Assignments which include mind-maps, mini-projects and case studies.</li>
							<li>Conduction of online and classroom quizzes, role play, surprise class tests, open book assignments, group discussions, flip discussions, pick and speak, seminars, social awareness programmes etc.</li>
							<li>Usage of ICT, Pupilpod and Google classrooms for posting assignments and lecture materials.</li>
							<li>Usage of Google forms and Kahoot for online interaction, assessment, and evaluation.</li>
						</ul>

						<p>The instructional materials and pedagogy activity reports are uploaded in Google drive and the associated links are made available on the institutional website for student and faculty access.</p>
                       
                     
                     
                     
                        <h3>Instructional Materials</h3>
                        
                          <#list eceInstructionalMaterialsList as eceInstructionalMaterials>
	                    
	                        <p>${eceInstructionalMaterials.heading!}  
	                      	    <a href="//${eceInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Lab Manuals</h3>
                        
                          <#list eceLabManualList as eceLabManual>
	                    
	                        <p>${eceLabManual.heading!}  
	                      	    <a href="//${eceLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
               <!-- pedagogy -->

                <div role="tabpanel" class="tab-pane fade" id="pedagogy">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	  <h3>Pedagogical Activities</h3>
                        
                          <#list ecePedagogicalActivitiesList as ecePedagogicalActivities>
	                    
	                        <p>${ecePedagogicalActivities.heading!}  
	                      	    <a href="//${ecePedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogy Review Form</h3>
                        
                          <#list ecePedagogyReviewFormList as ecePedagogyReviewForm>
	                    
	                        <p>${ecePedagogyReviewForm.heading!}  
	                      	    <a href="//${ecePedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>				
                     </div>
                  </div>
               </div>
               <!-- pedagogy -->

               <!-- content beyond syllabus -->

                <div role="tabpanel" class="tab-pane fade" id="content-beyond-syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Content Beyond Syllabus</h3>						
                        
                         <#list ececontentbeyondsyllabusList as ececontentbeyondsyllabus>
                        <p>${ececontentbeyondsyllabus.heading!}  <a href="${ececontentbeyondsyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	                 
                       											
                     </div>
                  </div>
               </div>
              
                <!-- content beyond syllabus -->
               
                 <!-- Project Exhibition -->
               <!--<div role="tabpanel" class="tab-pane fade" id="sponsored-projects">
               	<div class="row">
               		 <h3> Sponsored Projects </h3>
		                  <#list eceSponsoredProjectsList as eceSponsoredProjects>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${eceSponsoredProjects.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${eceSponsoredProjects.name!}</h3>	                      	 
			                       	 <p>Description: ${eceSponsoredProjects.designation!}</p>
			                       	 <a class="btn btn-primary" href="${eceSponsoredProjects.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- Project Exhibition -->
               
               <!-- MOU's signed -->
               <div role="tabpanel" class="tab-pane fade" id="mou-signed">
                <div class="row">
                  <h3>MOUs Signed</h3>
                  <p>Department of Electronics and Communication engineering signed MoUs with various companies for prototype and product development in Research and Development center of ECE department. The companies also provide guidance on mentoring frameworks for startups and setting up of resident incubators. KSIT EC department in collaboration with companies conduct various Faculty Development Programs, Workshops and Certification programmes with substantial focus on technology.</p>
                  <#list eceMouSignedList as eceMouSigned>              
                  <p>${eceMouSigned.heading!}  <a href="${eceMouSigned.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- MOU's signed -->
               
                <!-- Alumni Association -->
               <div role="tabpanel" class="tab-pane fade" id="alumni-association">
                <div class="row">
                  <h3>Alumni Association</h3>
                  <p>The Alumni Association <strong>'Chiranthana'</strong>  is an association of graduates of former students. The purpose of the association is to foster a spirit of loyalty and to promote the general welfare of the organization. Alumni association exist to support the parent organization's goals, and to strengthen the ties between alumni, the community, and the parent organization. The Alumni of the organization continually support the student�s community by presenting the technical talk, workshop, guest lecture continuously. </p>
                  <#list eceAlumniAssociationList as eceAlumniAssociation>              
                  <p>${eceAlumniAssociation.heading!}  <a href="${eceAlumniAssociation.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- Alumni Association -->
               
                <!--  nba -->
                <div role="tabpanel" class="tab-pane fade" id="nba">
	                <div class="row">
						<div class="col-lg-6">
							<h3>NBA Links</h3>
							<table style="width: 100%">
								<#list criteriaMap?values as nbaList>
									<tr>
										<td>
											<ul>
												<li>
													${nbaList[0].criteria.desc}
												</li>
												<li>
													<ul>
														<#list nbaList as  nba>
															<li style="padding: 20px"> - ${nba.heading} <a href="${nba.link}" target="_blank">view here</a></li>												
														</#list>
													</ul>
												</li>
											</ul>
										</td>
									</tr>
								</#list>
							</table>
						</div>	
	                </div>
                </div>								
              <!--  nba --> 
              
                <!-- Professional Bodies  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_bodies">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Bodies</h3>
                        
                        <p>Department of Electronics and communication engineering of KSIT has various professional bodies to help students to take-up innovative projects, present technical papers and organize various technical events thereby receiving a broader perspective on the professional front by providing opportunities for teamwork, leadership and service.</p>
						<p><strong>KSIT IEEE Student branch (STB)</strong> has been recognized as one of the most active student branches by the IEEE Bangalore section. This student chapter has been highly involved in the technical development of students by conducting workshops & tech talks.</p>
						<p><strong>ISTE</strong> formulates & generates goals & responsibilities of technical education. Seminars, workshops, conferences are organized on the topic of relevance to technical education for engineering students as well as teachers.</p>
						<p><strong>IETE</strong> focuses on advancing electro-technology. The vision of IETE is reaching the unreached & empowering youth through technical education & skill development. IETE conducts workshops & technical talks on various tech topics.</p>
						<p><strong>The Institution of Engineers (IEI)</strong> is the national organization of engineers in India. IEI in KSIT promotes an environment to enable ethical pursuits for professional excellence.</p>
						<p><strong>Innovations and Entrepreneurship Development Cell (IEDC)</strong> of KSIT genuinely believes that Entrepreneurship Cell will be able to successfully solve the student problem to a large extent, particularly, students will learn about the industrial perspectives, practices. The Industry, in turn, would gain from the output of KSIEDC and consequently get professionally trained and more usefully employable engineers from this institute. This initiative is thus expected to lead to a win-win situation for all concerned: KSIT, the students of KSIT, the Industry and the nation.</p>


						<h3>AIMS & OBJECTIVES OF IEDC:</h3>
						<ul class="list">
							<li>To promote Entrepreneurship as a means to achieve self reliance    & socio-economic independence.</li>
							<li>To provide support & guidance to aspiring Entrepreneurs of KSIT. </li>
						    <li>To augment the supply of Entrepreneurs through education& training. </li>
							<li>To improve the managerial capabilities of small Entrepreneurs.</li>
						    <li>To be a centre of learning for trainer-motivators on Entrepreneurship Development.</li>
						</ul>                        
						                        
           				<p><strong>Garut Aeromodeling Club (GAC)</strong> in KSIT provides the students with various aero modeling related workshops, technical talks & promotes environment to professional excellence.</p>
                        
                        
                        
                        <#list eceProfessionalBodiesList as eceProfessionalBodies>
                        <p>${eceProfessionalBodies.heading!}  <a href="${eceProfessionalBodies.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Professional Bodies   -->
           
           
           	<!-- Professional Links  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_links">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Links</h3>
                        
                         <#list eceProfessionalLinksList as eceProfessionalLinks>
	                    
	                        <p>${eceProfessionalLinks.heading!}  
	                      	    <a href="//${eceProfessionalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                												
                     </div>
                  </div>
               </div>
               <!-- Professional Links   -->
           
                 <!-- Higher Education  -->
               <div role="tabpanel" class="tab-pane fade" id="higher_education">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Higher Education & Entrepreneurship</h3>
                        <p>A higher study usually refers to education and training in universities, college and Institute of Technology or art. Higher education a tertiary education to award of an academic degree. KSIT ECE department students gained the knowledge in U.G programme, pursuing their Masters in India and various abroad universities through the entrance exams like GATE, GRE, TOFEL, IELTS, etc.</p>
                        <#list eceHigherEducationList as eceHigherEducation>
                        <p>${eceHigherEducation.heading!}  <a href="${eceHigherEducation.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Higher Education   -->
           
              
              
                 <!-- Students Club  -->
               <div role="tabpanel" class="tab-pane fade" id="student_club">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Garut Aeromodeling Club (GAC)</h3>
                     
                     
                        <h3>Club Activities</h3>
                        <#list eceClubActivitiesList as eceClubActivities>
                        <p>${eceClubActivities.heading!}  <a href="${eceClubActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>External Links (Club)</h3>	
                        
                         <#list eceClubExternalLinksList as eceClubExternalLinks>
	                    
	                        <p>${eceClubExternalLinks.heading!}  
	                      	    <a href="//${eceClubExternalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                        			
                        
                        							
                     </div>
                  </div>
               </div>
               <!-- Students Club   -->
           
               
                 <!-- Internship  -->
               <div role="tabpanel" class="tab-pane fade" id="internship">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Internship</h3>
                        <p>An internship is an opportunity offered by an employer to potential employees, called interns, to work at a firm for a fixed period of time. The interns usually will undergo internships which usually last between two to four weeks. Internships are usually part-time if offered during a university semester and full-time if offered during the vacation periods. An internship should give the practical skills, workplace experience and greater knowledge of that industry. The Internships are more flexible and they provide an opportunity for the students take up in different companies, work in different roles and really explore an industry in depth. The Internship will also provide an opportunity to get the employment and also to understand the industrial culture.</p>
                        
                        
                        <#list eceInternshipList as eceInternship>
                        <p>${eceInternship.heading!}  <a href="${eceInternship.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                  		<h3>Projects</h3>
                        <#list eceProjectsList as eceProjects>
                        <p>${eceProjects.heading!}  <a href="${eceProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Mini-Projects</h3>                       
                        <#list eceMiniProjectsList as eceMiniProjects>
                        <p>${eceMiniProjects.heading!}  <a href="${eceMiniProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        											
                     </div>
                  </div>
               </div>
               <!-- Internship   -->
               
               <!-- Social Activities  -->
               <div role="tabpanel" class="tab-pane fade" id="social_activities">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Social Activities</h3>
                        <#list eceSocialActivitiesList as eceSocialActivities>
                        <p>${eceSocialActivities.heading!}  <a href="${eceSocialActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Social Activities  -->   
               
                 <!-- Other Details  -->
               <div role="tabpanel" class="tab-pane fade" id="other_details">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Other Details</h3>
                        <#list eceOtherDetailsList as eceOtherDetails>
                        <p>${eceOtherDetails.heading!}  <a href="${eceOtherDetails.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Other Details  --> 
               
                    <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list eceFdpList as eceFdp>
                        <p>${eceFdp.heading!}  <a href="${eceFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
           
           
             <!-- Industrial Visit -->
               <div role="tabpanel" class="tab-pane fade" id="industrial_visit">
               	<div class="row">
               		 <h3>Industrial Visit </h3>
               		 <p>The department of ECE organizes industrial visits every year in association with professional bodies. Industrial visits offer a great source to gain practical knowledge. Students can observe and learn as to how theoretical concepts are put into action, thereby aiding their practical learning. Students are exposed to real world environment and shown how things are done in an organization. From the details about the management to the targets they achieve everything is covered in these visits. The objective is to let student to know things practically through interaction, working methods, industrial and employment standards, and practices.</p>
		                  <#list eceIndustrialVisitList as eceIndustrialVisit>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${eceIndustrialVisit.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${eceIndustrialVisit.name!}</h3>	                      	 
			                       	 <p>${eceIndustrialVisit.designation!}</p>
			                       	 <a class="btn btn-primary" href="${eceIndustrialVisit.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Industrial Visit -->
           
           
            <!-- Project Exhibition -->
               <div role="tabpanel" class="tab-pane fade" id="project_exhibition">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
               		  <p>Every year, Project exhibition for final year students & mini project exhibition for other students will be conducted during even semester. Judges from Industry and academics will be invited to assess the projects during the project exhibition. This will enable students to show case their projects, get feedback about project & understanding possible new features that can be included in the project.</p>
               		 
		                  <#list eceProjectExhibitionList as eceProjectExhibition>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${eceProjectExhibition.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${eceProjectExhibition.name!}</h3>	                      	 
			                       	 <p>Description: ${eceProjectExhibition.designation!}</p>
			                       	 <a class="btn btn-primary" href="${eceProjectExhibition.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Project Exhibition -->
               
               
            
               
               
               
               
               
                  
                  <!--  -->
                  <div role="tabpanel" class="tab-pane fade" id="Section4">
                     <h3>Section 4</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
                  </div>
                  <!--  -->
	            
	            </div>
	            <!-- Tab panes -->
	            
	           </div>
            </div>
         </div>
      </div>
</section>
<!--dept tabs -->

	
	
	

	
	
	
	
	 
</@page>