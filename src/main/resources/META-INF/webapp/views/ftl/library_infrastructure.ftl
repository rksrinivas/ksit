<@page>
	 
	 <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs_course">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="aboutus_area wow fadeInLeft">
            <h2 class="titile"> Infrastructure</h2>
				
				
				<p>The library is located on the first and second floor of Administrative block and is spacious, spreading over 9500 sq. ft.
				 It is well lit and adequately ventilated. The location allows easy access and offers a seating capacity for 200 users at a time.</p>
				
				
			</div>
        </div>
        
	  
			   
          </div>
        </div>
	  
	 
	
	
	  
	  </div>
	  </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 

	 <!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery" style="margin-top:0%;background-color: #686464;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
			
				<h3>First floor</h3>
				
                <a href="${img_path!}/library/infrastructure/CIRCULATION.JPG" title="Circulation counter">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/CIRCULATION.JPG" alt="img" />
                <span class="view_btn">View</span>
				
                </a>
				
                <a href="${img_path!}/library/infrastructure/STACK AREA.jpg" title="Stack Area">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/STACK AREA.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
                <a href="${img_path!}/library/infrastructure/technical section.jpg" title="Technical processing section">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/technical section.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
                <a href="${img_path!}/library/library_staff/library.jpg" title="Librarian office">
                  <img class="gallery_img" src="${img_path!}/library/library_staff/library.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
                <a href="${img_path!}/library/infrastructure/NEW ARRIVAL SECTION.jpg" title="New arrival section">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/NEW ARRIVAL SECTION.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
				
				<h3>Second floor</h3>
                <a href="${img_path!}/library/infrastructure/REFERENCE SECTION.jpg" title="Reference Section">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/REFERENCE SECTION.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
				 <a href="${img_path!}/library/infrastructure/discussion room.jpg" title="Reading and discussion Room">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/discussion room.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
                				
                <a href="${img_path!}/library/infrastructure/JOURNAL SECTION.jpg" title="Journals section">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/JOURNAL SECTION.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
                <a href="${img_path!}/library/infrastructure/NEWS PAPER SECTION.jpg" title="News paper section">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/NEWS PAPER SECTION.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
				<h3>Digital Library</h3>
				
				<a href="${img_path!}/library/infrastructure/DIGITAL LIBRARY.jpg" title="Digital Library">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/DIGITAL LIBRARY.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
				<h3>Electronic Surveillances</h3>
				
				<a href="${img_path!}/library/infrastructure/STACK AREA.jpg" title="Electronic Surveillances">
                  <img class="gallery_img" src="${img_path!}/library/infrastructure/STACK AREA.jpg" alt="img" />
                  <span class="view_btn">View</span>
                </a>
				
               
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
	 
	 
</@page>