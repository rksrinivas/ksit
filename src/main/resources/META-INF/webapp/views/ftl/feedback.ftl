<@page>
	        <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact">
     <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="titile text-center">Feedback </h2>
              <span></span> 
            <!--  <p>The Kammavari Sangham, a multi-activity non-profit oriented voluntary service organization, was established in the year 1952 with the sole objective of providing charitable service to community and society. The Sangham has diversified its activities since its establishment over five decades ago.</p>-->
            </div>
          </div>
       </div>
       <div class="row">
         <div class="col-lg-8 col-md-8 col-sm-8">
         
           <div class="contact_form wow fadeInLeft">
           
           	  <form action="" class="submitphoto_form" method="post">
                <input type="text" class="wp-form-control wpcf7-text" name="name" placeholder="Your name" required>
                <input type="email" class="wp-form-control wpcf7-email" name="email" placeholder="Email address" required>
				<input type="phone" class="wp-form-control wpcf7-phone" name="phone" placeholder="Phone No" required>                    
                <input type="text" class="wp-form-control wpcf7-text" name="subject" placeholder="Subject" required>
                <textarea class="wp-form-control wpcf7-textarea" name="description" cols="30" rows="10" placeholder="What would you like to tell us" required></textarea>
                <input type="submit" value="Submit" class="wpcf7-submit">
              </form>
              <br/>
           </div>
           
           
         </div>
         </div>
       </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->
	
</@page>