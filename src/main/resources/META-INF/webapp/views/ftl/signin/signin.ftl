<@pageAdmin>
<style>
	#header{
		position: relative;
	}
	a {
    	text-decoration: none;
    	color: #0645ad;
    	background: none;
	}
	.error {
		color: red;
	}
	#login-btn:hover{
		transform:scale(1.04);
		font-weight:600;
		transition:ease-in 0.1s;
	}
	#signin-title{
		margin-left:15px;
		margin-bottom:15px;
	}
</style>

<div class="form-container">
	<div class="container">
			<br/><br/><br/><br/>
		<#if errorMessage?has_content>
			<h4 class="error">${errorMessage!}</h4>
		</#if>
		<#if loggedInUser?has_content>
			<h3>
				<i class="fa fa-unlock-alt" style="color:#171973;"></i> Welcome ${loggedInUser.username!}
			</h3>
		<#else>
			<h3 id="signin-title"><i class="fa fa-lock" style="color:#171973;"></i> Sign In</h3>
			<form action="signin" method="post">
				<div class="col-xs-7">
					<div class="form-group">
						<label for="username">User Name</label>
						 <input type="text" class="form-control" id="username" name="username" required>
					</div>
					<div class="form-group">
						<label for="password">Password</label> <input type="password" class="form-control" id="password" name="password" required>
					</div>
					<div class="form-group">
						  <input id="login-btn" type="submit" value="Login">
					</div>
				</div>
			</form>
		</#if> 
	</div>
</div>
</@pageAdmin>