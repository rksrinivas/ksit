<@page>
<#include "feedback/feedback_list_modal.ftl">
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/profile.php?id=100083069814157" target="_blank"><i class="fa fa-facebook"></i></a></li>
			               <!--   <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li> -->
			                  <li><a data-toggle="tooltip" data-placement="top" title="instagram" class="soc_tooltip go"  href="https://www.instagram.com/ksit.official/" target="_blank"><i class="fa fa-instagram"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->

                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#if csdPageSliderImageList?has_content>
                     		<#list csdPageSliderImageList as csdPageSliderImage>
                     			<#if csdPageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${csdPageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">${csdPageSliderImage.heading}</p>
										<p class="slideInRight">${csdPageSliderImage.content}</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>
	                        <article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/aiml/slider/aiml_slider1.jpg" alt="">                       
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/aiml/slider/aiml_slider2.jpg" alt="">                          
	                        </article>
	                        
	                         <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/aiml/slider/aiml_slider3.jpg" alt="">                          
	                        </article>
						</#if>
                    
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 

<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 

<!-- upcoming and latest news list-->
      
<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at CS &amp; D
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
					
				<div class="col-md-12">
					<div class="row">

					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=csdPageLatestEventsList/>
									</div>
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
									<@eventDisplayMacro eventList=csdPageUpcomingEventsList/>
								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->
	
	<!-- welcome -->
	<div class="dept-title">
	   <h1>Welcome To Computer Science and Design</h1>
	   <p class="welcome-text">In the internet economy, all businesses are focused on engaging the user. A successful user engagement leads to increased revenue and profits. Any distraction results in business loss and bad reputation, often accompanied by an onslaught on social media. The CSD course contains two themes that address these two aspects, i.e., User Interface (UI) design and User Experience (UX) design.</p>
	</div>
	<!-- welcome -->
	


<!--dept tabs -->
<section id="vertical-tabs">
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
         <div class="vertical-tab padding" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
               <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="dept-cart">Profile</a></li>
               <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision & Mission, PEO, PSO & PO</a></li>
        <!--     <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>  -->
              
                <li role="presentation"><a href="#professional_bodies" aria-controls="professional_bodies" role="tab" data-toggle="tab" class="dept-cart">Professional Bodies</a></li>
               <li role="presentation"><a href="#professional_links" aria-controls="professional_links" role="tab" data-toggle="tab" class="dept-cart">Professional Links</a></li>
              
              
               <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
               <li role="presentation"><a href="#pedagogy" aria-controls="pedagogy" role="tab" data-toggle="tab" class="dept-cart">Pedagogy</a></li>                              
               <li role="presentation"><a href="#content-beyond-syllabus" aria-controls="content-beyond-syllabus" role="tab" data-toggle="tab" class="dept-cart">Content Beyond Syllabus</a></li>
               
               <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Time Table</a></li>
               
               <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>
               
                              
          <!--     <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li>  -->
               <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
              
               <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li>
              
              
               <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab" class="dept-cart">Achievers</a></li>
               <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
               <li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
               <li role="presentation"><a href="#library_dept" aria-controls="library" role="tab" data-toggle="tab" class="dept-cart">Departmental Library</a></li>
        <!--       <li role="presentation"><a href="#placement_dept" aria-controls="placement" role="tab" data-toggle="tab" class="dept-cart">Placement Details</a></li> 
               
               <li role="presentation"><a href="#higher_education" aria-controls="higher_education" role="tab" data-toggle="tab" class="dept-cart">Higher Education</a></li>  -->

           <li role="presentation"><a href="#student_club" aria-controls="student_club" role="tab" data-toggle="tab" class="dept-cart">Student Club</a></li>


              <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab" class="dept-cart">Internships & Projects </a></li>  
               
               <li role="presentation"><a href="#industrial_visit" aria-controls="industrial_visit" role="tab" data-toggle="tab" class="dept-cart">Industrial Visit</a></li>
             
             
               
               <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
               <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
               
               
         
  
            <!--   <li role="presentation"><a href="#project_exhibition" aria-controls="project_exhibition" role="tab" data-toggle="tab" class="dept-cart">Project Exhibition</a></li> -->
               
               <li role="presentation"><a href="#mou-signed" aria-controls="mou-signed" role="tab" data-toggle="tab" class="dept-cart">MOUs Signed</a></li>
        <!--       <li role="presentation"><a href="#alumni-association" aria-controls="alumni-association" role="tab" data-toggle="tab" class="dept-cart">Alumni Association</a></li>  
               
               <li role="presentation"><a href="#social_activities" aria-controls="social_activities" role="tab" data-toggle="tab" class="dept-cart">Social Activities</a></li>
               <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="tab" class="dept-cart">Testimonials</a></li>  -->
               
			<li role="presentation"><a href="#other_details" aria-controls="other_details" role="tab" data-toggle="tab" class="dept-cart">Other Details</a></li>              
                                 
               
               <!-- <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li> -->
            </ul>
            <!-- Nav tabs -->
            <!-- Tab panes -->
            <div class="tab-content tabs">
            
                  	
            	
               <!-- profile -->
               <div role="tabpanel" class="tab-pane fade in active" id="profile">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Profile</h3>
                     <p>The Department of <a href="https://ksgi.edu.in/Best-College-For-Computer-Science-and-Design" class="font-weight-600" target="_blank">Computer Science &amp; Design</a> (CSD) is established in the year 2021.</p>
						
						<p>
                     The current intake of the UG program is 60. 
                     The main focus of the department is to produce graduates with expertise in Computer Science and Design domain. 
                     The college is accredited by National Assessment and Accreditation Council (NAAC). 
                     The department has qualified, experienced & committed teaching faculty members with various specializations. Well-qualified faculties provide qualitative education.
                     Due to the quality of teaching, the students of the department are the top performers in the university. The outcome-based education helps students to achieve excellence in academics.

                  </p>
						
						<p>
                  The department has well-equipped laboratories with state-of-the-art equipment and software tools. Active professional bodies like CSI, ISTE, IEI, and IEEE help to conduct FDPs and workshops for the faculties to comprehend and upgrade to the latest technologies. 
                  </p>

						<p>
                  Activities such as technical conferences, hackathons, Coding Contests, Seminars, technical talks, guest lectures, and Workshops are carried out for the students under professional bodies and department clubs to boost their technical skills and knowledge. These events provide the students to interact with industry professionals and hone up their skills. This helps the students to analyze real-life problems and increases their eagerness to develop solutions for the same. 
                  </p>

                 
			       </div>
                  <h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
						<img class="hod-img img-responsive" src="/img/csd/csd_hod.jpg" alt="image">          
						<div class="qualification_info">
	                        <h3 class="qual-text">Dr. Deepa. S. R</h3>
	                        <h4 class="qual-text">Professor & Head</h4>
	                        <h3 class="qual-text">B.E, M.Tech, Ph.D</h3>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>The Department of Computer Science & Design offers B.E  program.
                           The department has well-qualified, knowledgeable, and dedicated faculty to provide qualitative, Industry tuned education to students. 
                           The department organizes various events like workshops, technical talks for students to interact with Industry Professionals and hone up their skills.  
                           This empowers them to think beyond regular course curriculum and possibly instill in them the concept to start on entrepreneurial journey.
                     </p>
                     <p>
                     Department staff and students are members of Professional bodies like ISTE, IEI, CSI & IEEE. The department club encourages showcasing student's talent in programming, debugging, teamwork and enhances their skills. 
                     </p>
						
                     </div>
                     
                    <!-- <div class="col-lg-12 col-md-12 col-sm-12">
                     	<p>The dept continues to engage with industry in many ways. We do provide consultancy as well sign Memorandum of Understanding (MoU). The consultancy mechanism provides an opportunity to faculty to work on current real life situations that required efficient technology solutions. The MoUs would enable our faculty and students to collaborate with industry to develop prototypes and engage in research to find practical workable solutions.
                     </p>
                     </div> -->
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>UG Program : </h3>
                     <p>
                        Bachelor of Engineering (B.E.) in Computer Science & Design (CS&D) with an approved intake of 60 students and a lateral entry of 10% from 2nd year.                   
                    </p>

                     <h3>Highlights of the department: </h3>
                      <ul class="list">
                       		<li>Experienced and dedicated faculties</li>
                       		<li>Faculties are provided with the latest computer, laser printer, scanner, high-speed internet connection, etc.</li>
                       		<li>State of Art Classrooms & laboratory facilities.</li>
                       		<li>Overall development of students through the conduction of various activities.</li>
                       		<li>Guest Lectures from Industry and Academia</li>
                       		
                       		
                       </ul>  
                   <h3>Career Opportunities : </h3>
                     <p>A great career catalyst for professionals.</p>
                     <p>A computer science and design graduate has manifold and multi-level opportunities in terms of career growth. Opportunities in segments of the digital media industry such as Gaming, animation, Augmented/Virtual reality, etc. </p>
                     
                  </div>
   
               </div>
               </div>
               <!-- profile -->
               
               <!-- vision and mission -->
               <div role="tabpanel" class="tab-pane fade in" id="vision">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Vision</h3>
				
						<ul class="list">
							<li>To prepare skilled and distinct professionals in Computer Science & Design with good  research skills backed by ethics for addressing societal needs. </li>
						</ul>
				<!--		<button type="button" style="float:right;" class="btn btn-info btn-sm" data-backgrop="static" data-toggle="modal" data-page="tele_page" data-target="#feedbackListModal">Feedback</button>  -->
						
				 		 <h3>Mission</h3>
						
						<ul class="list">
						<li>Instill a commitment to enhance innovative design skills on par with the technological advancements in the IT industry.</li>
						<li>To make efforts to explore the research ideas in hardware and software design.</li>
						<li>To groom the students with the design techniques to cater to the needs of society with professional ethics.</li>
						</ul>
					
			
					
						
						
						
						<h3>PROGRAM EDUCATIONAL OBJECTIVES (PEOs)</h3>	
						<p><b>PEO1:</b> Exhibit design skills and become an excellent employee in the interdisciplinary field.
</p>
						<p><b>PEO2:</b>  Exhibit creative thinking and continue learning through higher studies and research.
</p>
						<p><b>PEO3:</b>  Proficient in solving real-life problems related to innovation through teamwork and ethics.
 </p>
					
						<h3>PROGRAM SPECIFIC OUTCOMES (PSOs)</h3>
						<p><b>PSO1:</b> Ability to Utilize Concepts and Practices of computer science & design to develop solutions.
</p>
						<p><b>PSO2:</b> Ability to demonstrate design and development skills to solve problems in the broad area of programming concepts and real-world challenges. 
</p>

<h3>PROGRAM OUTCOMES (POs)</h3>	
                  <p><b>PO1 : </b>Engg Knowledge : Apply the knowledge of mathematics, science, engineering fundamentals, and engg. specialization to the solution of complex engineering problems. </p>
                  <p><b>PO2 : </b>Problem Analysis : Identify, formulate, research literature, and analyze engineering problems to arrive at substantiated conclusions using first principles of mathematics, natural, and engineering sciences. 
</p>
                  <p><b>PO3 : </b>Design/Development : Design solutions for complex engineering problems and design system components, processes to meet the specifications with consideration for the public health and safety, and the cultural, societal, and environmental considerations. 
</p>
                  <p><b>PO4 : </b>Conduct Investigations : Use research-based knowledge including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions. 
</p>
                  <p><b>PO5 : </b>Modern tool usage : Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations. 
</p>
                  <p><b>PO6 : </b>Engineer and Society : Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations. 
</p>
                  <p><b>PO7 : </b>Environment : Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development .
</p>
                  <p><b>PO8 : </b>Ethics : Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice .
</p>
                  <p><b>PO9 : </b>Individual and team work  : Function effectively as an individual, and as a member or leader in teams, and in multidisciplinary settings. 
</p>
                  <p><b>PO10 : </b>Communication : Communicate effectively with the engineering community and with society at large. Be able to comprehend and write effective reports documentation. Make effective presentations, and give and receive clear instructions. 
</p>
                  <p><b>PO11 : </b>Project Mgmt : Demonstrate knowledge and understanding of engineering and management principles and apply these to one's own work, as a member and leader in a team. Manage projects in multidisciplinary environments. 
</p>
                  <p><b>PO12 : </b>Lifelong learning : Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change. 
</p>
												  
                  </div>
               </div>
               <!-- vision and mission -->
				               
               
               <!-- Research and development -->
      <!--         <div role="tabpanel" class="tab-pane fade" id="research">
                  <h3>Research and Development</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
               						 <h3>R & D Activities</h3>
                        
                        <#list csdResearchList as csdResearch>
                        	<p>${csdResearch.heading!}  <a href="${csdResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        <p>Renowned projects have been awarded with cash prizes by prestigious government and private bodies to encourage students.</p>
                        
                        <#list csdSponsoredProjectsList as csdSponsoredProjects>
                        	<p>${csdSponsoredProjects.heading!}  <a href="${csdSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        
                        <#list csdPhdPursuingList as csdPhdPursuing>
                        	<p>${csdPhdPursuing.heading!}  <a href="${csdPhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        
                        <h3>Ph.D Awardees</h3>
                        
                         <#list csdPhdAwardeesList as csdPhdAwardees>
                        	<p>${csdPhdAwardees.heading!}  <a href="${csdPhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                    
					
                     	
                     
                     </div>
                  </div>
               </div>   -->
               <!-- Research and development -->
               
               
               <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="row">
                     <h3>Time Table</h3>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list csdClassTimeTableList as csdClassTimeTable>              
                        <p>${csdClassTimeTable.heading!}  <a href="${csdClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list csdTestTimeTableList as csdTestTimeTable>
                        <p>${csdTestTimeTable.heading!}  <a href="${csdTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list csdCalanderList as csdCalander>              
                        <p>${csdCalander.heading!}  <a href="${csdCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--Time Table  -->
               
            <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>Syllabus</h3>
                     
                        <h3>B.E in CS & D</h3>
                        
                        <#list csdUgSyllabusList as csdUgSyllabus>
                        <p>${csdUgSyllabus.heading!}  <a href="${csdUgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                        
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
             
               
               
               
               <!--News Letter  -->
      <!--         <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Newsletter</h3>
                        
                        <p>The main purpose of newsletter is to encourage students and faculties to involve themselves in overall development. Newsletter includes the vision-mission of college and department along with encouraging words from Principal & HOD.
                        </p>
						<p>News letter mainly projects the events conducted and attended by students & Staff. It also throws light on the latest technology and views of Alumni about the department & Telecommunication Engineering</p>
                        
                        
                        <#list csdNewsLetterList as csdNewsLetter>
                        <p>${csdNewsLetter.heading!}  <a href="${csdNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>  -->
               <!--News Letter  -->
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="row">
                     <h3>Faculty</h3>
                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Faculty</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name}</p>
                                 <p>${faculty.designation}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id!}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               <!--testimonials  -->
           <!--    <div role="tabpanel" class="tab-pane fade" id="testimonials">  -->
                  <!--slider start-->
             <!--     <div class="row">
                     <div class="col-sm-8">
                        <h3><strong>Testimonials</strong></h3>  -->
                        <!--<div class="seprator"></div>-->
                  <!--      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">  -->
                          
                           <!-- Wrapper for slides -->
                     <!--      <div class="carousel-inner">
                              <#list testimonialList as testimonial>
                              <div class="item <#if testimonial?is_first>active</#if>">
                                 <div class="row" style="padding: 20px">
                                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                    <p class="testimonial_para">${testimonial.content!}</p>
                                    <br>
                                    <div class="row">
                                       <div class="col-sm-2">
                                          <img src="${testimonial.imageURL!}" class="img-responsive" style="width: 80px">
                                       </div>
                                       <div class="col-sm-10">
                                          <h4><strong>${testimonial.heading!}</strong></h4>  -->
                                          <!--<p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                             <span>Officeal All Star Cafe</span>-->
                            <!--              </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </#list>  
                           </div>
                           <div class="controls testimonial_control pull-right">									            	
                              <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>    -->
                  <!--slider end -->
          <!--     </div>   -->
               <!--testimonials  -->
 
 
 			 <!-- Achievers -->
               <div role="tabpanel" class="tab-pane fade" id="achievers">
               	<div class="row">
               		 <h3>Department Achievers</h3>
                     <!--Department Achievers-->
		                  <#list csdDepartmentAchieversList as csdDepartmentAchievers>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csdDepartmentAchievers.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${csdDepartmentAchievers.heading!}</h3>	                      	 
			                       	 <p>${csdDepartmentAchievers.eventDate!} </p>
			                       	 <p> ${csdDepartmentAchievers.content!}</p>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list> 
		                    
		              <h3>List of FCD Students</h3>
		              <#list csdFcdStudentsList as csdFcdStudents>
                        <p>${csdFcdStudents.heading!}  <a href="${csdFcdStudents.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                      </#list>  
                    
               	</div>
               </div>
               <!-- Achievers -->

 				
 
               
                      <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="row">
               		
               		<h3>Infrastructure / Facilities</h3>
               		 <p>The department has good ambience and is wifi enabled. The laboratories have state-of-art equipments, and computer systems with higher end configuration. Safety measures are incorporated in the department</p>
               	
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
		                  <#list csdLabList as csdLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csdLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${csdLab.name!}</h3>	                      	 
			                       	 <p>${csdLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csdLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
               
               
               
              <!-- Gallery  -->
               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
                  <h3>Gallery</h3>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="gallerySLide" class="gallery_area">
                           <#list csdGalleryList as csdGallery>	
                           <a href="${csdGallery.imageURL!}" target="_blank">
                           <img class="gallery_img" src="${csdGallery.imageURL!}" alt="" />					                    			                   
                           <span class="view_btn">${csdGallery.heading!}</span>
                           </a>
                           </#list>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Gallery -->
               
                <!-- Dept Library  -->
               <div role="tabpanel" class="tab-pane fade" id="library_dept">
                  <h3>Departmental Library</h3>
                  <p>The department has its own library which has more than 500 text books and reference book catering to the needs of students as well as Teaching Staffs. The library preserves previous year's project, internship reports, journals; dissertation and seminar reports and the books contributed by Alumnis and these are available for students and staffs. Every year 50 to 100 books are being added to the stock. The students and Staffs borrow books from the library periodically. The department maintains separate register for borrowing books.
                  </p>
                  <h2 class="titile">Collection Of books</h2>
                  <table class="table table-striped course_table">
                     <#list csdLibraryBooksList as csdLibraryBooks>
                     <tr>
                        <th style="width:50%">${csdLibraryBooks.heading!}</th>
                        <td>${csdLibraryBooks.content!}</td>
                     </tr>
                     </#list>
                  </table>
               </div>
               <!--  Dept Library   -->
               
               <!-- Department placements -->
               <div role="tabpanel" class="tab-pane fade" id="placement_dept">
                  <h3>Placement Details</h3>
                  
                  <p>Placement Training will be provided to all semester students at the beginning of the semester.
                  </p>
                  <p>Placement Drives will be arranged for final year students from August. Company Specific
                  </p>
                  <p>Trainings will be arranged for final year students to grab more offers in their dream Companies.
                  </p>
                  <p> Technical training sessions will be provided to Pre-final Year students.
                  </p>
                  
                  
                  
                  
                  <#list csdPlacementList as csdPlacement>              
                  <p>${csdPlacement.heading!}  <a href="${csdPlacement.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>											
                  <!-- Department placements -->
                  
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                  
                    <h3>Events</h3>
                    
                    <#list csdEventsList[0..*3] as csdEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${csdEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${csdEvent.heading!}</h3>	                      	 
	                       	 <p>"${csdEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${csdEvent.eventDate!}  </span></p>
	                       	 <a class="btn btn-primary" href="csd_events.html">View more</a>
	                       	 <#if (csdEvent.reportURL)??>
	                       	 	<a class="title-red" href="${csdEvent.reportURL!}" target="_blank">Report</a>
	                       	 </#if>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
  						
                        
                  </div>
                  <!-- Events  -->
                  
                      <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        
                          <#list csdEresourcesList as csdEresource>
	                    
	                        <p>${csdEresource.heading!}  
	                      	    <a href="//${csdEresource.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
                              
               <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     <h3>Teaching and Learning</h3>
                     
                        <h3>Instructional Materials</h3>
                        
                          <#list csdInstructionalMaterialsList as csdInstructionalMaterials>
	                    
	                        <p>${csdInstructionalMaterials.heading!}  
	                      	    <a href="//${csdInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Lab Manuals</h3>
                        
                          <#list csdLabManualList as csdLabManual>
	                    
	                        <p>${csdLabManual.heading!}  
	                      	    <a href="//${csdLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                    
	                        
                        
                       											
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
                <!-- pedagogy -->

                <div role="tabpanel" class="tab-pane fade" id="pedagogy">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                          <h3>Pedagogical Activities</h3>
                         <p>Use of Slide projector.</p>
                         <p>Multimedia content including video and audio.</p>
                         <p>Use of Physical inexpensive material to conduct exercises to explain problem.</p>
                        
                          <#list csdPedagogicalActivitiesList as csdPedagogicalActivities>
	                    
	                        <p>${csdPedagogicalActivities.heading!}  
	                      	    <a href="//${csdPedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogy Review Form</h3>
                        
                          <#list csdPedagogyReviewFormList as csdPedagogyReviewForm>
	                    
	                        <p>${csdPedagogyReviewForm.heading!}  
	                      	    <a href="//${csdPedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                       											
                     </div>
                  </div>
               </div>
              
                <!-- pedagogy -->

               <!-- content beyond syllabus -->

                <div role="tabpanel" class="tab-pane fade" id="content-beyond-syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Content Beyond Syllabus</h3>						
                        
                         <#list csdcontentbeyondsyllabusList as csdcontentbeyondsyllabus>
                        <p>${csdcontentbeyondsyllabus.heading!}  <a href="${csdcontentbeyondsyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	                 
                       											
                     </div>
                  </div>
               </div>
              
                <!-- content beyond syllabus -->
               
               
               
               <!-- MOU's signed -->
               <div role="tabpanel" class="tab-pane fade" id="mou-signed">
                <div class="row">
                  <h3>MOUs Signed</h3>
                  <p>MoU's have been signed with companies to enhance the practical knowledge of students and have hands on experience with the live ongoing projects in companies.</p>
                  <#list csdMouSignedList as csdMouSigned>              
                  	<p>${csdMouSigned.heading!}  <a href="${csdMouSigned.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- MOU's signed -->
               
                <!-- Alumni Association -->
           <!--    <div role="tabpanel" class="tab-pane fade" id="alumni-association">
                <div class="row">
                  <h3>Alumni Association</h3>
                  <p>CHIRANTHANA with a tagline of EVERLASTING FOREVER is the name given to KSIT ALUMNI ASSOCIATION, with a motto of uniting all the alumni members under the umbrella of KSIT and growing together forever.KSIT alumni association was started in the year 2014.The association provides a platform to their alumni�s to share their knowledge & experience with the budding engineers through technical talks, seminars & workshops.</p>
                  <#list csdAlumniAssociationList as csdAlumniAssociation>              
                  	<p>${csdAlumniAssociation.heading!}  <a href="${csdAlumniAssociation.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>		-->									
               <!-- Alumni Association -->
               
               
               <!-- Professional Bodies  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_bodies">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Bodies</h3>
                        <p>We in our department have several professional bodies like IEEE, IEI, TEF and IETE. Several events and workshops are conducted under these bodies to enrich the knowledge of students in various fields.</p>
                        <#list csdProfessionalBodiesList as csdProfessionalBodies>
                        	<p>${csdProfessionalBodies.heading!}  <a href="${csdProfessionalBodies.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Professional Bodies   -->
           
           
           	<!-- Professional Links  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_links">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Links</h3>
                        
                         <#list csdProfessionalLinksList as csdProfessionalLinks>
	                    
	                        <p>${csdProfessionalLinks.heading!}  
	                      	    <a href="//${csdProfessionalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                												
                     </div>
                  </div>
               </div>
               <!-- Professional Links   -->
           
                 <!-- Higher Education  -->
               <div role="tabpanel" class="tab-pane fade" id="higher_education">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Higher Education</h3>
                        <#list csdHigherEducationList as csdHigherEducation>
                        	<p>${csdHigherEducation.heading!}  <a href="${csdHigherEducation.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Higher Education   -->
           
              
              
                 <!-- Students Club  -->
               <div role="tabpanel" class="tab-pane fade" id="student_club">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>CSD forum</h3>
                     
                     
                        <h3>Club Activities</h3>
                        <#list csdClubActivitiesList as csdClubActivities>
                        	<p>${csdClubActivities.heading!}  <a href="${csdClubActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>External Links (Club)</h3>	
                        
                         <#list csdClubExternalLinksList as csdClubExternalLinks>
	                    
	                        <p>${csdClubExternalLinks.heading!}  
	                      	    <a href="//${csdClubExternalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
                        			
                        
                        							
                     </div>
                  </div>
               </div>
               <!-- Students Club   -->
           
               
                 <!-- Internship  -->
               <div role="tabpanel" class="tab-pane fade" id="internship">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Internship</h3>
                        <p>Students are facilitated by providing list of companies /Industries, where they can undergo 4 weeks of Internship/ Professional practice. College takes initiative to organize in house internship by calling resource persons from industry. Student cumulates their learning in a report on their internship/ professional practice and gives a presentation in front of internship coordinator and the guide.</p>
                        <#list csdInternshipList as csdInternship>
                        	<p>${csdInternship.heading!}  <a href="${csdInternship.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                  
             			<h3>Projects</h3>
                        <#list csdProjectsList as csdProjects>
                        	<p>${csdProjects.heading!}  <a href="${csdProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Mini-Projects</h3>                       
                        <#list csdMiniProjectsList as csdMiniProjects>
                        	<p>${csdMiniProjects.heading!}  <a href="${csdMiniProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        											
                     </div>
                  </div>
               </div>
               <!-- Internship   -->
               
               <!-- Social Activities  -->
               <div role="tabpanel" class="tab-pane fade" id="social_activities">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Social Activities</h3>
                        <#list csdSocialActivitiesList as csdSocialActivities>
                        	<p>${csdSocialActivities.heading!}  <a href="${csdSocialActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Social Activities  -->
               
               <!-- Other Details  -->
               <div role="tabpanel" class="tab-pane fade" id="other_details">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Other Details</h3>
                        <#list csdOtherDetailsList as csdOtherDetails>
                        	<p>${csdOtherDetails.heading!}  <a href="${csdOtherDetails.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Other Details  -->    
               
                 <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list csdFdpList as csdFdp>
                        	<p>${csdFdp.heading!}  <a href="${csdFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
           
           
             <!-- Industrial Visit -->
               <div role="tabpanel" class="tab-pane fade" id="industrial_visit">
               	<div class="row">
               		 <h3>Industrial Visit </h3>
               	<!--	 	<p>The department strives to offer a great source of practical knowledge to students by exposing
							them to real working environment through Industrial visits. Regularly Industrial visits are
							arranged to organizations like Indian Space Research Organization, Karnataka State Load
							Dispatch Centre, All India Radio. Visits are arranged for students to view project exhibitions
							Organized by Indian Institute of Science, Visvesvaraya Industrial and Technological Museum, EMMA-Expo 2014 and so on. Also visits are arranged to places like HAL Heritage Centre and Aerospace Museum.
							      </p>  -->
		                  <#list csdIndustrialVisitList as csdIndustrialVisit>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csdIndustrialVisit.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${csdIndustrialVisit.name!}</h3>	                      	 
			                       	 <p>${csdIndustrialVisit.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csdIndustrialVisit.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Industrial Visit -->
           
           
            <!-- Project Exhibition -->
               <div role="tabpanel" class="tab-pane fade" id="project_exhibition">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
               		 <p>The department conducts the project exhibition in the even semester of every academic year. Students are encouraged to present their projects and mini projects to the invited guests and evaluators. The best innovative projects are selected and are awarded.</p>
		                  <#list csdProjectExhibitionList as csdProjectExhibition>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${csdProjectExhibition.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${csdProjectExhibition.name!}</h3>	                      	 
			                       	 <p>Description: ${csdProjectExhibition.designation!}</p>
			                       	 <a class="btn btn-primary" href="${csdProjectExhibition.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Project Exhibition -->
           
               
                  
                  
                  <!--  -->
                  <div role="tabpanel" class="tab-pane fade" id="Section4">
                     <h3>Section 4</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
                  </div>
                  <!--  -->
                  
               </div>
            </div>
         </div>
      </div>
</section>
<!--dept tabs -->
	


	
   
   
   
    	
	 
</@page>