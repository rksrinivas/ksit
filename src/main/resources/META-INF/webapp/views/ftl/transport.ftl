<@page>
	
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
             
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/transportation/t1.jpg" alt="">                           
                        </article>
                        <article class="item">
                           <img class="animated bounceIn" src="${img_path!}/transportation/t2.jpg" alt="">										
                           
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/transportation/t3.jpg" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/transportation/t4.jpg" alt="">
                          
                        </article>
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 



<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 
	
    
<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To Transport Department</h1>
   <p class="welcome-text"></p>
</div>
<!-- welcome -->

    
    
    <!--=========== BEGIN WHY US SECTION ================-->
    <section id="whyUs_dept">
      <!-- Start why us top -->
      <div class="row">        
        <div class="col-lg-12 col-sm-12">
          <div class="whyus_top">
            <div class="container">
            
                <!-- Why us top titile -->
              <div class="row">
                <div class="col-lg-12 col-md-12"> 
                  <div class="title_area">
                    <h2 class="title_two color-red">Transport Staff</h2>
                    <span></span> 
                  </div>
                </div>
              </div>
              <!-- End Why us top titile -->
              
              <!-- Start Why us top content  -->
              <div class="row">
               
                  
				  
				  
	<!-- faculty -->			  
				  
	<div class="teacher_area home-2">
		<div class="container">
			<div class="row">				  
		
				<!--start teacher single  item -->
				
			</div>
		</div>
	</div>
				
				<!-- teachers details-->
				
				
				<!--start ads  area -->
	<div class="teacher_area home-2">
		
		
		<div class="container">
			<div class="row">
			
			<div class="col-md-12 col-lg-12 col-sm-12">
			
			
			
			
			
				<!--faculty-->
				<#list facultyList as faculty>
			<div class="col-md-3 col-lg-3 col-sm-6">
				<div class="single_teacher_item">
						<div class="teacher_thumb">
							<img src="${faculty.imageURL!}" alt="" />
							<div class="thumb_text">
								<h2>${faculty.name}</h2>
								<p>${faculty.designation}</p>
							</div>
						</div>
						<div class="teacher_content">
							<h2>${faculty.name}</h2>
							<span>${faculty.designation}</span>
							<p>${faculty.qualification}</p>
							<span>${faculty.department}</span>
							<p><a href=""${faculty.profileURL!}" target="_blank">view profile</a></p>
							<!-- <div class="social_icons">
                                          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                                          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                                          <a href="#" class="lin"><i class="fa fa-linkedin"></i></a>
                                          <a href="#" class="go"><i class="fa fa-google-plus"></i></a>
                             </div>-->
						</div>
					</div>
				</div>
				
				</#list>
			
			<!--end-->
			
			
				
			</div>
		</div>
	</div>	
	<!--end teacher  area -->
				
				
				
				
				<!-- end of teacher details-->
				
				
              </div>
              <!-- End Why us top content  -->
            </div>
          </div>
        </div>        
      </div>
      <!-- End why us top -->

     
				

				
              </div>
            </div>            
          </div>
        </div>        
      </div>
      <!-- End why us bottom -->
    </section>
    <!--=========== END WHY US SECTION ================-->

   
    <!-- bus route details-->
   
   		<section id="courseArchive">
      <div class="container-fluid">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
            
            
            	  <!-- Why us top titile -->
	              <div class="row">
	                <div class="col-lg-12 col-md-12"> 
	                  <div class="title_area ">
	                    
						<h4 class="text-center color-red fs-32 mt-0 mb-4">BUS ROUTES 2023 - 2024 </h4>
	                     
	                  </div>
	                </div>
	              </div>
	              <!-- End Why us top titile -->

			  	<div clas="row">
			  
			  	<!--route-->
				<h6 class="color-red" id="rt1">ROUTE NO. : 1 <br> DRIVER'S NAME: VENKATESH <br> Contact : 9945784743 <br> Bus Number :  KA05-AH-1148</h6>
						<table class="table table-striped course_table">
							<thead>
								<tr> 
									<th>SL.NO. </th>
									<th>STOP NAME</th>
									<th>TIMINGS</th>
									
								</tr>
							</thead>
							
								<tr>
									<td>1</td>
									<td>RAJ KUMAR SAMADHI</td>
									<td> 7:05 AM</td>									
								</tr>
								<tr>
									<td>2</td>
									<td>KEMPEGOWDA NAGAR(ARCH)</td>
									<td> 7:15AM</td>
								</tr>
								<tr>
									<td>3</td>
									<td>SUMANAHALLI SIGNAL-LEFT </td>
									<td> 7:15 AM</td>
									
								</tr>
								<tr>
									<td>4</td>
									<td>KAMAKSHIPALYA</td>
									<td>7:20 AM</td>
								</tr>
								<tr>
									<td>5</td>
									<td>HOUSING BOARD </td>
									<td>7:25 AM</td>
								</tr>
								<tr>
									<td>6</td>
									<td>MUDALPALYA </td>
									<td> 7:30 AM</td>
								</tr>
								
								<tr>
									<td>7</td>
									<td>NAGARBHAVI CIRCLE RIGHT </td>
									<td>7:35 AM</td>									
								</tr>
								<tr>
									<td>8</td>
									<td>Dr. AMBEDKAR COLLEGE </td>
									<td>7:35 AM</td>
								</tr>
								<tr>
									<td>9</td>
									<td> MALLATHAHALLI </td>
									<td> 7:40 AM</td>
									
								</tr>
								<tr>
									<td>10</td>
									<td> NAGADEVANAHALLI </td>
									<td>7:40 AM</td>
								</tr>
								<tr>
									<td>11</td>
									<td>KENGERI HOUSING BOARD</td>
									<td> 7:45 AM</td>
								</tr>
								<tr>
									<td>12</td>
									<td>KENGERI UPANAGAR CIRCLE</td>
									<td>7:50 AM</td>									
								</tr>
								<tr>
									<td>13</td>
									<td> BHEL </td>
									<td>7:55 AM</td>
								</tr>
								<tr>
									<td>14</td>
									<td>BGS COLLEGE </td>
									<td>8:00 AM</td>
									
								</tr>
								<tr>
									<td>15</td>
									<td>JSS COLLEGE</td>
									<td>8:00 AM</td>
								</tr>
								
								<tr>
									<td>20</td>
									<td>CAMPUS 1 (KSIT & KSPUC) </td>
									<td>8:15AM</td>
								</tr>
								<tr>
									<td>21</td>
									<td>CAMPUS 2 (KSSEM, KSP, KSSA & KSRIF)</td>
									<td>8:25AM</td>
								</tr>
				</table>
				<!--route-->
				

				<!--route-->
				<h6 class="color-red"  id="rt2">ROUTE NO. : 2 <br>DRIVER'S NAME : THIPPESWAMY.G <br> Contact: 8892709551 <br> Bus number : KA 51 - D - 3744</h6>
					<table class="table table-striped course_table">
						<thead>
							<tr> 
								<th>SL.NO. </th>
								<th>STOP NAME</th>
								<th>TIMINGS</th>										
							</tr>
						             </thead>




						                 <tr>
							<td>1</td>
							<td>PES NICE ROAD</td>
							<td>6:50 AM</td>
						</tr>
						<tr>
							<td>2</td>
							<td>HOSA ROAD</td>
							<td>6:55 AM</td>
						</tr>
				
						<tr>
							<td>3</td>
							<td>SINGASANDRA</td>
							<td>7:00 AM</td>
						</tr>
						<tr>
							<td>4</td>
							<td>KUDLU GATE</td>
							<td>7:05 AM</td>
						</tr>
				
						<tr>
							<td>5</td>
							<td>GAREPALYA</td>
							<td>7:05 AM</td>
						</tr>
		
						<tr>
							<td>6</td>
							<td>OXFORD COLLEGE</td>
							<td>7:05 AM</td>
						</tr>
						<tr>
							<td>7</td>
							<td>BOMMANAHALLI</td>
							<td>7:10 AM</td>
						</tr>
						<tr>
							<td>8</td>
							<td> BEGUR - MAIN ROAD </td>
							<td>7:15 AM</td>
						</tr>
						<tr>
							<td>9</td>
							<td>P.K.KALYANA MANTAPA RIGHT </td>
							<td> 7:20 AM</td>
						</tr>
						<tr>
							<td>10</td>
							<td>DEVARACHIKKANAHALLI (AIYAPPA BAKERY LEFT)  </td>
							<td>7:30 AM</td>
						</tr>
						<tr>
							<td>11</td>
							<td>VIJAYA BANK LAYOUT(INDIAN OIL PETROL BUNK RIGHT)</td>
							<td> 7:35 AM</td>
						</tr>
						<tr>
							<td>12</td>
							<td>Dr. AMBEDKAR CIRCLE LEFT </td>
							<td> 7:35 AM</td>
						</tr>
						<tr>
							<td>13</td>
							<td>HSBC</td>
							<td> 7:35 AM</td>
						</tr>
						<tr>
							<td>14</td>
							<td>AREKERE GATE (RELIANCE)</td>
							<td> 7:35 AM</td>
						</tr>
						<tr>
							<td>15</td>
							<td>HULIMAVU</td>
							<td> 7:40 AM</td>
						</tr>
							<tr>
							<td>16</td>
							<td>MEENAKSHI TEMPLE</td>
							<td> 7:45 AM</td>
						</tr>
						<tr>
							<td>17</td>
							<td>HOLISPIRIT SCHOOL- RIGHT</td>
							<td>7:47 AM</td>
						</tr>
							<tr>
							<td>18</td>
							<td>B.K.CIRCLE</td>
							<td> 7:50 AM</td>
						</tr>
						<tr>
							<td>19</td>
							<td>JAMBU SAVARI DINNE</td>
							<td> 7:52 AM</td>
						</tr>
						<tr>
							<td>20</td>
							<td>KEMBATHALLI</td>
							<td>7:54 AM</td>
						</tr>
							<tr>
							<td>21</td>
							<td>GLASS FACTORY</td>
							<td> 7:55 AM</td>
						</tr>
						<tr>
							<td>22</td>
							<td>AVALAHALLI</td>
							<td> 8:00 AM</td>
						</tr>
						<tr>
							<td>16</td>
							<td>Campus 1 (KSIT & KSPUC)</td>
							<td> 8:15 AM</td>
						</tr>
						<tr>
							<td>17</td>
							<td>Campus 2 (KSSEM, KSP, KSSA & KSRIF)</td>
							<td>8:25 AM</td>
						</tr>

                                                                 
								
						
				</table>
				<!--route-->

				<!--route-->
                             <h6 class="color-red"  id="rt3">ROUTE NO : 3 <br>DRIVER'S NAME : RAMU <br> Contact : 9742215053 <br> Bus Number :  KA05-AH.1148</h6>
							  		
				<table class="table table-striped course_table">
								  	
					<thead>
						<tr>
							<th>SL.NO. </th>
							<th>STOP NAME</th>
							<th>TIMINGS</th>
						</tr>
					</thead>	
						<tr>
							<td>1</td>
							<td>VEERABHADRA NAGAR</td>
							<td>7:35 AM</td>
						</tr>
						
						<tr>
							<td>2</td>
							 <td>PESIT</td>
							 <td>7:35 AM</td>
						</tr>
						<tr>
							<td>3</td>
							<td>HOSAKEREHALLI PETROL BUNK</td>
							<td>7:38 AM</td>
						</tr>
						
						<tr>
							<td>4</td>
							   <td>HOSAKEREHALLI SIGNAL(SRINATH HOUSE)</td>
							<td>7:40 AM</td>
						</tr>
						
						<tr>
							<td>5</td>
							 <td>JANATHA BAZAR SIGNAL RIGHT</td>
							<td>7:42 AM</td>
						</tr>
						
						<tr>
							<td>6</td>
							<td>ITTUMADU MAIN ROAD</td>
							 <td>7:45 AM</td>
						</tr>
						
						<tr>
							<td>7</td>
							<td>ARAHALLI</td>
							<td>7:50 AM</td>
						</tr>
						
						<tr>
							<td>8</td>
							<td>POORNA PRAGNA LAYOUT</td>
							<td>7:50 AM</td>
						</tr>
						
						<tr>
							<td>9</td>
							 <td>PATALAMMA TEMPLE</td>
							<td>7:53 AM</td>
						</tr>
						
						<tr>
							<td>10</td>
							<td>CHANNASANDRA NICE ROAD LEFT</td>
							<td>7:55 AM</td>
						</tr>
							
						<tr>
							<td>11</td>
							<td>Campus 1 (KSIT & KSPUC)</td>
							<td>8:15 AM</td>
						</tr>
						
						<tr>
							<td>12</td>
							<td>Campus 2 (KSSEM, KSSA, KSP & KSRIF)</td>
							<td>8:25 AM</td>
						</tr>
						
						
							
					
				</table>
				
				<!--route3-->

				<!--route4-->
							  				  		
				<h6 class="color-red"  id="rt4">ROUTE NO : 4 <br>DRIVER'S NAME : SHANTHA KUMAR <br> Contact : 9945051310 <br> Bus Number : KA 05 - AH - 1149</h6>

                              <table class="table table-striped course_table">
	                    <thead>
		                          <tr>
			                         <th>SL.NO.</th>
						<th>STOP NAME</th>
						<th>TIMINGS</th>
					</tr>
	                   </thead>
	 				<tr>
						<td>1</td>
						<td>GOTTIGERE</td>
						<td>7:20 AM</td>
					</tr>
					<tr>
						<td>2</td>
						<td>KALYAN AGRAHARA</td>
						<td>7:25 AM</td>
					</tr>
					<tr>
						<td>3</td>
						<td>HULIMAVU</td>
						<td>7:28 AM</td>
					</tr>
					<tr>
						<td>4</td>
						<td>AREKERE LEFT</td>
						<td>7:30 AM</td>
					</tr>
					<tr>
						<td>5</td>
						<td>J.P.NAGAR ROYAL SCHOOL</td>
						<td>7:35 AM</td>
					</tr>
					<tr>
						<td>6</td>
						<td>NANDINI HOTEL LEFT</td>
						<td>7:37 AM</td>
					</tr>
					<tr>
						<td>7</td>
						<td>HOTEL INCHARA</td>
						<td>7:40 AM</td>
					</tr>
					<tr>
						<td>8</td>
						<td>PUTTENAHALLI PETROL BUNK</td>
						<td>7:45 AM</td>
					</tr>
					<tr>
						<td>9</td>
						<td>BRIGADE SIGNAL</td>
						<td>7:47 AM</td>
					</tr>
					<tr>
						<td>10</td>
						<td>CAPITAL SCHOOL</td>
						<td>7:50 AM</td>
						</tr>
	 				<tr>
						<td>11</td>
						<td>GAURAV NAGAR RIGHT</td>
						<td>7:52 AM</td>
					</tr>
					<tr>
						<td>12</td>
						<td>KEB</td>
						<td>7:53 AM</td>
					</tr>
					<tr>
						<td>13</td>
						<td>CHUNCHUGATTA TEMPLE</td>
						<td>7:57 AM</td>
					</tr>
					<tr>
						<td>14</td>
						<td>CHUNCHUGATTA CIRCLE LEFT</td>
						<td>8:00 AM</td>
					</tr>
					<tr>
						<td>15</td>
						<td>RAJANANDINI HOSPITAL</td>
						<td>8:02 AM</td>
					</tr>
					<tr>
						<td>16</td>
						<td>SOUDHAMINI</td>
						<td>8:05 AM</td>
					</tr>
					<tr>
						<td>17</td>
						<td>HARINAGAR CROSS</td>
						<td>8:08 AM</td>
					</tr>
					<tr>
						<td>18</td>
						<td>GLASS FACTORY</td>
						<td>8:10 AM</td>
					</tr>
					<tr>
						<td>19</td>
						<td>AVALAHALLI RIGHT</td>
						<td>8:10 AM</td>
					</tr>
					<tr>
						<td>20</td>
						<td>Campus 1 - KSIT & KSPUC</td>
						<td>8:15 AM</td>
					</tr>
					<tr>
						<td>21</td>
						<td>Campus 2 - KSSEM, KSP, KSSA & KSRIF</td>
						<td>8:25 AM</td>
					</tr>
					</table>
                                  <!--route4-->


				<!--route5-->
							  				  		
				<h6 class="color-red"  id="rt5">ROUTE NO.: 5 <br> DRIVER'S NAME : K DEVARAJULU NAIDU <br> Contact : 9113940767 <br> Bus Number :  KA05-AE-8125</h6>
                               <h6 class="color-red"  id="rt6">ROUTE NO.: 6 <br> DRIVER'S NAME : D. CHANDRASHEKAR NAIDU <br> Contact : 7349262060 <br> Bus Number : KA05-AE-8127</h6>
					<table class="table table-striped course_table">		  	
						<thead>
							<tr>
								<th>SL.NO. </th>
								<th>STOP NAME</th>
								<th>TIMINGS</th>
							</tr>
					    </thead>
							<tr>
								<td>1</td>
								<td> KS BOYS HOSTEL, MALLASANDRA </td>
								<td> 8:10 AM</td>
							</tr>
							
							<tr>
								<td>2</td>
								<td><div class="country"> Campus 1 -KSIT &amp; KSPUC</div> </td>
								<td>8:20 AM</td>
							</tr>
							
							
				</table>
				
				<!--route5-->
                                <!--route6-->
				
				

				<!--route7-->
							  				  		
				<h6 class="color-red"  id="rt7">ROUTE NO. : 7 <br> DRIVER'S NAME : E.PURUSHOTHAM NAIDU <br> Contact : 9886121591 <br> Bus Number : KA 05 - AG - 7826</h6>

				<table class="table table-striped course_table">
   							 <thead>
       								 <tr>
           							 <th>SL.NO. </th>
           							 <th>STOP NAME</th>
           							 <th>TIMINGS</th>
       								 </tr>
   							 </thead>
   				<tbody>
       							 <tr>
           							 <td>1</td>
           							 <td>GIRINAGAR BDA PARK</td>
           							 <td>7:25 AM</td>
      							  </tr>
        						<tr>
           							 <td>2</td>
           							 <td>50 FEET ROAD</td>
           							 <td>7:25 AM</td>
       							 </tr>
       							 <tr>
           							 <td>3</td>
          						         <td>MUNESHWARA BLOCK</td>
           							 <td>7:25 AM</td>
        						</tr>
       							 <tr>
           							 <td>4</td>
           							 <td>MEENAKSHI HOSPITAL (PES)</td>
           							 <td>7:25 AM</td>
       							 </tr>
       							 <tr>
            							<td>5</td>
           							 <td>SRINAGAR BUS STOP</td>
           							 <td>7:30 AM</td>
        						</tr>
       							 <tr>
           							 <td>6</td>
          							  <td>APEX BANK</td>
          							  <td>7:30 AM</td>
        						</tr>
       							 <tr>
            							<td>7</td>
            							<td>MARUTHI CIRCLE</td>
           							 <td>7:35 AM</td>
      							  </tr>
        						<tr>
           							 <td>8</td>
          						         <td>GANESH BHAVAN RIGHT</td>
            							<td>7:35 AM</td>
       							 </tr>
       							 <tr>
          							  <td>9</td>
            							 <td>ASHOK NAGAR</td>
           							 <td>7:37 AM</td>
      							  </tr>
      							  <tr>
          							  <td>10</td>
            							  <td>GANGAMMA TEMPLE</td>
           							 <td>7:40 AM</td>
       							 </tr>
       							 <tr>
            							<td>11</td>
            							<td>SSM SCHOOL</td>
           							 <td>7:42 AM</td>
       							 </tr>
      							  <tr>
          							  <td>12</td>
            							  <td>INDIRA NURSING HOME</td>
                                                                  <td>7:44 AM</td>
                                                         </tr>
        						 <tr>
           							 <td>13</td>
           							 <td>SRIHARI KALYANA MANTAPA</td>
          							  <td>7:46 AM</td>
       							 </tr>
       							 <tr>
          							  <td>14</td>
           							 <td>D.G. PETROL BUNK</td>
           							 <td>7:50 AM</td>
      							  </tr>
      							  <tr>
            							<td>15</td>
           							 <td>PAVANADHAMA</td>
           							 <td>7:52 AM</td>
       							 </tr>
       							 <tr>
           							 <td>16</td>
           							 <td>KADIRENAHALLI PETROL BUNK</td>
           							 <td>7:53 AM</td>
       							 </tr>
        						<tr>
          							  <td>17</td>
           							 <td>GOWDANAPALYA</td>
          							  <td>7:53 AM</td>
       							 </tr>
       							 <tr>
           							 <td>18</td>
           							 <td>CHIKKALASANDRA</td>
          							  <td>7:55 AM</td>
        						</tr>
        						<tr>
          							  <td>19</td>
           							 <td>KAGGIES BAKERY</td>
           							 <td>7:57 AM</td>
       							 </tr>
       							 <tr>
            							<td>20</td>
            							<td>K.R. HOSPITAL</td>
           							 <td>7:59 AM</td>
      							  </tr>
        						<tr>
            							<td>21</td>
            							<td>UTTARAHALLI CHURCH</td>
            							<td>8:00 AM</td>
      							  </tr>
       							 <tr>
           							 <td>22</td>
          							  <td>SUBRAMANYAPURA POLICE STATION</td>
           							 <td>8:02 AM</td>
       							 </tr>
       							 <tr>
            							<td>23</td>
            							<td>Campus 1 (KSIT & KSPUC)</td>
            							<td>8:15 AM</td>
      							  </tr>
       							 <tr>
           							 <td>24</td>
            							<td>Campus 2 (KSSEM, KSP, KSSA & KSRIF)</td>
           							 <td>8:25 AM</td>
       							 </tr>
   							 </tbody>
						</table>
		
				<!--route7-->

				<!-- route8-->

				
							  				  		
				<h6 class="color-red"  id="rt8	">ROUTE NO. : 8 <br> DRIVER'S NAME: RAMU <br>Contact : 9742215053 <br> Bus Number : KA 51 - AG - 7828</h6>

				<table class="table table-striped course_table">
    						<thead>
      						 <tr>
           						 <th>SL.NO.</th>
            						<th>STOP NAME</th>
            						<th>TIMINGS</th>
       						</tr>
    						</thead>
   					
       						 <tr>
           						 <td>1</td>
           						 <td>VEERABHADRA NAGAR</td>
           						 <td>7:35 AM</td>
       						 </tr>
      						  <tr>
           						 <td>2</td>
            						<td>PESIT</td>
           						 <td>7:35 AM</td>
       						 </tr>
       						 <tr>
          						  <td>3</td>
           						 <td>HOSAKEREHALLI PETROL BUNK</td>
           						 <td>7:38 AM</td>
       						 </tr>
     					       <tr>
           						 <td>4</td>
            						<td>HOSAKEREHALLI SIGNAL (SRINATH HOUSE)</td>
           						 <td>7:40 AM</td>
        					</tr>
      						  <tr>
           						 <td>5</td>
            						<td>JANATHA BAZAR SIGNAL RIGHT</td>
           						 <td>7:42 AM</td>
        					</tr>
       						 <tr>
           						 <td>6</td>
            						<td>ITTUMADU MAIN ROAD</td>
           						 <td>7:45 AM</td>
       						 </tr>
      						  <tr>
            						<td>7</td>
          					       <td>ARAHALLI</td>
            						<td>7:50 AM</td>
       						 </tr>
       						 <tr>
           						 <td>8</td>
           						 <td>POORNA PRAGNA LAYOUT</td>
           						 <td>7:50 AM</td>
        					</tr>
      						  <tr>
           					 	 <td>9</td>
           						 <td>PATALAMMA TEMPLE</td>
           						 <td>7:53 AM</td>
      						  </tr>
       						 <tr>
           						 <td>10</td>
            						<td>CHANNASANDRA NICE ROAD LEFT</td>
            						<td>7:55 AM</td>
       						 </tr>
       						 <tr>
           						 <td>11</td>
           						 <td>Campus 1 (KSIT & KSPUC)</td>
           						 <td>8:15 AM</td>
        					</tr>
        					<tr>
        	   					 <td>12</td>
           						 <td>Campus 2 (KSSEM, KSP, KSSA & KSRIF)</td>
            						<td>8:25 AM</td>
       						 </tr>
  					  
				</table>

				
				<!-- route8-->

				<!--route9-->
							  				  		
				<h6 class="color-red"  id="rt9">ROUTE NO.: 9 <br> DRIVER'S NAME : CHITTIBABU <br> Contact : 9448671845 <br> Bus Number : KA05-AM-7040</h6>

				<table class="table table-striped course_table">
    				<thead>
        					<tr>
            						<th>SL.NO.</th>
           						 <th>STOP NAME</th>
            						<th>TIMINGS</th>
        					</tr>
   				 </thead>
   			
       						 <tr>
           						 <td>1</td>
            						<td>PARIMALA SWEETS</td>
           						 <td>7:35 AM</td>
       						 </tr>
      						  <tr>
           						 <td>2</td>
            						<td>SEETHA CIRCLE</td>
           						 <td>7:36 AM</td>
        					</tr>
        					<tr>
            						<td>3</td>
          						<td>BANK COLONY</td>
           						<td>7:37 AM</td>
      						  </tr>
       						 <tr>
            						<td>4</td>
           						 <td>SRINIVASANAGAR</td>
            						<td>7:38 AM</td>
       						 </tr>
       						 <tr>
            						<td>5</td>
           						 <td>SHANKARNAG CIRCLE</td>
           						 <td>7:40 AM</td>
      						  </tr>
      						  <tr>
            						<td>6</td>
           					       <td>VIDYAPEETA CIRCLE</td>
            						<td>7:40 AM</td>
       						 </tr>
      						  <tr>
           						 <td>7</td>
          						  <td>S.L.V. HOTEL</td>
           						 <td>7:42 AM</td>
      						  </tr>
       						 <tr>
            						<td>8</td>
            						<td>I.T.I COLONY</td>
           						 <td>7:42 AM</td>
       						 </tr>
        					<tr>
           						 <td>9</td>
           						 <td>43 A BUS STOP</td>
           						 <td>7:45 AM</td>
       						 </tr>
        					<tr>
           						 <td>10</td>
          						 <td>KATRIGUPPE ARCH</td>
            						 <td>7:47 AM</td>
        					</tr>
      						  <tr>
           						 <td>11</td>
            						<td>FOOD WORLD</td>
            						<td>7:50 AM</td>
        					</tr>
       						 <tr>
           						 <td>12</td>
          						  <td>KAMAKYA</td>
            						<td>7:52 AM</td>
       						 </tr>
        					<tr>
            						<td>13</td>
            						<td>CORPORATION BANK</td>
           						 <td>7:53 AM</td>
       						 </tr>
      						  <tr>
            						<td>14</td>
           						 <td>D.G. PETROL BUNK</td>
           						 <td>7:55 AM</td>
       						</tr>
        					<tr>
            						<td>15</td>
           						 <td>Campus 1 (KSIT & KSPUC)</td>
           						 <td>8:15 AM</td>
        					</tr>
        					<tr>
            						<td>16</td>
           						 <td>Campus 2 (KSSEM, KSSA, KSP & KSRIF)</td>
            						<td>8:25 AM</td>
       						 </tr>
    						
					</table>

				
				<!--route9-->

				<!-- route10 -->
										  				  		
				<h6 class="color-red"  id="rt10">ROUTE NO. : 10 <br> DRIVER'S NAME : K. VINAYAKA NAIDU <br> Contact : 9900473699 <br> Bus Number : KA 05 - AL - 1530 </h6>

                              <table class="table table-striped course_table">
                              <thead>
                                                 <tr>
                                                      <th>SL.NO.</th>
                                                      <th>STOP NAME</th>
                                                      <th>TIMINGS</th>
                                                </tr>
                          </thead>
                                                <tr>
                                                     <td>1</td>
                                                     <td>DOUBLE ROAD</td>
                                                     <td>7:30 AM</td>
                                                </tr>
    						<tr>
        					     <td>2</td>
                                                     <td>LALBAGH</td>
                                                     <td>7:33 AM</td>
                                	       </tr>
   						<tr>
                                                     <td>3</td>
        					     <td>SIDDAPURA POLICE STATION</td>
       						     <td>7:35 AM</td>
                                               </tr>
                                               <tr>
        					     <td>4</td>
                                                    <td>CARMEL CONVENT</td>
                                                     <td>7:40 AM</td>
                                               </tr>
                                               <tr>
                                                     <td>5</td>
                                                     <td>PUMP HOUSE</td>
                                                     <td>7:42 AM</td>
                                              </tr>
                                               <tr>
                                                     <td>6</td>
                                                     <td>JAYANAGAR 9TH BLOCK</td>
                                                     <td>7:45 AM</td>
                                               </tr>
                                               <tr>
                                                     <td>7</td>
                                                     <td>GANESH TEMPLE</td>
                                                     <td>7:47 AM</td>
                                              </tr>
                                              <tr>
                                                     <td>8</td>
                                                     <td>SATHYA SAI</td>
                                                     <td>7:50 AM</td>
                                             </tr>
                                             <tr>
                                                     <td>9</td>
                                                     <td>RAGHAVENDRA MUTT</td>
                                                     <td>7:53 AM</td>
                                            </tr>
                                            <tr>
                                                    <td>10</td>
                                                    <td>R.V.DENTAL COLLEGE</td>
                                                    <td>7:55 AM</td>
                                            </tr>
                                            <tr>
                                                     <td>11</td>
                                                    <td>INDIRAGANDHI CIRCLE</td>
                                                    <td>7:57 AM</td>
                                           </tr>
                                           <tr>
                                                    <td>12</td>
                                                    <td>SARAKKI MARKET</td>
                                                    <td>7:59 AM</td>
                                           </tr>
                                           <tr>
                                                     <td>13</td>
                                                    <td>SARAKKI SIGNAL</td>
                                                    <td>8:02 AM</td>
                                            </tr>
      					     <tr>
        					    <td>14</td>
                                                    <td>YELACHENAHALLIL</td>
                                                   <td>8:05 AM</td>
                                           </tr>
                                           <tr>
                                                   <td>15</td>
                                                   <td>KONANKUNTE CROSS</td>
                                                   <td>8:08 AM</td>
                                           </tr>
                                            <tr>
                                                   <td>16</td>
                                                   <td>Campus 1 - KSIT & KSPUC</td>
                                                   <td>8:15 AM</td>
                                           </tr>
                                           <tr>
                                                   <td>17</td>
                                                   <td>Campus 2 - KSSEM, KSSA, KSP & KSRIF</td>
                                                   <td>8:25 AM</td>
                                          </tr>
                                        </table>
	

				
				<!-- route10-->

				<!--route 11-->
                              <h6 class="color-red"  id="rt11">ROUTE NO.: 11 <br> DRIVER'S NAME :  DHANARAJU  <br> Contact : 9972182021 <br> Bus Number : KA.05-AH-1146</h6>
           
                            <table class="table table-striped course_table">
                           <thead>
                                        <tr>
                                              <th>SL.NO.</th>
                                              <th>STOP NAME</th>
                                              <th>TIMINGS</th>
                                       </tr>
                         </thead>

                                       <tr>
                                             <td>1</td>
                                             <td>K.R.PURAM</td>
                                             <td>6:45 AM</td>
                                      </tr>
                                      <tr>
                                             <td>2</td>
                                             <td>ITI MAIN FACTORY</td>
                                             <td>6:50 AM</td>
                                      </tr>
                                      <tr>
                                             <td>3</td>
                                             <td>TIN FACTORY</td>
                                             <td>6:55 AM</td>
                                     </tr>
                                     <tr>
                                             <td>4</td>
                                             <td>NGEF LEFT</td>
                                             <td>7:05 AM</td>
                                     </tr>
                                     <tr>
                                             <td>5</td>
                                             <td>CV RAMAN NAGAR</td>
                                             <td>7:06 AM</td>
                                     </tr>
                                     <tr>
                                             <td>6</td>
                                             <td>BEML</td>
                                             <td>7:07 AM</td>
                                    </tr>
                                    <tr>
                                             <td>7</td>
                                             <td>JEEVAN BHIMA NAGAR</td>
                                             <td>7:10 AM</td>
                                    </tr>
                                    <tr>
                                             <td>8</td>
                                             <td>INDIRANAGAR 80 FT ROAD LEFT</td>
                                             <td>7:12 AM</td>
                                    </tr>
                                    <tr>
                                             <td>9</td>
                                             <td>DOMLUR RING ROAD</td>
                                             <td>7:15 AM</td>
                                    </tr>
                                    <tr>
                                             <td>10</td>
                                             <td>EJIPURA SIGNAL</td>
                                             <td>7:20 AM</td>
                                    </tr>
                                    <tr>
                                             <td>11</td>
                                             <td>KORMANGALA (SONY WORLD)</td>
                                             <td>7:25 AM</td>
                                    </tr>
                                    <tr>
                                             <td>12</td>
                                             <td>BDA COMPLEX</td>
                                             <td>7:30 AM</td>
                                    </tr>
                                    <tr>
                                             <td>13</td>
                                             <td>ST.JOHNSON HOSPITAL</td>
                                             <td>7:35 AM</td>
                                    </tr>
                                    <tr>
                                             <td>14</td>
                                             <td>AIYAPPA TEMPLE</td>
                                             <td>7:37 AM</td>
                                    </tr>

                                            <tr>
                                              <td>15</td>																			      <td>UDUPI GARDEN</td>																		      <td>7:39 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>16</td>
                                                    <td>BTM WATER TANK JAYADEVA</td>
                                                    <td>7:45 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>17</td>
                                                    <td>JAYA DEV HOSPITAL</td>
                                                    <td>7:45 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>18</td>
                                                    <td>SHOPPERS STOP</td>
                                                    <td>7:45 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>19</td>
                                                    <td>JEEDIMARA</td>
                                                    <td>7:45 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>20</td>
                                                    <td>HP PETROL PUMP</td>
                                                    <td>7:50 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>21</td>
                                                    <td>NANDINI HOTEL</td>
                                                    <td>7:50 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>23</td>
                                                    <td>J.P.NAGAR 6TH PHASE</td>
                                                    <td>7:53 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>24</td>
                                                    <td>SARAKKI SIGNAL LEFT</td>
                                                    <td>7:55 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>25</td>
                                                    <td>Campus 1 -KSIT & KSPUC</td>
                                                    <td>8:15 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>26</td>
                                                    <td>Campus 2 - KSSEM, KSSA, KSP & KSRIF</td>
                                                    <td>8:25 AM</td>
                                                </tr>

                                            </table>

                                          <!--route 11-->

                                          <!--route 12 -->

                              <h6 class="color-red"  id="rt12">ROUTE NO.: 12 <br> DRIVER'S NAME : G. RAVI <br> Contact : 8073241413 <br> Bus Number : KA 05 - AH - 1147</h6>

                                            <table class="table table-striped course_table">
                                                <thead>
                                                    <tr>
                                                        <th>SL.NO.</th>
                                                        <th>STOP NAME</th>
                                                        <th>TIMINGS</th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td>1</td>
                                                    <td>MYSORE CIRCLE</td>
                                                    <td>7:35 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>T.R.MILL</td>
                                                    <td>7:38 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>CHAMARAJPET CIRCLE</td>
                                                    <td>7:40 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>UMA TALKIES</td>
                                                    <td>7:42 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>RAMAKRISHNA ASHRAM</td>
                                                    <td>7:45 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>NORTH ROAD</td>
                                                    <td>7:45 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>7</td>
                                                    <td>BASAVANAGUDI POLICE STATION</td>
                                                    <td>7:47 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>8</td>
                                                    <td>NETKALAPPA CIRCLE</td>
                                                    <td>7:49 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>9</td>
                                                    <td>N.R.COLONY</td>
                                                    <td>7:52 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>10</td>
                                                    <td>T.R.NAGAR</td>
                                                    <td>7:54 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>11</td>
                                                    <td>BANASHANKARI POST OFFICE</td>
                                                    <td>7:55 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>12</td>
                                                    <td>BATA SHOW ROOM</td>
                                                    <td>7:57 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>13</td>
                                                    <td>KAVERI NAGAR</td>
                                                    <td>7:58 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>14</td>
                                                    <td>DAYANANDA SAGAR</td>
                                                    <td>8:00 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>15</td>
                                                    <td>KUMARASWAMY LAYOUT POLICE STATION</td>
                                                    <td>8:03 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>16</td>
                                                    <td>ISRO LAYOUT</td>
                                                    <td>8:05 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>17</td>
                                                    <td>KONANKUNTE CROSS</td>
                                                    <td>8:10 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>18</td>
                                                    <td>Campus 1 - KSIT & KSPUC</td>
                                                    <td>8:15 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>19</td>
                                                    <td>Campus 2 - KSSEM, KSSA, KSP & KSRIF</td>
                                                    <td>8:25 AM</td>
                                                </tr>
                                            </table>


                                          <!--route 12 -->
                                            				
                                          <!--route 13-->
                                         <h6 class="color-red"  id="rt13">ROUTE NO.: 13 <br> DRIVER'S NAME : MANI <br> Contact : 8618847595 <br> Bus Number : KA 51 - D - 3753</h6>

                                            <table class="table table-striped course_table">
                                                <thead>
                                                    <tr>
                                                        <th>SL.NO.</th>
                                                        <th>STOP NAME</th>
                                                        <th>TIMINGS</th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td>1</td>
                                                    <td>ISKON</td>
                                                    <td>7:10 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>RAJAJINAGAR 1ST BLOCK</td>
                                                    <td>7:15 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>NAVARANG BRIDGE</td>
                                                    <td>7:17 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>MODI HOSPITAL</td>
                                                    <td>7:19 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>SHANKARMUTT LEFT</td>
                                                    <td>7:20 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>PAVITHRA PARADISE</td>
                                                    <td>7:25 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>7</td>
                                                    <td>MAGADI ROAD, HOUSING BOARD</td>
                                                    <td>7:25 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>8</td>
                                                    <td>VIJAYA NAGAR</td>
                                                    <td>7:30 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>9</td>
                                                    <td>MARUTHI MANDIR</td>
                                                    <td>7:30 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>10</td>
                                                    <td>ATTIGUPPE</td>
                                                    <td>7:35 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>11</td>
                                                    <td>DEEPANJALI NAGAR</td>
                                                    <td>7:35 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>12</td>
                                                    <td>GALI ANJANEYA TEMPLE</td>
                                                    <td>7:40 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>13</td>
                                                    <td>BHEL</td>
                                                    <td>7:40 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>14</td>
                                                    <td>NAYANDAHALLI</td>
                                                    <td>7:40 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>15</td>
                                                    <td>RAJA RAJESHWARI GATE</td>
                                                    <td>7:45 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>16</td>
                                                    <td>BEML LAYOUT</td>
                                                    <td>7:55 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>17</td>
                                                    <td>IDEAL HOME</td>
                                                    <td>7:55 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>18</td>
                                                    <td>BEML BMTC DEPO</td>
                                                    <td>8:00 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>19</td>
                                                    <td>CHANNASANDRA</td>
                                                    <td>8:05 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>20</td>
                                                    <td>Campus 1 -KSIT & KSPUC</td>
                                                    <td>8:15 AM</td>
                                                </tr>
                                                <tr>
                                                    <td>21</td>
                                                    <td>Campus 2 - KSSEM, KSSA, KSP & KSRIF</td>
                                                    <td>8:25 AM</td>
                                                </tr>
                                            </table>

                                            									
                                            	<!--route13-->
                                            				
                                             <!--route14-->
                                            							  				  		
                                        <h6 class="color-red"  id="rt14">ROUTE NO.: 14 <br> DRIVER'S NAME : DHANRAJ <br> Contact : 9972182021 <br> Bus Number :    KA 05 - AH - 1146</h6>

                                            <table class="table table-striped course_table">
                                                <thead>
                                                    <tr>          
                                                        <th>SL.NO. </th>
                                                        <th>STOP NAME</th>
                                                        <th>TIMINGS</th>                                            
                                                    </tr>
                                                </thead>    
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>DOUBLE ROAD</td>
                                                        <td>7:23 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>LALBAGH</td>
                                                        <td>7:25 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>LALBAGH SIDDAPURA</td>
                                                        <td>7:28 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>ASHOKA PILLAR</td>
                                                        <td>7:30 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>MADHAVAN PARK</td>
                                                        <td>7:32 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>SWIMMING POOL 3RD BLOCK</td>
                                                        <td>7:35 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>VIJAYA COLLEGE 4TH BLOCK</td>
                                                        <td>7:37 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>8</td>
                                                        <td>POST OFFICE</td>
                                                        <td>7:39 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>9</td>
                                                        <td>RAGHAVENDRA MUTT</td>
                                                        <td>7:40 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>10</td>
                                                        <td>RV DENTAL COLLEGE</td>
                                                        <td>7:43 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>11</td>
                                                        <td>INDIRAGANDHI CIRCLE</td>
                                                        <td>7:45 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>12</td>
                                                        <td>BANASHANKARI MARKET</td>
                                                        <td>7:50 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>13</td>
                                                        <td>SARAKKI SIGNAL</td>
                                                        <td>8:00 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>14</td>
                                                        <td>Campus 1 - KSIT & KSPUC</td>
                                                        <td>8:15 AM</td>
                                                    </tr>
                                                    <tr>
                                                        <td>15</td>
                                                        <td>Campus 2 - KSSEM, KSSA, KSP & KSRIF</td>
                                                        <td>8:25 AM</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            									
                                           <!--route14-->				
                                            	
                                            </div>
                                           </div>
                                          </div>
                                        </div>
                                         </div>
                                        </section>
                                       <!-- bus route details-->
</@page>