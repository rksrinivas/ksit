<@page>

<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
              	  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#if libraryPageSliderImageList?has_content>
	                     		<#list libraryPageSliderImageList as libraryPageSliderImage>
	                     			<#if libraryPageSliderImage?index  == 0>
	                     				<article class="item active">
	                     			<#else>
	                     				<article class="item">
	                     			</#if>
		                           		<img class="animated slideInLeft" src="${libraryPageSliderImage.imageURL}" alt="">
										<div class="carousel-caption" style="background:none;">
											<p class="slideInLeft">${libraryPageSliderImage.heading}</p>
											<p class="slideInRight">${libraryPageSliderImage.content}</p>
										</div>                           
		                        	</article>
	                     		</#list>
	                     	<#else>
							   <article class="item active">
							       <img class="animated slideInLeft" src="${img_path!}/library/slider/s1.jpg" alt="">                         
							    </article>
							    <article class="item">
							       <img class="animated slideInLeft" src="${img_path!}/library/slider/s2.jpg" alt="">                         
							    </article>
							    <article class="item">
							       <img class="animated bounceIn" src="${img_path!}/library/slider/s3_new.jpg" alt="">								                        
							    </article>
							    <article class="item">
							       <img class="animated rollIn" src="${img_path!}/library/slider/s4.jpg" alt="">                         
							    </article>
							  <!--  <article class="item">
							       <img class="animated rollIn" src="${img_path!}/library/slider/s5.jpg" alt="">                        
							    </article> -->
							    <article class="item">
							       <img class="animated rollIn" src="${img_path!}/library/slider/s6.jpg" alt="">                         
							    </article>
							</#if>
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                        
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 



	
	    
<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 
    
    
    
      	
	 <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="vertical-tab padding" role="tabpanel">
			               
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>			                	
			                    <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab">Vision & Mission</a></li>
			                    <li role="presentation"><a href="#timings" aria-controls="timings" role="tab" data-toggle="tab">Timings</a></li>
			                    
			                    <li role="presentation"><a href="#books_collection" aria-controls="books_collection" role="tab" data-toggle="tab">Collections</a></li>
			                    <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services / Facilities</a></li>
			                    <li role="presentation"><a href="#infrastructure" aria-controls="infrastructure" role="tab" data-toggle="tab">infrastructure</a></li>
			              		<li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab">Staff</a></li>
			                	<li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
			                    <li role="presentation"><a href="#e_resources" aria-controls="e_resources" role="tab" data-toggle="tab">E-Resources</a></li>
			                    <li role="presentation"><a href="#opac" aria-controls="opac" role="tab" data-toggle="tab">OPAC</a></li>
			                    
			                    <li role="presentation"><a href="#course_materials" aria-controls="course_materials" role="tab" data-toggle="tab">Course Materials</a></li>
			                    <li role="presentation"><a href="#publications" aria-controls="publications" role="tab" data-toggle="tab">Publications</a></li>
			                    <li role="presentation"><a href="#links" aria-controls="links" role="tab" data-toggle="tab">Links</a></li>
			                    
			                    <li role="presentation"><a href="#other_details" aria-controls="other_details" role="tab" data-toggle="tab" class="dept-cart">Other Details</a></li>
			                    
			                </ul>
			                <!-- Nav tabs -->
			               
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                
			                	<!-- profile -->
			                    
			                    <div role="tabpanel" class="tab-pane fade in active" id="profile">
			                      
			                      <div class="aboutus_area wow fadeInLeft">
			                            
			                    	<h3>Profile</h3>
			                      
			                        <h3>Head Of The Department Desk</h3>
					                  <div class="aboutus_area wow fadeInLeft">
					                     <div class="col-lg-6 col-md-6 col-sm-6">
					                        <img class="hod-img img-responsive" src="${img_path!}/library/library_staff/bharathi-v-new.jpg" alt="image" />	
					                        <div class="qualification_info">
						                        <h3 class="qual-text">Dr. V. Bharathi</h3>
						                        <h4 class="qual-text">Head, Library and Information Centre</h4>
						                        <h3 class="qual-text">M.L.I. Sc., M. Phil., Ph. D</h3>
					                        </div>
					                     </div>
					                <div class="col-lg-6 col-md-6 col-sm-6">
					                <p>Library occupied a place of Pride in K.S. Institute of Technology and is Essential component of the Institute outstanding educational activities. It is a resource centre for teaching, learning and research. Being the heart of the academic centre, it is home for all the information services. It plays a proactive role in enabling access to information resources of all kind and providing innovative, responsive and effective services to meet the changing needs of the academic community.</p>
									<p>The library is spread over two floors with 9500 sq. ft. with a seating capacity of 200. In the First floor Circulation counter, Stack Area, Technical processing section, Librarian office and New arrival section. In the Second floor Reference Section, Digital Library, Reading Room, Discussion room, Journals section and News paper section. Library holds a collection of printed as well as electronic resources which includes books, journals, project reports, bound volumes, previous year question papers, CD's, course materials, e-books and e-journals etc.</p> 

									  
									
					              </div>
								                      
			                <div class="col-lg-12 col-md-12 col-sm-12">
			                 <p>The library operations are automated using LIBSOFT Software. The online Public Access Catalogue 
			                 (OPAC) enables users to search all materials (such as Books, Journals, Magazines, Digital material etc).
			                  available in the library collection. A separate Digital library is provided in the library to access 
			                  e-resources. Library has developed institutional digital repository to archive and provide online access
			                   to intellectual output of the institute.</p>

			                </div>
						
					                
									
			                    
			                    </div>
			                    </div>
			                    </div>
			                    
			                    <!-- profile -->
			    
			                    
			                    <!-- vision and mission -->
			                    <div role="tabpanel" class="tab-pane fade in" id="vision">
			                    
			                        <div class="aboutus_area wow fadeInLeft">
										  <h3>Vision</h3>
					                     <ul class="list">
					                        <li>
					                        To become a good repository of technical information amongst stakeholders.
					                        </li>
					                     </ul>
					                     <h3>Mission</h3>
					                     <ul class="list">
					                        <li>To serve as a storehouse of information through constant acquisition of books,
					                         journals and e-resources.</li>
					                        <li>To establish a friendly learning environment to the users. </li>
					                     </ul>					 
									  </div>
			
			                    </div>
			                    <!-- vision and mission -->
			                    
			                    <!-- Collection of books -->			                    
			                    <div role="tabpanel" class="tab-pane fade" id="books_collection">
			                      
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>Collection of Books</h3>
									<table class="table table-striped course_table">
										<#list libraryBooksList as libraryBooks>
										  <tr>          
											<th style="width:50%">${libraryBooks.heading!}</th>
											<td>${libraryBooks.content!}</td>
											
										  </tr>
										 </#list>
										
									  </table>
									
									
								  </div>
			                    
			                    </div>			                    
			                    <!-- Collection of books -->
			                    
			                    <!-- Services / Facilities -->			                    
			                    <div role="tabpanel" class="tab-pane fade" id="services">
			                      
			                      <div class="aboutus_area wow fadeInLeft">
					
										<h3>Facilities</h3>
									<ul class="list" id="circulation_service">
										
										<li>Stack Area</li>
										<li>Reference Section</li>
										<li>Periodicals Section</li>
										<li>Reading Hall</li>
										<li>Digital Library</li>
										<li>Discussion Room</li>
									</ul>
										<h3>Services</h3>
									
									<ul class="list" id="circulation_service">
									
										<li>Circulation Service</li>
										<li>Reference Service</li>
										<li>User Awareness Programme</li>
										<li>Institutional Repository</li>
										<li>Reprographic Service</li>
										<li>Inter Library Loan </li>
										<li>OPAC (Online Public Access Catalog)</li>
										<li>NPTEL</li>
										<li>News Paper Clippings</li>	 
										
							   
									</ul>
									
									
								  </div>
			                    
			                    </div>			                    
			                    <!-- Services / Facilities -->
			    	
			    				<!-- Infrastructure -->			                    
			                    <div role="tabpanel" class="tab-pane fade" id="infrastructure">			                      
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>Infrastructure </h3>
									<div id="gallerySLide" class="gallery_area">
			
										<h3>First floor</h3>
										
						                <a href="${img_path!}/library/infrastructure/CIRCULATION.JPG" title="Circulation counter">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/CIRCULATION.JPG" alt="img" />
						                <span class="view_btn">View</span>
										
						                </a>
										
						                <a href="${img_path!}/library/infrastructure/STACK AREA.jpg" title="Stack Area">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/STACK AREA.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
						                <a href="${img_path!}/library/infrastructure/technical section.jpg" title="Technical processing section">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/technical section.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
											
						                <a href="${img_path!}/library/infrastructure/NEW ARRIVAL SECTION.jpg" title="New arrival section">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/NEW ARRIVAL SECTION.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
										
										<h3>Second floor</h3>
						                <a href="${img_path!}/library/infrastructure/REFERENCE SECTION.jpg" title="Reference Section">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/REFERENCE SECTION.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
										 <a href="${img_path!}/library/infrastructure/discussion room.jpg" title="Reading and discussion Room">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/discussion room.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
						                				
						                <a href="${img_path!}/library/infrastructure/JOURNAL SECTION.jpg" title="Journals section">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/JOURNAL SECTION.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
						                <a href="${img_path!}/library/infrastructure/NEWS PAPER SECTION.jpg" title="News paper section">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/NEWS PAPER SECTION.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
										<h3>Digital Library</h3>
										
										<a href="${img_path!}/library/infrastructure/DIGITAL LIBRARY.jpg" title="Digital Library">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/DIGITAL LIBRARY.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
										<h3>Electronic Surveillances</h3>
										
										<a href="${img_path!}/library/infrastructure/STACK AREA.jpg" title="Electronic Surveillances">
						                  <img class="gallery_img" src="${img_path!}/library/infrastructure/STACK AREA.jpg" alt="img" />
						                  <span class="view_btn">View</span>
						                </a>
										
						               
						            </div>
															
								  </div>			                    
			                    </div>			                    
			                    <!-- Infrastructure -->
			    
							    
							    		 <!--Faculty  -->
				       <div role="tabpanel" class="tab-pane fade" id="faculty">
				                  <div class="row">
				                     <h3>Staff</h3>
				                     <!--Teaching faculty-->
				                     <div class="col-md-12 col-lg-12 col-sm-12">
				                        <!-- new card -->
				                        <#list facultyList as faculty>
				                        <div class="col-md-6 col-sm-6">
				                           <div class="faculty">
				                              <div class="col-md-6 col-sm-12 faculty-content">
				                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
				                              </div>
				                              <div class="col-md-6 col-sm-12 faculty-content">
				                                 <p>${faculty.name!}</p>
				                                 <p>${faculty.designation!}</p>
				                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
				                              </div>
				                           </div>
				                        </div>
				                        <!-- faculty modal -->
				                        <!-- Modal -->
				                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
				                           <div class="modal-dialog modal-md">
				                              <div class="modal-content">
				                                 <div class="modal-header">
				                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
				                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
				                                 </div>
				                                 <div class="modal-body">
				                                    <div class="col-md-6 col-sm-12">
				                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
				                                    </div>
				                                    <div class="col-md-6 col-sm-12">
				                                       <p><strong> Name:</strong> ${faculty.name!}</p>
				                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
				                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
				                                       <p><strong> Department:</strong> ${faculty.department!}</p>
				                                       <#if faculty.profileURL??>
				                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
				                                       </#if>
				                                    </div>
				                                 </div>
				                                 <div class="modal-footer">
				                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				                                 </div>
				                              </div>
				                           </div>
				                        </div>
				                        <!-- faculty modal -->
				                        </#list>
				                        <!-- new card -->
				                     </div>
				                     <!--Teaching faculty-->
				                   
				                     </div>
				                  </div>
				               
				               <!--Faculty  -->
				               
				               <!-- Gallery  -->
				               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
				                  <div class="row">
				                    <h3>Gallery</h3>
				                    
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <div id="gallerySLide1" class="gallery_area">
				                           <#list libraryGalleryList as libraryGallery>	
				                           <a href="${libraryGallery.imageURL!}" target="_blank">
				                           <img class="gallery_img" src="${libraryGallery.imageURL!}" alt="" />					                    			                   
				                           <span class="view_btn">${libraryGallery.heading!}</span>
				                           </a>
				                           </#list>
				                        </div>
				                     </div>
				                  </div>
				               </div>
				               <!-- Gallery -->
				          		
				          		<!-- E-resources -->			                    
			                    <div role="tabpanel" class="tab-pane fade" id="e_resources">
			                      <div class="row">
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>E-Resources </h3>
									<p>Online resources are subscribed to all the programmes Undergraduate, Postgraduate through VTU E-resource Consortium.

									</p>
									<table class="table table-striped course_table">

												<tr>
													<th>SL No. </th>
													<th>Publishers</th>
													<th>E-Portal</th>
													<th>No. of e- Resources</th>
												</tr>
												
												<#assign count = 1>
											<#list libraryEresourcesList as libraryEresource>	
												<tr>
													<td class="col_eresource">${count!}</td>
													<td class="col_eresource"><img class="eresource_logo" alt="img" src="${libraryEresource.imageURL!}"></td>
													<td class="col_eresource">${libraryEresource.heading!}</td>
													<td class="col_eresource">${libraryEresource.content!}</td>
													
													<#assign count = count + 1>
												</tr>
											</#list>	
									</table>
									<h3>Copyright Restrictions</h3>
									<p>All the users of library resources of KSIT must adhere to copyright laws of publishers
									 and consortia. No copyrighted work may be copied, published, disseminated, displayed,
									  performed, without permission of the copyright holder except in accordance with fair
									   use or licensed agreement. This includes DVDs/CDs and other copyrighted material. 
									   Using these resources for commercial purpose, systematic downloading, copying or 
									   distributing of information is prohibited. KSIT may terminate the online access of 
									   users who are found to have infringed copyright.</p>
									
									<table class="table table-striped course_table">
										<thead>
										  <tr>          
											<th>Do's</th>
											<th>Don'ts</th>
											
											
										  </tr>
										</thead>
										
										<tbody>
										  <tr>          
											<td>Can make limited print/ electronic copies.</td>
											<td>Cannot share resources with non KSIT community.</td>								
										  </tr>
										  
										  <tr>
											<td>Can use for teaching, research, discussion, and other academic purposes.</td>          
											<td>Cannot do systematic or substantial downloading, copying, printing etc.</td>                    
											
										  </tr>
										  
										  <tr>
											<td>Can share with professional community.</td>          
											<td>Cannot publish in other website/blogs/forums.</td>                    
											
										  </tr>
										  
										  
										</tbody>
									  </table>
								
									
								  </div>
			                    </div>
			                    </div>			                    
			                    <!-- E-resources -->
			                    
			                    <!-- OPAC -->			                    
			                    <div role="tabpanel" class="tab-pane fade" id="opac">
			                      <div class="row">
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>OPAC </h3>
									<p><a href="http://202.62.79.40/opac/" target="_blank">Click here</a></p>
									
								  </div>
			                     </div>
			                    </div>			                    
			                    <!-- OPAC -->
			                    
			                      <!-- Course Materials -->			                    
			                    <div role="tabpanel" class="tab-pane fade" id="course_materials">
			                      <div class="row">
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>Course Materials </h3>
									<p>Computer Science and Engineering  <a href="http://www.ksit.ac.in/cse_dept.html#teaching-learning" target="_blank">Click here</a></p>
									<p>Electronics and Communication Engineering  <a href="http://www.ksit.ac.in/ece_dept.html#teaching-learning" target="_blank">Click here</a></p>
									<p>Mechanical Engineering   <a href="http://www.ksit.ac.in/mech_dept.html#teaching-learning" target="_blank">Click here</a></p>
									<p>Electronics and Telecommunication Engineering   <a href="http://www.ksit.ac.in/tele_dept.html#teaching-learning" target="_blank">Click here</a></p>
									<p>Science and Humanities  <a href="http://www.ksit.ac.in/science_dept.html#teaching-learning" target="_blank">Click here</a></p>
									
								  </div>
			                    </div>
			                    </div>			                    
			                    <!-- Course Materials -->
			                    
			                    
			                     <!-- Publications -->			                    
			                    <div role="tabpanel" class="tab-pane fade" id="publications">
			                      <div class="row">
			                      <div class="aboutus_area wow fadeInLeft">
			                    	<h3>Publications </h3>
									<p>Computer Science and Engineering   <a href="http://www.ksit.ac.in/cse_dept.html#research" target="_blank">Click here</a></p>
									<p>Electronics and Communication Engineering   <a href="http://www.ksit.ac.in/ece_dept.html#research" target="_blank">Click here</a></p>
									<p>Mechanical Engineering   <a href="http://www.ksit.ac.in/mech_dept.html#research" target="_blank">Click here</a></p>
									<p>Electronics and Telecommunication Engineering   <a href="http://www.ksit.ac.in/tele_dept.html#research" target="_blank">Click here</a></p>
									<p>Science and Humanities   <a href="http://www.ksit.ac.in/science_dept.html#research" target="_blank">Click here</a></p>
									</div>
								  </div>
			                    
			                    </div>			                    
			                    <!-- Publications -->
			                    
			                    
			                    <!-- Timings -->
			                    <div role="tabpanel" class="tab-pane fade" id="timings">	
			                     <div class="row">	                        
			                        <h3>Library Timings</h3>
                       			
                       			<div class="list_items">
                       				Monday to Saturday : 8.30 AM to 6.00 PM
                       			</div>

                       			                       			
                       			<!--<div class="list_items">
                       				Saturday : 8.30 AM to 4.30 PM 
                       			</div> -->

                       			<h3 class="margin-top10">Circulation Timings</h3>
                       			
                       			<div class="list_items">
                       				Monday to Saturday : 8.30 AM to 4.00 PM
                       			</div>
                       			
								</div>
			                    </div>
			                    <!-- Timings -->
			                    
			                    	                    
			                    <!-- Timings -->
			                    <div role="tabpanel" class="tab-pane fade" id="links">	
			                     <div class="row">	                        
			                        
                       			<!--pdf -->
                       			<h3>Links</h3>
                       				<p><a class="links" href="library_new_arrivals.ftl" target="_blank">New Arrivals</a> </p>
                       				 <p><a class="links" href="http://202.62.79.41:8080/jspui/" target="_blank">KSIT Repository Link</a></p>
                       				
									<p>
                       				<#list libraryLinksList as libraryLinks>
                       				<p><a class="links" href="${libraryLinks.imageURL!}" target="_blank"> ${libraryLinks.content!} </a></p>
                       				</#list>
                       				
                       				
								<!--pdf -->
								</div>
			                    </div>
			                    <!-- Timings -->
			                    
							  <!-- Other Details  -->
				               <div role="tabpanel" class="tab-pane fade" id="other_details">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Other Details</h3>
				                        <#list libraryOtherDetailsList as libraryOtherDetails>
				                        <p>${libraryOtherDetails.heading!}  <a href="${libraryOtherDetails.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				               <!-- Other Details  --> 
							
			
			                    <div role="tabpanel" class="tab-pane fade" id="Section4">
			                        <h3>Section 4</h3>
			                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
			                    </div>
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
  


  
    
    
	
	
</@page>