<@page>
	         <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive_placement">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3 class="text-center">Academic Calendar</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      
                    </blockquote>

				 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
					<#list academicCalanderList as academicCalander>
              
					   <p style="color:blue;">${academicCalander.content!}  <a href="${academicCalander.imageURL!}" class="time_table_link" targrt="_blank">[download here]</a></p>         
											
										
					 </#list>
					
					<p style="color:blue;">Calendar of Events-ODD Semester AUG 2017-DEC 2017 <br/><a href="img\pdf\Calender of Events Odd Sem 2017-18.jpg" target="_blank"> Click here to View/Download</a></p>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
			
				
				
				 
            </div>
          </div>
          <!-- End course content -->
		
		

        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>