<@page>
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <!--<section id="imgBanner">
      <h2>Gallery</h2>
    </section>-->
    <!--=========== END COURSE BANNER SECTION ================-->
    
    <!--=========== BEGIN GALLERY SECTION ================-->
    <!--<section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
                <a href="img/gallery/img-large1.jpg" title="This is Title">
                  <img class="gallery_img" src="img/gallery/img-small1.jpg" alt="img" />
                <span class="view_btn">View</span>
                </a>

            </div>
          </div>
        </div>
      </div>
    </section>-->
    <!--=========== END GALLERY SECTION ================-->
	
	
	
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
              <!-- start blog archive  -->
              <div class="row">
			  
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="img/blog.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Name</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="img/blog.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Name</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
                <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="img/blog.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Name</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				 <!-- start single blog archive -->
                <div class="col-lg-6 col-12 col-sm-12">
                  <div class="single_blog_archive wow fadeInUp">
                    <div class="blogimg_container">
                      <a href="#" class="blog_img">
                        <img alt="img" src="img/blog.jpg">
                      </a>
                    </div>
                    <h2 class="blog_title">Name</h2>
					
                    <div class="blog_commentbox">
                      <p></p>
                      <p></p>
                      
                    </div>
					
                    <p class="blog_summary">
					
					</p>
                    
                  </div>
                </div>
                <!-- start single blog archive -->
				
				
				
				
				
				
              </div>
              <!-- end blog archive  -->
             
            </div>
          </div>
          <!-- End course content -->

         
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>