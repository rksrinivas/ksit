<@page>
<#include "feedback/feedback_list_modal.ftl">
<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                 		<#if mechPageSliderImageList?has_content>
                     		<#list mechPageSliderImageList as mechPageSliderImage>
                     			<#if mechPageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${mechPageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">${mechPageSliderImage.heading}</p>
										<p class="slideInRight">${mechPageSliderImage.content}</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>
	                        <article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/mech/slider/s11.jpg" alt="">
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/mech/slider/s12.jpg" alt="">										 
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/mech/slider/s3.jpg" alt="">
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/mech/slider/s4.jpg" alt="">
	                        </article>
						</#if>
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 

<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 

	
	  <!-- upcoming and latest news list-->
      
	<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at ME
						</h2>
					</div>
				</div>
			</div>		
			<div class="row">
					
				<div class="col-md-12">
					<div class="row">
					
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content darkslategray">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2>
								<!-- marquee start-->
									<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=mechPageLatestEventsList/>
									</div>
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Upcoming Events</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
									<@eventDisplayMacro eventList=mechPageUpcomingEventsList/>
								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->
	
	<!-- welcome -->
	<div class="dept-title">
	   <h1>Welcome To Mechanical Engineering</h1>
	   <p class="welcome-text">Mechanical engineering is an engineering discipline that combines engineering physics and mathematics principles with materials science to design, analyze, manufacture, and maintain mechanical systems. It is one of the oldest and broadest of the engineering disciplines.</p>
	</div>
	<!-- welcome -->


	
	<!--dept tabs -->
<section id="vertical-tabs">
   <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
         <div class="vertical-tab padding" role="tabpanel">
          
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
               <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="dept-cart">Profile</a></li>
               <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision & Mission, PEO, PSO & PO</a></li>
               <li role="presentation"><a href="#research" aria-controls="research" role="tab" data-toggle="tab" class="dept-cart">R & D</a></li>
              
              <li role="presentation"><a href="#professional_bodies" aria-controls="professional_bodies" role="tab" data-toggle="tab" class="dept-cart">Professional Bodies</a></li>
               <li role="presentation"><a href="#professional_links" aria-controls="professional_links" role="tab" data-toggle="tab" class="dept-cart">Professional Links</a></li>
               <li role="presentation"><a href="#teaching-learning" aria-controls="teaching-learning" role="tab" data-toggle="tab" class="dept-cart">Teaching and Learning</a></li>
			   <li role="presentation"><a href="#pedagogy" aria-controls="pedagogy" role="tab" data-toggle="tab" class="dept-cart">Pedagogy</a></li>               
               <li role="presentation"><a href="#content-beyond-syllabus" aria-controls="content-beyond-syllabus" role="tab" data-toggle="tab" class="dept-cart">Content Beyond Syllabus</a></li>
              
               <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab" class="dept-cart">Time Table & Calendar of Events</a></li>
              
               <li role="presentation"><a href="#syllabus" aria-controls="syllabus" role="tab" data-toggle="tab" class="dept-cart">Syllabus</a></li>
              
              
               <li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab" class="dept-cart">News Letter</a></li>
               <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
               
               <li role="presentation"><a href="#fdp" aria-controls="fdp" role="tab" data-toggle="tab" class="dept-cart">FDP</a></li>
               
               
               <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab" class="dept-cart">Achievers</a></li>
               <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
               <li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
               <li role="presentation"><a href="#library_dept" aria-controls="library" role="tab" data-toggle="tab" class="dept-cart">Departmental Library</a></li>
               <li role="presentation"><a href="#placement_dept" aria-controls="placement" role="tab" data-toggle="tab" class="dept-cart">Placement Details</a></li>
               
               <li role="presentation"><a href="#higher_education" aria-controls="higher_education" role="tab" data-toggle="tab" class="dept-cart">Higher Education & Entrepreneurship</a></li>
             
               <li role="presentation"><a href="#student_club" aria-controls="student_club" role="tab" data-toggle="tab" class="dept-cart">Student Club</a></li>

             
               <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab" class="dept-cart">Internships & Projects </a></li>
               <li role="presentation"><a href="#industrial_visit" aria-controls="industrial_visit" role="tab" data-toggle="tab" class="dept-cart">Industrial Visit</a></li>
             
             
               
               <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab" class="dept-cart">Events</a></li>
               <li role="presentation"><a href="#resources" aria-controls="resources" role="tab" data-toggle="tab" class="dept-cart">E-resources</a></li>
              
              
            
  
               <li role="presentation"><a href="#project_exhibition" aria-controls="project_exhibition" role="tab" data-toggle="tab" class="dept-cart">Project Exhibition</a></li>
               <li role="presentation"><a href="#mou-signed" aria-controls="mou-signed" role="tab" data-toggle="tab" class="dept-cart">MOUs Signed</a></li>
               <li role="presentation"><a href="#alumni-association" aria-controls="alumni-association" role="tab" data-toggle="tab" class="dept-cart">Alumni Association</a></li>
			   <li role="presentation"><a href="#nba" aria-controls="nba" role="tab" data-toggle="tab" class="dept-cart">NBA</a></li>
                                          
              <li role="presentation"><a href="#social_activities" aria-controls="social_activities" role="tab" data-toggle="tab" class="dept-cart">Social Activities</a></li>
               <li role="presentation"><a href="#other_details" aria-controls="other_details" role="tab" data-toggle="tab" class="dept-cart">Other Details</a></li>
                       
              <li role="presentation"><a href="#testimonials" aria-controls="testimonials" role="tab" data-toggle="tab" class="dept-cart">Testimonials</a></li>
              
               
               <!--<li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li> -->
            </ul>
            <!-- Nav tabs -->
            <!-- Tab panes -->
            <div class="tab-content tabs">
            
                  	
            	
               <!-- profile -->
               <div role="tabpanel" class="tab-pane fade in active" id="profile">
               <div class="row">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Profile</h3>
						<p>The Department of <a href="https://ksgi.edu.in/Best-College-for-Mechanical-Engineering" class="font-weight-600" target="_blank">Mechanical Engineering</a> is one among the senior and old branches in the
						institution with many prides to its feather. A facility of repute, excellent faculty and a good
						student community has contributed to make the Department vibrant and striving hard to
						achieve excellence in the area of academics and research. In keeping with the continued
						efforts for upgradation and advancement of knowledge, the Department has successfully
						completed the establishment of various labs as per the University requirements and care has
						been taken to procure specialized equipment to inculcate an aptitude for research in faculty as
						well as students. The faculty and students are actively involved in the Research and
						Development and have many International journal publications to their credit. The
						Department is recognized as a Research centre from VTU and 13 research scholars are
						carrying out their research in the various fields of Mechanical Engineering. The Department
						is running a departmental library. The Department has taken the leading role in the IPR
						activities, IEDC activities, NSS, sports and cultural activities of the institution. </p> 
						
						<p>The Students team &#39;Red Line Racing&#39; have participated in the recently concluded SAE BAJA
						competition and secured 34th place in the national level and 2nd place in the state. The
						student team &#39;Prodigy Racers&#39; has achieved 5th position in the GoKart virtual rounds and are
						participating in the actual round of SAE GO Kart Competition. The students of the
						Department have received the overall championship in the institution level sports
						consecutively for the third time. The Department has established a good interaction with the
						industries such as M/s TUV Rhineland, M/s Avasarala Technologies Ltd., M/s Sky Catcher
						Labs, M/s Exergy Heating Solutions Pvt. Ltd. and M/s Indo Qatar International Skills
						Development Centre. The department is organizing seminars, technical talks and industrial
						visits regularly to benefits the students. These interactions are being used effectively by the
						staff and students of the Department to prepare the Project Proposals, to take up the UG and
						PG industrial related projects and for organizing technical training programs. The faculty
						members of the department have the pride of BOE members of VTU and many autonomous
						institutions. They have chaired the sessions in different International and National
						conferences and have been invited as resource persons for delivering Technical talks. With all
						these positive activities, the department is moving forward with a positive attitude.</p>
						           
			       </div>
                  <h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <img class="hod-img img-responsive" src="/img/mech/dr.girish.jpg" alt="image">	
                        <div class="qualification_info">
	                        <h3 class="qual-text">Dr. Girish T. R</h3>
	                        <h4 class="qual-text">Associate Professor & Head</h4>
	                        <h3 class="qual-text">M.Tech. Ph.D</h3>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p> The Department of Mechanical Engineering at KSIT is as old as the Institute itself. 
                        Today, the department of mechanical engineering of KSIT attracts and features an extraordinary rich
                         diversity and quantity of talented individuals, with nearly 450 undergraduates supported by experienced
                          and highly qualified teaching and nonteaching staff. There is a concerted effort to raise the 
                          "Aspirational reference point" of the pupils undergoing graduate program in engineering by subjecting
                           them to continuous value addition. Hence The Department has been producing excellent results with 
                           distinctions in the university examinations consistently. In addition to class room teaching learning
                            processes students are guided and encouraged to be familiar with emerging technology. Our students
                             have been actively associating themselves with industries through industrial projects, in plant
                              training and industrial visits with almost all Mechanical engineering oriented organizations/industries.
                               We feel proud to inform that our students earn places in the merit list of University Examinations
                                every year.   
                           </p>
                        
                     </div>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     	<p>Many of our alumni have pursued higher education in premier institutions world over &
                                 are at top executive positions at various enterprises such as Infosys, Wipro, L&T, Mercedes, 
                                 TOYOTA, Robert Bosch, MICO and innumerable software, Manufacturing, Mechanical Design and service 
                                 organizations. The Department has been recognized as R & D Centre by Visvesvaraya Technological
                                  University for carrying out Research activities leading to M.Sc. (Engg.) and Ph.D degrees.   </p>
                     </div>
                  </div>
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Highlights of department</h3>
                     <ul class="list">
                     	<li>Department has highly motivated faculties with good academic experience.</li>
                     	<li>Club and associations in the department promote innovative thinking among the students.</li>
                     	<li>The department has the student club by name MECHANAS and associations like Society of Automotive Engineers, Institute of Indian Foundrymen, etc.
                     	</li>
                     	<li>The curriculum are framed to the requirements of the industry through industry based curriculum, internships, interscholastic activities etc
                     	</li>
                     	<li>Transparency in academic and administrative processes.</li>
                     	<li>Excellent teaching learning environment emerged as various awards for the students in disciplines of diversified sectors.</li>
                     	<li>The department R&D centres has several facilities with Research Scholars from other institutions and industry, pursuing PhD in various fields.
                     	</li>
                     	<li>The team Redline Racing is consistently participating in the national event BAJA SAEINDIA for the last 5 years and won several awards and medals in the various rounds of then contests.
                     	</li>
                     	<li>The team Prodigy Racers have won several awards and medals in the Indian Karting Championship which is a national level intercollegiate engineering design competition which involves Static and Dynamic stages.
                     	</li>                     	
                     </ul>
                    
                  </div>
                  
                   <div class="aboutus_area wow fadeInLeft">
                     <h3>Academic programs offered:</h3>
                     <h4>UG Program:</h4>
                     <p>Bachelor of Engineering (B.E.) in Mechanical Engineering with approved intake of 60 students and a lateral entry of 20% from 2nd year.
                     </p>
                     <h4>PG Program:</h4>
                     <p>M.Tech Programme in Machine Design with approved intake of 18 students</p>
                     <h4>RESEARCH PROGRAM:</h4>
                     <p>Ph.D</p>
                     
                     <h3>Course Opportunities</h3>
                     <p>The programme endows students with the basic understanding and knowledge of various domains of the stream.
                      A student pursuing a mechanical engineering programme will acquire knowledge about designing of automobiles, 
                      electric motors, aircraft and other heavy vehicles. The students graduated from our department have become 
                      successful professional engineers working in the various core companies and technology based enterprises like 
                      TCS, Infosys, Cognizant, General Motors, T E Connectivity India Pvt Ltd., Neviton Softech Private Limited,
                       Westline Ship Management, Alpha 9 Marine Services, Amazon Development Centre India Pvt Ltd, Tech Mahindra,
                      [24]*7.ai, etc.</p>
                     
                  </div>
                  
                </div> 
               </div>
               <!-- profile -->
               
               <!-- vision and mission -->
               <div role="tabpanel" class="tab-pane fade in" id="vision">
               <div class="row">
                  <div class="aboutus_area wow fadeInLeft">
                     <h3>Vision</h3>
                     	<ul class="list">
							<li>To groom incumbents to compete with their professional
							peers in mechanical engineering that brings recognition</li>
						</ul>
				     <button type="button" style="float:right;" class="btn btn-info btn-sm" data-backgrop="static" data-toggle="modal" data-page="mech_page" data-target="#feedbackListModal">Feedback</button>						
                     <h3>Mission</h3>
                     <ul class="list">
						<li>To impart sound fundamentals in mechanical engineering</li>
						<li>To expose students to new frontiers</li>
						<li>To achieve engineering excellence through experiential learning and team work. </li>
					</ul>
                     <h3>PROGRAM EDUCATIONAL OBJECTIVES (PEOs)</h3>
                     
                     <p>PEO1: To produce graduates who would have developed a strong background in basic science and mathematics
				      and ability to use these tools in Mechanical Engineering.</p> 
				     <p>PEO2: To prepare graduates who have the ability to demonstrate technical competence in their fields of 
				     Mechanical Engineering and develop solutions to the problems.</p> 
				     <p>PEO3: To equip graduates to function effectively in a multi-disciplinary environment individually, within a global,
				      societal, and environmental context.</p>  
				     
					<h3>PROGRAM SPECIFIC OUTCOMES (PSOs)</h3>
				     <p>It is expected that a student in mechanical engineering will possess an:</p> 
				     <p>PSO1: Ability to apply concept of mechanical engineering to design a system, a component or a process/system 
				     to address a real world challenges</p> 
				     <p>PSO2: Ability to develop effective communication, team work, entrepreneurial and computational skills</p>
				     
				     <h3>PROGRAM OUTCOMES (POs)</h3>
				     <p><strong>Engineering Graduates will be able to:</strong> </p>
				     
				     <ul class="list">
				     	<li><strong>Engineering knowledge:</strong> Apply the knowledge of mathematics, science, engineering fundamentals, and an engineering specialization to the solution of complex engineering problems. 
				     	</li>
				     	<li><strong>Problem analysis: </strong> Identify, formulate, review research literature, and analyze complex engineering problems reaching substantiated conclusions using first principles of mathematics, natural sciences, and engineering sciences. 
				     	</li>
				     	<li><strong>Design/development of solutions: </strong> Design solutions for complex engineering problems and design system components or processes that meet the specified needs with appropriate consideration for the public health and safety, and the cultural, societal, and environmental considerations. 
				     	</li>
				     	<li><strong>Conduct investigations of complex problems: </strong> Use research-based knowledge and research methods including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions. 
				     	 </li>
				     	<li><strong>Modern tool usage:</strong> Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations. 
				     	</li>
				     	<li><strong>The engineer and society:</strong>  Apply reasoning informed by the contextual knowledge to assess societal, health, safety, legal and cultural issues and the consequent responsibilities relevant to the professional engineering practice. 
				     	</li>
				     	<li><strong>Environment and sustainability:</strong> Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development. 
				     	 </li>
				     	<li><strong>Ethics:</strong> Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice. 
				     	 </li>
				     	<li><strong>Individual and team work:</strong> Function effectively as an individual, and as a member or leader in diverse teams, and in multidisciplinary settings. 
				     	</li>
				     	<li><strong>Communication:</strong> Communicate effectively on complex engineering activities with the engineering community and with society at large, such as, being able to comprehend and write effective reports and design documentation, make effective presentations, and give and receive clear instructions. 
				     	 </li>
				     	<li><strong>Project management and finance:</strong> Demonstrate knowledge and understanding of the engineering and management principles and apply these to one�s own work, as a member and leader in a team, to manage projects and in multidisciplinary environments. 
				     	 </li>
				     	<li><strong>Life-long learning:</strong> Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change. 
				     	 </li>
				     </ul>				                                                
                  </div>
                  </div>
               </div>
               <!-- vision and mission -->
                              
               <!-- Research and development -->
               <div role="tabpanel" class="tab-pane fade" id="research">
               <div class="row">
                  <h3>Research and Development</h3>
                  <p>Research and development is the first stage for the overall development of any organization. Basically R &amp; D work is carried out without any expectations of making profit, instead directed towards achieving awards, recognitions, patents, copy rights, etc. KSIT Mechanical Engineering R &amp;D centre is equipped with highly qualified and experienced supervisors with the computing and testing facilities for the research scholars to carry out their research.</p>
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     		<h3>MANTRA - The R & D Center</h3>
								<p id="news">Research and undergraduate education are not mutually exclusive components that compete with each other, but can and ought to be, integrated to enrich each other. Research is a learning process, and our goal is to be a community of scholars that joins together in discovering and sharing knowledge.</p>
								<p>Its is a matter of Proudness to announce that The Department has been recognized as R & D Centre by Visvesvaraya Technological University for carrying out Research activities leading to M. Sc. (Engg.) and Ph.D. degrees.</p>
								<p>Established in the year 2007-08 MANTRA (Micro, Aero, Nano, Tribo Research Activities) was tied up with Honeywell, one of the reputed Institutes at world level.</p>
								
								<h4>Major objectives of the MANTRA Centre are:</h4>
									
									<ul class="list">
										
										<li>To create indigenous capability in the emerging areas like Materials, Alloys, composites, Tribology etc.</li>
										<li>Promotion of research among faculties by conducting programs on research methodology, research possibilities in science and technology.</li>
										<li>Creating awareness about scientific writing and reporting in journals and conferences for young faculties.</li>
										<li>To pursue research, develop, evaluate, demonstrate and transfer advanced technologies of critical importance to the manufacturing industry in India & aboard</li>
										<li>To develop research capability on appropriate equipment and manufacturing processes</li>
										<li>To network with R & D centers of similar institutions in state and in India to source and develop the technology required for critical manufacturing applications</li>
										<li>To serve as a role model for industry-Institute collaboration through partnerships.</li>
									
									
									</ul>
									
									
								<p>Many teaching faculties have registered for M.Sc Engineering & their Ph,D at the centre. The Management of KSIT has magnanimously supported financially to start the research activities. The lab has all facilities covering the above mentioned areas. Various testing machines like Pin and Disc wear testing machine, Journal bearing testing equipment, Dry Abrasive wear testing Machine, Erosion & Corrosion equipment, Centrifugal Casting machine, Micro Balance with greater accuracy, and many more have been procured which are being continuously used by Research students of this institution and facilities are also extended to the research scholars of the other institutions.</p>
								
								<h4>Highly appreciated works carried out in the R&D Cell</h4>
								<ul class="list">
								 <li>The Design of Micro Air Vehicle FLEX received honor and appreciation from his Excellency, the former President of India, Dr. A.P.J.  Abdul Kalam.</li>
								 <li>Bagged Gold medal at National level for project titled Wall Climbing Robot from National Design Research Forum.</li>
								 <li>The Project entitled RC Plane was well appreciated and given financial aid by Honeywell, a reputed Multinational Company in Aero Engineering.</li>
								 <li>Bagged 1st prize at All India Level Aero modeling competition, Shastra-2009, held at IIT Madras where various reputed institutions participated.</li>
								
								
								</ul>
								
								
								<h4>Other Projects taken up and successfully carried out were</h4>
								
								<ul class="list">
								 <li>Unmanned Air Vehicles.</li>
								 <li>Design & development of Journal bearing test rig. Also by fabricating the entire set up in house and experiments are carried out successfully.</li>
								 <li>Dry abrasive wear test projects taken up by fabricating the required equipment in house and experiments are carried out successfully</li>
								 <li>Projects under title Erosion & Corrosion were taken up currently and is on going. The required equipments were also fabricated in house and experiments being conducted.</li>
								 <li>Experiments on Non-ferrous alloys, Zinc based Aluminum, cobalt, Nickel, Silicon., Chromium etc alloy have casted  and  experiments conducted in house.</li>
								 <li>The required equipments to carry out the experiments like centrifugal casting, temperature measurement and the required tools were fabricated in house.</li>
								 <li>Based on research outcomes nearly five papers presented at National level conferences</li>
														
								</ul>
								
								 <h3>R & D Activities</h3>
                        
                        <#list mechResearchList as mechResearch>
                        	<p>${mechResearch.heading!}  <a href="${mechResearch.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        <h3>Sponsored Projects</h3>
                        
                        <#list mechSponsoredProjectsList as mechSponsoredProjects>
                        	<p>${mechSponsoredProjects.heading!}  <a href="${mechSponsoredProjects.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        
                        
                        <h3>Ph.D Pursuing</h3>
                        <p>Many researcher scholars under KSIT Mechanical research centre are working in different areas that include design and analysis, composite materials, magnetic bearings, coating techniques, welding process, heat transfer.</p>
                        
                        <#list mechPhdPursuingList as mechPhdPursuing>
                        	<p>${mechPhdPursuing.heading!}  <a href="${mechPhdPursuing.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                        

                        <h3>Ph.D Awardees</h3>
                        <p>Four research scholars have successfully completed their research work from KSIT Mechanical research centre and have been awarded the doctoral degree by the university. Many scholars have published their research findings in good impact factor journals and have completed their Pre-PhD viva-voce.</p>
                        
                         <#list mechPhdAwardeesList as mechPhdAwardees>
                        	<p>${mechPhdAwardees.heading!}  <a href="${mechPhdAwardees.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                        </#list>
                    
                     </div>
                  </div>
                  </div>
               </div>
               <!-- Research and development -->
               <!--Time table  -->
               <div role="tabpanel" class="tab-pane fade" id="timetable">
                  <div class="row">
                     <h3>Time Table</h3>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Class Time Table</h4>
                        <#list mechClassTimeTableList as mechClassTimeTable>              
                        <p>${mechClassTimeTable.heading!}  <a href="${mechClassTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Test Time Table</h4>
                        <#list mechTestTimeTableList as mechTestTimeTable>
                        <p>${mechTestTimeTable.heading!}  <a href="${mechTestTimeTable.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>							
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <h4>Calendar of Events</h4>
                        <#list mechCalanderList as mechCalander>              
                        <p>${mechCalander.heading!}  <a href="${mechCalander.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--Time Table  -->
               
               
                   <!-- Syllabus  -->
               <div role="tabpanel" class="tab-pane fade" id="syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                        <h3>UG (B.E in ME)</h3>
                        
                        <#list mechUgSyllabusList as mechUgSyllabus>
                        <p>${mechUgSyllabus.heading!}  <a href="${mechUgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>			
                        
                         <h3>PG (M.Tech in Machine Design)</h3>
                        
                        <#list mechPgSyllabusList as mechPgSyllabus>
                        <p>${mechPgSyllabus.heading!}  <a href="${mechPgSyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        
                        									
                     </div>
                  </div>
               </div>
               <!-- Syllabus  -->
             
               
               
               <!--News Letter  -->
               <div role="tabpanel" class="tab-pane fade" id="newsletter">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>News letter</h3>
                        <p>Greetings and warm welcome to our department newsletter - EMANATION. Just like gods and the auras churned the ocean of milk to extract the nectar we have tried to churn out the creativity from this mess of science. We made our sincere efforts to bring out the talented concealed within our community. We hope you enjoy reading these volumes as much as we have enjoyed making much. Lastly we thank all the members who made this edition possible. - Team Emanation</p>
                        
                        <#list mechNewsLetterList as mechNewsLetter>
                        <p>${mechNewsLetter.heading!}  <a href="${mechNewsLetter.imageURL!}" class="time_table_link" target="_blank">[download here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!--News Letter  -->
               <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div class="row">
                     <h3>Faculty</h3>
                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Faculty</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name}</p>
                                 <p>${faculty.designation}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id!}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Profile</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               <!--testimonials  -->
               <div role="tabpanel" class="tab-pane fade" id="testimonials">
                  <!--slider start-->
                  <div class="row">
                     <div class="col-sm-8">
                        <h3><strong>Testimonials</strong></h3>
                        <!--<div class="seprator"></div>-->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          
                           <!-- Wrapper for slides -->
                           <div class="carousel-inner">
                              <#list testimonialList as testimonial>
                              <div class="item <#if testimonial?is_first>active</#if>">
                                 <div class="row" style="padding: 20px">
                                    <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                    <p class="testimonial_para">${testimonial.content!}</p>
                                    <br>
                                    <div class="row">
                                       <div class="col-sm-2">
                                          <img src="${testimonial.imageURL!}" class="img-responsive" style="width: 80px">
                                       </div>
                                       <div class="col-sm-10">
                                          <h4><strong>${testimonial.heading!}</strong></h4>
                                          <!--<p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                             <span>Officeal All Star Cafe</span>-->
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </#list>  
                           </div>
                           <div class="controls testimonial_control pull-right">									            	
                              <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="prev"></a>
                              <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                                 data-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--slider end -->
               </div>
               <!--testimonials  -->
          
          
          	               
                   <!-- Achievers -->
               <div role="tabpanel" class="tab-pane fade" id="achievers">
               	<div class="row">
               		 <h3>Department Achievers</h3>
                     <!--Department Achievers-->
		                  <#list mechDepartmentAchieversList as mechDepartmentAchievers>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${mechDepartmentAchievers.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${mechDepartmentAchievers.heading!}</h3>	                      	 
			                       	 <p>${mechDepartmentAchievers.eventDate!} </p>
			                       	 <p>${mechDepartmentAchievers.content!}</p>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
		                    
		              <h3>List of FCD Students</h3>
		              <#list mechFcdStudentsList as mechFcdStudentsList>
                        <p>${mechFcdStudentsList.heading!}  <a href="${mechFcdStudentsList.imageURL!}" class="time_table_link" target="_blank">[View here]</a></p>
                      </#list>
                    
                    
               	</div>
               </div>
               <!-- Achievers -->

               <!-- Facilities / Infrastructure -->
               <div role="tabpanel" class="tab-pane fade" id="facilities">
               	<div class="row">
               		
               		<h3>Infrastructure / Facilities</h3>
               		<p>Thirteen laboratory courses have been listed in the curriculum for the under graduate program in the department. Every laboratory has adequate facility in terms of necessary instruments / equipments for conducting experiments in the department as per the course requirement.</p>
               	
               		 <h3>Department Labs</h3>
                     <!--Teaching faculty-->
		                  <#list mechLabList as mechLab>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${mechLab.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${mechLab.name!}</h3>	                      	 
			                       	 <p>${mechLab.designation!}</p>
			                       	 <a class="btn btn-primary" href="${mechLab.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Facilities / Infrastructure -->
               
                  <!--phd pursuing  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdPursuing">
               	<div class="row">
               		 <h3>Ph.D. Pursuing</h3>
		                  <#list mechPhdPursuingList as mechPhdPursuing>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${mechPhdPursuing.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${mechPhdPursuing.name!}</h3>	                      	 
			                       	 <p>Description: ${mechPhdPursuing.designation!}</p>
			                       	 <a class="btn btn-primary" href="${mechPhdPursuing.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd pursuing -->
               
                 <!--phd awardees  -->
               <!--<div role="tabpanel" class="tab-pane fade" id="phdAwardees">
               	<div class="row">
               		 <h3>Ph.D. Awardees</h3>
		                  <#list mechPhdAwardeesList as mechPhdAwardees>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${mechPhdAwardees.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Name: ${mechPhdAwardees.name!}</h3>	                      	 
			                       	 <p>Description: ${mechPhdAwardees.designation!}</p>
			                       	 <a class="btn btn-primary" href="${mechPhdAwardees.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- phd awardees -->
               
               
               <!-- Gallery  -->
               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
                  <div class="row">
                      <h3>Gallery</h3>
                  
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="gallerySLide" class="gallery_area">
                           <#list mechGalleryList as mechGallery>	
                           <a href="${mechGallery.imageURL!}" target="_blank">
                           <img class="gallery_img" src="${mechGallery.imageURL!}" alt="" />					                    			                   
                           <span class="view_btn">${mechGallery.heading!}</span>
                           </a>
                           </#list>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Gallery -->
               <!-- Dept Library  -->
               <div role="tabpanel" class="tab-pane fade" id="library_dept">
               <div class="row">
                  <h3>Departmental Library</h3>
                  <p>The department has its own library which has more than 2500 text books and reference book catering to the needs of UG (B.E) and PG (M. Tech) students as well as Teaching Staffs. The library preserves previous year's project, dissertation and seminar reports. Every year books of worth Rs. 45,000/- are being added to the stock. The students and Staffs borrow books from the library periodically.
                  </p>
                  <h2 class="titile">Collection Of books</h2>
                  <table class="table table-striped course_table">
                     <#list mechLibraryBooksList as libraryBooks>
                     <tr>
                        <th style="width:50%">${libraryBooks.heading!}</th>
                        <td>${libraryBooks.content!}</td>
                     </tr>
                     </#list>
                  </table>
                  </div>
               </div>
               <!--  Dept Library   -->
               
               <!-- Department placements -->
               <div role="tabpanel" class="tab-pane fade" id="placement_dept">
               <div class="row">
                  <h3>Placement Details</h3>
                  <p>The department of Mechanical Engineering encourages the student to actively participate in the training sessions throughout the year conducted by Placement cell. The department also organizes seminars and workshops on various current topics, provide exposure to latest personality development modules, industrial visits, orientation program personal counseling by the professionals, industry-institute interactions, etc. which has resulted into dramatic placements in the reputed core companies across the country.</p>
                  <#list mechPlacementList as mechPlacement>              
                  <p>${mechPlacement.heading!}  <a href="${mechPlacement.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                  </div>
                </div>											
                  <!-- Department placements -->
                  
                    <!-- Events -->
                  <div role="tabpanel" class="tab-pane fade" id="events">
                  <div class="row">
                    <h3>Events</h3>
                    
                    <#list mechEventsList[0..*3] as mechEvent>
	                  <div class="row aboutus_area wow fadeInLeft">
	                     <div class="col-lg-6 col-md-6 col-sm-6">
	                        <img class="img-responsive" src="${mechEvent.imageURL!}" alt="image" />	                       
	                     </div>
	                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
	                     	 <h3>${mechEvent.heading!}</h3>	                      	 
	                       	 <p>"${mechEvent.content!}"</p>
	                       	 <p><span class="events-feed-date">${mechEvent.eventDate!}  </span></p>
	                       	 <a class="btn btn-primary" href="mech_events.html">View more</a>
	                       	 <#if (mechEvent.reportURL)??>
	                       	 	<a class="title-red" href="${mechEvent.reportURL!}" target="_blank">Report</a>
	                       	 </#if>
	                     </div>
	                  </div>
                  
                           <hr />  
                    </#list>   
  						
                     </div>   
                  </div>
                  <!-- Events  -->
                  
                      <!-- E-resources  -->
               <div role="tabpanel" class="tab-pane fade" id="resources">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>E-resources</h3>
                        
                          <#list mechEresourcesList as mechEresources>
	                    
	                        <p>${mechEresources.heading!}  
	                      	    <a href="//${mechEresources.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
							
                     </div>
                  </div>
               </div>
               <!-- E-resources  -->
               
               
               <!-- Teaching and Learning  -->
               <div role="tabpanel" class="tab-pane fade" id="teaching-learning">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Teaching and Learning</h3>
                     	<p>The faculty of Mechanical Engineering department attends various MOOCS and training programs on advanced topics, to update their knowledge and skills, and gives additional inputs in the classes. Further, the faculty conducts various innovative teaching and learning activities inside and outside the classrooms to engage the students effectively and efficiently. The teaching and learning activities conducted by the faculty for the improvement of student learning includes:</p>
						
						<ul class="list">
							<li>Teaching with working models, simulations and animated videos</li>
							<li>Assignments which include applied level questions and Case studies.</li>
							<li>Conduction of online and classroom quizzes, surprise class tests, open book assignments, group discussions, seminars etc.
							</li>
							<li>Usage of ICT tools, LMS platform: Pupilpod and Google classrooms for posting assignments and lecture materials.
							</li>
						</ul>
					
						<p>The instructional materials and pedagogical activities are uploaded in Google drive and the associated links are made available on institutional website for student and faculty access.</p>
						                     	
                     
                        <h3>Instructional Materials</h3>
                        
                          <#list mechInstructionalMaterialsList as mechInstructionalMaterials>
	                    
	                        <p>${mechInstructionalMaterials.heading!}  
	                      	    <a href="//${mechInstructionalMaterials.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                     <h3>Lab Manuals</h3>
                        
                          <#list mechLabManualList as mechLabManual>
	                    
	                        <p>${mechLabManual.heading!}  
	                      	    <a href="//${mechLabManual.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
									
                     </div>
                  </div>
               </div>
               <!-- Teaching and Learning  -->
               
               <!-- pedagogy -->

                <div role="tabpanel" class="tab-pane fade" id="pedagogy">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Pedagogical Activities</h3>
                        
                          <#list mechPedagogicalActivitiesList as mechPedagogicalActivities>
	                    
	                        <p>${mechPedagogicalActivities.heading!}  
	                      	    <a href="//${mechPedagogicalActivities.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                     
	                      <h3>Pedagogy Review Form</h3>
                        
                          <#list mechPedagogyReviewFormList as mechPedagogyReviewForm>
	                    
	                        <p>${mechPedagogyReviewForm.heading!}  
	                      	    <a href="//${mechPedagogyReviewForm.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>        
                       											
                     </div>
                  </div>
               </div>
              
                <!-- pedagogy -->

               <!-- content beyond syllabus -->

                <div role="tabpanel" class="tab-pane fade" id="content-beyond-syllabus">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Content Beyond Syllabus</h3>						
                        
                         <#list mechcontentbeyondsyllabusList as mechcontentbeyondsyllabus>
                        <p>${mechcontentbeyondsyllabus.heading!}  <a href="${mechcontentbeyondsyllabus.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	                 
                       											
                     </div>
                  </div>
               </div>
              
                <!-- content beyond syllabus -->
               
                 <!-- Sponsored Projects -->
               <!--<div role="tabpanel" class="tab-pane fade" id="sponsored-projects">
               	<div class="row">
               		 <h3> Sponsored Projects </h3>
               		 <p>The Institution receives grants and contracts from organizations, government agencies, non-profit as well as industry and business sponsors. A sponsored project is a short term activity typically lasting from one to five years (depending on the sponsor�s policy). Sponsored projects may be renewed and continue for a longer period.</p>
		                  <#list mechSponsoredProjectsList as mechSponsoredProjects>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${mechSponsoredProjects.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${mechSponsoredProjects.name!}</h3>	                      	 
			                       	 <p>Description: ${mechSponsoredProjects.designation!}</p>
			                       	 <a class="btn btn-primary" href="${mechSponsoredProjects.imageURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>-->
               <!-- Sponsored Projects -->
               
               <!-- MOU's signed -->
               <div role="tabpanel" class="tab-pane fade" id="mou-signed">
                <div class="row">
                  <h3>MOUs Signed</h3>
                  <p>A Memorandum of Understanding (MoU) is a formal agreement between two or more parties. Companies and organizations can use MoUs to establish official partnerships. The MoU intends to jointly work on projects required for industries and research needs. Faculty with good industrial experience and promising students, jointly agree to exchange their expertise for mutual benefit and growth.</p>
                  <#list mechMouSignedList as mechMouSigned>              
                  <p>${mechMouSigned.heading!}  <a href="${mechMouSigned.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- MOU's signed -->
               
                <!-- Alumni Association -->
               <div role="tabpanel" class="tab-pane fade" id="alumni-association">
                <div class="row">
                  <h3>Alumni Association</h3>
                  <p>The KSIT Alumini association "CHIRANTHANA" is an association of alma mater. The purpose of the association is to foster the spirit of loyalty and to promot the general welfare of the organisation. Alumini association exists to support the parent organisation goals and to strengthen the ties between alumni and present student community. The association continuously support the student community by organising the technical talk, workshops, guest lectures from the industry experts. The KSIT alumni meet (CHIRANTHANA) is held on first Sunday of November, every year.</p>
                  <#list mechAlumniAssociationList as mechAlumniAssociation>              
                  <p>${mechAlumniAssociation.heading!}  <a href="${mechAlumniAssociation.imageURL!}" class="time_table_link" target="_blank"> [view here] </a></p>
                  </#list>	
                </div>
                </div>											
               <!-- Alumni Association -->
               
                <!--  nba -->
                <div role="tabpanel" class="tab-pane fade" id="nba">
	                <div class="row">
						<div class="col-lg-6">
							<h3>NBA Links</h3>
							<table style="width: 100%">
								<#list criteriaMap?values as nbaList>
									<tr>
										<td>
											<ul>
												<li>
													${nbaList[0].criteria.desc}
												</li>
												<li>
													<ul>
														<#list nbaList as  nba>
															<li style="padding: 20px"> - ${nba.heading} <a href="${nba.link}" target="_blank">view here</a></li>												
														</#list>
													</ul>
												</li>
											</ul>
										</td>
									</tr>
								</#list>
							</table>
						</div>	
	                </div>
                </div>								
              <!--  nba --> 
              
                <!-- Professional Bodies  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_bodies">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Bodies</h3>
                        
                        <p>The department of Mechanical Engineering is associated with eminent professional bodies to improve the proficiency of the students.</p>
                        
                        <ul class="list">
                        	<li>"Insitution of Engineers (India)" (IEI)</li>
                        	<li>"Society of Automotive Engineers" (SAE)</li>
                        	<li>"Indian Society for Technical Education" (ISTE)</li>
                        	<li>"Indian Institute of Foundrymen" (IIF)</li>
                        </ul>

						<p>The department continuously encourages students and faculties to become members of these professional bodies and motivates to participate, organize various events and competitions under these platforms.
						</p>
                        <#list mechProfessionalBodiesList as mechProfessionalBodies>
                        <p>${mechProfessionalBodies.heading!}  <a href="${mechProfessionalBodies.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Professional Bodies   -->
           
           
           	<!-- Professional Links  -->
               <div role="tabpanel" class="tab-pane fade" id="professional_links">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Professional Links</h3>
                        
                         <#list mechProfessionalLinksList as mechProfessionalLinks>
	                    
	                        <p>${mechProfessionalLinks.heading!}  
	                      	    <a href="//${mechProfessionalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
	                												
                     </div>
                  </div>
               </div>
               <!-- Professional Links   -->
           
                 <!-- Higher Education  -->
               <div role="tabpanel" class="tab-pane fade" id="higher_education">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Higher Education & Entrepreneurship</h3>
                        <p>The department provides the guidelines for the students to pursue their higher studies. The students are trained to appear in the online examination relevant to their field of studies and motivated to score the required grade to get their admission in the reputed universities. The students are encouraged and guided for the selection of universities for the higher studies.</p>
                        <#list mechHigherEducationList as mechHigherEducation>
                        <p>${mechHigherEducation.heading!}  <a href="${mechHigherEducation.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>												
                     </div>
                  </div>
               </div>
               <!-- Higher Education   -->

               <!-- Students Club  -->
               <div role="tabpanel" class="tab-pane fade" id="student_club">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                     
                     	<h3>Mechanas</h3>
                     
                     
                        <h3>Club Activities</h3>
                        <#list mechClubActivitiesList as mechClubActivities>
                        <p>${mechClubActivities.heading!}  <a href="${mechClubActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>External Links (Club)</h3>	
                        
                         <#list mechClubExternalLinksList as mechClubExternalLinks>
	                    
	                        <p>${mechClubExternalLinks.heading!}  
	                      	    <a href="//${mechClubExternalLinks.content!}" class="time_table_link" target="_blank"> [view here] </a>
	                        </p>
	                     
	                     </#list>
						
                     </div>
                  </div>
               </div>
               <!-- Students Club   -->
           
               
                 <!-- Internship  -->
               <div role="tabpanel" class="tab-pane fade" id="internship">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Internship</h3>
                        <p>An internship is a period of work experience offered by an organisation for a limited period of time. It is an opportunity that employers offer to students interested in gaining work experience in particular industries. The purpose of an internship is to develop professional skills in a hands-on environment. Because internships are a learning opportunity, it is important to evaluate the skills that students had developed in their time with the company.</p>
                        
                        <#list mechInternshipList as mechInternship>
                        <p>${mechInternship.heading!}  <a href="${mechInternship.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        
                        
             			<h3>Projects</h3>
                        <#list mechProjectsList as mechProjects>
                        <p>${mechProjects.heading!}  <a href="${mechProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                        
                        <h3>Mini-Projects</h3>                       
                        <#list mechMiniProjectsList as mechMiniProjects>
                        <p>${mechMiniProjects.heading!}  <a href="${mechMiniProjects.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>
                        											
                     </div>
                  </div>
               </div>
               <!-- Internship   -->
               
               <!-- Social Activities  -->
               <div role="tabpanel" class="tab-pane fade" id="social_activities">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Social Activities</h3>
                        <#list mechSocialActivitiesList as mechSocialActivities>
                        <p>${mechSocialActivities.heading!}  <a href="${mechSocialActivities.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Social Activities  -->   
           
             <!-- Other Details  -->
               <div role="tabpanel" class="tab-pane fade" id="other_details">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Other Details</h3>
                        <#list mechOtherDetailsList as mechOtherDetails>
                        <p>${mechOtherDetails.heading!}  <a href="${mechOtherDetails.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Other Details  --> 
               
               <!-- Faculty Development Programme  -->
               <div role="tabpanel" class="tab-pane fade" id="fdp">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>Faculty Development Programme</h3>
                        <#list mechFdpList as mechFdp>
                        <p>${mechFdp.heading!}  <a href="${mechFdp.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
                        </#list>	
                     </div>
                   </div>
               </div>
               <!-- Faculty Development Programme  --> 
           
           
             <!-- Industrial Visit -->
               <div role="tabpanel" class="tab-pane fade" id="industrial_visit">
               	<div class="row">
               		 <h3>Industrial Visit </h3>
               		 
               		 <p>In order to cater the creative minds of mechanical engineering students and to make them abreast with the practical side of their coursework, the department organizes the industrial visit to the reputed organisations in every semester for all the students to enhance their practical knowledge.</p>
						<p>Purpose of Industrial Visit;</p>
						<ul class="list">
							<li>To interact the students with actual industry personals of all the departments.</li>
							<li>To make them aware of the industrial procedures required to get recruited in any company.</li>
							<li>To prepare the students for the selection of carrier path in different departments of industry.</li>
							
						</ul>

		                  <#list mechIndustrialVisitList as mechIndustrialVisit>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${mechIndustrialVisit.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>${mechIndustrialVisit.name!}</h3>	                      	 
			                       	 <p>${mechIndustrialVisit.designation!}</p>
			                       	 <a class="btn btn-primary" href="${mechIndustrialVisit.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Industrial Visit -->
           
           
            <!-- Project Exhibition -->
               <div role="tabpanel" class="tab-pane fade" id="project_exhibition">
               	<div class="row">
               		 <h3>Project Exhibition </h3>
               		 <p>Project work Exhibition will be organized for the final year project work students. Students exhibit their innovative project work for the evaluation from the panel members. It gives an opportunity for the students to expose and get the feedback of their project work and best project will be announced at the end of the program.</p>
		                  <#list mechProjectExhibitionList as mechProjectExhibition>
			                  <div class="row aboutus_area wow fadeInLeft">
			                     <div class="col-lg-6 col-md-6 col-sm-6">
			                        <img class="img-responsive" src="${mechProjectExhibition.imageURL!}" alt="image" />	                       
			                     </div>
			                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
			                     	 <h3>Project Name: ${mechProjectExhibition.name!}</h3>	                      	 
			                       	 <p>Description: ${mechProjectExhibition.designation!}</p>
			                       	 <a class="btn btn-primary" href="${mechProjectExhibition.profileURL!}" target="_blank">View more</a>
			                     </div>
			                  </div>
		                  
		                           <hr />  
		                    </#list>   
                    
               	</div>
               </div>
               <!-- Project Exhibition -->
               
                  <!--  -->
                  <div role="tabpanel" class="tab-pane fade" id="Section4">
                     <h3>Section 4</h3>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper, magna a ultricies volutpat, mi eros viverra massa, vitae consequat nisi justo in tortor. Proin accumsan felis ac felis dapibus, non iaculis mi varius.</p>
                  </div>
                  <!--  -->
                  
               </div>
            </div>
         </div>
      </div>
</section>
<!--dept tabs -->
	
	  
  
</@page>