<@page>
			<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
            
            
             <div class="row">
					<h3 class="text-center">VTU RANK ACHIEVERS</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2016-17</p>
					  				  
                    </blockquote>
                    
               <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>Name: </span>Miss. Nayana.M</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 79.68%</h3>
					<h3 class="singCourse_title"><span>Department: </span> B.E (Telecommunication Engineering Department) </h3>
					<h3 class="singCourse_title"><span>Rank: </span> 7th	</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2016-17</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
                    
							
                <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	1KS15LEC02 </h3>
					<h3 class="singCourse_title"><span>Name: </span> Mrs.NAVYA. R</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 79.21%</h3>
					<h3 class="singCourse_title"><span>Department: </span> M.Tech  (Digital Electronics&Communication)</h3>
					<h3 class="singCourse_title"><span>Rank: </span> 2nd	</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2016-17</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
                
              
              </div>
            
			  
              <div class="row">
					<!--<h3>VTU RANK ACHIEVERS</h3>-->
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2013-14</p>
					  				  
                    </blockquote>
                    
							
                <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	1KS10CS103 </h3>
					<h3 class="singCourse_title"><span>Name: </span> SUSHMA SHADAKSHARY</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 83.42%</h3>
					<h3 class="singCourse_title"><span>Rank: </span> 10th	</h3>
					<h3 class="singCourse_title"><span>Year: </span> 2013-14</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	1KS10ME087 </h3>
					<h3 class="singCourse_title"><span>Name: </span> SAI CHARAN.H.Y</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 84.75%</h3>
					<h3 class="singCourse_title"><span>Rank: </span> 9th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2013-14</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				 <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2012-13</p>
					  				  
                    </blockquote>
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	1KS09ME040 </h3>
					<h3 class="singCourse_title"><span>Name: </span> KARTHIK.N</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 85.57%</h3>
					<h3 class="singCourse_title"><span>Rank: </span> 4th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2012-13 </h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2011-12</p>
					  				  
                    </blockquote>
				
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	 1KS08TE015</h3>
					<h3 class="singCourse_title"><span>Name: </span> GREESHMA.S</h3>
					<h3 class="singCourse_title"><span>Percentage: </span>82.99%</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	8th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2011-12</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2010-11</p>
					  				  
                    </blockquote>
				
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	 1KS07ME050</h3>
					<h3 class="singCourse_title"><span>Name: </span> PUNEETH.B.M</h3>
					<h3 class="singCourse_title"><span>Percentage: </span>86.00% </h3>
					<h3 class="singCourse_title"><span>Rank: </span>	1st </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2010-11</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				<blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2009-10</p>
					  				  
                    </blockquote>
				
				
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	1KS06TE037 </h3>
					<h3 class="singCourse_title"><span>Name: </span> SHRUTHI.S</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 84.32%</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	2nd </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2009-10</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2008-09</p>
					  				  
                    </blockquote>
				
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	1KS05TE055 </h3>
					<h3 class="singCourse_title"><span>Name: </span> UTTAM CHAND. M</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 81.77 %</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	6th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2008-09</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				<blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2007-08</p>
					  				  
                    </blockquote>
				
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	1KS04TE045 </h3>
					<h3 class="singCourse_title"><span>Name: </span> SANDEEP.A.RAO</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 80.00%</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	9th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2007-08</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	 1KS04ME017</h3>
					<h3 class="singCourse_title"><span>Name: </span> D. PAWAN KUMAR</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 81.38%</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	8th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2007-08</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				<blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>Vtu rank achievers in the year 2006-07</p>
					  				  
                    </blockquote>
				
				
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	 1KS02ME057</h3>
					<h3 class="singCourse_title"><span>Name: </span> VIKAS ADIGA. B.J</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 82.10%</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	8th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2006-07</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	 1KS02ME056</h3>
					<h3 class="singCourse_title"><span>Name: </span> VAIBHAV. L.V</h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 83.06%</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	6th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2006-07</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				   <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
                    <h3 class="singCourse_title"><span>USN: </span>	 1KS02ME048</h3>
					<h3 class="singCourse_title"><span>Name: </span>SHASHANK. H.N </h3>
					<h3 class="singCourse_title"><span>Percentage: </span> 83.42%</h3>
					<h3 class="singCourse_title"><span>Rank: </span>	5th </h3>
					<h3 class="singCourse_title"><span>Year: </span> 2006-07</h3>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
			</div>
		 </div>
		</div>
	</div>
  </div>
</section>  
</@page>