<@page>
	<div class="container" style="background-color:white;min-height:600px">

		<div class="row">
			<#list placementStatisticsList as placementStatistics>
				<div class="col-lg-4 col-md-4 col-sm-4">
					${placementStatistics.heading!}
					<img src="${placementStatistics.imageURL!}" width="100%"/>
  				</div>
  			</#list>
  		</div>
	</div>
</@page>