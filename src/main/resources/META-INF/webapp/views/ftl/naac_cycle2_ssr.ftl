<@page>
			<div class="dept-title">
   				<h1>NAAC Cycle - 2 </h1>   
				<h3 class="color_red text-center">Part A</h3>
			</div>
<section id="vertical-tabs" style="min-height: 600px;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="vertical-tab padding" role="tabpanel">
					<ul class="nav nav-tabs" role="tablist" id="myTab">
						<#list criteriaList as criteria>
						
							<li role="${criteria}" <#if criteria?index==0>class="active"</#if>><a href="#${criteria}" aria-controls="${criteria}" role="tab" data-toggle="tab" class="dept-cart">${criteria.desc}</a></li>
						</#list>					
					</ul>
					<div class="tab-content tabs">
						<#list criteriaList as criteria>
							<div role="tabpanel" class="tab-pane fade in <#if criteria?index==0>active</#if>" id="${criteria}">
								<h2 style="text-align: center;">${criteria.desc}</h2>
								<table style="width: 100%">
									<#list criteriaMap?values as naacList>
										<#if naacList[0].criteria == criteria>
											<tr>
												<ul>
													<li>
														${naacList[0].criteria.desc}
													</li>
													<li>
														<ul>
															<#list naacList as  naac>
																<li style="padding: 20px"> - ${naac.heading} <a href="${naac.link}" target="_blank">view here</a></li>												
															</#list>
														</ul>
													</li>
												</ul>
											</tr>
										</#if>
									</#list>
								</table>
							</div>
						</#list>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

</@page>