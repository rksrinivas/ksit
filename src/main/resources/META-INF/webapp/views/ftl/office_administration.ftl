<@page>

<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
             
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/banner/office_admins.jpg" alt="office staff">                           
                        </article>                     
                     </div>
                     <!-- Indicators -->
                     <!--<ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                     </ol>-->
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 
	
   

          <!-- start course archive sidebar -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            
            
           
                
                      
                 
                      
                      	<div class="col-lg-12 col-md-12 col-sm-12">

                       		<div class="list">

                       			<h3>COLLEGE OFFICE TIMINGS</h3>
                       			<div class="list_items"> <b>	Monday to Saturday :</b>  </div>
                       			<div class="list_items"> &#9656; 8.30 AM to 4.00 PM	</div>
                           </div>
                        </div>
                           
                        <div class="col-lg-12 col-md-12 col-sm-12">
                           <div class="list">
                              <h3>SEMESTER MARKSCARD ISSUE TIMINGS</h3>									
                                 <div class="list_items"><b>	Monday to Saturday : </b></div>

                                 <div class="list_items">&#9656; Tea break & Lunch break </div>
                                                               
                       		</div>	
                       </div>
                      <!--alignment-->
                       					
                       
                      </div>
            
              
        
    
   
    </section>    <!--=========== END COURSE BANNER SECTION ================-->
    

   
   
    <!--=========== BEGIN WHY US SECTION ================-->
<section id="whyUs_dept">
   <!-- Start why us top -->
   <div class="row">
      <div class="col-lg-12 col-sm-12">
         <div class="whyus_top">
            <div class="container">
               <!-- Why us top titile -->
               <div class="row">
                  <div class="col-lg-12 col-md-12">
                     <div class="title_area">
                        <h2 class="titile text-center">Administrative Staff</h2>
                        <span></span> 
                     </div>
                  </div>
               </div>
               <!-- End Why us top titile -->
               <!-- Start Why us top content  -->
               <div class="row">
                  <!-- faculty -->			  
                  <div class="teacher_area home-2">
                     <div class="container">
                        <div class="row">
                           <!--start teacher single  item -->
                        </div>
                     </div>
                  </div>
                  <!-- teachers details-->
                  <!--start ads  area -->
                  <div class="teacher_area home-2">
                     <div class="container">
                        <div class="row">
                           <div class="col-md-12 col-lg-12 col-sm-12">
                              <!--faculty-->
                              <#list facultyList as faculty>
                              <div class="col-md-3 col-lg-3 col-sm-6">
                                 <div class="single_teacher_item">
                                    <div class="teacher_thumb">
                                       <img src="${faculty.imageURL!}" alt="" />
                                       <div class="thumb_text">
                                          <h2>${faculty.name}</h2>
                                          <p>${faculty.designation}</p>
                                       </div>
                                    </div>
                                    <div class="teacher_content">
                                       <h2>${faculty.name}</h2>
                                       <span>${faculty.designation}</span>
                                       <p>${faculty.qualification}</p>
                                       <span>${faculty.department}</span>
                                       <p><a href=""${faculty.profileURL!}" target="_blank">view profile</a></p>
                                       
                                    </div>
                                 </div>
                              </div>
                              </#list>
                              <!--end-->
                           </div>
                        </div>
                     </div>
                     <!--end teacher  area -->
                     <!-- end of teacher details-->
                  </div>
                  <!-- End Why us top content  -->
               </div>
            </div>
         </div>
      </div>
      <!-- End why us top -->
   </div>
   </div>            
   </div>
   </div>        
   </div>
   <!-- End why us bottom -->
</section>
<!--=========== END WHY US SECTION ================-->
<!--=========== BEGIN WHY US SECTION ================-->
<section id="whyUs_dept">
   <!-- Start why us top -->
   <div class="row">
      <div class="col-lg-12 col-sm-12">
         <div class="whyus_top">
            <div class="container">
               <!-- Why us top titile -->
               <div class="row">
                  <div class="col-lg-12 col-md-12">
                     <div class="title_area">
                        <h2 class="titile text-center">Supporting Staff</h2>
                        <span></span> 
                     </div>
                  </div>
               </div>
               <!-- End Why us top titile -->
               <!-- Start Why us top content  -->
               <div class="row">
                  <!-- faculty -->			  
                  <div class="teacher_area home-2">
                     <div class="container">
                        <div class="row">
                           <!--start teacher single  item -->
                        </div>
                     </div>
                  </div>
                  <!-- teachers details-->
                  <!--start ads  area -->
                  <div class="teacher_area home-2">
                     <div class="container">
                        <div class="row">
                           <div class="col-md-12 col-lg-12 col-sm-12">
                              <!--faculty-->
                              <#list supportingStaffFacultyList as faculty>
                              <div class="col-md-3 col-lg-3 col-sm-6">
                                 <div class="single_teacher_item">
                                    <div class="teacher_thumb">
                                       <img src="${faculty.imageURL!}" alt="" />
                                       <div class="thumb_text">
                                          <h2>${faculty.name}</h2>
                                          <p>${faculty.designation}</p>
                                       </div>
                                    </div>
                                    <div class="teacher_content">
                                       <h2>${faculty.name}</h2>
                                       <span>${faculty.designation}</span>
                                       <p>${faculty.qualification}</p>
                                       <span>${faculty.department}</span>
                                       <p><a href="${faculty.profileURL!}" target="_blank">view profile</a></p>
                                       
                                    </div>
                                 </div>
                              </div>
                              </#list>
                              <!--end-->
                           </div>
                        </div>
                     </div>
                     <!--end teacher  area -->
                     <!-- end of teacher details-->
                  </div>
                  <!-- End Why us top content  -->
               </div>
            </div>
         </div>
      </div>
      <!-- End why us top -->
   </div>
   </div>            
   </div>
   </div>        
   </div>
   <!-- End why us bottom -->
</section>
<!--=========== END WHY US SECTION ================-->    

					   
									 
   
</@page>