<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			<!--<h3>Site Map</h3>-->
          <div class="row container-fluid">
				  <h2 class="text-center">Department wise Research &amp; Development details</h2>
				  <div class="col-lg-12 col-md-12 col-sm-12">
				 	<p>
				 		The faculty of our college are actively engaged in carrying out sponsored research projects in the areas of Image Processing, Cryptography, Machine Learning and VLSI Technologies. The details of the funded projects are given below:
				 	</p>
				 	<p>
				 		"Development of Bioinspired Algorithms for Real Time Piracy Detection Systems based on Thermogram Analysis"
				 		<br />
				 		Government of Karnataka, Vision Group on Science and Technology has awarded Rs 3 Lakhs to Dr. Surekha Borra, Professor, Dept. of ECE, to implement this project under the scheme
				 		<br />
				 		"Research Grant for Scientists/Faculty (RGS/F)", in the academic year 2020-21. This Project is currently ongoing.
				 	</p>
				 </div>
			  
          <!-- start single course -->
          <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="single_course wow fadeInUp">   
          <div class="singCourse_content">
					<h2>&#9656; Computer Science &amp; Engineering</h2>
                    <!--<h3 class="singCourse_title"><a href="#" target="_blank">Home</a></h3>-->
					          <p><a href="img\cse\cse_links\About CSE R& D.pdf" target="_blank">About CSE R& D</a></p>
					          <p><a href="img\cse\cse_links\KSIT R&D research scholar details -2017-May.pdf" target="_blank">KSIT R&D research scholar details -2017-May</a></p> 
                    </div>
                  </div>
                </div>
                <!-- End single course -->
				
				
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					<h2>&#9656; Electronics &amp; Communication Engineering</h2>
								<p><a href="img\ece\ece_links\Funds and Projects.pdf" target="_blank">Funds and projects</a></p>
								<p><a href="img\ece\ece_links\Publications 2016-17.pdf" target="_blank">Publications 2016-17</a></p>
								<p><a href="img\ece\ece_links\Publications 2015-16.pdf" target="_blank">Publications 2015-16</a></p>
								<p><a href="img\ece\ece_links\Publications 2014-15.pdf" target="_blank">Publications 2014-15</a></p>
								<p><a href="img\ece\ece_links\Publications 2013-14.pdf" target="_blank">Publications 2013-14</a></p>
								<p><a href="img\ece\ece_links\Phd Scholars.pdf" target="_blank">Faculty pursuing PhD</a></p>  
                </div>
              </div>
            </div>
        <!-- End single course -->

				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    <div class="singCourse_content">
					      <h2>&#9656; Mechanical Engineering</h2>        
					          <p> <a href="#" target="_blank">MANTRA - The R & D Center </a></p>  
                    </div>
                  </div>
                </div>
                <!-- End single course -->
			
				<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    <div class="singCourse_content">
					      <h2>&#9656; Basic Science</h2>
                    <p> <a href="img\science\chemistry_events\Chemistry R&D details.pdf" target="_blank">Chemistry R&D details</a></p>
							      <p> <a href="img\science\chemistry_events\publications.pdf" target="_blank">National and International List of Publications </a></p> 
                    </div>
                  </div>
                </div>
                <!-- End single course -->
              </div>
            </div>
          </div>
          <!-- End course content -->
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>