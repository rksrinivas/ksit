 <@page>
	<section id="alumni">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12"> 
					<div class="title_area">
						<h2 class="title_two">Alumni Members</h2>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="alumni_form wow fadeInLeft">						
						<table id="alumniList" class="table table-striped table-bordered" style="width:80%;margin-left:auto;margin-right:auto;">
							<thead style="width:100%">
								<th>Name</th>
								<th>USN</th>
								<th>Year of Passing</th>
								<th>Branch</th>
							</thead>
							<tbody>
								<#if alumniList?has_content>
									<#list alumniList as alumni>
										<tr>
											<td>
												${alumni.name!}
											</td>
																					
											<td>
												${alumni.usn!}
											</td>
											
											<td>
												${alumni.yearOfPassing!}
											</td>
											
											<td>
												${alumni.branch!}
											</td>
											
										</tr>
									</#list>
								</#if>
							</tbody>
							<tfoot style="width:100%">
								<th>Name</th>
								<th>USN</th>
								<th>Year of Passing</th>
								<th>Branch</th>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
<script type="text/javascript">
	$(document).ready( function () {
	    $('#alumniList').DataTable({
	    	dom: 'Bfrtip',
        	buttons: [
            	'copy', 'csv', 'excel', 'pdf', 'print'
        	]
        });
	});
</script>	
</@page>