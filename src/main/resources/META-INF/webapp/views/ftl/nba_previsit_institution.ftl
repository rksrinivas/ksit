<@page>
<style>
.headingnba {
	text-align: center;
	color: red;
	font-size: 2em;
	font-weight:900;
	margin-bottom: 30px;
}
</style>
<br/>
<br/>
<div class="container" style="background-color:white;min-height:600px">
	<div class="row">
		<h1 class="headingnba">${heading!}</h1>
		<#list nbaPrevisitList as nbaPrevisit>
			<p>
				${nbaPrevisit.rank}. <span style="color: blue;">${nbaPrevisit.heading}</span>		
				<br />
				<span style="color: black;">${nbaPrevisit.description}</span>				
				<a href="${nbaPrevisit.link}" target="_blank"><span style="color: red;">${nbaPrevisit.link}</span></a>
				<br />
				<#if nbaPrevisit.subPrevisitList?has_content>
					<div style="padding-left: 100px">
						<ul>
						<#list nbaPrevisit.subPrevisitList as subList1>
							<li>
								${subList1.rank}. <span style="color: blue;">${subList1.heading}</span>
								<br />
								<span style="color: black;">${subList1.description}</span>
								<a href="${subList1.link}" target="_blank"><span style="color: red;">${subList1.link}</span></a>
								<br />										
								<#if subList1.subPrevisitList?has_content>
									<div style="padding-left: 100px">
										<ul>
										<#list subList1.subPrevisitList as subList2>
											<li>
												${subList2.rank}. <span style="color: blue;">${subList2.heading}</span>
												<br />
												<span style="color: black;">${subList2.description}</span>
												<a href="${subList2.link}" target="_blank"><span style="color: red;">${subList2.link}</span></a>
												<br />							
												<#if subList2.subPrevisitList?has_content>
													<div style="padding-left: 100px">
														<ul>
														<#list subList2.subPrevisitList as subList3>
															<li>
																${subList3.rank}. <span style="color: blue;">${subList3.heading}</span>
																<br />
																<span style="color: black;">${subList3.description}</span>
																<a href="${subList3.link}" target="_blank"><span style="color: red;">${subList3.link}</span></a>
																<br />																										
																<#if subList3.subPrevisitList?has_content>
																	<div style="padding-left: 100px">
																		<ul>
																		<#list subList3.subPrevisitList as subList4>
																			<li>
																				${subList4.rank}. <span style="color: blue;">${subList4.heading}</span>
																				<br />
																				<span style="color: black;">${subList4.description}</span>
																				<a href="${subList4.link}" target="_blank"><span style="color: red;">${subList4.link}</span></a>
																				<br />																																											
																			</li>
																		</#list>
																		</ul>
																	</div>
																</#if>
															</li>
														</#list>
														</ul>
													</div>
												</#if>
											</li>
										</#list>
										</ul>
									</div>
								</#if>
							</li>
						</#list>
						</ul>
					</div>
				</#if>
			</p>
		</#list>
	</div>
</div>
</@page>