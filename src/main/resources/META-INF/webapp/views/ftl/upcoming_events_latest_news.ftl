<@page>
	<div class="container" style="background-color:white;min-height:600px">

		<div class="row">
			<#list placementDriveList as placementDrive>
				<div class="col-lg-4 col-md-4 col-sm-4">
					${placedStudentsList.heading!}
					<img src="${placedStudentsList.imageURL!}" width="100%"/>
  				</div>
  			</#list>
  		</div>
	</div>
</@page>