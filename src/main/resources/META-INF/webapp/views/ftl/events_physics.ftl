<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <!--<section id="imgBanner_cse">
      <h2></h2>
    </section>-->
    <!--=========== END COURSE BANNER SECTION ================-->
	
	
	 <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
         

          <!-- start course archive sidebar -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_sidebar">
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Physics <span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
				
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img\" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <h4>National conference</h4>
					   <p>National Conference on current Advances of Science and Technology</p>
                       <span class="feed_date">2017-05-10</span>
                      </div>
                    </div>
                  </li>  
				  
				   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img\science\physics_events\DSC_0283.JPG" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <h4>Science Competitions </h4>
					   <p>Department of Science and Humanities of KSIT and KSGI is organizing Science Competitions for the 1st year students on Science Day on 18th March 2011.</p>
                       <span class="feed_date">2011-03-18</span>
                      </div>
                    </div>
                  </li>  
				  
				  
                                 
                </ul>
              </div>
              <!-- End single sidebar -->
             
			  
            </div>
          </div>
          <!-- start course archive sidebar -->
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>