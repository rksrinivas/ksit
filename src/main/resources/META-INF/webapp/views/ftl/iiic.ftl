<@page>
<br/>
<!-- welcome -->
<div class="dept-title">
	<h1>Welcome To Industry Institute Interaction Cell (IIIC)</h1>
</div>
<!-- welcome -->
<section id="vertical-tabs">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="vertical-tab padding" role="tabpanel">
					<ul class="nav nav-tabs" role="tablist" id="myTab">
						<li role="presentation" class="active"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Vision</a></li>
						<li role="presentation"><a href="#activities" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Activities</a></li>
						<li role="presentation"><a href="#members" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Members</a></li>
						<li role="presentation"><a href="#mou" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">MoU's</a></li>
						<li role="presentation"><a href="#gallery1" aria-controls="vision" role="tab" data-toggle="tab" class="dept-cart">Gallery</a></li>
					</ul>
					<div class="tab-content tabs">
						<div role="tabpanel" class="tab-pane fade in active" id="vision">
							<div class="row">
								<b>
									<h2>Vision</h2>
								</b>
								<p>
									To recognize and discover the possibilities of having linkages with the industry to enhance
									experiential learning
								</p>
								<b>
									<h2>Mission</h2>
								</b>
								<p>
								<ul style="list-style-type: circle;">
									<li>To develop the good relations with industrial partners.</li>
									<li>To take an active part to help solve problems in the industrial arena.</li>
									<li>To bring about meaningful linkages between the two partners through agreement and MoU�s.</li>
								</ul>
								</p>
								<b>
									<h2>Objective</h2>
								</b>
								<p>
								<ul>
									<li>To promote the experiential learning through collaboration with the industrial partners by jointly developing programs for 
										Training, Internships, Guest lectures, FDPs, Projects, Industrial visits and other links.
									</li>
								</ul>
								</p>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade in" id="activities">
							<div class="row">
								<p>
									<ul style="list-style-type: circle;">
										<li>
											Building rapport with user systems to develop data bases containing information about industrial personal with a flair for 
											academics who are interested in promoting the industry institution linkages like Guest Lectures, Interactive Workshops, Technical Seminars &amp; 
											Discussions, Project guidance for student as well as faculty training.
										</li>
										<li>
											To identify appropriate industrial expertise to help enrich the academic content.
										</li>
										<li>
											To guide the institution in arranging industrial visits/ internship/orientation/training for both teachers and students continuously.
										</li>
										<li>
											To arrange internship programmes for students of all semesters of engineering curriculum by creating a database of supporting user systems.
										</li>
										<li>
											To bring industries and institutions to jointly organize exhibitions and fairs for mutual benefit.
										</li>
									</ul>
								</p>
								<#list activitiesEventsList as activitiesEvent>
									<div class="row aboutus_area wow fadeInLeft">
										<div class="col-lg-6 col-md-6 col-sm-6">
											<img class="img-responsive" src="${activitiesEvent.imageURL!}" alt="image" />	                       
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6">
											<h3>${activitiesEvent.heading!}</h3>
											<p>"${activitiesEvent.content!}"</p>
											<p><span class="events-feed-date">${activitiesEvent.eventDate!}  </span></p>
											<#if (activitiesEvent.reportURL)??>
											<a class="title-red" href="${activitiesEvent.reportURL!}" target="_blank">Report</a>
											</#if>
										</div>
									</div>
									<hr />
								</#list>   
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade in" id="members">
							<div class="row">
								<h3><b>IIIC Members</b></h3>
								<table class="table table-striped course_table" style="text-align:left !important;">
									<thead>
										<tr>
											<th>
												Sl. No
											</th>
											<th>
												Name 
											</th>
											<th>
												Designation
											</th>
											<th>
												Role
											</th>
										</tr>
									</thead>
									<tbody>
										<#list facultyList as faculty>
										<tr>
											<td>${faculty?index+1}</td>
											<td>${faculty.name}</td>
											<td>${faculty.designation}</td>
											<td>${faculty.qualification}</td>
										</tr>
										</#list>
									</tbody>
								</table>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade in" id="mou">
							<div class="row">
								<b>
									<h1>MoU's signed till date</h1>
								</b>
								<#list mouList as mou>
								<a href="${mou.imageURL!}" title="" target="_blank">${mou.heading!}</a>
								<br />
								</#list>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade in" id="gallery1">
							<div class="row">
								<b>
									<h1>Photo Gallery</h1>
								</b>
								<div class="col-md-12">
									<#list iiicGalleryList as iiicGallery>
									<div class="col-md-4">
										<a href="${iiicGallery.imageURL!}" title="">
										<img class="gallery_img" src="${iiicGallery.imageURL!}" alt="img" style="width:100%"/>
										<span class="view_btn">View</span>
										</a>
									</div>
									</#list>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</@page>