<@page>
	<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/profile.php?id=100083111692109&mibextid=LQQJ4d" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                <!--  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li> -->
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://www.instagram.com/ksit.placements?igsh=ZzJ4cGw5MXlpd2V6" target="_blank"><i class="fa fa-instagram"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/placements/slider/s1.JPG" alt="">
                          
                        </article>
                        <article class="item">
                           <img class="animated bounceIn" src="${img_path!}/placements/slider/s2.JPG" alt="">										
                           
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/placements/slider/s3.JPG" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/placements/slider/s4.JPG" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/placements/slider/s5.JPG" alt="">
                           
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/placements/slider/s6.JPG" alt="">                         
                        </article>
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 

<!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 
    <!-- upcoming and latest news list-->
      
	<!--start news  area -->	
	<section class="quick_facts_news">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h2 class="titile newsarea">
							 The Latest at Placements
						</h2>
					</div>
				</div>
			</div>	 

		<!--	<div class="col-md-6 col-sm-6">
				<div class=""><img class="img-responsive" src="${img_path!}/placements/placementyearwise.jpg" alt="image" /></div>
			</div> -->

			<div class="row">
					
				<div class="col-md-12">
					<div class="row"> 
					
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb"> 
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<!--<div class="news_content darkslategray"> -->
							<div class="news_content">
								<div class="col-md-10 col-sm-10">
								<img class="img-responsive" src="${img_path!}/placements/placementyearwise.jpg" alt="image" />
								</div>
							<!--	<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Latest News</h2> -->
								<!-- marquee start-->
								<!--	<div class='marquee-with-options'>
										<@eventDisplayMacro eventList=placementPageLatestEventsList/>
									</div>  -->
									<!-- marquee end-->
								
							</div>
						</div>						
					</div>  
					<!--end single news  item -->	
					
					<!--start single news  item -->	
					<div class="col-md-6 col-sm-6">
						<div class="single_news_item">
							<div class="news_thumb">
							<!--<img src="${img_path!}/slider/Slide1.jpg" alt="" />	-->
							</div>
							<div class="news_content maroon">
								
								<h2 class="outline"><i class="fa fa-newspaper-o" id="newsletter-icon"></i> Placement News</h2>
								<!--<marquee direction="up" scrollamount="3" onmouseover="stop()"; onmouseout="start();"></marquee>-->
								
								<!-- marquee start-->
								<div class='marquee-with-options'>
								<@eventDisplayMacro eventList=placementPageUpcomingEventsList/>		

								</div>
								<!-- marquee end-->
							</div>
						</div>						
					</div>
					<!-- end single news-->
					
					</div>
				</div>
			</div>
	</div>
	</div>
	</section>
	<!--end news  area -->
 <!-- upcoming and latest news list-->

<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To Placements Department</h1>
   <p class="welcome-text"> Placement is an integral part of the education process of a student in KSIT. 
   Placement preparation starts soon after admissions.</p>
</div>
<!-- welcome -->

<!-- Companies Visited 2024 -->
<section id="about">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
		<div class="row">
			<div class="col-md-12">
			<h1 class="color-red">Companies Visited for 2024 Batch</h1>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-12">         	      
            		<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name Of Company</th>
												
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>1.</td>
												<td>SAP LABS </td>
												
											  </tr>
											  
											   <tr>          
												<td>2.</td>
												<td>AMAZON</td>
												
											  </tr>
											  
											   <tr>          
												<td>3.</td>
												<td>BOSCH</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>4.</td>
												<td>JUSPAY	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>5.</td>
												<td>SIEMENS</td>
											
											
											  </tr>
											  
											   <tr>          
												<td>6.</td>
												<td>LENOVA</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>7.</td>
												<td>MORGAN STANLY</td>
												
												
											  </tr>
											  
											
											  
											  <tr>          
												<td>8.</td>
												<td>VTIGERS </td>
												
											  </tr>

											  <tr>          
												<td>9.</td>
												<td>TCS  </td>
												
											  </tr>

											  <tr>          
												<td>10.</td>
												<td>ACCENTURE</td>
												
											  </tr>

											  <tr>          
												<td>11.</td>
												<td>INFOSYS </td>
												
											  </tr>

											  <tr>          
												<td>12.</td>
												<td>MATHCO </td>
												
											  </tr>

											  <tr>          
												<td>13.</td>
												<td>INFLUNCER LABS </td>
												
											  </tr>

                                              <tr>          
												<td>14.</td>
												<td>UST GLOBAL  </td>
												
											  </tr>

                                              <tr>          
												<td>15.</td>
												<td>1LATTICE  </td>
												
											  </tr>

                                              <tr>          
												<td>16.</td>
												<td>KEYENCE INDIA PRIVATE LIMITED </td>
												
											  </tr>

                                              <tr>          
												<td>17.</td>
												<td>ANORA LABS </td>
												
											  </tr>

                                              <tr>          
												<td>18.</td>
												<td>RECRUITCRM </td>
												
											  </tr>

                                              <tr>          
												<td>19.</td>
												<td>SIMPLOTEL</td>
												
											  </tr>

                                              <tr>          
												<td>20.</td>
												<td>INFO GAINS  </td>
												
											  </tr>

											  <tr>          
												<td>21.</td>
												<td>ITC INFOTECH  </td>
												
											  </tr>

											  <tr>          
												<td>22.</td>
												<td>FLIPKART  </td>												
											  </tr>

											   <tr>          
												<td>23.</td>
												<td>AVAALI SOLUTIONS  </td>												
											  </tr>

											   <tr>          
												<td>24.</td>
												<td>CSG SYSTEMS INTERNATIONAL PVT LTD  </td>												
											  </tr>

											   <tr>          
												<td>25.</td>
												<td>GENISYS  </td>												
											  </tr>

											   <tr>          
												<td>26.</td>
												<td>SUBEX </td>												
											  </tr>

											   <tr>          
												<td>27.</td>
												<td>L&T TECHNOLOGIES  </td>												
											  </tr>

											   <tr>          
												<td>28.</td>
												<td>EFI </td>												
											  </tr>

											   <tr>          
												<td>29.</td>
												<td>MITSOGO  </td>												
											  </tr>

											   <tr>          
												<td>30.</td>
												<td>MAVERIC  </td>												
											  </tr>

											   <tr>          
												<td>31.</td>
												<td>6D TECHNOLOGIES  </td>												
											  </tr>

											   <tr>          
												<td>32.</td>
												<td>RIGHTNOW TECHNOLOGIES </td>												
											  </tr>

											   <tr>          
												<td>33.</td>
												<td>L.S. DAVAR & CO  </td>												
											  </tr>

											   <tr>          
												<td>34.</td>
												<td>UNACADEMY </td>												
											  </tr>
											
											 </tbody>										  
											  
											  
								 </table>	
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12">         	      
            		<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name Of Company</th>
												
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>35.</td>
												<td>ELEATION  </td>
												
											  </tr>
											  
											   <tr>          
												<td>36.</td>
												<td>STRATOGENT</td>
												
											  </tr>
											  
											   <tr>          
												<td>37.</td>
												<td>GUVI</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>38.</td>
												<td>IBM 	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>39.</td>
												<td>ELFONZE</td>
											
											
											  </tr>
											  
											   <tr>          
												<td>40.</td>
												<td>EMOTORAD	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>41.</td>
												<td>VOLVO</td>
												
												
											  </tr>
											  
											
											  
											  <tr>          
												<td>42.</td>
												<td>BYJUS  </td>
												
											  </tr>

											  <tr>          
												<td>43.</td>
												<td>EMERTXE </td>
												
											  </tr>

											  <tr>          
												<td>44.</td>
												<td>FOUNDIT</td>
												
											  </tr>

											  <tr>          
												<td>45.</td>
												<td>INTRAINZ INNOVATION PVT LTD </td>
												
											  </tr>

											  <tr>          
												<td>46.</td>
												<td>TATA ADVANCE SYSTEM LTD </td>
												
											  </tr>

											  <tr>          
												<td>47.</td>
												<td>APOLLO FACILITY SERVICES </td>
												
											  </tr>

                                              <tr>          
												<td>48.</td>
												<td>TALENT SERVE </td>
												
											  </tr>

                                              <tr>          
												<td>49.</td>
												<td>LUMUS LEARNING </td>
												
											  </tr>

                                              <tr>          
												<td>50.</td>
												<td>EDS TECHNOLOGIES </td>
												
											  </tr>

                                              <tr>          
												<td>51.</td>
												<td>ANAKIN </td>
												
											  </tr>

                                              <tr>          
												<td>52.</td>
												<td>DELTAX </td>
												
											  </tr>

                                              <tr>          
												<td>53.</td>
												<td>CANON </td>
												
											  </tr>

                                              <tr>          
												<td>54.</td>
												<td>L&T EDUTECH  </td>
												
											  </tr>

											  <tr>          
												<td>55.</td>
												<td>DRDO </td>
												
											  </tr>

											  <tr>          
												<td>56.</td>
												<td>UNIVISION </td>												
											  </tr>

											   <tr>          
												<td>57.</td>
												<td>MUSIGMA </td>												
											  </tr>

											   <tr>          
												<td>58.</td>
												<td>LEKHA WIRELESS </td>												
											  </tr>

											   <tr>          
												<td>59.</td>
												<td>SMART CLIFF </td>												
											  </tr>

											   <tr>          
												<td>60.</td>
												<td>M/S ACE DESIGNER </td>												
											  </tr>

											   <tr>          
												<td>61.</td>
												<td>PURPLE DATA INC </td>												
											  </tr>

											   <tr>          
												<td>62.</td>
												<td>TVS ELECTRONICS LTD  </td>												
											  </tr>

											   <tr>          
												<td>63.</td>
												<td>SASKEN TECHNOLOGIES </td>												
											  </tr>

											   <tr>          
												<td>64.</td>
												<td>APPLIED STANDARDS & INNOVATIONS </td>												
											  </tr>

											   <tr>          
												<td>65.</td>
												<td>PRAKASH FERROUS INDUSTRIES </td>												
											  </tr>

											   <tr>          
												<td>66.</td>
												<td>SOLAR EDGE </td>												
											  </tr>

											   <tr>          
												<td>67.</td>
												<td>HEALTH ASYST  </td>												
											  </tr>

											   <tr>          
												<td>68.</td>
												<td>NANDI TOYOTA </td>												
											  </tr>
											
											 </tbody>										  
											  
											  
								 </table>		
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12">         	      
            		<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name Of Company</th>
												
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>69.</td>
												<td>TOYOTA INDUSTRIES ENGINE INDIA PVT. LTD </td>
												
											  </tr>
											  
											   <tr>          
												<td>70.</td>
												<td>NIMESA</td>
												
											  </tr>
											  
											   <tr>          
												<td>71.</td>
												<td>RADIANT TECHNOLOGIES</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>72.</td>
												<td>TRASCCON INTERCONNECTION	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>73.</td>
												<td>TECH MAHINDRA</td>
											
											
											  </tr>
											  
											   <tr>          
												<td>74.</td>
												<td>ARM ENGINEERING SOLUTIONS	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>75.</td>
												<td>SACHA ENGINEERING</td>
												
												
											  </tr>
											  
											
											  
											  <tr>          
												<td>76.</td>
												<td>HELIX PVT LTD </td>
												
											  </tr>

											  <tr>          
												<td>77.</td>
												<td>ZOHO CORP </td>
												
											  </tr>

											  <tr>          
												<td>78.</td>
												<td>HIGH TECHNEXT ENGINEERING& TELECOM PVT TLD</td>
												
											  </tr>

											  <tr>          
												<td>79.</td>
												<td>RADIANT TECH SOLUTIONS</td>
												
											  </tr>

											  <tr>          
												<td>80.</td>
												<td>IMAGE LABELS PVT LTD </td>
												
											  </tr>

											  <tr>          
												<td>81.</td>
												<td>HUDL  </td>
												
											  </tr>

                                              <tr>          
												<td>82.</td>
												<td>NXTWAVE  </td>
												
											  </tr>

                                              <tr>          
												<td>83.</td>
												<td>EASY COM   </td>
												
											  </tr>

                                              <tr>          
												<td>84.</td>
												<td>ELFONZE TECHNOLOGIES </td>
												
											  </tr>

                                              <tr>          
												<td>85.</td>
												<td>SIMPLOTEL  </td>
												
											  </tr>

                                              <tr>          
												<td>86.</td>
												<td>LULU GLOBAL MALL </td>
												
											  </tr>

                                              <tr>          
												<td>87.</td>
												<td>KNS METRO PROPERTIES </td>
												
											  </tr>

                                              <tr>          
												<td>88.</td>
												<td>TIO PVT LTD  </td>
												
											  </tr>

											  <tr>          
												<td>89.</td>
												<td>EXPLORE & EVOLVE PVT LTD  </td>
												
											  </tr>

											  <tr>          
												<td>90.</td>
												<td>ACADEMOR </td>												
											  </tr>

											   <tr>          
												<td>91.</td>
												<td>BOTTOM LINE TECHNOLOGIES </td>												
											  </tr>

											   <tr>          
												<td>92.</td>
												<td>MOTHERSON SUMI </td>												
											  </tr>

											   <tr>          
												<td>93.</td>
												<td>MPSHASIS </td>												
											  </tr>

											   <tr>          
												<td>94.</td>
												<td>ACUVATE </td>												
											  </tr>

											   <tr>          
												<td>95.</td>
												<td>QUGATES TECHNOLOGIES </td>												
											  </tr>

											   <tr>          
												<td>96.</td>
												<td>QUANTUM INNOVATIONS </td>												
											  </tr>

											   <tr>          
												<td>97.</td>
												<td>WIPRO </td>												
											  </tr>

											   <tr>          
												<td>98.</td>
												<td>ULTRA TECH </td>												
											  </tr>

											   <tr>          
												<td>99.</td>
												<td>PRASAD CONSULTANTS  </td>												
											  </tr>

											   <tr>          
												<td>100.</td>
												<td>BLACK OLIVES  </td>												
											  </tr>

											   <tr>          
												<td>101.</td>
												<td>AMBIENT </td>												
											  </tr>

											 <!--  <tr>          
												<td>102.</td>
												<td>Mr. Shivaprakash. K. M  </td>												
											  </tr> -->
											
											 </tbody>										  
											  
											  
								 </table>		
				</div>
		</div>
	</div>
	</div>
</section>
<!-- Companies Visited 2024 end -->

<!-- Companies Visited 2023 -->
<section id="about">
	<div class="container" style="width:100%;">
	<div class="news_area home-4">
		<div class="row">
			<div class="col-md-12">
			<h2 class="titile text-center">Companies Visited for 2023 Batch</h2>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-12">         	      
            		<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name Of Company</th>
												
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>1.</td>
												<td>INFOSYS </td>
												
											  </tr>
											  
											   <tr>          
												<td>2.</td>
												<td>TCS</td>
												
											  </tr>
											  
											   <tr>          
												<td>3.</td>
												<td>TECH MAHINDRA</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>4.</td>
												<td>HCL TECHNOLOGIES</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>5.</td>
												<td>DELL</td>
											
											
											  </tr>
											  
											   <tr>          
												<td>6.</td>
												<td>JUSPAY</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>7.</td>
												<td>MUSIGMA</td>
												
												
											  </tr>
											  
											
											  
											  <tr>          
												<td>8.</td>
												<td>VTIGER </td>
												
											  </tr>

											  <tr>          
												<td>9.</td>
												<td>SONATA SOFTWARE  </td>
												
											  </tr>

											  <tr>          
												<td>10.</td>
												<td>FLIPKART</td>
												
											  </tr>

											  <tr>          
												<td>11.</td>
												<td>SOTI </td>
												
											  </tr>

											  <tr>          
												<td>12.</td>
												<td>SASKEN </td>
												
											  </tr>

											  <tr>          
												<td>13.</td>
												<td>SAP </td>
												
											  </tr>

                                              <tr>          
												<td>14.</td>
												<td>CELSTREAM  </td>
												
											  </tr>

                                              <tr>          
												<td>15.</td>
												<td>KEYENCE </td>
												
											  </tr>

                                              <tr>          
												<td>16.</td>
												<td>LUMOS LEARNING </td>
												
											  </tr>

                                              <tr>          
												<td>17.</td>
												<td>HEXAWARE </td>
												
											  </tr>

                                              <tr>          
												<td>18.</td>
												<td>LEKHA WIRELESS SOLUTIONS </td>
												
											  </tr>

                                              <tr>          
												<td>19.</td>
												<td>RECRUIT CRM</td>
												
											  </tr>

                                              <tr>          
												<td>20.</td>
												<td>GLOBAL AUTOMATION </td>
												
											  </tr>

											  <tr>          
												<td>21.</td>
												<td>DELTA X  </td>
												
											  </tr>

											  <tr>          
												<td>22.</td>
												<td>WORLD OF RIVER </td>												
											  </tr>

											   <tr>          
												<td>23.</td>
												<td>AGILE POINT  </td>												
											  </tr>

											   <tr>          
												<td>24.</td>
												<td>AVAALI SOLUTIONS </td>												
											  </tr>

											   <tr>          
												<td>25.</td>
												<td>NTT DATA </td>												
											  </tr>

											   <tr>          
												<td>26.</td>
												<td>NSOFT INDIA</td>												
											  </tr>

											   <tr>          
												<td>27.</td>
												<td>CONTLO  </td>												
											  </tr>

											   <tr>          
												<td>28.</td>
												<td>6D TECHNOLOGIES</td>												
											  </tr>

											   <tr>          
												<td>29.</td>
												<td>VIT INFOTECH</td>												
											  </tr>

											   <tr>          
												<td>30.</td>
												<td>STAVTAR </td>												
											  </tr>

											   <tr>          
												<td>31.</td>
												<td>QUEST GLOBAL </td>												
											  </tr>

											   <tr>          
												<td>32.</td>
												<td>PRAXIS GLOBAL </td>												
											  </tr>

											   <tr>          
												<td>33.</td>
												<td>DIAGONAL.AI </td>												
											  </tr>

											   <tr>          
												<td>34.</td>
												<td>ACCOLITE DIGITAL </td>												
											  </tr>

											  <tr>          
												<td>35.</td>
												<td>HIGH PEAK SOFTWARE </td>												
											  </tr>

											  <tr>          
												<td>36.</td>
												<td>RECKONSYS</td>												
											  </tr>

											  <tr>          
												<td>37.</td>
												<td>LIFERAY (INTERNSHIP)</td>												
											  </tr>

											  <tr>          
												<td>38.</td>
												<td>ACADEMOR </td>												
											  </tr>

											  <tr>          
												<td>39.</td>
												<td>CUVETTE </td>												
											  </tr>
											
											 </tbody>										  
											  
											  
								 </table>	
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12">         	      
            		<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name Of Company</th>
												
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>40.</td>
												<td>LEKHA WIRELESS SOLUTIONS  </td>
												
											  </tr>
											  
											   <tr>          
												<td>41.</td>
												<td>PARK CONTROLS & COMMUNICATIONS</td>
												
											  </tr>
											  
											   <tr>          
												<td>42.</td>
												<td>INDO MIM</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>43.</td>
												<td>SACUMEN	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>44.</td>
												<td>COMPRO TECHNOLOGIES</td>
											
											
											  </tr>
											  
											   <tr>          
												<td>45.</td>
												<td>INFRA INDIA PVT LTD	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>46.</td>
												<td>AQUERA LABS </td>
												
												
											  </tr>
											  
											
											  
											  <tr>          
												<td>47.</td>
												<td>PEOL TECHNOLOGIES  </td>
												
											  </tr>

											  <tr>          
												<td>48.</td>
												<td>TANGENT LEARNING SOLUTIONS</td>
												
											  </tr>

											  <tr>          
												<td>49.</td>
												<td>INTELLIPAAT</td>
												
											  </tr>

											  <tr>          
												<td>50.</td>
												<td>JARO EDUCATION  </td>
												
											  </tr>

											  <tr>          
												<td>51.</td>
												<td>PROGARD </td>
												
											  </tr>

											  <tr>          
												<td>52.</td>
												<td>MAVERIC </td>
												
											  </tr>

                                              <tr>          
												<td>53.</td>
												<td>TECH MAHINDRA(FOR PWD) </td>
												
											  </tr>

                                              <tr>          
												<td>54.</td>
												<td>EMERTEX </td>
												
											  </tr>

                                              <tr>          
												<td>55.</td>
												<td>UPNEXT  </td>
												
											  </tr>

                                              <tr>          
												<td>56.</td>
												<td>THENCE </td>
												
											  </tr>

                                              <tr>          
												<td>57.</td>
												<td>SAINT GLOBAL</td>
												
											  </tr>

                                              <tr>          
												<td>58.</td>
												<td>NIMESA</td>
												
											  </tr>

                                              <tr>          
												<td>59.</td>
												<td>PARAM INNOVATIONS </td>
												
											  </tr>

											  <tr>          
												<td>60.</td>
												<td>TRASCCON INTERCONNECTION </td>
												
											  </tr>

											  <tr>          
												<td>61.</td>
												<td>TOYOTA INDUSTRIES ENGINE </td>												
											  </tr>

											   <tr>          
												<td>62.</td>
												<td>GHAR OFFICE</td>												
											  </tr>

											   <tr>          
												<td>63.</td>
												<td>PROMAN </td>												
											  </tr>

											   <tr>          
												<td>64.</td>
												<td>NEXT FIRST </td>												
											  </tr>

											   <tr>          
												<td>65.</td>
												<td>AUYS </td>												
											  </tr>

											   <tr>          
												<td>66.</td>
												<td>VISTAS LEARNING</td>												
											  </tr>

											   <tr>          
												<td>67.</td>
												<td>STRATOGENT  </td>												
											  </tr>

											   <tr>          
												<td>68.</td>
												<td>BYJUS </td>												
											  </tr>

											   <tr>          
												<td>69.</td>
												<td>SIMPLOTEL </td>												
											  </tr>

											   <tr>          
												<td>70.</td>
												<td>QUESS CORP</td>												
											  </tr>

											   <tr>          
												<td>71.</td>
												<td>NANDI TOYOTA MOTOR WORLD  </td>												
											  </tr>

											   <tr>          
												<td>72.</td>
												<td>CODEYOUNG  </td>												
											  </tr>

											   <tr>          
												<td>73.</td>
												<td>COURSE 5I </td>												
											  </tr>

											  <tr>          
												<td>74.</td>
												<td>FLIPKART</td>												
											  </tr>

											  <tr>          
												<td>75.</td>
												<td>TAYANA MOBILITY TECHNOLOGIES </td>												
											  </tr>

											  <tr>          
												<td>76.</td>
												<td>EMERTXE </td>												
											  </tr>

											  <tr>          
												<td>77.</td>
												<td>PROMAC</td>												
											  </tr>

											  <tr>          
												<td>78.</td>
												<td>TESCO </td>												
											  </tr>
											
											 </tbody>										  
											  
											  
								 </table>		
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12">         	      
            		<table class="table table-striped course_table">
											<thead>
											  <tr>          
												<th>SL.No. </th>
												<th>Name Of Company</th>
												
											  </tr>
											</thead>
											
											<tbody>
											
											  <tr>          
												<td>79.</td>
												<td>ANANDRATHI </td>
												
											  </tr>
											  
											   <tr>          
												<td>80.</td>
												<td>SUBEX</td>
												
											  </tr>
											  
											   <tr>          
												<td>81.</td>
												<td>6D TECHNOLOGIES</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>82.</td>
												<td>EFI	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>83.</td>
												<td>IMMENSPHERE</td>
											
											
											  </tr>
											  
											   <tr>          
												<td>84.</td>
												<td>TRACKOWORLD	</td>
												
												
											  </tr>
											  
											   <tr>          
												<td>85.</td>
												<td>BHIVE ALTS</td>
												
												
											  </tr>
											  
											
											  
											  <tr>          
												<td>86.</td>
												<td>VITESCO </td>
												
											  </tr>

											  <tr>          
												<td>87.</td>
												<td>KOCH(INTERNSHIP) </td>
												
											  </tr>

											  <tr>          
												<td>88.</td>
												<td>ZEROFOX</td>
												
											  </tr>

											  <tr>          
												<td>89.</td>
												<td>EID PARRY INDIA LIMITED</td>
												
											  </tr>

											  <tr>          
												<td>90.</td>
												<td>RENEVERSE </td>
												
											  </tr>

											  <tr>          
												<td>91.</td>
												<td>24/7AI </td>
												
											  </tr>

                                              <tr>          
												<td>92.</td>
												<td>KGIS </td>
												
											  </tr>

                                              <tr>          
												<td>93.</td>
												<td>VOLVO GROUP INDIA  </td>
												
											  </tr>

                                              <tr>          
												<td>94.</td>
												<td>PURPLE DATA TECHNOLOGIES</td>
												
											  </tr>

                                              <tr>          
												<td>95.</td>
												<td>PENTAGON SPACE </td>
												
											  </tr>

                                              <tr>          
												<td>96.</td>
												<td>NECTAR</td>
												
											  </tr>

                                              <tr>          
												<td>97.</td>
												<td>INFLUENCER LABS </td>
												
											  </tr>

                                              <tr>          
												<td>98.</td>
												<td>INFOSYS  </td>
												
											  </tr>

											  <tr>          
												<td>99.</td>
												<td>SECURITY SHELLS   </td>
												
											  </tr>

											  <tr>          
												<td>100.</td>
												<td>SLN TECHNOLOGIES </td>												
											  </tr>

											   <tr>          
												<td>101.</td>
												<td>PROMAN INFRASTRUCTURE </td>												
											  </tr>

											   <tr>          
												<td>102.</td>
												<td>MOTHER SON SUMI</td>												
											  </tr>

											   <tr>          
												<td>103.</td>
												<td>WHATSTOOL </td>												
											  </tr>

											   <tr>          
												<td>104.</td>
												<td>PLANET SPARK</td>												
											  </tr>

											   <tr>          
												<td>105.</td>
												<td>DELTA X </td>												
											  </tr>

											   <tr>          
												<td>106.</td>
												<td>JOSHMITHRA </td>												
											  </tr>

											   <tr>          
												<td>107.</td>
												<td>AFICIONADO TECHNOLOGIES </td>												
											  </tr>

											   <tr>          
												<td>108.</td>
												<td>UNITED BREWERIES</td>												
											  </tr>

											   <tr>          
												<td>109.</td>
												<td>WELLDOG  </td>												
											  </tr>

											   <tr>          
												<td>110.</td>
												<td>DIAGNOSYS ELECTRONICS </td>												
											  </tr>

											   <tr>          
												<td>111.</td>
												<td>RELANTO GLOBAL PVT LTD </td>												
											  </tr>

											   <tr>          
												<td>112.</td>
												<td>PREBO AUTOMOTIVE  </td>												
											  </tr> 

											   <tr>          
												<td>113.</td>
												<td>NEXUS VENTURE PARTERNS </td>												
											  </tr> 

											   <tr>          
												<td>114.</td>
												<td>GEEKS FOR GEEKS  </td>												
											  </tr> 

											   <tr>          
												<td>115.</td>
												<td>HP-(DISABILITY) </td>												
											  </tr> 

											   <tr>          
												<td>116.</td>
												<td>TCS NQT-NATIONAL QUALIFER TEST  </td>												
											  </tr> 

											   <tr>          
												<td>117.</td>
												<td>QUEST GLOBAL-INGENIUM 2023</td>												
											  </tr> 

											   											  											
											 </tbody>										  
											  
											  
								 </table>		
				</div>
		</div>
	</div>
	</div>
</section>
<!-- Companies Visited 2023 end -->
 	
	<!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12 padding">
			            <div class="vertical-tab" role="tabpanel">
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
			                    <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab">Vision and Mission</a></li>
			                    <li role="presentation"><a href="#goals" aria-controls="goals" role="tab" data-toggle="tab">Goals</a></li>
			                <!--    <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>-->
							 <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Placement Drives</a></li>
								<li role="presentation"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab">Placement Statistics</a></li>
								<li role="presentation"><a href="#highpackage" aria-controls="highpackage" role="tab" data-toggle="tab">Students with Highest Packages</a></li>
			                    <li role="presentation"><a href="#faculty" aria-controls="faculty" role="tab" data-toggle="tab" class="dept-cart">Faculty</a></li>
			                    
			                <!--    <li role="placed_students"><a href="#placed_students" aria-controls="placed_students" role="tab" data-toggle="tab">Placed Students</a></li>
								<li role="placement_statistics"><a href="#placement_statistics" aria-controls="placement_statistics" role="tab" data-toggle="tab">Placement Statistics</a></li> -->
								<li role="training_programs"><a href="#training_programs" aria-controls="training_programs" role="tab" data-toggle="tab">Training Programs</a></li>
								<!--<li role="internship"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab">Internship</a></li> -->
								<!--<li role="companies"><a href="#companies" aria-controls="companies" role="tab" data-toggle="tab">Companies</a></li>-->
							<!--<li role="placement_events"><a href="#placement_events" aria-controls="placement_events" role="tab" data-toggle="tab">Events</a></li> -->
							<li role="yearwise_placement"><a href="#yearwise_placement" aria-controls="yearwise_placement" role="tab" data-toggle="tab">Yearwise Placements</a></li>
								<!--<li role="higher_studiesGRE"><a href="#higher_studiesGRE" aria-controls="yearwise_placement" role="tab" data-toggle="tab">Higher Studies / GRE</a></li> -->

			                    <li role="presentation"><a href="#contacts" aria-controls="contacts" role="tab" data-toggle="tab">Contact</a></li>
			                    
			                </ul>
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                
			                	<!-- profile -->
			                	<div role="tabpanel" class="tab-pane fade in active" id="profile">
                             <div class="aboutus_area wow fadeInLeft">
			                        <h3>Profile</h3>
			                        <p>The Department of placement & training was established as a separate entity in the year 2004.
			                         The Department started its activities in a humble way in a small one room accommodation, to cater 
			                         to the needs of providing high quality placement services to the students of the Institution.
			                          In the year of inception, the department modestly established contact with about 30 organizations
			                           and placed 56 students. With every passing year, the number of recruiting organizations has increased
			                            in geometric progression. Many renowned organization visiting the campus annually and conduct 
			                            their recruitment process on our campus.</p>

										<p>The Department has set up all the required facilities for the conduction of the recruitment 
										process such as seminar hall for company presentations, group discussion rooms and conferencing
										 facilities, interview and Discussion rooms. The independent placement & training cell in the 
										 first floor of KSIT has been built with a view to provide the complete autonomy and convenience
										  for executives to conduct fair and transparent talent search to recruit good quality engineers
										   for positions in organizations.</p>
										
										<p>The progress of the recruitment programs can be budged by the increasing total number of offers 
										made each year. The trend of the placements in the institution is indicated below .The Department
										 presently coordinates with more than 100 leading organizations which recruits trainee engineers
										  for entry level positions in their respective organizations.</p>
									
									<h3>The following major functions are performed by the Department of Placement & Training:</h3>	
									
									<ul class="list">
										<li>Job opportunities to students through On-Campus, Off-Campus, Centralized Campus, State level Placement Programme and Job-fairs etc.</li>
										<li>Organize pre-placement training to all the Graduate students in their pre-final year. For this, top professional agencies are angaged to train the students through out the year, in having their technical, analytical and soft skills.</li>
										<li>Placement Office also co-ordinates and helps the students to get project work , arranging in-plant training, industrial visits, projects, guest lecturers and other industry institute activity.</li>
										<li>With the assistance of faculty members and student co-coordinators organize the logistic support to the companies who visit the College for placements by assisting them in conducting of Presentation, Testing, Group Discussions and Interviews.</li>
										<li>Assist the Companies like INFOSYS to organize Campus Connect Programmes and Workshops for the benefit of the students.</li>
										<li>Career Awareness Sessions.</li>
										<li>Guidance session for higher studies.</li>
									
									</ul>
						<h3>Head Of The Department Desk</h3>
                  <div class="aboutus_area wow fadeInLeft">
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <img class="hod-img img-responsive" src="${img_path!}/placements/drharishtpo.jpg" alt="image" />	
                         <div class="qualification_info">
							                        <h3 class="qual-text">Dr. Harish R</h3>
							                        <h4 class="qual-text">Training & Placement Officer</h4>
							                        <h3 class="qual-text">MBA, PGDHRM, Ph.D</h3>
						  </div>

                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>The world is evolving at a very fast pace. Current scenario demands the ability to learn, unlearn and relearn things. It has always remained a challenge for academia to produce industry ready people.</p>
                        <p>KSIT, our mission is to produce professionally competent engineers by providing value-based and quality education to students and to make them adaptable to ever changing demands and requirements of the world.</p>
                        <p>Our institute holds the pride of place being pioneers in the field of engineering and technical education. We have been continuously ranked among the elite by our peers and our constant pursuit of excellence has made our institute a focal point in technical education for students and faculty members alike.</p>
                        
                       
                     </div>
                     
                     <div class="col-lg-12 col-md-12 col-sm-12">
					 	<p>The primary objective of the Training and Placement cell are to provide technical training to give hands on experience of the actual work environment which helps the candidates to identify and inculcate the necessary soft skill.</p>
                     	<p>The Training and Placement cell conducts training sessions throughout the year, organizes seminars and workshops on various current topics, provide exposure to latest personality development modules, industrial visits, orientation program personal counseling by the professionals, industry-institute interactions, etc. which has resulted into dramatic placements in the reputed companies across the country </p>    
                     	<p>We encourage all private and government sector companies to participate in the recruitment process and we are sure that the students recruited will play pivotal role in the growth of the respective organizations.</p>
                        <p>The Training and Placement cell has set high goals for itself i.e if you can dream it, you can do it.</p>                
                        
                     </div>
				 </div>					
									  
									
			                      </div>
			                    </div>
			                    
			                    <!-- profile -->
			                	
			                	
			                
			                    
			                    <!-- vision and mission -->
			                    <div role="tabpanel" class="tab-pane fade in" id="vision">
			                      <div>
			                           <div class="aboutus_area wow fadeInLeft">
		  
										 <h3>Vision</h3>
										 
											 <ul class="list">
											    <li>Sustained Excellence in Employability </li>
											 </ul>
											 
											 
											<h3>Mission</h3>
										 
											 <ul class="list">
											    <li>Empower students with life - long career decision - making skills and job search strategies.</li>
											    <li>Imparting technical and soft skills for Industry readiness.</li>
											    <li>Imparting social and ethical values to be a good citizen.  </li>
											 </ul>

											 <p>The placement department of K. S. Institute of Technology Bangalore is headed by Harish R,
			                         Training and Placement Officer. The academic excellence at K. S. Institute of Technology is
			                          reflected in its students exemplary record in placements in the corporate and engineering sectors.
			                           The department of Training & Placement is the backbone of any institute. KSIT has consistently
			                            maintained an excellent recruitment record. The graduates of KSIT have been recruited by some 
			                            of the worlds leading corporate, besides leading Indian companies.</p>

										<p>Training for B.E. students is an integral part of the curriculum and in-built into the programs 
										of study. With the clear-cut objective of placing all meritorious students before completing
										 their course, the full-fledge Placement Division goes about its work with missionary zeal.
										  The sheer diversity of Indian economy and society, whose different shades are represented
										   among the students and faculty, prepares the students to work in global multicultural
										    corporations / industries.</p>
										
										<p>To assist the development of graduates with balanced set of technical skills,interpersonal
										 skills and with a positive attitude to life.</p>
										<p>To act as a nodal agency in the Institution for forging technology alliance between the 
										Departments of KSIT and the Industries.</p>
										<p>To act as a seamless conduit between the Industry and the Institute and provide quality 
										technical manpower to suit every organizational need.</p>
											
									  </div>
								 </div>
			                    </div>

			                    <!-- vision and mission -->
			                    
			                    <div role="tabpanel" class="tab-pane fade" id="goals">
			                    	<div>
				                        <h3>Goals of the placement Department</h3>
				                        <ul class="list">
											<li>Centre for Competency Mapping</li>
											<li>Centre for Excellence</li>
											<li>Centre for Learning and Development</li>
											<li>Entrepreneurship Development Cell</li>
											<li>Accreditation with top IT companies like Wipro, Tata Consultancy Services and Cognizant Technologies Solutions.</li>
										</ul>
			                        </div>
			                    </div>


								<!-- Placements Statistics -->

								 <div role="tabpanel" class="tab-pane fade" id="statistics">
			                    	<div class="container-fluid">
	        			<div class="col-lg-6 col-md-6 col-sm-6">

						<div class=""><img class="img-responsive" src="${img_path!}/placements/placementyearwise.jpg" alt="image" /></div>

		          		<!--	<div class="col-lg-4 col-md-4 col-sm-4">
	            				<div class=""><img class="img-responsive" src="${img_path!}/placements/highestpackage/hp_1.jpg" alt="image" /></div>
	            				
	            			</div>
		          			<div class="col-lg-4 col-md-4 col-sm-4">
		            			<div class=""><img class="img-responsive" src="${img_path!}/placements/highestpackage/hp_2.jpg" alt="image" /></div>
		            			
		          			</div>
		          			<div class="col-lg-4 col-md-4 col-sm-4">
		            			<div class=""> <img class="img-responsive" src="${img_path!}/placements/highestpackage/hp_3.jpg" alt="image" /> </div>
		            					            			
		          			</div>-->
							
		            	</div>
		            	
		          		
		          		
      			</div>
      			
			                    </div>

								<!-- Placement Statistics -->

								<!-- Students with High Package -->

								 <div role="tabpanel" class="tab-pane fade" id="highpackage">
			                    	<div class="container-fluid">
	        			<div class="col-lg-12 col-md-12 col-sm-12">
		          			<div class="col-lg-4 col-md-4 col-sm-4">
	            				<div class=""><img class="img-responsive" src="${img_path!}/placements/highestpackage/hp_1.jpg" alt="image" /></div>
	            				
	            			</div>
		          			<div class="col-lg-4 col-md-4 col-sm-4">
		            			<div class=""><img class="img-responsive" src="${img_path!}/placements/highestpackage/hp_2.jpg" alt="image" /></div>
		            			
		          			</div>
		          			<div class="col-lg-4 col-md-4 col-sm-4">
		            			<div class=""> <img class="img-responsive" src="${img_path!}/placements/highestpackage/hp_3.jpg" alt="image" /> </div>
		            					            			
		          			</div>
							
		            	</div>
		            	
		          		
		          		
      			</div>
      			
			                    </div>

								<!-- Students with High Package -->
			                   

						    <!-- Events -->
						             <!--     <div role="tabpanel" class="tab-pane fade" id="events">
						                    <div>
						                    <h3>Events</h3>
						                    <#list placementEventsList[0..*3] as placementEvent>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${placementEvent.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h1>${placementEvent.heading!}</h1>	                      	 
							                       	 <p>"${placementEvent.content!}"</p>
							                       	 <p><span class="events-feed-date">${placementEvent.eventDate!}  </span></p>
							                       	 <a class="btn btn-primary" href="placement_events.html">View more</a>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>   
						  						
						                    </div>    
						                  </div> -->
						                  <!-- Events  -->

										   <!-- placementdrive -->
			                  <div role="tabpanel" class="col-12 tab-pane fade" id="events">
			                    <div class="row">
				                    <h3>Placement Drives</h3>
	            					<table border="1">
	            						<tr>
	            							<th>Drive</th>
	            							<th>Type</th>
	            							<th>Date</th>	
	            						</tr>
	            						<#list placementDriveList as placementDrive>
	            							<tr>
	            								<td>${placementDrive.heading!}
	            								</td>
	            								<td>${placementDrive.content!}
	            								</td>
	            								<td>${placementDrive.eventDate!}
	            								</td>
	            						</#list>
									</table>			  						
			                    </div>    
			                  </div>
							<!-- placement drive  -->

			
										   <!-- Year Wise placements -->
						                  <div role="tabpanel" class="tab-pane fade" id="yearwise_placement">
						                    <div>
						                    <h3>Yearwise Placement Details</h3>
						                     <div clas="row">
			  	<#list yearSet as year>
			  		 <#setting number_format="0" />
			  		<h3 class="text-center">Placement Data for year ${year?c!}</h3>
            <hr/>
				  	<table class="table table-striped course_table">
						<thead>
							<tr>          
								<th>SL.NO. </th>
								<th>Company</th>
								<th>TOTAL STUDENTS PLACED</th>											
							</tr>
						</thead>
						<tbody>
							<#assign count = 1>
							
							<#list placementCompanyList as placementCompany>
								<#if placementCompany.year == year>
								
									<tr>
										<td>${count!}</td>
										<td>${placementCompany.company!}</td>
										<td>${placementCompany.noOfStudents!}</td>
										<#assign count = count + 1>
									</tr>
								</#if>
							</#list>
						</tbody>
					</table>
			  	</#list>
				
			</div>  
						  						
						                    </div>    
						                  </div>
						                  <!-- year wise placements  -->
						                  
						                    <!--Faculty  -->
               <div role="tabpanel" class="tab-pane fade" id="faculty">
                  <div>
                     <h3>Faculty</h3>
                     <!--Teaching faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list facultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name!}</p>
                                 <p>${faculty.designation!}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id!}">View More</button>                                 
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal fade modal-profile" id="profile${faculty.id!}" role="dialog">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name!}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Resume</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                     </div>
                     <!--Teaching faculty-->
                     <h3>Supporting Faculty</h3>
                     <!--supporting faculty-->
                     <div class="col-md-12 col-lg-12 col-sm-12">
                        <!-- new card -->
                        <#list supportingStaffFacultyList as faculty>
                        <div class="col-md-6 col-sm-6">
                           <div class="faculty">
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <img class="faculty-img" src="${faculty.imageURL!}" alt="${faculty.name}" />
                              </div>
                              <div class="col-md-6 col-sm-12 faculty-content">
                                 <p>${faculty.name}</p>
                                 <p>${faculty.designation}</p>
                                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#profile${faculty.id}">view profile</button>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        <!-- Modal -->
                        <div class="modal modal-profile fade in" id="profile${faculty.id}" role="dialog" data-backdrop="static">
                           <div class="modal-dialog modal-md">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">${faculty.name!} Profile</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="col-md-6 col-sm-12">
                                       <img class="modal-img" src="${faculty.imageURL!}" alt="${faculty.name}" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <p><strong> Name:</strong> ${faculty.name!}</p>
                                       <p><strong> Designation:</strong> ${faculty.designation!}</p>
                                       <p><strong> Qualification:</strong> ${faculty.qualification!}</p>
                                       <p><strong> Department:</strong> ${faculty.department!}</p>
                                       <#if faculty.profileURL??>
                                       <p><a href="${faculty.profileURL!}" target="_blank">View Resume</a></p>
                                       </#if>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- faculty modal -->
                        </#list>
                        <!-- new card -->
                        <!--supporting faculty-->
                     </div>
                  </div>
               </div>
               <!--Faculty  -->
               
                              <!--contacts  -->
							     <div role="tabpanel" class="tab-pane fade" id="contacts">
			                      <div>
									<h3>Contact</h3>
										<p>Dr. Harish R</p>
										
										<p>Training and Placement officer</p>
										
										<p>Mobile: 8496849656, 9845848331 </p>
										
										
										<p>placement@ksit.edu.in</p>
										<p>placementcell@ksit.edu.in</p>
										
										<p>www.ksit.ac.in</p>
			                      </div>
			                    </div>
               				<!--contacts  -->
               				
               				<!--training_programs  -->
							     <div role="tabpanel" class="tab-pane fade" id="training_programs">
			                      <div>
									<h3 class="text-center">Training Programs Organized by Placement Division</h3>
									 <blockquote>
				                      <span class="fa fa-quote-left"></span>
				                      <p>We believe that to be leaders in the real world, students must develop the intellectual depth & breadth to handle the challenges of increasing complexity, globalization and rapid change. The Student Development Program focuses on every aspect of student personality, helping them develop interpersonal, technical and business skills.</p>
				                    </blockquote>
				                    
									<#list placementTrainingList as placementTraining>
										<div class="col-lg-6 col-md-12 col-sm-12">
											<div class="single_course wow fadeInUp placement_training">
												<div class="singCourse_content">
													<h3 class="singCourse_title"><span>Program: </span>	${placementTraining.program!}</h3>
													<h3 class="singCourse_title"><span>Semester: </span> ${placementTraining.semester!}</h3>
													<h3 class="singCourse_title"><span>Year: </span> ${placementTraining.year?c!}</h3>
													<h3 class="singCourse_title"><span>Company: </span>	${placementTraining.company!}</h3>
													<h3 class="singCourse_title"><span>Schedule: </span> ${placementTraining.schedule!} </h3>
												</div>   
											</div>
										</div>
									</#list>
									
									<h3>Interpersonal Skill Development</h3>
									<p>Creativity, lateral thinking and communication people management skills are essential components for progress in any sphere. Students are encouraged to develop these through goal setting exercises, group discussions, mock interviews and presentations.</p>
									
									<h3>Technical Skill Development</h3>
									<p>The depth of technical skills that students develop, depend to a great extent on the course they have chosen. However, all students are given a conceptual grounding in core skills and application orientation through real time projects to ensure their skills are concurrent with market needs.</p>
									
									<h3>Faculty development programme</h3>
									<p>Our faculty is attending Wipro Mission 10X faculty development programme organized by Wipro Technologies. </p>
				                    
			                      </div>
			                    </div>
               				<!--training_programs  -->			
               				
               				<!--internship  -->
							     <div role="tabpanel" class="tab-pane fade" id="internship">
			                      <div>
									<h3 class="text-center">Internship Details</h3>
									  <blockquote>
				                      <span class="fa fa-quote-left"></span>
				                    </blockquote>
					                    
										
									<!-- start single course -->
									<#list placementInternshipList as internship>
						                <div class="col-lg-6 col-md-12 col-sm-12">
						                  <div class="single_course wow fadeInUp placement_training">
						                    
						                    <div class="singCourse_content">
											
						                    <h3 class="singCourse_title"><span>Program: </span>	${internship.program!}</h3>
											<h3 class="singCourse_title"><span>Semester: </span> ${internship.semester!}</h3>
											<h3 class="singCourse_title"><span>Year: </span> ${internship.year?c!}</h3>
											<h3 class="singCourse_title"><span>Company: </span>	${internship.company!}</h3>
											<h3 class="singCourse_title"><span>Schedule: </span> ${internship.schedule!} </h3>
											
						                    
						                    </div>
						                   
						                  </div>
						                </div>
					                </#list>
				                    
			                      </div>
			                    </div>
               				<!--training_programs  -->			
               				
               				<!--companies  -->
							     <div role="tabpanel" class="tab-pane fade" id="companies">
			                      <div>
									<h3 class="text-center">Recruitment Partners</h3>
				                    
									<#list placementCompanyList as placementCompany>
						                <div class="col-lg-6 col-md-12 col-sm-12">
						                
						                  <div class="single_course wow fadeInUp single_course_placement">
						                    
						                    <div class="singCourse_content">
											
						                    <h3 class="singCourse_title"><span>Company : </span>${placementCompany.company!}</h3>
											<h3 class="singCourse_title"><img src="${placementCompany.imageURL!}" alt="img" /></h3>
											<h3 class="singCourse_title"><span>Salary Package : </span>${placementCompany.salary!}</h3>
											<h3 class="singCourse_title"><span>Number of Students placed : </span>${placementCompany.noOfStudents!}</h3>
											                    
						                    </div>
						                   
						                  </div>
						                </div>
				                 	</#list>
				                    
			                      </div>
			                    </div>
               				<!--companies  -->			
               				
               				<!--companies  -->
							     <div role="tabpanel" class="tab-pane fade" id="placement_events">
			                      <div>
			                      
									<h3 class="text-center">Placement Events</h3>
										  <blockquote>
					                      <span class="fa fa-quote-left"></span>
					                    </blockquote>
					
									 <!-- start single course -->
					                <div class="col-lg-12 col-md-12 col-sm-12">
					                
					                 <#list placementEventsList as placementEvent>
					                	
					                  <div class="single_course wow fadeInUp">
					                    
					                    <div class="singCourse_content">
										
											<h2 class="singCourse_title">
											
												"${placementEvent.heading!}"
											
											</h2>
										
											<p>"${placementEvent.content!}"</p>
											
											<img class="placement_images" src="${placementEvent.imageURL!}" alt="image" style="height:200px;width:200px;" />
											
											<!--<a href="${placementEvent.imageURL!}" target="_blank">click here to download</a>-->
											
											<p>"${placementEvent.eventDate!}"</p>
										
					                    </div>
					                   
					                  </div>
					                  
					                  </#list>
					                  
					                </div>
				                    
			                      </div>
			                    </div>
               				<!--companies  -->			
			
			
			               
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->



</@page>