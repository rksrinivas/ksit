<@page>
	
	 <!--=========== BEGIN STUDENTS placement SECTION ================-->
	<section id="home_slider">
   	<div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<#--  <div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>  -->
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                     	<#--  <#if ecePageSliderImageList?has_content>
                     		<#list ecePageSliderImageList as ecePageSliderImage>
                     			<#if ecePageSliderImage?index == 0>
                     				<article class="item active">
                     			<#else>
                     				<article class="item">
                     			</#if>
	                           		<img class="animated slideInLeft" src="${ecePageSliderImage.imageURL}" alt="">
									<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Hello there</p>
										<p class="slideInRight">Hello there</p>
									</div>                           
	                        	</article>
                     		</#list>
                     	<#else>  -->
	                     	<article class="item active">
	                           <img class="animated slideInLeft" src="${img_path!}/alumni/Slide1.JPG" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">KSIT Alumni Association</p>
										<p class="slideInRight">We delightfully welcome all KSIT students</p>
								</div>
	                        </article>
							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide1.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">From your time as a student</p>
										<p class="slideInRight">To your graduate experience as an alumni</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide2.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Alumni Re-unions are not just about looking back</p>
										<p class="slideInRight">Where past meets present of shared experiences</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide3.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Memories thrive and friendship revive</p>
										<p class="slideInRight">In every handshake a chapter of shared history unfolds</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide4.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">From then to now, together again</p>
										<p class="slideInRight">A story waiting to be retold</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide5.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Time pauses to reflect on the beauty of experiences</p>
										<p class="slideInRight">Alumini meets are the most vibrant threads</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide6.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Weaving tales of friendship and growth</p>
										<p class="slideInRight">Beauty of familiarity meets the thrill of unexpected</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide7.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Alumimi meet lies in the enduring bonds of friendship</p>
										<p class="slideInRight">We rewrite the stories of our lives together</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide8.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Where past is celebrated and the future is envisioned</p>
										<p class="slideInRight">Every hello feels like coming home</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide9.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">We find true essence of alumini meet</p>
										<p class="slideInRight">We are building bridges to the future</p>
								</div>										 
	                        </article>

							<article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/alumnislide10.jpg" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Where bonds strengthen and stories deepen</p>
										<p class="slideInRight">Every glance sparks thousand memories</p>
								</div>										 
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated slideInRight" src="${img_path!}/alumni/Slide2.JPG" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Find the magic of alumini reunion</p>
										<p class="slideInRight">Every smiles tells story,every hug speaks volumes</p> 
								</div>
	                        </article>
	                        
	                        <article class="item">
	                           <img class="animated bounceIn" src="${img_path!}/alumni/Slide3.JPG" alt="img"/>
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">Create new memories to cherish</p>
										<p class="slideInRight">Past whishpers,present laughs,future awaits</p>
								</div>										 
	                        </article>
	                        
	                        <#--  <article class="item">
	                           <img class="animated rollIn" src="${img_path!}/alumni/alumni1.jpg" alt="img">
							   	<div class="carousel-caption" style="background:none;">
										<p class="slideInLeft">CHIRANTHANA with a tagline of EVERLASTING FOREVER</p>
										<p class="slideInRight">We delightfully welcome all KSIT students</p>
								</div> 
	                        </article>  -->
						<#--  </#if>  -->
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
						<li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li> 
						<li data-target="#myCarousel" data-slide-to="6"></li>
                        <li data-target="#myCarousel" data-slide-to="7"></li>
						<li data-target="#myCarousel" data-slide-to="8"></li>
                        <li data-target="#myCarousel" data-slide-to="9"></li>
                        <li data-target="#myCarousel" data-slide-to="10"></li> 
						<li data-target="#myCarousel" data-slide-to="11"></li>
                        <li data-target="#myCarousel" data-slide-to="12"></li> 
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>



    <#--  <section id="about_slider" style="opacity:0.99;">
      <div class="container">
      

        <!-- Start Our courses content -->
        <#--  <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">  -->
		  
			<!--<marquee>-->
            <#--  <div class="about_slider_content">  
			
              <div class="row">
			  
				 <div id="myCarousel" class="carousel slide"  style="opacity:0.999;">
					<div class="carousel-inner">
						<article class="item active">
								
								
								<img src="${img_path!}/alumni/alumni2.jpg" alt="">  -->
								<!--<img src="http://placehold.it/1200x440/4466e4/ffffff">-->
						      <!--<div class="carousel-caption">
							
							<p><a class="btn btn-info btn-sm">Read More</a></p>
							</div>-->
							<#--  </article>
							<article class="item">
						
										<img src="${img_path!}/alumni/alumni2.jpg" alt="">  -->
										<!--<img src="http://placehold.it/1200x440/00cc00/cccccc">-->
											<!--<div class="carousel-caption">
											<h3>Headline</h3>
											<p>content</p>
							
											</div>-->
							<#--  </article>
							<article class="item">
							
									<img src="${img_path!}/alumni/alumni2.jpg" alt="">  -->
									<!--<img src="http://placehold.it/1200x440/dddddd/333333">-->
							  <!--<div class="carousel-caption">
								<h3>Headline</h3>
								<p>content</p>
									
								</div>-->
							<#--  </article>
							
							<article class="item">
						
										<img src="${img_path!}/alumni/alumni2.jpg" alt="">  -->
										<!--<img src="http://placehold.it/1200x440/00cc00/cccccc">-->
											<!--<div class="carousel-caption">
											<h3>Headline</h3>
											<p>content</p>
							
											</div>-->
							<#--  </article>
						</div>  -->
								<!-- Indicators -->
									<!--<ol class="carousel-indicators">
											<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
											<li data-target="#myCarousel" data-slide-to="1"></li>
											<li data-target="#myCarousel" data-slide-to="2"></li>
									</ol>-->
										<!-- Controls -->
									<#--  <div class="carousel-controls">
											<a class="carousel-control left" href="#myCarousel" data-slide="prev">
											<span class="glyphicon glyphicon-backward"></span>

												</a>
											<a class="carousel-control right" href="#myCarousel" data-slide="next">
												<span class="glyphicon glyphicon-forward"></span>

										</a>
								</div>
							</div>
				
			  
		      </div>
			  
			  
            </div>  -->
			<!--</marquee>-->
		<#--  </div>
		</div>
			  -->
		
        <!-- End Our courses content -->
      <#--  </div>
	  
    </section>  -->
    <!--=========== END STUDENTS placement SECTION ================--> 
   

    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs">
      <div class="container">
        <div class="row">
        <!-- Start about us area -->
        
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="newsfeed_area wow fadeInRight">
            <ul class="nav nav-tabs feed_tabs" id="myTab2">
              <li class="active"><a href="#news" data-toggle="tab">KSITAA</a></li>
              <li><a href="#notice" data-toggle="tab">Members</a></li>
              <li><a href="#events" data-toggle="tab">Meets and Events</a></li>  
		<#--	  <li><a href="#members-list" data-toggle="tab">Alumni Members List</a></li> -->
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <!-- Start news tab content -->
              <div class="tab-pane fade in active" id="news">                
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="${img_path!}/alumni/ksitaaweb.jpg" alt="image" />
								</div>
							</div>
						
							<div class="col-lg-6 col-md-6 col-sm-6">
							
								
								<p>During the past years, hundreds and thousands of young men and women have graduated from ksit &amp; ksit has helped and nurtured them to realize their dreams. Our alumnus are exemplary professionals, entreprenuers and responsible citizens of our country and the whole world. We at KSIT are proud of each on of them. Our alumnus is one of the important stake holders of the institution and we wish to expand our alumni association spread all over the globe.</p>
								
								<p>KSIT alumni association was started in the year 2014 and the name <b>CHIRANTHANA</b> has been suggested and been used from 2016 alumni meet. An alumni association was officially registered on 27 August, 2021. Now the official name is <b>KSIT Alumni Association</b> (KSITAA).</p>
								
								<p>If you are a graduate from K S Institute of Technology, you will be a part of our global alumni community, We Delightfully welcome you to be a part of our strong network of former KSIT students. We encourage you to stay in touch with us, so that we can provide you with support and benefits for life. That may be through providing you with career support, helping you reconnect with old friends, access to a wide network of fellow alumni and a range of exciting events.</p>
								
								<p>Stay together with your own alumni network and actively participate on our regular events to  reunite.</p>
								
								<p style="color:#271277;font-size:15px;font-weight:500;">&#9656; Click to <a href="alumni_list.html" target="_blank">View Registered Alumni Member's List</a></p>

								<p style="color:#271277;font-size:15px;font-weight:500;text-align:start;">&#9656; If you are an Alumni of this college &amp; not yet registered,</p><button onclick="window.location.href='alumni_registration.html';" target="_blank" style="color:#271277;font-size:15px;font-weight:500;transform:translate(110%, 20%);">Register Here</button>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12">
								<p></p>	
							</div>
						</div>
                    </div>                    
                  </li>
                </ul>                
               
              </div>
			
			  
              <!-- Start notice tab content -->  
              <div class="tab-pane fade " id="notice">
                <div class="single_notice_pane">
                  <ul class="news_tab">
                    <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="container-fluid">
							<!--<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="img/about/sangham.jpg" alt="image" />
								</div>
							</div>-->
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--								
								
								<p><a href="#" target="_blank"></a></p>
								<p><a href="#" target="_blank"></a></p>	
								<p><a href="#" target="_blank"></a></p>-->
								
					<div class="row">
						<h1 style="text-align:center; color:red">KSIT Alumni Association Committee Members</h1><hr/>
	  				</div>
	
      				<div class="container-fluid">
	        			<div class="col-lg-12 col-md-12 col-sm-12">
		          			<div class="col-lg-3 col-md-3 col-sm-3">
	            				<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Slide1.JPG" alt="image" /></div>
	            				<p class="image_caption"><span style="color:darkblue;font-weight:500;">Sri. R. Rajagopal Naidu</span></p>
	            				<p class="text-left"><span style="color:red;">President, Kammavarisangham<br> - Chief Patron</span></p>
	            			</div>
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Slide2.JPG" alt="image" /></div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Sri. R. Leela Shankar Rao</span></p>
		            			<p class="text-left"><span style="color:red;">Hon. Secretary, Kammavarisangham <br> - Chief Patron</span></p>
		          			</div>
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""> <img class="img-responsive" src="${img_path!}/alumni_members/Slide3.JPG" alt="image" /> </div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Sri. T. Neerajakshulu Naidu</span></p>
		            			<p class="text-left"><span style="color:red;">Treasurer, Kammavarisangham <br> - Chief Patron</span></p>
		          			</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
	            				<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Slide4.JPG" alt="image" /></div>
	            				<p class="image_caption"><span style="color:darkblue;font-weight:500;">Dr. DilipKumar.K</span></p>
	            				<p class="text-left"><span style="color:red;">Principal, K.S.Institute Of Technology -Patron</span></p>
		            		</div>
		            	</div>
		            	<div class="col-lg-12 col-md-12 col-sm-12">
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Slide5.JPG" alt="image" /></div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mr. Vinay K.R</span></p>
		            			<p class="text-left"><span style="color:red;">Associate Manager, Commvault - President</span></p>
		          			</div>
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""> <img class="img-responsive" src="${img_path!}/alumni_members/niranjan.jpg" alt="image" /> </div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mr. Niranjan Balaji</span></p>
		            			<p class="text-left"><span style="color:red;">Founder & Director, Rapsol Technologies Pvt Ltd - Vice President</span></p>
		          			</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
		        				<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Anil.JPG" alt="image" /></div>
		        				<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mr. Anil Kumar. A</span></p>
		        				<p class="text-left"><span style="color:red;">Assistant Professor, K. S.Institute Of Technology - General Secretary</span></p>
		            		</div>
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Slide7.JPG" alt="image" /></div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mrs. Beena K</span></p>
		            			<p class="text-left"><span style="color:red;">Assistant Professor, K.S. Institute Of Technology - Joint Secretary</span></p>
		          			</div>
		          		</div>
		          		<div class="col-lg-12 col-md-12 col-sm-12">
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""> <img class="img-responsive" src="${img_path!}/alumni_members/Slide8.JPG" alt="image" /> </div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Suhas J. Ram</span></p>
		            			<p class="text-left"><span style="color:red;">Founder & CEO, SJR Constructions & Interiors Enterprises - Treasurer</span></p>
		          			</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
		        				<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Piyush.JPG" alt="image" /></div>
		        				<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mr. Piyush Varma</span></p>
		        				<p class="text-left"><span style="color:red;">Member</span></p>
		            		</div>
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""><img class="img-responsive" src="${img_path!}/alumni_members/Akhil.JPG" alt="image" /></div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mr. Akhil Mohan</span></p>
		            			<p class="text-center"><span style="color:red;">Member</span></p>
		          			</div>
		          			<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""> <img class="img-responsive" src="${img_path!}/alumni_members/Sathwik.JPG" alt="image" /> </div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mr. Sathwik Jain</span></p>
		            			<p class="text-left"><span style="color:red;">Member</span></p>
		          			</div>
		          		</div>
		          		<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="col-lg-3 col-md-3 col-sm-3">
		            			<div class=""> <img class="img-responsive" src="${img_path!}/alumni_members/Sunil.JPG" alt="image" /> </div>
		            			<p class="image_caption"><span style="color:darkblue;font-weight:500;">Mr. Sunil Sathyanarayana</span></p>
		            			<p class="text-left"><span style="color:red;">Member</span></p>
		          			</div>
	        			</div>
      			</div>
      			<hr/>
														
							</div>
						</div>
                    </div>                    
                  </li>                         
                  </ul>
                </div>              
              </div>
			  
			  <!-- Start members list tab content -->
              <div class="tab-pane fade " id="members-list">
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="container-fluid">
							<h3>Alumni Members List</h3>
					  		<blockquote>
                      			<span class="fa fa-quote-left"></span>
                      			<p>If you are an Alumni of this college and not yet registered: <a href="alumni_registration.html" target="_blank">&#9656; Please Register Here</a></p>			  
                    		</blockquote>

               <!--  cse start single course -->
                <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>CSE</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                   
                    <div class="singCourse_content">
                    
                    	<h1>CSE</h1>
                    
                    
                    
	                    <#list cseAlumniMembersList as cseAlumniMembers>
									
							<h3 class="singCourse_title"><span>${cseAlumniMembers.content!}</span> <a href="${cseAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>
                
                 <!--  cse end single course -->   
                 
                   
                 <!--  ece start single course -->
                <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>ECE</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                    
                    <div class="singCourse_content">
                    	<h1>ECE</h1>
	                    <#list eceAlumniMembersList as eceAlumniMembers>
									
							<h3 class="singCourse_title"><span>${eceAlumniMembers.content!}</span> <a href="${eceAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>
                
                 <!--  ece end single course -->    
                 
                  
                <!--  mech start single course -->
                <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>ME</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                    
                    <div class="singCourse_content">
                    	<h1>ME</h1>
	                    <#list mechAlumniMembersList as mechAlumniMembers>
									
							<h3 class="singCourse_title"><span>${mechAlumniMembers.content!}</span> <a href="${mechAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>
                
                 <!--  mech end single course -->     
                 
                 
                 <!--  tele start single course -->
                <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>TCE</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                    
                    <div class="singCourse_content">
                    	<h1>TCE</h1>
                    	
	                    <#list teleAlumniMembersList as teleAlumniMembers>
									
							<h3 class="singCourse_title"><span>${teleAlumniMembers.content!}</span> <a href="${teleAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>	
				</div>	
					</div>
                    </div>                    
                  </li>      
                </ul>
              </div>
              <!-- Start events tab content -->
              <div class="tab-pane fade " id="events">
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      
                      <div class="media-body">
                       <div class="row">
							<!--<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="">
									<img class="img-responsive" src="img/about/sangham.jpg" alt="image" />
								</div>
							</div>-->
						
							<div class="col-lg-12 col-md-12 col-sm-12">
							
							<#list alumniMeetsList as alumniMeets>
								
								<h2><a style="font-size:15px;color:#f5a04d;" href="${alumniMeets.imageURL!}" target="_blank">${alumniMeets.heading!}</a></h2>
								<p> ${alumniMeets.content!}  Organised on: ${alumniMeets.eventDate!}</p>
								
								
							</#list>
													
								
							</div>
						
						</div>
                    </div>                    
                  </li>      
                </ul>
                
              </div>
			</div>  
			</div>
			</div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>
    <!--=========== END ABOUT US SECTION ================--> 
   
   
	
	    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <#--  <section id="courseArchive" style="margin-top:0%;">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <#--  <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3>Alumni Members List</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p>If you are alumni of this college and not yet register, <a href="alumni_registration.html" target="_blank">Please register here</a></p>  -->
					  
					 <!-- <p> <a href="alumni_list.html" target="_blank">Click here to see the registered members.</a></p>-->
					  				  
                    <#--  </blockquote>  -->
                  
                    
               <!--  cse start single course -->
                <#--  <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>CSE</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                   
                    <div class="singCourse_content">
                    
                    	<h1>CSE</h1>
                    
                    
                    
	                    <#list cseAlumniMembersList as cseAlumniMembers>
									
							<h3 class="singCourse_title"><span>${cseAlumniMembers.content!}</span> <a href="${cseAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>  -->
                
                 <!--  cse end single course -->   
                 
                   
                 <!--  ece start single course -->
                <#--  <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>ECE</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                    
                    <div class="singCourse_content">
                    	<h1>ECE</h1>
	                    <#list eceAlumniMembersList as eceAlumniMembers>
									
							<h3 class="singCourse_title"><span>${eceAlumniMembers.content!}</span> <a href="${eceAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>  -->
                
                 <!--  ece end single course -->    
                 
                  
                <!--  mech start single course -->
                <#--  <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>ME</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                    
                    <div class="singCourse_content">
                    	<h1>ME</h1>
	                    <#list mechAlumniMembersList as mechAlumniMembers>
									
							<h3 class="singCourse_title"><span>${mechAlumniMembers.content!}</span> <a href="${mechAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>  -->
                
                 <!--  mech end single course -->     
                 
                 
                 <!--  tele start single course -->
                <#--  <div class="col-lg-3 col-md-4 col-sm-12">
                <h1>TCE</h1>
                  <div class="single_course wow fadeInUp alumni_list_members">
                    
                    <div class="singCourse_content">
                    	<h1>TCE</h1>
                    	
	                    <#list teleAlumniMembersList as teleAlumniMembers>
									
							<h3 class="singCourse_title"><span>${teleAlumniMembers.content!}</span> <a href="${teleAlumniMembers.imageURL!}" target="_blank">alumni details</a></h3>
	
						</#list>
					
					</div>
				  </div>
				 </div>  -->
                
                 <!--  tele end single course -->    
                 
				<#--  <p> <a href="alumni_list.html" target="_blank">View Registerd Alumni List</a></p>  -->
<#--  
			</div>
		 </div>
		</div>  -->
	</div>
  </div>
</section>  
				
</@page>