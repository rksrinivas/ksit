<@page>
<!--=========== slider  ================-->
<#--  <section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <#--  <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
              -->
            <#--  <div class="home_slider_content">
               <div class="row">
             
                  <#--  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/canteen/c1.jpg" alt="">                           
                        </article>                     
                     </div>
                     <!-- Indicators -->
                     <!--<ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                     </ol>-->
                     <!-- Indicators -->
                     
                  <!--</div>  -->
               <!--</div>-->
            <!-- </div>  -->
            
         <#--  </div>
      </div>  -->
      <!-- End Our courses content -->
   <#--  </div>
</section>  -->
<!--=========== slider end ================--> 

    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="about">
	<div class="container">
	<div class="news_area home-4">
    <h2 class="text-center">Disclaimer &amp; Privacy policy</h2>
			<div class="row">
				<div class="col-md-12">
          <div class="aboutus_area wow fadeInLeft">
	<hr/>		
<h3><i class="fa fa-info-circle" style="color:#ff5f00;"></i>&nbsp;DISCLAIMER</h3>

<p class="text-justify">
   The information of this website <b>(KSIT)</b> is for general information purposes only. All the information on the site is provided in good faith. Enough care is taken by Kammavari Sangham Group Of Institutions <a href="https://ksgi.edu.in/" target="_blank">(KSGI)</a> to ensure that information on the website is up to date, accurate and correct, readers are requested to make their independent enquiry before relying upon the same. In no event will <b>K S Institute of Technology</b> offer any warranty on the information made available, or be liable for any loss or damage including without limitation, indirect or consequential loss or damage in connection with, the use of information in this website.<br />
   We have appointed <a href="https://rapsoltechnologies.com/" target="_blank">Rapsol Technologies Pvt Ltd</a> as the technical parters in designing, creating and maintaining this website. The contents and images are solely created and provided to <b>Rapsol Technologies Pvt Ltd</b> by us. By using or accessing the website, you agree with the Disclaimer without any qualification or limitation. Design, Images, contents, forms, courses etc., are subject to change without prior notice and are the artist's impression and are an indicative of the actual designs. <br />
   The contents of this website are meant to provide information to the readers of this website about ourselves including our various services, various initiatives taken by us, CSR activities etc. They are only for general information and are subject to change.
   </p>
   <h3><i class="fa fa-info-circle" style="color:#ff5f00;"></i>&nbsp;PRIVACY POLICY</h3>
   <div class="text-justify">
   <p>With KSIT Website, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by the Website and how we use it.
   If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us.</p>
   <p>This privacy policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in the Website. This policy is not applicable to any information collected offline or via channels or links other than this website.</p>
   <p><strong>Information we collect</strong></p>
   <p>The personal information that you are asked to provide, and the reasons why you are asked to provide it, will be made clear to you at the point we ask you to provide your personal information.</p>
   <p>If you contact us directly, we may receive additional information about you such as your name, email address, phone number, the contents of the message and / or attachments you may send us, and any other information you may choose to provide.</p>
   <p><strong>How we use your information</strong></p>
   <p>We use the information we collect in various ways, including to:</p>
   <ul>
   <li>&#9632; Provide, operate, and maintain our website</li>
   <li>&#9632; Improve, personalize, and expand our website</li>
   <li>&#9632; Understand and analyze how you use our website</li>
   <li>&#9632; Develop new products, services, features, and functionality</li>
   <li>&#9632; Communicate with you, either directly or through one of our partners, including for customer service, to provide you with updates and other information relating to the website, and for marketing and promotional purposes</li>
   <li>&#9632; Send you emails</li>
   <li>&#9632; Find and prevent fraud</li>
   <li>&#9632; Log Files</li>
   </ul>
   <br />
   <p>This Website follows a standard procedure. All hosting companies do this and a part of hosting services' analytics. The information collected by log include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information.</p>
</div>
<h3><i class="fa fa-info-circle" style="color:#ff5f00;"></i>&nbsp;EXTERNAL LINKS DISCLAIMER FOR WEBSITE</h3>
<div class="text-justify">
   <p>This Site [and our mobile website] may contain (or you may be sent through the Site links to other websites or content belonging to or originating from third parties or links to websites and features in banners or other advertising) external links that are not investigated, monitored, or checked for accuracy, adequacy, validity, reliability, availability or completeness by us.</p>
   <p>We do not warrant, endorse, guarantee, or assume responsibility for the accuracy or reliability of any information offered by third-party websites linked through the site or any website or feature linked in any banner or other advertising. We will not be a party to or in any way be responsible for monitoring any transaction between you and third-party providers.</p>
</div>
<h3><i class="fa fa-info-circle" style="color:#ff5f00;"></i>&nbsp;PROFESSIONAL DISCLAIMER FOR WEBSITE</h3>
<div class="text-justify">
   <p>The Site cannot and does not contain [medical/legal/fitness/health/other] advice. The [legal/medical/fitness/health/other] information provided is for general informational and educational purposes only and is not a substitute for professional advice.</p>
   <p>Accordingly, before taking any actions based upon such information, we encourage you to consult with the appropriate professionals. We do not provide any kind of [medical/legal/fitness/health/other] advices. The use or reliance of any information contained on this site [or our mobile website] is solely at your own risk.</p>
</div>
<h3><i class="fa fa-info-circle" style="color:#ff5f00;"></i>&nbsp;TESTIMONIALS DISCLAIMER FOR WEBSITE</h3>
<div class="text-justify">
   <p>The Site may contain testimonials by users of our services. These testimonials reflect the real-life experiences and opinions of such users. However, the experiences are personal to those particular users, and may not necessarily be representative of all users of our services. We do not claim, and you should not assume, that all users will have the same experiences. Your individual results may vary.</p>
</div>
<h3><i class="fa fa-info-circle" style="color:#ff5f00;"></i>&nbsp;EMAIL SMS AND CALL POLICY</h3>
<div class="text-justify">
   <p>When you voluntarily send us electronic mail, we will keep a record of this information so that we can respond to you. We only collect information from you when you register on our site or fill out a form. Also, when filling out a form on our site, you may be asked to enter your: name, e-mail address or phone number. You may, however, visit our site anonymously. In case you have submitted your personal information and 
   contact details, we reserve the rights to Call, SMS, Email or WhatsApp about our products and offers, even if your number has DND activated on it.</p>
</div>
	<hr/>


		</div>
        </div>
		</div>
		</div>
	</section>

</@page>    