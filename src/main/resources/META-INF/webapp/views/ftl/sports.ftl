<@page>

<!--=========== slider  ================-->
<section id="home_slider">
   <div class="container" style="width:100%;">
      <!-- Start Our courses content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="home_slider_content">
               <div class="row">
                 
                 	<!--social media icons  -->
			  			<div id="media_icons">
			  				 <ul class="footer_social">
			                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip fb" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip tw"  href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip go"  href="https://plus.google.com/110268862132092225933" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip lin"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip yt"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
			                </ul>
			  			</div>
			  		<!--social media icons  -->
			  	
                 
                  <div id="myCarousel" class="carousel slide">
                     <div class="carousel-inner">
                        <article class="item active">
                           <img class="animated slideInLeft" src="${img_path!}/sports/slider/s1.jpg" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated bounceIn" src="${img_path!}/sports/slider/s2.jpg" alt="">										
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/sports/slider/s3.jpg" alt="">
                         
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/sports/slider/s4.jpg" alt="">
                        
                        </article>
                        <article class="item">
                           <img class="animated rollIn" src="${img_path!}/sports/slider/s5.jpg" alt="">
                          
                        </article>
                     </div>
                     <!-- Indicators -->
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                     </ol>
                     <!-- Indicators -->
                     
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <!-- End Our courses content -->
   </div>
</section>
<!--=========== slider end ================--> 



    
    <!--=========== BEGIN ABOUT US SECTION ================-->
<section id="aboutUs_dept"> <!---->
   <div class="container-fluid">
      <div class="row">
         <!-- Start about us area -->
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="notify">
               <p>Announcement | </p>
               <div class="content">
                  <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                     <h5>
                        "${scrollingTextBottom!}"
                     </h5>
                     <!--<h5>"Counselling Dates will be announced soon"</h5>-->
                  </marquee>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=========== END ABOUT US SECTION ================--> 

<!-- welcome -->
<div class="dept-title">
   <h1>Welcome To Sports Department</h1>
   <p class="welcome-text">Sport includes all forms of competitive physical activity or games which, through casual
    or organized participation, at least in part aim to use, maintain or improve physical ability and skills while
     providing enjoyment to participants, and in some cases, entertainment for spectators.</p>
</div>
<!-- welcome -->
	  	
	 <!--dept tabs -->
	<section id="vertical-tabs">
		<div class="container-fluid">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="vertical-tab padding" role="tabpanel">
			               
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
			                    <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab">Vision & Mission</a></li>
			                    <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab" class="dept-cart">Infrastructure / Facilities</a></li>
			                    <li role="presentation"><a href="#rules" aria-controls="rules" role="tab" data-toggle="tab">Rules & Regulations</a></li>
			                    <li role="presentation"><a href="#job" aria-controls="job" role="tab" data-toggle="tab">Job Responsibilities</a></li>
			                    <li role="presentation"><a href="#activities" aria-controls="activities" role="tab" data-toggle="tab">Sports Activities</a></li>
			                    
								<li role="presentation"><a href="#gallery_dept" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
			                    <li role="presentation"><a href="#achievers" aria-controls="achievers" role="tab" data-toggle="tab">Achievers</a></li>
			                    			                    
			                   <li role="presentation"><a href="#reports" aria-controls="achievers" role="tab" data-toggle="tab">Reports</a></li>
			                    
			                    
			                    <!--<li role="presentation"><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Section 3</a></li>
			                    <li role="presentation"><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">Section 4</a></li>-->
			                </ul>
			                <!-- Nav tabs -->
			               
			                <!-- Tab panes -->
			                <div class="tab-content tabs">
			                	
			                	<!-- profile -->
			                	 <div role="tabpanel" class="tab-pane fade in active" id="profile">
			                	  <div class="row">
			                        <h3>Profile</h3>
			                        <p>K S institute of technology, Department of Physical Education & Sports is a cell of major activity
			                         in the Campus. Visvesvaraya Technological University (VTU, Belagavi). The Department of Physical
			                          Education & Sports is well equipped with modern infrastructure and conducts activities like
			                           Basketball, Volleyball, Badminton, Table Tennis, Cricket, Handball, Football, Netball, Chess,
			                            and Athletics.</p>
			                            
			                            <h3>Head Of The Department Desk</h3>
						                  <div class="aboutus_area wow fadeInLeft">
						                     <div class="col-lg-6 col-md-6 col-sm-6">
						                        <img class="hod-img img-responsive" src="${img_path!}/sports/umesh_new.jpg" alt="image" />	
						                       <div class="qualification_info">
							                        <h3 class="qual-text">Umesh. S</h3>
							                        <h4 class="qual-text">Physical Education Director</h4>
							                        <h3 class="qual-text">B.A , M.Ped</h3>
						                        </div>
						                    
						                     </div>
						                     <div class="col-lg-6 col-md-6 col-sm-6">

													
													<p><b>Message:</b> Physical education is an integral part of the total education.
													 Quality physical education programs help all students develop health-related fitness, 
													 physical competence, cognitive understanding, and positive attitude about physical
													  activity, so that they can adopt healthy and physically active lifestyles.
													   The department of Physical Education at The Kammavari Sangham Institute of
													    Technology aims at such activities which helps the students develop a holistic 
													    approach. The students have participated in many various tournaments conducted by 
													    V.T.U , all India universities, district, state & national events and have brought 
													    laurels to their institution. </p>
													
													<p>We are proud to be one amongst the 10 College of VTU to Received Meritorious institutions Cash Award of 1 Lakh for sports achievement for the academic year 2015-16 in VTU sports.
																			                        </p>
													<hr/>						                        
													<#--  <p><span style="color:red;">Mobile No:</span> 9845413641, 9742288555</p>  -->
													
													<p><span style="color:red;">E-mail:</span> ksitsports@gmail.com, pdumeshgowda@gmail.com</p>
													
													<p><span style="color:red;">Area of Specialization:</span>  Hand Ball &amp; Athletics, Cricket, Kabaddi, Kho-Kho.</p>
													<hr/>
						                     </div>
						                   </div>
			                            
			                            
			                      </div>
			                    </div>
			                    <!-- profile -->
			                    
			                 
   <!-- vision and mission -->
			                    <div role="tabpanel" class="tab-pane fade" id="vision">
			                    	 <div class="row">
						                  <div class="aboutus_area wow fadeInLeft">
						                     <h3>Vision</h3>
						                     <ul class="list">
						                        <li>To develop physical and mental ability of students and make them capable of handling the pressures and have a healthy and well managed life style.</li>
						                     </ul>
						                     <h3>Mission</h3>
						                     <ul class="list">
						                        <li>To bring out the best in sporting talent among the student community and encourage them to participate in competitions at various levels.
											    <li>To educate the importance of physical education among the student community and enable then to manage stress.
						                        </li>
						                        <li>To bring awareness among students the need for physical education and wellbeing to make them effective learners.</li>
						                        </li>
						                     </ul>
						                     
						                      <h3>Values</h3>
						                      <ul class="list">
						                        <li>Being aware of the respect for individual differences, we promote professional, Social and ethical responsibility.</li>
						                     </ul>
						                     
						                      <h3>HIGHLIGHTS OF DEPARTMENT</h3>
						                      
						                      <p> K S institute of technology, Department of Physical Education & Sports is a cell of major
						                       activity in the Campus. Visvesvaraya Technological University (VTU, Belagavi).</p> 

												<p> The Department of Physical Education & Sports is well equipped with modern infrastructure
												 and conducts activities like Basketball, Volleyball, Badminton, Table Tennis, Cricket, Handball, Football, Netball,
												  Chess, and Athletics.</p> 
												
											    <p> 	Sports are in fact a way of life for the student's health and fitness and those looking for professional star status.</p> 
												
												<p> Physical education, a learning experience, offers a unique opportunity for problem solving, self-expression and socialization.</p> 
												
												<p> A well-implemented, comprehensive programme is an essential component for the growth of both mind and body</p> 
						                     
						                     
						                   </div>
						              </div>     
									</div>
			                    <!-- vision and mission -->
			                    
			                    			                    
			                    
			                     <!-- Facilities / Infrastructure -->
				               <div role="tabpanel" class="tab-pane fade" id="facilities">
				               	 <div class="row">
				               		 <h3>Facilities</h3>
				               		 <p>K S institute of technology, Department of Physical Education & Sports is a cell of major activity 
				               		 in the Campus. Visvesvaraya Technological University (VTU, Belagavi). The Department of Physical 
				               		 Education & Sports is well equipped with modern infrastructure and conducts activities like Basketball,
				               		  Volleyball, Badminton, Table Tennis, Cricket, Handball, Football, Netball, Chess, and Athletics. 
				               		  Sports are in fact a way of life for the student's health and fitness and those looking for 
				               		  professional star status. Physical education, a learning experience, offers a unique opportunity
				               		   for problem solving, self-expression and socialization. A well-implemented, comprehensive programme 
				               		   is an essential component for the growth of both mind and body</p>
				               		  
				               		  <h3>Out-door sports</h3>
				               		 <ul class="list">
						                   <li>Cricket</li>
						                   <li>Football</li>
						                   <li>Basket ball</li>
						                   <li>Volley ball</li>
						                   <li>Throw ball</li>
						                   <li>Kabaddi</li>
						                   <li>Hand ball</li>
						                   <li>Kho-Kho</li>
						                
						             </ul>
				               		  <h3>In-door sports</h3>
				               		 <ul class="list">
						                   <li>Chess</li>
						                   <li>Carrom</li>
						                   <li>Table Tennis</li>
						                   <li>Badminton</li>
						             </ul>
				               		 
				               		 
			                      </div>
						       </div>    
			                    <!-- facilities -->
			                    
			                     <!-- Rules and Regulations -->
			                    <div role="tabpanel" class="tab-pane fade" id="rules">
			                         <div class="row">
				               		   <h3>Rules and Regulations</h3>
				               		   <ul class="list">
						                   <li>Student can borrow the sports equipments by submitting his/her ID card &should signature in the issue register.
						                   </li>
						                   <li>The material is not transferable.
						                   </li>
						                   <li>In case of any damages made to the sports equipments, The P E Director has the authority to collect the same equipment from the Student & nature of damage will be assessed.
						                   </li>
						                   <li>If any student finds the equipments in the campus it is to bow returned to the department.
						                   </li>
										   <li>During the sports activities show the ID cards.
										   </li>
						               </ul>
				               		   
			                         </div>
			                    </div>
								<!-- Rules and Regulations -->
								
								
								  <!-- Jobs Responsibilities[ -->
			                    <div role="tabpanel" class="tab-pane fade" id="job">
			                         <div class="row">
				               		   <h3>Job Responsibilities</h3>
										<ul class="list">
						                   <li>Co-ordinate with students for all sports and games activities.</li>
						                   <li>Training the students at the undergraduate level; some graduates.</li>
						                   <li>Conduct matches for several game Conducting Inter-college tournaments for the students.</li>
						                   <li>To represent students locally and nationally on sporting issues.</li>
						                   <li>Advise undergraduate students in the department.</li>
						                   <li>Contribute to the service mission of Department, College, and University.</li>
						                   <li>Contribute to professional activities in the discipline.</li>
						                   <li>Control the warm-up, cool-down and stretching drills for all players.</li>
						                   <li>Ensure that all necessary team equipments are available.</li>
						                   <li>Assist the coach to assess player injuries sustained during training and playing.</li>
						                   <li>Working within specific guidelines, e.g. equal opportunities, health and safety, child protection.</li>
						                   <li>Offering coaching and supervision when appropriate.</li>
						                   <li>Instruct students in the safety practices associated with different environments.</li>
						                </ul>			                  
						              </div>
			                    </div>
			                    
								<!-- Jobs Responsibilities -->
			
			                   
			                   <!-- Sports activities -->
			                    <div role="tabpanel" class="tab-pane fade" id="activities">
			                    	<div class="row">
			                          <h3>SPORTS ACTIVITIES</h3>
			                          
			                           <#list sportsActivitiesList as sportsActivities>
						                  <div class="row aboutus_area wow fadeInLeft">
						                     <div class="col-lg-6 col-md-6 col-sm-6">
						                        <img class="img-responsive" src="${sportsActivities.imageURL!}" alt="image" />	                       
						                     </div>
						                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
						                     	 <h3>${sportsActivities.heading!}</h3>	                      	 
						                       	 <p>"${sportsActivities.content!}"</p>
						                       	 <p><span class="events-feed-date">${sportsActivities.eventDate!}  </span></p>
						                       	<!--<a class="btn btn-primary" href="nss_events.html">View more</a>-->
						                     </div>
						                  </div>
					                  
					                           <hr />  
					                    </#list>   
			                        
									  
									<!--<p><a href="${img_path!}/sports/slide/list of sports activities from 2014 to 2017.pdf" target="_blank">List of sports activities from 2014 to 2017</a></p>
								
									<p><a href="${img_path!}/sports/slide/sports activities and achievements.pdf" target="_blank">sports activities and achievements</a></p>
							-->
									
							
							
			                        </div>     
			                    </div>
			                  <!-- Sports activities --> 
			                  
			                      <!-- Gallery  -->
				               <div role="tabpanel" class="tab-pane fade" id="gallery_dept">
				               	<div class="row">
				                  <h3>Gallery</h3>
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <div id="gallerySLide" class="gallery_area">
				                           <#list sportsGalleryList as sportsGallery>	
				                           <a href="${sportsGallery.imageURL!}" target="_blank">
				                           <img class="gallery_img" src="${sportsGallery.imageURL!}" alt="" />					                    			                   
				                           <span class="view_btn">${sportsGallery.heading!}</span>
				                           </a>
				                           </#list>
				                        </div>
				                     </div>
				                  </div>
				                  </div>
				               </div>
				               <!-- Gallery -->
				                    
				  
				          <!-- Achievers -->
				               <div role="tabpanel" class="tab-pane fade" id="achievers">
				               	<div class="row">
				               		
				                     <!--Department Achievers-->
				                          <h3>College Achievers</h3>
						                  <#list sportsAchieversList as sportsAchievers>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${sportsAchievers.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h3>${sportsAchievers.heading!}</h3>	                      	 
							                       	 <p>${sportsAchievers.eventDate!} </p>
							                       	 <p> ${sportsAchievers.content!}</p>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>  
						                 <!--Department Achievers-->   
						                 
				                     <!--University Level Achievers-->
				                          <h3>University Level Achievers</h3>
				                       
						                  <#list universityAchieversList as universityAchievers>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${universityAchievers.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h3>${universityAchievers.heading!}</h3>	                      	 
							                       	 <p>${universityAchievers.eventDate!} </p>
							                       	 <p> ${universityAchievers.content!}</p>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>   
						            <!--University Level Achievers-->
						            
						            <!--State Level Achievers-->
				                          <h3>State Level Achievers</h3>
				                       
						                  <#list stateAchieversList as stateAchievers>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${stateAchievers.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h3>${stateAchievers.heading!}</h3>	                      	 
							                       	 <p>${stateAchievers.eventDate!} </p>
							                       	 <p> ${stateAchievers.content!}</p>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>   
						            <!--State Level Achievers-->
						            
						            <!--National Level Achievers-->
				                          <h3>National Level Achievers</h3>
				                       
						                  <#list nationalAchieversList as nationalAchievers>
							                  <div class="row aboutus_area wow fadeInLeft">
							                     <div class="col-lg-6 col-md-6 col-sm-6">
							                        <img class="img-responsive" src="${nationalAchievers.imageURL!}" alt="image" />	                       
							                     </div>
							                     <div class="col-lg-6 col-md-6 col-sm-6">	                     	
							                     	 <h3>${nationalAchievers.heading!}</h3>	                      	 
							                       	 <p>${nationalAchievers.eventDate!} </p>
							                       	 <p>${nationalAchievers.content!}</p>
							                     </div>
							                  </div>
						                  
						                           <hr />  
						                    </#list>   
						            <!--National Level Achievers-->
						            
				                    
				               	</div>
				               </div>
				               <!-- Achievers -->
				               
				                    <!-- Reports  -->
				               <div role="tabpanel" class="tab-pane fade" id="reports">
				                  <div class="row">
				                     <div class="col-lg-12 col-md-12 col-sm-12">
				                        <h3>Reports</h3>
				                        <#list sportsReportsList as sportsReports>
				                        <p>${sportsReports.heading!}  <a href="${sportsReports.imageURL!}" class="time_table_link" target="_blank">[view here]</a></p>
				                        </#list>	
				                     </div>
				                   </div>
				               </div>
				               <!-- Reports  -->   
			                    

			                  
			                   <!-- -->
			                    <div role="tabpanel" class="tab-pane fade" id="Section4">
			                    	<div class="row">
			                          <h3>Section 4</h3>
			                        </div     
			                    </div>
			                  <!-- -->
			                  
			                  <!-- -->
			                    <div role="tabpanel" class="tab-pane fade" id="Section4">
			                    	<div class="row">
			                          <h3>Section 4</h3>
			                        </div     
			                    </div>
			                  <!-- --> 
			                    
			                    
			
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
					
	</section>
	
	<!--dept tabs -->
  
 	
    
</@page>