<#macro libraryMacro libraryList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Active</th>
			<th>Rank</th>
			<th>Edit</th>
			<th>delete</th>
			
		</thead>
		<tbody>
			<#if libraryList?has_content>
				<#list libraryList as library>
					<tr>
					
						<td>
							<span id="${library.id!}Heading">${library.heading!}</span>
						</td>
								
						<td>
							<span id="${library.id!}Content">${library.content!}</span>
						</td>
						
						
						<td>
							<span id="${library.id!}Active">${library.active?string!}</span>
						</td>
						<td>
							<span id="${library.id!}Rank">
								<#if library.rank?has_content>
									${library.rank?string!}
								</#if>
							</span>
						</td>
												
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editLibraryModal" data-id=${library.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteLibrary('${library.id!}');">
								delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Active</th>
			<th>Rank</th>
			<th>Edit</th>
			<th>delete</th>
		</tfoot>
	</table>
</#macro>	