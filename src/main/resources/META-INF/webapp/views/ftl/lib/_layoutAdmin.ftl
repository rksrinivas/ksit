<#macro pageAdmin title="ksit" showFooter=true>
<#compress>
<!doctype html>
<html lang="en">
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
	<meta name="theme-color" content="#1e2223">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${title}</title>
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="${img_path!}/wpf-favicon.png"/>
    <!-- CSS
    ================================================== --> 
	<!-- ===================== preloader ===================== -->      
	<link href="${css_path!}/preloader.css" type="text/css" rel="stylesheet">      
    <!-- Bootstrap css file-->
    <link href="${css_path!}/bootstrap.min.css" rel="stylesheet">
     <link type="text/css" media="all" rel="stylesheet" href="${css_path!}/jquery.tosrus.all.css" />    
    <link href="${css_path!}/slider_fade.css" rel="stylesheet">
    <!-- Font awesome css file-->
    <link href="${css_path!}/font-awesome.min.css" rel="stylesheet">
    <!-- Superslide css file-->
    <link href="${css_path!}/superslides.css" rel="stylesheet" >
    <!-- Slick slider css file -->
    <link href="${css_path!}/slick.css" rel="stylesheet"> 
    <!-- Circle counter cdn css file -->
    <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/css/jquery.circliful.css'>  
    <!-- smooth animate css file -->
    <link rel="stylesheet" href="${css_path!}/animate.css"> 
    <!-- preloader -->
    <!--<link rel="stylesheet" href="${css_path!}/queryLoader.css" type="text/css" />-->
    <!-- gallery slider css -->
    <link type="text/css" media="all" rel="stylesheet" href="${css_path!}/jquery.tosrus.all.css" />    
    <!-- Default Theme css file -->
    <link id="switcher" href="${css_path!}/themes/default-theme.css" rel="stylesheet">
    <!-- Main structure css file -->
    <link href="${css_path!}/style${timestamp!}.css" rel="stylesheet">
	<link href="${css_path!}/slider_caption${timestamp!}.css" rel="stylesheet">
	<link href="${css_path!}/testimonial${timestamp!}.css" rel="stylesheet">
	<link href="${css_path!}/slider${timestamp!}.css" rel="stylesheet">
    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>    
	<!-- placcement slider -->
    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>   
	<link href="${css_path!}/admin${timestamp!}.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="${js_path!}/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- initialize jQuery Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Preloader js file -->
    <!--<script src="${js_path!}/queryloader2.min.js" type="text/javascript"></script>-->
    <!-- For smooth animatin  -->
    <script src="${js_path!}/wow.min.js"></script>  
    <!-- Bootstrap js -->
    <script src="${js_path!}/bootstrap.min.js"></script>
    <!-- slick slider -->
    <script src="${js_path!}/slick.min.js"></script>
    <!-- superslides slider -->
    <script src="${js_path!}/jquery.easing.1.3.js"></script>
    <script src="${js_path!}/jquery.animate-enhanced.min.js"></script>
    <script src="${js_path!}/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>   
    <!-- for circle counter -->
    <script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
    <!-- Gallery slider -->
    <script type="text/javascript" language="javascript" src="${js_path!}/jquery.tosrus.min.all.js"></script> 
    <script src="${js_path!}/main_slider.js"></script>
	<script src="${js_path!}/slider.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
    <!-- Custom js-->
    <script src="${js_path!}/custom${timestamp!}.js"></script>
    <script src="${js_path!}/util${timestamp!}.js"></script>
    <script src="${js_path!}/dataUtil${timestamp!}.js"></script>
    <script src="${js_path!}/eventUtil${timestamp!}.js"></script>
    <script src="${js_path!}/facultyUtil${timestamp!}.js"></script>
    <script src="${js_path!}/achieverUtil${timestamp!}.js"></script>
    <script src="${js_path!}/galleryUtil${timestamp!}.js"></script>
    <script src="${js_path!}/libraryUtil${timestamp!}.js"></script>
    <script src="${js_path!}/testimonialUtil${timestamp!}.js"></script>
    <script src="${js_path!}/placementUtil${timestamp!}.js"></script>
    <script src="${js_path!}/placementTrainingUtil${timestamp!}.js"></script>
	<script src="${js_path!}/permissions${timestamp!}.js"></script>
	<script src="${js_path!}/contactUsUtil${timestamp!}.js"></script>
	<script src="${js_path!}/hostelUtil${timestamp!}.js"></script>	
	<script src="${js_path!}/labDataUtil${timestamp!}.js"></script>
	<script src="${js_path!}/programmeUtil${timestamp!}.js"></script>
	<script src="${js_path!}/programmeDataUtil${timestamp!}.js"></script>
	<script src="${js_path!}/deptLabUtil${timestamp!}.js"></script>
	<script src="${js_path!}/sliderUtil${timestamp!}.js"></script>
	<script src="${js_path!}/nba${timestamp!}.js"></script>
	<link rel="stylesheet" href="${css_path!}/jquery.dataTables.min.css" type='text/css' />
  </head>
  <body>    
	<!-- ============= Preloader ============= -->
	<div class="loader-wrapper" id="loader-wrapper"></div>
    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"></a>
    <!-- END SCROLL TOP BUTTON -->
  <!--=========== BEGIN HEADER SECTION ================-->
  <!--=========== BEGIN HEADER SECTION ================-->
    <div class="main">
        <p class="code">College Code</p>
        <div class="coll_code">
            <p class="cet">CET : E091</p>
            <br/>
            <p class="comed">COMED-K : E068</p>
        </div>
         <#--  <p><i class="fa fa-bell" id="newsletter-icon"></i> Notification <span style="color:#f69c42;">|</span> </p>  -->
         <a class="online_fees text-center" href="http://www.ksit.ac.in/">Go Back <i class="fa fa-arrow-right"></i></a>
        <#--  <div class="news">
            <marquee scrollamount="3" onmouseover="stop();"  onmouseout="start();">
               <h5>
                  ${scrollingTextTop!}
               </h5>
            </marquee>
        </div>  -->
    </div>
      
        <div class="headertext" title="K S Institute of Technology (KSIT)">
			<div id="header-responsible">
                  <p class="head_name"><b>Kammavari Sangham (R) 1952, K.S.Group of Institutions</b></p>
                  <p class="head_address headergap text-shadow-head"><span><b>K. S. INSTITUTE OF TECHNOLOGY</span></b></p>
				  <p class="head_text headergap"><i class="fa fa-map-marker"></i>&nbsp;No.14, Raghuvanahalli, Kanakapura Road, Bengaluru - 560109, <a class="head_text headergap" href="tel:+91 9900710055" title="Call Now"><i id="ring-header" class="fa fa-phone"></i>&nbsp;9900710055</a></p>
                  <p class="head_text headergap">Affiliated to VTU, Belagavi &amp; Approved by AICTE, New Delhi, <span style="color:red;">Accredited by NBA , NAAC &amp; IEI</span></p>
				   <a href="index.html" style="text-decoration:none;"><img src="${img_path!}/sangam.jpeg" alt="logo" class="logoImage" id="sangamlogo"> </a>
               	  <a href="index.html" style="text-decoration:none;"><img src="${img_path!}/leftlogo.png" alt="logo" class="logoImage" id="leftlogo"> </a>
				  
          		 <img src="${img_path!}/rightlogo_12.jpg" alt="logo" id="rightlogo" class="logoImage fright">
		  	</div>
   		</div>
   		
   	<!-- ===================== New navbar ===================== -->
    <header id="header">
		
      <!-- BEGIN MENU -->
      <div class="menu_area">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation"> 
		<div class="container">
            <div class="navbar-header">
						
				<!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- LOGO -->
              <!-- TEXT BASED LOGO -->
              <!--<a class="navbar-brand" href="index.html">WpF <span>Degree</span></a>  -->            
              <!-- IMG BASED LOGO  -->
			  
			  
			 
           
			<div id="navbar" class="navbar-collapse collapse">
               <ul id="top-menu" class="nav navbar-nav n main-nav">
               
				<li><a href="/admin">Home</a></li>
												
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                   
                    <li><a href="/admin/academic_governing_council">Academic Governing Council</a></li>
					<li><a href="/admin/iiic">Industry Institute Interaction Cell(IIIC)</a></li>   			    		                                           
					<li><a href="/admin/office_administration">Office Administration</a></li>				
					<li><a href="/admin/disciplinary_measures">Disciplinary Measures</a></li>
					<li><a href="/admin/public_press">Press</a></li>	
					<li><a href="/admin/grievance">Grievance</a></li>	
					<li><a href="/admin/institutionalGovernance">Institutional governance</a></li>
					<li><a href="/admin/approvals">Affiliations & Approvals</a></li>
					                  
                  </ul>
                </li> 
				
				 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Programme <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                   
                    <li><a href="/admin/UG">UG</a></li>
                    <li><a href="/admin/PG">PG</a></li>
                                     
                  </ul>
                </li> 
				
					
				
				 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Departments <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
				  
          	<li><a href="/admin/aiml_dept">Artificial Intelligence &amp; Machine Learning</a></li>
            <li><a href="/admin/csd_dept">Computer Science &amp; Design</a></li>
					  <li><a href="/admin/cse_dept">Computer Science &amp; Engineering</a></li>
              <li><a href="/admin/csicb_dept">Computer Science &amp; Engineering (icb)</a></li>
                <li><a href="/admin/cce_dept">Computers &amp; Communication Engineering</a></li>
				  	<li><a href="/admin/ece_dept">Electronics &amp; Communication Engineering</a></li>
					  <li><a href="/admin/mech_dept">Mechanical Engineering</a></li> 
			<!--		<li><a href="/admin/tele_dept">Electronics & Telecommunication Engineering</a></li>  -->
                    <li><a href="/admin/science_dept">Science and Humanities</i></a></li>
					
                  </ul>
                </li>  
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">NBA<i class="fa fa-angle-down"></i></a>
                	<ul class="dropdown-menu" role="menu">
                	    <li class="dropdown">
                			<a href="/admin/nba_previsit">NBA Previsit</a>
                		</li>  
                	    <li class="dropdown">
                			<a href="/admin/nba">NBA Applied</a>
                		</li>  
                	</ul>
                </li>
                
                <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">NAAC<i class="fa fa-angle-down"></i></a>
                	<ul class="dropdown-menu" role="menu">
                	    <li class="dropdown">
                			<a href="/admin/naac">NAAC Cycle 1</a>
                		</li>  
                	    <li class="dropdown">
                			<a href="/admin/naac_cycle2_aqar">NAAC Cycle 2 AQAR</a>                	    
                			<a href="/admin/naac_cycle2_ssr">NAAC Cycle 2 SSR</a>
                			<a href="/admin/naac_cycle2_dvv_clarification">NAAC Cycle 2 DVV Clarification</a>
                			<a href="/admin/naac_cycle2_dvv_clarification_extended_profile">NAAC Cycle 2 DVV Clarification Extend Profiles</a>
                			
                		</li>  
                	</ul>
                                   
                </li>  


				<li class="dropdown">
                  <a href="/admin/onlineadmissions">Online Admissions</a>
                </li>  
                <li class="dropdown">
                  <a href="/admin/iqac">IQAC</a>
                </li>  
                
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Admissions</a>
                  <ul class="dropdown-menu" role="menu">
                   
                    <li><a href="/admin/fees_structure">Fees Structure</a></li>
					                  
                  </ul>
                </li> 
                
                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Facilities &nbsp;<i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    
					<li><a href="#">Library &nbsp;<i class="fa fa-angle-right"></i></a>
					
						<ul class="submenu">
							<li><a href="/admin/about_library">About Library</a></li>
						</ul>
					 </li>
					 
                    <li><a href="/admin/sports">Sports</a></li>
                    
					         <!-- <li><a href="/admin/transport">Transport</a></li>	-->
                  </ul>
                </li>  
                
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Life at KSIT &nbsp;<i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/admin/alumni">Alumni</a></li>
                    <li><a href="/admin/alumni_list">Alumni List</a></li>
                    <li><a href="/admin/ananya">Ananya</a></li>
                    <li><a href="/admin/nss">NSS</a></li>	
                    <li><a href="/admin/redcross">Red Cross</a></li>
                    <li><a href="/admin/feedback">Feedback</a></li>
					<li><a href="/admin/anti_ragging_committee">Anti Ragging Committee</a></li> 
					<li><a href="/admin/anti_sexual_harassment_committee">Anti Sexual Harassment Committee</a></li>                   					                  
                  </ul>
                </li> 
								
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Placements &nbsp;<i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/admin/about_placement">About Placement</a></li>
					<li><a href="/admin/placement_drive">Placement Drive</a></li>
					<li><a href="/admin/placed_students">Placed Students</a></li>
					<li><a href="/admin/placement_statistics">Placement Statistics</a></li>
					<li><a href="/admin/placement_upcoming_events_latest_news">Upcoming Events & Latest News</a></li>						                     
                  </ul>
                </li> 
                		
				<li><a href="/admin/contactUs">Contact Us</a></li>
				<li><a href="/admin/hostel">Hostel Enrollment</a></li>
				<li><a href="/admin/course">Admission Enquiries</a></li>								
				<li><a href="/admin/permissions">Permissions</a></li>
				
				<#if loggedInUser?has_content>
					<li><a href="/logout" style="color:yellow;font-weight:600 !important;">Sign Out</a></li>
				<#else>
					<li><a href="/admin/signin" style="color:yellow;font-weight:600 !important;">Sign In</a></li>
					<li><a href="/admin/signup" style="color:yellow;font-weight:600 !important;">Sign Up</a></li>
				</#if>
				
				<#if loggedInUser?has_content>
					<li style="background-color:blue;"><a href="#"><i class="fa fa-unlock-alt"></i> Welcome ${loggedInUser.username!}</a></li>
				</#if>
              </ul>          
            </div><!--/.nav-collapse -->
        </nav>  
      </div>
      <!-- END MENU -->    
    </header>
    <!--=========== END HEADER SECTION ================--> 
	
	<#include "/admin/popup/data.ftl">
	<#include "/admin/popup/event.ftl">
	<#include "/admin/popup/faculty.ftl">
	<#include "/admin/popup/achiever.ftl">
	<#include "/admin/popup/gallery.ftl">
	<#include "/admin/popup/testimonial.ftl">
	<#include "/admin/popup/placementCompany.ftl">
	<#include "/admin/popup/placementTraining.ftl">
	<#include "/admin/popup/library.ftl">
    <#include "/admin/popup/labData.ftl">
    <#include "/admin/popup/programme.ftl">
    <#include "/admin/popup/programmeData.ftl">
    <#include "/admin/popup/deptLab.ftl">
    <#include "/admin/popup/slider.ftl">	
	
    <#nested/>
    
    <#if showFooter>
	    <!--=========== BEGIN FOOTER SECTION ================-->
	    <footer id="footer">
	      <!-- Start footer top area -->
	      <div class="footer_top">
	        <div class="container">
	          <div class="row">
	            <div class="col-ld-3  col-md-3 col-sm-3">
	              <div class="single_footer_widget">
	                <!--<h3>Links</h3>-->
	                <ul class="footer_widget_nav">
					
						<p><i class="fa fa-link" id="footer-icons"></i> Important links</p>
	                  <li><a href="http://www.aicte-india.org/" target="_blank">AICTE</a></li>
	                  <li><a href="http://www.vtu.ac.in/" target="_blank">VTU</a></li>
	                  <li><a href="sitemap.html" target="_blank">Sitemap</a></li>
					          <li><a href="#footer" id="myBtn">Careers</a></li>                  
	                  <li><a href="https://easypay.axisbank.co.in/easyPay/makePayment?mid=MjYyODM%3D" target="_blank">Online Fee Payment</a></li>
	                </ul>
	              </div>
	            </div>
	            <div class="col-ld-3  col-md-3 col-sm-3">
	              <div class="single_footer_widget">
	                <!--<h3>Community</h3>-->
	                <ul class="footer_widget_nav">
	                  
					  <p><i class="fa fa-support" id="footer-icons"></i> Committees</p>    
	          <li><a href="public_press.html" target="_blank">Public Relations &amp; Press</a></li>
	          <li><a href="${img_path!}/pdf/KSIT_Grievance_Cell.pdf" target="_blank">Grievance Redressal Cell</a></li>
					  <li><a href="disciplinary_measures.html">Disciplinary Measures</a></li>
					  <li><a href="contact.html">Contact</a></li>     
					  <li><a href="privacy.html">Disclaimer &amp; Privacy Policy</a></li>
	                </ul>
	              </div>
	            </div>
	            <div class="col-ld-3  col-md-3 col-sm-3">
	              <div class="single_footer_widget">
	                <!--<h3>Others</h3>-->
	                <ul class="footer_widget_nav">
						<p><i class="fa fa-institution" id="footer-icons"></i> Group of Institutions</p>
              <li><a href="https://ksgi.edu.in/" target="_blank">KSGI</a></li>
	            <li><a href="https://www.ksit.ac.in/" target="_blank">KSIT</a>
              <li><a href="https://www.kssem.edu.in/" target="_blank">KSSEM</a></li>
	            <li><a href="https://www.kssa.edu.in/" target="_blank">KSSA</a></li>
					    <li><a href="https://kspolytechnic.edu.in/" target="_blank">KS Polytehnic</a></li>
               <li><a href="https://kspuc.edu.in/" target="_blank">KS Pre University College</a></li>
	            <li><a href="https://www.google.co.in/search?rlz=1C1CHBD_enIN746IN746&q=k+s+hospital+bangalore&sa=X&ved=0ahUKEwjb9J3t1qbWAhXHso8KHQ1aAggQ1QIImQEoAA&biw=1366&bih=662" target="_blank">KS Hospital</a></li>
					    <li><a href="http://www.kammavaricreditsociety.com/" target="_blank">KS Credit Co-operative Bank</a></li>
	                </ul>
	              </div>
	            </div>
	            <div class="col-ld-3  col-md-3 col-sm-3">
	              <div class="single_footer_widget">
	                <p><i class="fa fa-globe" id="footer-icons"></i> Social Links</p>
	                <ul class="footer_social">
	                  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip" href="https://www.facebook.com/ksit.official/"><i class="fa fa-facebook"></i></a></li>
	                  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip"  href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
	                  <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip"  href="https://plus.google.com/110268862132092225933"><i class="fa fa-google-plus"></i></a></li>
	                  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip"  href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
	                  <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip"  href="http://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
	                </ul>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <!-- End footer top area -->
	
	      <!-- Start footer bottom area -->
	      <div class="footer_bottom">
	        <div class="container">
	          <div class="row">
	            <div class="col-lg-6 col-md-6 col-sm-6">
	              <div class="footer_bootomLeft">
	                <p style="color:#fff;font-weight:400;">KSIT&nbsp;<span id="footer-icons">|</span>&nbsp;Copyright &copy; 2017 - <script>document.write(new Date().getFullYear());</script>&nbsp;<span id="footer-icons">|</span>&nbsp;All Rights Reserved</p>
	              </div>
	            </div>
	            <div class="col-lg-6 col-md-6 col-sm-6">
	              <div class="footer_bootomRight">
	                <p style="color:#fff;font-weight:400;">Designed and developed by <a href="http://www.rapsoltechnologies.com/" target="_blank" rel="nofollow">Rapsol Technologies Pvt Ltd</a></p>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <!-- End footer bottom area -->
	    </footer>
	    <!--=========== END FOOTER SECTION ================--> 
	</#if>
	<!-- =========== Begin Javascript ================ --> 
   	<script src="${js_path!}/preloader.js"></script>
	<script src="${js_path!}/jquery.dataTables.min.js"></script>
	<script src="${js_path!}/datatable/dataTables.buttons.min.js"></script>
	<script src="${js_path!}/datatable/buttons.flash.min.js"></script>
	<script src="${js_path!}/datatable/jszip.min.js"></script>
	<script src="${js_path!}/datatable/pdfmake.min.js"></script>
	<script src="${js_path!}/datatable/vfs_fonts.js"></script>
	<script src="${js_path!}/datatable/buttons.html5.min.js"></script>
	<script src="${js_path!}/datatable/buttons.print.min.js"></script>
	<script src="http://rawgithub.com/aamirafridi/jQuery.Marquee/master/jquery.marquee.min.js?v=4"type="text/javascript"></script>
</#compress>
</#macro>
