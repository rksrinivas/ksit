<#macro addNBAMacro>

	<div class="mymodal fade" id="addNBADataModal" data-backdrop="false" data-keyboard="true" tabindex="-1">
	    <div class="modal-dialog">
	        <div class="modal-content flat-style">
	          <div class="modal-header" style ="background-color: #fd3753;">
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 10px;"><i class="icon icon-remove-circle"></i></button>
	            <div class="row">
	              <div class="col-xs-9" style="margin-top: 4px;margin-left: 13px;">
	                <h4 class="modal-title" id="addTextTitle">Add</h4>
	              </div>
	            </div>
	          </div><!-- /.modal-header -->
	          <div class="modal-body">
	          
		          <form action="/admin/nba_previsit" method="post" enctype="multipart/form-data">
		          	<input type="hidden" name="parentId" id="nbaModalParentId" value="" />		         
					<label for="page">Page</label>
					<select class="wp-form-control wpcf7-text" name="page" required>
						<option value="institution_page">Institution</option>						
						<option value="cse_page">Computer Science and Engineering</option>
						<option value="ece_page">Electronics and Communication Engineering</option>
						<option value="mech_page">Mechanical Engineering</option>						
					</select>
					
					<label for="heading">Heading</label>
					<div class="wpcf7-text form-group"> 
						<input type="text" name="heading">
					</div>
					
					<label for="description">Description</label>
					<div class="wpcf7-text form-group">
						<input type="text" name="description">
					</div>
					
					<h1>Select File or Enter Link</h1>
					<label for="link">Link</label>
					<div class="wpcf7-text form-group"> 
						<input type="text" name="link">
					</div>
					
					<label for="file">File</label>				
					<div class="wpcf7-text form-group">
						<input type="file" name="file">
					</div>
					
					<label for="rank">Rank</label>				
					<div class="wpcf7-text form-group"> 
						<input type="number" name="rank" min="1" value="1">
					</div>
					
					<button class="btn btn-primary" type="submit" value="Submit">Save</button>
				</form>
	          
	          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</#macro>			