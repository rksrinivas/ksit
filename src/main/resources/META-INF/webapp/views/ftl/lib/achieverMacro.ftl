<#macro achieverMacro achieverList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Content</th>
			<th>Image</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>			
		</thead>
		<tbody>
			<#if achieverList?has_content>
				<#list achieverList as achiever>
					<tr>
						<td>
							<span id="${achiever.id!}Content">${achiever.content!}</span>
						</td>
						<td>
							<span id="${achiever.id!}Image">
								<#if achiever.imageURL?has_content>
									<a target="_blank"  href="${achiever.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						<td>
							<span id="${achiever.id!}Active">${achiever.active?string!}</span>
						</td>
						<td>
							<span id="${achiever.id!}Rank">
								<#if achiever.rank?has_content>
								${achiever.rank?string!}
								</#if>
							</span>
						</td>	
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editAchiversModal" data-id=${achiever.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteAchievers('${achiever.id!}');">
								Delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Content</th>
			<th>Image</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>	
		</tfoot>
	</table>
</#macro>	