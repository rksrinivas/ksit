<#macro feedbackStudentMacro feedbackStudentList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th></th>
			<th>Student Name</th>
			<th>Semeseter & Section</th>
			<th>Academic Year</th>
			<th>Usn</th>
			<th>Email Id</th>
			<th>Phone No</th>
			<th>Download</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<#if feedbackStudentList?has_content>
				<#list feedbackStudentList as feedbackStudent>
					<tr>
						<td><a href="/admin/feedbackStudent/${feedbackStudent.id}"  target="_blank">View</a></td>
						<td>${feedbackStudent.nameOfTheStudent}</td>
						<td>${feedbackStudent.semesterAndSection}</td>
						<td>${feedbackStudent.academicYear}</td>
						<td>${feedbackStudent.usn}</td>
						<td>${feedbackStudent.emailId}</td>
						<td>${feedbackStudent.phoneNo}</td>
						<td>
							<a href="/admin/feedbackStudent/${feedbackStudent.id}?download=true" target="_blank">Download</a>
						</td>
						<td>
							<form action="/admin/feedbackStudent/delete/${feedbackStudent.id}" method="post">
								<button type="submit" value="Submit">Delete</button>
							</form>
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
</#macro>