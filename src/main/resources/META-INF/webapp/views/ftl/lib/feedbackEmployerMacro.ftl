<#macro feedbackEmployerMacro feedbackEmployerList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th></th>
			<th>Name</th>
			<th>Company Name</th>
			<th>Designation</th>
			<th>Mobile</th>
			<th>Email Id</th>
			<th>Download</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<#if feedbackEmployerList?has_content>
				<#list feedbackEmployerList as feedbackEmployer>
					<tr>
						<td><a href="/admin/feedbackEmployer/${feedbackEmployer.id}" target="_blank">View</a></td>
						<td>${feedbackEmployer.name}</td>
						<td>${feedbackEmployer.companyName}</td>
						<td>${feedbackEmployer.designation}</td>
						<td>${feedbackEmployer.mobileNumber}</td>
						<td>${feedbackEmployer.emailId}</td>
						<td>
							<a href="/admin/feedbackEmployer/${feedbackEmployer.id}?download=true" target="_blank">Download</a>
						</td>
						<td>
							<form action="/admin/feedbackEmployer/delete/${feedbackEmployer.id}" method="post">
								<button type="submit" value="Submit">Delete</button>
							</form>
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
</#macro>