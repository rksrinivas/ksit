<#macro nbaMacro nbaPrevisitList>
	<table class="table table-striped table-bordered" style="width=100%">
		<tr>
			<th>
				Details
			</th>
			<th>
				Sub List
			</th>
			<th>
				Action
			</th>
		</tr>
		<#list nbaPrevisitList as nbaPrevisit>
			<tr>
				<td>
					Page: ${nbaPrevisit.page}
					<br />
					Heading: ${nbaPrevisit.heading}
					<br />
					Description: ${nbaPrevisit.description}
					<br />
					Link: ${nbaPrevisit.link}
					<br />
					Rank: ${nbaPrevisit.rank}
					<br />
				</td>
				<td>
					<#if nbaPrevisit.subPrevisitList?has_content>
						<@nbaMacro nbaPrevisitList=nbaPrevisit.subPrevisitList/>
					</#if>
				</td>
				<td>
					<form action="/admin/nba_previsit/delete/${nbaPrevisit.id}" method="post">
						<button type="submit" value="Submit">Delete</button>						
					</form>
					<br />
					<a href="#" class="tooltips" data-toggle="modal" data-target="#addNBADataModal" data-parent="${nbaPrevisit.id}">
						Add NBA Detail
					</a>
				</td>
			</tr>
		</#list>
	</table>
</#macro>