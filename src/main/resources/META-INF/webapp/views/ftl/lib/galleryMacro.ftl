<#macro galleryMacro galleryList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			
			<th>Heading</th>
			<th>Image</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>delete</th>
			
		</thead>
		<tbody>
			<#if galleryList?has_content>
				<#list galleryList as gallery>
					<tr>
					
						<td>
							<span id="${gallery.id!}Heading">${gallery.heading!}</span>
						</td>
						
						<td>
							<span id="${gallery.id!}Image">
								<#if gallery.imageURL?has_content>
									<a target="_blank"  href="${gallery.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						<td>
							<span id="${gallery.id!}Active">${gallery.active?string!}</span>
						</td>
						<td>
							<span id="${gallery.id!}Rank">
								<#if gallery.rank?has_content>
									${gallery.rank?string!}
								</#if>
							</span>
						</td>
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editGalleryModal" data-id=${gallery.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteGallery('${gallery.id!}');">
								delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			
			<th>Heading</th>
			<th>Image</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>delete</th>
			
			
		</tfoot>
	</table>
</#macro>	