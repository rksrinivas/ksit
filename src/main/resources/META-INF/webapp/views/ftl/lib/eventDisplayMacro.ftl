<#macro eventDisplayMacro eventList>
	<#list eventList as event>
		<ul class="marquee_list">
			<li>
				<h5 class="marquee_list_head">${event.heading!}</h5>
				<p>${event.content!}</p>
				<#if (event.reportURL)??>
					<p>
						<a class="read-more" href="${event.reportURL!}" target="_blank">view report</a>
					</p>
				</#if>
				<#if (event.imageURL)??>
					<p>
						<a class="read-more" href="${event.imageURL!}" target="_blank">view more</a>
					</p>
				</#if>
			</li>										
		</ul>				
	</#list>
</#macro>