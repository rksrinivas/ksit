<#macro feedbackAlumniMacro feedbackAlumniList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th></th>
			<th>Student Name</th>
			<th>Year of Graduation</th>
			<th>Position</th>
			<th>Usn</th>
			<th>Email Id</th>
			<th>Address</th>
			<th>Mobile</th>
			<th>Current Organization</th>
			<th>Designation</th>
			<th>Download</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<#if feedbackAlumniList?has_content>
				<#list feedbackAlumniList as feedbackAlumni>
					<tr>
						<td><a href="/admin/feedbackAlumni/${feedbackAlumni.id}"  target="_blank">View</a></td>
						<td>${feedbackAlumni.studentName}</td>
						<td>${feedbackAlumni.yearOfGraduation}</td>
						<td>${feedbackAlumni.positionInGraduation}</td>
						<td>${feedbackAlumni.usn}</td>
						<td>${feedbackAlumni.emailId}</td>
						<td>${feedbackAlumni.address}</td>
						<td>${feedbackAlumni.mobileNumber}</td>
						<td>${feedbackAlumni.currentOrganisation}</td>
						<td>${feedbackAlumni.designation}</td>
						<td>
							<a href="/admin/feedbackAlumni/${feedbackAlumni.id}?download=true" target="_blank">Download</a>
						</td>
						<td>
							<form action="/admin/feedbackAlumni/delete/${feedbackAlumni.id}" method="post">
								<button type="submit" value="Submit">Delete</button>
							</form>
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
</#macro>