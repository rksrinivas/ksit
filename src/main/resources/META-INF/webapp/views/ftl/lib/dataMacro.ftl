<#macro dataMacro dataList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Content</th>
			<th>Active</th>
			<th>Rank</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<#if dataList?has_content>
				<#list dataList as data>
					<tr>
						<td>
							<span id="${data.id!}Content">${data.content!}</span>
						</td>
						<td>
							<span id="${data.id!}Active">${data.active?string!}</span>
						</td>
						<td>
							<span id="${data.id!}Rank"><#if data.rank?has_content>${data.rank?string!}</#if></span>
						</td>
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editDataModal" data-id=${data.id!}>
								Edit
							</a>
						</td>
						<td>
							<a href="#" class="tooltips" onclick="deleteData('${data.id!}');">
								Delete
							</a>
						</td>
						
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Content</th>
			<th>Active</th>
			<th>Rank</th>
			<th>Edit</th>
			<th>Delete</th>
		</tfoot>
	</table>
</#macro>