<#macro deptLabMacro deptLabList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Name</th>
			<th>description</th>
			<th>Image</th>
			<th>Add File</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
			
		</thead>
		<tbody>
			<#if deptLabList?has_content>
				<#list deptLabList as deptLab>
					<tr>
					
						<td>
							<span id="${deptLab.id!}Name">${deptLab.name!}</span>
						</td>
												
											
						<td>
							<span id="${deptLab.id!}Designation">${deptLab.designation!}</span>
						</td>
						
						<td>
							<span id="${deptLab.id!}Image">
								<#if deptLab.imageURL?has_content>
									<a target="_blank"  href="${deptLab.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						<td>
							<span id="${deptLab.id!}Profile">
								<#if deptLab.profileURL?has_content>
									<a target="_blank"  href="${deptLab.profileURL!}">View Profile</a>
								</#if>
							</span>
						</td>
						
						<td>
							<span id="${deptLab.id!}Active">${deptLab.active?string!}</span>
						</td>
						<td>
							<span id="${deptLab.id!}Rank">
								<#if deptLab.rank?has_content>
									${deptLab.rank?string!}
								</#if>
							</span>
						</td>
						
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editDeptLabModal" data-id=${deptLab.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteDeptLab('${deptLab.id!}');">
								Delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Name</th>
			<th>description</th>
			<th>Image</th>
			<th>Add File</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
		</tfoot>
	</table>
</#macro>	