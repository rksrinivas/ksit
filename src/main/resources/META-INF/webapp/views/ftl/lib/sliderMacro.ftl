<#macro sliderMacro sliderImageList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Image</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
			
		</thead>
		<tbody>
			<#if sliderImageList?has_content>
				<#list sliderImageList as sliderImage>
					<tr>
					
						<td>
							<span id="${sliderImage.id!}Heading">${sliderImage.heading!}</span>
						</td>
								
						<td>
							<span id="${sliderImage.id!}Content">${sliderImage.content!}</span>
						</td>
						<td>
							<span id="${sliderImage.id!}Image">
								<#if sliderImage.imageURL?has_content>
									<a target="_blank"  href="${sliderImage.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						<td>
							<span id="${sliderImage.id!}Active">${sliderImage.active?string!}</span>
						</td>
						<td>
							<span id="${sliderImage.id!}Rank">
								<#if sliderImage.rank?has_content>
									${sliderImage.rank?string!}
								</#if>
							</span>
						</td>
						
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editSliderModal" data-id=${sliderImage.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteTestimonial('${sliderImage.id!}');">
								Delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Image</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
		</tfoot>
	</table>
</#macro>	