<#macro feedbackParentMacro feedbackParentList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th></th>
			<th>Name</th>
			<th>Student Name</th>
			<th>Relationship</th>
			<th>Email Id</th>
			<th>Phone</th>
			<th>Occupation</th>
			<th>Semester</th>
			<th>Academic Year</th>
			<th>Usn</th>
			<th>Download</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<#if feedbackParentList?has_content>
				<#list feedbackParentList as feedbackParent>
					<tr>
						<td><a href="/admin/feedbackParent/${feedbackParent.id}" target="_blank">View</a></td>
						<td>${feedbackParent?index+1}</td>
						<td>${feedbackParent.nameOfTheParent}</td>
						<td>${feedbackParent.studentName}</td>
						<td>${feedbackParent.relationship}</td>
						<td>${feedbackParent.emailId}</td>
						<td>${feedbackParent.phoneNo}</td>
						<td>${feedbackParent.occupation}</td>
						<td>${feedbackParent.semesterAndSection}</td>
						<td>${feedbackParent.academicYear}</td>
						<td>${feedbackParent.usn}</td>
						<td>
							<a href="/admin/feedbackParent/${feedbackParent.id}?download=true" target="_blank">Download</a>
						</td>
						<td>
							<form action="/admin/feedbackParent/delete/${feedbackParent.id}" method="post">
								<button type="submit" value="Submit">Delete</button>
							</form>
						</td>
					</tr>
				</#list>
			</#if>
		</tbody>
	</table>
</#macro>