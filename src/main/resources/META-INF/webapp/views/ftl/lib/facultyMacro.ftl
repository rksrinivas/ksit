<#macro facultyMacro facultyList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Name</th>
			<th>Designation</th>
			<th>Qualification</th>
			<th>Department</th>
			<th>Image</th>
			<th>Profile</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
			
		</thead>
		<tbody>
			<#if facultyList?has_content>
				<#list facultyList as faculty>
					<tr>
					
						<td>
							<span id="${faculty.id!}Name">${faculty.name!}</span>
						</td>
								
						<td>
							<span id="${faculty.id!}Designation">${faculty.designation!}</span>
						</td>
						
						<td>
							<span id="${faculty.id!}Qualification">${faculty.qualification!}</span>
						</td>
						
						<td>
							<span id="${faculty.id!}Department">${faculty.department!}</span>
						</td>
						
						<td>
							<span id="${faculty.id!}Image">
								<#if faculty.imageURL?has_content>
									<a target="_blank"  href="${faculty.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						<td>
							<span id="${faculty.id!}Profile">
								<#if faculty.profileURL?has_content>
									<a target="_blank"  href="${faculty.profileURL!}">View Profile</a>
								</#if>
							</span>
						</td>
						
						<td>
							<span id="${faculty.id!}Active">${faculty.active?string!}</span>
						</td>
						<td>
							<span id="${faculty.id!}Rank">
								<#if faculty.rank?has_content>
									${faculty.rank?string!}
								</#if>
							</span>
						</td>
						
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editFacultyModal" data-id=${faculty.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteFaculty('${faculty.id!}');">
								Delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Name</th>
			<th>Designation</th>
			<th>Qualification</th>
			<th>Department</th>
			<th>Image</th>
			<th>Profile</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
		</tfoot>
	</table>
</#macro>	