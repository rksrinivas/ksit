<#macro placementCompanyMacro placementCompanyList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Company</th>
			<th>Salary / Package</th>
			<th>No. of Students</th>
			<th>Year</th>
			<th>Company Logo</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
			
		</thead>
		<tbody>
			<#if placementCompanyList?has_content>
				<#list placementCompanyList as placementCompany>
					<tr>
					
						<td>
							<span id="${placementCompany.id!}Company">${placementCompany.company!}</span>
						</td>
								
						<td>
							<span id="${placementCompany.id!}Salary">${placementCompany.salary!}</span>
						</td>
						
						<td>
							<span id="${placementCompany.id!}noOfStudents">${placementCompany.noOfStudents!}</span>
						</td>
						<td>
							<span id="${placementCompany.id!}year">${placementCompany.year!}</span>
						</td>
						<td>
							<span id="${placementCompany.id!}Rank">
								<#if placementCompany.rank?has_content>
									${placementCompany.rank?string!}
								</#if>
							</span>
						</td>
						<td>
							<span id="${placementCompany.id!}Active">${placementCompany.active?string!}</span>
						</td>
						
						<td>
							<span id="${placementCompany.id!}Image">
								<#if placementCompany.imageURL?has_content>
									<a target="_blank"  href="${placementCompany.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editPlacementModal" data-id=${placementCompany.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deletePlacement('${placementCompany.id!}');">
								Delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Company</th>
			<th>Salary / Package</th>
			<th>No. of Students</th>
			<th>Year</th>
			<th>Company Logo</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
		</tfoot>
	</table>
</#macro>	