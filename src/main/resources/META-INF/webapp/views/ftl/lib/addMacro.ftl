<#macro addMacro modalId showName=false showDesignation=false showQualification=false showDepartment=false showHeading=false
 showContent=false showEventDate=false showActive=false showRank=false showImage=false showReport=false showProfile=false
  showCompany=false showSalary=false showNumberOfStudents=false showYear=false showProgram=false showSemester=false showSchedule=false>
  
  <div class="mymodal fade" id="${modalId}" data-backdrop="false" data-keyboard="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content flat-style">
          <div class="modal-header" style ="background-color:#0645ad;">
            <button type="button" title="Close" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top:10px;font-size:inherit;text-shadow:0 0 5px #0000004a;"><i class="fa fa-close"></i></button>
            <div class="row">
              <div class="col-xs-9" style="margin-top:4px;margin-left:0px;">
                <h4 class="modal-title" id="addTextTitle"><i class="fa fa-pencil-square-o"></i> Add</h4>
              </div>
            </div>
          </div><!-- /.modal-header -->
          <div class="modal-body" style="padding:7px;">
            
            <input type="hidden" name="page" id="pageAddValue${modalId}" />
            <input type="hidden" name="fieldName" id="fieldNameAddValue${modalId}" />
            
            <#if showName?has_content && showName>
              <div>
                <h3>Name</h3>
                <input type="text" name="name" id="nameAddValue${modalId}" />
              </div>
            </#if>
            
            <#if showDesignation?has_content && showDesignation>
              <div> 
                <h3>Designation / Description</h3>
                <input type="text" name="designation" id="designationAddValue${modalId}" />
              </div>
            </#if>  
            
            <#if showQualification?has_content && showQualification>
              <div>
                <h3>Qualification</h3>
                <input type="text" name="qualification" id="qualificationAddValue${modalId}" />
              </div>
            </#if>    
            
            <#if showDepartment?has_content && showDepartment>
              <div>
                <h3>Department</h3>
                <input type="text" name="department" id="departmentAddValue${modalId}" />
              </div>
            </#if>
            
            <#if showHeading?has_content && showHeading>  
              <div> 
                <h3>Heading</h3>
                <input type="text" name="heading" id="headingAddValue${modalId}" />
              </div>
            </#if>  
              
            <#if showContent?has_content && showContent>  
              <div>
                <h3>Content</h3>
                <textarea placeholder="" name="content" id="contentAddValue${modalId}" style="width: 100%;"></textarea>
              </div>
            </#if>  
            
            <#if showEventDate?has_content && showEventDate>
              <div>
                <h3>Event Date</h3>
                <input type="date" name="eventDate" id="eventDateAddValue${modalId}" />
              </div>
            </#if>
            
            <#if showActive?has_content && showActive>
              <div>
                <h3>Active</h3>
                <select id="activeAddValue${modalId}" name="active">
                  <option value="true">True</option>
                  <option value="false">False</option>
                </select>
              </div>
            </#if>
            
            <#if showRank?has_content && showRank>
              <#setting number_format="0" />
              <div>
                <h3>Rank</h3>
                <input type="number" min="1" id="rankAddValue${modalId}" name="rank" value="1"/>
              </div>
            </#if>
            
            <#if showImage?has_content && showImage>
              <div>
                <h3>Image</h3>
                <input type="file" name="image" id="imageAddValue${modalId}" />
              </div>
            </#if>
            
	          <#if showReport?has_content && showReport>
	          	<div>
	            <h3>Report</h3>
	            <input type="file" name="image" id="reportAddValue${modalId}" />
	          </div>
            </#if>    
              
            <#if showProfile?has_content && showProfile>    
              <div>
                <h3>Profile</h3>
                <input type="file" name="profile" id="profileAddValue${modalId}" />
              </div>
            </#if>  
            
            
            <#if showCompany?has_content && showCompany>
	            <div>
	                <h3>Company / Vendor</h3>
	                <input type="text" name="company" id="companyAddValue${modalId}" />
	              </div>
            </#if>  
            
            
            
             <#if showSalary?has_content && showSalary>
	            <div>
	                <h3>Salary / Package</h3>
	                <input type="text" name="salary" id="salaryAddValue${modalId}" />
	              </div>
            </#if>
            
             <#if showNumberOfStudents?has_content && showNumberOfStudents>
             	 <#setting number_format="0" />
	            <div>
	                <h3>Number of Students</h3>
	                <input type="text" name="noOfStudents" id="noOfStudentsAddValue${modalId}" />
	              </div>
            </#if>
            
            
            <#if showYear?has_content && showYear>
	            <div>
	                <h3>Year / Batch</h3>
	                <input type="text" name="year" id="yearAddValue${modalId}" />
	            </div>
            </#if>
            
            <#if showProgram?has_content && showProgram>
            	<div>
	                <h3>Training Program</h3>
	                <input type="text" name="program" id="programAddValue${modalId}" />
	            </div>
            
            </#if>
            
			            
            <#if showSemester?has_content && showSemester>
	            <div>
	                <h3>Semester</h3>
	                <input type="number" name="semester" id="semesterAddValue${modalId}" />
	              </div>
            </#if>
            
            <#if showSchedule?has_content && showSchedule>
	            <div>
	                <h3>Schedule / Date</h3>
	                <input type="text" name="schedule" id="scheduleAddValue${modalId}" />
	              </div>
            </#if>
            
            
            <div style="margin-top:10px;margin-bottom:0px">
              <button type="button" class="btn btn-primary" id="cancelAdd${modalId}">Cancel</button>
            <button type="submit" class="btn btn-success" id="saveAdd${modalId}" >Save</button>
            </div>            
              
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</#macro> 