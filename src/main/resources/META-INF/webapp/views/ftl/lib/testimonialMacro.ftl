<#macro testimonialMacro testimonialList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Image</th>
			<th>Report</th>
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<#if testimonialList?has_content>
				<#list testimonialList as testimonial>
					<tr>
					
						<td>
							<span id="${testimonial.id!}Heading">${testimonial.heading!}</span>
						</td>
								
						<td>
							<span id="${testimonial.id!}Content">${testimonial.content!}</span>
						</td>
						<td>
							<span id="${testimonial.id!}Image">
								<#if testimonial.imageURL?has_content>
									<a target="_blank"  href="${testimonial.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						<td>
							<span id="${testimonial.id!}Report">
								<#if testimonial.reportURL?has_content>
									<a target="_blank"  href="${testimonial.reportURL!}">View Report</a>
								</#if>
							</span>
						</td>
						<td>
							<span id="${testimonial.id!}Active">${testimonial.active?string!}</span>
						</td>
						<td>
							<span id="${testimonial.id!}Rank">
								<#if testimonial.rank?has_content>
									${testimonial.rank?string!}
								</#if>
							</span>
						</td>
						
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editTestimonialModal" data-id=${testimonial.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteTestimonial('${testimonial.id!}');">
								Delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Image</th>
			<th>Report</th>				
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>Delete</th>
		</tfoot>
	</table>
</#macro>	