<div class="container" style="background-color:white;min-height:600px;margin-top: 20px;">
	<table class="table table-striped table-bordered">
		<tr>
			<th>Department</th>
			<th>Syllabus & Time Table</th>
			<th>Action</th>
		</tr>
		<#list programmeList as programme>
			<tr>
				<td>
					${programme.department!}
				</td>
				<td>
					<#list programmeDataTypes as dataType>
						${dataType.desc}
						<br />
						<ul>
						<#list programme.programmeDataSet as programmeData>
							<#if programmeData.programmeDataType == dataType>
								<span>
									<a href="${programmeData.data}">${programmeData.name}</a>
									<button type="submit" class="btn btn-danger" style="margin-left:100px; margin-top:10px;" onclick="deleteSyllabusData('${programmeData.id}');">Delete Programme Data</button>							
								</span>
							</#if>
						</#list>
						</ul>
					</#list>
				</td>
				<td>
					<button type="button" class="btn btn-info btn-lg" data-programme_id="${programme.id}" data-toggle="modal" data-target="#addProgrammeDataModal">Add Data</button>					
					<br />
					<br />
					<button type="submit" class="btn btn-danger" onclick="deleteProgrammeData('${programme.id}');">Delete Programme</button>
				</td>
			</tr>
		</#list>
	</table>
	<button type="button" class="btn btn-info btn-lg" data-type="${programmeDataType}" data-toggle="modal" data-target="#addProgrammeModal">Add Programme</button>
</div>