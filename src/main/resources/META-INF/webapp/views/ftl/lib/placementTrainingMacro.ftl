<#macro placementTrainingMacro placementTrainingList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
		
			<th>Program</th>
			<th>Semester</th>
			<th>Year/Batch</th>
			<th>Vendor/Company</th>
			<th>Schedule/Date</th>
			<th>Active</th>
			<th>Rank</th>
			<th>Edit</th>
			<th>Delete</th>
			
		</thead>
		<tbody>
			<#if placementTrainingList?has_content>
				<#list placementTrainingList as placementTraining>
					<tr>
					
														
						<td>
							<span id="${placementTraining.id!}Program">${placementTraining.program!}</span>
						</td>
						
						<td>
							<span id="${placementTraining.id!}Semester">${placementTraining.semester!}</span>
						</td>
						
						<td>
							<span id="${placementTraining.id!}Year">${placementTraining.year!}</span>
						</td>
						
						<td>
							<span id="${placementTraining.id!}Company">${placementTraining.company!}</span>
						</td>
						
						<td>
							<span id="${placementTraining.id!}Schedule">${placementTraining.schedule!}</span>
						</td>
						
						
						<td>
							<span id="${placementTraining.id!}Active">${placementTraining.active?string!}</span>
						</td>
						<td>
							<span id="${placementTraining.id!}Rank">
								<#if placementTraining.rank?has_content>
									${placementTraining.rank?string!}
								</#if>
							</span>
						</td>
											
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editPlacementTrainingModal" data-id=${placementTraining.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deletePlacementTraining('${placementTraining.id!}');">
								Delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">			
			<th>Program</th>
			<th>Semester</th>
			<th>Year/Batch</th>
			<th>Vendor/Company</th>
			<th>Schedule/Date</th>
			<th>Active</th>
			<th>Rank</th>
			<th>Edit</th>
			<th>Delete</th>
		</tfoot>
	</table>
</#macro>	