<#macro page>
<#compress>
<!doctype html>
<html lang="en">
<!--======== Begin HTML of KSIT ============= -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="theme-color" content="#1e2223">
<title>KSIT | Best Engineering Colleges in Bangalore</title>
<!-- ===================== Mobile Specific Metas ======================== -->
<!--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="description" content="The Kammavari Sangham, a multi-activity non-profit oriented voluntary service organization, 
   was established in the year 1952 with the sole objective of providing charitable service to community and society.">
<!-- Favicon -->
<link rel="shortcut icon" type="image/icon" href="${img_path!}/wpf-favicon.png"/>
<!-- ===================== CSS ============================= --> 
<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
<!-- ===================== Bootstrap file ===================== --> 
<link href="${css_path!}/bootstrap.min.css" type="text/css" rel="stylesheet">
<!-- ===================== Gallery slider ===================== -->
<link type="text/css" media="all" rel="stylesheet" href="${css_path!}/jquery.tosrus.all.css" />
<link href="${css_path!}/slider_fade.css" rel="stylesheet">
<!-- ===================== Font awesome ===================== -->
<link href="${css_path!}/font-awesome.min.css" rel="stylesheet">
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<!-- ===================== Material Icons ===================== -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- ===================== Uperslide ===================== -->
<link href="${css_path!}/superslides.css" rel="stylesheet" >
<!-- ===================== Slick slider ===================== -->
<link href="${css_path!}/slick.css" rel="stylesheet">
<!-- ===================== Circle counter cdn ===================== -->
<link rel='stylesheet prefetch' href='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/css/jquery.circliful.css'>
<!-- ===================== Smooth animate ===================== -->
<link rel="stylesheet" href="${css_path!}/animate.css">
<!--<link rel="stylesheet" href="${css_path!}/queryLoader.css" type="text/css" />-->
<!-- ===================== Gallery slider ===================== -->
<link type="text/css" media="all" rel="stylesheet" href="${css_path!}/jquery.tosrus.all.css" />
<!-- ===================== Default Theme ===================== -->
<link id="switcher" href="${css_path!}/themes/default-theme.css" rel="stylesheet">
<link href="${css_path!}/achievers_testi.css" rel="stylesheet">
<!-- ===================== style.css files ===================== -->
<link href="${css_path!}/style${timestamp!}.css" rel="stylesheet">
<link href="${css_path!}/style_new${timestamp!}.css" rel="stylesheet">
<link href="${css_path!}/stellarnav${timestamp!}.css" rel="stylesheet">
<link href="${css_path!}/slider_caption${timestamp!}.css" rel="stylesheet">
<link href="${css_path!}/testimonial${timestamp!}.css" rel="stylesheet">
<link href="${css_path!}/slider${timestamp!}.css" rel="stylesheet">
<!-- ===================== Placement slider ===================== -->
<link rel="stylesheet" href="${css_path!}/jquery.dataTables.min.css" type='text/css' />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">

<!-- ===================== Programs Block ===================== -->
<link href="${css_path!}/program_styles${timestamp!}.css" rel="stylesheet">

<!-- ===================== life at KSIT  Block ===================== -->
<link href="${css_path!}/home${timestamp!}.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="https://chrisbin95.github.io/Portfolio/"></script>
	<![endif]-->
<script src="${js_path!}/jquery-3.2.1.min.js"></script>
<!-- ===================== END CSS ============================= --> 
</head>
<body>
<!--=========== BEGIN HEADER SECTION ================-->
    <div class="main">
        <p class="code" title="CET | COMED-K">College Code</p>
        <div class="coll_code">
            <p class="cet">CET : E091</p>
            <br/>            
            <p class="comed">COMED-K : E068</p>
        </div>
         <p><i class="fa fa-bell" id="bell-icon"></i> Notification <span style="color:#f69c42;">|</span> </p>
         <a class="online_fees" href="https://easypay.axisbank.co.in/easyPay/makePayment?mid=MjYyODM%3D" target="_blank" title="Pay Now">Online Fee Payment</a>
        <div class="news">
            <marquee class="scrolling-text" scrollamount="3" onmouseover="stop();"  onmouseout="start();">
               <p>
                  ${scrollingTextTop!}
               </p>
            </marquee>
        </div>
    </div>
      
        <div class="headertext" title="K S Institute of Technology (KSIT)">
			<div id="header-responsible">
                  <p class="head_name"><b>Kammavari Sangham (R) 1952, K.S.Group of Institutions</b></p>
                  <p class="head_address headergap text-shadow-head"><span><b>K. S. INSTITUTE OF TECHNOLOGY</span></b></p>
				  <p class="head_text headergap"><i class="fa fa-map-marker"></i>&nbsp;No.14, Raghuvanahalli, Kanakapura Road, Bengaluru - 560109, <a class="head_text headergap" href="tel:+91 9900710055" title="Call Now"><i id="ring-header" class="fa fa-phone"></i>&nbsp;9900710055</a></p>
                  <p class="head_text headergap">Affiliated to VTU, Belagavi &amp; Approved by AICTE, New Delhi, <span style="color:red;">Accredited by NBA (CSE & ECE) & NAAC A+</span></p>
				   <a href="index.html" style="text-decoration:none;"><img src="${img_path!}/sangam.jpeg" alt="logo" class="logoImage" id="sangamlogo"> </a>
               	  <a href="index.html" style="text-decoration:none;"><img src="${img_path!}/leftlogo.png" alt="logo" class="logoImage" id="leftlogo"> </a>
				  
				  
          		  <img src="${img_path!}/rightlogo_12.jpg" alt="logo" id="rightlogo" class="logoImage fright">
		  	</div>
   		</div>
		<!-- Admission Enquiry on Right Side-->
   		<div id="admissions_open">
			<a class="admissions-link" href="coursesOffered.html" target="_blank">Admission Enquiry</a>
			<a id="ring" class="admissions-link" href="tel:+919900710055" title="Call Now!"><i id="ring-header" class="fa fa-phone"></i></a>
		</div>
   	<!-- ===================== New navbar ===================== -->
   	<div class="header">
		<!--<a href="#"></a>-->
	<div class="stellarnav"  id="stellarnav">
		<ul class="menu">
			<li><a href="index.html">Home</a></li>
			<li><a href="#">About</a>
		    	<ul class="childmenu">
					<li><a href="about.html">History</a></li>
                    <li><a href="administration.html">Management</a></li>
                    <li><a href="academic_advisory_board.html">Academic Advisory Board</a></li>
                    <li><a href="ceo.html">CEO</a></li>
                    <li><a href="principal.html">Principal and Director</a></li>
                    <li><a href="directorAcademics.html">Director - ADM &amp; PRO</a></li>
                    <li><a href="academic_governing_council.html">Academic Governing Council</a></li>
					<li><a href="iiic">Industry Institute Interaction Cell(IIIC)</a></li>   			    		                       
                    <li><a href="office_administration.html">Office Administration</a></li>
                    <li><a href="right_to_information.html">Right To Information</a></li>
                    <li><a href="${img_path!}/about/Mandatory_2025-26.pdf" target="_blank">Mandatory Disclosure</a></li>
                    <li><a href="${img_path!}/about/codeof_conduct.pdf" target="_blank">Code of Conduct</a></li>		
					<li><a href="${img_path!}/about/staff_handbook_2018_new.pdf" target="_blank">Staff Handbook</a></li>
					<li><a href="${img_path!}/about/Audit_5yrs_24.pdf" target="_blank">Audit Statement</a></li>	
					<li ><a href="#" aria-current="page">Annual Report</a>
					 	<ul class="childmenu" style="float:left;">				

							<li><a href="${img_path!}/about/annual_Report_2021-22.pdf" target="_blank">2021-2022</a></li>
							<li><a href="${img_path!}/about/annual_Report_2022-23.pdf" target="_blank">2022-2023</a></li>
							<li><a href="${img_path!}/about/annual_Report_2023-24.pdf" target="_blank">2023-2024</a></li>
							
						</ul>
					</li>
						
					<li><a href="${img_path!}/about/KSIT_Organogram2023-24.pdf" target="_blank">Organogram</a></li>
					<li><a href="institutionalGovernance">Institutional governance</a></li>
					<li><a href="approvals">Affiliations & Approvals</a></li>						                       		                       
		    	</ul>
		    </li>	
		    
		 <!--	<li><a href="#">Programmes</a>
		    	<ul class="childmenu">
		    		<li><a href="UG.html">UG Program</a></li>
			    	<li><a href="PG.html">PG program</a></li>
			    	<li><a href="#" aria-current="page">Research program</a>
			    		<ul>
			    			<li><a href="cse_dept.html">Computer Science &amp; Engineering</a></li>
				    		<li><a href="ece_dept.html">Electronics &amp; Communication Engineering</a></li>
				    		<li><a href="mech_dept.html">Mechanical Engineering</a></li>
			    		</ul>
			    	</li>
			    	<li><a href="research_development.html">R &amp; D</a></li>
		  	   	</ul>
		 	</li>	 	-->
		 
		    <!--<li class="mega" data-column="5"><a href="">Departments</a>
		    	<ul>
		    		<li><a href="#">CSE</a></li>
		    		<li><a href="#">ECE</a></li>
		    		<li><a href="#">MECH</a></li>
		    		<li><a href="#">TELE</a></li>
		    		<li><a href="#">MATHS</a></li>
		    		<li><a href="#">PHYSICS</a></li>
		    		<li><a href="#">CHEMISTRY</a></li>
		    	</ul>
		    </li>-->
		    
		    <li><a href="#">Programmes</a>
		    	<ul class="childmenu">		
				 <li><a href="#" aria-current="page">UG Programmes</a>		    
						<ul class="childmenu">			
								<li><a href="aiml_dept">Artificial Intelligence &amp; Machine Learning Engineering</a></li>
								<li><a href="csd_dept">Computer Science and Design</a></li>
								<li><a href="cce_dept">Computer and Communication Engineering</a></li>
								<li><a href="csicb_dept">Computer Science &amp; Engineering(IoT & Cyber Security including Block Chain Technology)</a></li>
								<li><a href="cse_dept">Computer Science &amp; Engineering</a></li>
								<li><a href="ece_dept">Electronics &amp; Communication Engineering</a></li>
								<li><a href="mech_dept">Mechanical Engineering</a></li>
								<li><a href="science_dept">Science &amp; Humanities</a></li>
						</ul>
					</li>

					<li><a href="#" aria-current="page">PG Programmes</a>		    
						<ul class="childmenu">			
								<li><a href="cse_dept">M.Tech in CSE</a></li>
								<li><a href="mca_dept">MCA</a></li>
								
						</ul>
					</li>
				</ul>
		    </li>
		    
			<li><a href="#">Accreditation</a>		  
				<ul class="childmenu">
					<li><a href="https://drive.google.com/file/d/15TeGzydHE1UMOQHX6GQ0Xn7GsfhlWNK0/view?usp=sharing" target="_blank">IEI</a></li>
						<!-- Complete NAAC -->
						 <li><a href="#" aria-current="page">NAAC Cycle 1</a>		    
		    			<ul class="childmenu">
		    			<li><a href="#" aria-current="page">AQAR</a>
		    				<ul>
		    					<li><a href="/naac">NAAC AQAR</a></li>		    						    					
		    					<li><a href="/img/naac/AQAR-1_submitted_AUG_2020.pdf">AQAR 2018-19</a></li>
								<li><a href="/img/naac/Aqar_report_2019-20.pdf">AQAR 2019-20</a></li>
								<li><a href="/img/naac/AQAR_20-21.pdf">AQAR 2020-21</a></li>
								<li><a href="/img/naac/AQAR_21-22.pdf">AQAR 2021-22</a></li>
								<li><a href="/img/naac/AQAR_22-23.pdf">AQAR 2022-23</a></li>
		    				</ul>
		    			</li>	    

						<!-- NAAC IQAC-->
						<li><a href="#" aria-current="page">IQAC</a>		    						    
		    				<ul class="childmenu">
		    					<li><a href="#" aria-current="page">IQAC Composition</a>
									<ul>
										<li><a href="img/naac/IQAC Composition 2016.17 & 18.pdf" target="_blank">IQAC Composition 2016,17 &amp; 18</a></li>
										<li><a href="img/naac/Revised-List-of-members-2021-22.pdf" target="_blank">IQAC Composition 2021-2022</a></li>
										<li><a href="img/naac/Revised-List-of-members-2022-23.pdf" target="_blank">IQAC Composition 2022-2023</a></li>
									</ul>
								</li>
								<li><a href="img/naac/IQAC details.pdf" target="_blank">IQAC Details</a></li>
								<li><a href="#" aria-current="page">IQAC Minutes of the meeting</a>
									<ul>
										<li><a href="img/naac/IQAC Minutes of the meeting.pdf" target="_blank">2018-2019</a></li>
										<li><a href="img/naac/IQAC - 2019-20.pdf" target="_blank">2019-2020</a></li>
										<li><a href="img/naac/IQAC 2020-21.pdf" target="_blank">2020-2021</a></li>
										<li><a href="img/naac/IQAC 2021-22.pdf" target="_blank">2021-2022</a></li>
										<li><a href="img/naac/IQAC_2022-23_all_3_meetings.pdf" target="_blank">2022-2023</a></li>
		    						</ul>
								</li>
							</ul>
		    			</li>	    

			    		<!-- DVV Clarifications -->
						<li><a href="#" aria-current="page">DVV Clarifications</a>
			    			<ul>
					    		<li><a href="#" aria-current="page">Extended Profile</a>
					    			<ul>
					    				<li><a href="img/naac/ep/1.1 Extended Profile.pdf">1.1</a></li>
	                                    <li><a href="img/naac/ep/1.2 Extended Profile.pdf">1.2</a></li>
	                                    <li><a href="img/naac/ep/2.1.pdf">2.1</a></li>
	                                    <li><a href="img/naac/ep/2.2.pdf">2.2</a></li>
	                                    <li><a href="img/naac/ep/2.3.pdf">2.3</a></li>
	                                    <li><a href="img/naac/ep/3.1.pdf">3.1</a></li>
	                                    <li><a href="img/naac/ep/3.2.pdf">3.2</a></li>
					    				<li><a href="#" aria-current="page">4.1.3</a>
					    					<ul>
					    						<li><a href="https://drive.google.com/file/d/1plwXF8x68T21YaOac5sGAkw0OkJNTVJA/view?usp=sharing" target="_blank">Class rooms and seminar halls with ICT details</a></li>
                                          		<li><a href="https://drive.google.com/file/d/1tk9tF0VbzCKCxfvkz9THZuQAkw7_SqkN/view?usp=sharing" target="_blank">Geo tag photos of class rooms and seminar halls</a></li>
					    					</ul>
					    				</li>
					    				<li><a href="img/naac/ep/4.2_Extended Profile.pdf">4.2</a></li>
					    				<li><a href="#" aria-current="page">4.3.2</a>
					    					<ul>
					    						<li><a href="https://drive.google.com/file/d/1IWxZLXOm_vsvUGHt_wTBHsYW0hijauxx/view?usp=sharing" target="_blank">invoices of the computers purchase</a></li>
		                                        <li><a href="https://drive.google.com/file/d/1fHhID6Zm4ZoJv9v_gU0AOFwkr47zHkat/view?usp=sharing" target="_blank">Annual Stock entry of the Computers</a></li>
		                                        <li><a href="https://drive.google.com/file/d/1HIIMyC3MUOFH3vyMP3_wqRT-7hfaHLHV/view?usp=sharing" target="_blank">Details of the computers with locations</a></li>
		                                    </ul>
							    		</li>
					    				<!--<li><a href="#">Item</a></li>
					    				<li><a href="#">Item</a></li>
					    				<li><a href="#">Item</a></li>-->
					    			</ul>
					    		</li>
					    		
					    		<!-- C1 profile -->
					    		<li><a href="#" aria-current="page">C1</a>
					    			<ul>
					    			<li><a href="img/naac/c1/1.1.3.pdf">1.1.3</a></li>
					    			  	<li><a href="#" aria-current="page">1.2.1</a>
					    			  		<ul>
					    			  		 	<li><a href="https://drive.google.com/drive/folders/1LM5pvVrYgL-gIgiz6qjmjwTMSPK5hbjs" target="_blank">CSE</a></li>
	                                          	<li><a href="https://drive.google.com/drive/folders/18Vw0gNIApHT_0LeBNFPYL5mQ_0GlADWW" target="_blank">ECE</a></li>
	                                          	<li><a href="https://drive.google.com/drive/folders/1wUUvLIZZ3YeZG4kIREW4sq-mpRRpuLgM" target="_blank">ME</a></li>
	                                          	<li><a href="https://drive.google.com/drive/folders/1TPEOVbU650_KfaO8DfcuLLDuehh3SEAI" target="_blank">TE</a></li>
	                                          	<li><a href="https://drive.google.com/drive/folders/17V8Hg5XyX0xv2b6pThXuAEyzAcxY0_-W" target="_blank">1.2.1.1</a></li>
						    				</ul>
					    			  	</li>
					    			<li><a href="https://drive.google.com/drive/folders/1TNj6oRagJXr4ql3On2lpIPBatnmbWKL2? usp=sharing">1.2.2</a></li>
                                    <li><a href="https://drive.google.com/open?id=1_5RcRcdvK2Pjo0TDPwPV1sKUQ-3fmJ16">1.3.2</a></li>
                                    <li><a href="https://drive.google.com/open?id=1MFsr7Jb5mmngBcvyWtyfgClT-USeJWoE">1.4.1</a></li>
					    			</ul>
					    		</li>
					    		<!-- C1 profile -->
					    		
					    		<!-- C2 profile -->
					    		<li><a href="#" aria-current="page">C2</a>
					    			<ul>
					    			  	<li><a href="img/naac/c2/2.1.2.pdf">2.1.2</a></li>
	                                    <li><a href="img/naac/c2/2.1.3.pdf">2.1.3</a></li>
	                                    <li><a href="img/naac/c2/2.3.2.pdf">2.3.2</a></li>
	                                    <li><a href="img/naac/c2/2.4.1.pdf">2.4.1</a></li>
	                                    <li><a href="img/naac/c2/2.6.3.pdf">2.6.3</a></li>
					    			</ul>
					    		</li>
					    		<!-- C2 profile -->
					    		
					    		<!-- C3 profile -->
					    		<li><a href="#" aria-current="page">C3</a>
					    			<ul>
					    			    <li><a href="https://drive.google.com/file/d/1Q4_xl4DHjydzKA3y7Ivc8xjad1rYkcbE/view" target="_blank">c3.1.2</a></li>
                                    	<li><a href="https://drive.google.com/file/d/1NpRvLzlK4PH3WxnZdX-HUKBmtZml6QNT/view?usp=sharing" target="_blank">c3.2.2</a></li>
                                    	<li><a href="https://drive.google.com/file/d/1Q4_xl4DHjydzKA3y7Ivc8xjad1rYkcbE/view" target="_blank">c3.3.3</a></li>
					    			  	<li><a href="#" aria-current="page">c3.3.4</a>
					    			  	<ul>
					    			  	  <li><a href="https://drive.google.com/file/d/1hu4bno-0fYI_MVLsV1lGbfFgU2i5R1ne/view?usp=sharing" target="_blank">2017-18</a></li>
                                          <li><a href="https://drive.google.com/file/d/1eqpwCdGM6qwMKVO1kM8wKhkiu7VCet6Q/view?usp=sharing" target="_blank">2016-17</a></li>
                                          <li><a href="https://drive.google.com/file/d/1j08JHisz6yfGBKEqlIkyCMcfRxFeHrwe/view?usp=sharing" target="_blank">2015-16</a></li>
                                          <li><a href="https://drive.google.com/file/d/1pg7SoB9qWjh_liXKu0Zr1R7AmfFCahFu/view?usp=sharing" target="_blank">2014-15</a></li>
                                          <li><a href="https://drive.google.com/file/d/1nKmIAXHEEh0WtEiQfNHayPHTvPS9wT_X/view?usp=sharing" target="_blank">2013-14</a></li>
					    			  	</ul>
					    		</li>
					    			  
					    		<li><a href="#" aria-current="page">c3.3.5</a>
					    			<ul>
					    			  	<li><a href="https://drive.google.com/file/d/10650AIcNz9HBrv5-ic3ZCXuMIlgkcTX8/view?usp=sharing" target="_blank">2017-18</a></li>
                                        <li><a href="https://drive.google.com/file/d/1-pDISoEnY9sx63O_DVBz7US8upGPKGvk/view?usp=sharing" target="_blank">2016-17</a></li>
                                        <li><a href="https://drive.google.com/file/d/17_4Mua-rm_3LRZ1ChD1uNvPRVlm4HO_J/view?usp=sharing" target="_blank">2015-16</a></li>
                                        <li><a href="https://drive.google.com/file/d/1MIcqQ9JYn7lWhVDumC3IwBIc9cj_ldmg/view?usp=sharing" target="_blank">2014-15</a></li>
                                        <li><a href="https://drive.google.com/file/d/1kJfsgMF74Ke9ec_LwIi646rlc681jo1W/view?usp=sharing" target="_blank">2013-14</a></li>
					    			</ul>
					    		</li>
					    			  
					    			<li><a href="https://drive.google.com/file/d/1vw7EJNux7DElQzeiWg60Mo1pCUhO5YHC/view?usp=sharing" target="_blank">c3.4.2</a></li>
                                    <li><a href="https://drive.google.com/file/d/10Yb_IngcOC2sQJaS4lllRTc4p9bGk4Yg/view?usp=sharing" target="_blank">c3.4.3</a></li>
                                    <li><a href="https://drive.google.com/file/d/1mFMKQm5kuQUTk5sjwoV_hQiiQHsbTTw3/view?usp=sharing" target="_blank">c3.4.4</a></li>
					    			  
					    			<li><a href="#" aria-current="page">c3.5.1</a>
					    			  	<ul>
					    			  	  <li><a href="https://drive.google.com/file/d/1wH_MkJtZNvBv_bTZqMDB-mmdddChn4tf/view?usp=sharing" target="_blank">2017-18</a></li>
                                          <li><a href="https://drive.google.com/file/d/1K3K2c4340UkrriyfzCC3aMBoLIU7snbN/view?usp=sharing" target="_blank">2016-17</a></li>
                                          <li><a href="https://drive.google.com/file/d/1fGUAwuTpGTJgkpmY_rvOnsdhlOXDtXzC/view?usp=sharing" target="_blank">2015-16</a></li>
                                          <li><a href="https://drive.google.com/file/d/1dr-AHBd-xkltHmO6SHpSjM25vK3kGqk9/view?usp=sharing" target="_blank">2014-15</a></li>
                                          <li><a href="https://drive.google.com/file/d/1EAc7hqi_Bm8ACg0o9UIyaSEwzCMRZBBj/view?usp=sharing" target="_blank">2013-14</a></li>
                                          <li><a href="https://drive.google.com/file/d/1tdOIyG-o0d_HaAkXvYniyuYh1NouvnF_/view?usp=sharing" target="_blank">Total data 3.5.1</a></li>
					    			  	</ul>
					    			</li>
					    			  
					    			<li><a href="https://drive.google.com/file/d/1s5MgIEVmj_7faEOHuQrgbo5tgM1Qkx14/view?usp=sharing" target="_blank">Journal publication details</a></li>
					    			  
					    			</ul>
					    		</li>
					    		<!-- C3 profile -->
					    		
					    		<!-- C4 profile -->
					    		<li><a href="#" aria-current="page">C4</a>
					    			<ul>
					    			  <li><a href="#" aria-current="page">4.1.3</a>
					    			  	<ul>
					    			  	  <li><a href="https://drive.google.com/file/d/1plwXF8x68T21YaOac5sGAkw0OkJNTVJA/view?usp=sharing" target="_blank">Class rooms and seminar halls with ICT details</a></li>
                                          <li><a href="https://drive.google.com/file/d/1tk9tF0VbzCKCxfvkz9THZuQAkw7_SqkN/view?usp=sharing" target="_blank">Geo tag photos of class rooms and seminar halls</a></li>
					    			  	</ul>
					    			  </li>
					    			</ul>
					    			<ul>
					    			  <li><a href="#" aria-current="page">4.1.4</a>
					    			  	<ul>
					    			  	  <li><a href="https://drive.google.com/file/d/1mI3wSgazTFssP4_PC3DgwmUZiuhfVGdI/view?usp=sharing" target="_blank">Data Template</a></li>
                                          <li><a href="https://drive.google.com/file/d/1SnO1uy4ARCf4Wps1ThthocYbl1YTLgSs/view?usp=sharing" target="_blank">Abstract of expenditure on infrastructure certified by CA</a></li>
                                          <li><a href="https://drive.google.com/file/d/1HdPkQ-wwSkDUK8DDWUbM3s2eSnHL2hON/view?usp=sharing" target="_blank">Fund allocation on infrastructure certified by CA</a></li>
                                          <li><a href="https://drive.google.com/file/d/1vMzcniIc7cTpKy1vM1VPxJZ40ts9jGBk/view?usp=sharing" target="_blank">Total expenditure excluding salary certified by CA</a></li>
                                          <li><a href="https://drive.google.com/file/d/1ONUaKxySFIvVwKXuTvDfdcujIGx1GDoh/view?usp=sharing" target="_blank">Audited statements</a></li>
					    			  	</ul>
					    			  </li>
					    			</ul>
					    			<ul>
					    			  <li><a href="#" aria-current="page">4.2.4</a>
					    			  	<ul>
					    			  	  <li><a href="#"></a><li><a href="https://drive.google.com/file/d/13QBaTJe1GVMFk85V4_7hLwhx9XSoHqGn/view?usp=sharing" target="_blank">Data template</a></li>
                                          <li><a href="https://drive.google.com/file/d/1FDTc6W10AG-l2HzUkiZoMYBi-22LVGlt/view?usp=sharing" target="_blank">Expenditure abstract by charted account</a></li>
                                          <li><a href="https://drive.google.com/file/d/1V_foqm74xlnRL9y_AmjRQobH6L9nxS3H/view?usp=sharing" target="_blank">Audited statement</a></li>
					    			  	</ul>
					    			  </li>
					    			</ul>
					    			<ul>
					    			  <li><a href="#" aria-current="page">4.2.5</a>
					    			  	<ul>
					    			  		<li><a href="https://drive.google.com/file/d/1jGxTOI8XmIO_RWuyskRLRF_vljtCISgW/view?usp=sharing" target="_blank">snapshot of gate way and landing page used for remote access </a></li>
					    			  	</ul>
					    			  </li>
					    			</ul>
					    			
					    			<ul>
					    			  <li><a href="#" aria-current="page">4.2.6</a>
					    			  	<ul>
					    			  		<li><a href="https://drive.google.com/file/d/1Vi5yTAUdnyd2CaxhOoce6HwHT9NORs7L/view?usp=sharing" target="_blank">average number of usage statistics</a></li>
                                            <li><a href="https://drive.google.com/file/d/16pJ90bLyt-AiQplW4DTSZhN1aJTz6Cyf/view?usp=sharing" target="_blank">Log books</a></li>
					    			  	</ul>
					    			  </li>
					    			</ul>
					    			
					    			<ul>
					    			  <li><a href="#" aria-current="page">4.4.1</a>
					    			  	<ul>
					    			  	  <li><a href="https://drive.google.com/file/d/1LHSSPhNg0XFCVJhjUpDmhtQAfkSu8Yg2/view?usp=sharing" target="_blank">Data Template</a></li>
                                          <li><a href="https://drive.google.com/file/d/14VYfkvT4Jyetf7qESiGYlTVdqBTyiD93/view?usp=sharing" target="_blank">Expenditure on maintenance of physical facilities certified by CA</a></li>
                                          <li><a href="https://drive.google.com/file/d/1Vtq_71DTnkTzt4M8i8RGF48KyvDApe_2/view?usp=sharing" target="_blank">Expenditure on maintenance of academic support facilities certified by CA</a></li>
                                          <li><a href="https://drive.google.com/file/d/1vMzcniIc7cTpKy1vM1VPxJZ40ts9jGBk/view?usp=sharing" target="_blank">Total expenditure excluding salary certified by CA</a></li>
                                          <li><a href="https://drive.google.com/file/d/1ohIqwbHdtj4cLToIkaqklFwyUolA7Qpk/view?usp=sharing" target="_blank">Audited statements</a></li>
					    			  	</ul>
					    			  </li>
					    			</ul>
					    			
					    		</li>
					    		<!-- C4 profile -->
					    		
					    		<!-- C5 profile -->
					    		<li><a href="#" aria-current="page">C5</a>
					    			<ul>
					    			    <li><a href="https://drive.google.com/open?id=1njMMffBC6dnjVmqHPh25jB_Tb3bRpoph" target="_blank">5.1.1</a></li>
	                                    <li><a href="https://drive.google.com/open?id=19oxP-ByZAFQkv0dCM2XkCKOddfJLzYTV" target="_blank">5.1.3</a></li>
	                                    <li><a href="https://drive.google.com/open?id=1LiX4a5s11kln_Z3KAvSeyUTf90K-Pr2J" target="_blank">5.1.4</a></li>
	                                    <li><a href="https://drive.google.com/open?id=1X5hCSu5zS4XOsL7jahFVq-tWqjV6rKHa" target="_blank">5.2.2</a></li>
	                                    <li><a href="https://drive.google.com/open?id=196GdfIKuFpTf6SzbuJJZMqHNY8qAJ5nv" target="_blank">5.3.1</a></li>
	                                    <li><a href="https://drive.google.com/open?id=16u0NaxF-i3MvW57c_lostqIkJ4d4HZq7" target="_blank">5.3.3</a></li>
	                                    <li><a href="https://drive.google.com/open?id=1Lulqbm0l1bmwdnAT9AnX-Kwzb6mGnVBb" target="_blank">5.4.3</a></li>
					    			</ul>
					    		</li>
					    		<!-- C5 profile -->
					    		
					    		<!-- C6 profile -->
					    		<li><a href="#" aria-current="page">C6</a>
					    			<ul>
					    			  <li><a href="https://drive.google.com/file/d/1V67XRpghwTFjHDI_lZqoqHg5WTRuA6E7/view?usp=sharing" target="_blank">6.2.3</a></li>
	                                    <li><a href="https://drive.google.com/file/d/1p762g2sRbwlgS9QtLMi2vR3wfq1SvyLw/view?usp=sharing" target="_blank">6.3.2</a></li>
	                                    <li><a href="https://drive.google.com/file/d/15rmH77Fk28ISswiFS7E5EySjTXS1YO5D/view?usp=sharing" target="_blank">6.3.3</a></li>
	                                    <li><a href="https://drive.google.com/file/d/1f_3JKFqq0g-seJtBWD_JNLFbam_vD2Eb/view?usp=sharing" target="_blank">6.3.4</a></li>
						    			
						    			<li><a href="#" aria-current="page">6.5.4</a>
					    			  	<ul>
					    			  		<li><a href="#" aria-current="page">Quality Assurance Initiatives [IQAC Meetings]</a>
					    			  			<ul>
					    			  				<li><a href="https://drive.google.com/file/d/1EGdiZDBzYdUON2y68iIOpmvV2yoq1ogE/view?usp=sharing" target="_blank">Meeting 4 </a></li>
	                                                <li><a href="https://drive.google.com/file/d/1KVyueQvp4_TI-9ltxhwsxUfL2i8CV8lI/view?usp=sharing" target="_blank">Meeting 3 </a></li>
	                                                <li><a href="https://drive.google.com/file/d/1qIssG9eQx9fJD_NAXTzKt8UnIGnBP7p4/view?usp=sharing" target="_blank">Meeting 2 </a></li>
	                                                <li><a href="https://drive.google.com/file/d/16_t9aonJOKHjg7kPnHsClhsidgsez5xE/view?usp=sharing" target="_blank">Meeting 1 </a></li>
					    			  			</ul>
					    			  		</li>
					    			  	  <li><a href="https://drive.google.com/file/d/17NNw3C_OauZL_cMwALzcQHD3pjFtCxFE/view?usp=sharing" target="_blank">Academic Administrative Audit (AAA) </a></li>
                                          <li><a href="https://drive.google.com/file/d/15TeGzydHE1UMOQHX6GQ0Xn7GsfhlWNK0/view?usp=sharing" target="_blank"> IEI Certificate </a></li>
					    			  	</ul>
					    			  	
					    			  </li>
					    			</ul>
					    		</li>
					    		<!-- C6 profile -->
					    		
					    		<!-- C7 profile -->
					    		<li><a href="#" aria-current="page">C7</a>
					    			<ul>
					    			    <li><a href="img/naac/c7/7.1.1.pdf" target="_blank">7.1.1</a></li>
	                                    <li><a href="img/naac/c7/7.1.9.pdf" target="_blank">7.1.9</a></li>
	                                    <li><a href="img/naac/c7/7.1.11.pdf" target="_blank">7.1.11</a></li>
	                                    <li><a href="img/naac/c7/7.1.12.pdf" target="_blank">7.1.12</a></li>
	                                    <li><a href="img/naac/c7/(7.1.13) KSIT Core Values.pdf" target="_blank">(7.1.13) KSIT Core Values</a></li>
	                                    <li><a href="img/naac/c7/7.1.15.pdf" target="_blank">7.1.15</a></li>
	                                    <li><a href="img/naac/c7/7.1.16.pdf" target="_blank">7.1.16</a></li>
	                                    <li><a href="img/naac/c7/7.1.17.pdf" target="_blank">7.1.17</a></li>
					    			</ul>
					    		</li>
					    		<!-- C7 profile -->
			    			</ul>			    			
		    			</li>
		    		<!-- DVV Clarifications -->
		    		
		    		<!-- C2 -->
		    		<li><a href="#" aria-current="page">C2</a>
			    			<ul>
			    				<li><a href="img/naac/2.3.2_ICT usage details.pdf">c2.3.2_ICT usage details</a></li>
					    		<li><a href="#" aria-current="page">c2.6.1_PEO, PSO &amp; CO's</a>
					    			<ul>
					    			<li><a href="img/naac/Consolidated CO's.pdf" target="_blank">Consolidated CO's</a></li>
                                    <li><a href="img/naac/CO's of all Courses.pdf" target="_blank">CO's of all Courses</a></li>
                                    <li><a href="img/naac/PROGRAM EDUCATIONAL OBJECTIVES (PEO's).pdf" target="_blank">PROGRAM EDUCATIONAL OBJECTIVES (PEO's)</a></li>
                                    <li><a href="img/naac/PROGRAM OUTCOMES.pdf" target="_blank">PROGRAM OUTCOMES</a></li>
                                    <li><a href="img/naac/PROGRAM SPECIFIC OUTCOMES.pdf" target="_blank">PROGRAM SPECIFIC OUTCOMES</a></li>
					    			</ul>
					    		</li>
					    	</ul>
					</li>
		    		<!-- C2 -->
		    		
		    		<!-- C3 -->
		    		<li><a href="#" aria-current="page">C3</a>
			    			<ul>
					    		<li><a href="img/naac/3.1.2Guide details.pdf" target="_blank">3.1.2Guide details</a></li>
	                            <li><a href="img/naac/3.1.3 Information about funding agency.pdf" target="_blank">3.1.3 Information about funding agency</a></li>
	                            <li><a href="img/naac/3.2.2 Department wise events.pdf" target="_blank">3.2.2 Department wise events</a></li>
	                            <li><a href="img/naac/3.4.4 NSS and red Cross.pdf" target="_blank">3.4.4 NSS and red Cross</a></li>
						   </ul>
					</li>
		    		<!-- C3 -->
		    		
		    		<!-- C4 -->
		    		<li><a href="#" aria-current="page">C4</a>
			    			<ul>
					    		<li><a href="img/naac/4.4.2.pdf" target="_blank">c 4.4.2</a></li>
					    	</ul>
					</li>
		    		<!-- C4 -->
		    		
		    		<!-- C5 -->
		    		<li><a href="#" aria-current="page">C5</a>
			    			<ul>
					    		<li><a href="img/naac/5.1.3 Capability Enhancement.pdf" target="_blank">c 5.1.3 Capability Enhancement</a></li>
					    	</ul>
					 </li>
		    		<!-- C5 -->
		    		
		    		<!-- C6 -->
		    		<li><a href="#" aria-current="page">C6</a>
			    			<ul>
					    		 <li><a href="img/naac/6.2.1IEI accredation strategic plan and deployment.pdf" target="_blank">6.2.1IEI accredation strategic plan and deployment</a></li>
					    	</ul>
					  </li>
		    		<!-- C6 -->
		    		
		    		<!-- C7 -->
		    		<li><a href="#" aria-current="page">C7</a>
			    			<ul>
					    	  <li><a href="img/naac/7.1.13 core values/KSIT Core Values.pdf" target="_blank">7.1.13 core values</a></li>
                              <li><a href="img/naac/7.1.15 professional ethics/7.1.15.pdf" target="_blank">7.1.15 professional ethics</a></li>
							  <li><a href="img/naac/7.3/Best_practices.pdf" target="_blank">7.2 Best Practices</a></li>
							  <li><a href="img/naac/7.3/NAAC 7.3.1_mech details edited by HJR.pdf" target="_blank">7.3.1 Institutional Distinctiveness</a></li>
					    	</ul>
					  </li>
		    		<!-- C7 -->
		    		
		    		<li><a href="img/naac/SSR_Submitted 22 Jan 2019.pdf" target="_blank">SSR</a></li>
                    <li><a href="img/naac/naac_certificate.jpg" target="_blank">NAAC certificate</a></li>
					
		    		
		    	</ul>
		    </li>
					
		<!-- Complete NAAC -->
		<!-- NAAC CYCLE 2 -->
		<li><a href="#" aria-current="page">NAAC Cycle 2</a>
			<ul>
				<li><a href="#" aria-current="page">AQAR</a>
    				<ul>
    					<li><a href="/naac_cycle2_aqar">NAAC AQAR</a></li>	
						<li><a href="img/naac/AQAR_2023-24.pdf" target="_blank">AQAR 2023-2024</a></li>	    						    					
    				</ul>
				</li>	    
				<li><a href="/naac_cycle2_ssr">SSR</a></li>
				<li><a href="img/naac/Cycle_2_SSR-Submitted.pdf">Cycle 2 SSR Submitted</a></li>
				<li><a href="/naac_cycle2_dvv_clarification">DVV Clarification</a></li>	
				<li><a href="img/naac/Cycle_2-DVVClarification.pdf">Cycle 2 DVV Clarification</a></li>												
			</ul>
		</li>
		<!-- Complete  NAAC Cycle 2 -->
		
		<!-- Complete NBA -->
		
		<li><a href="#" aria-current="page">NBA</a>
			<ul>
				<li><a href="/nba_previsit">NBA Previsit</a></li>
				<li><a href="/cse_dept.html#nba">Computer Science and Engineering</a></li>
				<li><a href="/ece_dept.html#nba">Electronics and Communication Engineering</a></li>
				<li><a href="/mech_dept.html#nba">Mechanical Engineering</a></li>
				<li>
					<a href="#" aria-current="page">C.10.1.5</a>
					<ul>
						<li><a href="${img_path!}/about/staff_handbook_2018_new.pdf" target="_blank">Rule Book</a></li>
						<li><a href="img/nba/10.1.5/Student, Faculty & Staff.pdf" target="_blank">Students and Staff</a></li>
					</ul>
				</li>
				
				<li><a href="#" aria-current="page">C.10.2.3</a>
					<ul>
						<li><a href="${img_path!}/about/audit_statement_2019-2020.pdf" target="_blank">Audit Statement</a></li>		                       		                       				
					</ul>
				</li>
				<li><a href="img/nba/nba_certificate.pdf" target="_blank">NBA certificate</a></li>
				
			</ul>
		</li>
		
		
		<!-- Complete NBA -->
	 
	 	<li><a href="#" aria-current="page">ARIIA</a>
	 		<ul>
	 			<li><a href="/img/banner/ARIIA Final Submission Details.pdf" target="_blank">ARIIA 2020 Submission report</a></li>
	 			<li><a href="/img/naac/ariia-certificate.pdf" target="_blank">ARIIA 2020 certificate</a></li>
	 			<li><a href="/img/naac/ariia-certificate-2021.pdf" target="_blank">ARIIA 2021 Submission report</a></li>
	 		</ul>
	 	</li>		
			</ul>
			</li>	
		   <!-- <li><a href="#">IQAC</a>		    
		    	<ul class="childmenu">
		    		<li><a href="#" aria-current="page">IQAC Composition</a>
					<ul>
							<li><a href="img/naac/IQAC Composition 2016.17 & 18.pdf" target="_blank">IQAC Composition 2016,17 &amp; 18</a></li>
							<li><a href="img/naac/Revised-List-of-members-2021-22.pdf" target="_blank">IQAC Composition 2021-2022</a></li>
					</ul>
			</li>
			        <li><a href="img/naac/IQAC details.pdf" target="_blank">IQAC Details</a></li>
			        <li><a href="#" aria-current="page">IQAC Minutes of the meeting</a>
					<ul>
							<li><a href="img/naac/IQAC Minutes of the meeting.pdf" target="_blank">2018-2019</a></li>
							<li><a href="img/naac/IQAC - 2019-20.pdf" target="_blank">2019-2020</a></li>
							<li><a href="img/naac/IQAC 2020-21.pdf" target="_blank">2020-2021</a></li>
							<li><a href="img/naac/IQAC 2021-22.pdf" target="_blank">2021-2022</a></li>
		    		</ul></li>
					</ul>
		    </li>  -->

			<li><a href="iqac.html">IQAC</a></li>
		    <li><a href="#">Admissions</a>
		    	<ul class="childmenu">
					<li><a href="coursesOffered.html">Admission Enquiry</a></li>
                    <li><a href="fees_structure.html">Fee Structure</a></li>	
				<!--	<li><a href="onlineadmissions.html">Online Admission</a></li>			-->				    								    		
		    	</ul>
		    </li>	
		    
		    <li>
          	<!-- <a href="#" class="dropdown-toggle menublink-glow" data-toggle="dropdown" role="button" aria-expanded="false">NBA</a>  -->
	<!--		<a href="#">NBA</a>
            	<ul class="childmenu">
            		<li>
            	        <a href="#" aria-current="page">NBA Applied</a>
            	        <ul>
							<li><a href="/cse_dept.html#nba">Computer Science and Engineering</a></li>
							<li><a href="/ece_dept.html#nba">Electronics and Communication Engineering</a></li>
							<li><a href="/mech_dept.html#nba">Mechanical Engineering</a></li>
							<li>
								<a href="#" aria-current="page">C.10.1.5</a>
								<ul>
									<li><a href="${img_path!}/about/staff_handbook_2018_new.pdf" target="_blank">Rule Book</a></li>
									<li><a href="img/nba/10.1.5/Student, Faculty & Staff.pdf" target="_blank">Students &amp; Staff</a></li>
								</ul>
							</li>
							
							<li><a href="#" aria-current="page">C.10.2.3</a>
								<ul>
									<li><a href="${img_path!}/about/audit_statement_2019-2020.pdf" target="_blank">Audit Statement</a></li>		                       		                       				
								</ul>
							</li>
            	        </ul>            		
            		</li>  
            		<li>
            	    	<a href="/nba_previsit">NBA Previsit</a>
            		</li>
            	</ul>
            </li>  -->
		    
		   	<li><a href="about_library.html">Library</a></li>
		    
		     <li><a href="#">Facilities</a>
		    	<ul class="childmenu">
					 <li><a href="sports.html">Sports</a></li>
                     <li><a href="hostel.html">Hostel</a></li>
                     <li><a href="transport.html">Transport</a></li>
                     <li><a href="canteen.html">Canteen</a></li>			
					 <li><a href="/img/naac/naac_feedback_merged.pdf" target="_blank">NAAC Feedback</a></li>			    		
		    	</ul>
		    </li>	
		    
		  <!--  <li><a href="#">Gallery</a>
		    	<ul class="childmenu">
		    		<li><a href="cse_gallery.html">Artificial Intelligence &amp; Machine Learning Engineering</a></li>
		    		<li><a href="cse_gallery.html">Computer Science and Design</a></li>
					<li><a href="cse_gallery.html">Computer Science &amp; Engineering</a></li>
					<li><a href="ece_gallery.html">Electronics &amp; Communication Engineering</a></li>
                    <li><a href="mech_gallery.html">Mechanical Engineering </a></li>					
                	<li><a href="tele_gallery.html">Electronics & Telecommunication Engineering</a></li>
                    <li><a href="science_gallery.html">Science and Humanities</a></li>
                    <li><a href="sports_gallery.html">Sports</a></li>
                    <li><a href="placement_gallery.html">Placement</a></li>
                    <li><a href="library_gallery.html">Library</a></li>
                    <li><a href="nss_gallery.html">NSS</a></li>
                    <li><a href="redcross_gallery.html">Red Cross</a></li>
                    <li><a href="alumni_gallery.html">Alumni</a></li>	
		    	</ul>
		    </li>	-->	
		    	
		    <li><a href="#">Life at KSIT</a>
		    	<ul class="childmenu">
						<li><a href="alumni.html">Alumni</a></li>
                        <li><a href="clubs.html">Student Club</a></li>
                        <li><a href="ananya.html">Ananya</a></li>
                        <li><a href="nss.html">NSS</a></li>
                        <li><a href="${img_path!}/naac/7.2/bestpractices_12_24.pdf" target="_blank">Best Practices</a></li>
                        <li><a href="redcross.html">Red Cross</a></li>
						<li><a href="#" aria-current="page">Gallery</a>
					    	<ul class="childmenu">
					    		<li><a href="cse_gallery.html">Artificial Intelligence &amp; Machine Learning Engineering</a></li>
		    					<li><a href="cse_gallery.html">Computer Science and Design</a></li>
								<li><a href="cse_gallery.html">Computer Science &amp; Engineering</a></li>
								<li><a href="ece_gallery.html">Electronics &amp; Communication Engineering</a></li>
								<li><a href="mech_gallery.html">Mechanical Engineering </a></li>					
								<!-- <li><a href="tele_gallery.html">Electronics & Telecommunication Engineering</a></li>   -->
								<li><a href="science_gallery.html">Science and Humanities</a></li>
								<li><a href="sports_gallery.html">Sports</a></li>
								<li><a href="placement_gallery.html">Placement</a></li>
								<li><a href="library_gallery.html">Library</a></li>
								<li><a href="nss_gallery.html">NSS</a></li>
								<li><a href="redcross_gallery.html">Red Cross</a></li>
								<li><a href="alumni_gallery.html">Alumni</a></li>								
					    	</ul>
					    </li>
						<li><a href="#" aria-current="page">Statutory Bodies</a>
					    	<ul class="childmenu">
					    		<li><a href="anti_ragging_committee" target="anti_ragging_committee.html">Anti Ragging Committee</a></li>
					    		<li><a href="anti_ragging_squad" target="anti_ragging_squad.html">Anti Ragging Squad</a></li>
								<li><a href="internal_complaints" target="internal_complaints.html">Internal Complaints Committee</a></li>
								<li><a href="sc_st_committee" target="sc_st_committee.html">SC/ST Committee</a></li>
								<li><a href="student_counsellor_details" target="student_counsellor_details.html">Student Counsellor Details</a></li>
								<li><a href="grivance_redressal_committee" target="grivance_redressal_committee.html">Grievance Redressal Committee</a></li>
								<li><a href="anti_sexual_harassment_committee" target="anti_sexual_harassment_committee.html">Anti Sexual Harassment Committee</a></li>

								
					    	</ul>
					    </li>
                        <li><a href="#" aria-current="page">Graduation Day</a>
					    	<ul class="childmenu">
					    		<li><a href="https://drive.google.com/drive/folders/1oQAdmBUuZ_vcGUWtuXm1-KkHFm82Dvvy?usp=sharing" target="_blank">Graduation Day 2016-19</a></li>
					    		<li><a href="https://drive.google.com/drive/folders/178uOhFteKTk3JiBkBayXHoWXyc-hnKSE?usp=sharing" target="_blank">Graduation Day 2017-20</a></li>
								<li><a href="https://drive.google.com/drive/folders/1Mly3H5mXwCPsMqAIg5ZhVrpWYwV2wzce?usp=sharing" target="_blank">Graduation Day 2018-21</a></li>
								<li><a href="https://photos.app.goo.gl/1PQ3jAvondiELUnC9" target="_blank">Graduation Day 2019-22</a></li>
								<li><a href="https://photos.app.goo.gl/BJju7GieG1G1TBV26" target="_blank">Graduation Day 2020-23</a></li>
								
					    	</ul>
					    </li>

					    <li>
							<a target="_blank" href="https://photos.app.goo.gl/XWDoPdYaPMHVzzdo9">Inauguration of 2023 Batch</a>
						</li>           
		    			<li><a href="feedback.html">Feedback</a></li>		    


                        <!--<li><a href="https://drive.google.com/drive/folders/1oQAdmBUuZ_vcGUWtuXm1-KkHFm82Dvvy?usp=sharing" target="_blank">Graduation Day</a></li>-->		
		    	</ul>
		    </li>
		    <li><a href="#">Academics</a>
		    	<ul class="childmenu">
					<li><a href="academic_calender.html">Academic Calendar</a></li>
                    <li><a href="http://vtu.ac.in/time-table-of-ug-pg-for-the-examination-june-july-2016/">Exam Time-table</a></li>	

					<li ><a href="#" aria-current="page">Staff Details</a>
					 	<ul class="childmenu" style="float:left;">				

							<li><a href="${img_path!}/staff-2022.pdf" target="_blank">Staff details 2021-2022</a></li>
							<li><a href="${img_path!}/staff-list-2022-2023.pdf" target="_blank">Staff details 2022-2023</a></li>
								<li><a href="${img_path!}/staff-list-2023-24.pdf" target="_blank">Staff details 2023-2024</a></li>
							
							
						</ul>
					</li>
		    	</ul>
		    </li>	
			<li><a href="about_placement.html">Placements</a></li>
		  <!--  <li><a href="#">Placements</a>
		    	<ul class="childmenu">
					<li><a href="about_placement.html">About Placement</a></li>
		    	</ul>
		    </li>	-->
		    
		    <li><a href="#">Achievements</a>
		    	<ul class="childmenu">
					<!--<li><a href="vtu_ranks.html">Vtu Rankings</a></li>-->
					<li><a href="${img_path!}/Achievers/rank_achievers/rankachieverslist.pdf" target="_blank">VTU Rankings</a></li>
					<li><a href="${img_path!}/consutancy_services1.pdf" target="_blank">Consultancy Services</a></li>
					 <li><a href="#" aria-current="page">Student Achievements</a>
					 	<ul class="childmenu">				

							<li><a href="${img_path!}/hackathon_updated.pdf" target="_blank">Hackathon</a></li>
							<li><a href="${img_path!}/toycathon.pdf" target="_blank">Toycathon</a></li>
							<li><a href="${img_path!}/sae_india_2024.pdf" target="_blank">SAE India</a></li>
						</ul>
					</li> 
		    	</ul>
		    </li>	
		    <li><a href="contact.html">Contact</a></li>
		</ul>
	</div>
	<!-- .stellarnav -->
</div>
<!-- new navbar -->
<#nested/>
<!--=========== BEGIN FOOTER SECTION ================-->
   <footer id="footer">
      <!-- Start footer top area -->
      <div class="footer_top">
         <div class="container">
            <div class="row">
               <div class="col-ld-2  col-md-2 col-sm-3">
                  <div class="single_footer_widget">
                     <!--<h3>Links</h3>-->
                     <ul class="footer_widget_nav">
                        <p><i class="fa fa-link" id="footer-icons"></i> Important links</p>
                        <li><a href="http://www.aicte-india.org/" target="_blank">AICTE</a></li>
                        <li><a href="http://www.vtu.ac.in/" target="_blank">VTU</a></li>
                        <li><a href="http://www.ieeexplore.ieee.org/" target="_blank">IEEE</a></li>
                        <li><a href="https://kshec.ac.in/" target="_blank">KSHEC</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-ld-2  col-md-2 col-sm-3">
                  <div class="single_footer_widget">
                     <!--<h3>Community</h3>-->
                     <ul class="footer_widget_nav">
                        <p><i class="fa fa-support" id="footer-icons"></i> Committees</p>
                        <li><a href="public_press.html" target="_blank">Public Relations &amp; Press</a></li>
                        <li><a href="grievance.html" target="_blank">Grievance Redressal Cell</a></li>
                        <!--<li><a href="${img_path!}/pdf/KSIT_Grievance_Cell.pdf" target="_blank">Grievance Redressal Cell</a></li>-->
                        <li><a href="disciplinary_measures.html" target="_blank">Disciplinary Measures</a></li>
						<li><a href="privacy.html">Disclaimer &amp; Privacy Policy</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-ld-2  col-md-2 col-sm-3">
                  <div class="single_footer_widget">
                     <!--<h3>Community</h3>-->
                     <ul class="footer_widget_nav">
                        <p><i class="fa fa-link" id="footer-icons"></i> Quick Links</p>
                        <li><a href="career.html">Careers</a></li>
                        <li><a href="https://easypay.axisbank.co.in/easyPay/makePayment?mid=MjYyODM%3D" target="_blank">Online Fee Payment</a></li>
                        <li><a href="onlineadmissions.html">Online Admission</a></li>
						<li><a href="contact.html">Contact</a></li>
						<li><a href="admin/signin.html" target="_blank">Login</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-ld-2  col-md-2 col-sm-3">
                  <div class="single_footer_widget">
                     <!--<h3>Community</h3>-->
                     <ul class="footer_widget_nav">
                        <p><i class="fa fa-graduation-cap" id="footer-icons"></i> Scholarship</p>
						<li><a href="https://ssp.postmatric.karnataka.gov.in/" target="_blank">State Scholarship Portal</a></li>
						<li><a href="https://scholarships.gov.in" target="_blank">National Scholarship Portal</a></li>
						<li><a href="https://klwbapps.karnataka.gov.in/" target="_blank">Karnataka Labour Welfare Department</a></li>
						<#--  <li><a href="${img_path!}/ksgischolarship.pdf" target="_blank">KSGI Scholarship</a></li>							  -->
                     </ul>
                  </div>
               </div>
               <div class="col-ld-2  col-md-2 col-sm-3">
                  <div class="single_footer_widget">
                     <!--<h3>Others</h3>-->
                     <ul class="footer_widget_nav">
                        <p><i class="fa fa-institution" id="footer-icons"></i> Group of Institutions</p>
                        <li><a href="https://ksgi.edu.in/" target="_blank">KSGI</a></li>
						<#--  <li><a href="http://www.ksit.ac.in/" target="_blank">KSIT</a>  -->
                        <li><a href="https://www.kssem.edu.in/" target="_blank">KSSEM</a></li>
                        <li><a href="https://www.kssa.edu.in/" target="_blank">KSSA</a></li>
                        <li><a href="https://kspolytechnic.edu.in/" target="_blank">KS Polytehnic</a></li>
						 <li><a href="https://ksgi.edu.in/Best-PU-College-kspuc#" target="_blank">KSPUC</a></li>
                        <li><a href="https://www.google.co.in/search?rlz=1C1CHBD_enIN746IN746&q=k+s+hospital+bangalore&sa=X&ved=0ahUKEwjb9J3t1qbWAhXHso8KHQ1aAggQ1QIImQEoAA&biw=1366&bih=662" target="_blank">KS Hospital</a></li>
                        <li><a href="http://www.kammavaricreditsociety.com/" target="_blank">KS Credit Co-op Bank</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-ld-2  col-md-2 col-sm-3">
                  <div class="single_footer_widget">
                     <!--<h3>Social Links</h3>-->
                     <ul class="footer_social">
                        <p><i class="fa fa-globe" id="footer-icons"></i> Social Links</p>
                        <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip" href="https://www.facebook.com/ksit.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip"  href="https://twitter.com/KSIT_official" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a data-toggle="tooltip" data-placement="top" title="Instagram" class="soc_tooltip"  href="https://www.instagram.com/ksit.official/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip"  href="https://www.linkedin.com/company/k-s-institute-of-technology-bangalore" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip"  href="https://youtu.be/_cNld3MaPtA"><i class="fa fa-youtube" target="_blank"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
	<!--Live Counter-->
	<div class="counter">
        <div class="head"><span class="material-icons" id="footer-icon-counter">touch_app</span>Visitors Counter</div>
		<div class="count" id="CounterVisitor">0</div>
	</div>




    <!-- End footer top area -->
    <!-- Start footer bottom area -->
      <div class="footer_bottom">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="footer_bootomLeft text-center">
                    <p style="color:#fff;font-weight:400;">KSIT&nbsp;<span id="footer-icons">|</span>&nbsp;Copyright &copy; 2017 - <script>document.write(new Date().getFullYear());</script>&nbsp;<span id="footer-icons">|</span>&nbsp;All Rights Reserved</p>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="footer_bootomRight text-center">
                    <p style="color:#fff;font-weight:400;">Designed and developed by <a class="rapsol-link" href="http://www.rapsoltechnologies.com/" rel="nofollow">Rapsol Technologies Pvt Ltd</a>
                    <!--<a href="http://www.billmyschool.com" class="link_color" target="_blank">#BMS</a> -->
                    </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <!-- End footer bottom area -->
   </footer>
   <!-- =========== END FOOTER SECTION ================ --> 
   <!-- =========== Begin Javascript ================ --> 
   <script src="${js_path!}/bootstrap.min.js"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
   <script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
   <!-- =========== Gallery slider =========== -->
   <script type="text/javascript" language="javascript" src="${js_path!}/jquery.tosrus.min.all.js"></script> 
   <script src="http://rawgithub.com/aamirafridi/jQuery.Marquee/master/jquery.marquee.min.js?v=4"type="text/javascript"></script>
   <!--<script src="${js_path!}/jquery.animate-enhanced.min.js"></script>-->
   <!-- =========== Slick slider =========== -->
   <script src="${js_path!}/slick.min.js"></script>
   <!-- =========== Superslides slider =========== -->
   <script src="${js_path!}/jquery.easing.1.3.js"></script>
   <script src="${js_path!}/jquery.superslides.min.js" type="text/javascript" charset="utf-8"></script>   
   <!-- =========== Circle counter =========== -->
   <!-- =========== Smooth animation =========== -->
   <script src="${js_path!}/wow.min.js"></script>  
   <!-- =========== Bootstrap =========== -->
   <script src="${js_path!}/main_slider.js"></script>
   <script src="${js_path!}/slider.js"></script>
   <!-- =========== Lined-icons =========== -->
   <script src="${js_path!}/jquery.dataTables.min.js"></script>
   <script src="${js_path!}/jquery.validate.min.js"></script>
   <script src="${js_path!}/datatable/dataTables.buttons.min.js"></script>
   <script src="${js_path!}/datatable/buttons.flash.min.js"></script>
   <script src="${js_path!}/datatable/jszip.min.js"></script>
   <script src="${js_path!}/datatable/pdfmake.min.js"></script>
   <script src="${js_path!}/datatable/vfs_fonts.js"></script>
   <script src="${js_path!}/datatable/buttons.html5.min.js"></script>
   <script src="${js_path!}/datatable/buttons.print.min.js"></script>
   <!-- =========== Custom js =========== -->
   <script src="${js_path!}/visitors.js"></script>
   <script src="${js_path!}/custom${timestamp!}.js"></script>
   <script src="${js_path!}/feedback${timestamp!}.js"></script>
   <script src="${js_path!}/vertical-tabs-custom${timestamp!}.js"></script>
   <script src="${js_path!}/stellarnav.min.js"></script>
   <script src="${js_path!}/nav-active${timestamp!}.js"></script>
   <script src="${js_path!}/gallery_custom${timestamp!}.js"></script>
   <script src="${js_path!}/back-to-top${timestamp!}.js"></script>
   <!-- =========== Inline js =========== -->
   <script>
      $(document).ready(function(){
      	if($("#myCarousel").length){
      		$('#myCarousel').carousel({
		      	interval: 6000
		      });
      	}
      	
      	if($("#myTab2").length){
      		$('#myTab2').tabs();
      	}

      	function showModal(timeout){
      		setTimeout(function(){
      			$("#myModal").modal();
      		}, timeout);
      	}

      	$('#myModal').on('hidden.bs.modal', function () {
      		showModal(60 * 1000);
      	});
      	
      	showModal(10 * 1000);
      });
   	</script>
	<!--Script-3--> 
   	<script>
	window.onscroll = function() {myFunction()};
	var navbar = document.getElementById("stellarnav");
	//var icons = document.getElementById("media_icons");
	var admissions = document.getElementById("admissions_open");
	var sticky = navbar.offsetTop;
	function myFunction() {
  		if (window.pageYOffset >= 147) {
    		navbar.classList.add("fixed");
			//icons.classList.add("icons-fixed");
			admissions.classList.add("admissions-fixed");
  		} else {
    		navbar.classList.remove("fixed");
			//icons.classList.remove("icons-fixed");
			admissions.classList.remove("admissions-fixed");
  		}
	};
	</script> 
	<!--ScrollToTop--> 
	<!--programs section--> 
	<!--// JavaScript function to update details section dynamically -->
<!--	<script>    
    function updateDetails(title, imageSrc, description) {
      document.getElementById('program-details-title').innerText = title;
      document.getElementById('program-details-image').src = imageSrc;
      document.getElementById('program-details-description').innerText = description;
    }
  </script> -->
  <!--programs section--> 

	<a id="scrollToTop" class="hide" href="#"  onclick="topFunction()"> <i class="fa fa-arrow-circle-up"></i> </a>
</body>
</html>
</#compress>
</#macro>
