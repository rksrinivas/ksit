<#macro eventMacro eventList>
	<table class="table table-striped table-bordered" style="width:80%">
		<thead style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Event Date</th>
			<th>Image</th>
			<th>Report</th>	
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>delete</th>
			
		</thead>
		<tbody>
			<#if eventList?has_content>
				<#list eventList as event>
					<tr>
					
						<td>
							<span id="${event.id!}Heading">${event.heading!}</span>
						</td>
								
						<td>
							<span id="${event.id!}Content">${event.content!}</span>
						</td>
						
						<td>
							<span id="${event.id!}EventDate">${event.eventDate!}</span>
						</td>
						
						<td>
							<span id="${event.id!}Image">
								<#if event.imageURL?has_content>
									<a target="_blank"  href="${event.imageURL!}">View Image</a>
								</#if>
							</span>
						</td>
						
						<td>
							<span id="${event.id!}Report">
								<#if event.reportURL?has_content>
									<a target="_blank"  href="${event.reportURL!}">View Report</a>
								</#if>
							</span>
						</td>
						
						<td>
							<span id="${event.id!}Active">${event.active?string!}</span>
						</td>
						<td>
							<span id="${event.id!}Rank">
								<#if event.rank?has_content>
									${event.rank?string!}
								</#if>
							</span>
						</td>
						
						
						<td>
							<a href="#" class="tooltips" data-toggle="modal" data-target="#editEventModal" data-id=${event.id!}>
								Edit
							</a>
						</td>
						
						<td>
							<a href="#" class="tooltips" onclick="deleteEvent('${event.id!}');">
								delete
							</a>
						</td>
						
					</tr>
				</#list>
			</#if>
		</tbody>
		<tfoot style="width:100%">
			<th>Heading</th>
			<th>Content</th>
			<th>Event Date</th>
			<th>Image</th>
			<th>Report</th>				
			<th>Active</th>
			<th>Rank</th>			
			<th>Edit</th>
			<th>delete</th>
		</tfoot>
	</table>
</#macro>	