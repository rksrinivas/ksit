<@page>  
<br/>
<br/>  	
    <div class="container">
	  <div class="row">
		<h1 style="text-align:center;">Management Committee Members</h1>
	  </div>
	
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class=""><img class="img-responsive" src="${img_path!}/about/president.jpeg" alt="image" /></div>
            <p class="image_caption"><span style="color:darkblue;font-weight:500;">Sri. R. Rajagopal Naidu</span></p>
            <p class="text-center"><span style="color:red;">President</span></p>
            </div>
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class=""><img class="img-responsive" src="${img_path!}/about/secretary.jpeg" alt="image" /></div>
            <p class="image_caption"><span style="color:darkblue;font-weight:500;">Sri. R. Leela Shankar Rao</span></p>
            <p class="text-center"><span style="color:red;">Hon. Secretary</span></p>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class=""> <img class="img-responsive" src="${img_path!}/about/treasurer.jpeg" alt="image" /> </div>
            <p class="image_caption"><span style="color:darkblue;font-weight:500;">Sri. T. Neerajakshulu</span></p>
            <p class="text-center"><span style="color:red;">Treasurer</span></p>
          </div>
        </div>
      </div>
    </div>
    <hr/>
    <div class="row">
    	<div class="col-xs-1 col-lg-1 col-md-1 col-sm-1">
    	</div>
        <div class="col-lg-10 col-md-10 col-sm-10">
         	<img src="${img_path!}/about/ksgi_mgt_grp.jpeg" alt="image" style="width:100%;" />
        </div>
        <div class="col-xs-1 col-lg-1 col-md-1 col-sm-1">
    	</div>
	</div>
      
  <div class="row">
    <div class="col-xs-1 col-lg-1 col-md-1 col-sm-1">
    </div>
    	<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10">
			<h3><i class="fa fa-camera"></i> Sitting From Left to Right:</h3>   	
			<span style="color:darkblue;font-weight:500;">Sri. T. Neerajakshulu</span> (Treasurer), 
			<span style="color:darkblue;font-weight:500;">Sri. S. Venugopal Naidu</span> (Joint Secretary), 
			<span style="color:darkblue;font-weight:500;">Sri. B. Lokanadha Naidu</span> (Vice President), 
			<span style="color:darkblue;font-weight:500;">Sri. R. Rajagopal Naidu</span> (President), 
			<span style="color:darkblue;font-weight:500;">Dr. M. Rukmangada Naidu</span> (Vice President), 
			<span style="color:darkblue;font-weight:500;">Sri. R. Leela Shankar Rao</span> (Hon. Secretary), 
			<span style="color:darkblue;font-weight:500;">Sri. V. Rajendra Naidu</span> (Joint Secretary), 
			<span style="color:darkblue;font-weight:500;">Sri. M. Yogamurthy</span> (Internal Auditor)
			
			<h3><i class="fa fa-camera"></i> Standing From Left to Right:</h3>
			<span style="color:darkblue;font-weight:500;">Sri. V. Ramesh Kumar</span> (Director), 
			<span style="color:darkblue;font-weight:500;">Sri. J.M.Chandra Shekar</span> (Director), 
			<span style="color:darkblue;font-weight:500;">Sri. G. V. Ramesh</span> (Director), 
			<span style="color:darkblue;font-weight:500;">Sri. T. N. Manjunath</span> (Chairman, Environment Committee), 
			<span style="color:darkblue;font-weight:500;">Sri. D. Jagadish Kumar</span> (Chairman, Transport Committee), 
			<span style="color:darkblue;font-weight:500;">Sri. N. Krishnama Naidu</span> (Chairman, Hostel Committee), 
			<span style="color:darkblue;font-weight:500;">Sri. A. V. Nagaraj</span> (Chairman, Building Committee), 
			<span style="color:darkblue;font-weight:500;">Sri. N. M. Krishnamurthy</span> (Chairman, Finance Committee), 
			<span style="color:darkblue;font-weight:500;">Sri. G. Ramana Babu</span> (Director), 
			<span style="color:darkblue;font-weight:500;">Sri. M. Sudhakar</span> (Chairman, Legal cell), 
			<span style="color:darkblue;font-weight:500;">Sri. P. Prabhakar Naidu</span> (Director).
		</div>
		<div class="col-xs-1 col-lg-1 col-md-1 col-sm-1">
    	</div>
    </div>	
    <div class="row">
    	<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
    		<br />
    	</div>
    </div>
</@page>