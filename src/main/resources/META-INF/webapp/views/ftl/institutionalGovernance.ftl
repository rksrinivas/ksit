<@page>
<h1 style="text-align: center; color:red;">Institutional Governance</h1>
<div class="container" style="background-color:white;min-height:600px">
	<div class="row">
		<div class="col-lg-6">
			<table class="table table-striped table-bordered" style="width="100%">
				<tr>
					<th>
						Heading
					</th>
					<th>
						Link
					</th>
				</tr>
				<#list institutionalGovernanceList as institutionalGovernance>
					<tr>
						<td>${institutionalGovernance.heading}</td>
						<td><a href="${institutionalGovernance.link}" target="_blank">View</a></td>
					</tr>
				</#list>
			</table>
		</div>
	</div>
</div>
</@page>