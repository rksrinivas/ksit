<@page>
	<div class="container" style="background-color:white;min-height:600px">

		<h3>Latest Events</h3>
		<div class="row">
			<#list placementLatestEventsList as placementLatestEvents>
				<div class="col-lg-4 col-md-4 col-sm-4">
					${placementLatestEvents.heading!}
					<img src="${placementLatestEvents.imageURL!}" width="100%"/>
  				</div>
  			</#list>
  		</div>
  		
  		<h3>Upcoming Events</h3>
		<div class="row">
			<#list placementUpcomingEventsList as placementUpcomingEvents>
				<div class="col-lg-4 col-md-4 col-sm-4">
					${placementUpcomingEvents.heading!}
					<img src="${placementUpcomingEvents.imageURL!}" width="100%"/>
  				</div>
  			</#list>
  		</div>
	</div>
</@page>