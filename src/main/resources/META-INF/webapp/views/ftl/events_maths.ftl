<@page>
	<!--=========== BEGIN COURSE BANNER SECTION ================-->
    <!--<section id="imgBanner_cse">
      <h2></h2>
    </section>-->
    <!--=========== END COURSE BANNER SECTION ================-->
	
	
	 <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
         

          <!-- start course archive sidebar -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_sidebar">
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Events <span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
				
					                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img\" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <h4>National conference</h4>
					   <p>National Conference on current Advances of Science and Technology</p>
                       <span class="feed_date">2017-05-10</span>
                      </div>
                    </div>
                  </li>     
                  
				  
                </ul>
              </div>
              <!-- End single sidebar -->
             
			  
            </div>
          </div>
          <!-- start course archive sidebar -->
        </div>
      </div>
    </section>
    <!--=========== END COURSE BANNER SECTION ================-->
</@page>