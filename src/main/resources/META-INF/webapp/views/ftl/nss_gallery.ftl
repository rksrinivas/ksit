<@page>
	    <!--=========== BEGIN GALLERY SECTION ================-->
    <section id="gallery">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="gallerySLide" class="gallery_area">
            
             <#list nssGalleryList as nssGallery>
				
					<a href="${nssGallery.imageURL!}" title="">
	                    <img class="gallery_img" src="${nssGallery.imageURL!}" alt="img" />
	                	<span class="view_btn"><i class="fa fa-search-plus"></i> View</span>
	                </a>
				
				</#list>
            
            
             </div>
          </div>
        </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
</@page>