<@page>
    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <section id="courseArchive">
      <div class="container">
        <div class="row">
          <!-- start course content -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="courseArchive_content">
			  
              <div class="row">
					<h3>College Events</h3>
					  <blockquote>
                      <span class="fa fa-quote-left"></span>
                      <p></p>
                    </blockquote>
                    
			  
                <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference from May 10th-12th 2017</h2>
					
						<p>K.S.Institute of Technology is organizing 3 days National Conference from 10th to 12th May 2017 , in association with VTU Belagavi, CSI Bangalore Chapter, IEEE, ISTE and IETE in the following departments:</p>
						
						<p>Department of Computer Science & Engineering (<a href="${img_path!}/college_events/csconf.pdf" target="_blank">click here for more details</a>)</p>
						
						<p>Department of Mechanical Engineering (<a href="${img_path!}/college_events/mechconf.pdf" target="_blank">click here for more details</a>)</p>
						
						<p>Department of Electronics & Communication Engineering (<a href="${img_path!}/college_events/ecconf.pdf" target="_blank">click here for more details</a>)</p>
						
						<p>Department of Telecommunication Engineering (<a href="${img_path!}/college_events/tcconf.pdf" target="_blank">click here for more details</a>)</p>
						
						<p>Department of Science & Humanities (<a href="${img_path!}/college_events/BSfinal.pdf" target="_blank">click here for more details</a>)</p>
						
						<p>Library & Information Center (<a href="${img_path!}/college_events/library.pdf" target="_blank">click here for more details</a>)</p>
						
						<p>Department of Physical Education & Sports (<a href="${img_path!}/college_events/PED final.pdf" target="_blank">click here for more details</a>)</p>
						
						<a href="${img_path!}/college_events/administration-news-IEEE_paper_format_template.doc" target="_blank">click here to download  THE IEEE PAPER FORMAT TEMPLATE</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Free health checkup from Apollo ATHS in association with HSS Club and NSS Board</h2>
						
						<h3 class="singCourse_title"><img src="${img_path!}/college_events/IMG-20170206-WA0012.jpg" alt="img" /></h3>
					
						<p>Free health checkup from Apollo ATHS in association with HSS Club and NSS Board has been conducted on 6-02-2017 in KSIT campus</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Talk</h2>
					
						<p>Technical talk :Latest Trends in Wireless Communications,on 13-2-2017 from 10.30 to 12.30 A.M,by Mr.Devadas Pai.CEO Founder Nano Cell technologies Bangalore.</p>
						<a href="${img_path!}/college_events/administration-news-technical talk.doc" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">KSIT PARTICIPATED IN THE EVENT TO DONATE BLOOD FOR GUINNESS WORLD RECORD </h2>
					
						<p>KSIT has participated for GUINNESS WORLD RECORD in the event to donate blood jointly organised by INDIA RED CROSS and Rotary International District in Bangalore, on 4th August 2016.</p>
						<a href="${img_path!}/college_events/administration-news-New Doc.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">FDP on Raspberry Pi and its Applications in IoT</h2>
					
						<p>KSIT has organized Faculty Development Program on Raspberry Pi and its Applications in IoT from 18th -21st Jan 2017</p>
						<a href="${img_path!}/college_events/administration-news-FDP Broucher.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">The college will remain closed on Tuesday 13th December 2016 on the account of 'Id Milad' </h2>
					
						<p>The college will remain closed on Tuesday 13th December 2016 on the account of 'Id Milad' instead of 12th December 2016. The VTU theory exams scheduled on 13th Dec 2016 has been postponed and the dates will be announced soon in the vtu website.</p>
						<a href="${img_path!}/college_events/administration-news-ksit.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop on "Non Conventional Energy Resources and its Applications for the Society" (N-CERAS) from</h2>
						
						<h3 class="singCourse_title"><img src="${img_path!}/college_events/BROUCHERweb.JPG" alt="img" /></h3>
					
						<p>The prime objective of this Workshop is to educate participants about recent developments in Non Conventional Energy Resources.</p>
						<a href="${img_path!}/college_events/administration-news-BROUCHER.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">RC Aircraft Design &Fabrication workshop</h2>
					
						<p>Dept of ECE conducting "RC Aircraft Design &Fabrication workshop" on 30 september 2016 to 2nd october 2016.</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IEEE Workshop on 20th and 21st August 2016</h2>
					
						<p>IEEE Workshop on 20th and 21st August 2016</p>
						<a href="${img_path!}/college_events/administration-news-IMG-20160816-WA0002 (1).jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">LIFESKILL @ KSIT for 5th and 3rd sem students</h2>
					
						<p>Placement department has planned to conduct personality development program "LIFESKILL @ KSIT" for 7th semester students from 22nd August 2016 to 25th August 2016 and for 3rd sem students from 26th August 2016 to 30th August 2016 . It is mandatory to attend the program.</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Freshers World Online Assessment Test </h2>
					
						<p>Placement department has planned to conduct "Freshers World Online Assessment Test" for 7th semester students on 5th August 2016 at KSIT, Bengaluru.</p>
						<a href="${img_path!}/college_events/administration-news-20160804_143150.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Inauguration Of First year BE classes 2016 - 17 </h2>
					
						<p>The Management, Principal, Staff & Students cordially invite you for inauguration of first year BE classes 2016 - 17 on wednesday, the 3rd August - 2016</p>
						<a href="${img_path!}/college_events/administration-news-inagural invitation 2016 pdf (1).pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">ECE_External Project time table_2016</h2>
					
						<p>Dear students Dept of ECE External Project time table 2016 is attached.</p>
						<a href="${img_path!}/college_events/administration-news-project exam 2.jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PROJECT EXHIBITION of KSIT students in UDAYA NEWS channel</h2>
					
						<p>Hello Everyone Kindly watch UDAYA NEWS channel on saturday the 4th of June 2016 from 5pm to 5.15pm The PROJECT EXHIBITION of KSIT students is going to be telecasted.</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Alumni's are co-ordinarily invited for project exhibition of final year students on 14th May 2016</h2>
					
						<p>Alumni's are co-ordinarily invited for project exhibition of final year students on 14th May 2016 from 9AM on wards at college campus.</p>
											
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IInd National Karnataka Education Summit & Awards 2016 presented to KSIT</h2>
						
						<h3 class="singCourse_title"><img src="${img_path!}/college_events/award1.jpg" alt="img" /></h3>
						
						<p>I sincerely thank HOD's , Faculties, Non-Teaching staff, Supporting staff, Parents and Students for their support in achieving Excellent Engineering College in Karnataka award during IInd National Karnataka Education Summit & Awards 2016<br/>
                                                   - 						PRINCIPAL, KSIT</p>
						<a href="${img_path!}/college_events/administration-news-award1.jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Ananya 2016 on April 2nd</h2>
					
						<h3 class="singCourse_title"><img src="${img_path!}/college_events/Ananya-2016-Invitation.jpg" alt="img" /></h3>
					
						<p>Management, Principal, Staff and Students cordially invite to the Inaugural function of Annual Cultural Fest- Ananya 2016 on Saturday 2nd April 9am @ KSIT.</p>
						<a href="${img_path!}/college_events/administration-news-Ananya-2016-Invitation (2 files merged).pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Faculty Development Program. </h2>
					
						<p>Faculty Development Program on R&D funding opportunities and IPR on March 28th-30th organized by IPR/IEI FORUM, KSIT.</p>
						<a href="${img_path!}/college_events/administration-news-invitation 1.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Adroit'16</h2>
					
						<p>KSIT IEEE Student Branch is organizing Adroit, an inter collegiate semi-technical event on March 5th 2016. Come experience the assorted and put your skills to test to be called an Adroit!!</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two Days Workshop on MSP430 Microcontroller on 16 and 17Th March 2016 </h2>
					
						<p>Dept of ECE conducting "Two Days Workshop on MSP430 Microcontroller on 16 and 17Th March 2016"</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two Days Workshop on Wireless Embedded System on 26 and 27th February 2016</h2>
					
						<p>Dept of ECE conducting "Two Days Workshop on Wireless Embedded System on 26 and 27th February 2016</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two Days Workshop on QUADROTOR on 27 and 28th February 2016</h2>
					
						<p>Dept of ECE Conducting "quadrotor workshop" on 27th and 28th feb 2016.</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Adroit 2016 under IEEE student branch </h2>
					
						<p>Dept of ECE conducting event "Androit" on march 5th 2016 under IEEE branch</p>
											
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Dept of ECE_project first review dates 2016 </h2>
					
						<p>Dept of ECE project first review dates 2016  are uploaded.</p>
						<a href="${img_path!}/college_events/administration-news-ECE Project review Dates 2016.xls" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Dept of ECE _Project Guidelines and Review Dates 2016 Even semester </h2>
					
						<p>Dept Of ECE Project Guidelines and Dates of project review for Even semester 2016.</p>
						<a href="${img_path!}/college_events/administration-news-Project guidelines 2016 ECE.docx" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Schedule for Faculty Development Program on VLSI Design using Cadence Tools.</h2>
					
						<p>Schedule for Faculty Development Program on VLSI Design using Cadence Tools is attached.</p>
						<a href="${img_path!}/college_events/administration-news-SCHEDULE _CadenceFDP_10June2015 ON.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Reopening of college</h2>
					
						<h3 class="singCourse_title"><img src="${img_path!}/college_events/SST.jpg" alt="img" /></h3>
					
						<p>Reopening of college for Academic year 2015 odd sem</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Final Viva Dates 2015</h2>
					
						<p>Project Final Viva Dates for even semester 2015 is displayed on notice board..</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">KSIT Fest Ananya-2015 </h2>
					
						<p>The Management, Principal, Staff and Students Cordially invite you to the ANANYA 2015 on Saturday, 28th March 2015 @ 9.00am</p>
						
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Technical Seminar Dates Department Of ECE ,EVEN Sem 2015 </h2>
					
											
						<p>Department Of ECE.. Technical Seminar Dates for Final Year,EVEN Sem 2015 are attached.</p>
						<a href="${img_path!}/college_events/administration-news-circular3.docx" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">College will reopen on 21st january 2015 </h2>
					
						<h3 class="singCourse_title"><img src="${img_path!}/college_events" alt="img" /></h3>
					
						<p>Dear Students.. College will reopen on 21st Jan 2015 for all semester students.</p>
						<a href="${img_path!}/college_events/reopen college.jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Dept of ECE Students having shortage of attendance</h2>
					
											
						<p>Department of ECE... Students who are having shortage of attendance is uploaded..</p>
											
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">17th ISTE STATE LEVEL FACULTY CONVENTION 2014</h2>
					
						<h3 class="singCourse_title"><img src="${img_path!}/college_events/Optimized-poster.jpg" alt="img" /></h3>
					
						<p>K.S.INSTITUTE OF TECHNOLOGY ORGANIZING "17th ISTE STATE LEVEL FACULTY CONVENTION 2014" ON 20TH NOVEMBER 2014,IN ASSOCIATION WITH ISTE KARNATAKA STATE SECTION.</p>
						<a href="${img_path!}/college_events/administration-news-compressjpeg.rar" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference on " Innovations in Communication and Computation" (NCICC)</h2>
					
						<h3 class="singCourse_title"><img src="img\college_events\image url.jpg" alt="img" /></h3>
					
						<p>The Department of Telecommunication Engineering,KSIT,is Organizing National Conference on Innovations in Communication and Computation, on , 18th October 2014, at 9:00 am to 4:00 pm</p>
								
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">DEPT OF ECE, POSTPONEMENT of 2nd INTERNALS REVISED TIME TABLE</h2>
					
											
						<p>Dept Of ECE.... REVISED 2nd INTERNALS TIME TABLE</p>
						
						<a href="img\college_events\administration-news-2nd INTERNAL TIMETABLE.xlsx" target="_blank">click here to download</a>
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Elements</h2>
					
						<h3 class="singCourse_title"><img src="img\college_events\10_09_14_Poster_iEEE__1410427077_66168.jpg" alt="img" /></h3>
					
						<p>Dept of ECE is conducting IEEE Women in engineering on 15th September 2014</p>
										
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Google core committee,Application form(2014-2015) </h2>
														
						<p>Students who are interested in being a part of the Google Core Committee(GCC) click the below link. http://goo.gl/j25wVv</p>
								
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">2nd International Conference on Advanced Trends in VLSI and Signal Processing (ICAVSP-2014)</h2>
					
						
						<p>2nd International Conference on Advanced Trends in VLSI and Signal Processing (ICAVSP-2014),organized by the Dept of ECE,KSIT Bangalore on 13th &14th Of August 2014.</p>
						<a href="img\college_events\administration-news-Poster_New__04-04-14_Final_Print._Cur.jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A Farewell Function for all the outgoing Final Year Students</h2>
					
						<p>is arranged on 16th May 2014 (Friday) in the college. All the students are requested to attend the same in a neat Formal Dress. Venue for the function is 5th Floor New Block (Mechanical). All are required to be present by 9 am at the venue and on the same day they will be Presented with Course Completion Certificate on the dias.</p>
											
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">OPTIMUS</h2>
					
						<p>IEEE Event on 26th April 2014 Saturday ULTIMATE ENGINEER 1) Team of Max 4 persons 2) Total of 4 rounds 3) 10 teams enter the 3rd Round 4) Only teams entering the 3rd Round and after will be given IEEE participant Certificates Entry Fee Non-IEEE Members - 200 IEEE Members - 100 Timings : 9 am - 4 pm</p>
						<a href="img\college_events\administration-news-optimus copy.jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Google Student Ambassador 2014-2015 Application </h2>
					
										
						<p>Applications to the Google Student Ambassador Program is open to 4th semester students of KSIT. Interested students can apply by filling up the following form: </p>
						<a href="http://goo.gl/SWLfBM" target="_blank">http://goo.gl/SWLfBM</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">International conference ICAVSP 2014 </h2>
														
						<p>Dept of ECE is conducting International Conference on Advances in VLSI and Signal Processing on 13th and 14th August 2014.For any suggestions please feel free to contact Sunil-9743198023 Suraj-9035423362 click here </p>
						<a href="http://www.ksitieee.wix.com/icavsp2014" target="_blank">http://www.ksitieee.wix.com/icavsp2014</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National Conference on Networking, Image Processing and Multimedia 2014</h2>
					
						<p>The staff and students of Department of Computer Science and Engineering Department, KSIT, cordially invite you to the Inaugural Function of NaCoNIM - 2014. National Conference on Networking, Image Processing and Multimedia on Saturday, 5th April 2014, at 9:30 am, in Conference Hall, New Building, KSIT.</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National conference on Networking, Image Processing & Multimedia (NaCoNIM)</h2>
					
					<p>is organized by dept. of Computer Science and Engineering, KSIT, Bangalore on 5th April, 2014. Mail paper in IEEE format with .docx file extension.click on download for more details.</p>
						<a href="img\college_events\administration-news-NaCoNIM 2014 - Poster.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">PENCIL SKETCH COMPETETION</h2>
					
						<h3 class="singCourse_title"><img src="img\college_events\11.bmp" alt="img" /></h3>
					
						<p>The Department of MECHANICAL Engineering is organising PENCIL SKETCH competition under SAE collegiate club. FOR MORE DETAILS/REGISTER CONTACT: SHUBHAM - 8050562059</p>
						<a href="img\college_events\administration-news-1.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE BY MECHANICAL ENGINEERING DEPARTMENT</h2>
					
						<h3 class="singCourse_title"><img src="img\college_events\POSTER.bmp" alt="img" /></h3>
					
						<p>GUEST LECTURE ON " CAREER GUIDANCE AND OPPURTUNITIES IN MECHANICAL ENGINEERING" ON 22nd OF MARCH 2014 BY Mr. RAVI P S</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE On OSS/BSS IN TELECOM NETWORKS</h2>

						<p>GUEST LECTURE On OSS/BSS IN TELECOM NETWORKS Delivered by Mr.Girish and Mr.Ravindra Sharma wireless division, Reliance</p>
						
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
					  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">KSIT - ISTE Student Chapte</h2>
					
						<p>KSIT cordially invites you to the inaugural function of the "KSIT - ISTE Student Chapter" on Saturday, 22 March 2014, at 10:00 AM. Please find additional details in the attached document.</p>
						<a href="img\college_events\administration-news-ISTE Chapter - INVITATION.jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National conference on Networking, Image Processing & Multimedia (NaCoNIM) </h2>
					
						<p>is organized by dept. of Computer Science and Engineering, KSIT, Bangalore on 5th April, 2014. Mail paper in IEEE format with .docx file extension.</p>
						<a href="img\college_events\administration-news-NaCoNIM 2014 - Poster (2).pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">TCS Tech Bytes 2014 Prelims - KSIT</h2>
					
						<p>TCS is organizing Tech Bytes, a National Quiz Contest. The Bangalore regional finals will be held on 10 April 2014. K. S. Institute of Technology is conducting Quiz Prelims on Monday, 17 March 2014 from 08:45 AM. A team must consist of 2 Participants. Please fill the following form to register for the event. We will send confirmation emails to all selected students in the evening of Sunday, 16 March 2014. NOTE: Only those participants who have received the confirmation email are eligible to attend the Quiz. Register here:</p>
						<a href="http://goo.gl/nB2aDe" target="_blank">http://goo.gl/nB2aDe</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
			 <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">HTML5 and Web Development Workshop </h2>
					
						<p>The Google Student Ambassador Program in collaboration with Google Developer Group (GDG) Bangalore is organizing a hands-on workshop on HTML5 and Web Development at K. S. Institute of Technology on Saturday, 15th March 2014. Timings: 10:00 AM to 05:00 PM Students will have to register for the event. We have limited seats available. Please fill up the following form: </p>
						<a href="http://goo.gl/TQwYVc" target="_blank">http://goo.gl/TQwYVc</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
			<!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Dept. of CSE is organizing a Guest Lecture on Cloud Computing</h2>
					
						<p>By Mr.Anil Bidari,Founder and CEO,Cloud Enabled On 4th march 2014 for 8th sem CSE students at 11 am and on 7th march 2014 for 6th CSE students at 11 am Venue: Conference Hall, New Block</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National conference on Networking, Image Processing & Multimedia (NaCoNIM) </h2>
					
						<p>is organized by dept. of Computer Science and Engineering, KSIT, Bangalore on 5th April, 2014. Mail paper in IEEE format with .docx file extension.</p>
						<a href="img\college_events\administration-news-NaCoNIM 2014 - Poster (3).pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NEUTRON - an event organised by KSIT Huawei Club</h2>
					
						<p>Cordially invite you to NEUTRON - an event organised by KSIT Huawei Club on Thursday, 13 February 2014, at 9:00 AM. Venue : Conference Hall & Seminar Hall, New Building, KSIT.</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Cordially invite you to the inaugural function of HUAWEI CLUB - KSIT </h2>
					
						<p>On 28th October 2013 at 9:30 AM Venue: Conference Hall ---PRINCIPAL</p>
						<a href="img\college_events\administration-news-huawei program.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Cordially invite you to the inaugural function of HUAWEI CLUB - KSIT</h2>
					
						<p>On 28th October 2013 at 9:30 AM Venue: Conference Hall ---PRINCIPAL</p>
						<a href="img\college_events\administration-news-huawei program (1).pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Google Core Committee Application</h2>
					
						<p>To apply for Google Core Committee, click the following link: (Deadline: Sunday, 18th August 2013)</p>
						<a href="https://docs.google.com/forms/d/1wkMxCbVoFqMeUtxXh0svDm3WnHjzhspcdKpQqjEif-U/closedform" target="_blank">GCC Application Form</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">GUEST LECTURE ON PRACTICAL SCENARIOS AND EXAMPLE OF COMMUNICATION WITH SATELLITE ,on 15th April 2013 </h2>
					
						<p>Department of Telecommunication Engineering,KSIT is organizing a  " GUEST LECTURE ON PRACTICAL SCENARIOS AND EXAMPLE OF COMMUNICATION WITH SATELLITE " on 15th April 2013.</p>
											
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">One Day National Workshop on "Vibration & its Applications",on 15th March 2013.</h2>
						
						<h3 class="singCourse_title"><img src="img\college_events\National Workshop 11_4.jpg" alt="img" /></h3>
						
						<p>One Day National Workshop on "Vibration & its Applications",on 15th March 2013.Organized By Department of Mechanical Engineering.Download the Workshop Brochure more Details.</p>
						<a href="img\college_events\administration-news-National Workshop Reg Form.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two day State Level Workshop on ETHICAL HACKING & CYBER FORENSICS</h2>
					
						<p>Dept. of Computer Science & Engg., K S Institute of Technology organizing a two days State Level workshop on ETHICAL HACKING & CYBER FORENSICS hosted by Techbharat Consulting, New Delhi 0n 22-02-13 & 23-02-13. Click on download for Registration Form.</p>
						<a href="img\college_events\administration-news-Registration form.docx" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">HELP LINE CENTRE FOR NORTH EAST STUDENT</h2>
					
						<p>A Help-Line Centre has been established to assist the students from North East studying in our Institute. The North East students may contact the Nodal Officer, Mr. SANGAPPA, Assistant Professor, 9845238866</p>
						<a href="img\college_events\administration-news-help-line-ne -website-ksit.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
								  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Freshers Day 2012</h2>
					
						<p>FRESHER'S DAY-2012 function is scheduled on 02-09-2012 at 10 a.m. All newly admitted students and their parents/guardians are INVITED. Ist year B.E. classes commences from 03-09-2012.</p>
						<a href="img\college_events\administration-news-ksit_invitation.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">A State Level Technical Contest, Idea-2012 </h2>
					
						<h3 class="singCourse_title"><img src="img\college_events\224266_510886518928801_2002367001_n (1).jpg" alt="img" /></h3>
					
						<p>Idea-2012 is a State Level Technical Contest under IEEE student chapter. It offers you a chance to make your thoughts shine like a pearl. Ideas are to be explained precisely, need not be proved.</p>
						<a href="img\college_events\administration-news-idea 2012 format.doc" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Forthcoming Placement Events in the college</h2>
					
						<p>A detailed list of the upcoming events related to placement is available in the enclosed document.</p>
						<a href="img\college_events\administration-news-Placements Events.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">National conference on Networking, Image Processing & Multimedia (NaCoNIM)</h2>
					
						<p>is organized by dept. of Computer Science and Engineering, KSIT, Bangalore on 8th sept, 2012.</p>
						<a href="img\college_events\administration-news-naconim.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">NCCSC - 2012 List of Selected Papers</h2>
					
						<p>The shorlisted list of technical papers submitted for NCCSC-2012 is announced. Click on download for details..</p>
						<a href="img\college_events\administration-news-NCCSC List of Conference Papers Selected.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">AIEEE ONLINE EXAMINATIONS 2012</h2>
					
						<p>ON 19th & 26th May 2012.</p>
									
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">COMED-K Transportaion Facility</h2>
					
						<p>In view of the COMED-K exam in our institute on 6-5-2012, buses run from the below mentioned places to our college. Parents and students are requested to make use of this: 1. From City Railway Station   7.30 am 2. Near majestic Shantala silk saree center   7.30am</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">ICVSP INVITATION</h2>
					
						<p>ICVSP INVITATION</p>
						<a href="img\college_events\administration-news-KSIT Invitation.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">One day national conference</h2>
					
						<p>on August 9th,2012 organized by department of ECE, KSIT,In Association with ISTE & IETE. for more details click on download.</p>
						<a href="img\college_events\administration-news-NCCAC 2012 final Print.jpg" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">BLOOD DONATION CAMP on SATURDAY 21st APRIL 2012.</h2>
					
						<p>For more details click on download.</p>
						<a href="img\college_events\administration-news-camp.docx" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two Day International Conference on VLSI and Signal Provessing</h2>
					
						<p>on May 4th and 5th,2012 jointly organized by department of ECE, KSIT,In Association with ISTE,IETE,VTU,ISRO,cadence and National Instruments Bangalore. for more details click on http://icvsp.org/index.html</p>
						
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Low power Embedded Systems using msp 430</h2>
					
						<p>Two Day Workshop on Low power Embedded Systems using texas instruments msp 430 micro controller on 21st and 22nd Feb 2012, jointly organized by  Department of ECE KSIT, Texas Instruments and Cranes Software international ltd., Bangalore.</p>
						<a href="img\college_events\administration-news-msp430_broacher2.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Programme Schedule of TIEM </h2>
					
						<p>Programme Schedule of TIEM </p>
						<a href="img\college_events\administration-news-invitation 1.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Online Registration for Workshop on TIEM </h2>
					
						<p>Online Registration for Two Day National Seminar on Trends in Engineering Materials on Dec 5th - 6th 2011 is open. Download and refer the document for the link.</p>
						<a href="img\college_events\administration-news-Online_Registration.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Workshop on TIEM</h2>
					
						<p>Two Day National Seminar on Trends in Engineering Materials on Dec 5th - 6th  2011, organized by  Department of  Mechanical Engineering, KSIT, Bangalore</p>
						<a href="img\college_events\administration-news-invitation1 1.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Inaugural Day for 1st Sem 2011-12</h2>
					
						<p>Programme Schedule for the Inaugural function on 9th September, 2011</p>
						<a href="img\college_events\administration-news-Programme Schedule1.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">IDEA - 2011, A State Level Technical Contest, Last date extended upto 10th Sep </h2>
					
						<p>IDEA - 2011, A State Level Technical Contest on Sep 15th  2011, to celebrate 151st Birth Anniversary of Sir M.Visvesvaraya, organized by Department of ECE, KSIT. </p>
						<a href="img\college_events\administration-news-1 (1).pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Two Day Workshop on LABVIEW & NI HARDWARE</h2>
					
						<p>Two Day Workshop on LABVIEW & NI HARDWARE on sep 2nd and 3rd 2011, jointly organized by  Department of EC, KSIT & National Instruments, Bangalore</p>
						<a href="img\college_events\administration-news-LAB VIEW Workshop.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">2- day National Workshop on</h2>
					
						<p>Emerging Trends in e- Resource Management and Mandatory Library Standards 15-16 July, 2011.</p>
						<a href="img\college_events\administration-news-b.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Project Exhibition - 2011 will be held on 20th May</h2>
					
						<p>All the students and staff members are informed to attend the event in the respective departments.</p>
											
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Science Fest Program Details </h2>
					
						<p>Download Science Fest Program Details</p>
						<a href="img\college_events\administration-news-Program.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Parent-teachers meet on 19th march,2011</h2>
					
						<p>Please attend parent-teacher meet on 19th March, 2011 ---------principal, KSIT</p>
						<a href="img\college_events\administration-news-PROGRAM AGENDA1.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				  <!-- start single course -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="single_course wow fadeInUp">
                    
                    <div class="singCourse_content">
					
						<h2 class="singCourse_title">Science Fest -March 2011</h2>
					
						<p>Department of Science and Humanities is Organising Science Fest on 18-19 March 2011</p>
						<a href="${img_path!}/college_events/administration-news-FinalPoster.pdf" target="_blank">click here to download</a>
					
					
                    </div>
                   
                  </div>
                </div>
                <!-- End single course -->
				
				
				
				
				
               
              
            </div>
          </div>
          <!-- End course content -->


        </div>
      </div>
    </section>
 </@page> 