module.exports = function (grunt) {
    grunt.initConfig({
        babel: {
            dist: {
                files: [{
                    "expand": true,
                    "cwd": "app/js",
                    "src": ["**/*.js", "*.js", "!**min*.js"],
                    "dest": "js-compiled/",
                }]
            }
        },
        uglify : {
            dist : {
                files: [{
                    "expand": true,
                    "cwd": "js-compiled",
                    "src": ["**/*.js", "*.js"],
                    "dest": "dist/js/",
                }]
            }
        },
        cssmin: {
            dist : {
                files: [{
                    "expand": true,
                    "cwd": "app/css",
                    "src": ["**/*.css", "*.css",  "!**min*.css"],
                    "dest": "dist/css/",
                }]
            }
        },
        copy: {
            dist1: {
                files: [{
                    "expand": true,
                    "cwd": "app/img",
                    "src": ["*.*", "**/*.*"],
                    "dest": "dist/img/",
                }]
            },
            dist2: {
                files: [{
                    "expand": true,
                    "cwd": "app/fonts",
                    "src": ["*.*", "**/*.*"],
                    "dest": "dist/fonts/",
                }]
            },
            dist3: {
                files: [{
                    "expand": true,
                    "cwd": "app/js",
                    "src": ["**min*.js"],
                    "dest": "dist/js/",
                }]
            },
            dist4: {
                files: [{
                    "expand": true,
                    "cwd": "app/css",
                    "src": ["**min*.css"],
                    "dest": "dist/css/",
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.registerTask("default", ["babel", "uglify", "cssmin", "copy:dist1", "copy:dist2", "copy:dist3", "copy:dist4"]);
};