
function deleteFeedback(id){
	var params = {
		id: id
	};
	var url = "/api/v1/feedback/deleteFeedback";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},

		success : function(feedback) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}