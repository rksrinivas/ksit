$(document).ready(function() { 
    $(".add_configuration_lab_data").click(function(e){
        e.preventDefault();
        $(this).closest("div").append('<div><textarea rows="2" cols="50" class = "lab_input" name="configurationsLabData[]"></textarea><a href="#" class="delete">Delete</a></div>');
    });
    
    $(".add_no_of_system_lab_data").click(function(e){
        e.preventDefault();
        $(this).closest("div").append('<div><input type="number" min="1" name="noOfSystemsLabData[]" value="1"/><a href="#" class="delete">Delete</a></div>');
    });
    
    $(".add_purpose_lab_data").click(function(e){
        e.preventDefault();
        $(this).closest("div").append('<div><textarea rows="2" cols="50" class = "lab_input" name="purposesLabData[]"></textarea><a href="#" class="delete">Delete</a></div>');
    });
    
    
 
    $(".labcontainer").on("click",".delete", function(e){
        e.preventDefault(); 
        $(this).parent('div').remove();
    });
    
    $("#cancelAddLabData").on('click', function(e) {
    	$("#addLabDataModal").modal("hide")
    });
    
    $("#saveAddLabData").on('click', function(e) {
    	addLabData();
    });
});

function addLabData(){
	var formData = new FormData();
	if( $("#imageAddValueLabData")[0].files.length > 0){
		formData.append("img", $("#imageAddValueLabData")[0].files[0]);
	}
	formData.append('page', $("#pageAddValueLabData").val());
	formData.append("fieldName", $("#fieldNameAddValueLabData").val());
	formData.append("active", $("#activeAddValueLabData").val());
	formData.append("rank", $("#rankAddValueLabData").val());
	
	var serialNo = $("#serialNoAddValueLabData").val();
	var name = $("#nameAddValueLabData").val();
	var configurations = $("textarea[name='configurationsLabData[]']").map(function(){return $(this).val();}).get();
	var purposes = $("textarea[name='purposesLabData[]']").map(function(){return $(this).val();}).get();
	var noOfSystems = $("input[name='noOfSystemsLabData[]']").map(function(){return $(this).val();}).get();
	
	let inputDataJson = {
		serialNo: serialNo,
		name: name,
		configurations: configurations,
		purposes: purposes,
		noOfSystems: noOfSystems
	};
	formData.append("inputData", JSON.stringify(inputDataJson)); 
	
	var url = "/api/v1/lab/addLabData";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}

function deleteLabData(id){
	var params = {
		id: id
	};
	var url = "/api/v1/lab/deleteLabData";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function(event) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}