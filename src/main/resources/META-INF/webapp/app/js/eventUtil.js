$(document).ready(function(){
	var editEventModalId = "editEventModal";
	var addEventModalId = "addEventModal";
	
	$("#"+editEventModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		
		var id = source.data("id");
		var heading = $("#"+id+"Heading").text();
		var content = $("#"+id+"Content").text();
		var eventDate = $("#"+id+"EventDate").text();
		var active = $("#"+id+"Active").text();
		var rank = $("#"+id+"Rank").text();
		
		rank = Number.parseInt(rank);
		
		$("#idEditValue"+editEventModalId).val(id);
		if(heading){
			$("#headingEditValue"+editEventModalId).val(heading);
		}
		if(content){
			$("#contentEditValue"+editEventModalId).val(content);
		}
		if(eventDate){
			$("#eventDateEditValue"+editEventModalId).val(eventDate);
		}
		$("#activeEditValue"+editEventModalId).val(active);
		$("#rankEditValue"+editEventModalId).val(rank);
		
	});
	
	$("#"+addEventModalId).on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");
		
		$("#pageAddValue"+addEventModalId).val(page);
		$("#fieldNameAddValue"+addEventModalId).val(fieldName);
	});  
	
	$("#cancelEdit"+editEventModalId).on('click', function(e) {
		$("#"+editDataModalId).modal("hide");
	});
	$("#saveEdit"+editEventModalId).on('click', function(e){
		editEvent();
	});
	
	$("#cancelAdd"+addEventModalId).on('click', function(e){
		$("#"+addEventModalId).modal("hide");
	});
	
	$("#saveAdd"+addEventModalId).on("click", function(e){
		addEvent();
	});
});

function editEvent(){
	var editEventModalId = "editEventModal";
	
	var idEditValue = $("#idEditValue"+editEventModalId).val();
	var headingEditValue = $("#headingEditValue"+editEventModalId).val();
	var contentEditValue = $("#contentEditValue"+editEventModalId).val();
	var eventDateEditValue = $("#eventDateEditValue"+editEventModalId).val();
	var activeEditValue = $("#activeEditValue"+editEventModalId).val();
	var rankEditValue = $("#rankEditValue"+editEventModalId).val();
	
	var formData = new FormData();
	if( $('#imageEditValue'+editEventModalId)[0].files.length > 0){
		formData.append('img', $('#imageEditValue'+editEventModalId)[0].files[0]);
	}
	if( $('#reportEditValue'+editEventModalId)[0].files.length > 0){
		formData.append('report', $('#reportEditValue'+editEventModalId)[0].files[0]);
	}
	if(idEditValue){
		formData.append('id', idEditValue);
	}
	if(headingEditValue){
		formData.append('heading', headingEditValue);
	}
	if(contentEditValue){
		formData.append('content', contentEditValue);
	}
	if(eventDateEditValue){
		formData.append('eventDate', eventDateEditValue);
	}
	if(activeEditValue){
		formData.append("active", activeEditValue);
	}
	if(rankEditValue){
		formData.append("rank", rankEditValue);
	}
	
	var url = "/api/v1/event/editEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false, 
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addEvent(){
	var addEventModalId = "addEventModal";
	
	var pageAddValue = $("#pageAddValue"+addEventModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addEventModalId).val();
	var headingAddValue = $("#headingAddValue"+addEventModalId).val();
	var contentAddValue = $("#contentAddValue"+addEventModalId).val();
	var eventDateAddValue = $("#eventDateAddValue"+addEventModalId).val();
	var activeAddValue = $("#activeAddValue"+addEventModalId).val();
	var rankAddValue = $("#rankAddValue"+addEventModalId).val();
	
	var formData = new FormData();
	if( $("#imageAddValue"+addEventModalId)[0].files.length > 0){
		formData.append("img", $("#imageAddValue"+addEventModalId)[0].files[0]);
	}
	if( $("#reportAddValue"+addEventModalId)[0].files.length > 0){
		formData.append("report", $("#reportAddValue"+addEventModalId)[0].files[0]);
	}
	formData.append('page', pageAddValue);
	formData.append("fieldName", fieldNameAddValue);
	formData.append('heading', headingAddValue);
	formData.append('content', contentAddValue);
	formData.append('eventDate', eventDateAddValue);
	formData.append("active", activeAddValue);
	formData.append("rank", rankAddValue);
	
	
	var url = "/api/v1/event/addEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}


function deleteEvent(id){
	var params = {
		id: id
	};
	var url = "/api/v1/event/deleteEvent";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function(event) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}