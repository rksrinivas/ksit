$(document).ready(function(){
	var editGalleryModalId = "editGalleryModal";
	var addGalleryModalId = "addGalleryModal";
	
	$("#"+editGalleryModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		
		var id = source.data("id");
		var active = $("#"+id+"Active").text();
		var rank = $("#"+id+"Rank").text();
		var heading = $("#"+id+"Heading").text();
		
		rank = Number.parseInt(rank);
		
		$("#idEditValue"+editGalleryModalId).val(id);
		$("#activeEditValue"+editGalleryModalId).val(active);
		$("#rankEditValue"+editGalleryModalId).val(rank);
		if(heading){
			$("#headingEditValue"+editGalleryModalId).val(heading);
		}
		
	});
	
	$("#"+addGalleryModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");
		
		$("#pageAddValue"+addGalleryModalId).val(page);
		$("#fieldNameAddValue"+addGalleryModalId).val(fieldName);
	});  
	
	$("#cancelEdit"+editGalleryModalId).on('click', function(e) {
		$("#"+editGalleryModalId).modal("hide");
	});
	$("#saveEdit"+editGalleryModalId).on('click', function(e){
		editGallery();
	});
	
	$("#cancelAdd"+addGalleryModalId).on('click', function(e) {
		$("#"+addGalleryModalId).modal("hide");
	});
	$("#saveAdd"+addGalleryModalId).on('click', function(e){
		addGallery();
	});
});

function editGallery(){
	var editGalleryModalId = "editGalleryModal";
	
	var headingEditValue = $("#headingEditValue"+editGalleryModalId).val();
	var idEditValue = $("#idEditValue"+editGalleryModalId).val();
	var activeEditValue = $("#activeEditValue"+editGalleryModalId).val();
	var rankEditValue = $("#rankEditValue"+editGalleryModalId).val();
	
	var formData = new FormData();
	if(headingEditValue){
		formData.append('heading', headingEditValue);
	}
	if( $('#imageEditValue'+editGalleryModalId)[0].files.length > 0){
		formData.append('img', $('#imageEditValue'+editGalleryModalId)[0].files[0]);
	}
	if(idEditValue){
		formData.append('id', idEditValue);
	}
	if(activeEditValue){
		formData.append('active', activeEditValue);
	}
	if(rankEditValue){
		formData.append('rank', rankEditValue);
	}
	
	//Use editEvent or we can write new editGallery API
	var url = "/api/v1/event/editEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
	    processData: false,
	    contentType: false,
	    success : function(data) {
	    	reloadPage();
	    }
	});
}

function addGallery(){
	var addGalleryModalId = "addGalleryModal";
	
	var headingAddValue = $("#headingAddValue"+addGalleryModalId).val();
	var pageAddValue = $("#pageAddValue"+addGalleryModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addGalleryModalId).val();
	var activeAddValue = $("#activeAddValue"+addGalleryModalId).val();
	var rankAddValue = $("#rankAddValue"+addGalleryModalId).val();
	
	var formData = new FormData();
	if( $('#imageAddValue'+addGalleryModalId)[0].files.length > 0){
		formData.append('img', $('#imageAddValue'+addGalleryModalId)[0].files[0]);
	}
	formData.append('heading', headingAddValue);
	formData.append('page', pageAddValue);
	formData.append("fieldName", fieldNameAddValue);
	formData.append("active", activeAddValue);
	formData.append("rank", rankAddValue);
	
	//Use addEvent or we can write new addGallery API
	var url = "/api/v1/event/addEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}


function deleteGallery(id){
	var params = {
		id: id
	};
	var url = "/api/v1/event/deleteEvent";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function(event) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}

