$(document).ready(function() {
	$("#addProgrammeDataModal").on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var programmeId = source.data("programme_id");		
		$("#programmeIdProgrammeDataData").val(programmeId);
	});  
	
    $("#cancelAddProgrammeDataData").on('click', function(e) {
    	$("#addProgrammeDataModal").modal("hide")
    });
    
    $("#saveAddProgrammeDataData").on('click', function(e) {
    	addProgrammeDataData();
    });
});

function addProgrammeDataData(){
	var formData = new FormData();
	if( $("#programmeDataData")[0].files.length > 0){
		formData.append("programmeData", $("#programmeDataData")[0].files[0]);
	}
	formData.append("programmeDataType", $("#programmeDataType").val());	
	formData.append("programmeId", $("#programmeIdProgrammeDataData").val());
	formData.append("semester", $("#semesterSyllabusData").val());
	formData.append("startYear", $("#startYearProgrammeDataData").val());
	formData.append("endYear", $("#endYearProgrammeDataData").val());

	var url = "/api/v1/programmeData/add";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function() {
			reloadPage();
		}
	});
}

function deleteSyllabusData(id){
	var params = {
		id: id
	};
	var url = "/api/v1/programmeData/delete";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function() {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}