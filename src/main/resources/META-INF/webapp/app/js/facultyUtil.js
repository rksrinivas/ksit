$(document).ready(function() {
	var editFacultyModalId = "editFacultyModal";
	var addFacultyModalId = "addFacultyModal";
	
	$("#"+editFacultyModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);

		var id = source.data("id");
		var name = $("#" + id + "Name").text();
		var designation = $("#" + id + "Designation").text();
		var qualification = $("#" + id + "Qualification").text();
		var department = $("#" + id + "Department").text();

		var active = $("#" + id + "Active").text();
		var rank = $("#" + id + "Rank").text();

		rank = Number.parseInt(rank);

		$("#idEditValue"+editFacultyModalId).val(id);
		if (name) {
			$("#nameEditValue"+editFacultyModalId).val(name);
		}
		if (designation) {
			$("#designationEditValue"+editFacultyModalId).val(designation);
		}
		if (qualification) {
			$("#qualificationEditValue"+editFacultyModalId).val(qualification);
		}
		if (department) {
			$("#departmentEditValue"+editFacultyModalId).val(department);
		}
		$("#activeEditValue"+editFacultyModalId).val(active);
		$("#rankEditValue"+editFacultyModalId).val(rank);

	});

	$("#"+addFacultyModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");

		$("#pageAddValue"+addFacultyModalId).val(page);
		$("#fieldNameAddValue"+addFacultyModalId).val(fieldName);
	});

	$("#cancelEdit"+editFacultyModalId).on('click', function(e) {
		$("#"+editFacultyModalId).modal("hide");
	});
	$("#saveEdit"+editFacultyModalId).on('click', function(e){
		editFaculty();
	});
	
	$("#cancelAdd"+addFacultyModalId).on('click', function(e){
		$("#"+editFacultyModalId).modal("hide");
	});
	
	$("#saveAdd"+addFacultyModalId).on("click", function(e){
		addFaculty();
	});
});

function editFaculty() {
	var editFacultyModalId = "editFacultyModal";
	
	var idEditValue = $("#idEditValue"+editFacultyModalId).val();
	var nameEditValue = $("#nameEditValue"+editFacultyModalId).val();
	var designationEditValue = $("#designationEditValue"+editFacultyModalId).val();
	var qualificationEditValue = $("#qualificationEditValue"+editFacultyModalId).val();
	var departmentEditValue = $("#departmentEditValue"+editFacultyModalId).val();
	var activeEditValue = $("#activeEditValue"+editFacultyModalId).val();
	var rankEditValue = $("#rankEditValue"+editFacultyModalId).val();

	var form = new FormData();
	if ($("#imageEditValue"+editFacultyModalId)[0].files.length > 0) {
		form.append("img", $("#imageEditValue"+editFacultyModalId)[0].files[0]);
	}
	if ($("#profileEditValue"+editFacultyModalId)[0].files.length > 0) {
		form.append("profile", $("#profileEditValue"+editFacultyModalId)[0].files[0]);
	}
	if (idEditValue) {
		form.append("id", idEditValue);
	}
	if (nameEditValue) {
		form.append("name", nameEditValue);
	}
	if (designationEditValue) {
		form.append("designation", designationEditValue);
	}
	if (qualificationEditValue) {
		form.append("qualification", qualificationEditValue);
	}
	if (departmentEditValue) {
		form.append("department", departmentEditValue);
	}
	if (activeEditValue) {
		form.append("active", activeEditValue);
	}
	if (rankEditValue) {
		form.append("rank", rankEditValue);
	}

	var url = "/api/v1/faculty/editFaculty";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false,
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addFaculty() {
	var addFacultyModalId = "addFacultyModal";
	
	var pageAddValue = $("#pageAddValue"+addFacultyModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addFacultyModalId).val();
	var nameAddValue = $("#nameAddValue"+addFacultyModalId).val();
	var designationAddValue = $("#designationAddValue"+addFacultyModalId).val();
	var qualificationAddValue = $("#qualificationAddValue"+addFacultyModalId).val();
	var departmentAddValue = $("#departmentAddValue"+addFacultyModalId).val();
	var activeAddValue = $("#activeAddValue"+addFacultyModalId).val();
	var rankAddValue = $("#rankAddValue"+addFacultyModalId).val();

	var form = new FormData();
	if ($("#imageAddValue"+addFacultyModalId)[0].files.length > 0) {
		form.append("img", $("#imageAddValue"+addFacultyModalId)[0].files[0]);
	}
	if ($("#profileAddValue"+addFacultyModalId)[0].files.length > 0) {
		form.append("profile", $("#profileAddValue"+addFacultyModalId)[0].files[0]);
	}
	if (pageAddValue) {
		form.append("page", pageAddValue);
	}
	if (fieldNameAddValue) {
		form.append("fieldName", fieldNameAddValue);
	}
	if (nameAddValue) {
		form.append("name", nameAddValue);
	}
	if (designationAddValue) {
		form.append("designation", designationAddValue);
	}
	if (qualificationAddValue) {
		form.append("qualification", qualificationAddValue);
	}
	if (departmentAddValue) {
		form.append("department", departmentAddValue);
	}
	if (activeAddValue) {
		form.append("active", activeAddValue);
	}
	if (rankAddValue) {
		form.append("rank", rankAddValue);
	}

	var url = "/api/v1/faculty/addFaculty";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false, 
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function deleteFaculty(id) {
	var params = {
		id : id
	};
	var url = "/api/v1/faculty/deleteFaculty";
	$.ajax({
		url : url,
		data : params,
		type : 'POST',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded'
		},

		success : function(faculty) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}