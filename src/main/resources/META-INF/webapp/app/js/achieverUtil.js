$(document).ready(function(){
	var editAchiversModalId = "editAchiversModal";
	var addAchieversModalId = "addAchieversModal";
	
	$("#"+editAchiversModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		
		var id = source.data("id");
		var heading = $("#"+id+"Heading").text();
		var content = $("#"+id+"Content").text();
		var eventDate = $("#"+id+"EventDate").text();
		var active = $("#"+id+"Active").text();
		var rank = $("#"+id+"Rank").text();
		
		rank = Number.parseInt(rank);
		
		$("#idEditValue"+editAchiversModalId).val(id);
		if(heading){
			$("#headingEditValue"+editAchiversModalId).val(heading);
		}
		if(content){
			$("#contentEditValue"+editAchiversModalId).val(content);
		}
		if(eventDate){
			$("#eventDateEditValue"+editAchiversModalId).val(eventDate);
		}
		$("#activeEditValue"+editAchiversModalId).val(active);
		$("#rankEditValue"+editAchiversModalId).val(rank);
		
	});
	
	$("#"+addAchieversModalId).on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");
		
		$("#pageAddValue"+addAchieversModalId).val(page);
		$("#fieldNameAddValue"+addAchieversModalId).val(fieldName);
	});  
	
	$("#cancelEdit"+editAchiversModalId).on('click', function(e) {
		$("#"+editAchiversModalId).modal("hide");
	});
	$("#saveEdit"+editAchiversModalId).on('click', function(e){
		editAchivers();
	});
	
	$("#cancelAdd"+addAchieversModalId).on('click', function(e){
		$("#"+addAchieversModalId).modal("hide");
	});
	
	$("#saveAdd"+addAchieversModalId).on("click", function(e){
		addAchievers();
	});
});

function editAchivers(){
	var editAchiversModalId = "editAchiversModal";
	
	var idEditValue = $("#idEditValue"+editAchiversModalId).val();
	var headingEditValue = $("#headingEditValue"+editAchiversModalId).val();
	var contentEditValue = $("#contentEditValue"+editAchiversModalId).val();
	var eventDateEditValue = $("#eventDateEditValue"+editAchiversModalId).val();
	var activeEditValue = $("#activeEditValue"+editAchiversModalId).val();
	var rankEditValue = $("#rankEditValue"+editAchiversModalId).val();
	
	var formData = new FormData();
	if( $('#imageEditValue'+editAchiversModalId)[0].files.length > 0){
		formData.append('img', $('#imageEditValue'+editAchiversModalId)[0].files[0]);
	}
	if(idEditValue){
		formData.append('id', idEditValue);
	}
	if(headingEditValue){
		formData.append('heading', headingEditValue);
	}
	if(contentEditValue){
		formData.append('content', contentEditValue);
	}
	if(eventDateEditValue){
		formData.append('eventDate', eventDateEditValue);
	}
	if(activeEditValue){
		formData.append("active", activeEditValue);
	}
	if(rankEditValue){
		formData.append("rank", rankEditValue);
	}
	
	var url = "/api/v1/event/editEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false, 
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addAchievers(){
	var addAchieversModalId = "addAchieversModal";
	
	var pageAddValue = $("#pageAddValue"+addAchieversModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addAchieversModalId).val();
	var headingAddValue = $("#headingAddValue"+addAchieversModalId).val();
	var contentAddValue = $("#contentAddValue"+addAchieversModalId).val();
	var eventDateAddValue = $("#eventDateAddValue"+addAchieversModalId).val();
	var activeAddValue = $("#activeAddValue"+addAchieversModalId).val();
	var rankAddValue = $("#rankAddValue"+addAchieversModalId).val();
	
	var formData = new FormData();
	if( $("#imageAddValue"+addAchieversModalId)[0].files.length > 0){
		formData.append("img", $("#imageAddValue"+addAchieversModalId)[0].files[0]);
	}
	formData.append('page', pageAddValue);
	formData.append("fieldName", fieldNameAddValue);
	formData.append('heading', headingAddValue);
	formData.append('content', contentAddValue);
	formData.append('eventDate', eventDateAddValue);
	formData.append("active", activeAddValue);
	formData.append("rank", rankAddValue);
	
	
	var url = "/api/v1/event/addEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}


function deleteAchievers(id){
	var params = {
		id: id
	};
	var url = "/api/v1/event/deleteEvent";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function(event) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}