$(document).ready(function() {
	$("#feedbackListModal").on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var feedbackPage = source.data("page");		
		$("#feedbackParentPage").val(feedbackPage);
		$("#feedbackAlumniPage").val(feedbackPage);
		$("#feedbackStudentPage").val(feedbackPage);
		$("#feedbackEmployerPage").val(feedbackPage);
	});
	
	$('#feedbackList').on('change', function() {
		if(!this.value){
			return;
		}
		$("#feedbackListModal").modal("hide");
		
		let prefix = "";
		
		if(this.value == "PARENT"){
			prefix = "#feedbackParentModal";
		}else if(this.value == "STUDENT"){
			prefix = "#feedbackStudentModal";
		}else if(this.value == "ALUMNI"){
			prefix = "#feedbackAlumniModal";
		}
		else if(this.value == "EMPLOYER"){
			prefix = "#feedbackEmloyerModal";
		}
		
		let feedbackPage = $("#feedbackParentPage").val();
		
		
		if(feedbackPage == "cse_page"){
			$(prefix +' span[name="departmentName"]').text("Computer Science and Engineering");
			let title = "K. S INSTITUTE OF TECHNOLOGY, BENGALURU<br />#14, Raghuvanahalli, Kanakapura Main Road, Bengaluru-560019<br />Department Of Computer Science & Engineering";
			$(prefix + ' .modal-title').html(title);
		}
		else if(feedbackPage == "mech_page"){
			$(prefix + ' span[name="departmentName"]').text("Mechanical Engineering");
			let title = "K. S INSTITUTE OF TECHNOLOGY, BENGALURU<br />#14, Raghuvanahalli, Kanakapura Main Road, Bengaluru-560019<br />Department Of Mechanical Engineering";
			$(prefix + ' .modal-title').html(title);
		}else if(feedbackPage == "tele_page"){
			$(prefix + ' span[name="departmentName"]').text("Electronics and TeleCommunication");
			let title = "K. S INSTITUTE OF TECHNOLOGY, BENGALURU<br />#14, Raghuvanahalli, Kanakapura Main Road, Bengaluru-560019<br />Department Of Electronics & Telecommunication Engineering";
			$(prefix + ' .modal-title').html(title);
		}else if(feedbackPage == "ece_page"){
			$(prefix + ' span[name="departmentName"]').text("Electronics And Communication Engineering");
			let title = "K. S INSTITUTE OF TECHNOLOGY, BENGALURU<br />#14, Raghuvanahalli, Kanakapura Main Road, Bengaluru-560019<br />Department Of Electronics & Communication Engineering";
			$(prefix + ' .modal-title').html(title);
		}
		
		
		if(this.value == "PARENT"){
			$("#feedbackParentModal").modal("show");
		}else if(this.value == "STUDENT"){
			$("#feedbackStudentModal").modal("show");
		}else if(this.value == "ALUMNI"){
			$("#feedbackAlumniModal").modal("show");
		}
		else if(this.value == "EMPLOYER"){
			$("#feedbackEmloyerModal").modal("show");
		}
		
		$('#feedbackList').val("");
	});
});