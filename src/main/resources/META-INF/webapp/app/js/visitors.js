const countEl = document.getElementById('CounterVisitor');

updateVisitCount();

function updateVisitCount() {
	fetch("/api/v1/visit/update/ksit/visits?amount=1")
	.then(res => res.json())
	.then(res => {
		countEl.innerHTML = res.value;
	})
}