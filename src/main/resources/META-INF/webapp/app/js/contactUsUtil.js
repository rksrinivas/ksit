
function deleteContactUs(id){
	var params = {
		id: id
	};
	var url = "/api/v1/contactUs/deleteContactUs";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},

		success : function(contactUs) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}


function deleteFeedback(id){
	var params = {
		id: id
	};
	var url = "/api/v1/contactUs/deleteFeedback";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},

		success : function(contactUs) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}