$(document).ready(function(){
	var editLibraryModalId = "editLibraryModal";
	var addLibraryModalId = "addLibraryModal";
	
	$("#"+editLibraryModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		
		var id = source.data("id");
		var heading = $("#"+id+"Heading").text();
		var content = $("#"+id+"Content").text();
		var active = $("#"+id+"Active").text();
		var rank = $("#"+id+"Rank").text();
		
		rank = Number.parseInt(rank);
		
		$("#idEditValue"+editLibraryModalId).val(id);
		if(heading){
			$("#headingEditValue"+editLibraryModalId).val(heading);
		}
		if(content){
			$("#contentEditValue"+editLibraryModalId).val(content);
		}
		
		$("#activeEditValue"+editLibraryModalId).val(active);
		$("#rankEditValue"+editLibraryModalId).val(rank);
		
	});
	
	$("#"+addLibraryModalId).on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");
		
		$("#pageAddValue"+addLibraryModalId).val(page);
		$("#fieldNameAddValue"+addLibraryModalId).val(fieldName);
	});  
	
	$("#cancelEdit"+editLibraryModalId).on('click', function(e) {
		$("#"+editLibraryModalId).modal("hide");
	});
	$("#saveEdit"+editLibraryModalId).on('click', function(e){
		editLibrary();
	});
	
	$("#cancelAdd"+addLibraryModalId).on('click', function(e){
		$("#"+addLibraryModalId).modal("hide");
	});
	
	$("#saveAdd"+addLibraryModalId).on("click", function(e){
		addLibrary();
	});
});

function editLibrary(){
	var editLibraryModalId = "editLibraryModal";
	
	var idEditValue = $("#idEditValue"+editLibraryModalId).val();
	var headingEditValue = $("#headingEditValue"+editLibraryModalId).val();
	var contentEditValue = $("#contentEditValue"+editLibraryModalId).val();
	var eventDateEditValue = $("#eventDateEditValue"+editLibraryModalId).val();
	var activeEditValue = $("#activeEditValue"+editLibraryModalId).val();
	var rankEditValue = $("#rankEditValue"+editLibraryModalId).val();
	
	var formData = new FormData();
	
	if(idEditValue){
		formData.append('id', idEditValue);
	}
	if(headingEditValue){
		formData.append('heading', headingEditValue);
	}
	if(contentEditValue){
		formData.append('content', contentEditValue);
	}
	
	if(activeEditValue){
		formData.append("active", activeEditValue);
	}
	if(rankEditValue){
		formData.append("rank", rankEditValue);
	}
	
	var url = "/api/v1/event/editEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false, 
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addLibrary(){
	var addLibraryModalId = "addLibraryModal";
	
	var pageAddValue = $("#pageAddValue"+addLibraryModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addLibraryModalId).val();
	var headingAddValue = $("#headingAddValue"+addLibraryModalId).val();
	var contentAddValue = $("#contentAddValue"+addLibraryModalId).val();
	var activeAddValue = $("#activeAddValue"+addLibraryModalId).val();
	var rankAddValue = $("#rankAddValue"+addLibraryModalId).val();
	
	var formData = new FormData();
	
	formData.append('page', pageAddValue);
	formData.append("fieldName", fieldNameAddValue);
	formData.append('heading', headingAddValue);
	formData.append('content', contentAddValue);
	formData.append("active", activeAddValue);
	formData.append("rank", rankAddValue);
	
	
	var url = "/api/v1/event/addEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}


function deleteLibrary(id){
	var params = {
		id: id
	};
	var url = "/api/v1/event/deleteEvent";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function(event) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}