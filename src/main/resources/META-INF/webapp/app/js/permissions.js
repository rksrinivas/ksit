$(document).ready(function(){
	$('#add').click(function() {
		return !$('#select1 option:selected').remove().appendTo('#select2');
	});

	$('#select1').dblclick(function() {
		return !$('#select1 option:selected').remove().appendTo('#select2');
	});

	$('#remove').click(function() {
		return !$('#select2 option:selected').remove().appendTo('#select1');
	});

	$('#select2').dblclick(function() {
		return !$('#select2 option:selected').remove().appendTo('#select1');
	});
});

function viewUsersRole() {
	$("#displayUsers tr").remove();
	var url = "viewUsers?page="+$("#page").val();
	$.ajax({
		url: url,
		success: function(resp) {
			$.each(resp,function(i,item) {
				$('<tr>').append(
       				$('<td>').text(item)
        		).appendTo('#displayUsers');
			});
		},
		error:function(resp){
		}
	});
}

function grantRole() {
	var newRolesArray=new Array();
	var select2=document.getElementById('select2');
	for(i=0;i<select2.options.length;i++) {
		newRolesArray[i]=select2.options[i].value;
	}
	var username=$('#changerole #username').val();
	$.ajax({
		url:'grantRole?username='+username+'&newRoles='+newRolesArray,
		success: function(data) {
			alert(data.message);
		}
	});
}

function loadExisting() {
	var username = $('#changerole #username').val();
	document.getElementById("select1").options.length = 0;
	document.getElementById("select2").options.length = 0;
	var url = "loadexisting?username="+$("#username").val();
	$.ajax({
		url: url,
		success: function(data) {
			for(i=0;i<data.existingPageTypeList.length;i++) {
				$('#select2').append($('<option>', {
					value: data.existingPageTypeList[i],
					text: data.existingPageTypeList[i]
				}));
			}
			for(i=0;i<data.remainingPageTypeList.length;i++) {
				$('#select1').append($('<option>', {
					value: data.remainingPageTypeList[i],
					text: data.remainingPageTypeList[i]
				}));
			}
		}
	});
}