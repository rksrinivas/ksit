$(document).ready(function() {
	var editPlacementTrainingModalId = "editPlacementTrainingModal";
	var addPlacementTrainingModalId = "addPlacementTrainingModal";
	
	$("#"+editPlacementTrainingModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);

		var id = source.data("id");
		var company = $("#" + id + "Company").text();
		var year = $("#" + id + "year").text();
		var program = $("#" + id + "program").text();
		var semester = $("#" + id + "semester").text();
		var schedule = $("#" + id + "schedule").text();

		var active = $("#" + id + "Active").text();
		var rank = $("#" + id + "Rank").text();

		rank = Number.parseInt(rank);

		$("#idEditValue"+editPlacementTrainingModalId).val(id);
		if (company) {
			$("#companyEditValue"+editPlacementTrainingModalId).val(company);
		}
		
		if (year){
			$("#yearEditValue"+editPlacementTrainingModalId).val(year);
		}		
		if(program){
			$("#programEditValue"+editPlacementTrainingModalId).val(program);
		}
		if(semester){
			$("#semesterEditValue"+editPlacementTrainingModalId).val(semester);
		}
		if(schedule){
			$("#scheduleEditValue"+editPlacementTrainingModalId).val(schedule);
		}
		
		
		$("#activeEditValue"+editPlacementTrainingModalId).val(active);
		$("#rankEditValue"+editPlacementTrainingModalId).val(rank);

	});

	$("#"+addPlacementTrainingModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");

		$("#pageAddValue"+addPlacementTrainingModalId).val(page);
		$("#fieldNameAddValue"+addPlacementTrainingModalId).val(fieldName);
	});

	$("#cancelEdit"+editPlacementTrainingModalId).on('click', function(e) {
		$("#"+editPlacementTrainingModalId).modal("hide");
	});
	$("#saveEdit"+editPlacementTrainingModalId).on('click', function(e){
		editPlacementTraining();
	});
	
	$("#cancelAdd"+addPlacementTrainingModalId).on('click', function(e){
		$("#"+addPlacementTrainingModalId).modal("hide");
	});
	
	$("#saveAdd"+addPlacementTrainingModalId).on("click", function(e){
		addPlacementTraining();
	});
});

function editPlacementTraining() {
	var editPlacementTrainingModalId = "editPlacementTrainingModal";
	
	var idEditValue = $("#idEditValue"+editPlacementTrainingModalId).val();
	
	var companyEditValue = $("#companyEditValue"+editPlacementTrainingModalId).val();
	var yearEditValue = $("#yearEditValue"+editPlacementTrainingModalId).val();
	var programEditValue = $("#programEditValue"+editPlacementTrainingModalId).val();
	var semesterEditValue = $("#semesterEditValue"+editPlacementTrainingModalId).val();
	var scheduleEditValue = $("#scheduleEditValue"+editPlacementTrainingModalId).val();
	
	var activeEditValue = $("#activeEditValue"+editPlacementTrainingModalId).val();
	var rankEditValue = $("#rankEditValue"+editPlacementTrainingModalId).val();

	var form = new FormData();
	
	
	if (idEditValue) {
		form.append("id", idEditValue);
	}
	if (companyEditValue) {
		form.append("company", companyEditValue);
	}
	
	if (yearEditValue){
		form.append("year", yearEditValue);
	}
	if(programEditValue){
		form.append("program", programEditValue);
	}
	
	if(semesterEditValue){
		form.append("semester", semesterEditValue);
	}
	if(scheduleEditValue){
		form.append("schedule", scheduleEditValue);
	}
	
	if (activeEditValue) {
		form.append("active", activeEditValue);
	}
	if (rankEditValue) {
		form.append("rank", rankEditValue);
	}

	var url = "/api/v1/placement/editPlacement";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false,
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addPlacementTraining() {
	var addPlacementTrainingModalId = "addPlacementTrainingModal";
	
	var pageAddValue = $("#pageAddValue"+addPlacementTrainingModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addPlacementTrainingModalId).val();
	
	var companyAddValue = $("#companyAddValue"+addPlacementTrainingModalId).val();
	var yearAddValue = $("#yearAddValue"+addPlacementTrainingModalId).val();
	var programAddValue = $("#programAddValue"+addPlacementTrainingModalId).val();
	var semesterAddValue = $("#semesterAddValue"+addPlacementTrainingModalId).val();
	var scheduleAddValue = $("#scheduleAddValue"+addPlacementTrainingModalId).val();	
	var activeAddValue = $("#activeAddValue"+addPlacementTrainingModalId).val();
	var rankAddValue = $("#rankAddValue"+addPlacementTrainingModalId).val();

	var form = new FormData();
	
	if (pageAddValue) {
		form.append("page", pageAddValue);
	}
	if (fieldNameAddValue) {
		form.append("fieldName", fieldNameAddValue);
	}
	
	if (companyAddValue) {
		form.append("company", companyAddValue);
	}
	
	if(yearAddValue){
		form.append("year", yearAddValue);
	}
	if(programAddValue){
		form.append("program", programAddValue);
	}
	if(semesterAddValue){
		form.append("semester", semesterAddValue);
	}
	if(scheduleAddValue){
		form.append("schedule", scheduleAddValue);
	}
	
	if (activeAddValue) {
		form.append("active", activeAddValue);
	}
	if (rankAddValue) {
		form.append("rank", rankAddValue);
	}

	var url = "/api/v1/placement/addPlacement";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false, 
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function deletePlacementTraining(id) {
	var params = {
		id : id
	};
	var url = "/api/v1/placement/deletePlacement";
	$.ajax({
		url : url,
		data : params,
		type : 'POST',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded'
		},

		success : function(Placement) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}