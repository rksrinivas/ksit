$(document).ready(function() {
	var editPlacementModalId = "editPlacementModal";
	var addPlacementModalId = "addPlacementModal";
	
	$("#"+editPlacementModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);

		var id = source.data("id");
		var company = $("#" + id + "Company").text();
		var salary = $("#" + id + "Salary").text();
		var noOfStudents = $("#" + id + "noOfStudents").text();
		var year = $("#" + id + "year").text();
		var program = $("#" + id + "program").text();
		var semester = $("#" + id + "semester").text();
		var schedule = $("#" + id + "schedule").text();

		var active = $("#" + id + "Active").text();
		var rank = $("#" + id + "Rank").text();

		rank = Number.parseInt(rank);

		$("#idEditValue"+editPlacementModalId).val(id);
		if (company) {
			$("#companyEditValue"+editPlacementModalId).val(company);
		}
		if (salary) {
			$("#salaryEditValue"+editPlacementModalId).val(salary);
		}
		if (noOfStudents) {
			$("#noOfStudentsEditValue"+editPlacementModalId).val(noOfStudents);
		}
		if (year){
			$("#yearEditValue"+editPlacementModalId).val(year);
		}		
		if(program){
			$("#programEditValue"+editPlacementModalId).val(program);
		}
		if(semester){
			$("#semesterEditValue"+editPlacementModalId).val(semester);
		}
		if(schedule){
			$("#scheduleEditValue"+editPlacementModalId).val(schedule);
		}
		
		
		$("#activeEditValue"+editPlacementModalId).val(active);
		$("#rankEditValue"+editPlacementModalId).val(rank);

	});

	$("#"+addPlacementModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");

		$("#pageAddValue"+addPlacementModalId).val(page);
		$("#fieldNameAddValue"+addPlacementModalId).val(fieldName);
	});

	$("#cancelEdit"+editPlacementModalId).on('click', function(e) {
		$("#"+editPlacementModalId).modal("hide");
	});
	$("#saveEdit"+editPlacementModalId).on('click', function(e){
		editPlacement();
	});
	
	$("#cancelAdd"+addPlacementModalId).on('click', function(e){
		$("#"+addPlacementModalId).modal("hide");
	});
	
	$("#saveAdd"+addPlacementModalId).on("click", function(e){
		addPlacement();
	});
});

function editPlacement() {
	var editPlacementModalId = "editPlacementModal";
	
	var idEditValue = $("#idEditValue"+editPlacementModalId).val();
	
	var companyEditValue = $("#companyEditValue"+editPlacementModalId).val();
	var salaryEditValue = $("#salaryEditValue"+editPlacementModalId).val();
	var noOfStudentsEditValue = $("#noOfStudentsEditValue"+editPlacementModalId).val();	
	var yearEditValue = $("#yearEditValue"+editPlacementModalId).val();
	var programEditValue = $("#programEditValue"+editPlacementModalId).val();
	var semesterEditValue = $("#semesterEditValue"+editPlacementModalId).val();
	var scheduleEditValue = $("#scheduleEditValue"+editPlacementModalId).val();
	
	var activeEditValue = $("#activeEditValue"+editPlacementModalId).val();
	var rankEditValue = $("#rankEditValue"+editPlacementModalId).val();

	var form = new FormData();
	if ($("#imageEditValue"+editPlacementModalId)[0].files.length > 0) {
		form.append("img", $("#imageEditValue"+editPlacementModalId)[0].files[0]);
	}
	
	if (idEditValue) {
		form.append("id", idEditValue);
	}
	if (companyEditValue) {
		form.append("company", companyEditValue);
	}
	if (salaryEditValue) {
		form.append("salary", salaryEditValue);
	}
	if (noOfStudentsEditValue) {
		form.append("noOfStudents", noOfStudentsEditValue);
	}
	if (yearEditValue){
		form.append("year", yearEditValue);
	}
	if(programEditValue){
		form.append("program", programEditValue);
	}
	
	if(semesterEditValue){
		form.append("semester", semesterEditValue);
	}
	if(scheduleEditValue){
		form.append("schedule", scheduleEditValue);
	}
	
	if (activeEditValue) {
		form.append("active", activeEditValue);
	}
	if (rankEditValue) {
		form.append("rank", rankEditValue);
	}

	var url = "/api/v1/placement/editPlacement";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false,
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addPlacement() {
	var addPlacementModalId = "addPlacementModal";
	
	var pageAddValue = $("#pageAddValue"+addPlacementModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addPlacementModalId).val();
	
	var companyAddValue = $("#companyAddValue"+addPlacementModalId).val();
	var salaryAddValue = $("#salaryAddValue"+addPlacementModalId).val();
	var noOfStudentsAddValue = $("#noOfStudentsAddValue"+addPlacementModalId).val();
	var yearAddValue = $("#yearAddValue"+addPlacementModalId).val();
	var programAddValue = $("#programAddValue"+addPlacementModalId).val();
	var semesterAddValue = $("#semesterAddValue"+addPlacementModalId).val();
	var scheduleAddValue = $("#scheduleAddValue"+addPlacementModalId).val();	
	var activeAddValue = $("#activeAddValue"+addPlacementModalId).val();
	var rankAddValue = $("#rankAddValue"+addPlacementModalId).val();

	var form = new FormData();
	if ($("#imageAddValue"+addPlacementModalId)[0].files.length > 0) {
		form.append("img", $("#imageAddValue"+addPlacementModalId)[0].files[0]);
	}
	
	if (pageAddValue) {
		form.append("page", pageAddValue);
	}
	if (fieldNameAddValue) {
		form.append("fieldName", fieldNameAddValue);
	}
	
	if (companyAddValue) {
		form.append("company", companyAddValue);
	}
	if (salaryAddValue) {
		form.append("salary", salaryAddValue);
	}
	if (noOfStudentsAddValue) {
		form.append("noOfStudents", noOfStudentsAddValue);
	}
	if(yearAddValue){
		form.append("year", yearAddValue);
	}
	if(programAddValue){
		form.append("program", programAddValue);
	}
	if(semesterAddValue){
		form.append("semester", semesterAddValue);
	}
	if(scheduleAddValue){
		form.append("schedule", scheduleAddValue);
	}
	
	if (activeAddValue) {
		form.append("active", activeAddValue);
	}
	if (rankAddValue) {
		form.append("rank", rankAddValue);
	}

	var url = "/api/v1/placement/addPlacement";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false, 
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function deletePlacement(id) {
	var params = {
		id : id
	};
	var url = "/api/v1/placement/deletePlacement";
	$.ajax({
		url : url,
		data : params,
		type : 'POST',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded'
		},

		success : function(Placement) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}