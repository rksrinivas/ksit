$(document).ready(function() {
	var editDeptLabModalId = "editDeptLabModal";
	var addDeptLabModalId = "addDeptLabModal";
	
	$("#"+editDeptLabModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);

		var id = source.data("id");
		var name = $("#" + id + "Name").text();
		var designation = $("#" + id + "Designation").text();
	
		var active = $("#" + id + "Active").text();
		var rank = $("#" + id + "Rank").text();

		rank = Number.parseInt(rank);

		$("#idEditValue"+editDeptLabModalId).val(id);
		if (name) {
			$("#nameEditValue"+editDeptLabModalId).val(name);
		}
		if (designation) {
			$("#designationEditValue"+editDeptLabModalId).val(designation);
		}
		
		$("#activeEditValue"+editDeptLabModalId).val(active);
		$("#rankEditValue"+editDeptLabModalId).val(rank);

	});

	$("#"+addDeptLabModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");

		$("#pageAddValue"+addDeptLabModalId).val(page);
		$("#fieldNameAddValue"+addDeptLabModalId).val(fieldName);
	});

	$("#cancelEdit"+editDeptLabModalId).on('click', function(e) {
		$("#"+editDeptLabModalId).modal("hide");
	});
	$("#saveEdit"+editDeptLabModalId).on('click', function(e){
		editDeptLab();
	});
	
	$("#cancelAdd"+addDeptLabModalId).on('click', function(e){
		$("#"+addDeptLabModalId).modal("hide");
	});
	
	$("#saveAdd"+addDeptLabModalId).on("click", function(e){
		addDeptLab();
	});
});

function editDeptLab() {
	var editDeptLabModalId = "editDeptLabModal";
	
	var idEditValue = $("#idEditValue"+editDeptLabModalId).val();
	var nameEditValue = $("#nameEditValue"+editDeptLabModalId).val();
	var designationEditValue = $("#designationEditValue"+editDeptLabModalId).val();
	var activeEditValue = $("#activeEditValue"+editDeptLabModalId).val();
	var rankEditValue = $("#rankEditValue"+editDeptLabModalId).val();

	var form = new FormData();
	if ($("#imageEditValue"+editDeptLabModalId)[0].files.length > 0) {
		form.append("img", $("#imageEditValue"+editDeptLabModalId)[0].files[0]);
	}
	if ($("#profileEditValue"+editDeptLabModalId)[0].files.length > 0) {
		form.append("profile", $("#profileEditValue"+editDeptLabModalId)[0].files[0]);
	}
	if (idEditValue) {
		form.append("id", idEditValue);
	}
	if (nameEditValue) {
		form.append("name", nameEditValue);
	}
	if (designationEditValue) {
		form.append("designation", designationEditValue);
	}
	
	if (activeEditValue) {
		form.append("active", activeEditValue);
	}
	if (rankEditValue) {
		form.append("rank", rankEditValue);
	}

	var url = "/api/v1/faculty/editFaculty";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false,
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addDeptLab() {
	var addDeptLabModalId = "addDeptLabModal";
	
	var pageAddValue = $("#pageAddValue"+addDeptLabModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addDeptLabModalId).val();
	var nameAddValue = $("#nameAddValue"+addDeptLabModalId).val();
	var designationAddValue = $("#designationAddValue"+addDeptLabModalId).val();
	var activeAddValue = $("#activeAddValue"+addDeptLabModalId).val();
	var rankAddValue = $("#rankAddValue"+addDeptLabModalId).val();

	var form = new FormData();
	if ($("#imageAddValue"+addDeptLabModalId)[0].files.length > 0) {
		form.append("img", $("#imageAddValue"+addDeptLabModalId)[0].files[0]);
	}
	if ($("#profileAddValue"+addDeptLabModalId)[0].files.length > 0) {
		form.append("profile", $("#profileAddValue"+addDeptLabModalId)[0].files[0]);
	}
	if (pageAddValue) {
		form.append("page", pageAddValue);
	}
	if (fieldNameAddValue) {
		form.append("fieldName", fieldNameAddValue);
	}
	if (nameAddValue) {
		form.append("name", nameAddValue);
	}
	if (designationAddValue) {
		form.append("designation", designationAddValue);
	}
	
	if (activeAddValue) {
		form.append("active", activeAddValue);
	}
	if (rankAddValue) {
		form.append("rank", rankAddValue);
	}

	var url = "/api/v1/faculty/addFaculty";
	$.ajax({
		url : url,
		type : 'POST',
		data : form,
		processData : false, 
		contentType : false,
		success : function(data) {
			reloadPage();
		}
	});
}

function deleteDeptLab(id) {
	var params = {
		id : id
	};
	var url = "/api/v1/faculty/deleteFaculty";
	$.ajax({
		url : url,
		data : params,
		type : 'POST',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded'
		},

		success : function(faculty) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}