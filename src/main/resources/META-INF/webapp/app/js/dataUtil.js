$(document).ready(function(){
	var editDataModalId = "editDataModal";
	var addDataModalId = "addDataModal";

	$("#"+editDataModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);

		var id = source.data("id");
		var active = $("#"+id+"Active").text();
		var content = $("#"+id+"Content").text();
		var rank = $("#"+id+"Rank").text();

		rank = Number.parseInt(rank);

		$("#idEditValue"+editDataModalId).val(id);
		$("#contentEditValue"+editDataModalId).val(content);
		$("#activeEditValue"+editDataModalId).val(active);
		$("#rankEditValue"+editDataModalId).val(rank);

	}); 

	$("#"+addDataModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");

		$("#pageAddValue"+addDataModalId).val(page);
		$("#fieldNameAddValue"+addDataModalId).val(fieldName);
	});

	$("#cancelEdit"+editDataModalId).on('click', function(e) {
		$("#"+editDataModalId).modal("hide");
	});
	$("#saveEdit"+editDataModalId).on('click', function(e){
		editData();
	});

	$("#cancelAdd"+addDataModalId).on('click', function(e) {
		$("#"+addDataModalId).modal("hide");
	});
	$("#saveAdd"+addDataModalId).on("click", function(e){
		addData();
	});
});

function editData(){
	var editDataModalId = "editDataModal";

	var idEditValue = $("#idEditValue"+editDataModalId).val();
	var contentEditValue = $("#contentEditValue"+editDataModalId).val();
	var activeEditValue = $("#activeEditValue"+editDataModalId).val();
	var rankEditValue = $("#rankEditValue"+editDataModalId).val();

	var formData = new FormData();
	formData.append('id', idEditValue);
	formData.append('content', contentEditValue);
	formData.append("active", activeEditValue);
	formData.append("rank", rankEditValue);

	var url = "/api/v1/data/editData";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addData(){
	var addDataModalId = "addDataModal";

	var pageAddValue = $("#pageAddValue"+addDataModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addDataModalId).val();
	var contentAddValue = $("#contentAddValue"+addDataModalId).val();
	var activeAddValue = $("#activeAddValue"+addDataModalId).val();
	var rankAddValue = $("#rankAddValue"+addDataModalId).val();

	var formData = new FormData();
	formData.append('page', pageAddValue);
	formData.append("fieldName", fieldNameAddValue);
	formData.append('content', contentAddValue);
	formData.append("active", activeAddValue);
	formData.append("rank", rankAddValue);


	var url = "/api/v1/data/addData";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false, 
		success : function(data) {
			reloadPage();
		}
	});
}

function deleteData(id){
	var params = {
		id: id
	};
	var url = "/api/v1/data/deleteData";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},

		success : function(data) {
			// var mp3_url = 'https://media.geeksforgeeks.org/wp-content/uploads/20190531135120/beep.mp3';
			// (new Audio(mp3_url)).play();
			alert("Successfully deleted data");
			reloadPage();
		}
	});

}