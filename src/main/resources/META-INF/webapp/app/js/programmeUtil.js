$(document).ready(function() {
	$("#addProgrammeModal").on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var programmeType = source.data("type");		
		$("#programmeTypeProgrammeData").val(programmeType);
	});  
	
    $("#cancelAddProgrammeData").on('click', function(e) {
    	$("#addProgrammeModal").modal("hide")
    });
    
    $("#saveAddProgrammeData").on('click', function(e) {
    	addProgrammeData();
    });
});

function addProgrammeData(){
	var formData = new FormData();
	formData.append('department', $("#departmentProgrammeData").val());	
	formData.append("programmeType", $("#programmeTypeProgrammeData").val());
	var url = "/api/v1/programme/addProgramme";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function() {
			reloadPage();
		}
	});
}

function deleteProgrammeData(id){
	var params = {
		id: id
	};
	var url = "/api/v1/programme/deleteProgrammeData";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function() {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}