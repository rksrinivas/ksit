$(document).ready(function(){
	var editSliderModalId = "editSliderModal";
	var addSliderModalId = "addSliderModal";
	
	$("#"+editSliderModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		
		var id = source.data("id");
		var heading = $("#"+id+"Heading").text();
		var content = $("#"+id+"Content").text();
		var active = $("#"+id+"Active").text();
		var rank = $("#"+id+"Rank").text();
		
		rank = Number.parseInt(rank);
		
		$("#idEditValue"+editSliderModalId).val(id);
		if(heading){
			$("#headingEditValue"+editSliderModalId).val(heading);
		}
		if(content){
			$("#contentEditValue"+editSliderModalId).val(content);
		}
		$("#activeEditValue"+editSliderModalId).val(active);
		$("#rankEditValue"+editSliderModalId).val(rank);
		
	});
	
	$("#"+addSliderModalId).on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");
		
		$("#pageAddValue"+addSliderModalId).val(page);
		$("#fieldNameAddValue"+addSliderModalId).val(fieldName);
	});  
	
	$("#cancelEdit"+editSliderModalId).on('click', function(e) {
		$("#"+editSliderModalId).modal("hide");
	});
	$("#saveEdit"+editSliderModalId).on('click', function(e){
		editSider();
	});
	
	$("#cancelAdd"+addSliderModalId).on('click', function(e){
		$("#"+addSliderModalId).modal("hide");
	});
	
	$("#saveAdd"+addSliderModalId).on("click", function(e){
		addSlider();
	});
});

function editSider(){
	var editSliderModalId = "editSliderModal";
	
	var idEditValue = $("#idEditValue"+editSliderModalId).val();
	var headingEditValue = $("#headingEditValue"+editSliderModalId).val();
	var contentEditValue = $("#contentEditValue"+editSliderModalId).val();
	var activeEditValue = $("#activeEditValue"+editSliderModalId).val();
	var rankEditValue = $("#rankEditValue"+editSliderModalId).val();
	
	var formData = new FormData();
	if( $('#imageEditValue'+editSliderModalId)[0].files.length > 0){
		formData.append('img', $('#imageEditValue'+editSliderModalId)[0].files[0]);
	}
	if(idEditValue){
		formData.append('id', idEditValue);
	}
	if(headingEditValue){
		formData.append('heading', headingEditValue);
	}
	if(contentEditValue){
		formData.append('content', contentEditValue);
	}
	if(activeEditValue){
		formData.append("active", activeEditValue);
	}
	if(rankEditValue){
		formData.append("rank", rankEditValue);
	}
	
	var url = "/api/v1/event/editEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false, 
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addSlider(){
	var addSliderModalId = "addSliderModal";
	
	var pageAddValue = $("#pageAddValue"+addSliderModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addSliderModalId).val();
	var headingAddValue = $("#headingAddValue"+addSliderModalId).val();
	var contentAddValue = $("#contentAddValue"+addSliderModalId).val();
	var activeAddValue = $("#activeAddValue"+addSliderModalId).val();
	var rankAddValue = $("#rankAddValue"+addSliderModalId).val();
	
	var formData = new FormData();
	if( $("#imageAddValue"+addSliderModalId)[0].files.length > 0){
		formData.append("img", $("#imageAddValue"+addSliderModalId)[0].files[0]);
	}
	formData.append('page', pageAddValue);
	formData.append("fieldName", fieldNameAddValue);
	formData.append('heading', headingAddValue);
	formData.append('content', contentAddValue);
	formData.append("active", activeAddValue);
	formData.append("rank", rankAddValue);
	
	
	var url = "/api/v1/event/addEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}


function deleteSlider(id){
	var params = {
		id: id
	};
	var url = "/api/v1/event/deleteEvent";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function(event) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}