$(document).ready(function(){
	
	$(function(){
	  var hash = window.location.hash;
	  hash && $('ul.nav a[href="' + hash + '"]').tab('show');
	  
	  if (window.location.href.indexOf('?') >= 0) {
		  var offsetToScroll = '';
		  $('.nav-tabs li a').each(function() {
			  if(this.href == window.location.href){
				  $(this).parent().addClass('active');
				  offsetToScroll = $(this).offset().top
			  }else{
				  $(this).parent().removeClass('active');
			  }
		  });
		  if(offsetToScroll){
				$('html, body').animate({
			        scrollTop: offsetToScroll
			    }, 1000);
		  }
	   }
	   
	    var expression = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/gi;
	    var scrollingTextTop = $(".news").text();
	    if(scrollingTextTop){
		    var matches = scrollingTextTop.match(expression);
		    if(matches.length > 0){
				$('.news').click(function(){
					window.open(matches[0], '_blank');
				});
		    }
		}

	    
	});
	
	jQuery(function($){
	
	
		/* ----------------------------------------------------------- */
	  /*  1. DROPDOWN MENU
	  /* ----------------------------------------------------------- */
	
	   // for hover dropdown menu
	  $('ul.nav li.dropdown').hover(function() {
	      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
	    }, function() {
	      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
	    });
		
		/* ----------------------------------------------------------- */
		/*  2. SUPERSLIDES SLIDER
		/* ----------------------------------------------------------- */
		$('#slides').superslides({
	      animation: 'fade',
	      animation_easing: 'linear',
	      pagination: 'true'
	    });
		
		/* ----------------------------------------------------------- */
		/*  3. NEWS SLIDER
		/* ----------------------------------------------------------- */
		$('.single_notice_pane').slick({     
	      slide: 'ul'
	      
	    });
	    $('[href="#notice"]').on('shown.bs.tab', function (e) {
	    $('.single_notice_pane').resize();
		});
		 $('[href="#news"]').on('shown.bs.tab', function (e) {
	    $('.single_notice_pane').resize();
		});   
	    
	
	
		/* ----------------------------------------------------------- */
		/*  4. SKILL CIRCLE
		/* ----------------------------------------------------------- */
	
		$('#myStathalf').circliful();
		$('#myStat').circliful();
		$('#myStathalf2').circliful();
		$('#myStat2').circliful();
		$('#myStat3').circliful();
		$('#myStat4').circliful();
		$('#myStathalf3').circliful();
	
		/* ----------------------------------------------------------- */
		/*  5. WOW SMOOTH ANIMATIN
		/* ----------------------------------------------------------- */
	
		wow = new WOW(
	      {
	        animateClass: 'animated',
	        offset:100
	      }
	    );
	    wow.init();
	
	
		/* ----------------------------------------------------------- */
		/*  6. COURSE SLIDER
		/* ----------------------------------------------------------- */
		
	    $('.course_nav').slick({
		  dots: false,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 3,
		  arrows:true,  
		  slidesToScroll: 3,
		  slide: 'li',
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
	
		/* ----------------------------------------------------------- */
		/*  7. TUTORS SLIDER
		/* ----------------------------------------------------------- */
	
		 $('.tutors_nav').slick({
		  dots: true,	  
		  infinite: true,
		  speed: 300,
		  slidesToShow: 5,
		  arrows:false,  
		  slidesToScroll: 1,
		  slide: 'li',
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        infinite: true,
		        arrows:false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
			
	
		/* ----------------------------------------------------------- */
		/*  8. BOOTSTRAP TOOLTIP
		/* ----------------------------------------------------------- */
			$('.soc_tooltip').tooltip('hide')
		/* ----------------------------------------------------------- */
		/*  9. PRELOADER 
		/* ----------------------------------------------------------- */
		 /* window.addEventListener('DOMContentLoaded', function() {
	        new QueryLoader2(document.querySelector("body"), {
	            barColor: "#efefef",
	            backgroundColor: "#111",
	            percentage: true,
	            barHeight: 1,
	            minimumTime: 200,
	            fadeOutTime: 1000
	        });
	    });*/
	
	    /* ----------------------------------------------------------- */
		/*  10. EVENTS SLIDER
		/* ----------------------------------------------------------- */

		$('.events_slider').slick({
		  dots: true,
		  infinite: true,
		  speed: 500,
		  fade: true,
		  cssEase: 'linear'
		});
	
		/* ----------------------------------------------------------- */
		/*  12. SCROLL UP BUTTON
		/* ----------------------------------------------------------- */
	
		//Check to see if the window is top if not then display button
	
		  $(window).scroll(function(){
		    if ($(this).scrollTop() > 300) {
		      $('#scrollToTop').fadeIn();
		    } else {
		      $('#scrollToTop').fadeOut();
		    }
		  });
		   
		  //Click event to scroll to top
	
		  $('#scrollToTop').click(function(){
		    $('html, body').animate({scrollTop : 0},2000);
		    return false;
		  });
		
	});
	
	//navbar fixed
	 /* $(window).scroll(function(){
	      if ($(this).scrollTop() > 100) {
	          $('#navbar').addClass('fixed');
	      } else {
	          $('#navbar').removeClass('fixed');
	      }
	  });*/
});
  
  /* tool tip*/

$(document).ready(function(){
	if($('[data-toggle="popover"]').length){
		$('[data-toggle="popover"]').popover();
	}
});


/*achievers slider*/
$(document).ready(function(){
	if($("#testimonial-slider").length){
		$("#testimonial-slider").owlCarousel({
	        items:1,
	        itemsDesktop:[1000,1],
	        itemsDesktopSmall:[979,1],
	        itemsTablet:[768,1],
	        pagination:true,
	        navigation:false,
	        navigationText:["",""],
	        slideSpeed:1000,
	        singleItem:true,
	        autoPlay:true
	    });
	}
});

/*achievers slider*/

/*marquee*/
$(function(){
    var $mwo = $('.marquee-with-options');
    try{
	    $('.marquee').marquee();
	    $('.marquee-with-options').marquee({
	    	
	    	//startVisible The marquee will be visible from the start if set to true
	    	startVisible : true,
	        //speed in milliseconds of the marquee
	        speed: 50,
	        //gap in pixels between the tickers
	        gap: 20,
	        //gap in pixels between the tickers
	        delayBeforeStart: 0,
	        //'left' or 'right'
	        direction: 'up',
	        //true or false - should the marquee be duplicated to show an effect of continues flow
	        duplicated: true,
	        //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
	        pauseOnHover: true
	    });
	    //Direction upward
	    $('.marquee-vert').marquee({
	        direction: 'up',
	        speed: 5
	    });
	    //pause and resume links
	    $('.pause').click(function(e){
	        e.preventDefault();
	        $mwo.trigger('pause');
	    });
	    $('.resume').click(function(e){
	        e.preventDefault();
	        $mwo.trigger('resume');
	    });
	    //toggle
	    $('.toggle').hover(function(e){
	        $mwo.trigger('pause');
	    },function(){
	        $mwo.trigger('resume');
	    })
	    .click(function(e){
	        e.preventDefault();
	    })
	}catch(e){
		console.log(e);
	}
});

/*marquee*/

// $('a[href*="#"]').on("click", function (e) {
// 	// 1
// 	e.preventDefault();
// 	// 2
// 	const href = $(this).attr("href");
// 	// 3
// 	$("html, body").animate({ scrollTop: $(href).offset().top }, 500);
// });


 
$(document).ready(function(){
	//jQuery($(".program-grid .program-grid-item")[0]).addClass("active");
	jQuery($(".program-grid .program-grid-item")[0]).trigger("click");
});

function updateDetails(title, imageSrc, description, link) {
	document.getElementById('program-details-title').innerText = title;
	document.getElementById('program-details-image').src = imageSrc;
	document.getElementById('program-details-description').innerText = 
	description;
  document.getElementById("program-details-button").onclick = function () {
		location.href = link;
		};

	// Set active grid item style
	document.querySelectorAll('.program-grid-item').forEach(item => 
		item.classList.remove('active'));
	event.currentTarget.classList.add('active');

// document.querySelectorAll('.program-grid-item').forEach(item => {
// 	item.addEventListner('click',function (event) {
// 		document.querySelectorAll('.program-grid-item').forEach(i => i.classList.remove('active'));
// 		event.currentTarget.classList.add('active');

// 	});
	
// });
  }


 