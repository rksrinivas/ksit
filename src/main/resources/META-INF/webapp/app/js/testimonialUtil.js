$(document).ready(function(){
	var editTestimonialModalId = "editTestimonialModal";
	var addTestimonialModalId = "addTestimonialModal";
	
	$("#"+editTestimonialModalId).on("show.bs.modal", function(e) {
		var source = $(e.relatedTarget);
		
		var id = source.data("id");
		var heading = $("#"+id+"Heading").text();
		var content = $("#"+id+"Content").text();
		var active = $("#"+id+"Active").text();
		var rank = $("#"+id+"Rank").text();
		
		rank = Number.parseInt(rank);
		
		$("#idEditValue"+editTestimonialModalId).val(id);
		if(heading){
			$("#headingEditValue"+editTestimonialModalId).val(heading);
		}
		if(content){
			$("#contentEditValue"+editTestimonialModalId).val(content);
		}
		$("#activeEditValue"+editTestimonialModalId).val(active);
		$("#rankEditValue"+editTestimonialModalId).val(rank);
		
	});
	
	$("#"+addTestimonialModalId).on("show.bs.modal", function(e) { 
		var source = $(e.relatedTarget);
		var page = source.data("page");
		var fieldName = source.data("field_name");
		
		$("#pageAddValue"+addTestimonialModalId).val(page);
		$("#fieldNameAddValue"+addTestimonialModalId).val(fieldName);
	});  
	
	$("#cancelEdit"+editTestimonialModalId).on('click', function(e) {
		$("#"+editTestimonialModalId).modal("hide");
	});
	$("#saveEdit"+editTestimonialModalId).on('click', function(e){
		editTestimonial();
	});
	
	$("#cancelAdd"+addTestimonialModalId).on('click', function(e){
		$("#"+addTestimonialModalId).modal("hide");
	});
	
	$("#saveAdd"+addTestimonialModalId).on("click", function(e){
		addTestimonial();
	});
});

function editTestimonial(){
	var editTestimonialModalId = "editTestimonialModal";
	
	var idEditValue = $("#idEditValue"+editTestimonialModalId).val();
	var headingEditValue = $("#headingEditValue"+editTestimonialModalId).val();
	var contentEditValue = $("#contentEditValue"+editTestimonialModalId).val();
	var activeEditValue = $("#activeEditValue"+editTestimonialModalId).val();
	var rankEditValue = $("#rankEditValue"+editTestimonialModalId).val();
	
	var formData = new FormData();
	if( $('#imageEditValue'+editTestimonialModalId)[0].files.length > 0){
		formData.append('img', $('#imageEditValue'+editTestimonialModalId)[0].files[0]);
	}
	if( $('#reportEditValue'+editTestimonialModalId)[0].files.length > 0){
		formData.append('report', $('#reportEditValue'+editTestimonialModalId)[0].files[0]);
	}
	if(idEditValue){
		formData.append('id', idEditValue);
	}
	if(headingEditValue){
		formData.append('heading', headingEditValue);
	}
	if(contentEditValue){
		formData.append('content', contentEditValue);
	}
	if(activeEditValue){
		formData.append("active", activeEditValue);
	}
	if(rankEditValue){
		formData.append("rank", rankEditValue);
	}
	
	var url = "/api/v1/event/editEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false, 
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}

function addTestimonial(){
	var addTestimonialModalId = "addTestimonialModal";
	
	var pageAddValue = $("#pageAddValue"+addTestimonialModalId).val();
	var fieldNameAddValue = $("#fieldNameAddValue"+addTestimonialModalId).val();
	var headingAddValue = $("#headingAddValue"+addTestimonialModalId).val();
	var contentAddValue = $("#contentAddValue"+addTestimonialModalId).val();
	var activeAddValue = $("#activeAddValue"+addTestimonialModalId).val();
	var rankAddValue = $("#rankAddValue"+addTestimonialModalId).val();
	
	var formData = new FormData();
	if( $("#imageAddValue"+addTestimonialModalId)[0].files.length > 0){
		formData.append("img", $("#imageAddValue"+addTestimonialModalId)[0].files[0]);
	}
	if( $("#reportAddValue"+addTestimonialModalId)[0].files.length > 0){
		formData.append("report", $("#reportAddValue"+addTestimonialModalId)[0].files[0]);
	}
	formData.append('page', pageAddValue);
	formData.append("fieldName", fieldNameAddValue);
	formData.append('heading', headingAddValue);
	formData.append('content', contentAddValue);
	formData.append("active", activeAddValue);
	formData.append("rank", rankAddValue);
	
	
	var url = "/api/v1/event/addEvent";
	$.ajax({
		url : url,
		type : 'POST',
		data : formData,
		processData: false,  
		contentType: false,
		success : function(data) {
			reloadPage();
		}
	});
}


function deleteTestimonial(id){
	var params = {
		id: id
	};
	var url = "/api/v1/event/deleteEvent";
	$.ajax({
		url : url,
		data: params,
		type : 'POST',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		
		success : function(event) {
			alert("Successfully deleted data");
			reloadPage();
		}
	});
	
}