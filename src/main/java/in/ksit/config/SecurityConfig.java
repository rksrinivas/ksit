/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import in.ksit.service.UserService;
import in.ksit.service.ksitUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private DataSource dataSource;

	@Autowired
	private CustomWebAuthenticationDetailsSource authenticationDetailsSource;

	@Autowired
	ksitUserDetailsService ksitUserDetailsService;

	@Autowired
	UserService userService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		CustomDaoAuthenticationProvider authProvider = new CustomDaoAuthenticationProvider(userService);
		authProvider.setPasswordEncoder(passwordEncoder());
		authProvider.setUserDetailsService(ksitUserDetailsService);
		authProvider.setHideUserNotFoundExceptions(false);
		auth.authenticationProvider(authProvider);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/admin/signin/**").permitAll().antMatchers("/admin/signup/**").permitAll()
				.antMatchers("/admin/no-access/**").permitAll().antMatchers("/**/admin/**").authenticated().anyRequest()
				.permitAll();

		http.csrf().disable();
		http.exceptionHandling();
		http.formLogin().loginPage("/admin/signin").defaultSuccessUrl("/admin/index.html", true)
				.authenticationDetailsSource(authenticationDetailsSource);
		http.authorizeRequests().antMatchers("/admin/no-access/**").permitAll();
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		return tokenRepository;
	}
}
