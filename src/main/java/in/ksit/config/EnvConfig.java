/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import in.ksit.filter.ExceptionFilter;


@Configuration
@EnableTransactionManagement
@PropertySources({ @PropertySource("classpath:lib-settings.properties"),
		@PropertySource("classpath:wiring-local.properties") })
public class EnvConfig {

	@Autowired
	private Environment env;

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty("ksit.datasource.driver"));
		dataSource.setUrl(env.getRequiredProperty("ksit.datasource.url"));
		dataSource.setUsername(env.getRequiredProperty("ksit.datasource.username"));
		dataSource.setPassword(env.getRequiredProperty("ksit.datasource.password"));
		return dataSource;
	}

	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("in.ksit.entity");
		sessionBuilder.addProperties(getHibernateProperties());
		return sessionBuilder.buildSessionFactory();
	}

	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.setProperty(AvailableSettings.HBM2DDL_AUTO, env.getRequiredProperty("ksit.hibernate.hbm2ddl.auto"));
		properties.setProperty(AvailableSettings.DIALECT, env.getRequiredProperty("ksit.hibernate.dialect"));
		properties.setProperty(AvailableSettings.SHOW_SQL, env.getRequiredProperty("ksit.hibernate.show_sql"));
		properties.setProperty(AvailableSettings.MERGE_ENTITY_COPY_OBSERVER, "allow");
		properties.setProperty("hibernate.cache.use_second_level_cache", "true");
		properties.setProperty("hibernate.cache.use_query_cache", "false");
		properties.setProperty("hibernate.cache.region.factory_class",
				"org.hibernate.cache.ehcache.EhCacheRegionFactory");
		properties.setProperty("net.sf.ehcache.configurationResourceName", "/ehcache.xml");
		properties.setProperty("hibernate.cache.use_structured_entries", "true");

		return properties;
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setDefaultEncoding("utf-8");
		return resolver;
	}

	@Bean
	public FreeMarkerConfigurer freemarkerConfig() {
		FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
		freeMarkerConfigurer.setTemplateLoaderPath("/views/ftl/");
		freeMarkerConfigurer.setDefaultEncoding("utf-8");
		Properties properties = new Properties();
		List<String> autoIncludeFTL = new ArrayList<String>();
		autoIncludeFTL.add("lib/_layout.ftl");
		autoIncludeFTL.add("lib/eventDisplayMacro.ftl");
		autoIncludeFTL.add("lib/_layoutAdmin.ftl");
		autoIncludeFTL.add("lib/editMacro.ftl");
		autoIncludeFTL.add("lib/addMacro.ftl");
		autoIncludeFTL.add("lib/dataMacro.ftl");
		autoIncludeFTL.add("lib/eventMacro.ftl");
		autoIncludeFTL.add("lib/facultyMacro.ftl");
		autoIncludeFTL.add("lib/achieverMacro.ftl");
		autoIncludeFTL.add("lib/galleryMacro.ftl");
		autoIncludeFTL.add("lib/libraryMacro.ftl");
		autoIncludeFTL.add("lib/testimonialMacro.ftl");
		autoIncludeFTL.add("lib/placementCompanyMacro.ftl");
		autoIncludeFTL.add("lib/placementTrainingMacro.ftl");
		autoIncludeFTL.add("lib/deptLabMacro.ftl");
		autoIncludeFTL.add("lib/feedbackAlumniMacro.ftl");
		autoIncludeFTL.add("lib/feedbackStudentMacro.ftl");
		autoIncludeFTL.add("lib/feedbackParentMacro.ftl");
		autoIncludeFTL.add("lib/feedbackEmployerMacro.ftl");
		autoIncludeFTL.add("lib/sliderMacro.ftl");
		autoIncludeFTL.add("lib/nbaMacro.ftl");
		autoIncludeFTL.add("lib/addNBAMacro.ftl");
		properties.setProperty("auto_include", String.join(", ", autoIncludeFTL));
		freeMarkerConfigurer.setFreemarkerSettings(properties);
		return freeMarkerConfigurer;
	}

	@Bean
	public FreeMarkerViewResolver freemarkerViewResolver() {
		FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
		resolver.setCache(true);
		resolver.setSuffix(".ftl");
		resolver.setExposeSpringMacroHelpers(true);
		resolver.setExposeRequestAttributes(true);
		resolver.setExposeSessionAttributes(true);
		resolver.setAllowSessionOverride(true);
		resolver.setAllowRequestOverride(true);
		return resolver;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public JedisConnectionFactory connectionFactory() {
		JedisConnectionFactory factory = new JedisConnectionFactory();
		factory.setHostName(env.getRequiredProperty("ksit.redis.host"));
		factory.setPort(Integer.parseInt(env.getRequiredProperty("ksit.redis.port")));
		return factory;
	}

	@Bean
	public static ConfigureRedisAction configureRedisAction() {
		return ConfigureRedisAction.NO_OP;
	}
	
	@Bean
	public ExceptionFilter ExceptionFilter() {
		return new ExceptionFilter();
	}
}
