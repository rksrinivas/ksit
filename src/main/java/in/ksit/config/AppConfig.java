/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@Configuration
@EnableRedisHttpSession
@ComponentScan(value = { "in.ksit" })
@EnableAsync
public class AppConfig {
}
