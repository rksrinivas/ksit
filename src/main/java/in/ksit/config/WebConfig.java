/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import in.ksit.interceptor.LoggedInUserModelInterceptor;
import in.ksit.util.AppUtils;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
		registry.addResourceHandler("/css/**").addResourceLocations("/app/css/");
		registry.addResourceHandler("/js/**").addResourceLocations("/app/js/");
		registry.addResourceHandler("/fonts/**").addResourceLocations("/app/fonts/");
		registry.addResourceHandler("/img/**").addResourceLocations("/app/img/");
		registry.addResourceHandler("/bower_components/**").addResourceLocations("/app/bower_components/");

		registry.addResourceHandler("/robots.txt").addResourceLocations("classpath:/robots.txt");
		registry.addResourceHandler("/sitemap.xml").addResourceLocations("classpath:/sitemap.xml");

		String imageResourceLocation = AppUtils.getImageResourceLocation();
		if (imageResourceLocation.isEmpty()) {
			imageResourceLocation = "file:/data/images/";
		}
		registry.addResourceHandler("/images/**").addResourceLocations(imageResourceLocation);
	}

	@Bean
	public LoggedInUserModelInterceptor loggedInUserModelInterceptor() {
		return new LoggedInUserModelInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(loggedInUserModelInterceptor());
	}
}
