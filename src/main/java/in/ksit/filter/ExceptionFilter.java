/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.mashape.unirest.http.exceptions.UnirestException;

import in.ksit.service.SendEmail;
import in.ksit.util.AppUtils;

public class ExceptionFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		
		try {
			filterChain.doFilter(request, response);
		}catch (Throwable e) {
			String domain = "www.ksit.ac.in";
			if(AppUtils.isLocalDomain()) {
				domain = "local";
			}
			e.printStackTrace();
			try {
				if(AppUtils.isLocalDomain() == false) {
					SendEmail.sendEmail("ERROR", "rksrinivas033@gmail.com", "Error "+domain, e.getMessage() );
				}
			} catch (UnirestException e1) {
				e1.printStackTrace();
			}
			throw e;
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
