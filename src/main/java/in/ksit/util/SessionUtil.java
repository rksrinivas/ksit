/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.util;

import in.ksit.entity.PageType;
import in.ksit.entity.User;
import in.ksit.entity.UserPage;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SessionUtil {

    public static boolean isUserAnonymous() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean authenticated =
                auth != null
                        && !(auth instanceof AnonymousAuthenticationToken)
                        && auth.isAuthenticated();
        return !authenticated;
    }

    public static boolean isUserAuthenicated() {
        return !isUserAnonymous();
    }

    public static User getLoggedInUser() {
        User user = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (isUserAuthenicated()) {
            user = (User) auth.getPrincipal();
        }

        return user;
    }

    public static boolean isLoggedInUserAuthenticatedForPage(PageType page) {
        if (isUserAuthenicated()) {
            User user = getLoggedInUser();
            for (UserPage userPage : user.getPages()) {
                if (userPage.getPage().equals(page)) {
                    return true;
                }
            }
        }
        return false;
    }
}
