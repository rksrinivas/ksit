/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Component
public class AppUtils {

    private static AppUtils instance;

    @Autowired private Environment environment;

    @PostConstruct
    public void registerInstance() {
        instance = this;
    }

    public static final String getImageResourceLocation() {
        return "file:" + getImageLocation();
    }

    public static final String getImageLocation() {
        if (isLocalDomain()) {
            return "/Users/srinivas/images/";
        } else {
            return "/data/images/";
        }
    }

    public static boolean isLocalDomain() {
        if (instance == null) {
            return false;
        }
        String profile = instance.environment.getProperty("spring.profiles.active");
        if (StringUtils.isEmpty(profile) == false && profile.contains("prod")) {
            return false;
        }
        return true;
    }

    public static void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation)
            throws IOException {

        try {
            OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
            System.out.println("File uploaded successfully at " + uploadedFileLocation);
        } catch (IOException e) {
            System.out.print(e);
            throw (e);
        }
    }

    public static String getFileUrl(MultipartFile img, String suffix) {
        if (img != null) {
            String fileName = img.getOriginalFilename();
            String type = "";
            if (fileName.contains(".")) {
                int index = fileName.lastIndexOf(".");
                type = fileName.substring(index);
            }
            long timeNow = Calendar.getInstance().getTimeInMillis();
            String fileImageURL = getImageLocation() + timeNow + suffix + type;
            String imageURL = "/images/" + timeNow + suffix + type;
            try {
                AppUtils.writeToFile(img.getInputStream(), fileImageURL);
            } catch (IOException e) {
                return null;
            }
            return imageURL;
        }
        return null;
    }
}
