/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.controllers;

import in.ksit.entity.FeedbackAlumni;
import in.ksit.entity.FeedbackAlumniForm;
import in.ksit.entity.FeedbackEmloyerForm;
import in.ksit.entity.FeedbackEmployer;
import in.ksit.entity.FeedbackParent;
import in.ksit.entity.FeedbackParentForm;
import in.ksit.entity.FeedbackStudent;
import in.ksit.entity.FeedbackStudentForm;
import in.ksit.service.FeedbackAlumniService;
import in.ksit.service.FeedbackEmployerService;
import in.ksit.service.FeedbackParentService;
import in.ksit.service.FeedbackStudentService;
import javax.ws.rs.BeanParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/feedback")
public class FeedbackController {

    @Autowired private FeedbackParentService feedbackParentService;

    @Autowired private FeedbackStudentService feedbackStudentService;

    @Autowired private FeedbackAlumniService feedbackAlumniService;

    @Autowired private FeedbackEmployerService feedbackEmployerService;

    @RequestMapping(value = "/parent", method = RequestMethod.POST)
    public String feedbackParent(ModelMap model, @BeanParam FeedbackParentForm feedbackParentForm) {
        FeedbackParent feedbackParent = new FeedbackParent();
        BeanUtils.copyProperties(feedbackParentForm, feedbackParent);
        feedbackParentService.saveOrUpdate(feedbackParent);
        return "redirect:/";
    }

    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public String feedbackStudent(
            ModelMap model, @BeanParam FeedbackStudentForm feedbackStudentForm) {
        FeedbackStudent feedbackStudent = new FeedbackStudent();
        BeanUtils.copyProperties(feedbackStudentForm, feedbackStudent);
        feedbackStudentService.saveOrUpdate(feedbackStudent);
        return "redirect:/";
    }

    @RequestMapping(value = "/alumni", method = RequestMethod.POST)
    public String feedbackAlumni(ModelMap model, @BeanParam FeedbackAlumniForm feedbackAlumniForm) {
        FeedbackAlumni feedbackAlumni = new FeedbackAlumni();
        BeanUtils.copyProperties(feedbackAlumniForm, feedbackAlumni);
        feedbackAlumniService.saveOrUpdate(feedbackAlumni);
        return "redirect:/";
    }

    @RequestMapping(value = "/employer", method = RequestMethod.POST)
    public String feedbackEmployer(
            ModelMap model, @BeanParam FeedbackEmloyerForm feedbackEmloyerForm) {
        FeedbackEmployer feedbackEmployer = new FeedbackEmployer();
        BeanUtils.copyProperties(feedbackEmloyerForm, feedbackEmployer);
        feedbackEmployerService.saveOrUpdate(feedbackEmployer);
        return "redirect:/";
    }
}
