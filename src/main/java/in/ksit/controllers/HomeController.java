/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.BeanParam;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import in.ksit.entity.Alumni;
import in.ksit.entity.AlumniForm;
import in.ksit.entity.ContactUs;
import in.ksit.entity.Course.COURSE;
import in.ksit.entity.Criteria;
import in.ksit.entity.Data;
import in.ksit.entity.Event;
import in.ksit.entity.Faculty;
import in.ksit.entity.Feedback;
import in.ksit.entity.FeedbackResponse;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.Grievance;
import in.ksit.entity.HostelEnrollment;
import in.ksit.entity.InstitutionalGovernance;
import in.ksit.entity.LabDataInternal;
import in.ksit.entity.NAAC;
import in.ksit.entity.NBA;
import in.ksit.entity.NBAPrevisit;
import in.ksit.entity.OnlineAdmission;
import in.ksit.entity.OnlineAdmissionsForm;
import in.ksit.entity.PageType;
import in.ksit.entity.Placement;
import in.ksit.entity.Programme;
import in.ksit.entity.Programme.PROGRAMME_TYPE;
import in.ksit.service.AlumniService;
import in.ksit.service.ContactUsService;
import in.ksit.service.DataService;
import in.ksit.service.EventService;
import in.ksit.service.FacultyService;
import in.ksit.service.FeedbackAlumniService;
import in.ksit.service.FeedbackEmployerService;
import in.ksit.service.FeedbackParentService;
import in.ksit.service.FeedbackService;
import in.ksit.service.FeedbackStudentService;
import in.ksit.service.GrievanceService;
import in.ksit.service.HostelEnrollmentService;
import in.ksit.service.InstitutionalGovernanceService;
import in.ksit.service.LabDataService;
import in.ksit.service.NAACService;
import in.ksit.service.NBAPrevisitService;
import in.ksit.service.NBAService;
import in.ksit.service.OnlineAdmissionService;
import in.ksit.service.PlacementService;
import in.ksit.service.ProgrammeService;

@Controller
public class HomeController {

	@Autowired
	private AlumniService alumniService;

	@Autowired
	private ContactUsService contactUsService;
	
	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private HostelEnrollmentService hostelEnrollmentService;

	@Autowired
	private DataService dataService;

	@Autowired
	private FacultyService facultyService;

	@Autowired
	private EventService eventService;

	@Autowired
	private PlacementService placementService;

	@Autowired
	private GrievanceService grievanceService;

	@Autowired
	private LabDataService labDataService;

	@Autowired
	private ProgrammeService programmeService;

	@Autowired
	private NBAService nbaService;

	@Autowired
	private NBAPrevisitService nbaPrevisitService;

	@Autowired
	private NAACService naacService;

	@Autowired
	private FeedbackParentService feedbackParentService;

	@Autowired
	private FeedbackStudentService feedbackStudentService;

	@Autowired
	private FeedbackAlumniService feedbackAlumniService;

	@Autowired
	private FeedbackEmployerService feedbackEmployerService;

	@Autowired
	private OnlineAdmissionService onlineAdmissionService;

	@Autowired
	private InstitutionalGovernanceService institutionalGovernanceService;

	@RequestMapping(value = { "", "/", "/index" }, method = RequestMethod.GET)
	public String index(ModelMap model) {

		List<Event> homePageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_slider_images);
		model.addAttribute("homePageSliderImageList", homePageSliderImageList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> messageBoxList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_messagebox);

		if (messageBoxList.size() > 0) {
			model.addAttribute("messageBox", messageBoxList.get(0));
		}

		List<Event> homePageTimeTableList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_timetable);

		if (homePageTimeTableList.size() > 0) {
			model.addAttribute("homePageTimeTable", homePageTimeTableList.get(0));
		}

		List<Event> homePageCircularList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_circular);

		if (homePageCircularList.size() > 0) {
			model.addAttribute("homePageCircular", homePageCircularList.get(0));
		}

		List<Event> academicCalanderList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.academic_page_calander);

		model.addAttribute("academicCalanderList", academicCalanderList);

		List<Data> youtubeLinkList = dataService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.youtube_link);
		String youtubeLink = "";
		if (youtubeLinkList.size() > 0) {
			youtubeLink = youtubeLinkList.get(0).getContent();
		}
		model.addAttribute("youtubeLink", youtubeLink);

		List<Event> achieversList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_achievers);
		model.addAttribute("achieversList", achieversList);

		List<Event> homePageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_latest_events);
		model.addAttribute("homePageLatestEventsList", homePageLatestEventsList);

		List<Event> homePageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_upcoming_events);
		model.addAttribute("homePageUpcomingEventsList", homePageUpcomingEventsList);

		List<Event> newsletterPageNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.newsletter_page_newsletter);
		model.addAttribute("newsletterPageNewsLetterList", newsletterPageNewsLetterList);

		List<Event> souvenirPageSouvenirList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.souvenir_page_souvenir);
		model.addAttribute("souvenirPageSouvenirList", souvenirPageSouvenirList);

		List<Event> hostelFeesList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.hostel_fees);
		model.addAttribute("hostelFeesList", hostelFeesList);

		return "index";
	}

	@RequestMapping(value = { "", "/", "/index2" }, method = RequestMethod.GET)
	public String index2(ModelMap model) {

		List<Event> homePageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_slider_images);
		model.addAttribute("homePageSliderImageList", homePageSliderImageList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> messageBoxList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_messagebox);

		if (messageBoxList.size() > 0) {
			model.addAttribute("messageBox", messageBoxList.get(0));
		}

		List<Event> homePageTimeTableList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_timetable);

		if (homePageTimeTableList.size() > 0) {
			model.addAttribute("homePageTimeTable", homePageTimeTableList.get(0));
		}

		List<Event> homePageCircularList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_circular);

		if (homePageCircularList.size() > 0) {
			model.addAttribute("homePageCircular", homePageCircularList.get(0));
		}

		List<Event> academicCalanderList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.academic_page_calander);

		model.addAttribute("academicCalanderList", academicCalanderList);

		List<Data> youtubeLinkList = dataService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.youtube_link);
		String youtubeLink = "";
		if (youtubeLinkList.size() > 0) {
			youtubeLink = youtubeLinkList.get(0).getContent();
		}
		model.addAttribute("youtubeLink", youtubeLink);

		List<Event> achieversList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_achievers);
		model.addAttribute("achieversList", achieversList);

		List<Event> homePageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_latest_events);
		model.addAttribute("homePageLatestEventsList", homePageLatestEventsList);

		List<Event> homePageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_upcoming_events);
		model.addAttribute("homePageUpcomingEventsList", homePageUpcomingEventsList);

		List<Event> newsletterPageNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.newsletter_page_newsletter);
		model.addAttribute("newsletterPageNewsLetterList", newsletterPageNewsLetterList);

		List<Event> souvenirPageSouvenirList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.souvenir_page_souvenir);
		model.addAttribute("souvenirPageSouvenirList", souvenirPageSouvenirList);

		List<Event> hostelFeesList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.hostel_fees);
		model.addAttribute("hostelFeesList", hostelFeesList);

		return "index2";
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(ModelMap model) {

		// List<Faculty> academicAdvisoryFacultyList =
		// facultyService.getActiveByPageAndFieldName(PageType.about_page,
		// FieldNameType.about_page_academic_advisory_faculty);
		// model.addAttribute("academicAdvisoryFacultyList",
		// academicAdvisoryFacultyList);

		//
		// List<Faculty> academicGoverningCouncilFacultyList =
		// facultyService.getActiveByPageAndFieldName(PageType.about_page,
		// FieldNameType.about_page_governing_council_faculty);
		// model.addAttribute("academicGoverningCouncilFacultyList",
		// academicGoverningCouncilFacultyList);
		//

		return "about";
	}

	@RequestMapping(value = "/about_ksrif", method = RequestMethod.GET)
	public String about_ksrif(ModelMap model) {
		

		return "about_ksrif";
	}

	@RequestMapping(value = "/mca_dept", method = RequestMethod.GET)
	public String mca_dept(ModelMap model) {
		

		return "mca_dept";
	}

	@RequestMapping(value = "/newsletter", method = RequestMethod.GET)
	public String newsletter(ModelMap model) {

		List<Event> newsletterPageNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.newsletter_page_newsletter);
		model.addAttribute("newsletterPageNewsLetterList", newsletterPageNewsLetterList);

		return "newsletter";
	}

	@RequestMapping(value = "/souvenir", method = RequestMethod.GET)
	public String souvenir(ModelMap model) {

		List<Event> souvenirPageSouvenirList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.souvenir_page_souvenir);
		model.addAttribute("souvenirPageSouvenirList", souvenirPageSouvenirList);

		return "souvenir";
	}

	@RequestMapping(value = "/UG", method = RequestMethod.GET)
	public String UG(ModelMap model) {
		List<Programme> programmeList = programmeService.getProgrammesByType(PROGRAMME_TYPE.UG);
		model.addAttribute("programmeList", programmeList);
		return "UG";
	}

	@RequestMapping(value = "/PG", method = RequestMethod.GET)
	public String PG(ModelMap model) {
		List<Programme> programmeList = programmeService.getProgrammesByType(PROGRAMME_TYPE.PG);
		model.addAttribute("programmeList", programmeList);
		return "PG";
	}

	@RequestMapping(value = "/campus", method = RequestMethod.GET)
	public String campus(ModelMap model) {

		return "campus";
	}

	@RequestMapping(value = "/dept_demo", method = RequestMethod.GET)
	public String dept_demo(ModelMap model) {

		return "dept_demo";
	}

	@RequestMapping(value = "/research_development", method = RequestMethod.GET)
	public String research_development(ModelMap model) {

		return "research_development";
	}

	@RequestMapping(value = "/privacy", method = RequestMethod.GET)
	public String privacy(ModelMap model) {
		return "privacy";
	}
	
	@RequestMapping(value = "/placement_drive", method = RequestMethod.GET)
	public String placement_drive(ModelMap model) {
		List<Event> placementDriveList = eventService.getActiveByPageAndFieldName(PageType.placement_drive_page,
				FieldNameType.placement_drives);
		model.addAttribute("placementDriveList", placementDriveList);
		return "placement_drive";
	}
	
	@RequestMapping(value = "/placed_students", method = RequestMethod.GET)
	public String placed_students(ModelMap model) {
		List<Event> placedStudentsList = eventService.getActiveByPageAndFieldName(PageType.placed_students_page,
				FieldNameType.placed_students);
		model.addAttribute("placedStudentsList", placedStudentsList);
		return "placed_students";
	}
	
	@RequestMapping(value = "/placement_statistics", method = RequestMethod.GET)
	public String placement_statistics(ModelMap model) {
		List<Event> placementStatisticsList = eventService.getActiveByPageAndFieldName(PageType.placement_statistics_page,
				FieldNameType.placement_statistics);
		model.addAttribute("placementStatisticsList", placementStatisticsList);
		return "placement_statistics";
	}
	
	@RequestMapping(value = "/placement_upcoming_events_latest_news", method = RequestMethod.GET)
	public String upcoming_events_latest_news(ModelMap model) {		
		List<Event> placementLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_upcoming_events_latest_news,
				FieldNameType.placement_page_latest_events);
		model.addAttribute("placementLatestEventsList", placementLatestEventsList);
		
		List<Event> placementUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_upcoming_events_latest_news,
				FieldNameType.placement_page_upcoming_events);
		model.addAttribute("placementUpcomingEventsList", placementUpcomingEventsList);
		
		
		return "placement_upcoming_events_latest_news";
	}

	@RequestMapping(value = "/about_placement", method = RequestMethod.GET)
	public String about_placement(ModelMap model) {

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> placementPageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_latest_events);
		model.addAttribute("placementPageLatestEventsList", placementPageLatestEventsList);

		List<Event> placementPageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_upcoming_events);
		model.addAttribute("placementPageUpcomingEventsList", placementPageUpcomingEventsList);
		
		List<Event> placementEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_events);
		model.addAttribute("placementEventsList", placementEventsList);

		List<Event> placementGalleryList = eventService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_gallery);
		model.addAttribute("placementGalleryList", placementGalleryList);

		List<Placement> placementCompanyList = placementService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_company_page);
		model.addAttribute("placementCompanyList", placementCompanyList);

		List<Placement> placementTrainingList = placementService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_training_page);
		model.addAttribute("placementTrainingList", placementTrainingList);

		List<Placement> placementHigherStudiesList = placementService
				.getActiveByPageAndFieldName(PageType.placement_page, FieldNameType.placement_higher_studies_page);
		model.addAttribute("placementHigherStudiesList", placementHigherStudiesList);

		List<Placement> placementInternshipList = placementService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_internship_page);
		model.addAttribute("placementInternshipList", placementInternshipList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		Set<Integer> yearSet = new HashSet<Integer>();
		for (Placement placementCompany : placementCompanyList) {
			Integer year = placementCompany.getYear();
			yearSet.add(year);
		}
		List<Integer> tempList = new ArrayList<Integer>(yearSet);
		Collections.sort(tempList, Collections.reverseOrder());
		model.addAttribute("yearSet", tempList);
		
		List<Event> placementDriveList = eventService.getByPageAndFieldName(PageType.placement_drive_page, FieldNameType.placement_drives);
		model.addAttribute("placementDriveList", placementDriveList);
		model.addAttribute("placementDriveFieldName", FieldNameType.placement_drives);
	

		return "about_placement";
	}

	@RequestMapping(value = "/placement_gallery", method = RequestMethod.GET)
	public String placement_gallery(ModelMap model) {

		List<Event> placementGalleryList = eventService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_gallery);
		model.addAttribute("placementGalleryList", placementGalleryList);

		return "placement_gallery";
	}

	@RequestMapping(value = "/training_programs", method = RequestMethod.GET)
	public String training_programs(ModelMap model) {

		List<Placement> placementTrainingList = placementService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_training_page);
		model.addAttribute("placementTrainingList", placementTrainingList);

		return "training_programs";
	}

	@RequestMapping(value = "/companies", method = RequestMethod.GET)
	public String companies(ModelMap model) {

		List<Placement> placementCompanyList = placementService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_company_page);
		model.addAttribute("placementCompanyList", placementCompanyList);

		return "companies";
	}

	@RequestMapping(value = "/placement_events", method = RequestMethod.GET)
	public String placement_events(ModelMap model) {

		List<Event> placementEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_events);
		model.addAttribute("placementEventsList", placementEventsList);

		return "placement_events";
	}

	@RequestMapping(value = "/yearwise_placement", method = RequestMethod.GET)
	public String yearwise_placement(ModelMap model) {

		List<Placement> placementCompanyList = placementService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_company_page);

		/*
		 * Map<String, List<Placement>> placementCompanyMap = new HashMap<String,
		 * List<Placement>>(); for(Placement placementCompany: placementCompanyList) {
		 * Integer year = placementCompany.getYear(); if(
		 * placementCompanyMap.containsKey(year.toString()) ) {
		 * placementCompanyMap.get(year.toString()).add(placementCompany); }else {
		 * List<Placement> tempList = new ArrayList<Placement>();
		 * tempList.add(placementCompany); placementCompanyMap.put(year.toString(),
		 * tempList); } }
		 */
		Set<Integer> yearSet = new HashSet<Integer>();
		for (Placement placementCompany : placementCompanyList) {
			Integer year = placementCompany.getYear();
			yearSet.add(year);
		}
		List<Integer> tempList = new ArrayList<Integer>(yearSet);
		Collections.sort(tempList, Collections.reverseOrder());
		model.addAttribute("yearSet", tempList);

		model.addAttribute("placementCompanyList", placementCompanyList);
		return "yearwise_placement";
	}

	@RequestMapping(value = "/higher_studiesGRE", method = RequestMethod.GET)
	public String higher_studiesGRE(ModelMap model) {

		List<Placement> placementHigherStudiesList = placementService
				.getActiveByPageAndFieldName(PageType.placement_page, FieldNameType.placement_higher_studies_page);

		Set<Integer> yearSet = new HashSet<Integer>();
		for (Placement placementHigherStudies : placementHigherStudiesList) {
			Integer year = placementHigherStudies.getYear();
			yearSet.add(year);
		}
		List<Integer> tempList = new ArrayList<Integer>(yearSet);
		Collections.sort(tempList, Collections.reverseOrder());
		model.addAttribute("yearSet", tempList);

		model.addAttribute("placementHigherStudiesList", placementHigherStudiesList);

		return "higher_studiesGRE";
	}

	@RequestMapping(value = "/internship", method = RequestMethod.GET)
	public String internship(ModelMap model) {

		List<Placement> placementInternshipList = placementService.getActiveByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_internship_page);
		model.addAttribute("placementInternshipList", placementInternshipList);

		return "internship";
	}

	@RequestMapping(value = "/administration", method = RequestMethod.GET)
	public String administration(ModelMap model) {

		// List<Faculty> academicAdvisoryFacultyList =
		// facultyService.getActiveByPageAndFieldName(PageType.administration_page,
		// FieldNameType.about_page_academic_advisory_faculty);
		// model.addAttribute("academicAdvisoryFacultyList",
		// academicAdvisoryFacultyList);
		//

		return "administration";
	}

	@RequestMapping(value = "/office_administration", method = RequestMethod.GET)
	public String office_administration(ModelMap model) {

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.office_administration_page,
				FieldNameType.office_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(
				PageType.office_administration_page, FieldNameType.office_administration_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		return "office_administration";
	}

	@RequestMapping(value = "/right_to_information", method = RequestMethod.GET)
	public String right_to_information(ModelMap model) {

		return "right_to_information";
	}

	@RequestMapping(value = "/principal", method = RequestMethod.GET)
	public String principal(ModelMap model) {

		return "principal";
	}

	@RequestMapping(value = "/cse_dept", method = RequestMethod.GET)
	public String cse_dept(ModelMap model) {

		List<Event> csePageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_slider_images);
		model.addAttribute("csePageSliderImageList", csePageSliderImageList);

		List<Event> csePageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_latest_events);
		model.addAttribute("csePageLatestEventsList", csePageLatestEventsList);

		List<Event> csePageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_upcoming_events);
		model.addAttribute("csePageUpcomingEventsList", csePageUpcomingEventsList);

		/*
		 * List<Data> upComingEventsDataList =
		 * dataService.getActiveByPageAndFieldName(PageType.cse_page,
		 * FieldNameType.upcoming_events); List<String> upComingEvents = new
		 * ArrayList<String>(); for (int i = 0; i < upComingEventsDataList.size(); i++)
		 * { upComingEvents.add(upComingEventsDataList.get(i).getContent()); }
		 * model.addAttribute("upComingEvents", upComingEvents);
		 *
		 * List<Data> latestEventsDataList =
		 * dataService.getActiveByPageAndFieldName(PageType.cse_page,
		 * FieldNameType.latest_events); List<String> latestEvents = new
		 * ArrayList<String>(); for (int i = 0; i < latestEventsDataList.size(); i++) {
		 * latestEvents.add(latestEventsDataList.get(i).getContent()); }
		 * model.addAttribute("latestEvents", latestEvents);
		 */

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> cseEventsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_events);
		model.addAttribute("cseEventsList", cseEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> cseGalleryList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_gallery);
		model.addAttribute("cseGalleryList", cseGalleryList);

		List<Event> cseClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_class_time_table);
		model.addAttribute("cseClassTimeTableList", cseClassTimeTableList);

		List<Event> cseTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_test_time_table);

		model.addAttribute("cseTestTimeTableList", cseTestTimeTableList);

		List<Event> cseCalanderList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_calander);

		model.addAttribute("cseCalanderList", cseCalanderList);

		List<Event> cseNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_newsletter);

		model.addAttribute("cseNewsLetterList", cseNewsLetterList);

		List<Event> cseUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_ug_syllabus);

		model.addAttribute("cseUgSyllabusList", cseUgSyllabusList);

		List<Event> csePgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_pg_syllabus);

		model.addAttribute("csePgSyllabusList", csePgSyllabusList);

		/*
		 * if (cseTimeTableList.size() > 0) { model.addAttribute("cseTimeTable",
		 * cseTimeTableList.get(0)); }
		 */

		List<Event> cseDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_department_achiever_list);
		model.addAttribute("cseDepartmentAchieversList", cseDepartmentAchieversList);

		List<Event> cseFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_fcd_students);
		model.addAttribute("cseFcdStudentsList", cseFcdStudentsList);

		List<Event> cseLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_library_books_collection);
		model.addAttribute("cseLibraryBooksList", cseLibraryBooksList);

		List<Event> csePlacementList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_placement);
		model.addAttribute("csePlacementList", csePlacementList);

		List<LabDataInternal> labDataList = labDataService.getLabDataByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_lab, true);
		model.addAttribute("labDataList", labDataList);

		List<Faculty> cseLabList = facultyService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_labs);
		model.addAttribute("cseLabList", cseLabList);

		List<Event> cseEresourcesList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_eresources);
		model.addAttribute("cseEresourcesList", cseEresourcesList);

		List<Event> cseResearchList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_research);
		model.addAttribute("cseResearchList", cseResearchList);

		List<Event> cseSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_sponsored_projects);
		model.addAttribute("cseSponsoredProjectsList", cseSponsoredProjectsList);

		List<Event> csePhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_phd_pursuing);
		model.addAttribute("csePhdPursuingList", csePhdPursuingList);

		List<Event> csePhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_phd_awardees);
		model.addAttribute("csePhdAwardeesList", csePhdAwardeesList);

		List<Event> cseInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_instructional_materials);
		model.addAttribute("cseInstructionalMaterialsList", cseInstructionalMaterialsList);

		List<Event> csePedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_pedagogical_activities);
		model.addAttribute("csePedagogicalActivitiesList", csePedagogicalActivitiesList);

		List<Event> csePedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_pedagogy_review_form);
		model.addAttribute("csePedagogyReviewFormList", csePedagogyReviewFormList);

		List<Event> csecontentbeyondsyllabusList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_content_beyond_syllabus);
		model.addAttribute("csecontentbeyondsyllabusList", csecontentbeyondsyllabusList);

		List<Event> cseLabManualList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_lab_manual_form);
		model.addAttribute("cseLabManualList", cseLabManualList);

		List<Event> cseMouSignedList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_Mou_Signed);
		model.addAttribute("cseMouSignedList", cseMouSignedList);

		List<Event> cseAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_alumni_association);
		model.addAttribute("cseAlumniAssociationList", cseAlumniAssociationList);

		List<Event> cseProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_professional_bodies);
		model.addAttribute("cseProfessionalBodiesList", cseProfessionalBodiesList);

		List<Event> cseProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_professional_links);
		model.addAttribute("cseProfessionalLinksList", cseProfessionalLinksList);

		List<Event> cseHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_higher_education);
		model.addAttribute("cseHigherEducationList", cseHigherEducationList);

		List<Event> cseClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_club_activities);
		model.addAttribute("cseClubActivitiesList", cseClubActivitiesList);

		List<Event> cseClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_club_external_links);
		model.addAttribute("cseClubExternalLinksList", cseClubExternalLinksList);

		List<Event> cseInternshipList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_internship);
		model.addAttribute("cseInternshipList", cseInternshipList);

		List<Event> cseProjectsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_projects);
		model.addAttribute("cseProjectsList", cseProjectsList);

		List<Event> cseMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_mini_projects);
		model.addAttribute("cseMiniProjectsList", cseMiniProjectsList);

		List<Event> cseSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_social_activities);
		model.addAttribute("cseSocialActivitiesList", cseSocialActivitiesList);

		List<Faculty> cseIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_industrial_visit);
		model.addAttribute("cseIndustrialVisitList", cseIndustrialVisitList);

		List<Faculty> cseProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_project_exhibition);
		model.addAttribute("cseProjectExhibitionList", cseProjectExhibitionList);

		List<Event> cseOtherDetailsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_other_details);
		model.addAttribute("cseOtherDetailsList", cseOtherDetailsList);

		List<Event> cseFdpList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_fdp);
		model.addAttribute("cseFdpList", cseFdpList);

		Map<Criteria, List<NBA>> criteriaMap = nbaService.getByPage(PageType.cse_page);
		model.addAttribute("criteriaMap", criteriaMap);

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "cse_dept";
	}

	@RequestMapping(value = "/cse_gallery", method = RequestMethod.GET)
	public String cse_gallery(ModelMap model) {

		List<Event> cseGalleryList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_gallery);
		model.addAttribute("cseGalleryList", cseGalleryList);

		return "cse_gallery";
	}

	@RequestMapping(value = "/ece_dept", method = RequestMethod.GET)
	public String ece_dept(ModelMap model) {

		List<Event> ecePageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_slider_images);
		model.addAttribute("ecePageSliderImageList", ecePageSliderImageList);

		List<Event> ecePageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_latest_events);
		model.addAttribute("ecePageLatestEventsList", ecePageLatestEventsList);

		List<Event> ecePageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_upcoming_events);
		model.addAttribute("ecePageUpcomingEventsList", ecePageUpcomingEventsList);

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> eceEventsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_events);
		model.addAttribute("eceEventsList", eceEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> eceGalleryList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_gallery);
		model.addAttribute("eceGalleryList", eceGalleryList);

		List<Event> eceClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_class_time_table);

		model.addAttribute("eceClassTimeTableList", eceClassTimeTableList);

		List<Event> eceTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_test_time_table);

		model.addAttribute("eceTestTimeTableList", eceTestTimeTableList);

		List<Event> eceCalanderList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_calander);

		model.addAttribute("eceCalanderList", eceCalanderList);

		List<Event> eceNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_newsletter);

		model.addAttribute("eceNewsLetterList", eceNewsLetterList);

		List<Event> eceUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_ug_syllabus);

		model.addAttribute("eceUgSyllabusList", eceUgSyllabusList);

		List<Event> ecePgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_pg_syllabus);

		model.addAttribute("ecePgSyllabusList", ecePgSyllabusList);

		List<Event> eceDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_department_achiever_list);
		model.addAttribute("eceDepartmentAchieversList", eceDepartmentAchieversList);

		List<Event> eceFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_fcd_students);
		model.addAttribute("eceFcdStudentsList", eceFcdStudentsList);

		List<Event> eceLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_library_books_collection);
		model.addAttribute("eceLibraryBooksList", eceLibraryBooksList);

		List<Event> ecePlacementList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_placement);
		model.addAttribute("ecePlacementList", ecePlacementList);

		List<Faculty> eceLabList = facultyService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_labs);
		model.addAttribute("eceLabList", eceLabList);

		List<Event> eceEresourcesList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_eresources);
		model.addAttribute("eceEresourcesList", eceEresourcesList);

		List<Event> eceResearchList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_research);
		model.addAttribute("eceResearchList", eceResearchList);

		List<Event> eceSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_sponsored_projects);
		model.addAttribute("eceSponsoredProjectsList", eceSponsoredProjectsList);

		List<Event> ecePhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_phd_pursuing);
		model.addAttribute("ecePhdPursuingList", ecePhdPursuingList);

		List<Event> ecePhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_phd_awardees);
		model.addAttribute("ecePhdAwardeesList", ecePhdAwardeesList);

		List<Event> eceInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_instructional_materials);
		model.addAttribute("eceInstructionalMaterialsList", eceInstructionalMaterialsList);

		List<Event> ecePedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_pedagogical_activities);
		model.addAttribute("ecePedagogicalActivitiesList", ecePedagogicalActivitiesList);

		List<Event> ecePedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_pedagogy_review_form);
		model.addAttribute("ecePedagogyReviewFormList", ecePedagogyReviewFormList);

		List<Event> eceLabManualList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_lab_manual_form);
		model.addAttribute("eceLabManualList", eceLabManualList);

		List<Event> eceMouSignedList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_Mou_Signed);
		model.addAttribute("eceMouSignedList", eceMouSignedList);

		List<Event> eceAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_alumni_association);
		model.addAttribute("eceAlumniAssociationList", eceAlumniAssociationList);

		List<Event> eceProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_professional_bodies);
		model.addAttribute("eceProfessionalBodiesList", eceProfessionalBodiesList);

		List<Event> eceProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_professional_links);
		model.addAttribute("eceProfessionalLinksList", eceProfessionalLinksList);

		List<Event> eceHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_higher_education);
		model.addAttribute("eceHigherEducationList", eceHigherEducationList);

		List<Event> ececontentbeyondsyllabusList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_content_beyond_syllabus);
		model.addAttribute("ececontentbeyondsyllabusList", ececontentbeyondsyllabusList);

		List<Event> eceClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_club_activities);
		model.addAttribute("eceClubActivitiesList", eceClubActivitiesList);

		List<Event> eceClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_club_external_links);
		model.addAttribute("eceClubExternalLinksList", eceClubExternalLinksList);

		List<Event> eceInternshipList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_internship);
		model.addAttribute("eceInternshipList", eceInternshipList);

		List<Event> eceProjectsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_projects);
		model.addAttribute("eceProjectsList", eceProjectsList);

		List<Event> eceMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_mini_projects);
		model.addAttribute("eceMiniProjectsList", eceMiniProjectsList);

		List<Event> eceSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_social_activities);
		model.addAttribute("eceSocialActivitiesList", eceSocialActivitiesList);

		List<Faculty> eceIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_industrial_visit);
		model.addAttribute("eceIndustrialVisitList", eceIndustrialVisitList);

		List<Faculty> eceProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_project_exhibition);
		model.addAttribute("eceProjectExhibitionList", eceProjectExhibitionList);

		List<Event> eceOtherDetailsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_other_details);
		model.addAttribute("eceOtherDetailsList", eceOtherDetailsList);

		List<Event> eceFdpList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_fdp);
		model.addAttribute("eceFdpList", eceFdpList);

		/*
		 * List<Event> eceTimeTableList =
		 * eventService.getActiveByPageAndFieldName(PageType.ece_page,
		 * FieldNameType.ece_page_timetable);
		 *
		 * if (eceTimeTableList.size() > 0) { model.addAttribute("eceTimeTable",
		 * eceTimeTableList.get(0)); }
		 */
		Map<Criteria, List<NBA>> criteriaMap = nbaService.getByPage(PageType.ece_page);
		model.addAttribute("criteriaMap", criteriaMap);

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "ece_dept";
	}

	@RequestMapping(value = "/ece_gallery", method = RequestMethod.GET)
	public String ece_gallery(ModelMap model) {

		List<Event> eceGalleryList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_gallery);
		model.addAttribute("eceGalleryList", eceGalleryList);

		return "ece_gallery";
	}

	@RequestMapping(value = "/mech_dept", method = RequestMethod.GET)
	public String mech_dept(ModelMap model) {

		List<Event> mechPageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_slider_images);
		model.addAttribute("mechPageSliderImageList", mechPageSliderImageList);

		List<Event> mechPageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_latest_events);
		model.addAttribute("mechPageLatestEventsList", mechPageLatestEventsList);

		List<Event> mechPageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_upcoming_events);
		model.addAttribute("mechPageUpcomingEventsList", mechPageUpcomingEventsList);

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> mechEventsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_events);
		model.addAttribute("mechEventsList", mechEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> mechGalleryList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_gallery);
		model.addAttribute("mechGalleryList", mechGalleryList);

		List<Event> mechClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_class_time_table);

		model.addAttribute("mechClassTimeTableList", mechClassTimeTableList);

		List<Event> mechTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_test_time_table);

		model.addAttribute("mechTestTimeTableList", mechTestTimeTableList);

		List<Event> mechCalanderList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_calander);
		model.addAttribute("mechCalanderList", mechCalanderList);

		List<Event> mechcontentbeyondsyllabusList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_content_beyond_syllabus);
		model.addAttribute("mechcontentbeyondsyllabusList", mechcontentbeyondsyllabusList);

		List<Event> mechNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_newsletter);

		model.addAttribute("mechNewsLetterList", mechNewsLetterList);

		List<Event> mechUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_ug_syllabus);

		model.addAttribute("mechUgSyllabusList", mechUgSyllabusList);

		List<Event> mechPgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_pg_syllabus);

		model.addAttribute("mechPgSyllabusList", mechPgSyllabusList);

		List<Event> mechDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_department_achiever_list);
		model.addAttribute("mechDepartmentAchieversList", mechDepartmentAchieversList);

		List<Event> mechFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_fcd_students);
		model.addAttribute("mechFcdStudentsList", mechFcdStudentsList);

		List<Event> mechLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_library_books_collection);
		model.addAttribute("mechLibraryBooksList", mechLibraryBooksList);

		List<Event> mechPlacementList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_placement);
		model.addAttribute("mechPlacementList", mechPlacementList);

		List<Faculty> mechLabList = facultyService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_labs);
		model.addAttribute("mechLabList", mechLabList);

		List<Event> mechEresourcesList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_eresources);
		model.addAttribute("mechEresourcesList", mechEresourcesList);

		List<Event> mechResearchList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_research);
		model.addAttribute("mechResearchList", mechResearchList);

		List<Event> mechSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_sponsored_projects);
		model.addAttribute("mechSponsoredProjectsList", mechSponsoredProjectsList);

		List<Event> mechPhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_phd_pursuing);
		model.addAttribute("mechPhdPursuingList", mechPhdPursuingList);

		List<Event> mechPhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_phd_awardees);
		model.addAttribute("mechPhdAwardeesList", mechPhdAwardeesList);

		List<Event> mechInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_instructional_materials);
		model.addAttribute("mechInstructionalMaterialsList", mechInstructionalMaterialsList);

		List<Event> mechPedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_pedagogical_activities);
		model.addAttribute("mechPedagogicalActivitiesList", mechPedagogicalActivitiesList);

		List<Event> mechPedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_pedagogy_review_form);
		model.addAttribute("mechPedagogyReviewFormList", mechPedagogyReviewFormList);

		List<Event> mechLabManualList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_lab_manual_form);
		model.addAttribute("mechLabManualList", mechLabManualList);

		List<Event> mechMouSignedList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_Mou_Signed);
		model.addAttribute("mechMouSignedList", mechMouSignedList);

		List<Event> mechAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_alumni_association);
		model.addAttribute("mechAlumniAssociationList", mechAlumniAssociationList);

		List<Event> mechProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_professional_bodies);
		model.addAttribute("mechProfessionalBodiesList", mechProfessionalBodiesList);

		List<Event> mechProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_professional_links);
		model.addAttribute("mechProfessionalLinksList", mechProfessionalLinksList);

		List<Event> mechHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_higher_education);
		model.addAttribute("mechHigherEducationList", mechHigherEducationList);

		List<Event> mechClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_club_activities);
		model.addAttribute("mechClubActivitiesList", mechClubActivitiesList);

		List<Event> mechClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_club_external_links);
		model.addAttribute("mechClubExternalLinksList", mechClubExternalLinksList);

		List<Event> mechInternshipList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_internship);
		model.addAttribute("mechInternshipList", mechInternshipList);

		List<Event> mechProjectsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_projects);
		model.addAttribute("mechProjectsList", mechProjectsList);

		List<Event> mechMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_mini_projects);
		model.addAttribute("mechMiniProjectsList", mechMiniProjectsList);

		List<Event> mechSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_social_activities);
		model.addAttribute("mechSocialActivitiesList", mechSocialActivitiesList);

		List<Faculty> mechIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_industrial_visit);
		model.addAttribute("mechIndustrialVisitList", mechIndustrialVisitList);

		List<Faculty> mechProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_project_exhibition);
		model.addAttribute("mechProjectExhibitionList", mechProjectExhibitionList);

		List<Event> mechOtherDetailsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_other_details);
		model.addAttribute("mechOtherDetailsList", mechOtherDetailsList);

		List<Event> mechFdpList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_fdp);
		model.addAttribute("mechFdpList", mechFdpList);

		/*
		 * List<Faculty> mechSponsoredProjectsList =
		 * facultyService.getActiveByPageAndFieldName(PageType.mech_page,
		 * FieldNameType.mech_page_sponsored_projects);
		 * model.addAttribute("mechSponsoredProjectsList", mechSponsoredProjectsList);
		 */

		/*
		 * List<Event> mechTimeTableList =
		 * eventService.getActiveByPageAndFieldName(PageType.mech_page,
		 * FieldNameType.mech_page_timetable);
		 *
		 * if (mechTimeTableList.size() > 0) { model.addAttribute("mechTimeTable",
		 * mechTimeTableList.get(0)); }
		 */

		Map<Criteria, List<NBA>> criteriaMap = nbaService.getByPage(PageType.mech_page);
		model.addAttribute("criteriaMap", criteriaMap);

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "mech_dept";
	}

	@RequestMapping(value = "/mech_gallery", method = RequestMethod.GET)
	public String mech_gallery(ModelMap model) {

		List<Event> mechGalleryList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_gallery);
		model.addAttribute("mechGalleryList", mechGalleryList);

		return "mech_gallery";
	}

	@RequestMapping(value = "/tele_dept", method = RequestMethod.GET)
	public String tele_dept(ModelMap model) {

		List<Event> telePageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_slider_images);
		model.addAttribute("telePageSliderImageList", telePageSliderImageList);

		List<Event> telePageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_latest_events);
		model.addAttribute("telePageLatestEventsList", telePageLatestEventsList);

		List<Event> telePageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_upcoming_events);
		model.addAttribute("telePageUpcomingEventsList", telePageUpcomingEventsList);

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> teleEventsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_events);
		model.addAttribute("teleEventsList", teleEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> teleGalleryList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_gallery);
		model.addAttribute("teleGalleryList", teleGalleryList);

		List<Event> teleClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_class_time_table);

		model.addAttribute("teleClassTimeTableList", teleClassTimeTableList);

		List<Event> teleTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_test_time_table);

		model.addAttribute("teleTestTimeTableList", teleTestTimeTableList);

		List<Event> teleCalanderList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_calander);

		model.addAttribute("teleCalanderList", teleCalanderList);

		List<Event> teleNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_newsletter);

		model.addAttribute("teleNewsLetterList", teleNewsLetterList);

		List<Event> teleUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_ug_syllabus);

		model.addAttribute("teleUgSyllabusList", teleUgSyllabusList);

		List<Event> teleDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_department_achiever_list);
		model.addAttribute("teleDepartmentAchieversList", teleDepartmentAchieversList);

		List<Event> teleFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_fcd_students);
		model.addAttribute("teleFcdStudentsList", teleFcdStudentsList);

		List<Event> teleLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_library_books_collection);
		model.addAttribute("teleLibraryBooksList", teleLibraryBooksList);

		List<Event> telePlacementList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_placement);
		model.addAttribute("telePlacementList", telePlacementList);

		List<Faculty> teleLabList = facultyService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_labs);
		model.addAttribute("teleLabList", teleLabList);

		List<Event> teleEresourcesList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_eresources);
		model.addAttribute("teleEresourcesList", teleEresourcesList);

		List<Event> teleResearchList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_research);
		model.addAttribute("teleResearchList", teleResearchList);

		List<Event> teleSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_sponsored_projects);
		model.addAttribute("teleSponsoredProjectsList", teleSponsoredProjectsList);

		List<Event> telePhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_phd_pursuing);
		model.addAttribute("telePhdPursuingList", telePhdPursuingList);

		List<Event> telePhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_phd_awardees);
		model.addAttribute("telePhdAwardeesList", telePhdAwardeesList);

		List<Event> teleInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_instructional_materials);
		model.addAttribute("teleInstructionalMaterialsList", teleInstructionalMaterialsList);

		List<Event> telePedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_pedagogical_activities);
		model.addAttribute("telePedagogicalActivitiesList", telePedagogicalActivitiesList);

		List<Event> telePedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_pedagogy_review_form);
		model.addAttribute("telePedagogyReviewFormList", telePedagogyReviewFormList);

		List<Event> teleLabManualList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_lab_manual_form);
		model.addAttribute("teleLabManualList", teleLabManualList);

		List<Event> teleMouSignedList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_Mou_Signed);
		model.addAttribute("teleMouSignedList", teleMouSignedList);

		List<Event> teleAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_alumni_association);
		model.addAttribute("teleAlumniAssociationList", teleAlumniAssociationList);

		List<Event> teleProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_professional_bodies);
		model.addAttribute("teleProfessionalBodiesList", teleProfessionalBodiesList);

		List<Event> teleProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_professional_links);
		model.addAttribute("teleProfessionalLinksList", teleProfessionalLinksList);

		List<Event> teleHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_higher_education);
		model.addAttribute("teleHigherEducationList", teleHigherEducationList);

		List<Event> teleClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_club_activities);
		model.addAttribute("teleClubActivitiesList", teleClubActivitiesList);

		List<Event> teleClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_club_external_links);
		model.addAttribute("teleClubExternalLinksList", teleClubExternalLinksList);

		List<Event> teleInternshipList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_internship);
		model.addAttribute("teleInternshipList", teleInternshipList);

		List<Event> teleProjectsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_projects);
		model.addAttribute("teleProjectsList", teleProjectsList);

		List<Event> teleMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_mini_projects);
		model.addAttribute("teleMiniProjectsList", teleMiniProjectsList);

		List<Event> teleSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_social_activities);
		model.addAttribute("teleSocialActivitiesList", teleSocialActivitiesList);

		List<Faculty> teleIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_industrial_visit);
		model.addAttribute("teleIndustrialVisitList", teleIndustrialVisitList);

		List<Faculty> teleProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_project_exhibition);
		model.addAttribute("teleProjectExhibitionList", teleProjectExhibitionList);

		List<Event> teleFdpList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_fdp);
		model.addAttribute("teleFdpList", teleFdpList);

		/*
		 * List<Event> teleTimeTableList =
		 * eventService.getActiveByPageAndFieldName(PageType.tele_page,
		 * FieldNameType.tele_page_timetable);
		 *
		 * if (teleTimeTableList.size() > 0) { model.addAttribute("teleTimeTable",
		 * teleTimeTableList.get(0)); }
		 */

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "tele_dept";
	}

	@RequestMapping(value = "/aiml_dept", method = RequestMethod.GET)
	public String aiml_dept(ModelMap model) {

		List<Event> aimlPageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_slider_images);
		model.addAttribute("aimlPageSliderImageList", aimlPageSliderImageList);

		List<Event> aimlPageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_latest_events);
		model.addAttribute("aimlPageLatestEventsList", aimlPageLatestEventsList);

		List<Event> aimlPageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_upcoming_events);
		model.addAttribute("aimlPageUpcomingEventsList", aimlPageUpcomingEventsList);

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> aimlEventsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_events);
		model.addAttribute("aimlEventsList", aimlEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> aimlGalleryList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_gallery);
		model.addAttribute("aimlGalleryList", aimlGalleryList);

		List<Event> aimlClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_class_time_table);

		model.addAttribute("aimlClassTimeTableList", aimlClassTimeTableList);

		List<Event> aimlTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_test_time_table);

		model.addAttribute("aimlTestTimeTableList", aimlTestTimeTableList);

		List<Event> aimlCalanderList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_calander);

		model.addAttribute("aimlCalanderList", aimlCalanderList);

		List<Event> aimlNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_newsletter);

		model.addAttribute("aimlNewsLetterList", aimlNewsLetterList);

		List<Event> aimlUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_ug_syllabus);

		model.addAttribute("aimlUgSyllabusList", aimlUgSyllabusList);

		List<Event> aimlDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_department_achiever_list);
		model.addAttribute("aimlDepartmentAchieversList", aimlDepartmentAchieversList);

		List<Event> aimlFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_fcd_students);
		model.addAttribute("aimlFcdStudentsList", aimlFcdStudentsList);

		List<Event> aimlLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_library_books_collection);
		model.addAttribute("aimlLibraryBooksList", aimlLibraryBooksList);

		List<Event> aimlPlacementList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_placement);
		model.addAttribute("aimlPlacementList", aimlPlacementList);

		List<Faculty> aimlLabList = facultyService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_labs);
		model.addAttribute("aimlLabList", aimlLabList);

		List<Event> aimlEresourcesList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_eresources);
		model.addAttribute("aimlEresourcesList", aimlEresourcesList);

		List<Event> aimlResearchList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_research);
		model.addAttribute("aimlResearchList", aimlResearchList);

		List<Event> aimlSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_sponsored_projects);
		model.addAttribute("aimlSponsoredProjectsList", aimlSponsoredProjectsList);

		List<Event> aimlPhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_phd_pursuing);
		model.addAttribute("aimlPhdPursuingList", aimlPhdPursuingList);

		List<Event> aimlPhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_phd_awardees);
		model.addAttribute("aimlPhdAwardeesList", aimlPhdAwardeesList);

		List<Event> aimlInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_instructional_materials);
		model.addAttribute("aimlInstructionalMaterialsList", aimlInstructionalMaterialsList);

		List<Event> aimlPedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_pedagogical_activities);
		model.addAttribute("aimlPedagogicalActivitiesList", aimlPedagogicalActivitiesList);

		List<Event> aimlPedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_pedagogy_review_form);
		model.addAttribute("aimlPedagogyReviewFormList", aimlPedagogyReviewFormList);

		List<Event> aimlcontentbeyondsyllabusList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_content_beyond_syllabus);
		model.addAttribute("aimlcontentbeyondsyllabusList", aimlcontentbeyondsyllabusList);

		List<Event> aimlLabManualList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_lab_manual_form);
		model.addAttribute("aimlLabManualList", aimlLabManualList);

		List<Event> aimlMouSignedList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_Mou_Signed);
		model.addAttribute("aimlMouSignedList", aimlMouSignedList);

		List<Event> aimlAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_alumni_association);
		model.addAttribute("aimlAlumniAssociationList", aimlAlumniAssociationList);

		List<Event> aimlProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_professional_bodies);
		model.addAttribute("aimlProfessionalBodiesList", aimlProfessionalBodiesList);

		List<Event> aimlProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_professional_links);
		model.addAttribute("aimlProfessionalLinksList", aimlProfessionalLinksList);

		List<Event> aimlHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_higher_education);
		model.addAttribute("aimlHigherEducationList", aimlHigherEducationList);

		List<Event> aimlClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_club_activities);
		model.addAttribute("aimlClubActivitiesList", aimlClubActivitiesList);

		List<Event> aimlClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_club_external_links);
		model.addAttribute("aimlClubExternalLinksList", aimlClubExternalLinksList);

		List<Event> aimlInternshipList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_internship);
		model.addAttribute("aimlInternshipList", aimlInternshipList);

		List<Event> aimlProjectsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_projects);
		model.addAttribute("aimlProjectsList", aimlProjectsList);

		List<Event> aimlMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_mini_projects);
		model.addAttribute("aimlMiniProjectsList", aimlMiniProjectsList);

		List<Event> aimlSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_social_activities);
		model.addAttribute("aimlSocialActivitiesList", aimlSocialActivitiesList);

		List<Faculty> aimlIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_industrial_visit);
		model.addAttribute("aimlIndustrialVisitList", aimlIndustrialVisitList);

		List<Faculty> aimlProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_project_exhibition);
		model.addAttribute("aimlProjectExhibitionList", aimlProjectExhibitionList);

		List<Event> aimlFdpList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_fdp);
		model.addAttribute("aimlFdpList", aimlFdpList);

		/*
		 * List<Event> aimlTimeTableList =
		 * eventService.getActiveByPageAndFieldName(PageType.aiml_page,
		 * FieldNameType.aiml_page_timetable);
		 *
		 * if (aimlTimeTableList.size() > 0) { model.addAttribute("aimlTimeTable",
		 * aimlTimeTableList.get(0)); }
		 */

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "aiml_dept";
	}

	@RequestMapping(value = "/csd_dept", method = RequestMethod.GET)
	public String csd_dept(ModelMap model) {

		List<Event> csdPageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_slider_images);
		model.addAttribute("csdPageSliderImageList", csdPageSliderImageList);

		List<Event> csdPageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_latest_events);
		model.addAttribute("csdPageLatestEventsList", csdPageLatestEventsList);

		List<Event> csdPageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_upcoming_events);
		model.addAttribute("csdPageUpcomingEventsList", csdPageUpcomingEventsList);

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> csdEventsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_events);
		model.addAttribute("csdEventsList", csdEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> csdGalleryList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_gallery);
		model.addAttribute("csdGalleryList", csdGalleryList);

		List<Event> csdClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_class_time_table);

		model.addAttribute("csdClassTimeTableList", csdClassTimeTableList);

		List<Event> csdTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_test_time_table);

		model.addAttribute("csdTestTimeTableList", csdTestTimeTableList);

		List<Event> csdCalanderList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_calander);

		model.addAttribute("csdCalanderList", csdCalanderList);

		List<Event> csdcontentbeyondsyllabusList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_content_beyond_syllabus);
		model.addAttribute("csdcontentbeyondsyllabusList", csdcontentbeyondsyllabusList);

		List<Event> csdNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_newsletter);

		model.addAttribute("csdNewsLetterList", csdNewsLetterList);

		List<Event> csdUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_ug_syllabus);

		model.addAttribute("csdUgSyllabusList", csdUgSyllabusList);

		List<Event> csdDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_department_achiever_list);
		model.addAttribute("csdDepartmentAchieversList", csdDepartmentAchieversList);

		List<Event> csdFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_fcd_students);
		model.addAttribute("csdFcdStudentsList", csdFcdStudentsList);

		List<Event> csdLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_library_books_collection);
		model.addAttribute("csdLibraryBooksList", csdLibraryBooksList);

		List<Event> csdPlacementList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_placement);
		model.addAttribute("csdPlacementList", csdPlacementList);

		List<Faculty> csdLabList = facultyService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_labs);
		model.addAttribute("csdLabList", csdLabList);

		List<Event> csdEresourcesList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_eresources);
		model.addAttribute("csdEresourcesList", csdEresourcesList);

		List<Event> csdResearchList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_research);
		model.addAttribute("csdResearchList", csdResearchList);

		List<Event> csdSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_sponsored_projects);
		model.addAttribute("csdSponsoredProjectsList", csdSponsoredProjectsList);

		List<Event> csdPhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_phd_pursuing);
		model.addAttribute("csdPhdPursuingList", csdPhdPursuingList);

		List<Event> csdPhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_phd_awardees);
		model.addAttribute("csdPhdAwardeesList", csdPhdAwardeesList);

		List<Event> csdInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_instructional_materials);
		model.addAttribute("csdInstructionalMaterialsList", csdInstructionalMaterialsList);

		List<Event> csdPedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_pedagogical_activities);
		model.addAttribute("csdPedagogicalActivitiesList", csdPedagogicalActivitiesList);

		List<Event> csdPedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_pedagogy_review_form);
		model.addAttribute("csdPedagogyReviewFormList", csdPedagogyReviewFormList);

		List<Event> csdLabManualList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_lab_manual_form);
		model.addAttribute("csdLabManualList", csdLabManualList);

		List<Event> csdMouSignedList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_Mou_Signed);
		model.addAttribute("csdMouSignedList", csdMouSignedList);

		List<Event> csdAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_alumni_association);
		model.addAttribute("csdAlumniAssociationList", csdAlumniAssociationList);

		List<Event> csdProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_professional_bodies);
		model.addAttribute("csdProfessionalBodiesList", csdProfessionalBodiesList);

		List<Event> csdProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_professional_links);
		model.addAttribute("csdProfessionalLinksList", csdProfessionalLinksList);

		List<Event> csdHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_higher_education);
		model.addAttribute("csdHigherEducationList", csdHigherEducationList);

		List<Event> csdClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_club_activities);
		model.addAttribute("csdClubActivitiesList", csdClubActivitiesList);

		List<Event> csdClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_club_external_links);
		model.addAttribute("csdClubExternalLinksList", csdClubExternalLinksList);

		List<Event> csdInternshipList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_internship);
		model.addAttribute("csdInternshipList", csdInternshipList);

		List<Event> csdProjectsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_projects);
		model.addAttribute("csdProjectsList", csdProjectsList);

		List<Event> csdMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_mini_projects);
		model.addAttribute("csdMiniProjectsList", csdMiniProjectsList);

		List<Event> csdSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_social_activities);
		model.addAttribute("csdSocialActivitiesList", csdSocialActivitiesList);
		
		List<Event> csdOtherDetailsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_other_details);
		model.addAttribute("csdOtherDetailsList", csdOtherDetailsList);

		List<Faculty> csdIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_industrial_visit);
		model.addAttribute("csdIndustrialVisitList", csdIndustrialVisitList);

		List<Faculty> csdProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_project_exhibition);
		model.addAttribute("csdProjectExhibitionList", csdProjectExhibitionList);

		List<Event> csdFdpList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_fdp);
		model.addAttribute("csdFdpList", csdFdpList);

		/*
		 * List<Event> aimlTimeTableList =
		 * eventService.getActiveByPageAndFieldName(PageType.aiml_page,
		 * FieldNameType.aiml_page_timetable);
		 *
		 * if (aimlTimeTableList.size() > 0) { model.addAttribute("aimlTimeTable",
		 * aimlTimeTableList.get(0)); }
		 */

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "csd_dept";
	}

	@RequestMapping(value = "/tele_gallery", method = RequestMethod.GET)
	public String tele_gallery(ModelMap model) {

		List<Event> teleGalleryList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_gallery);
		model.addAttribute("teleGalleryList", teleGalleryList);

		return "tele_gallery";
	}

	@RequestMapping(value = "/aiml_gallery", method = RequestMethod.GET)
	public String aiml_gallery(ModelMap model) {

		List<Event> aimlGalleryList = eventService.getActiveByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_gallery);
		model.addAttribute("aimlGalleryList", aimlGalleryList);

		return "aiml_gallery";
	}

	@RequestMapping(value = "/csd_gallery", method = RequestMethod.GET)
	public String csd_gallery(ModelMap model) {

		List<Event> csdGalleryList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_gallery);
		model.addAttribute("csdGalleryList", csdGalleryList);

		return "csd_gallery";
	}

	@RequestMapping(value = "/science_dept", method = RequestMethod.GET)
	public String science_dept(ModelMap model) {

		List<Event> scienceEventsList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_events);
		model.addAttribute("scienceEventsList", scienceEventsList);

		List<Faculty> physicsFacultyList = facultyService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_physics_faculty);
		model.addAttribute("physicsFacultyList", physicsFacultyList);

		List<Faculty> chemistryFacultyList = facultyService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_chemistry_faculty);
		model.addAttribute("chemistryFacultyList", chemistryFacultyList);

		List<Faculty> mathsFacultyList = facultyService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_maths_faculty);
		model.addAttribute("mathsFacultyList", mathsFacultyList);

		List<Faculty> humanitiesFacultyList = facultyService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_humanities_faculty);
		model.addAttribute("humanitiesFacultyList", humanitiesFacultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_supporting_staff_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> scienceGalleryList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_gallery);
		model.addAttribute("scienceGalleryList", scienceGalleryList);

		List<Event> scienceClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_class_time_table);

		model.addAttribute("scienceClassTimeTableList", scienceClassTimeTableList);

		List<Event> scienceTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_test_time_table);

		model.addAttribute("scienceTestTimeTableList", scienceTestTimeTableList);

		List<Event> scienceCalanderList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_calander);

		model.addAttribute("scienceCalanderList", scienceCalanderList);

		List<Event> scienceNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_newsletter);

		model.addAttribute("scienceNewsLetterList", scienceNewsLetterList);

		List<Event> physicsSyllabusList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_physics_syllabus);

		model.addAttribute("physicsSyllabusList", physicsSyllabusList);

		List<Event> chemistrySyllabusList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_chemistry_syllabus);

		model.addAttribute("chemistrySyllabusList", chemistrySyllabusList);

		List<Event> mathematicsSyllabusList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_mathematics_syllabus);

		model.addAttribute("mathematicsSyllabusList", mathematicsSyllabusList);

		List<Faculty> scienceLabList = facultyService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_labs);
		model.addAttribute("scienceLabList", scienceLabList);

		List<Event> scienceEresourcesList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_eresources);
		model.addAttribute("scienceEresourcesList", scienceEresourcesList);

		List<Event> scienceResearchList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_research);
		model.addAttribute("scienceResearchList", scienceResearchList);

		List<Event> scienceSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_sponsored_projects);
		model.addAttribute("scienceSponsoredProjectsList", scienceSponsoredProjectsList);

		List<Event> sciencePhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_phd_pursuing);
		model.addAttribute("sciencePhdPursuingList", sciencePhdPursuingList);

		List<Event> sciencePhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_phd_awardees);
		model.addAttribute("sciencePhdAwardeesList", sciencePhdAwardeesList);

		List<Event> scienceInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_instructional_materials);
		model.addAttribute("scienceInstructionalMaterialsList", scienceInstructionalMaterialsList);

		List<Event> sciencePedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_pedagogical_activities);
		model.addAttribute("sciencePedagogicalActivitiesList", sciencePedagogicalActivitiesList);

		List<Event> sciencePedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_pedagogy_review_form);
		model.addAttribute("sciencePedagogyReviewFormList", sciencePedagogyReviewFormList);

		List<Event> scienceLabManualList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_lab_manual_form);
		model.addAttribute("scienceLabManualList", scienceLabManualList);

		List<Event> scienceFdpList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_fdp);
		model.addAttribute("scienceFdpList", scienceFdpList);

		return "science_dept";
	}

	@RequestMapping(value = "/science_events", method = RequestMethod.GET)
	public String science_events(ModelMap model) {
		List<Event> scienceEventsList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_events);
		model.addAttribute("scienceEventsList", scienceEventsList);
		return "science_events";
	}

	@RequestMapping(value = "/science_gallery", method = RequestMethod.GET)
	public String science_gallery(ModelMap model) {

		List<Event> scienceGalleryList = eventService.getActiveByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_gallery);
		model.addAttribute("scienceGalleryList", scienceGalleryList);

		return "science_gallery";
	}

	@RequestMapping(value = "/nss", method = RequestMethod.GET)
	public String nss(ModelMap model) {

		List<Event> nssGalleryList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_page_gallery);
		model.addAttribute("nssGalleryList", nssGalleryList);

		List<Data> upComingEventsDataList = dataService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.upcoming_events);
		List<String> upComingEvents = new ArrayList<String>();
		for (int i = 0; i < upComingEventsDataList.size(); i++) {
			upComingEvents.add(upComingEventsDataList.get(i).getContent());
		}
		model.addAttribute("upComingEvents", upComingEvents);

		List<Event> nssEventList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_events);
		model.addAttribute("nssEventList", nssEventList);

		List<Event> nssAchievementsList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_achievements);
		model.addAttribute("nssAchievementsList", nssAchievementsList);

		List<Event> nssReportsList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_reports);
		model.addAttribute("nssReportsList", nssReportsList);

		return "nss";
	}
	
	@RequestMapping(value = "/iqac", method = RequestMethod.GET)
	public String iqac(ModelMap model) {

		List<Event> nssGalleryList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_page_gallery);
		model.addAttribute("nssGalleryList", nssGalleryList);

		List<Data> upComingEventsDataList = dataService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.upcoming_events);
		List<String> upComingEvents = new ArrayList<String>();
		for (int i = 0; i < upComingEventsDataList.size(); i++) {
			upComingEvents.add(upComingEventsDataList.get(i).getContent());
		}
		model.addAttribute("upComingEvents", upComingEvents);

		List<Event> nssEventList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_events);
		model.addAttribute("nssEventList", nssEventList);

		List<Event> nssAchievementsList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_achievements);
		model.addAttribute("nssAchievementsList", nssAchievementsList);

		List<Event> nssReportsList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_reports);
		model.addAttribute("nssReportsList", nssReportsList);
		
		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.iqac_page,
				FieldNameType.iqac_page_faculty);
		model.addAttribute("facultyList", facultyList);
		
		List<Event> annualCompositionList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_annual_composition);
		model.addAttribute("annualCompositionList", annualCompositionList);
		
		List<Event> minutesOfMeetingList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_minutes_of_meeting);
		model.addAttribute("minutesOfMeetingList", minutesOfMeetingList);
		
		List<Event> bestPractiseList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_best_practises);
		model.addAttribute("bestPractiseList", bestPractiseList);
		
		List<Event> annualReportsList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_annual_reports);
		model.addAttribute("annualReportsList", annualReportsList);
		
		List<Event> otherReportsList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_other_reports);
		model.addAttribute("otherReportsList", otherReportsList);

		return "iqac";
	}

	@RequestMapping(value = "/nss_events", method = RequestMethod.GET)
	public String nss_events(ModelMap model) {

		List<Event> nssEventsList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_events);
		model.addAttribute("nssEventsList", nssEventsList);

		return "nss_events";
	}

	@RequestMapping(value = "/nss_gallery", method = RequestMethod.GET)
	public String nss_gallery(ModelMap model) {

		List<Event> nssGalleryList = eventService.getActiveByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_page_gallery);
		model.addAttribute("nssGalleryList", nssGalleryList);

		return "nss_gallery";
	}

	@RequestMapping(value = "/redcross", method = RequestMethod.GET)
	public String redcross(ModelMap model) {

		List<Event> redcrossGalleryList = eventService.getActiveByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_page_gallery);
		model.addAttribute("redcrossGalleryList", redcrossGalleryList);

		List<Data> upComingEventsDataList = dataService.getActiveByPageAndFieldName(PageType.redcross_page,
				FieldNameType.upcoming_events);
		List<String> upComingEvents = new ArrayList<String>();
		for (int i = 0; i < upComingEventsDataList.size(); i++) {
			upComingEvents.add(upComingEventsDataList.get(i).getContent());
		}
		model.addAttribute("upComingEvents", upComingEvents);

		List<Event> redcrossEventList = eventService.getActiveByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_events);
		model.addAttribute("redcrossEventList", redcrossEventList);

		List<Event> redcrossAchievementsList = eventService.getActiveByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_achievements);
		model.addAttribute("redcrossAchievementsList", redcrossAchievementsList);

		return "redcross";
	}

	@RequestMapping(value = "/redcross_events", method = RequestMethod.GET)
	public String redcross_events(ModelMap model) {

		List<Event> redcrossEventsList = eventService.getActiveByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_events);
		model.addAttribute("redcrossEventsList", redcrossEventsList);

		return "redcross_events";
	}

	@RequestMapping(value = "/redcross_gallery", method = RequestMethod.GET)
	public String redcross_gallery(ModelMap model) {

		List<Event> redcrossGalleryList = eventService.getActiveByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_page_gallery);
		model.addAttribute("redcrossGalleryList", redcrossGalleryList);

		return "redcross_gallery";
	}

	@RequestMapping(value = "/coursesOffered", method = RequestMethod.GET)
	public String coursesOffered(ModelMap model) {
		model.addAttribute("courseList", COURSE.values());
		return "coursesOffered";
	}

	@RequestMapping(value = "/fees_structure", method = RequestMethod.GET)
	public String fees_structure(ModelMap model) {

		List<Event> feeStructureList = eventService.getActiveByPageAndFieldName(PageType.fee_structure_page,
				FieldNameType.fee_structure_page_fees);
		model.addAttribute("feeStructureList", feeStructureList);

		return "fees_structure";
	}

	@RequestMapping(value = "/about_library", method = RequestMethod.GET)
	public String about_library(ModelMap model) {
		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> libraryPageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_slider_images);
		model.addAttribute("libraryPageSliderImageList", libraryPageSliderImageList);

		List<Event> libraryBooksList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_books_collection);
		model.addAttribute("libraryBooksList", libraryBooksList);

		List<Event> newArrivalList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_newArrivals);
		model.addAttribute("newArrivalList", newArrivalList);

		List<Event> libraryLinksList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_links);

		model.addAttribute("libraryLinksList", libraryLinksList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Event> libraryGalleryList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_gallery);
		model.addAttribute("libraryGalleryList", libraryGalleryList);

		List<Event> libraryEresourcesList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_eresources);
		model.addAttribute("libraryEresourcesList", libraryEresourcesList);

		List<Event> libraryOtherDetailsList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_other_details);
		model.addAttribute("libraryOtherDetailsList", libraryOtherDetailsList);

		return "about_library";
	}

	@RequestMapping(value = "/library_eresources", method = RequestMethod.GET)
	public String library_eresources(ModelMap model) {

		List<Event> libraryEresourcesList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_eresources);
		model.addAttribute("libraryEresourcesList", libraryEresourcesList);

		return "library_eresources";
	}

	@RequestMapping(value = "/library_infrastructure", method = RequestMethod.GET)
	public String library_infrastructure(ModelMap model) {

		return "library_infrastructure";
	}

	@RequestMapping(value = "/library_services", method = RequestMethod.GET)
	public String library_services(ModelMap model) {

		return "library_services";
	}

	@RequestMapping(value = "/library_staff", method = RequestMethod.GET)
	public String library_staff(ModelMap model) {

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_faculty);
		model.addAttribute("facultyList", facultyList);

		return "library_staff";
	}

	@RequestMapping(value = "/library_gallery", method = RequestMethod.GET)
	public String library_gallery(ModelMap model) {

		List<Event> libraryGalleryList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_gallery);
		model.addAttribute("libraryGalleryList", libraryGalleryList);

		return "library_gallery";
	}

	@RequestMapping(value = "/library_new_arrivals", method = RequestMethod.GET)
	public String library_new_arrivals(ModelMap model) {

		List<Event> newArrivalList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_newArrivals);
		model.addAttribute("newArrivalList", newArrivalList);

		return "library_new_arrivals";
	}

	@RequestMapping(value = "/library_activities", method = RequestMethod.GET)
	public String library_activities(ModelMap model) {

		List<Event> libraryActivityList = eventService.getActiveByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_activities);
		model.addAttribute("libraryActivityList", libraryActivityList);

		return "library_activities";
	}

	@RequestMapping(value = "/sports", method = RequestMethod.GET)
	public String sports(ModelMap model) {

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Event> sportsGalleryList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_page_gallery);
		model.addAttribute("sportsGalleryList", sportsGalleryList);

		List<Event> sportsActivitiesList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_activities);
		model.addAttribute("sportsActivitiesList", sportsActivitiesList);

		List<Event> sportsAchieversList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_achiever_list);
		model.addAttribute("sportsAchieversList", sportsAchieversList);

		List<Event> universityAchieversList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.university_achiever_list);
		model.addAttribute("universityAchieversList", universityAchieversList);

		List<Event> stateAchieversList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.state_achiever_list);
		model.addAttribute("stateAchieversList", stateAchieversList);

		List<Event> nationalAchieversList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.national_achiever_list);
		model.addAttribute("nationalAchieversList", nationalAchieversList);

		List<Event> sportsReportsList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_reports_list);
		model.addAttribute("sportsReportsList", sportsReportsList);

		return "sports";
	}

	@RequestMapping(value = "/sports_gallery", method = RequestMethod.GET)
	public String sports_gallery(ModelMap model) {

		List<Event> sportsGalleryList = eventService.getActiveByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_page_gallery);
		model.addAttribute("sportsGalleryList", sportsGalleryList);

		return "sports_gallery";
	}

	@RequestMapping(value = "/hostel", method = RequestMethod.GET)
	public String hostel(ModelMap model) {

		List<Event> hostelFeesList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.hostel_fees);
		model.addAttribute("hostelFeesList", hostelFeesList);

		return "hostel";
	}

	@RequestMapping(value = "/canteen", method = RequestMethod.GET)
	public String canteen(ModelMap model) {

		return "canteen";
	}

	@RequestMapping(value = "/transport", method = RequestMethod.GET)
	public String transport(ModelMap model) {

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.transport_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.transport_page,
				FieldNameType.transport_page_faculty);
		model.addAttribute("facultyList", facultyList);

		return "transport";
	}

	@RequestMapping(value = "/alumni", method = RequestMethod.GET)
	public String alumni(ModelMap model) {

		List<Event> alumniMeetsList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.alumni_meets);
		model.addAttribute("alumniMeetsList", alumniMeetsList);

		List<Event> cseAlumniMembersList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.cse_alumni_members_list);
		model.addAttribute("cseAlumniMembersList", cseAlumniMembersList);

		List<Event> eceAlumniMembersList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.ece_alumni_members_list);
		model.addAttribute("eceAlumniMembersList", eceAlumniMembersList);

		List<Event> mechAlumniMembersList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.mech_alumni_members_list);
		model.addAttribute("mechAlumniMembersList", mechAlumniMembersList);

		List<Event> teleAlumniMembersList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.tele_alumni_members_list);
		model.addAttribute("teleAlumniMembersList", teleAlumniMembersList);

		return "alumni_old";
	}

	@RequestMapping(value = "/alumni_gallery", method = RequestMethod.GET)
	public String alumni_gallery(ModelMap model) {

		List<Event> alumniGalleryList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.alumni_page_gallery);
		model.addAttribute("alumniGalleryList", alumniGalleryList);

		return "alumni_gallery";
	}

	@RequestMapping(value = "/alumni_registration", method = RequestMethod.GET)
	public String alumni_registration(ModelMap model) {

		return "alumni_registration";
	}

	@RequestMapping(value = "/alumni_registration", method = RequestMethod.POST)
	public String alumni_registrationPost(ModelMap model, @BeanParam AlumniForm alumniForm) {
		Alumni alumni = new Alumni();
		BeanUtils.copyProperties(alumniForm, alumni);
		alumniService.saveOrUpdate(alumni);
		return "redirect:/alumni";
	}

	@RequestMapping(value = "/alumni_list", method = RequestMethod.GET)
	public String alumni_list(ModelMap model) {

		List<Alumni> alumniList = alumniService.findAll();
		model.addAttribute("alumniList", alumniList);

		return "alumni_list";
	}

	@RequestMapping(value = "/clubs", method = RequestMethod.GET)
	public String clubs(ModelMap model) {

		return "clubs";
	}

	@RequestMapping(value = "/grievance", method = RequestMethod.GET)
	public String grievance(ModelMap model) {
		return "grievance";
	}

	@RequestMapping(value = "/grievance", method = RequestMethod.POST)
	public String grievancePost(ModelMap model, @RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "course", required = false) String course,
			@RequestParam(value = "branch", required = false) String branch,
			@RequestParam(value = "usn", required = false) String usn,
			@RequestParam(value = "mobileNumber", required = false) String mobileNumber,
			@RequestParam(value = "emailId", required = false) String emailId,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "description", required = false) String description) {

		/*
		 * grievanceService.saveNewGrievance(name, category, course, branch, usn,
		 * mobileNumber, emailId, subject, description);
		 */

		Grievance grievance = new Grievance(name, category, course, branch, usn, mobileNumber, emailId, subject,
				description);
		grievanceService.addNewGrievanceInformation(grievance);

		return "redirect:/grievance";
	}

	@RequestMapping(value = "/ananya", method = RequestMethod.GET)
	public String ananya(ModelMap model) {

		List<Event> ananyaGalleryList = eventService.getActiveByPageAndFieldName(PageType.ananya_page,
				FieldNameType.ananya_page_gallery);
		model.addAttribute("ananyaGalleryList", ananyaGalleryList);

		return "ananya";
	}

	@RequestMapping(value = "/academic_calender", method = RequestMethod.GET)
	public String academic_calender(ModelMap model) {
		List<Event> academicCalanderList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.academic_page_calander);

		model.addAttribute("academicCalanderList", academicCalanderList);

		return "academic_calender";
	}

	@RequestMapping(value = "/vtu_ranks", method = RequestMethod.GET)
	public String vtu_ranks(ModelMap model) {

		return "vtu_ranks";
	}

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String contact(ModelMap model) {

		return "contact";
	}

	@RequestMapping(value = "/contact", method = RequestMethod.POST)
	public String contactPost(ModelMap model, @RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "phone", required = false) String phone,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "description", required = false) String description) {

		ContactUs contactUs = new ContactUs(name, email, phone, subject, description);
		contactUsService.addNewContactInformation(contactUs);
		return "redirect:/contact";
	}
	
	@RequestMapping(value = "/feedback", method = RequestMethod.GET)
	public String feedback(ModelMap model) {

		return "feedback";
	}
	
	@RequestMapping(value = "/feedback", method = RequestMethod.POST)
	public String feedbackPost(ModelMap model, @RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "phone", required = false) String phone,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "description", required = false) String description) {

		Feedback feedback = new Feedback(name, email, phone, subject, description);
		feedbackService.add(feedback);
		return "redirect:/feedback";
	}

	@RequestMapping(value = "/career", method = RequestMethod.GET)
	public String career(ModelMap model) {

		return "career";
	}

	@RequestMapping(value = "/sitemap", method = RequestMethod.GET)
	public String sitemap(ModelMap model) {

		return "sitemap";
	}

	@RequestMapping(value = "/public_press", method = RequestMethod.GET)
	public String public_press(ModelMap model) {

		List<Event> publicPressList = eventService.getActiveByPageAndFieldName(PageType.public_press_page,
				FieldNameType.public_press_page_achievement);
		model.addAttribute("publicPressList", publicPressList);

		return "public_press";
	}

	@RequestMapping(value = "/disciplinary_measures", method = RequestMethod.GET)
	public String disciplinary_measures(ModelMap model) {

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.disciplinary_measures_page,
				FieldNameType.disciplinary_page_faculty);
		model.addAttribute("facultyList", facultyList);

		return "disciplinary_measures";
	}

	@RequestMapping(value = "/achievers_cse", method = RequestMethod.GET)
	public String achievers_cse(ModelMap model) {

		List<Event> cseDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_department_achiever_list);
		model.addAttribute("cseDepartmentAchieversList", cseDepartmentAchieversList);

		return "achievers_cse";
	}

	@RequestMapping(value = "/achievers_ece", method = RequestMethod.GET)
	public String achievers_ece(ModelMap model) {

		List<Event> eceDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_department_achiever_list);
		model.addAttribute("eceDepartmentAchieversList", eceDepartmentAchieversList);

		return "achievers_ece";
	}

	@RequestMapping(value = "/achievers_mech", method = RequestMethod.GET)
	public String achievers_mech(ModelMap model) {

		List<Event> mechDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_department_achiever_list);
		model.addAttribute("mechDepartmentAchieversList", mechDepartmentAchieversList);

		return "achievers_mech";
	}

	@RequestMapping(value = "/achievers_tele", method = RequestMethod.GET)
	public String achievers_tele(ModelMap model) {

		List<Event> teleDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_department_achiever_list);
		model.addAttribute("teleDepartmentAchieversList", teleDepartmentAchieversList);

		return "achievers_tele";
	}

	@RequestMapping(value = "/achievers_csd", method = RequestMethod.GET)
	public String achievers_csd(ModelMap model) {

		List<Event> csdDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_department_achiever_list);
		model.addAttribute("csdDepartmentAchieversList", csdDepartmentAchieversList);

		return "achievers_csd";
	}

	@RequestMapping(value = "/cse_events", method = RequestMethod.GET)
	public String cse_events(ModelMap model) {

		List<Event> cseEventsList = eventService.getActiveByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_events);
		model.addAttribute("cseEventsList", cseEventsList);

		return "cse_events";
	}

	@RequestMapping(value = "/ece_events", method = RequestMethod.GET)
	public String ece_events(ModelMap model) {

		List<Event> eceEventsList = eventService.getActiveByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_events);
		model.addAttribute("eceEventsList", eceEventsList);

		return "ece_events";
	}

	@RequestMapping(value = "/mech_events", method = RequestMethod.GET)
	public String mech_events(ModelMap model) {

		List<Event> mechEventsList = eventService.getActiveByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_events);
		model.addAttribute("mechEventsList", mechEventsList);

		return "mech_events";
	}

	@RequestMapping(value = "/tele_events", method = RequestMethod.GET)
	public String tele_events(ModelMap model) {

		List<Event> teleEventsList = eventService.getActiveByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_events);
		model.addAttribute("teleEventsList", teleEventsList);

		return "tele_events";
	}

	@RequestMapping(value = "/csd_events", method = RequestMethod.GET)
	public String csd_events(ModelMap model) {

		List<Event> csdEventsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_events);
		model.addAttribute("csdEventsList", csdEventsList);

		return "csd_events";
	}

	@RequestMapping(value = "/academic_advisory_board", method = RequestMethod.GET)
	public String academic_advisory_board(ModelMap model) {

		return "academic_advisory_board";
	}

	@RequestMapping(value = "/ceo", method = RequestMethod.GET)
	public String ceo(ModelMap model) {

		return "ceo";
	}

	@RequestMapping(value = "/directorAcademics", method = RequestMethod.GET)
	public String directorAcademics(ModelMap model) {

		return "directorAcademics";
	}

	@RequestMapping(value = "/academic_governing_council", method = RequestMethod.GET)
	public String academic_governing_council(ModelMap model) {

		List<Faculty> academicGoverningCouncilFacultyList = facultyService.getActiveByPageAndFieldName(
				PageType.academic_governing_council_page, FieldNameType.academic_governing_council_faculty);
		model.addAttribute("academicGoverningCouncilFacultyList", academicGoverningCouncilFacultyList);

		return "academic_governing_council";
	}

	@RequestMapping(value = "/naac", method = RequestMethod.GET)
	public String naac(ModelMap model) {
		Map<Integer, Map<Criteria, List<NAAC>>> yearMap = naacService.groupByYear(PageType.naac_page);
		List<Integer> yearList = new ArrayList<Integer>();
		for (Integer year : yearMap.keySet()) {
			yearList.add(year);
		}
		model.addAttribute("yearMap", yearMap);
		model.addAttribute("yearList", yearList);
		return "naac";
	}
	
	@RequestMapping(value = "/naac_cycle2_aqar", method = RequestMethod.GET)
	public String naac_cycle2_aqar(ModelMap model) {
		Map<Integer, Map<Criteria, List<NAAC>>> yearMap = naacService.groupByYear(PageType.naac_cycle2_aqar_page);
		List<Integer> yearList = new ArrayList<Integer>();
		for (Integer year : yearMap.keySet()) {
			yearList.add(year);
		}
		model.addAttribute("yearMap", yearMap);
		model.addAttribute("yearList", yearList);
		return "naac_cycle2_aqar";
	}

	@RequestMapping(value = "/naac_cycle2_ssr", method = RequestMethod.GET)
	public String naac_cycle2_ssr(ModelMap model) {
		Map<Criteria, List<NAAC>> criteriaMap = naacService.groupByCriteria(PageType.naac_cycle2_page);
		List<Criteria> criteriaList = new ArrayList<>();
		for (Criteria criteria : criteriaMap.keySet()) {
			criteriaList.add(criteria);
		}
		model.addAttribute("criteriaList", criteriaList);
		model.addAttribute("criteriaMap", criteriaMap);
		return "naac_cycle2_ssr";
	}
	
	@RequestMapping(value = "/naac_cycle2_dvv_clarification", method = RequestMethod.GET)
	public String naac_cycle2_dvv_clarification(ModelMap model) {
		Map<Criteria, List<NAAC>> criteriaMap = naacService.groupByCriteria(PageType.naac_cycle2_dvv_clarification_page);
		List<NAAC> extendProfiles = naacService.getAllByPage(PageType.naac_cycle2_dvv_clarification_extended_profile_page);		
		List<Criteria> criteriaList = new ArrayList<>();
		for (Criteria criteria : criteriaMap.keySet()) {
			criteriaList.add(criteria);
		}
		model.addAttribute("criteriaList", criteriaList);
		model.addAttribute("criteriaMap", criteriaMap);
		model.addAttribute("extendProfiles", extendProfiles);
		return "naac_cycle2_dvv_clarification";
	}

	@RequestMapping(value = "/onlineadmissions", method = RequestMethod.GET)
	public String online_admissions(ModelMap model) {

		return "onlineadmissions";
	}

	@RequestMapping(value = "/onlineadmissions", method = RequestMethod.POST)
	public String online_admissions_post(ModelMap model, @BeanParam OnlineAdmissionsForm onlineAdmissionsForm) {

		onlineAdmissionsForm.saveFiles();
		OnlineAdmission onlineAdmission = new OnlineAdmission();
		BeanUtils.copyProperties(onlineAdmissionsForm, onlineAdmission);
		onlineAdmissionService.saveOrUpdate(onlineAdmission);
		return "onlineadmissions";
	}

	@RequestMapping(value = "/iiic", method = RequestMethod.GET)
	public String iiic(ModelMap model) {
		List<Event> iiicGalleryList = eventService.getActiveByPageAndFieldName(PageType.iiic_page,
				FieldNameType.iiic_page_gallery);
		model.addAttribute("iiicGalleryList", iiicGalleryList);

		List<Event> mouList = eventService.getActiveByPageAndFieldName(PageType.iiic_page,
				FieldNameType.iiic_page_Mou_Signed);
		model.addAttribute("mouList", mouList);
		
		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.iiic_page,
				FieldNameType.iiic_page_faculty);
		model.addAttribute("facultyList", facultyList);
		
		List<Event> activitiesEventsList = eventService.getByPageAndFieldName(PageType.iiic_page, FieldNameType.iiic_events);
		model.addAttribute("activitiesEventsList", activitiesEventsList);
		return "iiic";
	}

	@RequestMapping(value = "/nba_previsit_institution", method = RequestMethod.GET)
	public String nba_previsit_institution(ModelMap model) {
		List<NBAPrevisit> nbaPrevisitList = nbaPrevisitService.getByPage(PageType.institution_page);
		model.addAttribute("heading", "NBA Documents - Institutional");
		model.addAttribute("nbaPrevisitList", nbaPrevisitList);
		return "nba_previsit_institution";
	}

	@RequestMapping(value = "/nba_previsit_dept_cse", method = RequestMethod.GET)
	public String nba_previsit_dept_cse(ModelMap model) {
		List<NBAPrevisit> nbaPrevisitList = nbaPrevisitService.getByPage(PageType.cse_page);
		model.addAttribute("nbaPrevisitList", nbaPrevisitList);
		model.addAttribute("heading", "NBA Documents - Computer Science & Engineering ");
		return "nba_previsit_institution";
	}

	@RequestMapping(value = "/nba_previsit_dept_ece", method = RequestMethod.GET)
	public String nba_previsit_dept_ece(ModelMap model) {
		List<NBAPrevisit> nbaPrevisitList = nbaPrevisitService.getByPage(PageType.ece_page);
		model.addAttribute("nbaPrevisitList", nbaPrevisitList);
		model.addAttribute("heading", "NBA Documents - Electronics and Communication Engineering ");
		return "nba_previsit_institution";
	}

	@RequestMapping(value = "/nba_previsit_dept_mech", method = RequestMethod.GET)
	public String nba_previsit_dept_mech(ModelMap model) {
		List<NBAPrevisit> nbaPrevisitList = nbaPrevisitService.getByPage(PageType.mech_page);
		model.addAttribute("nbaPrevisitList", nbaPrevisitList);
		model.addAttribute("heading", "NBA Documents - Mechanical Engineering");
		return "nba_previsit_institution";
	}

	@RequestMapping(value = "/nba_previsit", method = RequestMethod.GET)
	public String nba_previsit(ModelMap model) {
		return "nba_previsit";
	}

	@RequestMapping(value = "/nba_previsit_dept", method = RequestMethod.GET)
	public String nba_previsit_dept(ModelMap model) {
		return "nba_previsit_dept";
	}

	@RequestMapping(value = "/hostel", method = RequestMethod.POST)
	public String hostelPost(ModelMap model, @RequestParam(value = "collegeName", required = false) String collegeName,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "sem", required = false) String sem,
			@RequestParam(value = "phone", required = false) String phone,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "fatherName", required = false) String fatherName,
			@RequestParam(value = "fatherPhone", required = false) String fatherPhone,
			@RequestParam(value = "motherName", required = false) String motherName,
			@RequestParam(value = "motherPhone", required = false) String motherPhone,
			@RequestParam(value = "address", required = false) String address) {

		HostelEnrollment hostelEnrollment = new HostelEnrollment(collegeName, firstName, lastName, year, sem, phone,
				email, fatherName, fatherPhone, motherName, motherPhone, address);
		hostelEnrollmentService.saveOrUpdate(hostelEnrollment);
		List<Event> hostelFeesList = eventService.getActiveByPageAndFieldName(PageType.home_page,
				FieldNameType.hostel_fees);
		model.addAttribute("hostelFeesList", hostelFeesList);
		model.addAttribute("showSuccess", true);
		return "hostel";
	}

	@RequestMapping(value = "/institutionalGovernance", method = RequestMethod.GET)
	public String institutionalGovernanceGet(ModelMap model) {
		List<InstitutionalGovernance> institutionalGovernanceList = institutionalGovernanceService.getAll();
		model.addAttribute("institutionalGovernanceList", institutionalGovernanceList);
		return "institutionalGovernance";
	}
	
	@RequestMapping(value = "/csicb_dept", method = RequestMethod.GET)
	public String csicb_dept(ModelMap model) {

		List<Event> csicbPageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_slider_images);
		model.addAttribute("csicbPageSliderImageList", csicbPageSliderImageList);

		List<Event> csicbPageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_latest_events);
		model.addAttribute("csicbPageLatestEventsList", csicbPageLatestEventsList);

		List<Event> csicbPageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_upcoming_events);
		model.addAttribute("csicbPageUpcomingEventsList", csicbPageUpcomingEventsList);

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> csicbEventsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_events);
		model.addAttribute("csicbEventsList", csicbEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> csicbGalleryList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_gallery);
		model.addAttribute("csicbGalleryList", csicbGalleryList);

		List<Event> csicbClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_class_time_table);

		model.addAttribute("csicbClassTimeTableList", csicbClassTimeTableList);

		List<Event> csicbTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_test_time_table);

		model.addAttribute("csicbTestTimeTableList", csicbTestTimeTableList);

		List<Event> csicbCalanderList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_calander);

		model.addAttribute("csicbCalanderList", csicbCalanderList);

		List<Event> csicbNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_newsletter);

		model.addAttribute("csicbNewsLetterList", csicbNewsLetterList);

		List<Event> csicbUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_ug_syllabus);

		model.addAttribute("csicbUgSyllabusList", csicbUgSyllabusList);

		List<Event> csicbDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_department_achiever_list);
		model.addAttribute("csicbDepartmentAchieversList", csicbDepartmentAchieversList);

		List<Event> csicbFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_fcd_students);
		model.addAttribute("csicbFcdStudentsList", csicbFcdStudentsList);

		List<Event> csicbLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_library_books_collection);
		model.addAttribute("csicbLibraryBooksList", csicbLibraryBooksList);

		List<Event> csicbPlacementList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_placement);
		model.addAttribute("csicbPlacementList", csicbPlacementList);

		List<Faculty> csicbLabList = facultyService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_labs);
		model.addAttribute("csicbLabList", csicbLabList);

		List<Event> csicbEresourcesList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_eresources);
		model.addAttribute("csicbEresourcesList", csicbEresourcesList);

		List<Event> csicbResearchList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_research);
		model.addAttribute("csicbResearchList", csicbResearchList);

		List<Event> csicbSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_sponsored_projects);
		model.addAttribute("csicbSponsoredProjectsList", csicbSponsoredProjectsList);

		List<Event> csicbPhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_phd_pursuing);
		model.addAttribute("csicbPhdPursuingList", csicbPhdPursuingList);

		List<Event> csicbPhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_phd_awardees);
		model.addAttribute("csicbPhdAwardeesList", csicbPhdAwardeesList);

		List<Event> csicbInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_instructional_materials);
		model.addAttribute("csicbInstructionalMaterialsList", csicbInstructionalMaterialsList);

		List<Event> csicbPedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_pedagogical_activities);
		model.addAttribute("csicbPedagogicalActivitiesList", csicbPedagogicalActivitiesList);

		List<Event> csicbPedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_pedagogy_review_form);
		model.addAttribute("csicbPedagogyReviewFormList", csicbPedagogyReviewFormList);

		List<Event> csicbcontentbeyondsyllabusList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_content_beyond_syllabus);
		model.addAttribute("csicbcontentbeyondsyllabusList", csicbcontentbeyondsyllabusList);

		List<Event> csicbLabManualList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_lab_manual_form);
		model.addAttribute("csicbLabManualList", csicbLabManualList);

		List<Event> csicbMouSignedList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_Mou_Signed);
		model.addAttribute("csicbMouSignedList", csicbMouSignedList);

		List<Event> csicbAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_alumni_association);
		model.addAttribute("csicbAlumniAssociationList", csicbAlumniAssociationList);

		List<Event> csicbProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_professional_bodies);
		model.addAttribute("csicbProfessionalBodiesList", csicbProfessionalBodiesList);

		List<Event> csicbProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_professional_links);
		model.addAttribute("csicbProfessionalLinksList", csicbProfessionalLinksList);

		List<Event> csicbHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_higher_education);
		model.addAttribute("csicbHigherEducationList", csicbHigherEducationList);

		List<Event> csicbClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_club_activities);
		model.addAttribute("csicbClubActivitiesList", csicbClubActivitiesList);

		List<Event> csicbClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_club_external_links);
		model.addAttribute("csicbClubExternalLinksList", csicbClubExternalLinksList);

		List<Event> csicbInternshipList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_internship);
		model.addAttribute("csicbInternshipList", csicbInternshipList);

		List<Event> csicbProjectsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_projects);
		model.addAttribute("csicbProjectsList", csicbProjectsList);

		List<Event> csicbMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_mini_projects);
		model.addAttribute("csicbMiniProjectsList", csicbMiniProjectsList);

		List<Event> csicbSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_social_activities);
		model.addAttribute("csicbSocialActivitiesList", csicbSocialActivitiesList);

		List<Faculty> csicbIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_industrial_visit);
		model.addAttribute("csicbIndustrialVisitList", csicbIndustrialVisitList);

		List<Faculty> csicbProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_project_exhibition);
		model.addAttribute("csicbProjectExhibitionList", csicbProjectExhibitionList);

		List<Event> csicbFdpList = eventService.getActiveByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_fdp);
		model.addAttribute("csicbFdpList", csicbFdpList);

		/*
		 * List<Event> csicbTimeTableList =
		 * eventService.getActiveByPageAndFieldName(PageType.csicb_page,
		 * FieldNameType.csicb_page_timetable);
		 *
		 * if (csicbTimeTableList.size() > 0) { model.addAttribute("csicbTimeTable",
		 * csicbTimeTableList.get(0)); }
		 */

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "csicb_dept";
	}
	
	@RequestMapping(value = "/cce_dept", method = RequestMethod.GET)
	public String cce_dept(ModelMap model) {

		List<Event> ccePageSliderImageList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_slider_images);
		model.addAttribute("ccePageSliderImageList", ccePageSliderImageList);

		List<Event> ccePageLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_latest_events);
		model.addAttribute("ccePageLatestEventsList", ccePageLatestEventsList);

		List<Event> ccePageUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_upcoming_events);
		model.addAttribute("ccePageUpcomingEventsList", ccePageUpcomingEventsList);

		List<Data> scrollingTextBottomList = dataService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.scrolling_text_bottom);
		String scrollingTextBottom = "";
		if (scrollingTextBottomList.size() > 0) {
			scrollingTextBottom = scrollingTextBottomList.get(0).getContent();
		}
		model.addAttribute("scrollingTextBottom", scrollingTextBottom);

		List<Event> cceEventsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_events);
		model.addAttribute("cceEventsList", cceEventsList);

		List<Event> testimonialList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);

		List<Event> achieverList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_achiever);
		model.addAttribute("achieverList", achieverList);

		List<Faculty> facultyList = facultyService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_faculty);
		model.addAttribute("facultyList", facultyList);

		List<Faculty> supportingStaffFacultyList = facultyService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);

		List<Event> cceGalleryList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_gallery);
		model.addAttribute("cceGalleryList", cceGalleryList);

		List<Event> cceClassTimeTableList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_class_time_table);

		model.addAttribute("cceClassTimeTableList", cceClassTimeTableList);

		List<Event> cceTestTimeTableList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_test_time_table);

		model.addAttribute("cceTestTimeTableList", cceTestTimeTableList);

		List<Event> cceCalanderList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_calander);

		model.addAttribute("cceCalanderList", cceCalanderList);

		List<Event> cceNewsLetterList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_newsletter);

		model.addAttribute("cceNewsLetterList", cceNewsLetterList);

		List<Event> cceUgSyllabusList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_ug_syllabus);

		model.addAttribute("cceUgSyllabusList", cceUgSyllabusList);

		List<Event> cceDepartmentAchieversList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_department_achiever_list);
		model.addAttribute("cceDepartmentAchieversList", cceDepartmentAchieversList);

		List<Event> cceFcdStudentsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_fcd_students);
		model.addAttribute("cceFcdStudentsList", cceFcdStudentsList);

		List<Event> cceLibraryBooksList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_library_books_collection);
		model.addAttribute("cceLibraryBooksList", cceLibraryBooksList);

		List<Event> ccePlacementList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_placement);
		model.addAttribute("ccePlacementList", ccePlacementList);

		List<Faculty> cceLabList = facultyService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_labs);
		model.addAttribute("cceLabList", cceLabList);

		List<Event> cceEresourcesList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_eresources);
		model.addAttribute("cceEresourcesList", cceEresourcesList);

		List<Event> cceResearchList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_research);
		model.addAttribute("cceResearchList", cceResearchList);

		List<Event> cceSponsoredProjectsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_sponsored_projects);
		model.addAttribute("cceSponsoredProjectsList", cceSponsoredProjectsList);

		List<Event> ccePhdPursuingList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_phd_pursuing);
		model.addAttribute("ccePhdPursuingList", ccePhdPursuingList);

		List<Event> ccePhdAwardeesList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_phd_awardees);
		model.addAttribute("ccePhdAwardeesList", ccePhdAwardeesList);

		List<Event> cceInstructionalMaterialsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_instructional_materials);
		model.addAttribute("cceInstructionalMaterialsList", cceInstructionalMaterialsList);

		List<Event> ccePedagogicalActivitiesList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_pedagogical_activities);
		model.addAttribute("ccePedagogicalActivitiesList", ccePedagogicalActivitiesList);

		List<Event> ccePedagogyReviewFormList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_pedagogy_review_form);
		model.addAttribute("ccePedagogyReviewFormList", ccePedagogyReviewFormList);

		List<Event> ccecontentbeyondsyllabusList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_content_beyond_syllabus);
		model.addAttribute("ccecontentbeyondsyllabusList", ccecontentbeyondsyllabusList);

		List<Event> cceLabManualList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_lab_manual_form);
		model.addAttribute("cceLabManualList", cceLabManualList);

		List<Event> cceMouSignedList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_Mou_Signed);
		model.addAttribute("cceMouSignedList", cceMouSignedList);

		List<Event> cceAlumniAssociationList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_alumni_association);
		model.addAttribute("cceAlumniAssociationList", cceAlumniAssociationList);

		List<Event> cceProfessionalBodiesList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_professional_bodies);
		model.addAttribute("cceProfessionalBodiesList", cceProfessionalBodiesList);

		List<Event> cceProfessionalLinksList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_professional_links);
		model.addAttribute("cceProfessionalLinksList", cceProfessionalLinksList);

		List<Event> cceHigherEducationList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_higher_education);
		model.addAttribute("cceHigherEducationList", cceHigherEducationList);

		List<Event> cceClubActivitiesList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_club_activities);
		model.addAttribute("cceClubActivitiesList", cceClubActivitiesList);

		List<Event> cceClubExternalLinksList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_club_external_links);
		model.addAttribute("cceClubExternalLinksList", cceClubExternalLinksList);

		List<Event> cceInternshipList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_internship);
		model.addAttribute("cceInternshipList", cceInternshipList);

		List<Event> cceProjectsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_projects);
		model.addAttribute("cceProjectsList", cceProjectsList);

		List<Event> cceMiniProjectsList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_mini_projects);
		model.addAttribute("cceMiniProjectsList", cceMiniProjectsList);

		List<Event> cceSocialActivitiesList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_social_activities);
		model.addAttribute("cceSocialActivitiesList", cceSocialActivitiesList);

		List<Faculty> cceIndustrialVisitList = facultyService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_industrial_visit);
		model.addAttribute("cceIndustrialVisitList", cceIndustrialVisitList);

		List<Faculty> cceProjectExhibitionList = facultyService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_project_exhibition);
		model.addAttribute("cceProjectExhibitionList", cceProjectExhibitionList);

		List<Event> cceFdpList = eventService.getActiveByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_fdp);
		model.addAttribute("cceFdpList", cceFdpList);

		/*
		 * List<Event> cceTimeTableList =
		 * eventService.getActiveByPageAndFieldName(PageType.cce_page,
		 * FieldNameType.cce_page_timetable);
		 *
		 * if (cceTimeTableList.size() > 0) { model.addAttribute("cceTimeTable",
		 * cceTimeTableList.get(0)); }
		 */

		feedbackParentService.addDataToModel(model);
		feedbackAlumniService.addDataToModel(model);
		feedbackStudentService.addDataToModel(model);
		feedbackEmployerService.addDataToModel(model);
		model.addAttribute("responses", FeedbackResponse.values());

		return "cce_dept";
	}
	
	@RequestMapping(value = "/anti_ragging_committee", method = RequestMethod.GET)
	public String anti_ragging_committee(ModelMap model) {
		List<Event> reportsList = eventService.getActiveByPageAndFieldName(PageType.anti_ragging_committee_page,
				FieldNameType.anti_ragging_committee_events);
		model.addAttribute("reportsList", reportsList);
		return "anti_ragging_committee";
	}
	
	@RequestMapping(value = "/anti_ragging_squad", method = RequestMethod.GET)
	public String anti_ragging_squad(ModelMap model) {
		return "anti_ragging_squad";
	}
	
	@RequestMapping(value = "/internal_complaints", method = RequestMethod.GET)
	public String internal_complaints(ModelMap model) {
		return "internal_complaints";
	}
	
	@RequestMapping(value = "/sc_st_committee", method = RequestMethod.GET)
	public String sc_st_committee(ModelMap model) {
		return "sc_st_committee";
	}
	
	@RequestMapping(value = "/student_counsellor_details", method = RequestMethod.GET)
	public String student_counsellor_details(ModelMap model) {
		return "student_counsellor_details";
	}
	
	@RequestMapping(value = "/grivance_redressal_committee", method = RequestMethod.GET)
	public String grivance_redressal_committee(ModelMap model) {
		return "grivance_redressal_committee";
	}
	
	@RequestMapping(value = "/anti_sexual_harassment_committee", method = RequestMethod.GET)
	public String anti_sexual_harassment_committee(ModelMap model) {
		List<Event> reportsList = eventService.getActiveByPageAndFieldName(PageType.anti_sexual_harassment_committee_page,
				FieldNameType.anti_sexual_harassment_committee_events);
		model.addAttribute("reportsList", reportsList);
		return "anti_sexual_harassment_committee";
	}
	
	
	@RequestMapping(value = "/404", method = RequestMethod.GET)
	public String handleError404(ModelMap model) {
		return "404";
	}
	
	@RequestMapping(value = "/approvals", method = RequestMethod.GET)
	public String approvals(ModelMap model) {
		List<Event> approvalsMouSignedList = eventService.getByPageAndFieldName(PageType.approvals_page,
				FieldNameType.approvals_page_mou_signed);
		model.addAttribute("approvalsMouSignedList", approvalsMouSignedList);
		return "approvals";
	}



}
