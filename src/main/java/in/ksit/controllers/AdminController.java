/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import in.ksit.entity.Alumni;
import in.ksit.entity.ContactUs;
import in.ksit.entity.Course;
import in.ksit.entity.Criteria;
import in.ksit.entity.Data;
import in.ksit.entity.Event;
import in.ksit.entity.Faculty;
import in.ksit.entity.Feedback;
import in.ksit.entity.FeedbackAlumni;
import in.ksit.entity.FeedbackEmployer;
import in.ksit.entity.FeedbackParent;
import in.ksit.entity.FeedbackStudent;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.Grievance;
import in.ksit.entity.HostelEnrollment;
import in.ksit.entity.InstitutionalGovernance;
import in.ksit.entity.LabDataInternal;
import in.ksit.entity.NAAC;
import in.ksit.entity.NBA;
import in.ksit.entity.NBAPrevisit;
import in.ksit.entity.NBAPrevisitParentData;
import in.ksit.entity.OnlineAdmission;
import in.ksit.entity.PageType;
import in.ksit.entity.Placement;
import in.ksit.entity.Programme;
import in.ksit.entity.Programme.PROGRAMME_TYPE;
import in.ksit.entity.User;
import in.ksit.entity.UserPage;
import in.ksit.service.AlumniService;
import in.ksit.service.ContactUsService;
import in.ksit.service.CourseService;
import in.ksit.service.DataService;
import in.ksit.service.EventService;
import in.ksit.service.FacultyService;
import in.ksit.service.FeedbackAlumniService;
import in.ksit.service.FeedbackEmployerService;
import in.ksit.service.FeedbackParentService;
import in.ksit.service.FeedbackService;
import in.ksit.service.FeedbackStudentService;
import in.ksit.service.GrievanceService;
import in.ksit.service.HostelEnrollmentService;
import in.ksit.service.InstitutionalGovernanceService;
import in.ksit.service.LabDataService;
import in.ksit.service.NAACService;
import in.ksit.service.NBAPrevisitService;
import in.ksit.service.NBAService;
import in.ksit.service.OnlineAdmissionService;
import in.ksit.service.PlacementService;
import in.ksit.service.ProgrammeService;
import in.ksit.service.UserPageService;
import in.ksit.service.UserService;
import in.ksit.util.AppUtils;
import in.ksit.util.SessionUtil;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private DataService dataService;

	@Autowired
	private EventService eventService;

	@Autowired
	private FacultyService facultyService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserPageService userPageService;

	@Autowired
	private ContactUsService contactUsService;
	
	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private HostelEnrollmentService hostelEnrollmentService;

	@Autowired
	private AlumniService alumniService;

	@Autowired
	private PlacementService placementService;

	@Autowired
	private GrievanceService grievanceService;

	@Autowired
	private LabDataService labDataService;

	@Autowired
	private ProgrammeService programmeService;

	@Autowired
	private FeedbackParentService feedbackParentService;

	@Autowired
	private FeedbackStudentService feedbackStudentService;

	@Autowired
	private FeedbackAlumniService feedbackAlumniService;

	@Autowired
	private FeedbackEmployerService feedbackEmployerService;

	@Autowired
	private NBAService nbaService;

	@Autowired
	private NBAPrevisitService nbaPrevisitService;

	@Autowired
	private NAACService naacService;

	@Autowired
	private CourseService courseService;

	@Autowired
	private OnlineAdmissionService onlineAdmissionService;

	@Autowired
	private InstitutionalGovernanceService institutionalGovernanceService;

	@RequestMapping(value = "/no-access", method = RequestMethod.GET)
	public String noAccess(ModelMap model) {
		return "admin/no-access";
	}

	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String signinGet(@RequestParam(value = "error", required = false) String error, ModelMap model) {
		return "signin/signin";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signupGet() {
		return "signin/signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(ModelMap model, @RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "password", required = false) String password) {

		User user = userService.createAccount(username, password);
		if (user == null) {
			String errorMessage = "User already exists with " + username;
			model.addAttribute("errorMessage", errorMessage);
			return "signin/signup";
		} else {
			Authentication authentication = new UsernamePasswordAuthenticationToken(user, user.getPassword(),
					user.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(authentication);
			return "redirect:/admin/index";
		}
	}

	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String home(ModelMap model) {
		return "redirect:/admin/index";
	}

	@RequestMapping(value = { "/index" }, method = RequestMethod.GET)
	public String index(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.home_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.home_page);

		List<Event> homePageSliderImageList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_slider_images);
		model.addAttribute("homePageSliderImageList", homePageSliderImageList);
		model.addAttribute("homePageSliderImageFieldName", FieldNameType.home_page_slider_images);

		List<Event> newsletterPageNewsLetterList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.newsletter_page_newsletter);
		model.addAttribute("newsletterPageNewsLetterList", newsletterPageNewsLetterList);
		model.addAttribute("newsletterPageNewsLetterFieldName", FieldNameType.newsletter_page_newsletter);

		List<Event> souvenirPageSouvenirList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.souvenir_page_souvenir);
		model.addAttribute("souvenirPageSouvenirList", souvenirPageSouvenirList);
		model.addAttribute("souvenirPageSouvenirFieldName", FieldNameType.souvenir_page_souvenir);

		List<Data> scrollingTextTopList = dataService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.scrolling_text_top);
		model.addAttribute("scrollingTextTopList", scrollingTextTopList);
		model.addAttribute("scrollingTextTopFieldName", FieldNameType.scrolling_text_top);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.home_page_testimonials);

		List<Event> messageBoxList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_messagebox);
		model.addAttribute("messageBoxList", messageBoxList);
		model.addAttribute("messageBoxFieldName", FieldNameType.home_page_messagebox);

		List<Event> timetableList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_timetable);
		model.addAttribute("homepagetimetableList", timetableList);
		model.addAttribute("homepagetimetableFieldName", FieldNameType.home_page_timetable);

		List<Event> circularList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_circular);
		model.addAttribute("homepagecircularList", circularList);
		model.addAttribute("homepagecircularFieldName", FieldNameType.home_page_circular);

		List<Event> academicCalanderList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.academic_page_calander);
		model.addAttribute("academicCalanderList", academicCalanderList);
		model.addAttribute("academicCalanderFieldName", FieldNameType.academic_page_calander);

		List<Data> youtubeLinkList = dataService.getByPageAndFieldName(PageType.home_page, FieldNameType.youtube_link);
		model.addAttribute("youtubeLinkList", youtubeLinkList);
		model.addAttribute("youtubeLinkFieldName", FieldNameType.youtube_link);

		List<Event> achieversList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_achievers);
		model.addAttribute("achieversList", achieversList);
		model.addAttribute("achieversHomeFieldName", FieldNameType.home_page_achievers);

		List<Event> homePageLatestEventsList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_latest_events);
		model.addAttribute("homePageLatestEventsList", homePageLatestEventsList);
		model.addAttribute("latestEventsHomeFieldName", FieldNameType.home_page_latest_events);

		List<Event> homePageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_upcoming_events);
		model.addAttribute("homePageUpcomingEventsList", homePageUpcomingEventsList);
		model.addAttribute("upcomingEventsHomeFieldName", FieldNameType.home_page_upcoming_events);

		List<Event> hostelFeesList = eventService.getByPageAndFieldName(PageType.home_page, FieldNameType.hostel_fees);
		model.addAttribute("hostelFeesList", hostelFeesList);
		model.addAttribute("hostelFeesFieldName", FieldNameType.hostel_fees);

		return "admin/index";
	}

	@RequestMapping(value = { "/index2" }, method = RequestMethod.GET)
	public String index2(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.home_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.home_page);

		List<Event> homePageSliderImageList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_slider_images);
		model.addAttribute("homePageSliderImageList", homePageSliderImageList);
		model.addAttribute("homePageSliderImageFieldName", FieldNameType.home_page_slider_images);

		List<Event> newsletterPageNewsLetterList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.newsletter_page_newsletter);
		model.addAttribute("newsletterPageNewsLetterList", newsletterPageNewsLetterList);
		model.addAttribute("newsletterPageNewsLetterFieldName", FieldNameType.newsletter_page_newsletter);

		List<Event> souvenirPageSouvenirList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.souvenir_page_souvenir);
		model.addAttribute("souvenirPageSouvenirList", souvenirPageSouvenirList);
		model.addAttribute("souvenirPageSouvenirFieldName", FieldNameType.souvenir_page_souvenir);

		List<Data> scrollingTextTopList = dataService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.scrolling_text_top);
		model.addAttribute("scrollingTextTopList", scrollingTextTopList);
		model.addAttribute("scrollingTextTopFieldName", FieldNameType.scrolling_text_top);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.home_page_testimonials);

		List<Event> messageBoxList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_messagebox);
		model.addAttribute("messageBoxList", messageBoxList);
		model.addAttribute("messageBoxFieldName", FieldNameType.home_page_messagebox);

		List<Event> timetableList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_timetable);
		model.addAttribute("homepagetimetableList", timetableList);
		model.addAttribute("homepagetimetableFieldName", FieldNameType.home_page_timetable);

		List<Event> circularList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_circular);
		model.addAttribute("homepagecircularList", circularList);
		model.addAttribute("homepagecircularFieldName", FieldNameType.home_page_circular);

		List<Event> academicCalanderList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.academic_page_calander);
		model.addAttribute("academicCalanderList", academicCalanderList);
		model.addAttribute("academicCalanderFieldName", FieldNameType.academic_page_calander);

		List<Data> youtubeLinkList = dataService.getByPageAndFieldName(PageType.home_page, FieldNameType.youtube_link);
		model.addAttribute("youtubeLinkList", youtubeLinkList);
		model.addAttribute("youtubeLinkFieldName", FieldNameType.youtube_link);

		List<Event> achieversList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_achievers);
		model.addAttribute("achieversList", achieversList);
		model.addAttribute("achieversHomeFieldName", FieldNameType.home_page_achievers);

		List<Event> homePageLatestEventsList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_latest_events);
		model.addAttribute("homePageLatestEventsList", homePageLatestEventsList);
		model.addAttribute("latestEventsHomeFieldName", FieldNameType.home_page_latest_events);

		List<Event> homePageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.home_page,
				FieldNameType.home_page_upcoming_events);
		model.addAttribute("homePageUpcomingEventsList", homePageUpcomingEventsList);
		model.addAttribute("upcomingEventsHomeFieldName", FieldNameType.home_page_upcoming_events);

		List<Event> hostelFeesList = eventService.getByPageAndFieldName(PageType.home_page, FieldNameType.hostel_fees);
		model.addAttribute("hostelFeesList", hostelFeesList);
		model.addAttribute("hostelFeesFieldName", FieldNameType.hostel_fees);

		return "admin/index2";
	}

	@RequestMapping(value = "/UG", method = RequestMethod.GET)
	public String UG(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.ug_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<Programme> programmeList = programmeService.getProgrammesByType(PROGRAMME_TYPE.UG);
		model.addAttribute("programmeList", programmeList);
		model.addAttribute("programmeDataType", PROGRAMME_TYPE.UG);
		return "admin/UG";
	}

	@RequestMapping(value = "/PG", method = RequestMethod.GET)
	public String PG(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.pg_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<Programme> programmeList = programmeService.getProgrammesByType(PROGRAMME_TYPE.PG);
		model.addAttribute("programmeList", programmeList);
		model.addAttribute("programmeDataType", PROGRAMME_TYPE.PG);
		return "admin/PG";
	}

	@RequestMapping(value = "/aiml_dept", method = RequestMethod.GET)
	public String aiml_dept(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.aiml_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.aiml_page);

		List<Event> aimlPageSliderImageList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_slider_images);
		model.addAttribute("aimlPageSliderImageList", aimlPageSliderImageList);
		model.addAttribute("aimlPageSliderImageFieldName", FieldNameType.aiml_page_slider_images);

		List<Event> aimlPageLatestEventsList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_latest_events);
		model.addAttribute("aimlPageLatestEventsList", aimlPageLatestEventsList);
		model.addAttribute("aimlLatestEventsFieldName", FieldNameType.aiml_page_latest_events);

		List<Event> aimlPageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_upcoming_events);
		model.addAttribute("aimlPageUpcomingEventsList", aimlPageUpcomingEventsList);
		model.addAttribute("aimlUpcomingEventsFieldName", FieldNameType.aiml_page_upcoming_events);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> aimlEventList = eventService.getByPageAndFieldName(PageType.aiml_page, FieldNameType.aiml_events);
		model.addAttribute("aimlEventList", aimlEventList);
		model.addAttribute("aimlEventFieldName", FieldNameType.aiml_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.aiml_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.aiml_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.aiml_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.aiml_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.aiml_page_gallery);

		List<Event> aimlDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_department_achiever_list);
		model.addAttribute("aimlDepartmentAchieversList", aimlDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.aiml_department_achiever_list);

		List<Event> aimlFcdStudentsList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_fcd_students);
		model.addAttribute("aimlFcdStudentsList", aimlFcdStudentsList);
		model.addAttribute("aimlFcdStudentsFieldName", FieldNameType.aiml_page_fcd_students);

		List<Event> aimlcontentbeyondsyllabusList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_content_beyond_syllabus);
		model.addAttribute("aimlcontentbeyondsyllabusList", aimlcontentbeyondsyllabusList);
		model.addAttribute("aimlcontentbeyondsyllabusFieldName", FieldNameType.aiml_page_content_beyond_syllabus);

		List<Event> aimlClassTimeTableList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_class_time_table);
		model.addAttribute("aimlClassTimeTableList", aimlClassTimeTableList);
		model.addAttribute("aimlClassTimeTableFieldName", FieldNameType.aiml_page_class_time_table);

		List<Event> aimlTestTimeTableList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_test_time_table);
		model.addAttribute("aimlTestTimeTableList", aimlTestTimeTableList);
		model.addAttribute("aimlTestTimeTableFieldName", FieldNameType.aiml_page_test_time_table);

		List<Event> aimlCalanderList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_calander);
		model.addAttribute("aimlCalanderList", aimlCalanderList);
		model.addAttribute("aimlCalanderFieldName", FieldNameType.aiml_page_calander);

		List<Event> aimlNewsLetterList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_newsletter);
		model.addAttribute("aimlNewsLetterList", aimlNewsLetterList);
		model.addAttribute("aimlNewsLetterFieldName", FieldNameType.aiml_page_newsletter);

		List<Event> aimlUgSyllabusList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_ug_syllabus);
		model.addAttribute("aimlUgSyllabusList", aimlUgSyllabusList);
		model.addAttribute("aimlUgSyllabusFieldName", FieldNameType.aiml_page_ug_syllabus);

		List<Event> aimlLibraryBooksList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_library_books_collection);
		model.addAttribute("aimlLibraryBooksList", aimlLibraryBooksList);
		model.addAttribute("aimlLibraryBooksFieldName", FieldNameType.aiml_library_books_collection);

		List<Event> aimlPlacementList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_placement);
		model.addAttribute("aimlPlacementList", aimlPlacementList);
		model.addAttribute("aimlPlacementFieldName", FieldNameType.aiml_page_placement);

		List<Faculty> aimlLabList = facultyService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_labs);
		model.addAttribute("aimlLabList", aimlLabList);
		model.addAttribute("aimlLabFieldName", FieldNameType.aiml_page_labs);

		List<Event> aimlEresourcesList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_eresources);
		model.addAttribute("aimlEresourcesList", aimlEresourcesList);
		model.addAttribute("aimlEresourcesFieldName", FieldNameType.aiml_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.aiml_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.aiml_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.aiml_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.aiml_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> aimlResearchList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_research);
		model.addAttribute("aimlResearchList", aimlResearchList);
		model.addAttribute("aimlResearchFieldName", FieldNameType.aiml_page_research);

		List<Event> aimlSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_sponsored_projects);
		model.addAttribute("aimlSponsoredProjectsList", aimlSponsoredProjectsList);
		model.addAttribute("aimlSponsoredProjectsFieldName", FieldNameType.aiml_page_sponsored_projects);

		List<Event> aimlPhdPursuingList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_phd_pursuing);
		model.addAttribute("aimlPhdPursuingList", aimlPhdPursuingList);
		model.addAttribute("aimlPhdPursuingFieldName", FieldNameType.aiml_page_phd_pursuing);

		List<Event> aimlPhdAwardeesList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_phd_awardees);
		model.addAttribute("aimlPhdAwardeesList", aimlPhdAwardeesList);
		model.addAttribute("aimlPhdAwardeesFieldName", FieldNameType.aiml_page_phd_awardees);

		List<Event> aimlInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_instructional_materials);
		model.addAttribute("aimlInstructionalMaterialsList", aimlInstructionalMaterialsList);
		model.addAttribute("aimlInstructionalMaterialsFieldName", FieldNameType.aiml_instructional_materials);

		List<Event> aimlPedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_pedagogical_activities);
		model.addAttribute("aimlPedagogicalActivitiesList", aimlPedagogicalActivitiesList);
		model.addAttribute("aimlPedagogicalActivitiesFieldName", FieldNameType.aiml_pedagogical_activities);

		List<Event> aimlPedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_pedagogy_review_form);
		model.addAttribute("aimlPedagogyReviewFormList", aimlPedagogyReviewFormList);
		model.addAttribute("aimlPedagogyReviewFormFieldName", FieldNameType.aiml_pedagogy_review_form);

		List<Event> aimlLabManualList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_lab_manual_form);
		model.addAttribute("aimlLabManualList", aimlLabManualList);
		model.addAttribute("aimlLabManualFieldName", FieldNameType.aiml_lab_manual_form);

		List<Event> aimlMouSignedList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_Mou_Signed);
		model.addAttribute("aimlMouSignedList", aimlMouSignedList);
		model.addAttribute("aimlMouSignedFieldName", FieldNameType.aiml_page_Mou_Signed);

		List<Event> aimlAlumniAssociationList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_alumni_association);
		model.addAttribute("aimlAlumniAssociationList", aimlAlumniAssociationList);
		model.addAttribute("aimlAlumniAssociationFieldName", FieldNameType.aiml_page_alumni_association);

		List<Event> aimlProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_professional_bodies);
		model.addAttribute("aimlProfessionalBodiesList", aimlProfessionalBodiesList);
		model.addAttribute("aimlProfessionalBodiesFieldName", FieldNameType.aiml_page_professional_bodies);

		List<Event> aimlProfessionalLinksList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_professional_links);
		model.addAttribute("aimlProfessionalLinksList", aimlProfessionalLinksList);
		model.addAttribute("aimlProfessionalLinksFieldName", FieldNameType.aiml_page_professional_links);

		List<Event> aimlHigherEducationList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_higher_education);
		model.addAttribute("aimlHigherEducationList", aimlHigherEducationList);
		model.addAttribute("aimlHigherEducationFieldName", FieldNameType.aiml_page_higher_education);

		List<Event> aimlClubActivitiesList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_club_activities);
		model.addAttribute("aimlClubActivitiesList", aimlClubActivitiesList);
		model.addAttribute("aimlClubActivitiesFieldName", FieldNameType.aiml_page_club_activities);

		List<Event> aimlClubExternalLinksList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_club_external_links);
		model.addAttribute("aimlClubExternalLinksList", aimlClubExternalLinksList);
		model.addAttribute("aimlClubExternalLinksFieldName", FieldNameType.aiml_page_club_external_links);

		List<Event> aimlInternshipList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_internship);
		model.addAttribute("aimlInternshipList", aimlInternshipList);
		model.addAttribute("aimlInternshipFieldName", FieldNameType.aiml_page_internship);

		List<Event> aimlProjectsList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_projects);
		model.addAttribute("aimlProjectsList", aimlProjectsList);
		model.addAttribute("aimlProjectsFieldName", FieldNameType.aiml_page_projects);

		List<Event> aimlMiniProjectsList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_mini_projects);
		model.addAttribute("aimlMiniProjectsList", aimlMiniProjectsList);
		model.addAttribute("aimlMiniProjectsFieldName", FieldNameType.aiml_page_mini_projects);

		List<Event> aimlSocialActivitiesList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_social_activities);
		model.addAttribute("aimlSocialActivitiesList", aimlSocialActivitiesList);
		model.addAttribute("aimlSocialActivitiesFieldName", FieldNameType.aiml_page_social_activities);

		List<Faculty> aimlIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_industrial_visit);
		model.addAttribute("aimlIndustrialVisitList", aimlIndustrialVisitList);
		model.addAttribute("aimlIndustrialVisitFieldName", FieldNameType.aiml_page_industrial_visit);

		List<Faculty> aimlProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.aiml_page_project_exhibition);
		model.addAttribute("aimlProjectExhibitionList", aimlProjectExhibitionList);
		model.addAttribute("aimlProjectExhibitionFieldName", FieldNameType.aiml_page_project_exhibition);

		List<Event> aimlFdpList = eventService.getByPageAndFieldName(PageType.aiml_page, FieldNameType.aiml_page_fdp);
		model.addAttribute("aimlFdpList", aimlFdpList);
		model.addAttribute("aimlFdpFieldName", FieldNameType.aiml_page_fdp);

		return "admin/aiml_dept";
	}

	@RequestMapping(value = "/csd_dept", method = RequestMethod.GET)
	public String csd_dept(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.csd_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.csd_page);

		List<Event> csdPageSliderImageList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_slider_images);
		model.addAttribute("csdPageSliderImageList", csdPageSliderImageList);
		model.addAttribute("csdPageSliderImageFieldName", FieldNameType.csd_page_slider_images);

		List<Event> csdPageLatestEventsList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_latest_events);
		model.addAttribute("csdPageLatestEventsList", csdPageLatestEventsList);
		model.addAttribute("csdLatestEventsFieldName", FieldNameType.csd_page_latest_events);

		List<Event> csdPageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_upcoming_events);
		model.addAttribute("csdPageUpcomingEventsList", csdPageUpcomingEventsList);
		model.addAttribute("csdUpcomingEventsFieldName", FieldNameType.csd_page_upcoming_events);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> csdEventList = eventService.getByPageAndFieldName(PageType.csd_page, FieldNameType.csd_events);
		model.addAttribute("csdEventList", csdEventList);
		model.addAttribute("csdEventFieldName", FieldNameType.csd_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.csd_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.csd_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.csd_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.csd_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.csd_page, FieldNameType.csd_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.csd_page_gallery);

		List<Event> csdDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.aiml_page,
				FieldNameType.csd_department_achiever_list);
		model.addAttribute("csdDepartmentAchieversList", csdDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.csd_department_achiever_list);

		List<Event> csdFcdStudentsList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_fcd_students);
		model.addAttribute("csdFcdStudentsList", csdFcdStudentsList);
		model.addAttribute("csdFcdStudentsFieldName", FieldNameType.csd_page_fcd_students);

		List<Event> csdClassTimeTableList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_class_time_table);
		model.addAttribute("csdClassTimeTableList", csdClassTimeTableList);
		model.addAttribute("csdClassTimeTableFieldName", FieldNameType.csd_page_class_time_table);

		List<Event> csdTestTimeTableList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_test_time_table);
		model.addAttribute("csdTestTimeTableList", csdTestTimeTableList);
		model.addAttribute("csdTestTimeTableFieldName", FieldNameType.csd_page_test_time_table);

		List<Event> csdCalanderList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_calander);
		model.addAttribute("csdCalanderList", csdCalanderList);
		model.addAttribute("csdCalanderFieldName", FieldNameType.csd_page_calander);

		List<Event> csdcontentbeyondsyllabusList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_content_beyond_syllabus);
		model.addAttribute("csdcontentbeyondsyllabusList", csdcontentbeyondsyllabusList);
		model.addAttribute("csdcontentbeyondsyllabusFieldName", FieldNameType.csd_page_content_beyond_syllabus);

		List<Event> csdNewsLetterList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_newsletter);
		model.addAttribute("csdNewsLetterList", csdNewsLetterList);
		model.addAttribute("csdNewsLetterFieldName", FieldNameType.csd_page_newsletter);

		List<Event> csdUgSyllabusList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_ug_syllabus);
		model.addAttribute("csdUgSyllabusList", csdUgSyllabusList);
		model.addAttribute("csdUgSyllabusFieldName", FieldNameType.csd_page_ug_syllabus);

		List<Event> csdLibraryBooksList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_library_books_collection);
		model.addAttribute("csdLibraryBooksList", csdLibraryBooksList);
		model.addAttribute("csdLibraryBooksFieldName", FieldNameType.csd_library_books_collection);

		List<Event> csdPlacementList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_placement);
		model.addAttribute("csdPlacementList", csdPlacementList);
		model.addAttribute("csdPlacementFieldName", FieldNameType.csd_page_placement);

		List<Faculty> csdLabList = facultyService.getByPageAndFieldName(PageType.csd_page, FieldNameType.csd_page_labs);
		model.addAttribute("csdLabList", csdLabList);
		model.addAttribute("csdLabFieldName", FieldNameType.csd_page_labs);

		List<Event> csdEresourcesList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_eresources);
		model.addAttribute("csdEresourcesList", csdEresourcesList);
		model.addAttribute("csdEresourcesFieldName", FieldNameType.csd_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.csd_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.csd_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.csd_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.csd_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> csdResearchList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_research);
		model.addAttribute("csdResearchList", csdResearchList);
		model.addAttribute("csdResearchFieldName", FieldNameType.csd_page_research);

		List<Event> csdSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_sponsored_projects);
		model.addAttribute("csdSponsoredProjectsList", csdSponsoredProjectsList);
		model.addAttribute("csdSponsoredProjectsFieldName", FieldNameType.csd_page_sponsored_projects);

		List<Event> csdPhdPursuingList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_phd_pursuing);
		model.addAttribute("csdPhdPursuingList", csdPhdPursuingList);
		model.addAttribute("csdPhdPursuingFieldName", FieldNameType.csd_page_phd_pursuing);

		List<Event> csdPhdAwardeesList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_phd_awardees);
		model.addAttribute("csdPhdAwardeesList", csdPhdAwardeesList);
		model.addAttribute("csdPhdAwardeesFieldName", FieldNameType.csd_page_phd_awardees);

		List<Event> csdInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_instructional_materials);
		model.addAttribute("csdInstructionalMaterialsList", csdInstructionalMaterialsList);
		model.addAttribute("csdInstructionalMaterialsFieldName", FieldNameType.csd_instructional_materials);

		List<Event> csdPedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_pedagogical_activities);
		model.addAttribute("csdPedagogicalActivitiesList", csdPedagogicalActivitiesList);
		model.addAttribute("csdPedagogicalActivitiesFieldName", FieldNameType.csd_pedagogical_activities);

		List<Event> csdPedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_pedagogy_review_form);
		model.addAttribute("csdPedagogyReviewFormList", csdPedagogyReviewFormList);
		model.addAttribute("csdPedagogyReviewFormFieldName", FieldNameType.csd_pedagogy_review_form);

		List<Event> csdLabManualList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_lab_manual_form);
		model.addAttribute("csdLabManualList", csdLabManualList);
		model.addAttribute("csdlLabManualFieldName", FieldNameType.csd_lab_manual_form);

		List<Event> csdMouSignedList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_Mou_Signed);
		model.addAttribute("csdMouSignedList", csdMouSignedList);
		model.addAttribute("csdMouSignedFieldName", FieldNameType.csd_page_Mou_Signed);

		List<Event> csdAlumniAssociationList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_alumni_association);
		model.addAttribute("csdAlumniAssociationList", csdAlumniAssociationList);
		model.addAttribute("csdAlumniAssociationFieldName", FieldNameType.csd_page_alumni_association);

		List<Event> csdProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_professional_bodies);
		model.addAttribute("csdProfessionalBodiesList", csdProfessionalBodiesList);
		model.addAttribute("csdProfessionalBodiesFieldName", FieldNameType.csd_page_professional_bodies);

		List<Event> csdProfessionalLinksList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_professional_links);
		model.addAttribute("csdProfessionalLinksList", csdProfessionalLinksList);
		model.addAttribute("csdProfessionalLinksFieldName", FieldNameType.csd_page_professional_links);

		List<Event> csdHigherEducationList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_higher_education);
		model.addAttribute("csdHigherEducationList", csdHigherEducationList);
		model.addAttribute("csdHigherEducationFieldName", FieldNameType.csd_page_higher_education);

		List<Event> csdClubActivitiesList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_club_activities);
		model.addAttribute("csdClubActivitiesList", csdClubActivitiesList);
		model.addAttribute("csdClubActivitiesFieldName", FieldNameType.csd_page_club_activities);

		List<Event> csdClubExternalLinksList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_club_external_links);
		model.addAttribute("csdClubExternalLinksList", csdClubExternalLinksList);
		model.addAttribute("csdClubExternalLinksFieldName", FieldNameType.csd_page_club_external_links);

		List<Event> csdInternshipList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_internship);
		model.addAttribute("csdInternshipList", csdInternshipList);
		model.addAttribute("csdInternshipFieldName", FieldNameType.csd_page_internship);

		List<Event> csdProjectsList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_projects);
		model.addAttribute("csdProjectsList", csdProjectsList);
		model.addAttribute("csdProjectsFieldName", FieldNameType.csd_page_projects);

		List<Event> csdMiniProjectsList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_mini_projects);
		model.addAttribute("csdMiniProjectsList", csdMiniProjectsList);
		model.addAttribute("csdMiniProjectsFieldName", FieldNameType.csd_page_mini_projects);

		List<Event> csdSocialActivitiesList = eventService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_social_activities);
		model.addAttribute("csdSocialActivitiesList", csdSocialActivitiesList);
		model.addAttribute("csdSocialActivitiesFieldName", FieldNameType.csd_page_social_activities);

		List<Faculty> csdIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.csd_page, FieldNameType.csd_page_industrial_visit);
		model.addAttribute("csdIndustrialVisitList", csdIndustrialVisitList);
		model.addAttribute("csdIndustrialVisitFieldName", FieldNameType.csd_page_industrial_visit);
		
		List<Event> csdOtherDetailsList = eventService.getActiveByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_other_details);
		model.addAttribute("csdOtherDetailsList", csdOtherDetailsList);
        model.addAttribute("csdOtherDetailsFieldName", FieldNameType.csd_page_other_details);

		List<Faculty> csdProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.csd_page,
				FieldNameType.csd_page_project_exhibition);
		model.addAttribute("csdProjectExhibitionList", csdProjectExhibitionList);
		model.addAttribute("csdProjectExhibitionFieldName", FieldNameType.csd_page_project_exhibition);

		List<Event> csdFdpList = eventService.getByPageAndFieldName(PageType.csd_page, FieldNameType.csd_page_fdp);
		model.addAttribute("csdFdpList", csdFdpList);
		model.addAttribute("csdFdpFieldName", FieldNameType.csd_page_fdp);

		return "admin/csd_dept";
	}

	@RequestMapping(value = "/cse_dept", method = RequestMethod.GET)
	public String cse_dept(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.cse_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.cse_page);

		List<Event> csePageSliderImageList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_slider_images);
		model.addAttribute("csePageSliderImageList", csePageSliderImageList);
		model.addAttribute("csePageSliderImageFieldName", FieldNameType.cse_page_slider_images);

		List<Event> csePageLatestEventsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_latest_events);
		model.addAttribute("csePageLatestEventsList", csePageLatestEventsList);
		model.addAttribute("cseLatestEventsFieldName", FieldNameType.cse_page_latest_events);

		List<Event> csePageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_upcoming_events);
		model.addAttribute("csePageUpcomingEventsList", csePageUpcomingEventsList);
		model.addAttribute("cseUpcomingEventsFieldName", FieldNameType.cse_page_upcoming_events);

		/*
		 * List<Data> upComingEventsDataList =
		 * dataService.getByPageAndFieldName(PageType.cse_page,
		 * FieldNameType.upcoming_events); model.addAttribute("upComingEventsDataList",
		 * upComingEventsDataList); model.addAttribute("upComingEventsFieldName",
		 * FieldNameType.upcoming_events);
		 *
		 * List<Data> latestEventsDataList =
		 * dataService.getByPageAndFieldName(PageType.cse_page,
		 * FieldNameType.latest_events); model.addAttribute("latestEventsDataList",
		 * latestEventsDataList); model.addAttribute("latestEventsFieldName",
		 * FieldNameType.latest_events);
		 */

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> cseEventList = eventService.getByPageAndFieldName(PageType.cse_page, FieldNameType.cse_events);
		model.addAttribute("cseEventList", cseEventList);
		model.addAttribute("cseEventFieldName", FieldNameType.cse_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.cse_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.cse_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.cse_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.cse_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.cse_page, FieldNameType.cse_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.cse_page_gallery);

		/*
		 * List<Event> timetableList =
		 * eventService.getByPageAndFieldName(PageType.cse_page,
		 * FieldNameType.cse_page_timetable); model.addAttribute("csetimetableList",
		 * timetableList); model.addAttribute("csetimetableFieldName",
		 * FieldNameType.cse_page_timetable);
		 */

		List<Event> cseDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_department_achiever_list);
		model.addAttribute("cseDepartmentAchieversList", cseDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.cse_department_achiever_list);

		List<Event> cseFcdStudentsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_fcd_students);
		model.addAttribute("cseFcdStudentsList", cseFcdStudentsList);
		model.addAttribute("cseFcdStudentsFieldName", FieldNameType.cse_page_fcd_students);

		List<Event> cseClassTimeTableList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_class_time_table);
		model.addAttribute("cseClassTimeTableList", cseClassTimeTableList);
		model.addAttribute("cseClassTimeTableFieldName", FieldNameType.cse_page_class_time_table);

		List<Event> cseTestTimeTableList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_test_time_table);
		model.addAttribute("cseTestTimeTableList", cseTestTimeTableList);
		model.addAttribute("cseTestTimeTableFieldName", FieldNameType.cse_page_test_time_table);

		List<Event> cseCalanderList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_calander);
		model.addAttribute("cseCalanderList", cseCalanderList);
		model.addAttribute("cseCalanderFieldName", FieldNameType.cse_page_calander);

		List<Event> cseNewsLetterList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_newsletter);
		model.addAttribute("cseNewsLetterList", cseNewsLetterList);
		model.addAttribute("cseNewsLetterFieldName", FieldNameType.cse_page_newsletter);

		List<Event> cseUgSyllabusList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_ug_syllabus);
		model.addAttribute("cseUgSyllabusList", cseUgSyllabusList);
		model.addAttribute("cseUgSyllabusFieldName", FieldNameType.cse_page_ug_syllabus);

		List<Event> csePgSyllabusList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_pg_syllabus);
		model.addAttribute("csePgSyllabusList", csePgSyllabusList);
		model.addAttribute("csePgSyllabusFieldName", FieldNameType.cse_page_pg_syllabus);

		List<Event> cseLibraryBooksList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_library_books_collection);
		model.addAttribute("cseLibraryBooksList", cseLibraryBooksList);
		model.addAttribute("cseLibraryBooksFieldName", FieldNameType.cse_library_books_collection);

		List<Event> csePlacementList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_placement);
		model.addAttribute("csePlacementList", csePlacementList);
		model.addAttribute("csePlacementFieldName", FieldNameType.cse_page_placement);

		List<Faculty> cseLabList = facultyService.getByPageAndFieldName(PageType.cse_page, FieldNameType.cse_page_labs);
		model.addAttribute("cseLabList", cseLabList);
		model.addAttribute("cseLabFieldName", FieldNameType.cse_page_labs);

		List<LabDataInternal> labDataList = labDataService.getLabDataByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_lab, true);
		model.addAttribute("labDataList", labDataList);

		List<Event> cseEresourcesList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_eresources);
		model.addAttribute("cseEresourcesList", cseEresourcesList);
		model.addAttribute("cseEresourcesFieldName", FieldNameType.cse_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.cse_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.cse_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.cse_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.cse_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> cseResearchList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_research);
		model.addAttribute("cseResearchList", cseResearchList);
		model.addAttribute("cseResearchFieldName", FieldNameType.cse_page_research);

		List<Event> cseSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_sponsored_projects);
		model.addAttribute("cseSponsoredProjectsList", cseSponsoredProjectsList);
		model.addAttribute("cseSponsoredProjectsFieldName", FieldNameType.cse_page_sponsored_projects);

		List<Event> csePhdPursuingList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_phd_pursuing);
		model.addAttribute("csePhdPursuingList", csePhdPursuingList);
		model.addAttribute("csePhdPursuingFieldName", FieldNameType.cse_page_phd_pursuing);

		List<Event> csePhdAwardeesList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_phd_awardees);
		model.addAttribute("csePhdAwardeesList", csePhdAwardeesList);
		model.addAttribute("csePhdAwardeesFieldName", FieldNameType.cse_page_phd_awardees);

		List<Event> cseInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_instructional_materials);
		model.addAttribute("cseInstructionalMaterialsList", cseInstructionalMaterialsList);
		model.addAttribute("cseInstructionalMaterialsFieldName", FieldNameType.cse_instructional_materials);

		List<Event> csePedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_pedagogical_activities);
		model.addAttribute("csePedagogicalActivitiesList", csePedagogicalActivitiesList);
		model.addAttribute("csePedagogicalActivitiesFieldName", FieldNameType.cse_pedagogical_activities);

		List<Event> csePedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_pedagogy_review_form);
		model.addAttribute("csePedagogyReviewFormList", csePedagogyReviewFormList);
		model.addAttribute("csePedagogyReviewFormFieldName", FieldNameType.cse_pedagogy_review_form);

		List<Event> csecontentbeyondsyllabusList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_content_beyond_syllabus);
		model.addAttribute("csecontentbeyondsyllabusList", csecontentbeyondsyllabusList);
		model.addAttribute("csecontentbeyondsyllabusFieldName", FieldNameType.cse_page_content_beyond_syllabus);

		List<Event> cseLabManualList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_lab_manual_form);
		model.addAttribute("cseLabManualList", cseLabManualList);
		model.addAttribute("cseLabManualFieldName", FieldNameType.cse_lab_manual_form);

		List<Event> cseMouSignedList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_Mou_Signed);
		model.addAttribute("cseMouSignedList", cseMouSignedList);
		model.addAttribute("cseMouSignedFieldName", FieldNameType.cse_page_Mou_Signed);

		List<Event> cseAlumniAssociationList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_alumni_association);
		model.addAttribute("cseAlumniAssociationList", cseAlumniAssociationList);
		model.addAttribute("cseAlumniAssociationFieldName", FieldNameType.cse_page_alumni_association);

		List<Event> cseProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_professional_bodies);
		model.addAttribute("cseProfessionalBodiesList", cseProfessionalBodiesList);
		model.addAttribute("cseProfessionalBodiesFieldName", FieldNameType.cse_page_professional_bodies);

		List<Event> cseProfessionalLinksList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_professional_links);
		model.addAttribute("cseProfessionalLinksList", cseProfessionalLinksList);
		model.addAttribute("cseProfessionalLinksFieldName", FieldNameType.cse_page_professional_links);

		List<Event> cseHigherEducationList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_higher_education);
		model.addAttribute("cseHigherEducationList", cseHigherEducationList);
		model.addAttribute("cseHigherEducationFieldName", FieldNameType.cse_page_higher_education);

		List<Event> cseClubActivitiesList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_club_activities);
		model.addAttribute("cseClubActivitiesList", cseClubActivitiesList);
		model.addAttribute("cseClubActivitiesFieldName", FieldNameType.cse_page_club_activities);

		List<Event> cseClubExternalLinksList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_club_external_links);
		model.addAttribute("cseClubExternalLinksList", cseClubExternalLinksList);
		model.addAttribute("cseClubExternalLinksFieldName", FieldNameType.cse_page_club_external_links);

		List<Event> cseInternshipList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_internship);
		model.addAttribute("cseInternshipList", cseInternshipList);
		model.addAttribute("cseInternshipFieldName", FieldNameType.cse_page_internship);

		List<Event> cseProjectsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_projects);
		model.addAttribute("cseProjectsList", cseProjectsList);
		model.addAttribute("cseProjectsFieldName", FieldNameType.cse_page_projects);

		List<Event> cseMiniProjectsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_mini_projects);
		model.addAttribute("cseMiniProjectsList", cseMiniProjectsList);
		model.addAttribute("cseMiniProjectsFieldName", FieldNameType.cse_page_mini_projects);

		List<Event> cseSocialActivitiesList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_social_activities);
		model.addAttribute("cseSocialActivitiesList", cseSocialActivitiesList);
		model.addAttribute("cseSocialActivitiesFieldName", FieldNameType.cse_page_social_activities);

		List<Faculty> cseIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_industrial_visit);
		model.addAttribute("cseIndustrialVisitList", cseIndustrialVisitList);
		model.addAttribute("cseIndustrialVisitFieldName", FieldNameType.cse_page_industrial_visit);

		List<Faculty> cseProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_project_exhibition);
		model.addAttribute("cseProjectExhibitionList", cseProjectExhibitionList);
		model.addAttribute("cseProjectExhibitionFieldName", FieldNameType.cse_page_project_exhibition);

		List<Event> cseOtherDetailsList = eventService.getByPageAndFieldName(PageType.cse_page,
				FieldNameType.cse_page_other_details);
		model.addAttribute("cseOtherDetailsList", cseOtherDetailsList);
		model.addAttribute("cseOtherDetailsFieldName", FieldNameType.cse_page_other_details);

		List<Event> cseFdpList = eventService.getByPageAndFieldName(PageType.cse_page, FieldNameType.cse_page_fdp);
		model.addAttribute("cseFdpList", cseFdpList);
		model.addAttribute("cseFdpFieldName", FieldNameType.cse_page_fdp);

		return "admin/cse_dept";
	}

	@RequestMapping(value = "/ece_dept", method = RequestMethod.GET)
	public String ece_dept(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.ece_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.ece_page);

		List<Event> ecePageSliderImageList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_slider_images);
		model.addAttribute("ecePageSliderImageList", ecePageSliderImageList);
		model.addAttribute("ecePageSliderImageFieldName", FieldNameType.ece_page_slider_images);

		List<Event> ecePageLatestEventsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_latest_events);
		model.addAttribute("ecePageLatestEventsList", ecePageLatestEventsList);
		model.addAttribute("eceLatestEventsFieldName", FieldNameType.ece_page_latest_events);

		List<Event> ecePageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_upcoming_events);
		model.addAttribute("ecePageUpcomingEventsList", ecePageUpcomingEventsList);
		model.addAttribute("eceUpcomingEventsFieldName", FieldNameType.ece_page_upcoming_events);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> eceEventList = eventService.getByPageAndFieldName(PageType.ece_page, FieldNameType.ece_events);
		model.addAttribute("eceEventList", eceEventList);
		model.addAttribute("eceEventFieldName", FieldNameType.ece_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.ece_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.ece_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.ece_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.ece_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.ece_page, FieldNameType.ece_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.ece_page_gallery);

		List<Event> eceDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_department_achiever_list);
		model.addAttribute("eceDepartmentAchieversList", eceDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.ece_department_achiever_list);

		List<Event> eceFcdStudentsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_fcd_students);
		model.addAttribute("eceFcdStudentsList", eceFcdStudentsList);
		model.addAttribute("eceFcdStudentsFieldName", FieldNameType.ece_page_fcd_students);

		List<Event> eceClassTimeTableList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_class_time_table);
		model.addAttribute("eceClassTimeTableList", eceClassTimeTableList);
		model.addAttribute("eceClassTimeTableFieldName", FieldNameType.ece_page_class_time_table);

		List<Event> eceTestTimeTableList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_test_time_table);
		model.addAttribute("eceTestTimeTableList", eceTestTimeTableList);
		model.addAttribute("eceTestTimeTableFieldName", FieldNameType.ece_page_test_time_table);

		List<Event> eceCalanderList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_calander);
		model.addAttribute("eceCalanderList", eceCalanderList);
		model.addAttribute("eceCalanderFieldName", FieldNameType.ece_page_calander);

		List<Event> ececontentbeyondsyllabusList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_content_beyond_syllabus);
		model.addAttribute("ececontentbeyondsyllabusList", ececontentbeyondsyllabusList);
		model.addAttribute("ececontentbeyondsyllabusFieldName", FieldNameType.ece_page_content_beyond_syllabus);

		List<Event> eceNewsLetterList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_newsletter);
		model.addAttribute("eceNewsLetterList", eceNewsLetterList);
		model.addAttribute("eceNewsLetterFieldName", FieldNameType.ece_page_newsletter);

		List<Event> eceUgSyllabusList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_ug_syllabus);
		model.addAttribute("eceUgSyllabusList", eceUgSyllabusList);
		model.addAttribute("eceUgSyllabusFieldName", FieldNameType.ece_page_ug_syllabus);

		List<Event> ecePgSyllabusList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_pg_syllabus);
		model.addAttribute("ecePgSyllabusList", ecePgSyllabusList);
		model.addAttribute("ecePgSyllabusFieldName", FieldNameType.ece_page_pg_syllabus);

		List<Event> eceLibraryBooksList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_library_books_collection);
		model.addAttribute("eceLibraryBooksList", eceLibraryBooksList);
		model.addAttribute("eceLibraryBooksFieldName", FieldNameType.ece_library_books_collection);

		List<Event> ecePlacementList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_placement);
		model.addAttribute("ecePlacementList", ecePlacementList);
		model.addAttribute("ecePlacementFieldName", FieldNameType.ece_page_placement);

		List<Faculty> eceLabList = facultyService.getByPageAndFieldName(PageType.ece_page, FieldNameType.ece_page_labs);
		model.addAttribute("eceLabList", eceLabList);
		model.addAttribute("eceLabFieldName", FieldNameType.ece_page_labs);

		List<Event> eceEresourcesList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_eresources);
		model.addAttribute("eceEresourcesList", eceEresourcesList);
		model.addAttribute("eceEresourcesFieldName", FieldNameType.ece_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.ece_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.ece_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.ece_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.ece_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> eceResearchList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_research);
		model.addAttribute("eceResearchList", eceResearchList);
		model.addAttribute("eceResearchFieldName", FieldNameType.ece_page_research);

		List<Event> eceSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_sponsored_projects);
		model.addAttribute("eceSponsoredProjectsList", eceSponsoredProjectsList);
		model.addAttribute("eceSponsoredProjectsFieldName", FieldNameType.ece_page_sponsored_projects);

		List<Event> ecePhdPursuingList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_phd_pursuing);
		model.addAttribute("ecePhdPursuingList", ecePhdPursuingList);
		model.addAttribute("ecePhdPursuingFieldName", FieldNameType.ece_page_phd_pursuing);

		List<Event> ecePhdAwardeesList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_phd_awardees);
		model.addAttribute("ecePhdAwardeesList", ecePhdAwardeesList);
		model.addAttribute("ecePhdAwardeesFieldName", FieldNameType.ece_page_phd_awardees);

		List<Event> eceInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_instructional_materials);
		model.addAttribute("eceInstructionalMaterialsList", eceInstructionalMaterialsList);
		model.addAttribute("eceInstructionalMaterialsFieldName", FieldNameType.ece_instructional_materials);

		List<Event> ecePedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_pedagogical_activities);
		model.addAttribute("ecePedagogicalActivitiesList", ecePedagogicalActivitiesList);
		model.addAttribute("ecePedagogicalActivitiesFieldName", FieldNameType.ece_pedagogical_activities);

		List<Event> ecePedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_pedagogy_review_form);
		model.addAttribute("ecePedagogyReviewFormList", ecePedagogyReviewFormList);
		model.addAttribute("ecePedagogyReviewFormFieldName", FieldNameType.ece_pedagogy_review_form);

		List<Event> eceLabManualList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_lab_manual_form);
		model.addAttribute("eceLabManualList", eceLabManualList);
		model.addAttribute("eceLabManualFieldName", FieldNameType.ece_lab_manual_form);

		List<Event> eceMouSignedList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_Mou_Signed);
		model.addAttribute("eceMouSignedList", eceMouSignedList);
		model.addAttribute("eceMouSignedFieldName", FieldNameType.ece_page_Mou_Signed);

		List<Event> eceAlumniAssociationList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_alumni_association);
		model.addAttribute("eceAlumniAssociationList", eceAlumniAssociationList);
		model.addAttribute("eceAlumniAssociationFieldName", FieldNameType.ece_page_alumni_association);

		List<Event> eceProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_professional_bodies);
		model.addAttribute("eceProfessionalBodiesList", eceProfessionalBodiesList);
		model.addAttribute("eceProfessionalBodiesFieldName", FieldNameType.ece_page_professional_bodies);

		List<Event> eceProfessionalLinksList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_professional_links);
		model.addAttribute("eceProfessionalLinksList", eceProfessionalLinksList);
		model.addAttribute("eceProfessionalLinksFieldName", FieldNameType.ece_page_professional_links);

		List<Event> eceHigherEducationList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_higher_education);
		model.addAttribute("eceHigherEducationList", eceHigherEducationList);
		model.addAttribute("eceHigherEducationFieldName", FieldNameType.ece_page_higher_education);

		List<Event> eceClubActivitiesList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_club_activities);
		model.addAttribute("eceClubActivitiesList", eceClubActivitiesList);
		model.addAttribute("eceClubActivitiesFieldName", FieldNameType.ece_page_club_activities);

		List<Event> eceClubExternalLinksList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_club_external_links);
		model.addAttribute("eceClubExternalLinksList", eceClubExternalLinksList);
		model.addAttribute("eceClubExternalLinksFieldName", FieldNameType.ece_page_club_external_links);

		List<Event> eceInternshipList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_internship);
		model.addAttribute("eceInternshipList", eceInternshipList);
		model.addAttribute("eceInternshipFieldName", FieldNameType.ece_page_internship);

		List<Event> eceProjectsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_projects);
		model.addAttribute("eceProjectsList", eceProjectsList);
		model.addAttribute("eceProjectsFieldName", FieldNameType.ece_page_projects);

		List<Event> eceMiniProjectsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_mini_projects);
		model.addAttribute("eceMiniProjectsList", eceMiniProjectsList);
		model.addAttribute("eceMiniProjectsFieldName", FieldNameType.ece_page_mini_projects);

		List<Event> eceSocialActivitiesList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_social_activities);
		model.addAttribute("eceSocialActivitiesList", eceSocialActivitiesList);
		model.addAttribute("eceSocialActivitiesFieldName", FieldNameType.ece_page_social_activities);

		List<Faculty> eceIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_industrial_visit);
		model.addAttribute("eceIndustrialVisitList", eceIndustrialVisitList);
		model.addAttribute("eceIndustrialVisitFieldName", FieldNameType.ece_page_industrial_visit);

		List<Faculty> eceProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_project_exhibition);
		model.addAttribute("eceProjectExhibitionList", eceProjectExhibitionList);
		model.addAttribute("eceProjectExhibitionFieldName", FieldNameType.ece_page_project_exhibition);

		List<Event> eceOtherDetailsList = eventService.getByPageAndFieldName(PageType.ece_page,
				FieldNameType.ece_page_other_details);
		model.addAttribute("eceOtherDetailsList", eceOtherDetailsList);
		model.addAttribute("eceOtherDetailsFieldName", FieldNameType.ece_page_other_details);

		List<Event> eceFdpList = eventService.getByPageAndFieldName(PageType.ece_page, FieldNameType.ece_page_fdp);
		model.addAttribute("eceFdpList", eceFdpList);
		model.addAttribute("eceFdpFieldName", FieldNameType.ece_page_fdp);

		/*
		 * List<Faculty> eceSponsoredProjectsList =
		 * facultyService.getByPageAndFieldName(PageType.ece_page,
		 * FieldNameType.ece_page_sponsored_projects);
		 * model.addAttribute("eceSponsoredProjectsList", eceSponsoredProjectsList);
		 * model.addAttribute("eceSponsoredProjectsFieldName",
		 * FieldNameType.ece_page_sponsored_projects);
		 */

		return "admin/ece_dept";
	}

	@RequestMapping(value = "/mech_dept", method = RequestMethod.GET)
	public String mech_dept(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.mech_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.mech_page);

		List<Event> mechPageSliderImageList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_slider_images);
		model.addAttribute("mechPageSliderImageList", mechPageSliderImageList);
		model.addAttribute("mechPageSliderImageFieldName", FieldNameType.mech_page_slider_images);

		List<Event> mechPageLatestEventsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_latest_events);
		model.addAttribute("mechPageLatestEventsList", mechPageLatestEventsList);
		model.addAttribute("mechLatestEventsFieldName", FieldNameType.mech_page_latest_events);

		List<Event> mechPageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_upcoming_events);
		model.addAttribute("mechPageUpcomingEventsList", mechPageUpcomingEventsList);
		model.addAttribute("mechUpcomingEventsFieldName", FieldNameType.mech_page_upcoming_events);

		List<Data> upComingEventsDataList = dataService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.upcoming_events);
		model.addAttribute("upComingEventsDataList", upComingEventsDataList);
		model.addAttribute("upComingEventsFieldName", FieldNameType.upcoming_events);

		List<Data> latestEventsDataList = dataService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.latest_events);
		model.addAttribute("latestEventsDataList", latestEventsDataList);
		model.addAttribute("latestEventsFieldName", FieldNameType.latest_events);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> mechEventList = eventService.getByPageAndFieldName(PageType.mech_page, FieldNameType.mech_events);
		model.addAttribute("mechEventList", mechEventList);
		model.addAttribute("mechEventFieldName", FieldNameType.mech_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.mech_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.mech_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.mech_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.mech_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.mech_page_gallery);

		List<Event> mechDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_department_achiever_list);
		model.addAttribute("mechDepartmentAchieversList", mechDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.mech_department_achiever_list);

		List<Event> mechFcdStudentsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_fcd_students);
		model.addAttribute("mechFcdStudentsList", mechFcdStudentsList);
		model.addAttribute("mechFcdStudentsFieldName", FieldNameType.mech_page_fcd_students);

		List<Event> mechClassTimeTableList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_class_time_table);
		model.addAttribute("mechClassTimeTableList", mechClassTimeTableList);
		model.addAttribute("mechClassTimeTableFieldName", FieldNameType.mech_page_class_time_table);

		List<Event> mechTestTimeTableList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_test_time_table);
		model.addAttribute("mechTestTimeTableList", mechTestTimeTableList);
		model.addAttribute("mechTestTimeTableFieldName", FieldNameType.mech_page_test_time_table);

		List<Event> mechCalanderList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_calander);
		model.addAttribute("mechCalanderList", mechCalanderList);
		model.addAttribute("mechCalanderFieldName", FieldNameType.mech_page_calander);

		List<Event> mechNewsLetterList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_newsletter);
		model.addAttribute("mechNewsLetterList", mechNewsLetterList);
		model.addAttribute("mechNewsLetterFieldName", FieldNameType.mech_page_newsletter);

		List<Event> mechUgSyllabusList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_ug_syllabus);
		model.addAttribute("mechUgSyllabusList", mechUgSyllabusList);
		model.addAttribute("mechUgSyllabusFieldName", FieldNameType.mech_page_ug_syllabus);

		List<Event> mechPgSyllabusList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_pg_syllabus);
		model.addAttribute("mechPgSyllabusList", mechPgSyllabusList);
		model.addAttribute("mechPgSyllabusFieldName", FieldNameType.mech_page_pg_syllabus);

		List<Event> mechLibraryBooksList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_library_books_collection);
		model.addAttribute("mechLibraryBooksList", mechLibraryBooksList);
		model.addAttribute("mechLibraryBooksFieldName", FieldNameType.mech_library_books_collection);

		List<Event> mechcontentbeyondsyllabusList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_content_beyond_syllabus);
		model.addAttribute("mechcontentbeyondsyllabusList", mechcontentbeyondsyllabusList);
		model.addAttribute("mechcontentbeyondsyllabusFieldName", FieldNameType.mech_page_content_beyond_syllabus);

		List<Event> mechPlacementList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_placement);
		model.addAttribute("mechPlacementList", mechPlacementList);
		model.addAttribute("mechPlacementFieldName", FieldNameType.mech_page_placement);

		List<Faculty> mechLabList = facultyService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_labs);
		model.addAttribute("mechLabList", mechLabList);
		model.addAttribute("mechLabFieldName", FieldNameType.mech_page_labs);

		List<Event> mechEresourcesList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_eresources);
		model.addAttribute("mechEresourcesList", mechEresourcesList);
		model.addAttribute("mechEresourcesFieldName", FieldNameType.mech_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.mech_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.mech_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.mech_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.mech_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> mechResearchList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_research);
		model.addAttribute("mechResearchList", mechResearchList);
		model.addAttribute("mechResearchFieldName", FieldNameType.mech_page_research);

		List<Event> mechSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_sponsored_projects);
		model.addAttribute("mechSponsoredProjectsList", mechSponsoredProjectsList);
		model.addAttribute("mechSponsoredProjectsFieldName", FieldNameType.mech_page_sponsored_projects);

		List<Event> mechPhdPursuingList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_phd_pursuing);
		model.addAttribute("mechPhdPursuingList", mechPhdPursuingList);
		model.addAttribute("mechPhdPursuingFieldName", FieldNameType.mech_page_phd_pursuing);

		List<Event> mechPhdAwardeesList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_phd_awardees);
		model.addAttribute("mechPhdAwardeesList", mechPhdAwardeesList);
		model.addAttribute("mechPhdAwardeesFieldName", FieldNameType.mech_page_phd_awardees);

		List<Event> mechInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_instructional_materials);
		model.addAttribute("mechInstructionalMaterialsList", mechInstructionalMaterialsList);
		model.addAttribute("mechInstructionalMaterialsFieldName", FieldNameType.mech_instructional_materials);

		List<Event> mechPedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_pedagogical_activities);
		model.addAttribute("mechPedagogicalActivitiesList", mechPedagogicalActivitiesList);
		model.addAttribute("mechPedagogicalActivitiesFieldName", FieldNameType.mech_pedagogical_activities);

		List<Event> mechPedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_pedagogy_review_form);
		model.addAttribute("mechPedagogyReviewFormList", mechPedagogyReviewFormList);
		model.addAttribute("mechPedagogyReviewFormFieldName", FieldNameType.mech_pedagogy_review_form);

		List<Event> mechLabManualList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_lab_manual_form);
		model.addAttribute("mechLabManualList", mechLabManualList);
		model.addAttribute("mechLabManualFieldName", FieldNameType.mech_lab_manual_form);

		List<Event> mechMouSignedList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_Mou_Signed);
		model.addAttribute("mechMouSignedList", mechMouSignedList);
		model.addAttribute("mechMouSignedFieldName", FieldNameType.mech_page_Mou_Signed);

		List<Event> mechAlumniAssociationList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_alumni_association);
		model.addAttribute("mechAlumniAssociationList", mechAlumniAssociationList);
		model.addAttribute("mechAlumniAssociationFieldName", FieldNameType.mech_page_alumni_association);

		List<Event> mechProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_professional_bodies);
		model.addAttribute("mechProfessionalBodiesList", mechProfessionalBodiesList);
		model.addAttribute("mechProfessionalBodiesFieldName", FieldNameType.mech_page_professional_bodies);

		List<Event> mechProfessionalLinksList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_professional_links);
		model.addAttribute("mechProfessionalLinksList", mechProfessionalLinksList);
		model.addAttribute("mechProfessionalLinksFieldName", FieldNameType.mech_page_professional_links);

		List<Event> mechHigherEducationList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_higher_education);
		model.addAttribute("mechHigherEducationList", mechHigherEducationList);
		model.addAttribute("mechHigherEducationFieldName", FieldNameType.mech_page_higher_education);

		List<Event> mechClubActivitiesList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_club_activities);
		model.addAttribute("mechClubActivitiesList", mechClubActivitiesList);
		model.addAttribute("mechClubActivitiesFieldName", FieldNameType.mech_page_club_activities);

		List<Event> mechClubExternalLinksList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_club_external_links);
		model.addAttribute("mechClubExternalLinksList", mechClubExternalLinksList);
		model.addAttribute("mechClubExternalLinksFieldName", FieldNameType.mech_page_club_external_links);

		List<Event> mechInternshipList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_internship);
		model.addAttribute("mechInternshipList", mechInternshipList);
		model.addAttribute("mechInternshipFieldName", FieldNameType.mech_page_internship);

		List<Event> mechProjectsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_projects);
		model.addAttribute("mechProjectsList", mechProjectsList);
		model.addAttribute("mechProjectsFieldName", FieldNameType.mech_page_projects);

		List<Event> mechMiniProjectsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_mini_projects);
		model.addAttribute("mechMiniProjectsList", mechMiniProjectsList);
		model.addAttribute("mechMiniProjectsFieldName", FieldNameType.mech_page_mini_projects);

		List<Event> mechSocialActivitiesList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_social_activities);
		model.addAttribute("mechSocialActivitiesList", mechSocialActivitiesList);
		model.addAttribute("mechSocialActivitiesFieldName", FieldNameType.mech_page_social_activities);

		List<Faculty> mechIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_industrial_visit);
		model.addAttribute("mechIndustrialVisitList", mechIndustrialVisitList);
		model.addAttribute("mechIndustrialVisitFieldName", FieldNameType.mech_page_industrial_visit);

		List<Faculty> mechProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_project_exhibition);
		model.addAttribute("mechProjectExhibitionList", mechProjectExhibitionList);
		model.addAttribute("mechProjectExhibitionFieldName", FieldNameType.mech_page_project_exhibition);

		List<Event> mechOtherDetailsList = eventService.getByPageAndFieldName(PageType.mech_page,
				FieldNameType.mech_page_other_details);
		model.addAttribute("mechOtherDetailsList", mechOtherDetailsList);
		model.addAttribute("mechOtherDetailsFieldName", FieldNameType.mech_page_other_details);

		List<Event> mechFdpList = eventService.getByPageAndFieldName(PageType.mech_page, FieldNameType.mech_page_fdp);
		model.addAttribute("mechFdpList", mechFdpList);
		model.addAttribute("mechFdpFieldName", FieldNameType.mech_page_fdp);

		/*
		 * List<Faculty> mechSponsoredProjectsList =
		 * facultyService.getByPageAndFieldName(PageType.mech_page,
		 * FieldNameType.mech_page_sponsored_projects);
		 * model.addAttribute("mechSponsoredProjectsList", mechSponsoredProjectsList);
		 * model.addAttribute("mechSponsoredProjectsFieldName",
		 * FieldNameType.mech_page_sponsored_projects);
		 */

		return "admin/mech_dept";
	}

	@RequestMapping(value = "/tele_dept", method = RequestMethod.GET)
	public String tele_dept(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.tele_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.tele_page);

		List<Event> telePageSliderImageList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_slider_images);
		model.addAttribute("telePageSliderImageList", telePageSliderImageList);
		model.addAttribute("telePageSliderImageFieldName", FieldNameType.tele_page_slider_images);

		List<Event> telePageLatestEventsList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_latest_events);
		model.addAttribute("telePageLatestEventsList", telePageLatestEventsList);
		model.addAttribute("teleLatestEventsFieldName", FieldNameType.tele_page_latest_events);

		List<Event> telePageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_upcoming_events);
		model.addAttribute("telePageUpcomingEventsList", telePageUpcomingEventsList);
		model.addAttribute("teleUpcomingEventsFieldName", FieldNameType.tele_page_upcoming_events);
		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> teleEventList = eventService.getByPageAndFieldName(PageType.tele_page, FieldNameType.tele_events);
		model.addAttribute("teleEventList", teleEventList);
		model.addAttribute("teleEventFieldName", FieldNameType.tele_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.tele_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.tele_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.tele_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.tele_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.tele_page_gallery);

		List<Event> teleDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_department_achiever_list);
		model.addAttribute("teleDepartmentAchieversList", teleDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.tele_department_achiever_list);

		List<Event> teleFcdStudentsList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_fcd_students);
		model.addAttribute("teleFcdStudentsList", teleFcdStudentsList);
		model.addAttribute("teleFcdStudentsFieldName", FieldNameType.tele_page_fcd_students);

		List<Event> teleClassTimeTableList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_class_time_table);
		model.addAttribute("teleClassTimeTableList", teleClassTimeTableList);
		model.addAttribute("teleClassTimeTableFieldName", FieldNameType.tele_page_class_time_table);

		List<Event> teleTestTimeTableList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_test_time_table);
		model.addAttribute("teleTestTimeTableList", teleTestTimeTableList);
		model.addAttribute("teleTestTimeTableFieldName", FieldNameType.tele_page_test_time_table);

		List<Event> teleCalanderList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_calander);
		model.addAttribute("teleCalanderList", teleCalanderList);
		model.addAttribute("teleCalanderFieldName", FieldNameType.tele_page_calander);

		List<Event> teleNewsLetterList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_newsletter);
		model.addAttribute("teleNewsLetterList", teleNewsLetterList);
		model.addAttribute("teleNewsLetterFieldName", FieldNameType.tele_page_newsletter);

		List<Event> teleUgSyllabusList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_ug_syllabus);
		model.addAttribute("teleUgSyllabusList", teleUgSyllabusList);
		model.addAttribute("teleUgSyllabusFieldName", FieldNameType.tele_page_ug_syllabus);

		List<Event> teleLibraryBooksList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_library_books_collection);
		model.addAttribute("teleLibraryBooksList", teleLibraryBooksList);
		model.addAttribute("teleLibraryBooksFieldName", FieldNameType.tele_library_books_collection);

		List<Event> telePlacementList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_placement);
		model.addAttribute("telePlacementList", telePlacementList);
		model.addAttribute("telePlacementFieldName", FieldNameType.tele_page_placement);

		List<Faculty> teleLabList = facultyService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_labs);
		model.addAttribute("teleLabList", teleLabList);
		model.addAttribute("teleLabFieldName", FieldNameType.tele_page_labs);

		List<Event> teleEresourcesList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_eresources);
		model.addAttribute("teleEresourcesList", teleEresourcesList);
		model.addAttribute("teleEresourcesFieldName", FieldNameType.tele_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.tele_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.tele_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.tele_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.tele_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> teleResearchList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_research);
		model.addAttribute("teleResearchList", teleResearchList);
		model.addAttribute("teleResearchFieldName", FieldNameType.tele_page_research);

		List<Event> teleSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_sponsored_projects);
		model.addAttribute("teleSponsoredProjectsList", teleSponsoredProjectsList);
		model.addAttribute("teleSponsoredProjectsFieldName", FieldNameType.tele_page_sponsored_projects);

		List<Event> telePhdPursuingList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_phd_pursuing);
		model.addAttribute("telePhdPursuingList", telePhdPursuingList);
		model.addAttribute("telePhdPursuingFieldName", FieldNameType.tele_page_phd_pursuing);

		List<Event> telePhdAwardeesList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_phd_awardees);
		model.addAttribute("telePhdAwardeesList", telePhdAwardeesList);
		model.addAttribute("telePhdAwardeesFieldName", FieldNameType.tele_page_phd_awardees);

		List<Event> teleInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_instructional_materials);
		model.addAttribute("teleInstructionalMaterialsList", teleInstructionalMaterialsList);
		model.addAttribute("teleInstructionalMaterialsFieldName", FieldNameType.tele_instructional_materials);

		List<Event> telePedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_pedagogical_activities);
		model.addAttribute("telePedagogicalActivitiesList", telePedagogicalActivitiesList);
		model.addAttribute("telePedagogicalActivitiesFieldName", FieldNameType.tele_pedagogical_activities);

		List<Event> telePedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_pedagogy_review_form);
		model.addAttribute("telePedagogyReviewFormList", telePedagogyReviewFormList);
		model.addAttribute("telePedagogyReviewFormFieldName", FieldNameType.tele_pedagogy_review_form);

		List<Event> teleLabManualList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_lab_manual_form);
		model.addAttribute("teleLabManualList", teleLabManualList);
		model.addAttribute("teleLabManualFieldName", FieldNameType.tele_lab_manual_form);

		List<Event> teleMouSignedList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_Mou_Signed);
		model.addAttribute("teleMouSignedList", teleMouSignedList);
		model.addAttribute("teleMouSignedFieldName", FieldNameType.tele_page_Mou_Signed);

		List<Event> teleAlumniAssociationList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_alumni_association);
		model.addAttribute("teleAlumniAssociationList", teleAlumniAssociationList);
		model.addAttribute("teleAlumniAssociationFieldName", FieldNameType.tele_page_alumni_association);

		List<Event> teleProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_professional_bodies);
		model.addAttribute("teleProfessionalBodiesList", teleProfessionalBodiesList);
		model.addAttribute("teleProfessionalBodiesFieldName", FieldNameType.tele_page_professional_bodies);

		List<Event> teleProfessionalLinksList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_professional_links);
		model.addAttribute("teleProfessionalLinksList", teleProfessionalLinksList);
		model.addAttribute("teleProfessionalLinksFieldName", FieldNameType.tele_page_professional_links);

		List<Event> teleHigherEducationList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_higher_education);
		model.addAttribute("teleHigherEducationList", teleHigherEducationList);
		model.addAttribute("teleHigherEducationFieldName", FieldNameType.tele_page_higher_education);

		List<Event> teleClubActivitiesList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_club_activities);
		model.addAttribute("teleClubActivitiesList", teleClubActivitiesList);
		model.addAttribute("teleClubActivitiesFieldName", FieldNameType.tele_page_club_activities);

		List<Event> teleClubExternalLinksList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_club_external_links);
		model.addAttribute("teleClubExternalLinksList", teleClubExternalLinksList);
		model.addAttribute("teleClubExternalLinksFieldName", FieldNameType.tele_page_club_external_links);

		List<Event> teleInternshipList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_internship);
		model.addAttribute("teleInternshipList", teleInternshipList);
		model.addAttribute("teleInternshipFieldName", FieldNameType.tele_page_internship);

		List<Event> teleProjectsList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_projects);
		model.addAttribute("teleProjectsList", teleProjectsList);
		model.addAttribute("teleProjectsFieldName", FieldNameType.tele_page_projects);

		List<Event> teleMiniProjectsList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_mini_projects);
		model.addAttribute("teleMiniProjectsList", teleMiniProjectsList);
		model.addAttribute("teleMiniProjectsFieldName", FieldNameType.tele_page_mini_projects);

		List<Event> teleSocialActivitiesList = eventService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_social_activities);
		model.addAttribute("teleSocialActivitiesList", teleSocialActivitiesList);
		model.addAttribute("teleSocialActivitiesFieldName", FieldNameType.tele_page_social_activities);

		List<Faculty> teleIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_industrial_visit);
		model.addAttribute("teleIndustrialVisitList", teleIndustrialVisitList);
		model.addAttribute("teleIndustrialVisitFieldName", FieldNameType.tele_page_industrial_visit);

		List<Faculty> teleProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.tele_page,
				FieldNameType.tele_page_project_exhibition);
		model.addAttribute("teleProjectExhibitionList", teleProjectExhibitionList);
		model.addAttribute("teleProjectExhibitionFieldName", FieldNameType.tele_page_project_exhibition);

		List<Event> teleFdpList = eventService.getByPageAndFieldName(PageType.tele_page, FieldNameType.tele_page_fdp);
		model.addAttribute("teleFdpList", teleFdpList);
		model.addAttribute("teleFdpFieldName", FieldNameType.tele_page_fdp);

		return "admin/tele_dept";
	}

	@RequestMapping(value = "/about_library", method = RequestMethod.GET)
	public String about_library(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.library_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.library_page);

		List<Event> libraryPageSliderImageList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_slider_images);
		model.addAttribute("libraryPageSliderImageList", libraryPageSliderImageList);
		model.addAttribute("libraryPageSliderImageFieldName", FieldNameType.library_page_slider_images);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.library_page_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.library_page_gallery);

		List<Event> libraryBooksList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_books_collection);
		model.addAttribute("libraryBooksList", libraryBooksList);
		model.addAttribute("libraryBooksFieldName", FieldNameType.library_books_collection);

		List<Event> newArrivalList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_newArrivals);
		model.addAttribute("newArrivalList", newArrivalList);
		model.addAttribute("newArrivalListFieldName", FieldNameType.library_page_newArrivals);

		List<Event> libraryActivityList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_activities);
		model.addAttribute("libraryActivityList", libraryActivityList);
		model.addAttribute("libraryActivityListFieldName", FieldNameType.library_page_activities);

		List<Event> libraryLinksList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_links);
		model.addAttribute("libraryLinksList", libraryLinksList);
		model.addAttribute("libraryLinksFieldName", FieldNameType.library_page_links);

		List<Event> libraryEresourcesList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_eresources);
		model.addAttribute("libraryEresourcesList", libraryEresourcesList);
		model.addAttribute("libraryEresourcesListFieldName", FieldNameType.library_page_eresources);

		List<Event> libraryOtherDetailsList = eventService.getByPageAndFieldName(PageType.library_page,
				FieldNameType.library_page_other_details);
		model.addAttribute("libraryOtherDetailsList", libraryOtherDetailsList);
		model.addAttribute("libraryOtherDetailsFieldName", FieldNameType.library_page_other_details);

		return "admin/about_library";
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.about_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.about_page);

		return "admin/about";
	}

	@RequestMapping(value = "/about_ksrif", method = RequestMethod.GET)
	public String about_ksrif(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.about_ksrif_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.about_ksrif_page);

		return "admin/about_ksrif";
	}

	@RequestMapping(value = "/mca_dept", method = RequestMethod.GET)
	public String mca_dept(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.mca_dept_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.mca_dept_page);

		return "admin/mca_dept";
	}

	@RequestMapping(value = "/administration", method = RequestMethod.GET)
	public String administration(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.administration_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.administration_page);

		return "admin/administration";
	}

	@RequestMapping(value = "/academic_governing_council", method = RequestMethod.GET)
	public String academic_governing_council(ModelMap model) {

		boolean permissionGranted = SessionUtil
				.isLoggedInUserAuthenticatedForPage(PageType.academic_governing_council_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.academic_governing_council_page);

		// List<Faculty> academicAdvisoryFacultyList =
		// facultyService.getByPageAndFieldName(PageType.administration_page,
		// FieldNameType.about_page_academic_advisory_faculty);
		// model.addAttribute("academicAdvisoryFacultyList",
		// academicAdvisoryFacultyList);
		// model.addAttribute("academicAdvisoryFacultyFieldName",
		// FieldNameType.about_page_academic_advisory_faculty);

		List<Faculty> academicGoverningCouncilFacultyList = facultyService.getByPageAndFieldName(
				PageType.academic_governing_council_page, FieldNameType.academic_governing_council_faculty);
		model.addAttribute("academicGoverningCouncilFacultyList", academicGoverningCouncilFacultyList);
		model.addAttribute("governingCouncilFacultyFieldName", FieldNameType.academic_governing_council_faculty);

		return "admin/academic_governing_council";
	}

	@RequestMapping(value = "/office_administration", method = RequestMethod.GET)
	public String office_administration(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.office_administration_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.office_administration_page);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.office_administration_page,
				FieldNameType.office_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.office_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(
				PageType.office_administration_page, FieldNameType.office_administration_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName",
				FieldNameType.office_administration_page_supporting_faculty);

		return "admin/office_administration";
	}

	@RequestMapping(value = "/sports", method = RequestMethod.GET)
	public String sports(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.sports_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.sports_page);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.sports_page_achiever);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.sports_page_gallery);

		List<Event> sportsActivitiesList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_activities);
		model.addAttribute("sportsActivitiesList", sportsActivitiesList);
		model.addAttribute("sportsActivitiesFieldName", FieldNameType.sports_activities);

		List<Event> sportsAchieversList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_achiever_list);
		model.addAttribute("sportsAchieversList", sportsAchieversList);
		model.addAttribute("sportsAchieversFieldName", FieldNameType.sports_achiever_list);

		List<Event> universityAchieversList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.university_achiever_list);
		model.addAttribute("universityAchieversList", universityAchieversList);
		model.addAttribute("universityAchieversFieldName", FieldNameType.university_achiever_list);

		List<Event> stateAchieversList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.state_achiever_list);
		model.addAttribute("stateAchieversList", stateAchieversList);
		model.addAttribute("stateAchieversFieldName", FieldNameType.state_achiever_list);

		List<Event> nationalAchieversList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.national_achiever_list);
		model.addAttribute("nationalAchieversList", nationalAchieversList);
		model.addAttribute("nationalAchieversFieldName", FieldNameType.national_achiever_list);

		List<Event> sportsReportsList = eventService.getByPageAndFieldName(PageType.sports_page,
				FieldNameType.sports_reports_list);
		model.addAttribute("sportsReportsList", sportsReportsList);
		model.addAttribute("sportsReportsFieldName", FieldNameType.sports_reports_list);

		return "admin/sports";
	}

	@RequestMapping(value = "/nss", method = RequestMethod.GET)
	public String nss(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.nss_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.nss_page);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.nss_page, FieldNameType.nss_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.nss_page_gallery);

		List<Data> upComingEventsDataList = dataService.getByPageAndFieldName(PageType.nss_page,
				FieldNameType.upcoming_events);
		model.addAttribute("upComingEventsDataList", upComingEventsDataList);
		model.addAttribute("upComingEventsFieldName", FieldNameType.upcoming_events);

		List<Event> nssEventList = eventService.getByPageAndFieldName(PageType.nss_page, FieldNameType.nss_events);
		model.addAttribute("nssEventList", nssEventList);
		model.addAttribute("nssEventFieldName", FieldNameType.nss_events);

		List<Event> nssAchievementsList = eventService.getByPageAndFieldName(PageType.nss_page,
				FieldNameType.nss_achievements);
		model.addAttribute("nssAchievementsList", nssAchievementsList);
		model.addAttribute("nssAchievementsFieldName", FieldNameType.nss_achievements);

		List<Event> nssReportsList = eventService.getByPageAndFieldName(PageType.nss_page, FieldNameType.nss_reports);
		model.addAttribute("nssReportsList", nssReportsList);
		model.addAttribute("nssReportsFieldName", FieldNameType.nss_reports);

		return "admin/nss";
	}

	@RequestMapping(value = "/redcross", method = RequestMethod.GET)
	public String redcross(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.redcross_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.redcross_page);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.redcross_page_gallery);

		List<Data> upComingEventsDataList = dataService.getByPageAndFieldName(PageType.redcross_page,
				FieldNameType.upcoming_events);
		model.addAttribute("upComingEventsDataList", upComingEventsDataList);
		model.addAttribute("upComingEventsFieldName", FieldNameType.upcoming_events);

		List<Event> redcrossEventList = eventService.getByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_events);
		model.addAttribute("redcrossEventList", redcrossEventList);
		model.addAttribute("redcrossEventFieldName", FieldNameType.redcross_events);

		List<Event> redcrossAchievementsList = eventService.getByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_achievements);
		model.addAttribute("redcrossAchievementsList", redcrossAchievementsList);
		model.addAttribute("redcrossAchievementsFieldName", FieldNameType.redcross_achievements);

		List<Event> redcrossReportsList = eventService.getByPageAndFieldName(PageType.redcross_page,
				FieldNameType.redcross_reports);
		model.addAttribute("redcrossReportsList", redcrossReportsList);
		model.addAttribute("redcrossReportsFieldName", FieldNameType.redcross_reports);

		return "admin/redcross";
	}

	@RequestMapping(value = "/public_press", method = RequestMethod.GET)
	public String public_press(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.public_press_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.public_press_page);

		List<Event> publicPressList = eventService.getByPageAndFieldName(PageType.public_press_page,
				FieldNameType.public_press_page_achievement);
		model.addAttribute("publicPressList", publicPressList);
		model.addAttribute("publicPressFieldName", FieldNameType.public_press_page_achievement);

		return "admin/public_press";
	}

	@RequestMapping(value = "/iiic", method = RequestMethod.GET)
	public String iiic(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.iiic_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.iiic_page);
		List<Event> mouSignedList = eventService.getByPageAndFieldName(PageType.iiic_page,
				FieldNameType.iiic_page_Mou_Signed);
		model.addAttribute("mouSignedList", mouSignedList);
		model.addAttribute("mouFieldName", FieldNameType.iiic_page_Mou_Signed);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.iiic_page,
				FieldNameType.iiic_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.iiic_page_gallery);

		
		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.iiic_page,
				FieldNameType.iiic_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.iiic_page_faculty);
		
		List<Event> activitiesEventsList = eventService.getByPageAndFieldName(PageType.iiic_page, FieldNameType.iiic_events);
		model.addAttribute("activitiesEventsList", activitiesEventsList);
		model.addAttribute("activitiesFieldName", FieldNameType.iiic_events);

		return "admin/iiic";
	}
	
	@RequestMapping(value = "/iqac", method = RequestMethod.GET)
	public String iqac(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.iqac_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.iqac_page);	
		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.iqac_page,
				FieldNameType.iqac_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.iqac_page_faculty);
		
		List<Event> annualCompositionList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_annual_composition);
		model.addAttribute("annualCompositionList", annualCompositionList);
		model.addAttribute("annualCompositionFieldName", FieldNameType.iqac_annual_composition);
		
		List<Event> minutesOfMeetingList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_minutes_of_meeting);
		model.addAttribute("minutesOfMeetingList", minutesOfMeetingList);
		model.addAttribute("minutesOfMeetingFieldName", FieldNameType.iqac_minutes_of_meeting);
		
		List<Event> bestPractiseList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_best_practises);
		model.addAttribute("bestPractiseList", bestPractiseList);
		model.addAttribute("bestPractiseFieldName", FieldNameType.iqac_best_practises);
		
		List<Event> annualReportsList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_annual_reports);
		model.addAttribute("annualReportsList", annualReportsList);
		model.addAttribute("annualReportsFieldName", FieldNameType.iqac_annual_reports);
		
		List<Event> otherReportsList = eventService.getByPageAndFieldName(PageType.iqac_page, FieldNameType.iqac_other_reports);
		model.addAttribute("otherReportsList", otherReportsList);
		model.addAttribute("annualReportsFieldName", FieldNameType.iqac_other_reports);
		
		return "admin/iqac";
	}

	@RequestMapping(value = "/about_placement", method = RequestMethod.GET)
	public String about_placement(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.placement_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.placement_page);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> placementPageLatestEventsList = eventService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_latest_events);
		model.addAttribute("placementPageLatestEventsList", placementPageLatestEventsList);
		model.addAttribute("placementLatestEventsFieldName", FieldNameType.placement_page_latest_events);

		List<Event> placementPageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_upcoming_events);
		model.addAttribute("placementPageUpcomingEventsList", placementPageUpcomingEventsList);
		model.addAttribute("placementUpcomingEventsFieldName", FieldNameType.placement_page_upcoming_events);
		
		List<Event> eventList = eventService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_events);
		model.addAttribute("eventList", eventList);
		model.addAttribute("eventFieldName", FieldNameType.placement_events);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.placement_page_gallery);

		List<Placement> placementCompanyList = placementService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_company_page);
		model.addAttribute("placementCompanyList", placementCompanyList);
		model.addAttribute("placementCompanyFieldName", FieldNameType.placement_company_page);

		List<Placement> placementTrainingList = placementService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_training_page);
		model.addAttribute("placementTrainingList", placementTrainingList);
		model.addAttribute("placementTrainingFieldName", FieldNameType.placement_training_page);

		List<Placement> placementHigherStudiesList = placementService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_higher_studies_page);
		model.addAttribute("placementHigherStudiesList", placementHigherStudiesList);
		model.addAttribute("placementHigherStudiesFieldName", FieldNameType.placement_higher_studies_page);

		List<Placement> placementInternshipList = placementService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_internship_page);
		model.addAttribute("placementInternshipList", placementInternshipList);
		model.addAttribute("placementInternshipFieldName", FieldNameType.placement_internship_page);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.placement_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.placement_page,
				FieldNameType.placement_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.placement_page_supporting_faculty);

		return "admin/about_placement";
	}

	@RequestMapping(value = "/transport", method = RequestMethod.GET)
	public String transport(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.transport_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.transport_page);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.transport_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.transport_page,
				FieldNameType.transport_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.transport_page_faculty);

		return "admin/transport";
	}

	@RequestMapping(value = "/science_dept", method = RequestMethod.GET)
	public String science_dept(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.science_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.science_page);

		List<Event> eventList = eventService.getByPageAndFieldName(PageType.science_page, FieldNameType.science_events);
		model.addAttribute("eventList", eventList);
		model.addAttribute("eventFieldName", FieldNameType.science_events);

		List<Faculty> physicsFacultyList = facultyService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_physics_faculty);
		model.addAttribute("physicsFacultyList", physicsFacultyList);
		model.addAttribute("physicsFacultyFieldName", FieldNameType.science_page_physics_faculty);

		List<Faculty> chemistryFacultyList = facultyService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_chemistry_faculty);
		model.addAttribute("chemistryFacultyList", chemistryFacultyList);
		model.addAttribute("chemistryFacultyFieldName", FieldNameType.science_page_chemistry_faculty);

		List<Faculty> mathsFacultyList = facultyService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_maths_faculty);
		model.addAttribute("mathsFacultyList", mathsFacultyList);
		model.addAttribute("mathsFacultyFieldName", FieldNameType.science_page_maths_faculty);

		List<Faculty> humanitiesFacultyList = facultyService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_humanities_faculty);
		model.addAttribute("humanitiesFacultyList", humanitiesFacultyList);
		model.addAttribute("humanitiesFacultyFieldName", FieldNameType.science_page_humanities_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_supporting_staff_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.science_page_supporting_staff_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.science_page_gallery);

		List<Event> scienceClassTimeTableList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_class_time_table);
		model.addAttribute("scienceClassTimeTableList", scienceClassTimeTableList);
		model.addAttribute("scienceClassTimeTableFieldName", FieldNameType.science_page_class_time_table);

		List<Event> scienceTestTimeTableList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_test_time_table);
		model.addAttribute("scienceTestTimeTableList", scienceTestTimeTableList);
		model.addAttribute("scienceTestTimeTableFieldName", FieldNameType.science_page_test_time_table);

		List<Event> scienceCalanderList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_calander);
		model.addAttribute("scienceCalanderList", scienceCalanderList);
		model.addAttribute("scienceCalanderFieldName", FieldNameType.science_page_calander);

		List<Event> scienceNewsLetterList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_newsletter);
		model.addAttribute("scienceNewsLetterList", scienceNewsLetterList);
		model.addAttribute("scienceNewsLetterFieldName", FieldNameType.science_page_newsletter);

		List<Event> physicsSyllabusList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_physics_syllabus);
		model.addAttribute("physicsSyllabusList", physicsSyllabusList);
		model.addAttribute("physicsSyllabusFieldName", FieldNameType.science_page_physics_syllabus);

		List<Event> chemistrySyllabusList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_chemistry_syllabus);
		model.addAttribute("chemistrySyllabusList", chemistrySyllabusList);
		model.addAttribute("chemistrySyllabusFieldName", FieldNameType.science_page_chemistry_syllabus);

		List<Event> mathematicsSyllabusList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_mathematics_syllabus);
		model.addAttribute("mathematicsSyllabusList", mathematicsSyllabusList);
		model.addAttribute("mathematicsSyllabusFieldName", FieldNameType.science_page_mathematics_syllabus);

		List<Faculty> scienceLabList = facultyService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_labs);
		model.addAttribute("scienceLabList", scienceLabList);
		model.addAttribute("scienceLabFieldName", FieldNameType.science_page_labs);

		List<Event> scienceEresourcesList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_eresources);
		model.addAttribute("scienceEresourcesList", scienceEresourcesList);
		model.addAttribute("scienceEresourcesFieldName", FieldNameType.science_eresources);

		List<Event> scienceResearchList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_research);
		model.addAttribute("scienceResearchList", scienceResearchList);
		model.addAttribute("scienceResearchFieldName", FieldNameType.science_page_research);

		List<Event> scienceSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_sponsored_projects);
		model.addAttribute("scienceSponsoredProjectsList", scienceSponsoredProjectsList);
		model.addAttribute("scienceSponsoredProjectsFieldName", FieldNameType.science_page_sponsored_projects);

		List<Event> sciencePhdPursuingList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_phd_pursuing);
		model.addAttribute("sciencePhdPursuingList", sciencePhdPursuingList);
		model.addAttribute("sciencePhdPursuingFieldName", FieldNameType.science_page_phd_pursuing);

		List<Event> sciencePhdAwardeesList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_phd_awardees);
		model.addAttribute("sciencePhdAwardeesList", sciencePhdAwardeesList);
		model.addAttribute("sciencePhdAwardeesFieldName", FieldNameType.science_page_phd_awardees);

		List<Event> scienceInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_instructional_materials);
		model.addAttribute("scienceInstructionalMaterialsList", scienceInstructionalMaterialsList);
		model.addAttribute("scienceInstructionalMaterialsFieldName", FieldNameType.science_instructional_materials);

		List<Event> sciencePedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_pedagogical_activities);
		model.addAttribute("sciencePedagogicalActivitiesList", sciencePedagogicalActivitiesList);
		model.addAttribute("sciencePedagogicalActivitiesFieldName", FieldNameType.science_pedagogical_activities);

		List<Event> sciencePedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_pedagogy_review_form);
		model.addAttribute("sciencePedagogyReviewFormList", sciencePedagogyReviewFormList);
		model.addAttribute("sciencePedagogyReviewFormFieldName", FieldNameType.science_pedagogy_review_form);

		List<Event> scienceLabManualList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_lab_manual_form);
		model.addAttribute("scienceLabManualList", scienceLabManualList);
		model.addAttribute("scienceLabManualFieldName", FieldNameType.science_lab_manual_form);

		List<Event> scienceFdpList = eventService.getByPageAndFieldName(PageType.science_page,
				FieldNameType.science_page_fdp);
		model.addAttribute("scienceFdpList", scienceFdpList);
		model.addAttribute("scienceFdpFieldName", FieldNameType.science_page_fdp);

		return "admin/science_dept";
	}

	@RequestMapping(value = "/ananya", method = RequestMethod.GET)
	public String ananya(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.ananya_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.ananya_page);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.ananya_page,
				FieldNameType.ananya_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.ananya_page_gallery);

		return "admin/ananya";
	}

	@RequestMapping(value = "/disciplinary_measures", method = RequestMethod.GET)
	public String disciplinary_measures(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.disciplinary_measures_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.disciplinary_measures_page);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.disciplinary_measures_page,
				FieldNameType.disciplinary_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.disciplinary_page_faculty);

		return "admin/disciplinary_measures";
	}

	@RequestMapping(value = "/fees_structure", method = RequestMethod.GET)
	public String fees_structure(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.fee_structure_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.fee_structure_page);

		List<Event> feeStructureList = eventService.getByPageAndFieldName(PageType.fee_structure_page,
				FieldNameType.fee_structure_page_fees);
		model.addAttribute("feeStructureList", feeStructureList);
		model.addAttribute("feeStructureFieldName", FieldNameType.fee_structure_page_fees);

		return "admin/fees_structure";
	}

	@RequestMapping(value = "/alumni", method = RequestMethod.GET)
	public String alumni(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.alumni_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.alumni_page);

		List<Event> alumniMeetsList = eventService.getByPageAndFieldName(PageType.alumni_page,
				FieldNameType.alumni_meets);
		model.addAttribute("alumniMeetsList", alumniMeetsList);
		model.addAttribute("alumniMeetsFieldName", FieldNameType.alumni_meets);

		List<Event> cseAlumniMembersList = eventService.getByPageAndFieldName(PageType.alumni_page,
				FieldNameType.cse_alumni_members_list);
		model.addAttribute("cseAlumniMembersList", cseAlumniMembersList);
		model.addAttribute("cseAlumniMembersFieldName", FieldNameType.cse_alumni_members_list);

		List<Event> eceAlumniMembersList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.ece_alumni_members_list);
		model.addAttribute("eceAlumniMembersList", eceAlumniMembersList);
		model.addAttribute("eceAlumniMembersFieldName", FieldNameType.ece_alumni_members_list);

		List<Event> mechAlumniMembersList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.mech_alumni_members_list);
		model.addAttribute("mechAlumniMembersList", mechAlumniMembersList);
		model.addAttribute("mechAlumniMembersFieldName", FieldNameType.mech_alumni_members_list);

		List<Event> teleAlumniMembersList = eventService.getActiveByPageAndFieldName(PageType.alumni_page,
				FieldNameType.tele_alumni_members_list);
		model.addAttribute("teleAlumniMembersList", teleAlumniMembersList);
		model.addAttribute("teleAlumniMembersFieldName", FieldNameType.tele_alumni_members_list);

		List<Event> alumniGalleryList = eventService.getByPageAndFieldName(PageType.alumni_page,
				FieldNameType.alumni_page_gallery);
		model.addAttribute("alumniGalleryList", alumniGalleryList);
		model.addAttribute("alumniGalleryFieldName", FieldNameType.alumni_page_gallery);

		return "admin/alumni";
	}

	@RequestMapping(value = "/alumni_list", method = RequestMethod.GET)
	public String alumni_list(ModelMap model, @RequestParam(value = "page", required = false) String page) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.alumni_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		if (StringUtils.isEmpty(page)) {
			page = "1";
		}
		Integer pageInt = Integer.parseInt(page);
		if (pageInt < 1) {
			pageInt = 1;
		}

		List<Alumni> alumniList = alumniService.findByPage(page);
		model.addAttribute("alumniList", alumniList);
		if (pageInt > 1) {
			model.addAttribute("hasPrev", true);
			model.addAttribute("prevPage", pageInt - 1);
		}
		if (alumniList.size() == alumniService.getPAGE_SIZE()) {
			model.addAttribute("hasNext", true);
			model.addAttribute("nextPage", pageInt + 1);
		}
		return "admin/alumni_list";
	}

	@RequestMapping(value = "/grievance", method = RequestMethod.GET)
	public String grievance(ModelMap model, @RequestParam(value = "page", required = false) String page) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.grievance_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		if (StringUtils.isEmpty(page)) {
			page = "1";
		}
		Integer pageInt = Integer.parseInt(page);
		if (pageInt < 1) {
			pageInt = 1;
		}

		List<Grievance> grievanceList = grievanceService.findByPage(page);
		model.addAttribute("grievanceList", grievanceList);
		if (pageInt > 1) {
			model.addAttribute("hasPrev", true);
			model.addAttribute("prevPage", pageInt - 1);
		}
		if (grievanceList.size() == grievanceService.getPAGE_SIZE()) {
			model.addAttribute("hasNext", true);
			model.addAttribute("nextPage", pageInt + 1);
		}
		return "admin/grievance";
	}

	@RequestMapping(value = "/permissions", method = RequestMethod.GET)
	public String permissions(ModelMap model) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.permissions_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		PageType[] pageTypeArr = PageType.values();
		model.addAttribute("pageTypeList", pageTypeArr);
		model.addAttribute("tab", "changerole");
		return "admin/permissions";
	}

	@RequestMapping(value = "/contactUs", method = RequestMethod.GET)
	public String contactUs(ModelMap model, @RequestParam(value = "page", required = false) String page) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.contact_us_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		if (StringUtils.isEmpty(page)) {
			page = "1";
		}
		Integer pageInt = Integer.parseInt(page);
		if (pageInt < 1) {
			pageInt = 1;
		}

		List<ContactUs> contactUsList = contactUsService.findByPage(page);
		model.addAttribute("contactUsList", contactUsList);
		if (pageInt > 1) {
			model.addAttribute("hasPrev", true);
			model.addAttribute("prevPage", pageInt - 1);
		}
		if (contactUsList.size() == contactUsService.getPAGE_SIZE()) {
			model.addAttribute("hasNext", true);
			model.addAttribute("nextPage", pageInt + 1);
		}
		return "admin/contactUs";
	}
	
	@RequestMapping(value = "/feedback", method = RequestMethod.GET)
	public String feedback(ModelMap model, @RequestParam(value = "page", required = false) String page) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.contact_us_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		if (StringUtils.isEmpty(page)) {
			page = "1";
		}
		Integer pageInt = Integer.parseInt(page);
		if (pageInt < 1) {
			pageInt = 1;
		}

		List<Feedback> feedbackList = feedbackService.findByPage(page);
		model.addAttribute("feedbackList", feedbackList);
		if (pageInt > 1) {
			model.addAttribute("hasPrev", true);
			model.addAttribute("prevPage", pageInt - 1);
		}
		if (feedbackList.size() == feedbackService.getPAGE_SIZE()) {
			model.addAttribute("hasNext", true);
			model.addAttribute("nextPage", pageInt + 1);
		}
		return "admin/feedback";
	}

	@RequestMapping(value = "/hostel", method = RequestMethod.GET)
	public String hostel(ModelMap model, @RequestParam(value = "page", required = false) String page) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.hostel_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		if (StringUtils.isEmpty(page)) {
			page = "1";
		}
		Integer pageInt = Integer.parseInt(page);
		if (pageInt < 1) {
			pageInt = 1;
		}

		List<HostelEnrollment> hostelEnrollmentList = hostelEnrollmentService.findByPage(page);
		model.addAttribute("hostelEnrollmentList", hostelEnrollmentList);
		if (pageInt > 1) {
			model.addAttribute("hasPrev", true);
			model.addAttribute("prevPage", pageInt - 1);
		}
		if (hostelEnrollmentList.size() == contactUsService.getPAGE_SIZE()) {
			model.addAttribute("hasNext", true);
			model.addAttribute("nextPage", pageInt + 1);
		}
		return "admin/hostel";
	}

	@RequestMapping(value = "/course", method = RequestMethod.GET)
	public String course(ModelMap model, @RequestParam(value = "page", required = false) String page) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.course_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		if (StringUtils.isEmpty(page)) {
			page = "1";
		}
		Integer pageInt = Integer.parseInt(page);
		if (pageInt < 1) {
			pageInt = 1;
		}

		List<Course> courseList = courseService.findByPage(page);
		model.addAttribute("courseList", courseList);
		if (pageInt > 1) {
			model.addAttribute("hasPrev", true);
			model.addAttribute("prevPage", pageInt - 1);
		}
		if (courseList.size() == courseService.getPAGE_SIZE()) {
			model.addAttribute("hasNext", true);
			model.addAttribute("nextPage", pageInt + 1);
		}
		model.addAttribute("courseList", courseList);
		return "admin/course";
	}

	@RequestMapping(value = "/loadexisting", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String loadExisting(ModelMap model, @RequestParam(value = "username", required = true) String username) {

		List<PageType> existingPageTypeList = new ArrayList<PageType>();
		List<PageType> remainingPageTypeList = new ArrayList<PageType>();
		Map<String, Object> resultMap = new HashMap<String, Object>();

		User user = userService.getByUsername(username);
		if (user != null) {

			for (UserPage userPage : user.getPages()) {
				existingPageTypeList.add(userPage.getPage());
			}

			PageType[] pageTypeArr = PageType.values();
			for (int i = 0; i < pageTypeArr.length; i++) {
				if (existingPageTypeList.contains(pageTypeArr[i]) == false) {
					remainingPageTypeList.add(pageTypeArr[i]);
				}
			}
		}
		resultMap.put("existingPageTypeList", existingPageTypeList);
		resultMap.put("remainingPageTypeList", remainingPageTypeList);
		return new Gson().toJson(resultMap);
	}

	@RequestMapping(value = "/viewUsers", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String viewUsers(ModelMap model, @RequestParam(value = "page", required = true) String pageStr) {
		PageType page = PageType.valueOf(pageStr);
		List<UserPage> userPageList = userPageService.getByPage(page);
		Set<User> userSet = new HashSet<User>();
		for (UserPage userPage : userPageList) {
			userSet.add(userPage.getUser());
		}
		List<String> userNameList = new ArrayList<String>();
		for (User user : userSet) {
			userNameList.add(user.getUsername());
		}
		return new Gson().toJson(userNameList);
	}

	@RequestMapping(value = "/grantRole", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String grantRole(ModelMap model, @RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "newRoles", required = true) String newRoles) {

		User user = userService.getByUsername(username);
		Set<PageType> oldPageTypeSet = new HashSet<>();
		for (UserPage userPage : user.getPages()) {
			oldPageTypeSet.add(userPage.getPage());
		}
		Set<PageType> newPageTypeSet = new HashSet<>();
		String[] pageArr = newRoles.split(",");
		for (int i = 0; i < pageArr.length; i++) {
			PageType page = PageType.valueOf(pageArr[i]);
			newPageTypeSet.add(page);
		}
		List<UserPage> newUserPageList = new ArrayList<>();
		for (int i = 0; i < pageArr.length; i++) {
			PageType page = PageType.valueOf(pageArr[i]);
			if(oldPageTypeSet.contains(page)) {
				continue;
			}
			UserPage userPage = new UserPage();
			userPage.setPage(page);
			userPage.setUser(user);
			newUserPageList.add(userPage);
		}
		
		List<UserPage> oldUserPageList = new ArrayList<>();
		for (UserPage userPage : user.getPages()) {
			if(newPageTypeSet.contains(userPage.getPage())) {
				continue;
			}
			oldUserPageList.add(userPage);
		}
		userPageService.deleteUserPage(oldUserPageList);
		userPageService.saveOrUpdate(newUserPageList);
		JsonObject jsonObj = new JsonObject();
		jsonObj.addProperty("message", username + " Profile Has been successfully updated.");
		return jsonObj.toString();
	}

	@RequestMapping(value = "/nba", method = RequestMethod.GET)
	public String nba(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.nba_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<NBA> nbaList = nbaService.getAll();
		model.addAttribute("nbaList", nbaList);
		model.addAttribute("criteriaList", Criteria.values());
		return "admin/nba";
	}

	@RequestMapping(value = "/nba", method = RequestMethod.POST)
	public String nbaPost(@RequestParam("page") PageType page, @RequestParam("criteria") Criteria criteria,
			@RequestParam("heading") String heading, @RequestParam("link") String link, ModelMap model) {
		NBA nba = new NBA();
		nba.setPage(page);
		nba.setCriteria(criteria);
		nba.setHeading(heading);
		nba.setLink(link);
		nba.setPage(page);
		nbaService.saveOrUpdate(nba);
		return "redirect:/admin/nba";
	}

	@RequestMapping(value = "/nba/delete/{id}", method = RequestMethod.POST)
	public String nbaDelete(@PathVariable(value = "id") String id) {
		NBA nba = nbaService.getById(id);
		nbaService.delete(nba);
		return "redirect:/admin/nba";
	}

	@RequestMapping(value = "/nba_previsit", method = RequestMethod.GET)
	public String nbaPrevisit(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.nba_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<NBAPrevisit> nbaPrevisitList = nbaPrevisitService.getAll();
		model.addAttribute("nbaPrevisitList", nbaPrevisitList);
		return "admin/nba_previsit";
	}

	@RequestMapping(value = "/nba_previsit", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String nbaPrevisitPost(@RequestParam(value = "page", required = false) PageType page,
			@RequestParam(value = "heading", required = false) String heading,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam("rank") Integer rank, @RequestParam(value = "link", required = false) String link,
			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam("parentId") String parentId, ModelMap model) {
		NBAPrevisit nbaPrevisit = new NBAPrevisit();
		nbaPrevisit.setPage(page);
		nbaPrevisit.setHeading(heading);
		nbaPrevisit.setDescription(description);
		if (link != null && !link.isEmpty()) {
			nbaPrevisit.setLink(link);
		} else if (file != null) {
			String fileName = file.getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index);

			long timeNow = Calendar.getInstance().getTimeInMillis();

			String linkFile = String.format("nba_%d%s", timeNow, type);
			String fileImageURL = AppUtils.getImageLocation() + linkFile;
			String linkURL = "/images/" + linkFile;
			try {
				AppUtils.writeToFile(file.getInputStream(), fileImageURL);
			} catch (IOException e) {
			}
			nbaPrevisit.setLink(linkURL);
		} else {
			return "redirect:/admin/nba_previsit";
		}
		nbaPrevisit.setRank(rank);
		nbaPrevisitService.saveOrUpdate(nbaPrevisit);
		if (parentId != null) {
			NBAPrevisitParentData nbaPrevisitParentData = new NBAPrevisitParentData();
			nbaPrevisitParentData.setChildId(nbaPrevisit.getId());
			nbaPrevisitParentData.setParentId(parentId);
			nbaPrevisitService.saveOrUpdate(nbaPrevisitParentData);
		}
		return "redirect:/admin/nba_previsit";
	}

	@RequestMapping(value = "/nba_previsit/delete/{id}", method = RequestMethod.POST)
	public String nbaPrevisitPostDelete(@PathVariable(value = "id") String id) {
		NBAPrevisit nbaPrevisit = nbaPrevisitService.getById(id);
		nbaPrevisitService.delete(nbaPrevisit);
		return "redirect:/admin/nba_previsit";
	}

	@RequestMapping(value = "/naac", method = RequestMethod.GET)
	public String naac(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.naac_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<Integer> yearList = new ArrayList<>();
		for(int year=2024; year>=2018; year--) {
			yearList.add(year);
		}
		List<NAAC> naacList = naacService.getAllByPage(PageType.naac_page);
		model.addAttribute("yearList", yearList);		
		model.addAttribute("naacList", naacList);
		model.addAttribute("criteriaList", Criteria.values());
		return "admin/naac";
	}

	@RequestMapping(value = "/naac", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String naacPost(@RequestParam("year") String yearStr, @RequestParam("criteria") Criteria criteria,
			@RequestParam("heading") String heading, @RequestParam("file") MultipartFile file, ModelMap model) {
		NAAC naac = new NAAC();
		naac.setPage(PageType.naac_page);
		naac.setYear(Integer.valueOf(yearStr));
		naac.setCriteria(criteria);
		naac.setHeading(heading);

		if (file != null) {
			String fileName = file.getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index);

			long timeNow = Calendar.getInstance().getTimeInMillis();

			String link = String.format("naac_%s_%s_%d%s", yearStr, criteria, timeNow, type);
			String fileImageURL = AppUtils.getImageLocation() + link;
			String linkURL = "/images/" + link;
			try {
				AppUtils.writeToFile(file.getInputStream(), fileImageURL);
			} catch (IOException e) {
			}
			naac.setLink(linkURL);
		}
		naacService.saveOrUpdate(naac);
		return "redirect:/admin/naac";
	}
	
	@RequestMapping(value = "/naac_cycle2_aqar", method = RequestMethod.GET)
	public String naac_cycle2_aqar(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.naac_cycle2_aqar_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<Integer> yearList = new ArrayList<>();
		for(int year=2025; year>=2023; year--) {
			yearList.add(year);
		}
		List<NAAC> naacList = naacService.getAllByPage(PageType.naac_cycle2_aqar_page);
		model.addAttribute("yearList", yearList);		
		model.addAttribute("naacList", naacList);
		model.addAttribute("criteriaList", Criteria.values());
		return "admin/naac_cycle2_aqar";
	}

	@RequestMapping(value = "/naac_cycle2_aqar", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String naac_cycle2_aqarPost(@RequestParam("year") String yearStr, @RequestParam("criteria") Criteria criteria,
			@RequestParam("heading") String heading, @RequestParam("file") MultipartFile file, ModelMap model) {
		NAAC naac = new NAAC();
		naac.setPage(PageType.naac_cycle2_aqar_page);
		naac.setYear(Integer.valueOf(yearStr));
		naac.setCriteria(criteria);
		naac.setHeading(heading);

		if (file != null) {
			String fileName = file.getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index);

			long timeNow = Calendar.getInstance().getTimeInMillis();

			String link = String.format("naac_%s_%s_%d%s", yearStr, criteria, timeNow, type);
			String fileImageURL = AppUtils.getImageLocation() + link;
			String linkURL = "/images/" + link;
			try {
				AppUtils.writeToFile(file.getInputStream(), fileImageURL);
			} catch (IOException e) {
			}
			naac.setLink(linkURL);
		}
		naacService.saveOrUpdate(naac);
		return "redirect:/admin/naac_cycle2_aqar";
	}
	
	@RequestMapping(value = "/naac_cycle2_ssr", method = RequestMethod.GET)
	public String naac_cycle2(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.naac_cycle2_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<Integer> yearList = new ArrayList<>();
		for(int year=2024; year>=2018; year--) {
			yearList.add(year);
		}
		List<NAAC> naacList = naacService.getAllByPage(PageType.naac_cycle2_page);
		model.addAttribute("yearList", yearList);		
		model.addAttribute("naacList", naacList);
		model.addAttribute("criteriaList", Criteria.values());
		return "admin/naac_cycle2_ssr";
	}

	@RequestMapping(value = "/naac_cycle2_ssr", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String naac_cycle2Post(@RequestParam("year") String yearStr, @RequestParam("criteria") Criteria criteria,
			@RequestParam("heading") String heading, @RequestParam("file") MultipartFile file, ModelMap model) {
		NAAC naac = new NAAC();
		naac.setPage(PageType.naac_cycle2_page);
		naac.setYear(Integer.valueOf(yearStr));
		naac.setCriteria(criteria);
		naac.setHeading(heading);

		if (file != null) {
			String fileName = file.getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index);

			long timeNow = Calendar.getInstance().getTimeInMillis();

			String link = String.format("naac_%s_%s_%d%s", yearStr, criteria, timeNow, type);
			String fileImageURL = AppUtils.getImageLocation() + link;
			String linkURL = "/images/" + link;
			try {
				AppUtils.writeToFile(file.getInputStream(), fileImageURL);
			} catch (IOException e) {
			}
			naac.setLink(linkURL);
		}
		naacService.saveOrUpdate(naac);
		return "redirect:/admin/naac_cycle2";
	}
	
	@RequestMapping(value = "/naac_cycle2_dvv_clarification", method = RequestMethod.GET)
	public String naac_cycle2_dvv_clarification(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.naac_cycle2_dvv_clarification_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<NAAC> naacList = naacService.getAllByPage(PageType.naac_cycle2_dvv_clarification_page);
		model.addAttribute("naacList", naacList);
		model.addAttribute("criteriaList", Criteria.values());
		return "admin/naac_cycle2_dvv_clarification";
	}

	@RequestMapping(value = "/naac_cycle2_dvv_clarification", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String naac_cycle2_dvv_clarificationPost(@RequestParam("year") String yearStr, @RequestParam("criteria") Criteria criteria,
			@RequestParam("heading") String heading, @RequestParam("file") MultipartFile file, ModelMap model) {
		NAAC naac = new NAAC();
		naac.setPage(PageType.naac_cycle2_dvv_clarification_page);
		naac.setYear(Integer.valueOf(yearStr));
		naac.setCriteria(criteria);
		naac.setHeading(heading);

		if (file != null) {
			String fileName = file.getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index);

			long timeNow = Calendar.getInstance().getTimeInMillis();

			String link = String.format("naac_%s_%s_%d%s", yearStr, criteria, timeNow, type);
			String fileImageURL = AppUtils.getImageLocation() + link;
			String linkURL = "/images/" + link;
			try {
				AppUtils.writeToFile(file.getInputStream(), fileImageURL);
			} catch (IOException e) {
			}
			naac.setLink(linkURL);
		}
		naacService.saveOrUpdate(naac);
		return "redirect:/admin/naac_cycle2_dvv_clarification";
	}
	
	@RequestMapping(value = "/naac_cycle2_dvv_clarification_extended_profile", method = RequestMethod.GET)
	public String naac_cycle2_dvv_clarification_extended_profile(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.naac_cycle2_dvv_clarification_extended_profile_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<NAAC> naacList = naacService.getAllByPage(PageType.naac_cycle2_dvv_clarification_extended_profile_page);
		model.addAttribute("naacList", naacList);
		return "admin/naac_cycle2_dvv_clarification_extended_profile";
	}
	
	@RequestMapping(value = "/naac_cycle2_dvv_clarification_extended_profile", method = RequestMethod.POST, consumes = "multipart/form-data")
	public String naac_cycle2_dvv_clarification_extended_profilePost(@RequestParam("year") String yearStr, @RequestParam("heading") String heading, @RequestParam("file") MultipartFile file, ModelMap model) {
		NAAC naac = new NAAC();
		naac.setPage(PageType.naac_cycle2_dvv_clarification_extended_profile_page);
		naac.setYear(Integer.valueOf(yearStr));
		naac.setHeading(heading);

		if (file != null) {
			String fileName = file.getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index);

			long timeNow = Calendar.getInstance().getTimeInMillis();

			String link = String.format("naac_%s_%d%s", yearStr, timeNow, type);
			String fileImageURL = AppUtils.getImageLocation() + link;
			String linkURL = "/images/" + link;
			try {
				AppUtils.writeToFile(file.getInputStream(), fileImageURL);
			} catch (IOException e) {
			}
			naac.setLink(linkURL);
		}
		naacService.saveOrUpdate(naac);
		return "redirect:/admin/naac_cycle2_dvv_clarification_extended_profile";
	}
	
	

	@RequestMapping(value = "/naac/delete/{id}", method = RequestMethod.POST)
	public String naacDelete(@PathVariable(value = "id") String id, @RequestParam("page") String page) {
		NAAC naac = naacService.getById(id);
		naacService.delete(naac);
		if(page != null) {
			return "redirect:/admin/"+page;
		}else {
			return "redirect:/admin/naac";
		}
	}

	@RequestMapping(value = "/onlineadmissions", method = RequestMethod.GET)
	public String onlineAdmissions(ModelMap model, @RequestParam(value = "page", required = false) String page) {

		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.online_admission_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		if (StringUtils.isEmpty(page)) {
			page = "1";
		}
		Integer pageInt = Integer.parseInt(page);
		if (pageInt < 1) {
			pageInt = 1;
		}

		List<OnlineAdmission> onlineAdmissionList = onlineAdmissionService.findByPage(page);
		model.addAttribute("onlineAdmissionList", onlineAdmissionList);
		if (pageInt > 1) {
			model.addAttribute("hasPrev", true);
			model.addAttribute("prevPage", pageInt - 1);
		}
		if (onlineAdmissionList.size() == contactUsService.getPAGE_SIZE()) {
			model.addAttribute("hasNext", true);
			model.addAttribute("nextPage", pageInt + 1);
		}
		return "admin/onlineadmissions";
	}

	@RequestMapping(value = "/institutionalGovernance", method = RequestMethod.GET)
	public String institutionalGovernanceGet(ModelMap model) {
		boolean permissionGranted = SessionUtil
				.isLoggedInUserAuthenticatedForPage(PageType.institutional_governance_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		List<InstitutionalGovernance> institutionalGovernanceList = institutionalGovernanceService.getAll();
		model.addAttribute("institutionalGovernanceList", institutionalGovernanceList);
		return "admin/institutionalGovernance";
	}

	@RequestMapping(value = "/institutionalGovernance", method = RequestMethod.POST)
	public String institutionalGovernancePost(@RequestParam(value = "heading") String heading,
			@RequestParam("file") MultipartFile file, ModelMap model) {
		InstitutionalGovernance institutionalGovernance = new InstitutionalGovernance();
		institutionalGovernance.setHeading(heading);
		if (file != null) {
			String fileName = file.getOriginalFilename();
			int index = fileName.lastIndexOf(".");
			String type = fileName.substring(index);

			long timeNow = Calendar.getInstance().getTimeInMillis();

			String link = String.format("institutional_governance_%d%s", timeNow, type);
			String fileImageURL = AppUtils.getImageLocation() + link;
			String linkURL = "/images/" + link;
			try {
				AppUtils.writeToFile(file.getInputStream(), fileImageURL);
			} catch (IOException e) {
			}
			institutionalGovernance.setLink(linkURL);
		}
		institutionalGovernanceService.saveOrUpdate(institutionalGovernance);
		return "redirect:/admin/institutionalGovernance";
	}

	@RequestMapping(value = "/institutionalGovernance/delete/{id}", method = RequestMethod.POST)
	public String institutionalGovernanceDelete(@PathVariable(value = "id") String id) {
		InstitutionalGovernance institutionalGovernance = institutionalGovernanceService.getById(id);
		institutionalGovernanceService.delete(institutionalGovernance);
		return "redirect:/admin/institutionalGovernance";
	}

	@RequestMapping(value = "/csicb_dept", method = RequestMethod.GET)
	public String csicb_dept(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.csicb_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.csicb_page);

		List<Event> csicbPageSliderImageList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_slider_images);
		model.addAttribute("csicbPageSliderImageList", csicbPageSliderImageList);
		model.addAttribute("csicbPageSliderImageFieldName", FieldNameType.csicb_page_slider_images);

		List<Event> csicbPageLatestEventsList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_latest_events);
		model.addAttribute("csicbPageLatestEventsList", csicbPageLatestEventsList);
		model.addAttribute("csicbLatestEventsFieldName", FieldNameType.csicb_page_latest_events);

		List<Event> csicbPageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_upcoming_events);
		model.addAttribute("csicbPageUpcomingEventsList", csicbPageUpcomingEventsList);
		model.addAttribute("csicbUpcomingEventsFieldName", FieldNameType.csicb_page_upcoming_events);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> csicbEventList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_events);
		model.addAttribute("csicbEventList", csicbEventList);
		model.addAttribute("csicbEventFieldName", FieldNameType.csicb_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.csicb_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.csicb_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.csicb_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.csicb_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.csicb_page_gallery);

		List<Event> csicbDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_department_achiever_list);
		model.addAttribute("csicbDepartmentAchieversList", csicbDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.csicb_department_achiever_list);

		List<Event> csicbFcdStudentsList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_fcd_students);
		model.addAttribute("csicbFcdStudentsList", csicbFcdStudentsList);
		model.addAttribute("csicbFcdStudentsFieldName", FieldNameType.csicb_page_fcd_students);

		List<Event> csicbcontentbeyondsyllabusList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_content_beyond_syllabus);
		model.addAttribute("csicbcontentbeyondsyllabusList", csicbcontentbeyondsyllabusList);
		model.addAttribute("csicbcontentbeyondsyllabusFieldName", FieldNameType.csicb_page_content_beyond_syllabus);

		List<Event> csicbClassTimeTableList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_class_time_table);
		model.addAttribute("csicbClassTimeTableList", csicbClassTimeTableList);
		model.addAttribute("csicbClassTimeTableFieldName", FieldNameType.csicb_page_class_time_table);

		List<Event> csicbTestTimeTableList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_test_time_table);
		model.addAttribute("csicbTestTimeTableList", csicbTestTimeTableList);
		model.addAttribute("csicbTestTimeTableFieldName", FieldNameType.csicb_page_test_time_table);

		List<Event> csicbCalanderList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_calander);
		model.addAttribute("csicbCalanderList", csicbCalanderList);
		model.addAttribute("csicbCalanderFieldName", FieldNameType.csicb_page_calander);

		List<Event> csicbNewsLetterList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_newsletter);
		model.addAttribute("csicbNewsLetterList", csicbNewsLetterList);
		model.addAttribute("csicbNewsLetterFieldName", FieldNameType.csicb_page_newsletter);

		List<Event> csicbUgSyllabusList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_ug_syllabus);
		model.addAttribute("csicbUgSyllabusList", csicbUgSyllabusList);
		model.addAttribute("csicbUgSyllabusFieldName", FieldNameType.csicb_page_ug_syllabus);

		List<Event> csicbLibraryBooksList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_library_books_collection);
		model.addAttribute("csicbLibraryBooksList", csicbLibraryBooksList);
		model.addAttribute("csicbLibraryBooksFieldName", FieldNameType.csicb_library_books_collection);

		List<Event> csicbPlacementList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_placement);
		model.addAttribute("csicbPlacementList", csicbPlacementList);
		model.addAttribute("csicbPlacementFieldName", FieldNameType.csicb_page_placement);

		List<Faculty> csicbLabList = facultyService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_labs);
		model.addAttribute("csicbLabList", csicbLabList);
		model.addAttribute("csicbLabFieldName", FieldNameType.csicb_page_labs);

		List<Event> csicbEresourcesList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_eresources);
		model.addAttribute("csicbEresourcesList", csicbEresourcesList);
		model.addAttribute("csicbEresourcesFieldName", FieldNameType.csicb_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.csicb_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.csicb_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.csicb_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.csicb_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> csicbResearchList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_research);
		model.addAttribute("csicbResearchList", csicbResearchList);
		model.addAttribute("csicbResearchFieldName", FieldNameType.csicb_page_research);

		List<Event> csicbSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_sponsored_projects);
		model.addAttribute("csicbSponsoredProjectsList", csicbSponsoredProjectsList);
		model.addAttribute("csicbSponsoredProjectsFieldName", FieldNameType.csicb_page_sponsored_projects);

		List<Event> csicbPhdPursuingList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_phd_pursuing);
		model.addAttribute("csicbPhdPursuingList", csicbPhdPursuingList);
		model.addAttribute("csicbPhdPursuingFieldName", FieldNameType.csicb_page_phd_pursuing);

		List<Event> csicbPhdAwardeesList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_phd_awardees);
		model.addAttribute("csicbPhdAwardeesList", csicbPhdAwardeesList);
		model.addAttribute("csicbPhdAwardeesFieldName", FieldNameType.csicb_page_phd_awardees);

		List<Event> csicbInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_instructional_materials);
		model.addAttribute("csicbInstructionalMaterialsList", csicbInstructionalMaterialsList);
		model.addAttribute("csicbInstructionalMaterialsFieldName", FieldNameType.csicb_instructional_materials);

		List<Event> csicbPedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_pedagogical_activities);
		model.addAttribute("csicbPedagogicalActivitiesList", csicbPedagogicalActivitiesList);
		model.addAttribute("csicbPedagogicalActivitiesFieldName", FieldNameType.csicb_pedagogical_activities);

		List<Event> csicbPedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_pedagogy_review_form);
		model.addAttribute("csicbPedagogyReviewFormList", csicbPedagogyReviewFormList);
		model.addAttribute("csicbPedagogyReviewFormFieldName", FieldNameType.csicb_pedagogy_review_form);

		List<Event> csicbLabManualList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_lab_manual_form);
		model.addAttribute("csicbLabManualList", csicbLabManualList);
		model.addAttribute("csicbLabManualFieldName", FieldNameType.csicb_lab_manual_form);

		List<Event> csicbMouSignedList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_Mou_Signed);
		model.addAttribute("csicbMouSignedList", csicbMouSignedList);
		model.addAttribute("csicbMouSignedFieldName", FieldNameType.csicb_page_Mou_Signed);

		List<Event> csicbAlumniAssociationList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_alumni_association);
		model.addAttribute("csicbAlumniAssociationList", csicbAlumniAssociationList);
		model.addAttribute("csicbAlumniAssociationFieldName", FieldNameType.csicb_page_alumni_association);

		List<Event> csicbProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_professional_bodies);
		model.addAttribute("csicbProfessionalBodiesList", csicbProfessionalBodiesList);
		model.addAttribute("csicbProfessionalBodiesFieldName", FieldNameType.csicb_page_professional_bodies);

		List<Event> csicbProfessionalLinksList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_professional_links);
		model.addAttribute("csicbProfessionalLinksList", csicbProfessionalLinksList);
		model.addAttribute("csicbProfessionalLinksFieldName", FieldNameType.csicb_page_professional_links);

		List<Event> csicbHigherEducationList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_higher_education);
		model.addAttribute("csicbHigherEducationList", csicbHigherEducationList);
		model.addAttribute("csicbHigherEducationFieldName", FieldNameType.csicb_page_higher_education);

		List<Event> csicbClubActivitiesList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_club_activities);
		model.addAttribute("csicbClubActivitiesList", csicbClubActivitiesList);
		model.addAttribute("csicbClubActivitiesFieldName", FieldNameType.csicb_page_club_activities);

		List<Event> csicbClubExternalLinksList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_club_external_links);
		model.addAttribute("csicbClubExternalLinksList", csicbClubExternalLinksList);
		model.addAttribute("csicbClubExternalLinksFieldName", FieldNameType.csicb_page_club_external_links);

		List<Event> csicbInternshipList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_internship);
		model.addAttribute("csicbInternshipList", csicbInternshipList);
		model.addAttribute("csicbInternshipFieldName", FieldNameType.csicb_page_internship);

		List<Event> csicbProjectsList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_projects);
		model.addAttribute("csicbProjectsList", csicbProjectsList);
		model.addAttribute("csicbProjectsFieldName", FieldNameType.csicb_page_projects);

		List<Event> csicbMiniProjectsList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_mini_projects);
		model.addAttribute("csicbMiniProjectsList", csicbMiniProjectsList);
		model.addAttribute("csicbMiniProjectsFieldName", FieldNameType.csicb_page_mini_projects);

		List<Event> csicbSocialActivitiesList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_social_activities);
		model.addAttribute("csicbSocialActivitiesList", csicbSocialActivitiesList);
		model.addAttribute("csicbSocialActivitiesFieldName", FieldNameType.csicb_page_social_activities);

		List<Faculty> csicbIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_industrial_visit);
		model.addAttribute("csicbIndustrialVisitList", csicbIndustrialVisitList);
		model.addAttribute("csicbIndustrialVisitFieldName", FieldNameType.csicb_page_industrial_visit);

		List<Faculty> csicbProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_project_exhibition);
		model.addAttribute("csicbProjectExhibitionList", csicbProjectExhibitionList);
		model.addAttribute("csicbProjectExhibitionFieldName", FieldNameType.csicb_page_project_exhibition);

		List<Event> csicbFdpList = eventService.getByPageAndFieldName(PageType.csicb_page,
				FieldNameType.csicb_page_fdp);
		model.addAttribute("csicbFdpList", csicbFdpList);
		model.addAttribute("csicbFdpFieldName", FieldNameType.csicb_page_fdp);

		return "admin/csicb_dept";
	}

	@RequestMapping(value = "/cce_dept", method = RequestMethod.GET)
	public String cce_dept(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.cce_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.cce_page);

		List<Event> ccePageSliderImageList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_slider_images);
		model.addAttribute("ccePageSliderImageList", ccePageSliderImageList);
		model.addAttribute("ccePageSliderImageFieldName", FieldNameType.cce_page_slider_images);

		List<Event> ccePageLatestEventsList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_latest_events);
		model.addAttribute("ccePageLatestEventsList", ccePageLatestEventsList);
		model.addAttribute("cceLatestEventsFieldName", FieldNameType.cce_page_latest_events);

		List<Event> ccePageUpcomingEventsList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_upcoming_events);
		model.addAttribute("ccePageUpcomingEventsList", ccePageUpcomingEventsList);
		model.addAttribute("cceUpcomingEventsFieldName", FieldNameType.cce_page_upcoming_events);

		List<Data> scrollingTextBottomList = dataService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.scrolling_text_bottom);
		model.addAttribute("scrollingTextBottomList", scrollingTextBottomList);
		model.addAttribute("scrollingTextBottomFieldName", FieldNameType.scrolling_text_bottom);

		List<Event> cceEventList = eventService.getByPageAndFieldName(PageType.cce_page, FieldNameType.cce_events);
		model.addAttribute("cceEventList", cceEventList);
		model.addAttribute("cceEventFieldName", FieldNameType.cce_events);

		List<Event> testimonialList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_testimonials);
		model.addAttribute("testimonialList", testimonialList);
		model.addAttribute("testimonialFieldName", FieldNameType.cce_page_testimonials);

		List<Event> achieverList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_achiever);
		model.addAttribute("achieverList", achieverList);
		model.addAttribute("achieverFieldName", FieldNameType.cce_page_achiever);

		List<Faculty> facultyList = facultyService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_faculty);
		model.addAttribute("facultyList", facultyList);
		model.addAttribute("facultyFieldName", FieldNameType.cce_page_faculty);

		List<Faculty> supportingStaffFacultyList = facultyService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_supporting_faculty);
		model.addAttribute("supportingStaffFacultyList", supportingStaffFacultyList);
		model.addAttribute("supportingStaffFacultyFieldName", FieldNameType.cce_page_supporting_faculty);

		List<Event> galleryList = eventService.getByPageAndFieldName(PageType.cce_page, FieldNameType.cce_page_gallery);
		model.addAttribute("galleryList", galleryList);
		model.addAttribute("galleryFieldName", FieldNameType.cce_page_gallery);

		List<Event> cceDepartmentAchieversList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_department_achiever_list);
		model.addAttribute("cceDepartmentAchieversList", cceDepartmentAchieversList);
		model.addAttribute("eventFieldName", FieldNameType.cce_department_achiever_list);

		List<Event> cceFcdStudentsList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_fcd_students);
		model.addAttribute("cceFcdStudentsList", cceFcdStudentsList);
		model.addAttribute("cceFcdStudentsFieldName", FieldNameType.cce_page_fcd_students);

		List<Event> ccecontentbeyondsyllabusList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_content_beyond_syllabus);
		model.addAttribute("ccecontentbeyondsyllabusList", ccecontentbeyondsyllabusList);
		model.addAttribute("ccecontentbeyondsyllabusFieldName", FieldNameType.cce_page_content_beyond_syllabus);

		List<Event> cceClassTimeTableList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_class_time_table);
		model.addAttribute("cceClassTimeTableList", cceClassTimeTableList);
		model.addAttribute("cceClassTimeTableFieldName", FieldNameType.cce_page_class_time_table);

		List<Event> cceTestTimeTableList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_test_time_table);
		model.addAttribute("cceTestTimeTableList", cceTestTimeTableList);
		model.addAttribute("cceTestTimeTableFieldName", FieldNameType.cce_page_test_time_table);

		List<Event> cceCalanderList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_calander);
		model.addAttribute("cceCalanderList", cceCalanderList);
		model.addAttribute("cceCalanderFieldName", FieldNameType.cce_page_calander);

		List<Event> cceNewsLetterList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_newsletter);
		model.addAttribute("cceNewsLetterList", cceNewsLetterList);
		model.addAttribute("cceNewsLetterFieldName", FieldNameType.cce_page_newsletter);

		List<Event> cceUgSyllabusList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_ug_syllabus);
		model.addAttribute("cceUgSyllabusList", cceUgSyllabusList);
		model.addAttribute("cceUgSyllabusFieldName", FieldNameType.cce_page_ug_syllabus);

		List<Event> cceLibraryBooksList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_library_books_collection);
		model.addAttribute("cceLibraryBooksList", cceLibraryBooksList);
		model.addAttribute("cceLibraryBooksFieldName", FieldNameType.cce_library_books_collection);

		List<Event> ccePlacementList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_placement);
		model.addAttribute("ccePlacementList", ccePlacementList);
		model.addAttribute("ccePlacementFieldName", FieldNameType.cce_page_placement);

		List<Faculty> cceLabList = facultyService.getByPageAndFieldName(PageType.cce_page, FieldNameType.cce_page_labs);
		model.addAttribute("cceLabList", cceLabList);
		model.addAttribute("cceLabFieldName", FieldNameType.cce_page_labs);

		List<Event> cceEresourcesList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_eresources);
		model.addAttribute("cceEresourcesList", cceEresourcesList);
		model.addAttribute("cceEresourcesFieldName", FieldNameType.cce_eresources);

		List<FeedbackParent> feedbackParentList = feedbackParentService.findByPage(PageType.cce_page);
		List<FeedbackStudent> feedbackStudentList = feedbackStudentService.findByPage(PageType.cce_page);
		List<FeedbackAlumni> feedbackAlumniList = feedbackAlumniService.findByPage(PageType.cce_page);
		List<FeedbackEmployer> feedbackEmployerList = feedbackEmployerService.findByPage(PageType.cce_page);

		model.addAttribute("feedbackParentList", feedbackParentList);
		model.addAttribute("feedbackStudentList", feedbackStudentList);
		model.addAttribute("feedbackAlumniList", feedbackAlumniList);
		model.addAttribute("feedbackEmployerList", feedbackEmployerList);

		List<Event> cceResearchList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_research);
		model.addAttribute("cceResearchList", cceResearchList);
		model.addAttribute("cceResearchFieldName", FieldNameType.cce_page_research);

		List<Event> cceSponsoredProjectsList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_sponsored_projects);
		model.addAttribute("cceSponsoredProjectsList", cceSponsoredProjectsList);
		model.addAttribute("cceSponsoredProjectsFieldName", FieldNameType.cce_page_sponsored_projects);

		List<Event> ccePhdPursuingList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_phd_pursuing);
		model.addAttribute("ccePhdPursuingList", ccePhdPursuingList);
		model.addAttribute("ccePhdPursuingFieldName", FieldNameType.cce_page_phd_pursuing);

		List<Event> ccePhdAwardeesList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_phd_awardees);
		model.addAttribute("ccePhdAwardeesList", ccePhdAwardeesList);
		model.addAttribute("ccePhdAwardeesFieldName", FieldNameType.cce_page_phd_awardees);

		List<Event> cceInstructionalMaterialsList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_instructional_materials);
		model.addAttribute("cceInstructionalMaterialsList", cceInstructionalMaterialsList);
		model.addAttribute("cceInstructionalMaterialsFieldName", FieldNameType.cce_instructional_materials);

		List<Event> ccePedagogicalActivitiesList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_pedagogical_activities);
		model.addAttribute("ccePedagogicalActivitiesList", ccePedagogicalActivitiesList);
		model.addAttribute("ccePedagogicalActivitiesFieldName", FieldNameType.cce_pedagogical_activities);

		List<Event> ccePedagogyReviewFormList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_pedagogy_review_form);
		model.addAttribute("ccePedagogyReviewFormList", ccePedagogyReviewFormList);
		model.addAttribute("ccePedagogyReviewFormFieldName", FieldNameType.cce_pedagogy_review_form);

		List<Event> cceLabManualList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_lab_manual_form);
		model.addAttribute("cceLabManualList", cceLabManualList);
		model.addAttribute("cceLabManualFieldName", FieldNameType.cce_lab_manual_form);

		List<Event> cceMouSignedList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_Mou_Signed);
		model.addAttribute("cceMouSignedList", cceMouSignedList);
		model.addAttribute("cceMouSignedFieldName", FieldNameType.cce_page_Mou_Signed);

		List<Event> cceAlumniAssociationList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_alumni_association);
		model.addAttribute("cceAlumniAssociationList", cceAlumniAssociationList);
		model.addAttribute("cceAlumniAssociationFieldName", FieldNameType.cce_page_alumni_association);

		List<Event> cceProfessionalBodiesList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_professional_bodies);
		model.addAttribute("cceProfessionalBodiesList", cceProfessionalBodiesList);
		model.addAttribute("cceProfessionalBodiesFieldName", FieldNameType.cce_page_professional_bodies);

		List<Event> cceProfessionalLinksList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_professional_links);
		model.addAttribute("cceProfessionalLinksList", cceProfessionalLinksList);
		model.addAttribute("cceProfessionalLinksFieldName", FieldNameType.cce_page_professional_links);

		List<Event> cceHigherEducationList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_higher_education);
		model.addAttribute("cceHigherEducationList", cceHigherEducationList);
		model.addAttribute("cceHigherEducationFieldName", FieldNameType.cce_page_higher_education);

		List<Event> cceClubActivitiesList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_club_activities);
		model.addAttribute("cceClubActivitiesList", cceClubActivitiesList);
		model.addAttribute("cceClubActivitiesFieldName", FieldNameType.cce_page_club_activities);

		List<Event> cceClubExternalLinksList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_club_external_links);
		model.addAttribute("cceClubExternalLinksList", cceClubExternalLinksList);
		model.addAttribute("cceClubExternalLinksFieldName", FieldNameType.cce_page_club_external_links);

		List<Event> cceInternshipList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_internship);
		model.addAttribute("cceInternshipList", cceInternshipList);
		model.addAttribute("cceInternshipFieldName", FieldNameType.cce_page_internship);

		List<Event> cceProjectsList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_projects);
		model.addAttribute("cceProjectsList", cceProjectsList);
		model.addAttribute("cceProjectsFieldName", FieldNameType.cce_page_projects);

		List<Event> cceMiniProjectsList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_mini_projects);
		model.addAttribute("cceMiniProjectsList", cceMiniProjectsList);
		model.addAttribute("cceMiniProjectsFieldName", FieldNameType.cce_page_mini_projects);

		List<Event> cceSocialActivitiesList = eventService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_social_activities);
		model.addAttribute("cceSocialActivitiesList", cceSocialActivitiesList);
		model.addAttribute("cceSocialActivitiesFieldName", FieldNameType.cce_page_social_activities);

		List<Faculty> cceIndustrialVisitList = facultyService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_industrial_visit);
		model.addAttribute("cceIndustrialVisitList", cceIndustrialVisitList);
		model.addAttribute("cceIndustrialVisitFieldName", FieldNameType.cce_page_industrial_visit);

		List<Faculty> cceProjectExhibitionList = facultyService.getByPageAndFieldName(PageType.cce_page,
				FieldNameType.cce_page_project_exhibition);
		model.addAttribute("cceProjectExhibitionList", cceProjectExhibitionList);
		model.addAttribute("cceProjectExhibitionFieldName", FieldNameType.cce_page_project_exhibition);

		List<Event> cceFdpList = eventService.getByPageAndFieldName(PageType.cce_page, FieldNameType.cce_page_fdp);
		model.addAttribute("cceFdpList", cceFdpList);
		model.addAttribute("cceFdpFieldName", FieldNameType.cce_page_fdp);

		return "admin/cce_dept";
	}
	
	@RequestMapping(value = "/placement_drive", method = RequestMethod.GET)
	public String placement_drive(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.placement_drive_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}

		model.addAttribute("pageName", PageType.placement_drive_page);
		List<Event> placementDriveList = eventService.getByPageAndFieldName(PageType.placement_drive_page, FieldNameType.placement_drives);
		model.addAttribute("placementDriveList", placementDriveList);
		model.addAttribute("placementDriveFieldName", FieldNameType.placement_drives);
		return "admin/placement_drive";
	}
	
	@RequestMapping(value = "/placed_students", method = RequestMethod.GET)
	public String placed_students(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.placed_students_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		model.addAttribute("pageName", PageType.placed_students_page);
		List<Event> placedStudentsList = eventService.getActiveByPageAndFieldName(PageType.placed_students_page,
				FieldNameType.placed_students);
		model.addAttribute("placedStudentsList", placedStudentsList);
		model.addAttribute("placedStudentsFieldName", FieldNameType.placed_students);
		return "admin/placed_students";
	}
	
	@RequestMapping(value = "/placement_statistics", method = RequestMethod.GET)
	public String placement_statistics(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.placement_statistics_page);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		model.addAttribute("pageName", PageType.placement_statistics_page);
		List<Event> placementStatisticsList = eventService.getActiveByPageAndFieldName(PageType.placement_statistics_page,
				FieldNameType.placement_statistics);
		model.addAttribute("placementStatisticsList", placementStatisticsList);
		model.addAttribute("placementStatisticsFieldName", FieldNameType.placement_statistics);
		return "admin/placement_statistics";
	}
	
	@RequestMapping(value = "/placement_upcoming_events_latest_news", method = RequestMethod.GET)
	public String upcoming_events_latest_news(ModelMap model) {
		boolean permissionGranted = SessionUtil.isLoggedInUserAuthenticatedForPage(PageType.placement_upcoming_events_latest_news);
		if (permissionGranted == false) {
			// User does not have permission for this page
			return "redirect:/admin/no-access";
		}
		model.addAttribute("pageName", PageType.placement_upcoming_events_latest_news);
		
		
		List<Event> placementLatestEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_upcoming_events_latest_news,
				FieldNameType.placement_page_latest_events);
		model.addAttribute("placementLatestEventsList", placementLatestEventsList);
		model.addAttribute("placementLatestEventsFieldName", FieldNameType.placement_page_latest_events);
		
		List<Event> placementUpcomingEventsList = eventService.getActiveByPageAndFieldName(PageType.placement_upcoming_events_latest_news,
				FieldNameType.placement_page_upcoming_events);
		model.addAttribute("placementUpcomingEventsList", placementUpcomingEventsList);
		model.addAttribute("placementUpcomingEventsFieldName", FieldNameType.placement_page_upcoming_events);
		
		
		return "admin/placement_upcoming_events_latest_news";
	}
	
	@RequestMapping(value = "/approvals", method = RequestMethod.GET)
	public String approvals(ModelMap model) {
		model.addAttribute("pageName", PageType.approvals_page);

		List<Event> approvalsMouSignedList = eventService.getByPageAndFieldName(PageType.approvals_page,
				FieldNameType.approvals_page_mou_signed);
		model.addAttribute("approvalsMouSignedList", approvalsMouSignedList);
		model.addAttribute("approvalsMouSignedFieldName", FieldNameType.approvals_page_mou_signed);

		return "admin/approvals";
	}
	
	@RequestMapping(value = "/anti_ragging_committee", method = RequestMethod.GET)
	public String anti_ragging_committee(ModelMap model) {
		model.addAttribute("pageName", PageType.anti_ragging_committee_page);
		
		List<Event> reportsList = eventService.getActiveByPageAndFieldName(PageType.anti_ragging_committee_page,
				FieldNameType.anti_ragging_committee_events);
		model.addAttribute("reportsList", reportsList);
		model.addAttribute("reportsFieldName", FieldNameType.anti_ragging_committee_events);

		return "admin/anti_ragging_committee";
	}
	
	@RequestMapping(value = "/anti_sexual_harassment_committee", method = RequestMethod.GET)
	public String anti_sexual_harassment_committee(ModelMap model) {
		model.addAttribute("pageName", PageType.anti_sexual_harassment_committee_page);
		
		List<Event> reportsList = eventService.getActiveByPageAndFieldName(PageType.anti_sexual_harassment_committee_page,
				FieldNameType.anti_sexual_harassment_committee_events);
		model.addAttribute("reportsList", reportsList);
		model.addAttribute("reportsFieldName", FieldNameType.anti_sexual_harassment_committee_events);

		return "admin/anti_sexual_harassment_committee";
	}
}
