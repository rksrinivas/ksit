/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.controllers;

import in.ksit.entity.FeedbackAlumni;
import in.ksit.entity.FeedbackEmployer;
import in.ksit.entity.FeedbackParent;
import in.ksit.entity.FeedbackStudent;
import in.ksit.service.FeedbackAlumniService;
import in.ksit.service.FeedbackEmployerService;
import in.ksit.service.FeedbackParentService;
import in.ksit.service.FeedbackStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
public class FeedbackAdminController {

    @Autowired private FeedbackParentService feedbackParentService;

    @Autowired private FeedbackStudentService feedbackStudentService;

    @Autowired private FeedbackAlumniService feedbackAlumniService;

    @Autowired private FeedbackEmployerService feedbackEmployerService;

    @RequestMapping(value = "/feedbackParent/{id}", method = RequestMethod.GET)
    public String feedbackParent(@PathVariable(value = "id") String id, ModelMap model) {
        FeedbackParent feedbackParent = feedbackParentService.getById(id);
        feedbackParentService.addDataToModel(model);
        model.addAttribute("feedbackParent", feedbackParent);
        return "admin/feedback_parent";
    }

    @RequestMapping(value = "/feedbackStudent/{id}", method = RequestMethod.GET)
    public String feedbackStudent(@PathVariable(value = "id") String id, ModelMap model) {
        FeedbackStudent feedbackStudent = feedbackStudentService.getById(id);
        feedbackStudentService.addDataToModel(model);
        model.addAttribute("feedbackStudent", feedbackStudent);
        return "admin/feedback_student";
    }

    @RequestMapping(value = "/feedbackEmployer/{id}", method = RequestMethod.GET)
    public String feedbackEmployer(@PathVariable(value = "id") String id, ModelMap model) {
        FeedbackEmployer feedbackEmployer = feedbackEmployerService.getById(id);
        feedbackEmployerService.addDataToModel(model);
        model.addAttribute("feedbackEmployer", feedbackEmployer);
        return "admin/feedback_employer";
    }

    @RequestMapping(value = "/feedbackAlumni/{id}", method = RequestMethod.GET)
    public String feedbackAlumni(@PathVariable(value = "id") String id, ModelMap model) {
        FeedbackAlumni feedbackAlumni = feedbackAlumniService.getById(id);
        feedbackAlumniService.addDataToModel(model);
        model.addAttribute("feedbackAlumni", feedbackAlumni);
        return "admin/feedback_alumni";
    }

    @RequestMapping(value = "/feedbackParent/delete/{id}", method = RequestMethod.POST)
    public String deleteFeedbackParent(@PathVariable(value = "id") String id) {
        FeedbackParent feedbackParent = feedbackParentService.getById(id);
        feedbackParentService.delete(feedbackParent);
        return "redirect:/admin/index";
    }

    @RequestMapping(value = "/feedbackStudent/delete/{id}", method = RequestMethod.POST)
    public String deleteFeedbackStudent(@PathVariable(value = "id") String id) {
        FeedbackStudent feedbackStudent = feedbackStudentService.getById(id);
        feedbackStudentService.delete(feedbackStudent);
        return "redirect:/admin/index";
    }

    @RequestMapping(value = "/feedbackEmployer/delete/{id}", method = RequestMethod.POST)
    public String deleteFeedbackEmployer(@PathVariable(value = "id") String id) {
        FeedbackEmployer feedbackEmployer = feedbackEmployerService.getById(id);
        feedbackEmployerService.delete(feedbackEmployer);
        return "redirect:/admin/index";
    }

    @RequestMapping(value = "/feedbackAlumni/delete/{id}", method = RequestMethod.POST)
    public String deleteFeedbackAlumni(@PathVariable(value = "id") String id) {
        FeedbackAlumni feedbackAlumni = feedbackAlumniService.getById(id);
        feedbackAlumniService.delete(feedbackAlumni);
        return "redirect:/admin/index";
    }
}
