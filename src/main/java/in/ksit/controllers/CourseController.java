/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.controllers;

import in.ksit.entity.Course;
import in.ksit.entity.Course.COURSE;
import in.ksit.service.CourseService;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/course")
public class CourseController {

    @Autowired private CourseService courseService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addCourse(
            ModelMap model,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "mobileNumber", required = false) String phone,
            @RequestParam(value = "course", required = false) COURSE courseStr,
            @RequestParam(value = "SSLC", required = false) double sslc,
            @RequestParam(value = "PUC", required = false) double puc,
            @RequestParam(value = "address", required = false) String address) {
        Course course =
                new Course(
                        name,
                        email,
                        phone,
                        courseStr,
                        address,
                        sslc,
                        puc,
                        Calendar.getInstance().getTime());
        courseService.saveOrUpdate(course);
        return "redirect:/coursesOffered";
    }
}
