/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import com.google.gson.Gson;
import in.ksit.dao.LabDataDao;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.LabData;
import in.ksit.entity.LabDataInternal;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LabDataServiceImpl implements LabDataService {

    @Autowired private LabDataDao labDataDao;

    Gson gson = new Gson();

    @Override
    public void saveOrUpdate(LabData labData) {
        labDataDao.saveOrUpdate(labData);
    }

    @Override
    public List<LabData> getByPageAndFieldName(
            PageType page, FieldNameType fieldName, Boolean active) {
        return labDataDao.getByPageAndFieldName(page, fieldName, active);
    }

    @Override
    public LabData getById(String id) {
        return labDataDao.getById(id);
    }

    @Override
    public List<LabDataInternal> getLabDataByPageAndFieldName(
            PageType page, FieldNameType fieldName, Boolean active) {
        List<LabData> labDataList = getByPageAndFieldName(page, fieldName, active);
        List<LabDataInternal> labDataInternalList = new ArrayList<LabDataInternal>();
        for (int i = 0; i < labDataList.size(); i++) {
            LabDataInternal labDataInternal =
                    gson.fromJson(labDataList.get(i).getData(), LabDataInternal.class);
            labDataInternal.setId(labDataList.get(i).getId());
            labDataInternalList.add(labDataInternal);
        }
        return labDataInternalList;
    }
}
