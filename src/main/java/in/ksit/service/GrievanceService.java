/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import com.mashape.unirest.http.exceptions.UnirestException;
import in.ksit.dao.GrievanceDao;
import in.ksit.entity.Grievance;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GrievanceService {

    @Autowired private GrievanceDao grievanceDao;

    public Grievance saveNewGrievance(
            String name,
            String category,
            String course,
            String branch,
            String usn,
            String mobileNumber,
            String emailId,
            String subject,
            String description) {
        Grievance grievance =
                new Grievance(
                        name,
                        category,
                        course,
                        branch,
                        usn,
                        mobileNumber,
                        emailId,
                        subject,
                        description);
        saveOrUpdate(grievance);
        return grievance;
    }

    public void saveOrUpdate(Grievance grievance) {
        grievanceDao.saveOrUpdate(grievance);
    }

    public void addNewGrievanceInformation(Grievance grievance) {
        saveOrUpdate(grievance);
        String body = "";
        body += "Name: " + grievance.getName() + "\n";
        body += "Category: " + grievance.getCategory() + "\n";
        body += "Course: " + grievance.getCourse() + "\n";
        body += "Branch: " + grievance.getBranch() + "\n";
        body += "Usn: " + grievance.getUsn() + "\n";
        body += "Mobile Number: " + grievance.getMobileNumber() + "\n";
        body += "Email: " + grievance.getEmailId() + "\n";
        body += "Subject: " + grievance.getSubject() + "\n";
        body += "Description: " + grievance.getDescription() + "\n";

        try {
            SendEmail.sendEmail("KSIT WEBMASTER", "grievance@ksit.edu.in", "Grievance Form", body);
        } catch (UnirestException e) {
            System.out.println("Error while sending email for grievance " + e.getMessage());
            e.printStackTrace();
        }
    }

    public List<Grievance> findByPage(String page) {
        return grievanceDao.findByPage(page);
    }

    public List<Grievance> findAll() {
        return grievanceDao.findAll();
    }

    public int getPAGE_SIZE() {
        return grievanceDao.getPAGE_SIZE();
    }
}
