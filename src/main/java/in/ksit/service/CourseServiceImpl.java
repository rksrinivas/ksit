/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.CourseDao;
import in.ksit.entity.Course;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired private CourseDao courseDao;

    @Override
    public void saveOrUpdate(Course course) {
        courseDao.saveOrUpdate(course);
    }

    @Override
    public List<Course> findByPage(String page) {
        return courseDao.findByPage(page);
    }

    @Override
    public int getPAGE_SIZE() {
        return courseDao.getPAGE_SIZE();
    }

    @Override
    public Course getById(String id) {
        return courseDao.getById(id);
    }

    @Override
    public void delete(Course course) {
        courseDao.delete(course);
    }
}
