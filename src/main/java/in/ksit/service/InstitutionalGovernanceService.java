/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.List;

import in.ksit.entity.InstitutionalGovernance;

public interface InstitutionalGovernanceService {

	public void saveOrUpdate(InstitutionalGovernance institutionalGovernance);

	public List<InstitutionalGovernance> getAll();

	public void delete(InstitutionalGovernance naac);

	public InstitutionalGovernance getById(String id);
}
