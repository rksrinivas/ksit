/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.ksit.dao.InstitutionalGovernanceDao;
import in.ksit.entity.InstitutionalGovernance;

@Service
public class InstitutionalGovernanceServiceImpl implements InstitutionalGovernanceService {

	@Autowired
	InstitutionalGovernanceDao institutionalGovernanceDao;

	@Override
	public void saveOrUpdate(InstitutionalGovernance institutionalGovernance) {
		institutionalGovernanceDao.saveOrUpdate(institutionalGovernance);
	}

	@Override
	public List<InstitutionalGovernance> getAll() {
		return institutionalGovernanceDao.getAll();
	}

	@Override
	public void delete(InstitutionalGovernance institutionalGovernance) {
		institutionalGovernanceDao.delete(institutionalGovernance);
	}

	@Override
	public InstitutionalGovernance getById(String id) {
		return institutionalGovernanceDao.getById(id);
	}

}
