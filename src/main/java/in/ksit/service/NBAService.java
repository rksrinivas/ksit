/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.Criteria;
import in.ksit.entity.NBA;
import in.ksit.entity.PageType;
import java.util.List;
import java.util.Map;

public interface NBAService {
    public void saveOrUpdate(NBA nba);

    public Map<Criteria, List<NBA>> getByPage(PageType page);

    public List<NBA> getAll();

    public void delete(NBA nba);

    public NBA getById(String id);
}
