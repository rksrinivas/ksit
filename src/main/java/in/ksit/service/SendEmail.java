/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class SendEmail {

    public static JsonNode sendEmail(String from, String to, String subject, String body)
            throws UnirestException {

        String API_KEY = "key-d4645699b6ba0e4f7d5d1f075abd4a72";
        String DOMAIN_NAME = "schoolbill.in";
        String fromName = from + "@" + DOMAIN_NAME;
        HttpResponse<JsonNode> request =
                Unirest.post("https://api.mailgun.net/v3/" + DOMAIN_NAME + "/messages")
                        .basicAuth("api", API_KEY)
                        .queryString("from", fromName)
                        .queryString("to", to)
                        .queryString("subject", subject)
                        .queryString("text", body)
                        .asJson();

        System.out.println(request.getBody());
        return request.getBody();
    }

    public static JsonNode sendEmail(
            String from, String to, String subject, String body, String html)
            throws UnirestException {

        String API_KEY = "key-d4645699b6ba0e4f7d5d1f075abd4a72";
        String DOMAIN_NAME = "schoolbill.in";
        String fromName = from + "@" + DOMAIN_NAME;
        HttpResponse<JsonNode> request =
                Unirest.post("https://api.mailgun.net/v3/" + DOMAIN_NAME + "/messages")
                        .basicAuth("api", API_KEY)
                        .queryString("from", fromName)
                        .queryString("to", to)
                        .queryString("subject", subject)
                        .queryString("text", body)
                        .field("html", html)
                        .asJson();

        System.out.println(request.getBody());
        return request.getBody();
    }
}
