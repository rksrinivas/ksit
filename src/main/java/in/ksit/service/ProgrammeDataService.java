/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.ProgrammeData;

public interface ProgrammeDataService {

    public void saveOrUpdate(ProgrammeData programmeData);

    public ProgrammeData getById(String id);

    public void delete(ProgrammeData programmeData);
}
