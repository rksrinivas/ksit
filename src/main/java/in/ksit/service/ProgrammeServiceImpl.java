/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.ProgrammeDao;
import in.ksit.entity.Programme;
import in.ksit.entity.Programme.PROGRAMME_TYPE;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProgrammeServiceImpl implements ProgrammeService {

    @Autowired private ProgrammeDao programmeDao;

    @Override
    public void saveOrUpdate(Programme programme) {
        programmeDao.saveOrUpdate(programme);
    }

    @Override
    public List<Programme> getProgrammesByType(PROGRAMME_TYPE programmeType) {
        return programmeDao.getProgrammesByType(programmeType);
    }

    @Override
    public Programme getById(String id) {
        return programmeDao.getById(id);
    }

    @Override
    public void delete(Programme programme) {
        programmeDao.delete(programme);
    }
}
