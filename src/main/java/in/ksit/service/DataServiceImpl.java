/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.DataDao;
import in.ksit.entity.Data;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataServiceImpl implements DataService {

    @Autowired private DataDao dataDao;

    @Override
    public void saveOrUpdate(Data data) {
        dataDao.saveOrUpdate(data);
    }

    @Override
    public List<Data> getActiveByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return dataDao.getByPageAndFieldName(page, fieldName, true);
    }

    @Override
    public List<Data> getByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return dataDao.getByPageAndFieldName(page, fieldName, null);
    }

    @Override
    public Data getById(String id) {
        return dataDao.getById(id);
    }
}
