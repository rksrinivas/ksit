/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.ksit.dao.HostelEnrollmentDao;
import in.ksit.entity.HostelEnrollment;

@Service
public class HostelEnrollmentServiceImpl implements HostelEnrollmentService {

    @Autowired private HostelEnrollmentDao hostelEnrollmentDao;

    @Override
    public void saveOrUpdate(HostelEnrollment hostelEnrollment) {
    	hostelEnrollmentDao.saveOrUpdate(hostelEnrollment);
    }

    @Override
    public List<HostelEnrollment> findByPage(String page) {
        return hostelEnrollmentDao.findByPage(page);
    }

    @Override
    public int getPAGE_SIZE() {
        return hostelEnrollmentDao.getPAGE_SIZE();
    }

    @Override
    public HostelEnrollment getById(String id) {
        return hostelEnrollmentDao.getById(id);
    }

    @Override
    public void delete(HostelEnrollment hostelEnrollment) {
    	hostelEnrollmentDao.delete(hostelEnrollment);
    }
}
