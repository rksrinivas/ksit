/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.List;

import in.ksit.entity.Feedback;

public interface FeedbackService {

	public void saveOrUpdate(Feedback feedback);

	public List<Feedback> findByPage(String page);

	public int getPAGE_SIZE();

	public void add(Feedback feedback);

	public Feedback getById(String id);

	public void delete(Feedback feedback);
}
