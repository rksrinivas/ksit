/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.Programme;
import in.ksit.entity.Programme.PROGRAMME_TYPE;
import java.util.List;

public interface ProgrammeService {
    public void saveOrUpdate(Programme programme);

    public List<Programme> getProgrammesByType(PROGRAMME_TYPE programmeType);

    public Programme getById(String id);

    public void delete(Programme programme);
}
