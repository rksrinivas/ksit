/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.NBAPrevisit;
import in.ksit.entity.NBAPrevisitParentData;
import in.ksit.entity.PageType;
import java.util.List;

public interface NBAPrevisitService {
    public void saveOrUpdate(NBAPrevisit nbaPrevisit);

    public void saveOrUpdate(NBAPrevisitParentData nbaPrevisitParentData);

    public List<NBAPrevisit> getByPage(PageType page);

    public List<NBAPrevisit> getAll();

    public void delete(NBAPrevisit nba);

    public void delete(NBAPrevisitParentData nbaPrevisitParentData);

    public NBAPrevisit getById(String id);
}
