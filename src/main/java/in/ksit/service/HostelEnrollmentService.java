/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.List;

import in.ksit.entity.HostelEnrollment;

public interface HostelEnrollmentService {
	public void saveOrUpdate(HostelEnrollment hostelEnrollment);

    public List<HostelEnrollment> findByPage(String page);

    public int getPAGE_SIZE();

    public HostelEnrollment getById(String id);

    public void delete(HostelEnrollment contactUs);
}
