/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.List;
import java.util.Map;

import in.ksit.entity.Criteria;
import in.ksit.entity.NAAC;
import in.ksit.entity.PageType;

public interface NAACService {

    public void saveOrUpdate(NAAC naac);

    public Map<Integer, Map<Criteria, List<NAAC>>> groupByYear(PageType page);
    
    public Map<Criteria, List<NAAC>> groupByCriteria(PageType page);

    public List<NAAC> getAllByPage(PageType page);

    public void delete(NAAC naac);

    public NAAC getById(String id);
}
