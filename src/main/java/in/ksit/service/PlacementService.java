/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.PlacementDao;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import in.ksit.entity.Placement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlacementService {

    @Autowired private PlacementDao placementDao;

    public void saveOrUpdate(Placement placement) {
        placementDao.saveOrUpdate(placement);
    }

    public List<Placement> getActiveByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return placementDao.getByPageAndFieldName(page, fieldName, true);
    }

    public List<Placement> getByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return placementDao.getByPageAndFieldName(page, fieldName, null);
    }

    public Placement getById(String id) {
        return placementDao.getById(id);
    }
}
