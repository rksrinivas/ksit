/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.FeedbackParent;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.ui.ModelMap;

public interface FeedbackParentService {

    public void saveOrUpdate(FeedbackParent feedbackParent);

    public List<FeedbackParent> findByPage(PageType page);

    public void delete(FeedbackParent feedbackParent);

    public FeedbackParent getById(String id);

    public void addDataToModel(ModelMap model);
}
