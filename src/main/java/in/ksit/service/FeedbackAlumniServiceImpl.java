/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.FeedbackAlumniDao;
import in.ksit.entity.FeedbackAlumni;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class FeedbackAlumniServiceImpl implements FeedbackAlumniService {

    @Autowired private FeedbackAlumniDao feedbackAlumniDao;

    @Override
    public void saveOrUpdate(FeedbackAlumni feedbackAlumni) {
        feedbackAlumniDao.saveOrUpdate(feedbackAlumni);
    }

    @Override
    public List<FeedbackAlumni> findByPage(PageType page) {
        return feedbackAlumniDao.findByPage(page);
    }

    @Override
    public void delete(FeedbackAlumni feedbackAlumni) {
        feedbackAlumniDao.delete(feedbackAlumni);
    }

    @Override
    public FeedbackAlumni getById(String id) {
        return feedbackAlumniDao.getById(id);
    }

    @Override
    public void addDataToModel(ModelMap model) {
        String[] responses = new String[] {"Excellent", "Very Good", "Good", "Average", "Bad"};
        String[] knowledgeFeedbacks =
                new String[] {
                    "Math, Science, Humanities and professional discipline, (if applicable)",
                    "Problem formulation and solving skills.",
                    "Collecting and analyzing appropriate data.",
                    "Ability to link theory to practice.",
                    "Ability to design a system component or process.",
                    "IT knowledge."
                };
        String[] communicationFeedbacks =
                new String[] {"Oral communication", "Report writing", "Presentation skills"};
        String[] interPersonalFeedbacks =
                new String[] {
                    "Ability to work in teams",
                    "Ability to work in arduous /Challenging situation",
                    "Independent thinking",
                    "Appreciation of ethical Values"
                };
        String[] managementFeedbacks =
                new String[] {"Resource and Time management skills.", "Judgment", "Discipline"};
        model.addAttribute("knowledgeFeedbacks", knowledgeFeedbacks);
        model.addAttribute("communicationFeedbacks", communicationFeedbacks);
        model.addAttribute("interPersonalFeedbacks", interPersonalFeedbacks);
        model.addAttribute("managementFeedbacks", managementFeedbacks);
        model.addAttribute("responses", responses);
    }
}
