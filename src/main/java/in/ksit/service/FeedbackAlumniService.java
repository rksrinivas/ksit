/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.FeedbackAlumni;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.ui.ModelMap;

public interface FeedbackAlumniService {
    public void saveOrUpdate(FeedbackAlumni feedbackAlumni);

    public List<FeedbackAlumni> findByPage(PageType page);

    public void delete(FeedbackAlumni feedbackAlumni);

    public FeedbackAlumni getById(String id);

    public void addDataToModel(ModelMap model);
}
