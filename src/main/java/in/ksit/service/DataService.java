/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.Data;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import java.util.List;

public interface DataService {

    public void saveOrUpdate(Data data);

    public List<Data> getActiveByPageAndFieldName(PageType page, FieldNameType fieldName);

    public List<Data> getByPageAndFieldName(PageType page, FieldNameType fieldName);

    public Data getById(String id);
}
