/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.FeedbackEmployerDao;
import in.ksit.entity.FeedbackEmployer;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class FeedbackEmployerServiceImpl implements FeedbackEmployerService {

    @Autowired private FeedbackEmployerDao feedbackEmployerDao;

    @Override
    public void saveOrUpdate(FeedbackEmployer feedbackEmployer) {
        feedbackEmployerDao.saveOrUpdate(feedbackEmployer);
    }

    @Override
    public List<FeedbackEmployer> findByPage(PageType page) {
        return feedbackEmployerDao.findByPage(page);
    }

    @Override
    public void delete(FeedbackEmployer feedbackEmployer) {
        feedbackEmployerDao.delete(feedbackEmployer);
    }

    @Override
    public FeedbackEmployer getById(String id) {
        return feedbackEmployerDao.getById(id);
    }

    @Override
    public void addDataToModel(ModelMap model) {
        String[] employerFeedbacks =
                new String[] {
                    "Ability to contribute to the goal of the your organization",
                    "Technical knowledge / Skill / Leadership / Innovativeness",
                    "Creativity",
                    "Relationship with Seniors / Peers / Subordinates",
                    "Involvement in social activities",
                    "Ability and motivation for social activity",
                    "Obligation to work beyond schedule (if required)",
                    "Overall impression about their performance"
                };
        model.addAttribute("employerFeedbacks", employerFeedbacks);
    }
}
