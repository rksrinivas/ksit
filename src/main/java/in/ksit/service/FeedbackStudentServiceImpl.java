/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.FeedbackStudentDao;
import in.ksit.entity.FeedbackStudent;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class FeedbackStudentServiceImpl implements FeedbackStudentService {

    @Autowired private FeedbackStudentDao feedbackStudentDao;

    @Override
    public void saveOrUpdate(FeedbackStudent feedbackStudent) {
        feedbackStudentDao.saveOrUpdate(feedbackStudent);
    }

    @Override
    public List<FeedbackStudent> findByPage(PageType page) {
        return feedbackStudentDao.findByPage(page);
    }

    @Override
    public void delete(FeedbackStudent feedbackStudent) {
        feedbackStudentDao.delete(feedbackStudent);
    }

    @Override
    public FeedbackStudent getById(String id) {
        return feedbackStudentDao.getById(id);
    }

    @Override
    public void addDataToModel(ModelMap model) {
        String[] studentFeedbacks =
                new String[] {
                    "Infrastructure & Lab facilities",
                    "Quality of support material",
                    "Training & Placement",
                    "Library /Reading Room",
                    "Canteen Facilities",
                    "Alumni Association",
                    "Internet & Wi-Fi",
                    "Sports and Cultural facilities",
                    "Classrooms",
                    "Seminars &Workshop",
                    "Environment / Learning Experience",
                    "Faculty",
                    "Project Guidance",
                    "Overall Rating of the College"
                };
        model.addAttribute("studentFeedbacks", studentFeedbacks);
    }
}
