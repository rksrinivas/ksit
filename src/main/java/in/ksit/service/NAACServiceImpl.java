/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.ksit.dao.NAACDao;
import in.ksit.entity.Criteria;
import in.ksit.entity.NAAC;
import in.ksit.entity.PageType;

@Service
public class NAACServiceImpl implements NAACService {

    @Autowired private NAACDao naacDao;

    @Override
    public void saveOrUpdate(NAAC naac) {
        naacDao.saveOrUpdate(naac);
    }

    @Override
    public Map<Integer, Map<Criteria, List<NAAC>>> groupByYear(PageType page) {
        List<NAAC> naacList = naacDao.getAllByPage(page);
        Collections.sort(
                naacList,
                new Comparator<NAAC>() {

                    @Override
                    public int compare(NAAC naac1, NAAC naac2) {
                        if (naac1.getYear().equals(naac2.getYear())) {
                            return Integer.valueOf(naac1.getCriteria().getSortOrder())
                                    .compareTo(naac2.getCriteria().getSortOrder());
                        } else {
                            return naac1.getYear().compareTo(naac2.getYear());
                        }
                    }
                });
        Map<Integer, Map<Criteria, List<NAAC>>> yearMap =
                new LinkedHashMap<Integer, Map<Criteria, List<NAAC>>>();
        for (NAAC naac : naacList) {
            Map<Criteria, List<NAAC>> criteriaMap;
            if (yearMap.containsKey(naac.getYear())) {
                criteriaMap = yearMap.get(naac.getYear());
            } else {
                criteriaMap = new LinkedHashMap<Criteria, List<NAAC>>();
                yearMap.put(naac.getYear(), criteriaMap);
            }
            if (criteriaMap.containsKey(naac.getCriteria())) {
                criteriaMap.get(naac.getCriteria()).add(naac);
            } else {
                List<NAAC> tempList = new ArrayList<NAAC>();
                tempList.add(naac);
                criteriaMap.put(naac.getCriteria(), tempList);
            }
        }
        return yearMap;
    }
    
    @Override
    public Map<Criteria, List<NAAC>> groupByCriteria(PageType page) {
        List<NAAC> naacList = naacDao.getAllByPage(page);
        Collections.sort(
                naacList,
                new Comparator<NAAC>() {

                    @Override
                    public int compare(NAAC naac1, NAAC naac2) {
                    	return Integer.valueOf(naac1.getCriteria().getSortOrder()).compareTo(naac2.getCriteria().getSortOrder());
                    }
                });
        Map<Criteria, List<NAAC>> criteriaMap = new LinkedHashMap<>();
        for (NAAC naac : naacList) {
        	if(criteriaMap.containsKey(naac.getCriteria())) {
        		criteriaMap.get(naac.getCriteria()).add(naac);
        	}else {
        		List<NAAC> tempList = new ArrayList<>();
        		tempList.add(naac);
        		criteriaMap.put(naac.getCriteria(), tempList);
        	}
        }
        return criteriaMap;
       
    }


    @Override
    public List<NAAC> getAllByPage(PageType page) {
        return naacDao.getAllByPage(page);
    }

    @Override
    public void delete(NAAC naac) {
        naacDao.delete(naac);
    }

    @Override
    public NAAC getById(String id) {
        return naacDao.getById(id);
    }
}
