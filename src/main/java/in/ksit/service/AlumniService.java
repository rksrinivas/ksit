/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.Alumni;
import java.util.List;

public interface AlumniService {

    public void saveOrUpdate(Alumni alumni);

    public List<Alumni> findByPage(String page);

    public List<Alumni> findAll();

    public int getPAGE_SIZE();
}
