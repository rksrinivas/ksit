/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.FeedbackEmployer;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.ui.ModelMap;

public interface FeedbackEmployerService {
    public void saveOrUpdate(FeedbackEmployer feedbackEmployer);

    public List<FeedbackEmployer> findByPage(PageType page);

    public void delete(FeedbackEmployer feedbackEmployer);

    public FeedbackEmployer getById(String id);

    public void addDataToModel(ModelMap model);
}
