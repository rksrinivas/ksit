/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import com.mashape.unirest.http.exceptions.UnirestException;
import in.ksit.dao.ContactUsDao;
import in.ksit.entity.ContactUs;
import in.ksit.util.AppUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactUsServiceImpl implements ContactUsService {

    @Autowired private ContactUsDao contactUsDao;

    @Override
    public void saveOrUpdate(ContactUs contactUs) {
        contactUsDao.saveOrUpdate(contactUs);
    }

    @Override
    public List<ContactUs> findByPage(String page) {
        return contactUsDao.findByPage(page);
    }

    @Override
    public int getPAGE_SIZE() {
        return contactUsDao.getPAGE_SIZE();
    }

    @Override
    public void addNewContactInformation(ContactUs contactUs) {
        saveOrUpdate(contactUs);
        if (AppUtils.isLocalDomain() == false) {
            String body = "";
            body += "Name: " + contactUs.getName() + "\n";
            body += "Email: " + contactUs.getEmail() + "\n";
            body += "Phone: " + contactUs.getPhone() + "\n";
            body += "Subject: " + contactUs.getSubject() + "\n";
            body += "Description: " + contactUs.getDescription() + "\n";
            try {
                SendEmail.sendEmail(
                        "KSIT WEBMASTER", "contact@ksit.edu.in", "Contact Us Form", body);
            } catch (UnirestException e) {
                System.out.println("Error while sending email for cotactus " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Override
    public ContactUs getById(String id) {
        return contactUsDao.getById(id);
    }

    @Override
    public void delete(ContactUs contactUs) {
        contactUsDao.delete(contactUs);
    }
}
