/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.FacultyDao;
import in.ksit.entity.Faculty;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FacultyService {

    @Autowired private FacultyDao facultyDao;

    public void saveOrUpdate(Faculty faculty) {
        facultyDao.saveOrUpdate(faculty);
    }

    public List<Faculty> getActiveByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return facultyDao.getByPageAndFieldName(page, fieldName, true);
    }

    public List<Faculty> getByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return facultyDao.getByPageAndFieldName(page, fieldName, null);
    }

    public Faculty getById(String id) {
        return facultyDao.getById(id);
    }
}
