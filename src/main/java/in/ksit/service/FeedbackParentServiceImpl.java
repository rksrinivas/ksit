/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.FeedbackParentDao;
import in.ksit.entity.FeedbackParent;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class FeedbackParentServiceImpl implements FeedbackParentService {

    @Autowired private FeedbackParentDao feedbackParentDao;

    @Override
    public void saveOrUpdate(FeedbackParent feedbackParent) {
        feedbackParentDao.saveOrUpdate(feedbackParent);
    }

    @Override
    public List<FeedbackParent> findByPage(PageType page) {
        return feedbackParentDao.findByPage(page);
    }

    @Override
    public void delete(FeedbackParent feedbackParent) {
        feedbackParentDao.delete(feedbackParent);
    }

    @Override
    public FeedbackParent getById(String id) {
        return feedbackParentDao.getById(id);
    }

    @Override
    public void addDataToModel(ModelMap model) {
        String[] parentFeedbacks =
                new String[] {
                    "Holistic development of students is ensured by participation of students in"
                            + " various sports, cultural and co-curricular activities organized"
                            + " throughout the year.",
                    "Students are sensitized towards cross cutting issues like gender equality,"
                        + " environment and sustainability, ethics and values etc. through relevant"
                        + " courses in the curriculum as well as through community service /"
                        + " projects with NGOs, participation in various awareness campaigns and"
                        + " blood donation drives, exhibitions on socially relevant issues etc.",
                    "The academic flexibility embedded in the curriculum provides opportunities to"
                        + " students to pursue their interest by choosing from a vast number of"
                        + " pathways / electives from own area/specialization as well as from other"
                        + " areas.",
                    "Courses in the curriculum promote Entrepreneurship and students are encouraged"
                            + " to develop Entrepreneurship qualities.",
                    "The Curriculum has been designed to make students industry ready by imparting"
                            + " analytical and reasoning, language and soft skills in addition to"
                            + " technical competencies, as desired by the industry.",
                    "The curriculum is outcome based and the expected outcomes, through various"
                            + " courses, are attained."
                };
        model.addAttribute("parentFeedbacks", parentFeedbacks);
    }
}
