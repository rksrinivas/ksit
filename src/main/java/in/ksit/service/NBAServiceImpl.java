/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.NBADao;
import in.ksit.entity.Criteria;
import in.ksit.entity.NBA;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NBAServiceImpl implements NBAService {

    @Autowired private NBADao nbaDao;

    @Override
    public void saveOrUpdate(NBA nba) {
        nbaDao.saveOrUpdate(nba);
    }

    @Override
    public Map<Criteria, List<NBA>> getByPage(PageType page) {
        List<NBA> nbaList = nbaDao.getByPage(page);
        Collections.sort(
                nbaList,
                new Comparator<NBA>() {

                    @Override
                    public int compare(NBA nba1, NBA nba2) {
                        return Integer.valueOf(nba1.getCriteria().getSortOrder())
                                .compareTo(nba2.getCriteria().getSortOrder());
                    }
                });
        Map<Criteria, List<NBA>> criteriaMap = new LinkedHashMap<Criteria, List<NBA>>();
        for (NBA nba : nbaList) {
            if (criteriaMap.containsKey(nba.getCriteria())) {
                criteriaMap.get(nba.getCriteria()).add(nba);
            } else {
                List<NBA> tempList = new ArrayList<NBA>();
                tempList.add(nba);
                criteriaMap.put(nba.getCriteria(), tempList);
            }
        }
        return criteriaMap;
    }

    @Override
    public List<NBA> getAll() {
        List<NBA> nbaList = nbaDao.getAll();
        Collections.sort(
                nbaList,
                new Comparator<NBA>() {

                    @Override
                    public int compare(NBA nba1, NBA nba2) {
                        if (nba1.getPage().equals(nba2.getPage())) {
                            return Integer.valueOf(nba1.getCriteria().getSortOrder())
                                    .compareTo(nba2.getCriteria().getSortOrder());
                        } else {
                            return nba1.getPage().compareTo(nba2.getPage());
                        }
                    }
                });
        return nbaList;
    }

    @Override
    public void delete(NBA nba) {
        nbaDao.delete(nba);
    }

    @Override
    public NBA getById(String id) {
        return nbaDao.getById(id);
    }
}
