/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.EventDao;
import in.ksit.entity.Event;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventService {

    @Autowired private EventDao eventDao;

    public void saveOrUpdate(Event event) {
        eventDao.saveOrUpdate(event);
    }

    public List<Event> getActiveByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return eventDao.getByPageAndFieldName(page, fieldName, true);
    }

    public List<Event> getByPageAndFieldName(PageType page, FieldNameType fieldName) {
        return eventDao.getByPageAndFieldName(page, fieldName, null);
    }

    public Event getById(String id) {
        return eventDao.getById(id);
    }
}
