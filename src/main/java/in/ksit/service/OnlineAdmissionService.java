/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.OnlineAdmission;
import java.util.List;

public interface OnlineAdmissionService {

    public void saveOrUpdate(OnlineAdmission onlineAdmissions);

    public List<OnlineAdmission> findByPage(String page);

    public List<OnlineAdmission> findAll();

    public int getPAGE_SIZE();
}
