/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.AlumniDao;
import in.ksit.entity.Alumni;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlumniServiceImpl implements AlumniService {

    @Autowired private AlumniDao alumniDao;

    @Override
    public void saveOrUpdate(Alumni alumni) {
        alumniDao.saveOrUpdate(alumni);
    }

    @Override
    public List<Alumni> findByPage(String page) {
        return alumniDao.findByPage(page);
    }

    @Override
    public List<Alumni> findAll() {
        return alumniDao.findAll();
    }

    @Override
    public int getPAGE_SIZE() {
        return alumniDao.getPAGE_SIZE();
    }
}
