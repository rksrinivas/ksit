/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.NBAPrevisitDao;
import in.ksit.dao.NBAPrevisitParentDataDao;
import in.ksit.entity.NBAPrevisit;
import in.ksit.entity.NBAPrevisitParentData;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NBAPrevisitServiceImpl implements NBAPrevisitService {

    @Autowired private NBAPrevisitDao nbaPrevisitDao;

    @Autowired private NBAPrevisitParentDataDao nbaPrevisitParentDataDao;

    @Override
    public void saveOrUpdate(NBAPrevisit nbaPrevisit) {
        nbaPrevisitDao.saveOrUpdate(nbaPrevisit);
    }

    @Override
    public void saveOrUpdate(NBAPrevisitParentData nbaPrevisitParentData) {
        nbaPrevisitParentDataDao.saveOrUpdate(nbaPrevisitParentData);
    }

    @Override
    public List<NBAPrevisit> getByPage(PageType page) {
        List<NBAPrevisit> nbaPrevisitList = nbaPrevisitDao.getByPage(page);
        Set<String> childIdSet = new HashSet<String>();
        addNBAChildDetails(nbaPrevisitList, childIdSet);
        List<NBAPrevisit> newList = new ArrayList<NBAPrevisit>();
        for (NBAPrevisit temp : nbaPrevisitList) {
            if (childIdSet.contains(temp.getId()) == false) {
                newList.add(temp);
            }
        }
        return newList;
    }

    @Override
    public List<NBAPrevisit> getAll() {
        List<NBAPrevisit> nbaPrevisitList = nbaPrevisitDao.getAll();
        Set<String> childIdSet = new HashSet<String>();
        addNBAChildDetails(nbaPrevisitList, childIdSet);
        List<NBAPrevisit> newList = new ArrayList<NBAPrevisit>();
        for (NBAPrevisit temp : nbaPrevisitList) {
            if (childIdSet.contains(temp.getId()) == false) {
                newList.add(temp);
            }
        }
        return newList;
    }

    @Override
    public void delete(NBAPrevisit nbaPrevisit) {
        List<NBAPrevisitParentData> nbaPrevisitParentDataList =
                nbaPrevisitParentDataDao.getByParent(nbaPrevisit.getId());
        for (NBAPrevisitParentData nbaPrevisitParentData : nbaPrevisitParentDataList) {
            NBAPrevisit child = nbaPrevisitDao.getById(nbaPrevisitParentData.getChildId());
            delete(nbaPrevisitParentData);
            delete(child);
        }
        List<NBAPrevisitParentData> nbaPrevisitChildDataList =
                nbaPrevisitParentDataDao.getByChild(nbaPrevisit.getId());
        for (NBAPrevisitParentData nbaPrevisitChildData : nbaPrevisitChildDataList) {
            delete(nbaPrevisitChildData);
        }
        nbaPrevisitDao.delete(nbaPrevisit);
    }

    @Override
    public void delete(NBAPrevisitParentData nbaPrevisitParentData) {
        nbaPrevisitParentDataDao.delete(nbaPrevisitParentData);
    }

    @Override
    public NBAPrevisit getById(String id) {
        NBAPrevisit nbaPrevisit = nbaPrevisitDao.getById(id);
        Set<String> childIdSet = new HashSet<String>();
        addNBAChildDetails(nbaPrevisit, childIdSet);
        return nbaPrevisit;
    }

    public void addNBAChildDetails(List<NBAPrevisit> nbaPrevisitList, Set<String> childIdSet) {
        for (NBAPrevisit nbaPrevisit : nbaPrevisitList) {
            this.addNBAChildDetails(nbaPrevisit, childIdSet);
        }
        sort(nbaPrevisitList);
    }

    public void addNBAChildDetails(NBAPrevisit nbaPrevisit, Set<String> childIdSet) {
        List<NBAPrevisitParentData> nbaPrevisitParentDataList =
                nbaPrevisitParentDataDao.getByParent(nbaPrevisit.getId());
        if (nbaPrevisitParentDataList.size() > 0) {
            List<NBAPrevisit> childList = new ArrayList<NBAPrevisit>();
            for (NBAPrevisitParentData nbaPrevisitParentData : nbaPrevisitParentDataList) {
                NBAPrevisit temp = nbaPrevisitDao.getById(nbaPrevisitParentData.getChildId());
                childList.add(temp);
                childIdSet.add(temp.getId());
            }
            addNBAChildDetails(childList, childIdSet);
            sort(childList);
            nbaPrevisit.setSubPrevisitList(childList);
        }
    }

    private void sort(List<NBAPrevisit> nbaPrevisitList) {
        Collections.sort(
                nbaPrevisitList,
                new Comparator<NBAPrevisit>() {

                    @Override
                    public int compare(NBAPrevisit nbaPrevisit1, NBAPrevisit nbaPrevisit2) {
                        return Integer.valueOf(
                                nbaPrevisit1.getRank().compareTo(nbaPrevisit2.getRank()));
                    }
                });
    }
}
