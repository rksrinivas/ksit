/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.ksit.dao.VisitDao;
import in.ksit.entity.Visit;

@Service
public class VisitServiceImpl implements VisitService {

	@Autowired
	private VisitDao visitDao;

	@Override
	public void saveOrUpdate(Visit visit) {
		visitDao.saveOrUpdate(visit);
	}

	@Override
	public Visit findByName(String name) {
		return visitDao.findByName(name);
	}

}