/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.Visit;

public interface VisitService {
	Visit findByName(String name);
	
	void saveOrUpdate(Visit visit);

}
