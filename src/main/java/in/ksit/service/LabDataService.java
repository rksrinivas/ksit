/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.FieldNameType;
import in.ksit.entity.LabData;
import in.ksit.entity.LabDataInternal;
import in.ksit.entity.PageType;
import java.util.List;

public interface LabDataService {

    public void saveOrUpdate(LabData labData);

    public List<LabData> getByPageAndFieldName(
            PageType page, FieldNameType fieldName, Boolean active);

    public List<LabDataInternal> getLabDataByPageAndFieldName(
            PageType page, FieldNameType fieldName, Boolean active);

    public LabData getById(String id);
}
