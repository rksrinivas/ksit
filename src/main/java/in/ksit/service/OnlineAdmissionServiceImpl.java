/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.OnlineAdmissionDao;
import in.ksit.entity.OnlineAdmission;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnlineAdmissionServiceImpl implements OnlineAdmissionService {

    @Autowired private OnlineAdmissionDao onlineAdmissionDao;

    @Override
    public void saveOrUpdate(OnlineAdmission onlineAdmission) {
        onlineAdmissionDao.saveOrUpdate(onlineAdmission);
    }

    @Override
    public List<OnlineAdmission> findByPage(String page) {
        return onlineAdmissionDao.findByPage(page);
    }

    @Override
    public List<OnlineAdmission> findAll() {
        return onlineAdmissionDao.findAll();
    }

    @Override
    public int getPAGE_SIZE() {
        return onlineAdmissionDao.getPAGE_SIZE();
    }
}
