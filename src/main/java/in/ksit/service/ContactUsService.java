/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.ContactUs;
import java.util.List;

public interface ContactUsService {

    public void saveOrUpdate(ContactUs contactUs);

    public List<ContactUs> findByPage(String page);

    public int getPAGE_SIZE();

    public void addNewContactInformation(ContactUs contactUs);

    public ContactUs getById(String id);

    public void delete(ContactUs contactUs);
}
