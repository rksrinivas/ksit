/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.Course;
import java.util.List;

public interface CourseService {

    public void saveOrUpdate(Course course);

    public List<Course> findByPage(String page);

    public int getPAGE_SIZE();

    public Course getById(String id);

    public void delete(Course course);
}
