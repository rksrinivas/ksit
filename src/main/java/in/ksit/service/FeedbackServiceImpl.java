/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.ksit.dao.FeedbackDao;
import in.ksit.entity.Feedback;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired private FeedbackDao feedbackDao;

    @Override
    public void saveOrUpdate(Feedback feedback) {
    	feedbackDao.saveOrUpdate(feedback);
    }

    @Override
    public List<Feedback> findByPage(String page) {
        return feedbackDao.findByPage(page);
    }

    @Override
    public int getPAGE_SIZE() {
        return feedbackDao.getPAGE_SIZE();
    }

    @Override
    public Feedback getById(String id) {
        return feedbackDao.getById(id);
    }

    @Override
    public void delete(Feedback feedback) {
    	feedbackDao.delete(feedback);
    }

	@Override
	public void add(Feedback feedback) {
		 saveOrUpdate(feedback);
	}
}