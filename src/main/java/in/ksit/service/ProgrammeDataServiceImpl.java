/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.ProgrammeDataDao;
import in.ksit.entity.ProgrammeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProgrammeDataServiceImpl implements ProgrammeDataService {

    @Autowired private ProgrammeDataDao programmeDataDao;

    @Override
    public void saveOrUpdate(ProgrammeData programmeData) {
        programmeDataDao.saveOrUpdate(programmeData);
    }

    @Override
    public ProgrammeData getById(String id) {
        return programmeDataDao.getById(id);
    }

    @Override
    public void delete(ProgrammeData programmeData) {
        programmeDataDao.delete(programmeData);
    }
}
