/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.entity.FeedbackStudent;
import in.ksit.entity.PageType;
import java.util.List;
import org.springframework.ui.ModelMap;

public interface FeedbackStudentService {
    public void saveOrUpdate(FeedbackStudent feedbackStudent);

    public List<FeedbackStudent> findByPage(PageType page);

    public void delete(FeedbackStudent feedbackStudent);

    public FeedbackStudent getById(String id);

    public void addDataToModel(ModelMap model);
}
