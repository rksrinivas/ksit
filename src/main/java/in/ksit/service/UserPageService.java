/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.UserPageDao;
import in.ksit.entity.PageType;
import in.ksit.entity.UserPage;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserPageService {

    @Autowired private UserPageDao userPageDao;

    public void saveOrUpdate(UserPage userPage) {
        userPageDao.saveOrUpdate(userPage);
    }
    
    public void saveOrUpdate(List<UserPage> userPageList) {
        userPageDao.saveOrUpdate(userPageList);
    }

    public void deleteUserPage(UserPage userPage) {
        userPageDao.deleteUserPage(userPage);
    }
    
    public void deleteUserPage(List<UserPage> userPageList) {
        userPageDao.deleteUserPage(userPageList);
    }

    public List<UserPage> getByPage(PageType page) {
        return userPageDao.getByPage(page);
    }
}
