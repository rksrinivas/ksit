/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.service;

import in.ksit.dao.UserDao;
import in.ksit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired private PasswordEncoder passwordEncoder;

    @Autowired private UserDao userDao;

    public void saveOrUpdate(User user) {
        userDao.saveOrUpdate(user);
    }

    public User getByUsername(String username) {
        return userDao.getByUsername(username);
    }

    public User createAccount(String username, String password) {
        User user = userDao.getByUsername(username);
        if (user != null) {
            // User exists by that username, do not create new account
            return null;
        } else {
            user = new User();
        }
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        userDao.saveOrUpdate(user);
        return user;
    }
}
