/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.interceptor;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import in.ksit.entity.BaseData;
import in.ksit.entity.Data;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import in.ksit.entity.ProgrammeData.PROGRAMME_DATA_TYPE;
import in.ksit.entity.User;
import in.ksit.service.DataService;
import in.ksit.util.AppUtils;
import in.ksit.util.SessionUtil;

/*
 * Adds header related context as requestAttributes. FreemarkerView is p:p:exposeRequestAttributes=true.
 */
public class LoggedInUserModelInterceptor extends HandlerInterceptorAdapter {

    @Autowired private DataService dataService;
    
    private void cacheControl(ModelAndView modelAndView, HttpServletResponse response) {
		if(modelAndView != null && modelAndView.getModel() != null) {
			boolean mustRevalidate = false;
			for(String key: modelAndView.getModel().keySet()) {
				Object value = modelAndView.getModel().get(key);
				if(value instanceof List) {
					List<Object> tempValueList = (List)value;
					for(Object tempValue: tempValueList) {
						if(tempValue instanceof BaseData) {
							BaseData baseData = (BaseData)tempValue;
							Date lastUpdated = baseData.getLastUpdatedOn();
							if(lastUpdated == null) {
								continue;
							}
							long differenceInMillis = Calendar.getInstance().getTimeInMillis() - lastUpdated.getTime();
							long differenceInMinutes = differenceInMillis/1000/60;
							if(differenceInMinutes < 60) {
								mustRevalidate = true;
								break;
							}
						}
					}
					if(mustRevalidate) {
						break;
					}
				}
				if(value instanceof BaseData) {
					BaseData baseData = (BaseData)value;
					Date lastUpdated = baseData.getLastUpdatedOn();
					if(lastUpdated == null) {
						continue;
					}
					long differenceInMillis = Calendar.getInstance().getTimeInMillis() - lastUpdated.getTime();
					long differenceInMinutes = differenceInMillis/1000/60;
					if(differenceInMinutes < 60) {
						mustRevalidate = true;
						break;
					}
				}
			}
			if(mustRevalidate) {
				response.addHeader("Cache-Control", "max-age=0,no-cache, must-revalidate");
			}else {
				response.addHeader("Cache-Control", "max-age=86400");;
			}
		}
	}

    @Override
    public void postHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            ModelAndView modelAndView)
            throws Exception {
        super.postHandle(request, response, handler, modelAndView);

        request.setAttribute("js_path", "/js");
        request.setAttribute("css_path", "/css");
        request.setAttribute("img_path", "/img");
        
		cacheControl(modelAndView, response);

        if (AppUtils.isLocalDomain()) {
            request.setAttribute("timestamp", "");
        } else {
            request.setAttribute("timestamp", "1741201682666");
        }

        List<Data> scrollingTextTopList =
                dataService.getActiveByPageAndFieldName(
                        PageType.home_page, FieldNameType.scrolling_text_top);
        String scrollingTextTop = "";
        if (scrollingTextTopList.size() > 0) {
            scrollingTextTop = scrollingTextTopList.get(0).getContent();
        }
        request.setAttribute("scrollingTextTop", scrollingTextTop);
        request.setAttribute("programmeDataTypes", PROGRAMME_DATA_TYPE.values());

        if (SecurityContextHolder.getContext() != null
                && SecurityContextHolder.getContext().getAuthentication() != null) {
            request.setAttribute("loggedIn", SessionUtil.isUserAuthenicated());
            if (SessionUtil.isUserAuthenicated()) {
                User loggedInUser = SessionUtil.getLoggedInUser();
                request.setAttribute("loggedInUser", loggedInUser);
            }
            request.setAttribute(
                    "principal",
                    SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        }
    }
}
