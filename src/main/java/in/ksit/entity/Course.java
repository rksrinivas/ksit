/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "course")
public class Course {

    public enum COURSE {
        AIML("BE in Artificial Intelligence & Machine Learning Engineering"),
        CSD("BE in Computer Science and Design"),
        CCE("BE in Computer Science and Communication Engineering"),
        CSIOT("BE in Computer Science(IoT, Cyber Security including Block Chain)"),
        CSE("BE in Computer Science & Engineering"),
        ECE("BE in Electronics & Communication Engineering"),
        ME("BE in Mechanical Engineering"),

        MCSE("M.tech in Computer Science & Engineering"),
        MEC("M.tech in Digital Electronics & Communication"),
        MME("M.tech in Machine Design"),
        MCA("Master of Computer Application");

        String desc;

        COURSE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Enumerated(EnumType.STRING)
    @Column(name = "course")
    private COURSE course;

    @Column(name = "address")
    private String address;

    @Column(name = "sslc")
    private Double sslc;

    @Column(name = "puc")
    private Double puc;

    @Column(name = "created_on")
    private Date createdOn;

    public Course() {}

    public Course(
            String name,
            String email,
            String phone,
            COURSE course,
            String address,
            Double sslc,
            Double puc,
            Date createdOn) {
        super();
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.course = course;
        this.address = address;
        this.sslc = sslc;
        this.puc = puc;
        this.createdOn = createdOn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public COURSE getCourse() {
        return course;
    }

    public void setCourse(COURSE course) {
        this.course = course;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getSslc() {
        return sslc;
    }

    public void setSslc(Double sslc) {
        this.sslc = sslc;
    }

    public Double getPuc() {
        return puc;
    }

    public void setPuc(Double puc) {
        this.puc = puc;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
