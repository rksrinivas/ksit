/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "programme")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "hibernateEntityCache")
public class Programme {

    public enum PROGRAMME_TYPE {
        UG,
        PG
    };

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "programme_type")
    private PROGRAMME_TYPE programmeType;

    @Column(name = "department")
    private String department;

    @OneToMany(mappedBy = "programme", fetch = FetchType.EAGER)
    private Set<ProgrammeData> programmeDataSet;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PROGRAMME_TYPE getProgrammeType() {
        return programmeType;
    }

    public void setProgrammeType(PROGRAMME_TYPE programmeType) {
        this.programmeType = programmeType;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Set<ProgrammeData> getProgrammeDataSet() {
        return programmeDataSet;
    }

    public void setProgrammeDataSet(Set<ProgrammeData> programmeDataSet) {
        this.programmeDataSet = programmeDataSet;
    }
}
