/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.util.List;

public class LabDataInternal {

    private String id;

    private Integer serialNo;

    private String name;

    private List<String> configurations;

    private List<Integer> noOfSystems;

    private List<String> purposes;

    private String imageURL;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Integer serialNo) {
        this.serialNo = serialNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<String> configurations) {
        this.configurations = configurations;
    }

    public List<Integer> getNoOfSystems() {
        return noOfSystems;
    }

    public void setNoOfSystems(List<Integer> noOfSystems) {
        this.noOfSystems = noOfSystems;
    }

    public List<String> getPurposes() {
        return purposes;
    }

    public void setPurposes(List<String> purposes) {
        this.purposes = purposes;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
