/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import javax.ws.rs.FormParam;

public class FeedbackParentForm {

    @FormParam("page")
    private PageType page;

    @FormParam("nameOfTheParent")
    private String nameOfTheParent;

    @FormParam("emailId")
    private String emailId;

    @FormParam("phoneNo")
    private String phoneNo;

    @FormParam("occupation")
    private String occupation;

    @FormParam("studentName")
    private String studentName;

    @FormParam("semesterAndSection")
    private String semesterAndSection;

    @FormParam("academicYear")
    private String academicYear;

    @FormParam("usn")
    private String usn;

    @FormParam("relationship")
    private String relationship;

    @FormParam("visionMission")
    private String visionMission;

    @FormParam("visionAgree")
    private String visionAgree;

    @FormParam("missionAgree ")
    private String missionAgree;

    @FormParam("visionSuggestion")
    private String visionSuggestion;

    @FormParam("missionSuggestion")
    private String missionSuggestion;

    @FormParam("peoSuggestion")
    private String peoSuggestion;

    @FormParam("response1")
    private FeedbackResponse response1;

    @FormParam("response2")
    private FeedbackResponse response2;

    @FormParam("response3")
    private FeedbackResponse response3;

    @FormParam("response4")
    private FeedbackResponse response4;

    @FormParam("response5")
    private FeedbackResponse response5;

    @FormParam("response6")
    private FeedbackResponse response6;

    @FormParam("response")
    private String response;

    public PageType getPage() {
        return page;
    }

    public void setPage(PageType page) {
        this.page = page;
    }

    public String getNameOfTheParent() {
        return nameOfTheParent;
    }

    public void setNameOfTheParent(String nameOfTheParent) {
        this.nameOfTheParent = nameOfTheParent;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSemesterAndSection() {
        return semesterAndSection;
    }

    public void setSemesterAndSection(String semesterAndSection) {
        this.semesterAndSection = semesterAndSection;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getVisionMission() {
        return visionMission;
    }

    public void setVisionMission(String visionMission) {
        this.visionMission = visionMission;
    }

    public String getVisionAgree() {
        return visionAgree;
    }

    public void setVisionAgree(String visionAgree) {
        this.visionAgree = visionAgree;
    }

    public String getMissionAgree() {
        return missionAgree;
    }

    public void setMissionAgree(String missionAgree) {
        this.missionAgree = missionAgree;
    }

    public String getVisionSuggestion() {
        return visionSuggestion;
    }

    public void setVisionSuggestion(String visionSuggestion) {
        this.visionSuggestion = visionSuggestion;
    }

    public String getMissionSuggestion() {
        return missionSuggestion;
    }

    public void setMissionSuggestion(String missionSuggestion) {
        this.missionSuggestion = missionSuggestion;
    }

    public String getPeoSuggestion() {
        return peoSuggestion;
    }

    public void setPeoSuggestion(String peoSuggestion) {
        this.peoSuggestion = peoSuggestion;
    }

    public FeedbackResponse getResponse1() {
        return response1;
    }

    public void setResponse1(FeedbackResponse response1) {
        this.response1 = response1;
    }

    public FeedbackResponse getResponse2() {
        return response2;
    }

    public void setResponse2(FeedbackResponse response2) {
        this.response2 = response2;
    }

    public FeedbackResponse getResponse3() {
        return response3;
    }

    public void setResponse3(FeedbackResponse response3) {
        this.response3 = response3;
    }

    public FeedbackResponse getResponse4() {
        return response4;
    }

    public void setResponse4(FeedbackResponse response4) {
        this.response4 = response4;
    }

    public FeedbackResponse getResponse5() {
        return response5;
    }

    public void setResponse5(FeedbackResponse response5) {
        this.response5 = response5;
    }

    public FeedbackResponse getResponse6() {
        return response6;
    }

    public void setResponse6(FeedbackResponse response6) {
        this.response6 = response6;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
