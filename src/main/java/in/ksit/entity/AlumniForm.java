/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import javax.ws.rs.FormParam;

public class AlumniForm {

    @FormParam("name")
    private String name;

    @FormParam("course")
    private String course;

    @FormParam("branch ")
    private String branch;

    @FormParam("usn")
    private String usn;

    @FormParam("yearOfPassing")
    private String yearOfPassing;

    @FormParam("mobileNumber")
    private String mobileNumber;

    @FormParam("emailId")
    private String emailId;

    @FormParam("companyName")
    private String companyName;

    @FormParam("careerDetails")
    private String careerDetails;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    public String getYearOfPassing() {
        return yearOfPassing;
    }

    public void setYearOfPassing(String yearOfPassing) {
        this.yearOfPassing = yearOfPassing;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCareerDetails() {
        return careerDetails;
    }

    public void setCareerDetails(String careerDetails) {
        this.careerDetails = careerDetails;
    }
}
