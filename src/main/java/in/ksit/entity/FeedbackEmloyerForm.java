/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import javax.ws.rs.FormParam;

public class FeedbackEmloyerForm {

    @FormParam("page")
    private PageType page;

    @FormParam("name")
    private String name;

    @FormParam("companyName")
    private String companyName;

    @FormParam("designation")
    private String designation;

    @FormParam("mobileNumber")
    private String mobileNumber;

    @FormParam("emailId")
    private String emailId;

    @FormParam("visionMission")
    private String visionMission;

    @FormParam("visionAgree")
    private String visionAgree;

    @FormParam("missionAgree ")
    private String missionAgree;

    @FormParam("visionSuggestion")
    private String visionSuggestion;

    @FormParam("missionSuggestion")
    private String missionSuggestion;

    @FormParam("peoSuggestion")
    private String peoSuggestion;

    @FormParam("response1")
    private FeedbackResponse response1;

    @FormParam("response2")
    private FeedbackResponse response2;

    @FormParam("response3")
    private FeedbackResponse response3;

    @FormParam("response4")
    private FeedbackResponse response4;

    @FormParam("response5")
    private FeedbackResponse response5;

    @FormParam("response6")
    private FeedbackResponse response6;

    @FormParam("response7")
    private FeedbackResponse response7;

    @FormParam("response8")
    private FeedbackResponse response8;

    @FormParam("response")
    private String response;

    public PageType getPage() {
        return page;
    }

    public void setPage(PageType page) {
        this.page = page;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getVisionMission() {
        return visionMission;
    }

    public void setVisionMission(String visionMission) {
        this.visionMission = visionMission;
    }

    public String getVisionAgree() {
        return visionAgree;
    }

    public void setVisionAgree(String visionAgree) {
        this.visionAgree = visionAgree;
    }

    public String getMissionAgree() {
        return missionAgree;
    }

    public void setMissionAgree(String missionAgree) {
        this.missionAgree = missionAgree;
    }

    public String getVisionSuggestion() {
        return visionSuggestion;
    }

    public void setVisionSuggestion(String visionSuggestion) {
        this.visionSuggestion = visionSuggestion;
    }

    public String getMissionSuggestion() {
        return missionSuggestion;
    }

    public void setMissionSuggestion(String missionSuggestion) {
        this.missionSuggestion = missionSuggestion;
    }

    public String getPeoSuggestion() {
        return peoSuggestion;
    }

    public void setPeoSuggestion(String peoSuggestion) {
        this.peoSuggestion = peoSuggestion;
    }

    public FeedbackResponse getResponse1() {
        return response1;
    }

    public void setResponse1(FeedbackResponse response1) {
        this.response1 = response1;
    }

    public FeedbackResponse getResponse2() {
        return response2;
    }

    public void setResponse2(FeedbackResponse response2) {
        this.response2 = response2;
    }

    public FeedbackResponse getResponse3() {
        return response3;
    }

    public void setResponse3(FeedbackResponse response3) {
        this.response3 = response3;
    }

    public FeedbackResponse getResponse4() {
        return response4;
    }

    public void setResponse4(FeedbackResponse response4) {
        this.response4 = response4;
    }

    public FeedbackResponse getResponse5() {
        return response5;
    }

    public void setResponse5(FeedbackResponse response5) {
        this.response5 = response5;
    }

    public FeedbackResponse getResponse6() {
        return response6;
    }

    public void setResponse6(FeedbackResponse response6) {
        this.response6 = response6;
    }

    public FeedbackResponse getResponse7() {
        return response7;
    }

    public void setResponse7(FeedbackResponse response7) {
        this.response7 = response7;
    }

    public FeedbackResponse getResponse8() {
        return response8;
    }

    public void setResponse8(FeedbackResponse response8) {
        this.response8 = response8;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
