/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import org.springframework.security.core.GrantedAuthority;

public enum PageType implements GrantedAuthority {
    about_page,
    academic_calender_page,
    academic_governing_council_page,
    administration_page,
    aiml_page,
    alumni_page,
    ananya_page,
    anti_ragging_committee_page,
    anti_sexual_harassment_committee_page,
    cce_dept_page,
    cce_page,
    contact_us_page,
    course_page,
    csd_page,
    cse_page,
    csicb_page,
    disciplinary_measures_page,
    ece_page,
    fee_structure_page,
    grievance_page,
    hostel_page,
    iiic_page,
    institution_page,
    institutional_governance_page,
    iqac_page,
    library_page,
    mech_page,
    naac_cycle2_aqar_page,
    naac_cycle2_page,
    naac_cycle2_dvv_clarification_page,
    naac_cycle2_dvv_clarification_extended_profile_page,
    naac_page,
    nba_page,
    newsletter_page,
    nss_page,
    office_administration_page,
    online_admission_page,
    permissions_page,
    pg_page,
    placed_students_page,
    placement_drive_page,
    placement_page,
    placement_statistics_page,
    placement_upcoming_events_latest_news,
    public_press_page,
    redcross_page,
    science_page,
    sports_page,
    tele_page,
    transport_page,
    ug_page,
    home_page,
    about_ksrif_page,
    mca_dept_page,
    index2_page,
    approvals_page;

    @Override
    public String getAuthority() {
        return null;
    }
}
