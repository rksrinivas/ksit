/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

public enum FeedbackResponse {
    EXCELLENT("Excellent"),
    VERY_GOOD("Very Good"),
    GOOD("Good"),
    AVERAGE("Average");

    String desc;

    FeedbackResponse(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
