/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

public enum Criteria {
    CRITERIA1("Criteria 1", 1),
    CRITERIA2("Criteria 2", 2),
    CRITERIA3("Criteria 3", 3),
    CRITERIA4("Criteria 4", 4),
    CRITERIA5("Criteria 5", 5),
    CRITERIA6("Criteria 6", 6),
    CRITERIA7("Criteria 7", 7),
    CRITERIA8("Criteria 8", 8),
    CRITERIA9("Criteria 9", 9),
    CRITERIA10("Criteria 10", 10);

    String desc;
    int sortOrder;

    Criteria(String desc, int sortOrder) {
        this.desc = desc;
        this.sortOrder = sortOrder;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }
}
