/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "hostel_enrollment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "hibernateEntityCache")
public class HostelEnrollment {

	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@GeneratedValue(generator = "uuid")
	private String id;

	@Column(name = "college_name")
	private String collegeName;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "year")
	private String year;

	@Column(name = "sem")
	private String sem;

	@Column(name = "phone")
	private String phone;

	@Column(name = "email")
	private String email;

	@Column(name = "father_name")
	private String fatherName;

	@Column(name = "father_phone")
	private String fatherPhone;

	@Column(name = "mother_name")
	private String motherName;

	@Column(name = "mother_phone")
	private String motherPhone;

	@Column(name = "address")
	private String address;

	@Column(name = "created_on")
	private Date createdOn;

	public HostelEnrollment(String collegeName, String firstName, String lastName, String year, String sem,
			String phone, String email, String fatherName, String fatherPhone, String motherName, String motherPhone,
			String address) {
		super();
		this.collegeName = collegeName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.year = year;
		this.sem = sem;
		this.phone = phone;
		this.email = email;
		this.fatherName = fatherName;
		this.fatherPhone = fatherPhone;
		this.motherName = motherName;
		this.motherPhone = motherPhone;
		this.address = address;
		this.createdOn = Calendar.getInstance().getTime();
	}

	public HostelEnrollment() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getSem() {
		return sem;
	}

	public void setSem(String sem) {
		this.sem = sem;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFatherPhone() {
		return fatherPhone;
	}

	public void setFatherPhone(String fatherPhone) {
		this.fatherPhone = fatherPhone;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMotherPhone() {
		return motherPhone;
	}

	public void setMotherPhone(String motherPhone) {
		this.motherPhone = motherPhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
