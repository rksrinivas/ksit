/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "alumni")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "hibernateEntityCache")
public class Alumni {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "course")
    private String course;

    @Column(name = "branch")
    private String branch;

    @Column(name = "usn")
    private String usn;

    @Column(name = "year_of_passing")
    private String yearOfPassing;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "email_id")
    private String emailId;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "career_details")
    private String careerDetails;

    @Column(name = "created_on")
    private Date createdOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    public String getYearOfPassing() {
        return yearOfPassing;
    }

    public void setYearOfPassing(String yearOfPassing) {
        this.yearOfPassing = yearOfPassing;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCareerDetails() {
        return careerDetails;
    }

    public void setCareerDetails(String careerDetails) {
        this.careerDetails = careerDetails;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    //	public Alumni(String name, String email, String subject, String description, String gender,
    // String course, String branch, String yearOfPassing, String maritalStatus, String dateOfBirth,
    // String mobileNumber, String emailId, String presentAddress, String permanentAddress, String
    // city, String state, String country, String ideas, String designation, String companyName,
    // String domain, String workPhone, String workAddress, String careerDetails) {
    //		this.name = name;
    //		this.gender = gender;
    //		this.course = course;
    //		this.branch = branch;
    //		this.yearOfPassing = yearOfPassing;
    //		this.maritalStatus = maritalStatus;
    //		this.dateOfBirth = dateOfBirth;
    //		this.mobileNumber = mobileNumber;
    //		this.emailId = emailId;
    //		this.presentAddress=presentAddress;
    //		this.permanentAddress = permanentAddress;
    //		this.city=city;
    //		this.state=state;
    //		this.country=country;
    //		this.ideas=ideas;
    //		this.designation=designation;
    //		this.companyName=companyName;
    //		this.domain=domain;
    //		this.workPhone=workPhone;
    //		this.workAddress=workAddress;
    //		this.careerDetails=careerDetails;
    //
    //
    //		this.createdOn = Calendar.getInstance().getTime();
    //	}

}
