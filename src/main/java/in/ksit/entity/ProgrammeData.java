/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "programme_data")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "hibernateEntityCache")
public class ProgrammeData {
    public enum PROGRAMME_DATA_TYPE {
        SYLLABUS("Syllabus"),
        CLASS_TIME_TABLE("Class Time Table"),
        MODEL_QUESTION_PAPERS("Model Question Papers");

        String desc;

        PROGRAMME_DATA_TYPE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    };

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "programme_id", updatable = true)
    private Programme programme;

    @Enumerated(EnumType.STRING)
    @Column(name = "programme_type")
    private PROGRAMME_DATA_TYPE programmeDataType;

    @Column(name = "semester")
    private Integer semester;

    @Column(name = "start_year")
    private Integer startYear;

    @Column(name = "end_year")
    private Integer endYear;

    @Column(name = "data")
    private String data;

    public String getName() {
        return String.format("sem-%d (%d-%d)", semester, startYear, endYear);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Programme getProgramme() {
        return programme;
    }

    public void setProgramme(Programme programme) {
        this.programme = programme;
    }

    public PROGRAMME_DATA_TYPE getProgrammeDataType() {
        return programmeDataType;
    }

    public void setProgrammeDataType(PROGRAMME_DATA_TYPE programmeDataType) {
        this.programmeDataType = programmeDataType;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
