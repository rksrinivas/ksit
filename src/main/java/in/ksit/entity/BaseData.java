/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "base_data")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "data_type", discriminatorType = DiscriminatorType.STRING)
public class BaseData implements Serializable {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "field_name")
    private FieldNameType fieldName;

    @Enumerated(EnumType.STRING)
    @Column(name = "page")
    private PageType page;

    @Column(name = "active", columnDefinition = "BIT", length = 1)
    private boolean active;

    @Column(name = "hidden", columnDefinition = "BIT", length = 1)
    private boolean hidden;

    @Column(name = "ranking")
    private Integer rank;

    @Enumerated(EnumType.STRING)
    @Column(name = "data_type", updatable = false, insertable = false)
    private DataType dataType;

    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "last_updated_on")
    private Date lastUpdatedOn;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FieldNameType getFieldName() {
        return fieldName;
    }

    public void setFieldName(FieldNameType fieldName) {
        this.fieldName = fieldName;
    }

    public PageType getPage() {
        return page;
    }

    public void setPage(PageType page) {
        this.page = page;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
        result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
        result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
        result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
        result = prime * result + (hidden ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastUpdatedBy == null) ? 0 : lastUpdatedBy.hashCode());
        result = prime * result + ((lastUpdatedOn == null) ? 0 : lastUpdatedOn.hashCode());
        result = prime * result + ((page == null) ? 0 : page.hashCode());
        result = prime * result + ((rank == null) ? 0 : rank.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        BaseData other = (BaseData) obj;
        if (active != other.active) return false;
        if (createdBy == null) {
            if (other.createdBy != null) return false;
        } else if (!createdBy.equals(other.createdBy)) return false;
        if (createdOn == null) {
            if (other.createdOn != null) return false;
        } else if (!createdOn.equals(other.createdOn)) return false;
        if (dataType != other.dataType) return false;
        if (fieldName != other.fieldName) return false;
        if (hidden != other.hidden) return false;
        if (id == null) {
            if (other.id != null) return false;
        } else if (!id.equals(other.id)) return false;
        if (lastUpdatedBy == null) {
            if (other.lastUpdatedBy != null) return false;
        } else if (!lastUpdatedBy.equals(other.lastUpdatedBy)) return false;
        if (lastUpdatedOn == null) {
            if (other.lastUpdatedOn != null) return false;
        } else if (!lastUpdatedOn.equals(other.lastUpdatedOn)) return false;
        if (page != other.page) return false;
        if (rank == null) {
            if (other.rank != null) return false;
        } else if (!rank.equals(other.rank)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "BaseData [id="
                + id
                + ", fieldName="
                + fieldName
                + ", page="
                + page
                + ", active="
                + active
                + ", hidden="
                + hidden
                + ", rank="
                + rank
                + ", dataType="
                + dataType
                + ", createdOn="
                + createdOn
                + ", createdBy="
                + createdBy
                + ", lastUpdatedOn="
                + lastUpdatedOn
                + ", lastUpdatedBy="
                + lastUpdatedBy
                + "]";
    }
}
