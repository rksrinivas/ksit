/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

public enum DataType {
    DATA,
    EVENT,
    FACULTY,
    PLACEMENT,
    LAB,
    LAB_DATA
}
