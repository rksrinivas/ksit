/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@DiscriminatorValue("FACULTY")
@SecondaryTable(
        name = "faculty",
        pkJoinColumns = {@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")})
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "hibernateEntityCache")
public class Faculty extends BaseData {

    @Column(name = "name", table = "faculty")
    private String name;

    @Column(name = "designation", table = "faculty")
    private String designation;

    @Column(name = "qualification", table = "faculty")
    private String qualification;

    @Column(name = "department", table = "faculty")
    private String department;

    @Column(name = "image_url", table = "faculty")
    private String imageURL;

    @Column(name = "profile_url", table = "faculty")
    private String profileURL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getProfileURL() {
        return profileURL;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }
}
