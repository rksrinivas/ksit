/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import in.ksit.util.AppUtils;
import java.io.IOException;
import javax.ws.rs.FormParam;
import org.springframework.web.multipart.MultipartFile;

public class OnlineAdmissionsForm {

    // 1st tab

    @FormParam("admittedBranch")
    private String admittedBranch;

    @FormParam("cetRank")
    private String cetRank;

    @FormParam("photo")
    private MultipartFile photo;

    private String photoUrl;

    @FormParam("candidateName")
    private String candidateName;

    @FormParam("dob")
    private String dob;

    @FormParam("sex")
    private String sex;

    @FormParam("caste")
    private String caste;

    @FormParam("category")
    private String category;

    @FormParam("religion")
    private String religion;

    @FormParam("nationality")
    private String nationality;

    @FormParam("mobileNo")
    private String mobileNo;

    @FormParam("email")
    private String email;

    // 2nd tab

    @FormParam("fathersName")
    private String fathersName;

    @FormParam("fathersOccupation")
    private String fathersOccupation;

    @FormParam("fathersAnnualIncome")
    private String fathersAnnualIncome;

    @FormParam("fathersMobileNo")
    private String fathersMobileNo;

    @FormParam("fathersEmail")
    private String fathersEmail;

    // 3rd tab

    @FormParam("mothersName")
    private String mothersName;

    @FormParam("mothersOccupation")
    private String mothersOccupation;

    @FormParam("mothersAnnualIncome")
    private String mothersAnnualIncome;

    @FormParam("mothersMobileNo")
    private String mothersMobileNo;

    @FormParam("mothersEmail")
    private String mothersEmail;

    // 4th tab

    @FormParam("guardiansName")
    private String guardiansName;

    @FormParam("guardiansMobileNo")
    private String guardiansMobileNo;

    // 5th tab

    @FormParam("correspondenceAddress")
    private String correspondenceAddress;

    @FormParam("correspondencePinCode")
    private String correspondencePinCode;

    @FormParam("permanentAddress")
    private String permanentAddress;

    @FormParam("permanentPinCode")
    private String permanentPinCode;

    @FormParam("prevInstitution")
    private String prevInstitution;

    @FormParam("prevInstitutionBoard")
    private String prevInstitutionBoard;

    @FormParam("prevDateOfLeaving")
    private String prevDateOfLeaving;

    @FormParam("prevRegisterNo")
    private String prevRegisterNo;

    @FormParam("prevPassingYear")
    private String prevPassingYear;

    // 6th tab

    @FormParam("physicsMax")
    private Integer physicsMax;

    @FormParam("physicsMarks")
    private Integer physicsMarks;

    @FormParam("mathsMax")
    private Integer mathsMax;

    @FormParam("mathsMarks")
    private Integer mathsMarks;

    @FormParam("otherMax")
    private Integer otherMax;

    @FormParam("otherMarks")
    private Integer otherMarks;

    @FormParam("totalMax")
    private Integer totalMax;

    @FormParam("otherMarks")
    private Integer totalMarks;

    @FormParam("percentage")
    private String percentage;

    // 7th tab

    @FormParam("keaYear")
    private Integer keaYear;

    @FormParam("keaRegNo")
    private String keaRegNo;

    @FormParam("keaRank")
    private Integer keaRank;

    @FormParam("comedYear")
    private Integer comedYear;

    @FormParam("comedRegNo")
    private String comedRegNo;

    @FormParam("comedRank")
    private Integer comedRank;

    @FormParam("jeeYear")
    private Integer jeeYear;

    @FormParam("jeeRegNo")
    private String jeeRegNo;

    @FormParam("jeeRank")
    private Integer jeeRank;

    @FormParam("othersYear")
    private Integer othersYear;

    @FormParam("othersRegNo")
    private String othersRegNo;

    @FormParam("othersRank")
    private Integer othersRank;

    @FormParam("extraActivites")
    private String extraActivites;

    // 8th tab
    @FormParam("sslcMarksCard")
    private MultipartFile sslcMarksCard;

    private String sslcMarksCardUrl;

    @FormParam("pucMarksCard")
    private MultipartFile pucMarksCard;

    private String pucMarksCardUrl;

    @FormParam("rankCertificate")
    private MultipartFile rankCertificate;

    private String rankCertificateUrl;

    @FormParam("transferCertificate")
    private MultipartFile transferCertificate;

    private String transferCertificateUrl;

    @FormParam("migrationCertificate")
    private MultipartFile migrationCertificate;

    private String migrationCertificateUrl;

    @FormParam("physicalFitnessCertificate")
    private MultipartFile physicalFitnessCertificate;

    private String physicalFitnessCertificateUrl;

    @FormParam("foreignStudentsCertificate")
    private MultipartFile foreignStudentsCertificate;

    private String foreignStudentsCertificateUrl;

    @FormParam("transactionNo")
    private String transactionNo;

    @FormParam("screenshot")
    private MultipartFile screenshot;

    private String screenshotUrl;

    public String saveFile(MultipartFile file) {
        String fileUrl = null;
        if (file != null) {
            String fileName = file.getOriginalFilename();
            String type = "";
            if (fileName.contains(".")) {
                int index = fileName.lastIndexOf(".");
                type = fileName.substring(index);
            }

            long time = System.currentTimeMillis();

            String fileImageURL = AppUtils.getImageLocation() + time + type;
            String imageURL = "/images/" + time + type;
            try {
                AppUtils.writeToFile(file.getInputStream(), fileImageURL);
                fileUrl = imageURL;
            } catch (IOException e) {
            }
        }
        return fileUrl;
    }

    public void saveFiles() {
        photoUrl = saveFile(photo);
        sslcMarksCardUrl = saveFile(sslcMarksCard);
        pucMarksCardUrl = saveFile(pucMarksCard);
        rankCertificateUrl = saveFile(rankCertificate);
        transferCertificateUrl = saveFile(transferCertificate);
        migrationCertificateUrl = saveFile(migrationCertificate);
        physicalFitnessCertificateUrl = saveFile(physicalFitnessCertificate);
        foreignStudentsCertificateUrl = saveFile(foreignStudentsCertificate);
        screenshotUrl = saveFile(screenshot);
    }

    public String getAdmittedBranch() {
        return admittedBranch;
    }

    public void setAdmittedBranch(String admittedBranch) {
        this.admittedBranch = admittedBranch;
    }

    public String getCetRank() {
        return cetRank;
    }

    public void setCetRank(String cetRank) {
        this.cetRank = cetRank;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFathersName() {
        return fathersName;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public String getFathersOccupation() {
        return fathersOccupation;
    }

    public void setFathersOccupation(String fathersOccupation) {
        this.fathersOccupation = fathersOccupation;
    }

    public String getFathersAnnualIncome() {
        return fathersAnnualIncome;
    }

    public void setFathersAnnualIncome(String fathersAnnualIncome) {
        this.fathersAnnualIncome = fathersAnnualIncome;
    }

    public String getFathersMobileNo() {
        return fathersMobileNo;
    }

    public void setFathersMobileNo(String fathersMobileNo) {
        this.fathersMobileNo = fathersMobileNo;
    }

    public String getFathersEmail() {
        return fathersEmail;
    }

    public void setFathersEmail(String fathersEmail) {
        this.fathersEmail = fathersEmail;
    }

    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    public String getMothersOccupation() {
        return mothersOccupation;
    }

    public void setMothersOccupation(String mothersOccupation) {
        this.mothersOccupation = mothersOccupation;
    }

    public String getMothersAnnualIncome() {
        return mothersAnnualIncome;
    }

    public void setMothersAnnualIncome(String mothersAnnualIncome) {
        this.mothersAnnualIncome = mothersAnnualIncome;
    }

    public String getMothersMobileNo() {
        return mothersMobileNo;
    }

    public void setMothersMobileNo(String mothersMobileNo) {
        this.mothersMobileNo = mothersMobileNo;
    }

    public String getMothersEmail() {
        return mothersEmail;
    }

    public void setMothersEmail(String mothersEmail) {
        this.mothersEmail = mothersEmail;
    }

    public String getGuardiansName() {
        return guardiansName;
    }

    public void setGuardiansName(String guardiansName) {
        this.guardiansName = guardiansName;
    }

    public String getGuardiansMobileNo() {
        return guardiansMobileNo;
    }

    public void setGuardiansMobileNo(String guardiansMobileNo) {
        this.guardiansMobileNo = guardiansMobileNo;
    }

    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public String getCorrespondencePinCode() {
        return correspondencePinCode;
    }

    public void setCorrespondencePinCode(String correspondencePinCode) {
        this.correspondencePinCode = correspondencePinCode;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentPinCode() {
        return permanentPinCode;
    }

    public void setPermanentPinCode(String permanentPinCode) {
        this.permanentPinCode = permanentPinCode;
    }

    public String getPrevInstitution() {
        return prevInstitution;
    }

    public void setPrevInstitution(String prevInstitution) {
        this.prevInstitution = prevInstitution;
    }

    public String getPrevInstitutionBoard() {
        return prevInstitutionBoard;
    }

    public void setPrevInstitutionBoard(String prevInstitutionBoard) {
        this.prevInstitutionBoard = prevInstitutionBoard;
    }

    public String getPrevDateOfLeaving() {
        return prevDateOfLeaving;
    }

    public void setPrevDateOfLeaving(String prevDateOfLeaving) {
        this.prevDateOfLeaving = prevDateOfLeaving;
    }

    public String getPrevRegisterNo() {
        return prevRegisterNo;
    }

    public void setPrevRegisterNo(String prevRegisterNo) {
        this.prevRegisterNo = prevRegisterNo;
    }

    public String getPrevPassingYear() {
        return prevPassingYear;
    }

    public void setPrevPassingYear(String prevPassingYear) {
        this.prevPassingYear = prevPassingYear;
    }

    public Integer getPhysicsMax() {
        return physicsMax;
    }

    public void setPhysicsMax(Integer physicsMax) {
        this.physicsMax = physicsMax;
    }

    public Integer getPhysicsMarks() {
        return physicsMarks;
    }

    public void setPhysicsMarks(Integer physicsMarks) {
        this.physicsMarks = physicsMarks;
    }

    public Integer getMathsMax() {
        return mathsMax;
    }

    public void setMathsMax(Integer mathsMax) {
        this.mathsMax = mathsMax;
    }

    public Integer getMathsMarks() {
        return mathsMarks;
    }

    public void setMathsMarks(Integer mathsMarks) {
        this.mathsMarks = mathsMarks;
    }

    public Integer getOtherMax() {
        return otherMax;
    }

    public void setOtherMax(Integer otherMax) {
        this.otherMax = otherMax;
    }

    public Integer getOtherMarks() {
        return otherMarks;
    }

    public void setOtherMarks(Integer otherMarks) {
        this.otherMarks = otherMarks;
    }

    public Integer getTotalMax() {
        return totalMax;
    }

    public void setTotalMax(Integer totalMax) {
        this.totalMax = totalMax;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public Integer getKeaYear() {
        return keaYear;
    }

    public void setKeaYear(Integer keaYear) {
        this.keaYear = keaYear;
    }

    public String getKeaRegNo() {
        return keaRegNo;
    }

    public void setKeaRegNo(String keaRegNo) {
        this.keaRegNo = keaRegNo;
    }

    public Integer getKeaRank() {
        return keaRank;
    }

    public void setKeaRank(Integer keaRank) {
        this.keaRank = keaRank;
    }

    public Integer getComedYear() {
        return comedYear;
    }

    public void setComedYear(Integer comedYear) {
        this.comedYear = comedYear;
    }

    public String getComedRegNo() {
        return comedRegNo;
    }

    public void setComedRegNo(String comedRegNo) {
        this.comedRegNo = comedRegNo;
    }

    public Integer getComedRank() {
        return comedRank;
    }

    public void setComedRank(Integer comedRank) {
        this.comedRank = comedRank;
    }

    public Integer getJeeYear() {
        return jeeYear;
    }

    public void setJeeYear(Integer jeeYear) {
        this.jeeYear = jeeYear;
    }

    public String getJeeRegNo() {
        return jeeRegNo;
    }

    public void setJeeRegNo(String jeeRegNo) {
        this.jeeRegNo = jeeRegNo;
    }

    public Integer getJeeRank() {
        return jeeRank;
    }

    public void setJeeRank(Integer jeeRank) {
        this.jeeRank = jeeRank;
    }

    public Integer getOthersYear() {
        return othersYear;
    }

    public void setOthersYear(Integer othersYear) {
        this.othersYear = othersYear;
    }

    public String getOthersRegNo() {
        return othersRegNo;
    }

    public void setOthersRegNo(String othersRegNo) {
        this.othersRegNo = othersRegNo;
    }

    public Integer getOthersRank() {
        return othersRank;
    }

    public void setOthersRank(Integer othersRank) {
        this.othersRank = othersRank;
    }

    public String getExtraActivites() {
        return extraActivites;
    }

    public void setExtraActivites(String extraActivites) {
        this.extraActivites = extraActivites;
    }

    public MultipartFile getSslcMarksCard() {
        return sslcMarksCard;
    }

    public void setSslcMarksCard(MultipartFile sslcMarksCard) {
        this.sslcMarksCard = sslcMarksCard;
    }

    public String getSslcMarksCardUrl() {
        return sslcMarksCardUrl;
    }

    public void setSslcMarksCardUrl(String sslcMarksCardUrl) {
        this.sslcMarksCardUrl = sslcMarksCardUrl;
    }

    public MultipartFile getPucMarksCard() {
        return pucMarksCard;
    }

    public void setPucMarksCard(MultipartFile pucMarksCard) {
        this.pucMarksCard = pucMarksCard;
    }

    public String getPucMarksCardUrl() {
        return pucMarksCardUrl;
    }

    public void setPucMarksCardUrl(String pucMarksCardUrl) {
        this.pucMarksCardUrl = pucMarksCardUrl;
    }

    public MultipartFile getRankCertificate() {
        return rankCertificate;
    }

    public void setRankCertificate(MultipartFile rankCertificate) {
        this.rankCertificate = rankCertificate;
    }

    public String getRankCertificateUrl() {
        return rankCertificateUrl;
    }

    public void setRankCertificateUrl(String rankCertificateUrl) {
        this.rankCertificateUrl = rankCertificateUrl;
    }

    public MultipartFile getTransferCertificate() {
        return transferCertificate;
    }

    public void setTransferCertificate(MultipartFile transferCertificate) {
        this.transferCertificate = transferCertificate;
    }

    public String getTransferCertificateUrl() {
        return transferCertificateUrl;
    }

    public void setTransferCertificateUrl(String transferCertificateUrl) {
        this.transferCertificateUrl = transferCertificateUrl;
    }

    public MultipartFile getMigrationCertificate() {
        return migrationCertificate;
    }

    public void setMigrationCertificate(MultipartFile migrationCertificate) {
        this.migrationCertificate = migrationCertificate;
    }

    public String getMigrationCertificateUrl() {
        return migrationCertificateUrl;
    }

    public void setMigrationCertificateUrl(String migrationCertificateUrl) {
        this.migrationCertificateUrl = migrationCertificateUrl;
    }

    public MultipartFile getPhysicalFitnessCertificate() {
        return physicalFitnessCertificate;
    }

    public void setPhysicalFitnessCertificate(MultipartFile physicalFitnessCertificate) {
        this.physicalFitnessCertificate = physicalFitnessCertificate;
    }

    public String getPhysicalFitnessCertificateUrl() {
        return physicalFitnessCertificateUrl;
    }

    public void setPhysicalFitnessCertificateUrl(String physicalFitnessCertificateUrl) {
        this.physicalFitnessCertificateUrl = physicalFitnessCertificateUrl;
    }

    public MultipartFile getForeignStudentsCertificate() {
        return foreignStudentsCertificate;
    }

    public void setForeignStudentsCertificate(MultipartFile foreignStudentsCertificate) {
        this.foreignStudentsCertificate = foreignStudentsCertificate;
    }

    public String getForeignStudentsCertificateUrl() {
        return foreignStudentsCertificateUrl;
    }

    public void setForeignStudentsCertificateUrl(String foreignStudentsCertificateUrl) {
        this.foreignStudentsCertificateUrl = foreignStudentsCertificateUrl;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public MultipartFile getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(MultipartFile screenshot) {
        this.screenshot = screenshot;
    }

    public String getScreenshotUrl() {
        return screenshotUrl;
    }

    public void setScreenshotUrl(String screenshotUrl) {
        this.screenshotUrl = screenshotUrl;
    }
}
