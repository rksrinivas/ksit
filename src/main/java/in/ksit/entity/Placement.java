/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@DiscriminatorValue("PLACEMENT")
@SecondaryTable(
        name = "placement",
        pkJoinColumns = {@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")})
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "hibernateEntityCache")
public class Placement extends BaseData implements Serializable {

    @Column(name = "company", table = "placement")
    private String company;

    @Column(name = "salary", table = "placement")
    private String salary;

    @Column(name = "no_of_students", table = "placement")
    private String noOfStudents;

    @Column(name = "year", table = "placement")
    private Integer year;

    @Column(name = "program", table = "placement")
    private String program;

    @Column(name = "semester", table = "placement")
    private Integer semester;

    @Column(name = "schedule", table = "placement")
    private String schedule;

    @Column(name = "image_url", table = "placement")
    private String imageURL;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getNoOfStudents() {
        return noOfStudents;
    }

    public void setNoOfStudents(String noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((company == null) ? 0 : company.hashCode());
        result = prime * result + ((imageURL == null) ? 0 : imageURL.hashCode());
        result = prime * result + ((noOfStudents == null) ? 0 : noOfStudents.hashCode());
        result = prime * result + ((program == null) ? 0 : program.hashCode());
        result = prime * result + ((salary == null) ? 0 : salary.hashCode());
        result = prime * result + ((schedule == null) ? 0 : schedule.hashCode());
        result = prime * result + ((semester == null) ? 0 : semester.hashCode());
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        Placement other = (Placement) obj;
        if (company == null) {
            if (other.company != null) return false;
        } else if (!company.equals(other.company)) return false;
        if (imageURL == null) {
            if (other.imageURL != null) return false;
        } else if (!imageURL.equals(other.imageURL)) return false;
        if (noOfStudents == null) {
            if (other.noOfStudents != null) return false;
        } else if (!noOfStudents.equals(other.noOfStudents)) return false;
        if (program == null) {
            if (other.program != null) return false;
        } else if (!program.equals(other.program)) return false;
        if (salary == null) {
            if (other.salary != null) return false;
        } else if (!salary.equals(other.salary)) return false;
        if (schedule == null) {
            if (other.schedule != null) return false;
        } else if (!schedule.equals(other.schedule)) return false;
        if (semester == null) {
            if (other.semester != null) return false;
        } else if (!semester.equals(other.semester)) return false;
        if (year == null) {
            if (other.year != null) return false;
        } else if (!year.equals(other.year)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "Placement [company="
                + company
                + ", salary="
                + salary
                + ", noOfStudents="
                + noOfStudents
                + ", year="
                + year
                + ", program="
                + program
                + ", semester="
                + semester
                + ", schedule="
                + schedule
                + ", imageURL="
                + imageURL
                + "]";
    }
}
