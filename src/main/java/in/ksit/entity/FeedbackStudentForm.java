/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import javax.persistence.Column;
import javax.ws.rs.FormParam;

public class FeedbackStudentForm {

    @FormParam("page")
    private PageType page;

    @FormParam("nameOfTheStudent")
    private String nameOfTheStudent;

    @FormParam("semesterAndSection")
    private String semesterAndSection;

    @FormParam("academicYear")
    private String academicYear;

    @FormParam("usn")
    private String usn;

    @FormParam("emailId")
    private String emailId;

    @FormParam("phoneNo")
    private String phoneNo;

    @FormParam("visionMission")
    private String visionMission;

    @FormParam("visionAgree")
    private String visionAgree;

    @FormParam("missionAgree ")
    private String missionAgree;

    @FormParam("visionSuggestion")
    private String visionSuggestion;

    @FormParam("missionSuggestion")
    private String missionSuggestion;

    @FormParam("peoSuggestion")
    private String peoSuggestion;

    @Column(name = "r1")
    private FeedbackResponse response1;

    @Column(name = "r2")
    private FeedbackResponse response2;

    @Column(name = "r3")
    private FeedbackResponse response3;

    @Column(name = "r4")
    private FeedbackResponse response4;

    @Column(name = "r5")
    private FeedbackResponse response5;

    @Column(name = "r6")
    private FeedbackResponse response6;

    @Column(name = "r7")
    private FeedbackResponse response7;

    @Column(name = "r8")
    private FeedbackResponse response8;

    @Column(name = "r9")
    private FeedbackResponse response9;

    @Column(name = "r10")
    private FeedbackResponse response10;

    @Column(name = "r11")
    private FeedbackResponse response11;

    @Column(name = "r12")
    private FeedbackResponse response12;

    @Column(name = "r13")
    private FeedbackResponse response13;

    @Column(name = "r14")
    private FeedbackResponse response14;

    @Column(name = "response")
    private String response;

    public PageType getPage() {
        return page;
    }

    public void setPage(PageType page) {
        this.page = page;
    }

    public String getNameOfTheStudent() {
        return nameOfTheStudent;
    }

    public void setNameOfTheStudent(String nameOfTheStudent) {
        this.nameOfTheStudent = nameOfTheStudent;
    }

    public String getSemesterAndSection() {
        return semesterAndSection;
    }

    public void setSemesterAndSection(String semesterAndSection) {
        this.semesterAndSection = semesterAndSection;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getVisionMission() {
        return visionMission;
    }

    public void setVisionMission(String visionMission) {
        this.visionMission = visionMission;
    }

    public String getVisionAgree() {
        return visionAgree;
    }

    public void setVisionAgree(String visionAgree) {
        this.visionAgree = visionAgree;
    }

    public String getMissionAgree() {
        return missionAgree;
    }

    public void setMissionAgree(String missionAgree) {
        this.missionAgree = missionAgree;
    }

    public String getVisionSuggestion() {
        return visionSuggestion;
    }

    public void setVisionSuggestion(String visionSuggestion) {
        this.visionSuggestion = visionSuggestion;
    }

    public String getMissionSuggestion() {
        return missionSuggestion;
    }

    public void setMissionSuggestion(String missionSuggestion) {
        this.missionSuggestion = missionSuggestion;
    }

    public String getPeoSuggestion() {
        return peoSuggestion;
    }

    public void setPeoSuggestion(String peoSuggestion) {
        this.peoSuggestion = peoSuggestion;
    }

    public FeedbackResponse getResponse1() {
        return response1;
    }

    public void setResponse1(FeedbackResponse response1) {
        this.response1 = response1;
    }

    public FeedbackResponse getResponse2() {
        return response2;
    }

    public void setResponse2(FeedbackResponse response2) {
        this.response2 = response2;
    }

    public FeedbackResponse getResponse3() {
        return response3;
    }

    public void setResponse3(FeedbackResponse response3) {
        this.response3 = response3;
    }

    public FeedbackResponse getResponse4() {
        return response4;
    }

    public void setResponse4(FeedbackResponse response4) {
        this.response4 = response4;
    }

    public FeedbackResponse getResponse5() {
        return response5;
    }

    public void setResponse5(FeedbackResponse response5) {
        this.response5 = response5;
    }

    public FeedbackResponse getResponse6() {
        return response6;
    }

    public void setResponse6(FeedbackResponse response6) {
        this.response6 = response6;
    }

    public FeedbackResponse getResponse7() {
        return response7;
    }

    public void setResponse7(FeedbackResponse response7) {
        this.response7 = response7;
    }

    public FeedbackResponse getResponse8() {
        return response8;
    }

    public void setResponse8(FeedbackResponse response8) {
        this.response8 = response8;
    }

    public FeedbackResponse getResponse9() {
        return response9;
    }

    public void setResponse9(FeedbackResponse response9) {
        this.response9 = response9;
    }

    public FeedbackResponse getResponse10() {
        return response10;
    }

    public void setResponse10(FeedbackResponse response10) {
        this.response10 = response10;
    }

    public FeedbackResponse getResponse11() {
        return response11;
    }

    public void setResponse11(FeedbackResponse response11) {
        this.response11 = response11;
    }

    public FeedbackResponse getResponse12() {
        return response12;
    }

    public void setResponse12(FeedbackResponse response12) {
        this.response12 = response12;
    }

    public FeedbackResponse getResponse13() {
        return response13;
    }

    public void setResponse13(FeedbackResponse response13) {
        this.response13 = response13;
    }

    public FeedbackResponse getResponse14() {
        return response14;
    }

    public void setResponse14(FeedbackResponse response14) {
        this.response14 = response14;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
