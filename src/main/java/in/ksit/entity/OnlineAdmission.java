/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "online_admission")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE, region = "hibernateEntityCache")
public class OnlineAdmission {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    @Column(name = "created_on")
    private Date createdOn;

    private String admittedBranch;

    private String cetRank;

    private String photoUrl;

    private String candidateName;

    private String dob;

    private String gender;

    private String caste;

    private String category;

    private String religion;

    private String nationality;

    private String mobileNo;

    private String email;

    private String fathersName;

    private String fathersOccupation;

    private String fathersAnnualIncome;

    private String fathersMobileNo;

    private String fathersEmail;

    private String mothersName;

    private String mothersOccupation;

    private String mothersAnnualIncome;

    private String mothersMobileNo;

    private String mothersEmail;

    private String guardiansName;

    private String guardiansMobileNo;

    private String correspondenceAddress;

    private String correspondencePinCode;

    private String permanentAddress;

    private String permanentPinCode;

    private String prevInstitution;

    private String prevInstitutionBoard;

    private String prevDateOfLeaving;

    private String prevRegisterNo;

    private String prevPassingYear;

    private String physicsMax;

    private String physicsMarks;

    private String mathsMax;

    private String mathsMarks;

    private String otherMax;

    private String otherMarks;

    private String totalMax;

    private String totalMarks;

    private String percentage;

    private String keaYear;

    private String keaRegNo;

    private String keaRank;

    private String comedYear;

    private String comedRegNo;

    private String comedRank;

    private String jeeYear;

    private String jeeRegNo;

    private String jeeRank;

    private String othersYear;

    private String othersRegNo;

    private String othersRank;

    private String extraActivites;

    private String sslcMarksCardUrl;

    private String pucMarksCardUrl;

    private String rankCertificateUrl;

    private String transferCertificateUrl;

    private String migrationCertificateUrl;

    private String physicalFitnessCertificateUrl;

    private String foreignStudentsCertificateUrl;

    private String transactionNo;

    private String screenshotUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getAdmittedBranch() {
        return admittedBranch;
    }

    public void setAdmittedBranch(String admittedBranch) {
        this.admittedBranch = admittedBranch;
    }

    public String getCetRank() {
        return cetRank;
    }

    public void setCetRank(String cetRank) {
        this.cetRank = cetRank;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFathersName() {
        return fathersName;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public String getFathersOccupation() {
        return fathersOccupation;
    }

    public void setFathersOccupation(String fathersOccupation) {
        this.fathersOccupation = fathersOccupation;
    }

    public String getFathersAnnualIncome() {
        return fathersAnnualIncome;
    }

    public void setFathersAnnualIncome(String fathersAnnualIncome) {
        this.fathersAnnualIncome = fathersAnnualIncome;
    }

    public String getFathersMobileNo() {
        return fathersMobileNo;
    }

    public void setFathersMobileNo(String fathersMobileNo) {
        this.fathersMobileNo = fathersMobileNo;
    }

    public String getFathersEmail() {
        return fathersEmail;
    }

    public void setFathersEmail(String fathersEmail) {
        this.fathersEmail = fathersEmail;
    }

    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    public String getMothersOccupation() {
        return mothersOccupation;
    }

    public void setMothersOccupation(String mothersOccupation) {
        this.mothersOccupation = mothersOccupation;
    }

    public String getMothersAnnualIncome() {
        return mothersAnnualIncome;
    }

    public void setMothersAnnualIncome(String mothersAnnualIncome) {
        this.mothersAnnualIncome = mothersAnnualIncome;
    }

    public String getMothersMobileNo() {
        return mothersMobileNo;
    }

    public void setMothersMobileNo(String mothersMobileNo) {
        this.mothersMobileNo = mothersMobileNo;
    }

    public String getMothersEmail() {
        return mothersEmail;
    }

    public void setMothersEmail(String mothersEmail) {
        this.mothersEmail = mothersEmail;
    }

    public String getGuardiansName() {
        return guardiansName;
    }

    public void setGuardiansName(String guardiansName) {
        this.guardiansName = guardiansName;
    }

    public String getGuardiansMobileNo() {
        return guardiansMobileNo;
    }

    public void setGuardiansMobileNo(String guardiansMobileNo) {
        this.guardiansMobileNo = guardiansMobileNo;
    }

    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public String getCorrespondencePinCode() {
        return correspondencePinCode;
    }

    public void setCorrespondencePinCode(String correspondencePinCode) {
        this.correspondencePinCode = correspondencePinCode;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentPinCode() {
        return permanentPinCode;
    }

    public void setPermanentPinCode(String permanentPinCode) {
        this.permanentPinCode = permanentPinCode;
    }

    public String getPrevInstitution() {
        return prevInstitution;
    }

    public void setPrevInstitution(String prevInstitution) {
        this.prevInstitution = prevInstitution;
    }

    public String getPrevInstitutionBoard() {
        return prevInstitutionBoard;
    }

    public void setPrevInstitutionBoard(String prevInstitutionBoard) {
        this.prevInstitutionBoard = prevInstitutionBoard;
    }

    public String getPrevDateOfLeaving() {
        return prevDateOfLeaving;
    }

    public void setPrevDateOfLeaving(String prevDateOfLeaving) {
        this.prevDateOfLeaving = prevDateOfLeaving;
    }

    public String getPrevRegisterNo() {
        return prevRegisterNo;
    }

    public void setPrevRegisterNo(String prevRegisterNo) {
        this.prevRegisterNo = prevRegisterNo;
    }

    public String getPrevPassingYear() {
        return prevPassingYear;
    }

    public void setPrevPassingYear(String prevPassingYear) {
        this.prevPassingYear = prevPassingYear;
    }

    public String getPhysicsMax() {
        return physicsMax;
    }

    public void setPhysicsMax(String physicsMax) {
        this.physicsMax = physicsMax;
    }

    public String getPhysicsMarks() {
        return physicsMarks;
    }

    public void setPhysicsMarks(String physicsMarks) {
        this.physicsMarks = physicsMarks;
    }

    public String getMathsMax() {
        return mathsMax;
    }

    public void setMathsMax(String mathsMax) {
        this.mathsMax = mathsMax;
    }

    public String getMathsMarks() {
        return mathsMarks;
    }

    public void setMathsMarks(String mathsMarks) {
        this.mathsMarks = mathsMarks;
    }

    public String getOtherMax() {
        return otherMax;
    }

    public void setOtherMax(String otherMax) {
        this.otherMax = otherMax;
    }

    public String getOtherMarks() {
        return otherMarks;
    }

    public void setOtherMarks(String otherMarks) {
        this.otherMarks = otherMarks;
    }

    public String getTotalMax() {
        return totalMax;
    }

    public void setTotalMax(String totalMax) {
        this.totalMax = totalMax;
    }

    public String getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getKeaYear() {
        return keaYear;
    }

    public void setKeaYear(String keaYear) {
        this.keaYear = keaYear;
    }

    public String getKeaRegNo() {
        return keaRegNo;
    }

    public void setKeaRegNo(String keaRegNo) {
        this.keaRegNo = keaRegNo;
    }

    public String getKeaRank() {
        return keaRank;
    }

    public void setKeaRank(String keaRank) {
        this.keaRank = keaRank;
    }

    public String getComedYear() {
        return comedYear;
    }

    public void setComedYear(String comedYear) {
        this.comedYear = comedYear;
    }

    public String getComedRegNo() {
        return comedRegNo;
    }

    public void setComedRegNo(String comedRegNo) {
        this.comedRegNo = comedRegNo;
    }

    public String getComedRank() {
        return comedRank;
    }

    public void setComedRank(String comedRank) {
        this.comedRank = comedRank;
    }

    public String getJeeYear() {
        return jeeYear;
    }

    public void setJeeYear(String jeeYear) {
        this.jeeYear = jeeYear;
    }

    public String getJeeRegNo() {
        return jeeRegNo;
    }

    public void setJeeRegNo(String jeeRegNo) {
        this.jeeRegNo = jeeRegNo;
    }

    public String getJeeRank() {
        return jeeRank;
    }

    public void setJeeRank(String jeeRank) {
        this.jeeRank = jeeRank;
    }

    public String getOthersYear() {
        return othersYear;
    }

    public void setOthersYear(String othersYear) {
        this.othersYear = othersYear;
    }

    public String getOthersRegNo() {
        return othersRegNo;
    }

    public void setOthersRegNo(String othersRegNo) {
        this.othersRegNo = othersRegNo;
    }

    public String getOthersRank() {
        return othersRank;
    }

    public void setOthersRank(String othersRank) {
        this.othersRank = othersRank;
    }

    public String getExtraActivites() {
        return extraActivites;
    }

    public void setExtraActivites(String extraActivites) {
        this.extraActivites = extraActivites;
    }

    public String getSslcMarksCardUrl() {
        return sslcMarksCardUrl;
    }

    public void setSslcMarksCardUrl(String sslcMarksCardUrl) {
        this.sslcMarksCardUrl = sslcMarksCardUrl;
    }

    public String getPucMarksCardUrl() {
        return pucMarksCardUrl;
    }

    public void setPucMarksCardUrl(String pucMarksCardUrl) {
        this.pucMarksCardUrl = pucMarksCardUrl;
    }

    public String getRankCertificateUrl() {
        return rankCertificateUrl;
    }

    public void setRankCertificateUrl(String rankCertificateUrl) {
        this.rankCertificateUrl = rankCertificateUrl;
    }

    public String getTransferCertificateUrl() {
        return transferCertificateUrl;
    }

    public void setTransferCertificateUrl(String transferCertificateUrl) {
        this.transferCertificateUrl = transferCertificateUrl;
    }

    public String getMigrationCertificateUrl() {
        return migrationCertificateUrl;
    }

    public void setMigrationCertificateUrl(String migrationCertificateUrl) {
        this.migrationCertificateUrl = migrationCertificateUrl;
    }

    public String getPhysicalFitnessCertificateUrl() {
        return physicalFitnessCertificateUrl;
    }

    public void setPhysicalFitnessCertificateUrl(String physicalFitnessCertificateUrl) {
        this.physicalFitnessCertificateUrl = physicalFitnessCertificateUrl;
    }

    public String getForeignStudentsCertificateUrl() {
        return foreignStudentsCertificateUrl;
    }

    public void setForeignStudentsCertificateUrl(String foreignStudentsCertificateUrl) {
        this.foreignStudentsCertificateUrl = foreignStudentsCertificateUrl;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getScreenshotUrl() {
        return screenshotUrl;
    }

    public void setScreenshotUrl(String screenshotUrl) {
        this.screenshotUrl = screenshotUrl;
    }
}
