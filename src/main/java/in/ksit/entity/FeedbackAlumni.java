/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Feedback_Alumni")
public class FeedbackAlumni {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "page")
    private PageType page;

    private String studentName;

    private String yearOfGraduation;

    private String positionInGraduation;

    private String usn;

    private String emailId;

    private String address;

    private String mobileNumber;

    private String currentOrganisation;

    private String designation;

    private String visionMission;

    private String visionAgree;

    private String missionAgree;

    private String visionSuggestion;

    private String missionSuggestion;

    private String peoSuggestion;

    @Enumerated(EnumType.STRING)
    @Column(name = "k_r1")
    private FeedbackResponse knowledgeResponse1;

    @Enumerated(EnumType.STRING)
    @Column(name = "k_r2")
    private FeedbackResponse knowledgeResponse2;

    @Enumerated(EnumType.STRING)
    @Column(name = "k_r3")
    private FeedbackResponse knowledgeResponse3;

    @Enumerated(EnumType.STRING)
    @Column(name = "k_r4")
    private FeedbackResponse knowledgeResponse4;

    @Enumerated(EnumType.STRING)
    @Column(name = "k_r5")
    private FeedbackResponse knowledgeResponse5;

    @Enumerated(EnumType.STRING)
    @Column(name = "k_r6")
    private FeedbackResponse knowledgeResponse6;

    @Enumerated(EnumType.STRING)
    @Column(name = "c_r1")
    private FeedbackResponse communicationResponse1;

    @Enumerated(EnumType.STRING)
    @Column(name = "c_r2")
    private FeedbackResponse communicationResponse2;

    @Enumerated(EnumType.STRING)
    @Column(name = "c_r3")
    private FeedbackResponse communicationResponse3;

    @Enumerated(EnumType.STRING)
    @Column(name = "ip_r1")
    private FeedbackResponse interPersonalResponse1;

    @Enumerated(EnumType.STRING)
    @Column(name = "ip_r2")
    private FeedbackResponse interPersonalResponse2;

    @Enumerated(EnumType.STRING)
    @Column(name = "ip_r3")
    private FeedbackResponse interPersonalResponse3;

    @Enumerated(EnumType.STRING)
    @Column(name = "ip_r4")
    private FeedbackResponse interPersonalResponse4;

    @Enumerated(EnumType.STRING)
    @Column(name = "m_r1")
    private FeedbackResponse managementResponse1;

    @Enumerated(EnumType.STRING)
    @Column(name = "m_r2")
    private FeedbackResponse managementResponse2;

    @Enumerated(EnumType.STRING)
    @Column(name = "m_r3")
    private FeedbackResponse managementResponse3;

    @Column(name = "response")
    private String response;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PageType getPage() {
        return page;
    }

    public void setPage(PageType page) {
        this.page = page;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getYearOfGraduation() {
        return yearOfGraduation;
    }

    public void setYearOfGraduation(String yearOfGraduation) {
        this.yearOfGraduation = yearOfGraduation;
    }

    public String getPositionInGraduation() {
        return positionInGraduation;
    }

    public void setPositionInGraduation(String positionInGraduation) {
        this.positionInGraduation = positionInGraduation;
    }

    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCurrentOrganisation() {
        return currentOrganisation;
    }

    public void setCurrentOrganisation(String currentOrganisation) {
        this.currentOrganisation = currentOrganisation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getVisionMission() {
        return visionMission;
    }

    public void setVisionMission(String visionMission) {
        this.visionMission = visionMission;
    }

    public String getVisionAgree() {
        return visionAgree;
    }

    public void setVisionAgree(String visionAgree) {
        this.visionAgree = visionAgree;
    }

    public String getMissionAgree() {
        return missionAgree;
    }

    public void setMissionAgree(String missionAgree) {
        this.missionAgree = missionAgree;
    }

    public String getVisionSuggestion() {
        return visionSuggestion;
    }

    public void setVisionSuggestion(String visionSuggestion) {
        this.visionSuggestion = visionSuggestion;
    }

    public String getMissionSuggestion() {
        return missionSuggestion;
    }

    public void setMissionSuggestion(String missionSuggestion) {
        this.missionSuggestion = missionSuggestion;
    }

    public String getPeoSuggestion() {
        return peoSuggestion;
    }

    public void setPeoSuggestion(String peoSuggestion) {
        this.peoSuggestion = peoSuggestion;
    }

    public FeedbackResponse getKnowledgeResponse1() {
        return knowledgeResponse1;
    }

    public void setKnowledgeResponse1(FeedbackResponse knowledgeResponse1) {
        this.knowledgeResponse1 = knowledgeResponse1;
    }

    public FeedbackResponse getKnowledgeResponse2() {
        return knowledgeResponse2;
    }

    public void setKnowledgeResponse2(FeedbackResponse knowledgeResponse2) {
        this.knowledgeResponse2 = knowledgeResponse2;
    }

    public FeedbackResponse getKnowledgeResponse3() {
        return knowledgeResponse3;
    }

    public void setKnowledgeResponse3(FeedbackResponse knowledgeResponse3) {
        this.knowledgeResponse3 = knowledgeResponse3;
    }

    public FeedbackResponse getKnowledgeResponse4() {
        return knowledgeResponse4;
    }

    public void setKnowledgeResponse4(FeedbackResponse knowledgeResponse4) {
        this.knowledgeResponse4 = knowledgeResponse4;
    }

    public FeedbackResponse getKnowledgeResponse5() {
        return knowledgeResponse5;
    }

    public void setKnowledgeResponse5(FeedbackResponse knowledgeResponse5) {
        this.knowledgeResponse5 = knowledgeResponse5;
    }

    public FeedbackResponse getKnowledgeResponse6() {
        return knowledgeResponse6;
    }

    public void setKnowledgeResponse6(FeedbackResponse knowledgeResponse6) {
        this.knowledgeResponse6 = knowledgeResponse6;
    }

    public FeedbackResponse getCommunicationResponse1() {
        return communicationResponse1;
    }

    public void setCommunicationResponse1(FeedbackResponse communicationResponse1) {
        this.communicationResponse1 = communicationResponse1;
    }

    public FeedbackResponse getCommunicationResponse2() {
        return communicationResponse2;
    }

    public void setCommunicationResponse2(FeedbackResponse communicationResponse2) {
        this.communicationResponse2 = communicationResponse2;
    }

    public FeedbackResponse getCommunicationResponse3() {
        return communicationResponse3;
    }

    public void setCommunicationResponse3(FeedbackResponse communicationResponse3) {
        this.communicationResponse3 = communicationResponse3;
    }

    public FeedbackResponse getInterPersonalResponse1() {
        return interPersonalResponse1;
    }

    public void setInterPersonalResponse1(FeedbackResponse interPersonalResponse1) {
        this.interPersonalResponse1 = interPersonalResponse1;
    }

    public FeedbackResponse getInterPersonalResponse2() {
        return interPersonalResponse2;
    }

    public void setInterPersonalResponse2(FeedbackResponse interPersonalResponse2) {
        this.interPersonalResponse2 = interPersonalResponse2;
    }

    public FeedbackResponse getInterPersonalResponse3() {
        return interPersonalResponse3;
    }

    public void setInterPersonalResponse3(FeedbackResponse interPersonalResponse3) {
        this.interPersonalResponse3 = interPersonalResponse3;
    }

    public FeedbackResponse getInterPersonalResponse4() {
        return interPersonalResponse4;
    }

    public void setInterPersonalResponse4(FeedbackResponse interPersonalResponse4) {
        this.interPersonalResponse4 = interPersonalResponse4;
    }

    public FeedbackResponse getManagementResponse1() {
        return managementResponse1;
    }

    public void setManagementResponse1(FeedbackResponse managementResponse1) {
        this.managementResponse1 = managementResponse1;
    }

    public FeedbackResponse getManagementResponse2() {
        return managementResponse2;
    }

    public void setManagementResponse2(FeedbackResponse managementResponse2) {
        this.managementResponse2 = managementResponse2;
    }

    public FeedbackResponse getManagementResponse3() {
        return managementResponse3;
    }

    public void setManagementResponse3(FeedbackResponse managementResponse3) {
        this.managementResponse3 = managementResponse3;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
