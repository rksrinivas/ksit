/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.FeedbackParent;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FeedbackParentDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(FeedbackParent feedbackParent) {
        sessionFactory.getCurrentSession().saveOrUpdate(feedbackParent);
    }

    @Transactional
    public void delete(FeedbackParent feedbackParent) {
        sessionFactory.getCurrentSession().delete(feedbackParent);
    }

    @Transactional
    public List<FeedbackParent> findByPage(PageType page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackParent> query = builder.createQuery(FeedbackParent.class);
        Root<FeedbackParent> root = query.from(FeedbackParent.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("page"), page));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<FeedbackParent> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public FeedbackParent getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackParent> query = builder.createQuery(FeedbackParent.class);
        Root<FeedbackParent> root = query.from(FeedbackParent.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<FeedbackParent> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
