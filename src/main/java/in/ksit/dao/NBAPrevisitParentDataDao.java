/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.NBAPrevisitParentData;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class NBAPrevisitParentDataDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(NBAPrevisitParentData nbaPrevisitParentData) {
        sessionFactory.getCurrentSession().saveOrUpdate(nbaPrevisitParentData);
    }

    @Transactional(readOnly = true)
    public List<NBAPrevisitParentData> getByParent(String parentId) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBAPrevisitParentData> query =
                builder.createQuery(NBAPrevisitParentData.class);
        Root<NBAPrevisitParentData> root = query.from(NBAPrevisitParentData.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("parentId"), parentId));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NBAPrevisitParentData> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public List<NBAPrevisitParentData> getByChild(String childId) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBAPrevisitParentData> query =
                builder.createQuery(NBAPrevisitParentData.class);
        Root<NBAPrevisitParentData> root = query.from(NBAPrevisitParentData.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("childId"), childId));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NBAPrevisitParentData> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public NBAPrevisitParentData getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBAPrevisitParentData> query =
                builder.createQuery(NBAPrevisitParentData.class);
        Root<NBAPrevisitParentData> root = query.from(NBAPrevisitParentData.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<NBAPrevisitParentData> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }

    @Transactional
    public void delete(NBAPrevisitParentData nbaPrevisitParentData) {
        sessionFactory.getCurrentSession().delete(nbaPrevisitParentData);
    }
}
