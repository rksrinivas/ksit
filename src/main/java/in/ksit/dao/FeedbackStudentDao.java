/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.FeedbackStudent;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FeedbackStudentDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(FeedbackStudent feedbackStudent) {
        sessionFactory.getCurrentSession().saveOrUpdate(feedbackStudent);
    }

    @Transactional
    public void delete(FeedbackStudent feedbackStudent) {
        sessionFactory.getCurrentSession().delete(feedbackStudent);
    }

    @Transactional
    public List<FeedbackStudent> findByPage(PageType page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackStudent> query = builder.createQuery(FeedbackStudent.class);
        Root<FeedbackStudent> root = query.from(FeedbackStudent.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("page"), page));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<FeedbackStudent> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public FeedbackStudent getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackStudent> query = builder.createQuery(FeedbackStudent.class);
        Root<FeedbackStudent> root = query.from(FeedbackStudent.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<FeedbackStudent> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
