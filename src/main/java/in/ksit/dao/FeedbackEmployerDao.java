/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.FeedbackEmployer;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FeedbackEmployerDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(FeedbackEmployer feedbackEmployer) {
        sessionFactory.getCurrentSession().saveOrUpdate(feedbackEmployer);
    }

    @Transactional
    public List<FeedbackEmployer> findByPage(PageType page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackEmployer> query = builder.createQuery(FeedbackEmployer.class);
        Root<FeedbackEmployer> root = query.from(FeedbackEmployer.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("page"), page));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<FeedbackEmployer> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional
    public void delete(FeedbackEmployer feedbackEmployer) {
        sessionFactory.getCurrentSession().delete(feedbackEmployer);
    }

    @Transactional(readOnly = true)
    public FeedbackEmployer getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackEmployer> query = builder.createQuery(FeedbackEmployer.class);
        Root<FeedbackEmployer> root = query.from(FeedbackEmployer.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<FeedbackEmployer> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
