/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.Grievance;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Repository
public class GrievanceDao {

    @Autowired private SessionFactory sessionFactory;

    private final int PAGE_SIZE = 25;

    @Transactional
    public void saveOrUpdate(Grievance grievance) {
        sessionFactory.getCurrentSession().saveOrUpdate(grievance);
    }

    @Transactional
    public List<Grievance> findAll() {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Grievance> query = builder.createQuery(Grievance.class);
        Root<Grievance> root = query.from(Grievance.class);
        query.orderBy(builder.desc(root.get("createdOn")));
        Query<Grievance> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional
    public List<Grievance> findByPage(String page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Grievance> query = builder.createQuery(Grievance.class);
        Root<Grievance> root = query.from(Grievance.class);
        query.orderBy(builder.desc(root.get("createdOn")));
        Query<Grievance> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        addPagination(q, page);
        return q.list();
    }

    private int getFirstResult(String page) {
        int pageNo = 1;
        if (StringUtils.isEmpty(page) == false) {
            pageNo = Integer.valueOf(String.valueOf(page)).intValue();
        }
        return PAGE_SIZE * (pageNo - 1);
    }

    private void addPagination(Query<Grievance> query, String page) {
        int firstResult = getFirstResult(page);
        query.setFirstResult(firstResult);
        query.setMaxResults(PAGE_SIZE);
    }

    public int getPAGE_SIZE() {
        return PAGE_SIZE;
    }
}
