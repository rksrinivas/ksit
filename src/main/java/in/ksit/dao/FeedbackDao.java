/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import in.ksit.entity.Feedback;

@Repository
public class FeedbackDao {

    @Autowired private SessionFactory sessionFactory;

    private final int PAGE_SIZE = 25;

    @Transactional
    public void saveOrUpdate(Feedback feedback) {
        sessionFactory.getCurrentSession().saveOrUpdate(feedback);
    }

    @Transactional
    public List<Feedback> findByPage(String page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Feedback> query = builder.createQuery(Feedback.class);
        Root<Feedback> root = query.from(Feedback.class);
        query.orderBy(builder.desc(root.get("createdOn")));
        Query<Feedback> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        addPagination(q, page);
        return q.list();
    }

    private int getFirstResult(String page) {
        int pageNo = 1;
        if (StringUtils.isEmpty(page) == false) {
            pageNo = Integer.valueOf(String.valueOf(page)).intValue();
        }
        return PAGE_SIZE * (pageNo - 1);
    }

    private void addPagination(Query<Feedback> query, String page) {
        int firstResult = getFirstResult(page);
        query.setFirstResult(firstResult);
        query.setMaxResults(PAGE_SIZE);
    }

    public int getPAGE_SIZE() {
        return PAGE_SIZE;
    }

    @Transactional(readOnly = true)
    public Feedback getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Feedback> query = builder.createQuery(Feedback.class);
        Root<Feedback> root = query.from(Feedback.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<Feedback> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }

    @Transactional
    public void delete(Feedback feedback) {
        sessionFactory.getCurrentSession().delete(feedback);
    }
}
