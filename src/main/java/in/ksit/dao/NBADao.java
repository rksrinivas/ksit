/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.NBA;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class NBADao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(NBA nba) {
        sessionFactory.getCurrentSession().saveOrUpdate(nba);
    }

    @Transactional(readOnly = true)
    public List<NBA> getByPage(PageType page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBA> query = builder.createQuery(NBA.class);
        Root<NBA> root = query.from(NBA.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("page"), page));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NBA> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public List<NBA> getAll() {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBA> query = builder.createQuery(NBA.class);
        Root<NBA> root = query.from(NBA.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NBA> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional
    public void delete(NBA nba) {
        sessionFactory.getCurrentSession().delete(nba);
    }

    @Transactional(readOnly = true)
    public NBA getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBA> query = builder.createQuery(NBA.class);
        Root<NBA> root = query.from(NBA.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<NBA> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
