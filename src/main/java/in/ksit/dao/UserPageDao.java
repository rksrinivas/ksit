/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.PageType;
import in.ksit.entity.UserPage;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserPageDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(UserPage userPage) {
        sessionFactory.getCurrentSession().saveOrUpdate(userPage);
    }
    
    @Transactional
    public void saveOrUpdate(List<UserPage> userPageList) {
    	for(UserPage userPage: userPageList) {
            sessionFactory.getCurrentSession().saveOrUpdate(userPage);
    	}
    }

    @Transactional(readOnly = true)
    public List<UserPage> getByPage(PageType page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<UserPage> query = builder.createQuery(UserPage.class);
        Root<UserPage> root = query.from(UserPage.class);
        query.select(root).where(builder.equal(root.get("page"), page));
        Query<UserPage> q = sessionFactory.getCurrentSession().createQuery(query);
        return q.list();
    }

    @Transactional
    public void deleteUserPage(UserPage userPage) {
        sessionFactory.getCurrentSession().delete(userPage);
    }
    
    @Transactional
    public void deleteUserPage(List<UserPage> userPageList) {
    	for(UserPage userPage: userPageList) {
            sessionFactory.getCurrentSession().delete(userPage);
    	}
    }
}
