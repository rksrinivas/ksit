/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.NBAPrevisit;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class NBAPrevisitDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(NBAPrevisit nbaPrevisit) {
        sessionFactory.getCurrentSession().saveOrUpdate(nbaPrevisit);
    }

    @Transactional(readOnly = true)
    public List<NBAPrevisit> getByPage(PageType page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBAPrevisit> query = builder.createQuery(NBAPrevisit.class);
        Root<NBAPrevisit> root = query.from(NBAPrevisit.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("page"), page));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NBAPrevisit> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public List<NBAPrevisit> getAll() {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBAPrevisit> query = builder.createQuery(NBAPrevisit.class);
        Root<NBAPrevisit> root = query.from(NBAPrevisit.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NBAPrevisit> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional
    public void delete(NBAPrevisit nbaPrevisit) {
        sessionFactory.getCurrentSession().delete(nbaPrevisit);
    }

    @Transactional(readOnly = true)
    public NBAPrevisit getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NBAPrevisit> query = builder.createQuery(NBAPrevisit.class);
        Root<NBAPrevisit> root = query.from(NBAPrevisit.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<NBAPrevisit> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
