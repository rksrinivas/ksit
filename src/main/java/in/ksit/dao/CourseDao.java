/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.Course;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Repository
public class CourseDao {

    private final int PAGE_SIZE = 25;

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(Course course) {
        sessionFactory.getCurrentSession().saveOrUpdate(course);
    }

    @Transactional
    public List<Course> findByPage(String page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Course> query = builder.createQuery(Course.class);
        Root<Course> root = query.from(Course.class);
        query.orderBy(builder.desc(root.get("createdOn")));
        Query<Course> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        addPagination(q, page);
        return q.list();
    }

    private int getFirstResult(String page) {
        int pageNo = 1;
        if (StringUtils.isEmpty(page) == false) {
            pageNo = Integer.valueOf(String.valueOf(page)).intValue();
        }
        return PAGE_SIZE * (pageNo - 1);
    }

    private void addPagination(Query<Course> query, String page) {
        int firstResult = getFirstResult(page);
        query.setFirstResult(firstResult);
        query.setMaxResults(PAGE_SIZE);
    }

    public int getPAGE_SIZE() {
        return PAGE_SIZE;
    }

    @Transactional(readOnly = true)
    public Course getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Course> query = builder.createQuery(Course.class);
        Root<Course> root = query.from(Course.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<Course> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }

    @Transactional
    public void delete(Course course) {
        sessionFactory.getCurrentSession().delete(course);
    }
}
