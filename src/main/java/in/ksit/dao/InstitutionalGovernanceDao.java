/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.ksit.entity.InstitutionalGovernance;

@Repository
public class InstitutionalGovernanceDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public void saveOrUpdate(InstitutionalGovernance institutionalGovernance) {
		sessionFactory.getCurrentSession().saveOrUpdate(institutionalGovernance);
	}

	@Transactional(readOnly = true)
	public List<InstitutionalGovernance> getAll() {
		CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
		CriteriaQuery<InstitutionalGovernance> query = builder.createQuery(InstitutionalGovernance.class);
		Root<InstitutionalGovernance> root = query.from(InstitutionalGovernance.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		query.select(root).where(predicates.toArray(new Predicate[] {}));
		Query<InstitutionalGovernance> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
		return q.list();
	}

	@Transactional
	public void delete(InstitutionalGovernance institutionalGovernance) {
		sessionFactory.getCurrentSession().delete(institutionalGovernance);
	}

	@Transactional(readOnly = true)
	public InstitutionalGovernance getById(String id) {
		CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
		CriteriaQuery<InstitutionalGovernance> query = builder.createQuery(InstitutionalGovernance.class);
		Root<InstitutionalGovernance> root = query.from(InstitutionalGovernance.class);
		query.select(root).where(builder.equal(root.get("id"), id));
		Query<InstitutionalGovernance> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
		return q.getSingleResult();
	}
}
