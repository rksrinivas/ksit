/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.ProgrammeData;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ProgrammeDataDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(ProgrammeData programmeData) {
        sessionFactory.getCurrentSession().saveOrUpdate(programmeData);
    }

    @Transactional(readOnly = true)
    public ProgrammeData getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<ProgrammeData> query = builder.createQuery(ProgrammeData.class);
        Root<ProgrammeData> root = query.from(ProgrammeData.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<ProgrammeData> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }

    @Transactional
    public void delete(ProgrammeData programmeData) {
        sessionFactory.getCurrentSession().delete(programmeData);
    }
}
