/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.ksit.entity.Visit;

@Repository
public class VisitDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public void saveOrUpdate(Visit visit) {
		sessionFactory.getCurrentSession().saveOrUpdate(visit);
	}

	@Transactional
	public Visit findByName(String name) {
		CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
		CriteriaQuery<Visit> query = builder.createQuery(Visit.class);
		Root<Visit> root = query.from(Visit.class);
		query.select(root).where(builder.equal(root.get("name"), name));
		Query<Visit> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
		return q.uniqueResult();
	}

	@Transactional(readOnly = true)
	public Visit getById(String id) {
		CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
		CriteriaQuery<Visit> query = builder.createQuery(Visit.class);
		Root<Visit> root = query.from(Visit.class);
		query.select(root).where(builder.equal(root.get("id"), id));
		Query<Visit> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
		return q.getSingleResult();
	}

	@Transactional
	public void delete(Visit visit) {
		sessionFactory.getCurrentSession().delete(visit);
	}
}
