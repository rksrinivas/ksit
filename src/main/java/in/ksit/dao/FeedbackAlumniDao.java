/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.FeedbackAlumni;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FeedbackAlumniDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(FeedbackAlumni feedbackAlumni) {
        sessionFactory.getCurrentSession().saveOrUpdate(feedbackAlumni);
    }

    @Transactional
    public void delete(FeedbackAlumni feedbackAlumni) {
        sessionFactory.getCurrentSession().delete(feedbackAlumni);
    }

    @Transactional
    public List<FeedbackAlumni> findByPage(PageType page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackAlumni> query = builder.createQuery(FeedbackAlumni.class);
        Root<FeedbackAlumni> root = query.from(FeedbackAlumni.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("page"), page));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<FeedbackAlumni> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public FeedbackAlumni getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<FeedbackAlumni> query = builder.createQuery(FeedbackAlumni.class);
        Root<FeedbackAlumni> root = query.from(FeedbackAlumni.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<FeedbackAlumni> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
