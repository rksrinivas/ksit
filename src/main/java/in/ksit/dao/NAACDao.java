/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.ksit.entity.NAAC;
import in.ksit.entity.PageType;

@Repository
public class NAACDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(NAAC naac) {
        sessionFactory.getCurrentSession().saveOrUpdate(naac);
    }

    @Transactional(readOnly = true)
    public List<NAAC> getByYear(Integer year) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NAAC> query = builder.createQuery(NAAC.class);
        Root<NAAC> root = query.from(NAAC.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("year"), year));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NAAC> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public List<NAAC> getAllByPage(PageType pageType) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NAAC> query = builder.createQuery(NAAC.class);
        Root<NAAC> root = query.from(NAAC.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("page"), pageType));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<NAAC> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional
    public void delete(NAAC naac) {
        sessionFactory.getCurrentSession().delete(naac);
    }

    @Transactional(readOnly = true)
    public NAAC getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<NAAC> query = builder.createQuery(NAAC.class);
        Root<NAAC> root = query.from(NAAC.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<NAAC> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
