/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.Programme;
import in.ksit.entity.Programme.PROGRAMME_TYPE;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ProgrammeDao {
    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(Programme programme) {
        sessionFactory.getCurrentSession().saveOrUpdate(programme);
    }

    @Transactional(readOnly = true)
    public List<Programme> getProgrammesByType(PROGRAMME_TYPE programmeType) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Programme> query = builder.createQuery(Programme.class);
        Root<Programme> root = query.from(Programme.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("programmeType"), programmeType));
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<Programme> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public Programme getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Programme> query = builder.createQuery(Programme.class);
        Root<Programme> root = query.from(Programme.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<Programme> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }

    @Transactional
    public void delete(Programme programme) {
        sessionFactory.getCurrentSession().delete(programme);
    }
}
