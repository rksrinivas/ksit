/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.Data;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DataDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(Data data) {
        sessionFactory.getCurrentSession().saveOrUpdate(data);
    }

    @Transactional(readOnly = true)
    public List<Data> getByPageAndFieldName(
            PageType page, FieldNameType fieldName, Boolean active) {

        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Data> query = builder.createQuery(Data.class);
        Root<Data> root = query.from(Data.class);
        query.orderBy(builder.desc(root.get("rank")));
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("hidden"), false));
        predicates.add(builder.equal(root.get("page"), page));
        predicates.add(builder.equal(root.get("fieldName"), fieldName));
        if (active != null) {
            predicates.add(builder.equal(root.get("active"), active));
        }
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<Data> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public Data getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Data> query = builder.createQuery(Data.class);
        Root<Data> root = query.from(Data.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<Data> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
