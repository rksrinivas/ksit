/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import in.ksit.entity.Placement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PlacementDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(Placement placement) {
        sessionFactory.getCurrentSession().saveOrUpdate(placement);
    }

    @Transactional(readOnly = true)
    public List<Placement> getByPageAndFieldName(
            PageType page, FieldNameType fieldName, Boolean active) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Placement> query = builder.createQuery(Placement.class);
        Root<Placement> root = query.from(Placement.class);
        query.orderBy(builder.desc(root.get("rank")));
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("hidden"), false));
        predicates.add(builder.equal(root.get("page"), page));
        predicates.add(builder.equal(root.get("fieldName"), fieldName));
        if (active != null) {
            predicates.add(builder.equal(root.get("active"), active));
        }
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<Placement> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public Placement getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Placement> query = builder.createQuery(Placement.class);
        Root<Placement> root = query.from(Placement.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<Placement> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
