/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import in.ksit.entity.Faculty;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FacultyDao {

    @Autowired private SessionFactory sessionFactory;

    @Transactional
    public void saveOrUpdate(Faculty faculty) {
        sessionFactory.getCurrentSession().saveOrUpdate(faculty);
    }

    @Transactional(readOnly = true)
    public List<Faculty> getByPageAndFieldName(
            PageType page, FieldNameType fieldName, Boolean active) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Faculty> query = builder.createQuery(Faculty.class);
        Root<Faculty> root = query.from(Faculty.class);
        query.orderBy(builder.desc(root.get("rank")));
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("hidden"), false));
        predicates.add(builder.equal(root.get("page"), page));
        predicates.add(builder.equal(root.get("fieldName"), fieldName));
        if (active != null) {
            predicates.add(builder.equal(root.get("active"), active));
        }
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        Query<Faculty> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.list();
    }

    @Transactional(readOnly = true)
    public Faculty getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Faculty> query = builder.createQuery(Faculty.class);
        Root<Faculty> root = query.from(Faculty.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<Faculty> q = sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }
}
