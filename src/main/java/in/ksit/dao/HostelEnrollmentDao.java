/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import in.ksit.entity.HostelEnrollment;

@Repository
public class HostelEnrollmentDao {

    @Autowired private SessionFactory sessionFactory;

    private final int PAGE_SIZE = 25;

    @Transactional
    public void saveOrUpdate(HostelEnrollment hostelEnrollment) {
        sessionFactory.getCurrentSession().saveOrUpdate(hostelEnrollment);
    }

    @Transactional
    public List<HostelEnrollment> findByPage(String page) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<HostelEnrollment> query = builder.createQuery(HostelEnrollment.class);
        Root<HostelEnrollment> root = query.from(HostelEnrollment.class);
        query.orderBy(builder.desc(root.get("createdOn")));
        Query<HostelEnrollment> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        addPagination(q, page);
        return q.list();
    }

    private int getFirstResult(String page) {
        int pageNo = 1;
        if (StringUtils.isEmpty(page) == false) {
            pageNo = Integer.valueOf(String.valueOf(page)).intValue();
        }
        return PAGE_SIZE * (pageNo - 1);
    }

    private void addPagination(Query<HostelEnrollment> query, String page) {
        int firstResult = getFirstResult(page);
        query.setFirstResult(firstResult);
        query.setMaxResults(PAGE_SIZE);
    }

    public int getPAGE_SIZE() {
        return PAGE_SIZE;
    }

    @Transactional(readOnly = true)
    public HostelEnrollment getById(String id) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<HostelEnrollment> query = builder.createQuery(HostelEnrollment.class);
        Root<HostelEnrollment> root = query.from(HostelEnrollment.class);
        query.select(root).where(builder.equal(root.get("id"), id));
        Query<HostelEnrollment> q =
                sessionFactory.getCurrentSession().createQuery(query).setCacheable(true);
        return q.getSingleResult();
    }

    @Transactional
    public void delete(HostelEnrollment contactUs) {
        sessionFactory.getCurrentSession().delete(contactUs);
    }
}
