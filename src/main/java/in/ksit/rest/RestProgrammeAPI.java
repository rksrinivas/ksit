/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import in.ksit.entity.Programme;
import in.ksit.entity.Programme.PROGRAMME_TYPE;
import in.ksit.service.ProgrammeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/programme")
@RestController
public class RestProgrammeAPI {

    @Autowired ProgrammeService programmeService;

    @RequestMapping(
            value = "/addProgramme",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void addEvent(
            @RequestParam(name = "department", required = false) String department,
            @RequestParam(name = "programmeType", required = false) PROGRAMME_TYPE programmeType) {

        Programme programme = new Programme();
        programme.setDepartment(department);
        programme.setProgrammeType(programmeType);
        programmeService.saveOrUpdate(programme);
    }

    @RequestMapping(
            value = "/deleteProgrammeData",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public void deleteLabData(@RequestParam("id") String id) {

        Programme programme = programmeService.getById(id);
        programmeService.delete(programme);
    }
}
