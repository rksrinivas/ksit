/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import in.ksit.entity.Data;
import in.ksit.entity.DataType;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import in.ksit.entity.User;
import in.ksit.service.DataService;
import in.ksit.util.SessionUtil;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/data")
@RestController
public class RestDataAPI {

    @Autowired private DataService dataService;

    @RequestMapping(
            value = "/editData",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void editData(
            @RequestParam("id") String id,
            @RequestParam(name = "content", required = false) String content,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank) {
        Data data = dataService.getById(id);
        if (!StringUtils.isEmpty(content)) {
            data.setContent(content);
        }
        if (active != null) {
            data.setActive(active);
        }
        if (rank != null) {
            data.setRank(rank);
        }

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            data.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            data.setLastUpdatedBy("SYSTEM");
        }
        data.setLastUpdatedOn(Calendar.getInstance().getTime());

        dataService.saveOrUpdate(data);
    }

    @RequestMapping(
            value = "/addData",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void addData(
            @RequestParam("page") PageType page,
            @RequestParam("fieldName") FieldNameType fieldName,
            @RequestParam(name = "content", required = false) String content,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank) {

        Data data = new Data();
        data.setPage(page);
        data.setFieldName(fieldName);
        data.setContent(content);
        data.setActive(active);
        data.setRank(rank);
        data.setDataType(DataType.DATA);
        data.setHidden(false);

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            data.setCreatedBy(loggedInUser.getUsername());
            data.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            data.setCreatedBy("SYSTEM");
            data.setLastUpdatedBy("SYSTEM");
        }
        data.setCreatedOn(Calendar.getInstance().getTime());
        data.setLastUpdatedOn(Calendar.getInstance().getTime());

        dataService.saveOrUpdate(data);
    }

    @RequestMapping(
            value = "/deleteData",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public void deleteData(@RequestParam("id") String id) {

        Data data = dataService.getById(id);
        data.setHidden(true);
        dataService.saveOrUpdate(data);
    }
}
