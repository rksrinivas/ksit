/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import in.ksit.entity.DataType;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import in.ksit.entity.Placement;
import in.ksit.entity.User;
import in.ksit.service.PlacementService;
import in.ksit.util.AppUtils;
import in.ksit.util.SessionUtil;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/api/v1/placement")
@RestController
public class RestPlacementAPI {

    @Autowired private PlacementService placementService;

    @RequestMapping(
            value = "/editPlacement",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void editPlacement(
            @RequestParam("id") String id,
            @RequestParam(name = "company", required = false) String company,
            @RequestParam(name = "salary", required = false) String salary,
            @RequestParam(name = "noOfStudents", required = false) String noOfStudents,
            @RequestParam(name = "year", required = false) Integer year,
            @RequestParam(name = "program", required = false) String program,
            @RequestParam(name = "semester", required = false) Integer semester,
            @RequestParam(name = "schedule", required = false) String schedule,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank,
            @RequestParam(name = "img", required = false) MultipartFile img) {

        Placement placement = placementService.getById(id);
        if (!StringUtils.isEmpty(company)) {
            placement.setCompany(company);
        }
        if (!StringUtils.isEmpty(salary)) {
            placement.setSalary(salary);
        }

        if (noOfStudents != null) {
            placement.setNoOfStudents(noOfStudents);
        }

        if (year != null) {
            placement.setYear(year);
        }

        if (program != null) {
            placement.setProgram(program);
        }

        if (semester != null) {
            placement.setSemester(semester);
        }

        if (schedule != null) {
            placement.setSchedule(schedule);
        }

        if (active != null) {
            placement.setActive(active);
        }
        if (rank != null) {
            placement.setRank(rank);
        }
        String imageUrl = AppUtils.getFileUrl(img, "_image");
        if (imageUrl != null) {
            placement.setImageURL(imageUrl);
        }

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            placement.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            placement.setLastUpdatedBy("SYSTEM");
        }
        placement.setLastUpdatedOn(Calendar.getInstance().getTime());

        placementService.saveOrUpdate(placement);
    }

    @RequestMapping(
            value = "/addPlacement",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void addFaculty(
            @RequestParam("page") PageType page,
            @RequestParam("fieldName") FieldNameType fieldName,
            @RequestParam(name = "company", required = false) String company,
            @RequestParam(name = "salary", required = false) String salary,
            @RequestParam(name = "noOfStudents", required = false) String noOfStudents,
            @RequestParam(name = "year", required = false) Integer year,
            @RequestParam(name = "program", required = false) String program,
            @RequestParam(name = "semester", required = false) Integer semester,
            @RequestParam(name = "schedule", required = false) String schedule,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank,
            @RequestParam(name = "img", required = false) MultipartFile img) {

        Placement placement = new Placement();

        placement.setPage(page);
        placement.setFieldName(fieldName);
        placement.setCompany(company);
        placement.setSalary(salary);
        placement.setNoOfStudents(noOfStudents);
        placement.setYear(year);
        placement.setProgram(program);
        placement.setSemester(semester);
        placement.setSchedule(schedule);
        placement.setActive(active);
        placement.setRank(rank);
        placement.setDataType(DataType.PLACEMENT);
        placement.setHidden(false);

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            placement.setCreatedBy(loggedInUser.getUsername());
            placement.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            placement.setCreatedBy("SYSTEM");
            placement.setLastUpdatedBy("SYSTEM");
        }
        placement.setCreatedOn(Calendar.getInstance().getTime());
        placement.setLastUpdatedOn(Calendar.getInstance().getTime());

        String imageUrl = AppUtils.getFileUrl(img, "_image");
        if (imageUrl != null) {
            placement.setImageURL(imageUrl);
        }
        placementService.saveOrUpdate(placement);
    }

    @RequestMapping(
            value = "/deletePlacement",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public void deletePlacement(@RequestParam("id") String id) {

        Placement placement = placementService.getById(id);
        placement.setHidden(true);
        placementService.saveOrUpdate(placement);
    }
}
