/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import in.ksit.entity.Visit;
import in.ksit.service.VisitService;

@RequestMapping(value = "/api/v1/visit")
@RestController
public class RestVisitAPI {

	@Autowired
	private VisitService visitService;

	@RequestMapping(value = "/update/{name}/visits", method = RequestMethod.GET, produces = "application/json")
	public String updateVisits(@PathVariable(value = "name") String name,
			@RequestParam(value = "amount", required = true) Integer amount) {

		Visit visit = visitService.findByName(name);
		if (visit == null) {
			visit = new Visit();
			visit.setName(name);
			visit.setCount(amount);
		} else {
			visit.setCount(visit.getCount() + amount);
		}
		visitService.saveOrUpdate(visit);
		Map<String, Object> resultMap = new HashMap();
		resultMap.put("name", visit.getName());
		resultMap.put("value", visit.getCount());
		return new Gson().toJson(resultMap);
	}

	@RequestMapping(value = "/set/{name}/visits", method = RequestMethod.GET, produces = "application/json")
	public String setVisits(@PathVariable(value = "name") String name,
			@RequestParam(value = "amount", required = true) Integer amount) {

		Visit visit = visitService.findByName(name);
		if (visit == null) {
			visit = new Visit();
			visit.setName(name);
		}
		visit.setCount(amount);
		visitService.saveOrUpdate(visit);
		Map<String, Object> resultMap = new HashMap();
		resultMap.put("name", visit.getName());
		resultMap.put("value", visit.getCount());
		return new Gson().toJson(resultMap);
	}

}
