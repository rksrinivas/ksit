/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import in.ksit.entity.ContactUs;
import in.ksit.entity.Feedback;
import in.ksit.service.ContactUsService;
import in.ksit.service.FeedbackService;

@RequestMapping(value = "/api/v1/contactUs")
@RestController
public class RestContactUsAPI {

    @Autowired private ContactUsService contactUsService;
    
    @Autowired private FeedbackService feedbackService;


    @RequestMapping(
            value = "/deleteContactUs",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public Response deleteContactUs(@RequestParam("id") String id) {

        ContactUs contactUs = contactUsService.getById(id);
        contactUsService.delete(contactUs);
        return Response.ok().build();
    }
    
    
    @RequestMapping(
            value = "/deleteFeedback",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public Response deleteFeedback(@RequestParam("id") String id) {

        Feedback feedback = feedbackService.getById(id);
        feedbackService.delete(feedback);
        return Response.ok().build();
    }
}
