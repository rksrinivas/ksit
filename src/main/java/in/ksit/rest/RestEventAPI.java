/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import in.ksit.entity.DataType;
import in.ksit.entity.Event;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import in.ksit.entity.User;
import in.ksit.service.EventService;
import in.ksit.util.AppUtils;
import in.ksit.util.SessionUtil;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/api/v1/event")
@RestController
public class RestEventAPI {

    @Autowired private EventService eventService;

    @RequestMapping(
            value = "/editEvent",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void editEvent(
            @RequestParam("id") String id,
            @RequestParam(name = "heading", required = false) String heading,
            @RequestParam(name = "content", required = false) String content,
            @RequestParam(name = "eventDate", required = false) String eventDate,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank,
            @RequestParam(name = "img", required = false) MultipartFile img,
            @RequestParam(name = "report", required = false) MultipartFile report) {

        Event event = eventService.getById(id);
        if (!StringUtils.isEmpty(content)) {
            event.setContent(content);
        }
        if (!StringUtils.isEmpty(heading)) {
            event.setHeading(heading);
        }
        if (!StringUtils.isEmpty(eventDate)) {
            event.setEventDate(eventDate);
        }
        if (active != null) {
            event.setActive(active);
        }
        if (rank != null) {
            event.setRank(rank);
        }

        String imageUrl = AppUtils.getFileUrl(img, "_image");
        if (imageUrl != null) {
            event.setImageURL(imageUrl);
        }

        String reportUrl = AppUtils.getFileUrl(report, "_report");
        if (reportUrl != null) {
            event.setReportURL(reportUrl);
        }

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            event.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            event.setLastUpdatedBy("SYSTEM");
        }
        event.setLastUpdatedOn(Calendar.getInstance().getTime());

        eventService.saveOrUpdate(event);
    }

    @RequestMapping(
            value = "/addEvent",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void addEvent(
            @RequestParam("page") PageType page,
            @RequestParam("fieldName") FieldNameType fieldName,
            @RequestParam(name = "heading", required = false) String heading,
            @RequestParam(name = "content", required = false) String content,
            @RequestParam(name = "eventDate", required = false) String eventDate,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank,
            @RequestParam(name = "img", required = false) MultipartFile img,
            @RequestParam(name = "report", required = false) MultipartFile report) {


        Event event = new Event();
        event.setPage(page);
        event.setFieldName(fieldName);
        event.setHeading(heading);
        event.setContent(content);
        event.setEventDate(eventDate);
        event.setActive(active);
        event.setRank(rank);
        event.setDataType(DataType.EVENT);
        event.setHidden(false);

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            event.setCreatedBy(loggedInUser.getUsername());
            event.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            event.setCreatedBy("SYSTEM");
            event.setLastUpdatedBy("SYSTEM");
        }
        event.setCreatedOn(Calendar.getInstance().getTime());
        event.setLastUpdatedOn(Calendar.getInstance().getTime());

        String imageUrl = AppUtils.getFileUrl(img, "_image");
        if (imageUrl != null) {
            event.setImageURL(imageUrl);
        }

        String reportUrl = AppUtils.getFileUrl(report, "_report");
        if (reportUrl != null) {
            event.setReportURL(reportUrl);
        }
        eventService.saveOrUpdate(event);
    }

    @RequestMapping(
            value = "/deleteEvent",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public void deleteEvent(@RequestParam("id") String id) {

        Event event = eventService.getById(id);
        event.setHidden(true);
        eventService.saveOrUpdate(event);
    }
}
