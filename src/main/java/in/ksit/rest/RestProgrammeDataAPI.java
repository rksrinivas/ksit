/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import in.ksit.entity.Programme;
import in.ksit.entity.ProgrammeData;
import in.ksit.entity.ProgrammeData.PROGRAMME_DATA_TYPE;
import in.ksit.service.ProgrammeDataService;
import in.ksit.service.ProgrammeService;
import in.ksit.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/api/v1/programmeData")
@RestController
public class RestProgrammeDataAPI {

    @Autowired private ProgrammeDataService programmeDataService;

    @Autowired private ProgrammeService programmeService;

    @RequestMapping(
            value = "/add",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void addSyllabus(
            @RequestParam(name = "programmeId", required = false) String programmeId,
            @RequestParam(name = "programmeDataType", required = false)
                    PROGRAMME_DATA_TYPE programmeDataType,
            @RequestParam(name = "semester", required = false) Integer semester,
            @RequestParam(name = "startYear", required = false) Integer startYear,
            @RequestParam(name = "endYear", required = false) Integer endYear,
            @RequestParam(name = "programmeData", required = false)
                    MultipartFile programmeDataFile) {

        Programme programme = programmeService.getById(programmeId);
        ProgrammeData programmeData = new ProgrammeData();
        programmeData.setProgrammeDataType(programmeDataType);
        programmeData.setProgramme(programme);
        programmeData.setSemester(semester);
        programmeData.setStartYear(startYear);
        programmeData.setEndYear(endYear);
        String imageUrl = AppUtils.getFileUrl(programmeDataFile, "_image");
        if (imageUrl != null) {
            programmeData.setData(imageUrl);
        }
        programmeDataService.saveOrUpdate(programmeData);
    }

    @RequestMapping(
            value = "/delete",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public void deleteSyllabusData(@RequestParam("id") String id) {

        ProgrammeData programmeData = programmeDataService.getById(id);
        programmeDataService.delete(programmeData);
    }
}
