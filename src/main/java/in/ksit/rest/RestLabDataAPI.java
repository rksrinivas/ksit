/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import com.google.gson.Gson;
import in.ksit.entity.DataType;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.LabData;
import in.ksit.entity.LabDataInternal;
import in.ksit.entity.PageType;
import in.ksit.entity.User;
import in.ksit.service.LabDataService;
import in.ksit.util.AppUtils;
import in.ksit.util.SessionUtil;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/api/v1/lab")
@RestController
public class RestLabDataAPI {

    @Autowired private LabDataService labDataService;

    @RequestMapping(
            value = "/addLabData",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void addLabData(
            @RequestParam("page") PageType page,
            @RequestParam("fieldName") FieldNameType fieldName,
            @RequestParam(name = "inputData", required = false) String inputData,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank,
            @RequestParam(name = "img", required = false) MultipartFile img) {

        LabDataInternal labDataInternal = new Gson().fromJson(inputData, LabDataInternal.class);

        String imageUrl = AppUtils.getFileUrl(img, "_image");
        if (imageUrl != null) {
            labDataInternal.setImageURL(imageUrl);
        }
        String labDataInternalStr = new Gson().toJson(labDataInternal);
        LabData labData = new LabData();
        labData.setData(labDataInternalStr);

        labData.setPage(page);
        labData.setFieldName(fieldName);
        labData.setRank(rank);
        labData.setActive(active);
        labData.setDataType(DataType.LAB);
        labData.setHidden(false);

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            labData.setCreatedBy(loggedInUser.getUsername());
            labData.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            labData.setCreatedBy("SYSTEM");
            labData.setLastUpdatedBy("SYSTEM");
        }
        labData.setCreatedOn(Calendar.getInstance().getTime());
        labData.setLastUpdatedOn(Calendar.getInstance().getTime());

        labDataService.saveOrUpdate(labData);
    }

    @RequestMapping(
            value = "/deleteLabData",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public void deleteLabData(@RequestParam("id") String id) {

        LabData labData = labDataService.getById(id);
        labData.setHidden(true);
        labDataService.saveOrUpdate(labData);
    }
}
