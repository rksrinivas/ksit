/* (C)Copyright Rapsol Technologies (C) 2024 */
package in.ksit.rest;

import in.ksit.entity.DataType;
import in.ksit.entity.Faculty;
import in.ksit.entity.FieldNameType;
import in.ksit.entity.PageType;
import in.ksit.entity.User;
import in.ksit.service.FacultyService;
import in.ksit.util.AppUtils;
import in.ksit.util.SessionUtil;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/api/v1/faculty")
@RestController
public class RestFacultyAPI {

    @Autowired private FacultyService facultyService;

    @RequestMapping(
            value = "/editFaculty",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void editFaculty(
            @RequestParam("id") String id,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "designation", required = false) String designation,
            @RequestParam(name = "qualification", required = false) String qualification,
            @RequestParam(name = "department", required = false) String department,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank,
            @RequestParam(name = "img", required = false) MultipartFile img,
            @RequestParam(name = "profile", required = false) MultipartFile profile) {

        Faculty faculty = facultyService.getById(id);
        if (!StringUtils.isEmpty(name)) {
            faculty.setName(name);
        }
        if (!StringUtils.isEmpty(designation)) {
            faculty.setDesignation(designation);
        }
        if (!StringUtils.isEmpty(qualification)) {
            faculty.setQualification(qualification);
        }
        if (!StringUtils.isEmpty(department)) {
            faculty.setDepartment(department);
        }

        if (active != null) {
            faculty.setActive(active);
        }
        if (rank != null) {
            faculty.setRank(rank);
        }
        String imageUrl = AppUtils.getFileUrl(img, "_image");
        if (imageUrl != null) {
            faculty.setImageURL(imageUrl);
        }

        String profileUrl = AppUtils.getFileUrl(profile, "_profile");
        if (profileUrl != null) {
            faculty.setProfileURL(profileUrl);
        }

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            faculty.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            faculty.setLastUpdatedBy("SYSTEM");
        }
        faculty.setLastUpdatedOn(Calendar.getInstance().getTime());

        facultyService.saveOrUpdate(faculty);
    }

    @RequestMapping(
            value = "/addFaculty",
            method = RequestMethod.POST,
            consumes = "multipart/form-data",
            produces = "application/json")
    public void addFaculty(
            @RequestParam("page") PageType page,
            @RequestParam("fieldName") FieldNameType fieldName,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "designation", required = false) String designation,
            @RequestParam(name = "qualification", required = false) String qualification,
            @RequestParam(name = "department", required = false) String department,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(name = "rank", required = false) Integer rank,
            @RequestParam(name = "img", required = false) MultipartFile img,
            @RequestParam(name = "profile", required = false) MultipartFile profile) {

        Faculty faculty = new Faculty();

        faculty.setPage(page);
        faculty.setFieldName(fieldName);
        faculty.setName(name);
        faculty.setDesignation(designation);
        faculty.setQualification(qualification);
        faculty.setDepartment(department);
        faculty.setActive(active);
        faculty.setRank(rank);
        faculty.setDataType(DataType.FACULTY);
        faculty.setHidden(false);

        User loggedInUser = SessionUtil.getLoggedInUser();
        if (loggedInUser != null) {
            faculty.setCreatedBy(loggedInUser.getUsername());
            faculty.setLastUpdatedBy(loggedInUser.getUsername());
        } else {
            faculty.setCreatedBy("SYSTEM");
            faculty.setLastUpdatedBy("SYSTEM");
        }
        faculty.setCreatedOn(Calendar.getInstance().getTime());
        faculty.setLastUpdatedOn(Calendar.getInstance().getTime());

        String imageUrl = AppUtils.getFileUrl(img, "_image");
        if (imageUrl != null) {
            faculty.setImageURL(imageUrl);
        }

        String profileUrl = AppUtils.getFileUrl(profile, "_profile");
        if (profileUrl != null) {
            faculty.setProfileURL(profileUrl);
        }

        facultyService.saveOrUpdate(faculty);
    }

    @RequestMapping(
            value = "/deleteFaculty",
            method = RequestMethod.POST,
            consumes = "application/x-www-form-urlencoded",
            produces = "application/json")
    public void deleteFaculty(@RequestParam("id") String id) {

        Faculty faculty = facultyService.getById(id);
        faculty.setHidden(true);
        facultyService.saveOrUpdate(faculty);
    }
}
