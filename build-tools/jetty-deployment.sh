#!/bin/bash

get_total_memory () {
	echo $(free -m |grep 'Mem:' |awk '{print $2}')
}

get_max_memory () {
	total=$(get_total_memory)
	echo $(expr $(expr $total \* 1) / 2)
}

get_new_memory () {
	total=$(get_total_memory)
	echo $(expr $(expr $total \* 1) / 8)
}

#### JAVA_OPTS ####

JAVA_OPTS=""

## Memory settings
JAVA_OPTS="$JAVA_OPTS \
			-Xms$(get_max_memory)M -Xmx$(get_max_memory)M \
			-XX:NewSize=$(get_new_memory)M -XX:MaxNewSize=$(get_new_memory)M \
			-XX:PermSize=256M -XX:MaxPermSize=256M"

## GC settings
JAVA_OPTS="$JAVA_OPTS \
			-XX:+UseParNewGC \
			-XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled \
			-XX:+DisableExplicitGC"

## GC logging
JAVA_OPTS="$JAVA_OPTS \
			-XX:+PrintCommandLineFlags \
			-Xloggc:$LOG_DIR/gc.log -verbose:gc \
			-XX:+PrintGCDetails -XX:+PrintHeapAtGC \
			-XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps \
			-XX:+PrintTenuringDistribution"
			

function usage() {
    [ -n "$1" ] && echo "Error: $*" >&2
    echo "Usage: $0 <command>" >&2
    echo "     commands: help, deploy, status, start, oor, bir" >&2
    [ -n "$1" ] && exit 1
    exit 0
}

function kill_existing_process() {
	echo "kill_existing_process"
	pid=$(ps -aef| grep doorapp-services | grep -v grep | awk -F' ' '{print $2}')
	echo "PID found - "$pid
	if [ -n "$pid" ]; then echo "killing - $pid"; kill -s SIGTERM $pid;  fi
} 

function start_server() {
    sleep 5
    schema="$1"
	if [ -z "$1" ]
  		then
    		schema="validate"
	fi
	echo "Start server."
	exec java $JAVA_OPTS -jar -Djava.rmi.server.hostname=localhost -Denv=prod -Djava.security.egd=file:/dev/./urandom doorapp-services-0.0.1-SNAPSHOT.jar > /dev/null 2>&1 &
	echo "Disowning"
	disown -h
}

curuser=$(whoami)
USER=prod
CMD="$1"
case "$CMD" in
    help) usage;;
    start)
        [ "$curuser" == "$USER" ] || die "'start' command must be run as user $USER;"
       	kill_existing_process
		start_server $2
        ;;
	stop)
		kill_existing_process	;;
esac			
