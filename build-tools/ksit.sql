CREATE TABLE SPRING_SESSION (
	PRIMARY_ID CHAR(36) NOT NULL,
	SESSION_ID CHAR(36) NOT NULL,
	CREATION_TIME BIGINT NOT NULL,
	LAST_ACCESS_TIME BIGINT NOT NULL,
	MAX_INACTIVE_INTERVAL INT NOT NULL,
	EXPIRY_TIME BIGINT NOT NULL,
	PRINCIPAL_NAME VARCHAR(100),
	CONSTRAINT SPRING_SESSION_PK PRIMARY KEY (PRIMARY_ID)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC;

CREATE UNIQUE INDEX SPRING_SESSION_IX1 ON SPRING_SESSION (SESSION_ID);
CREATE INDEX SPRING_SESSION_IX2 ON SPRING_SESSION (EXPIRY_TIME);
CREATE INDEX SPRING_SESSION_IX3 ON SPRING_SESSION (PRINCIPAL_NAME);

CREATE TABLE SPRING_SESSION_ATTRIBUTES (
	SESSION_PRIMARY_ID CHAR(36) NOT NULL,
	ATTRIBUTE_NAME VARCHAR(200) NOT NULL,
	ATTRIBUTE_BYTES BLOB NOT NULL,
	CONSTRAINT SPRING_SESSION_ATTRIBUTES_PK PRIMARY KEY (SESSION_PRIMARY_ID, ATTRIBUTE_NAME),
	CONSTRAINT SPRING_SESSION_ATTRIBUTES_FK FOREIGN KEY (SESSION_PRIMARY_ID) REFERENCES SPRING_SESSION(PRIMARY_ID) ON DELETE CASCADE
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins` (
    `username` varchar(64) DEFAULT NULL,
    `series` varchar(64) NOT NULL,
    `token` varchar(64) DEFAULT NULL,
    `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`series`)
);

DROP TABLE IF EXISTS `alumni`;
CREATE TABLE IF NOT EXISTS alumni (
    id varchar(36) NOT NULL,
    name VARCHAR(36) DEFAULT NULL,
    course VARCHAR(36) DEFAULT NULL,
    branch VARCHAR(36) DEFAULT NULL,
    usn VARCHAR(36) DEFAULT NULL,
    year_of_passing VARCHAR(36) DEFAULT NULL,
    mobile_number VARCHAR(36) DEFAULT NULL,
    email_id VARCHAR(36) DEFAULT NULL,
    company_name VARCHAR(36) DEFAULT NULL,
    career_details VARCHAR(255) DEFAULT NULL,
    created_on datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `grievance`;
CREATE TABLE IF NOT EXISTS grievance (
    id varchar(36) NOT NULL,
    name VARCHAR(36) DEFAULT NULL,
    category VARCHAR(36) DEFAULT NULL,
    course VARCHAR(36) DEFAULT NULL,
    branch VARCHAR(36) DEFAULT NULL,
    usn VARCHAR(36) DEFAULT NULL,
    mobile_number VARCHAR(36) DEFAULT NULL,
    email_id VARCHAR(36) DEFAULT NULL,
    subject VARCHAR(36) DEFAULT NULL,
    description VARCHAR(36) DEFAULT NULL,
    created_on datetime DEFAULT NULL,
    PRIMARY KEY (id)
);


DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE IF NOT EXISTS contact_us (
    id varchar(36) NOT NULL,
    name VARCHAR(36) DEFAULT NULL,
    email VARCHAR(36) DEFAULT NULL,
    phone VARCHAR(36) DEFAULT NULL,
    subject VARCHAR(255) DEFAULT NULL,
    description VARCHAR(4096) DEFAULT NULL,
    created_on datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS feedback (
    id varchar(36) NOT NULL,
    name VARCHAR(36) DEFAULT NULL,
    email VARCHAR(36) DEFAULT NULL,
    phone VARCHAR(36) DEFAULT NULL,
    subject VARCHAR(255) DEFAULT NULL,
    description VARCHAR(4096) DEFAULT NULL,
    created_on datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `hostel_enrollment`;
CREATE TABLE IF NOT EXISTS hostel_enrollment (
    id varchar(36) NOT NULL,
    college_name VARCHAR(36) DEFAULT NULL,
    first_name VARCHAR(36) DEFAULT NULL,
    last_name VARCHAR(36) DEFAULT NULL,
    year VARCHAR(36) DEFAULT NULL,
    sem VARCHAR(36) DEFAULT NULL,
    phone VARCHAR(36) DEFAULT NULL,
    email VARCHAR(36) DEFAULT NULL,
    father_name VARCHAR(36) DEFAULT NULL,
    father_phone VARCHAR(36) DEFAULT NULL,
    mother_name VARCHAR(36) DEFAULT NULL,
    mother_phone VARCHAR(36) DEFAULT NULL,
    address VARCHAR(255) DEFAULT NULL,
    created_on datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
    `id` varchar(36) NOT NULL,
    `user_name` varchar(255) DEFAULT NULL,
    `password` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `User_Page`;
CREATE TABLE `User_Page` (
    `id` varchar(36) NOT NULL,
    `user_id` varchar(255) DEFAULT NULL,
    `page` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `base_data`;
CREATE TABLE IF NOT EXISTS base_data (
    id varchar(36) NOT NULL,
    field_name VARCHAR(255) DEFAULT NULL,
    page varchar(255) DEFAULT NULL,
    active bit(1) DEFAULT true,
    hidden bit(1) DEFAULT true,
    ranking int(11) DEFAULT NULL,
    data_type varchar(255) DEFAULT NULL,
    created_on datetime DEFAULT NULL,
    created_by varchar(255) DEFAULT NULL,
    last_updated_on datetime DEFAULT NULL,
    last_updated_by varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `data`;
    CREATE TABLE IF NOT EXISTS data (
    id varchar(36) NOT NULL,
    content VARCHAR(4096) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `event`;
    CREATE TABLE IF NOT EXISTS event (
    id varchar(36) NOT NULL,
    heading varchar(255) DEFAULT NULL,
    content VARCHAR(4096) DEFAULT NULL,
    event_date VARCHAR(255) DEFAULT NULL,
    image_url VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `placement`;
CREATE TABLE IF NOT EXISTS placement (
    id varchar(36) NOT NULL,
    company VARCHAR(255) DEFAULT NULL,
    salary VARCHAR(36) DEFAULT NULL,
    no_of_students VARCHAR(11) DEFAULT NULL,
    year int(11) DEFAULT NULL,
    program VARCHAR(255) DEFAULT NULL,
	semester int(11) DEFAULT NULL,
    schedule VARCHAR(36) DEFAULT NULL,
    image_url VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
);


DROP TABLE IF EXISTS `faculty`;
CREATE TABLE IF NOT EXISTS faculty (
    id varchar(36) NOT NULL,
    name varchar(255) NOT NULL,
    designation VARCHAR(4096) NOT NULL,
    qualification varchar(255) DEFAULT NULL,
    department VARCHAR(255) DEFAULT NULL,
    image_url VARCHAR(255) DEFAULT NULL,
    profile_url VARCHAR(255) DEFAULT NULL,
    
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `lab_data`;
CREATE TABLE IF NOT EXISTS lab_data (
    id varchar(36) NOT NULL,
    data text DEFAULT NULL,
    
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `programme_data`;
CREATE TABLE IF NOT EXISTS programme_data (
    id varchar(36) NOT NULL,
    programme_id varchar(36) NOT NULL,
   	programme_type varchar(36) NOT NULL,
    semester int(11) DEFAULT NULL,
    start_year int(11) DEFAULT NULL,
    end_year int(11) DEFAULT NULL,
    data varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `programme`;
CREATE TABLE IF NOT EXISTS programme (
    id varchar(36) NOT NULL,
    programme_type varchar(10) NOT NULL,
    department varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `Feedback_Parent`;
CREATE TABLE IF NOT EXISTS Feedback_Parent (
    id int NOT NULL AUTO_INCREMENT,
    page varchar(36) NOT NULL,
    nameOfTheParent VARCHAR(255) DEFAULT NULL,
    emailId VARCHAR(255) DEFAULT NULL,
    phoneNo VARCHAR(255) DEFAULT NULL,
    occupation VARCHAR(255) DEFAULT NULL,
    studentName VARCHAR(255) DEFAULT NULL,
    semesterAndSection VARCHAR(255) DEFAULT NULL,
    academicYear VARCHAR(255) DEFAULT NULL,
    usn VARCHAR(255) DEFAULT NULL,
    relationship VARCHAR(255) DEFAULT NULL,
    visionMission VARCHAR(255) DEFAULT NULL,
    visionAgree VARCHAR(255) DEFAULT NULL,
    missionAgree VARCHAR(255) DEFAULT NULL,
    visionSuggestion VARCHAR(4096) DEFAULT NULL,
    missionSuggestion VARCHAR(4096) DEFAULT NULL,
    peoSuggestion VARCHAR(4096) DEFAULT NULL,
    r1 varchar(36) DEFAULT NULL,
    r2 varchar(36) DEFAULT NULL,
    r3 varchar(36) DEFAULT NULL,
    r4 varchar(36) DEFAULT NULL,
    r5 varchar(36) DEFAULT NULL,
    r6 varchar(36) DEFAULT NULL,
    response varchar(4096) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `Feedback_Alumni`;
CREATE TABLE IF NOT EXISTS Feedback_Alumni (
    id int NOT NULL AUTO_INCREMENT,
    page varchar(36) NOT NULL,
    studentName VARCHAR(255) DEFAULT NULL,
    yearOfGraduation VARCHAR(255) DEFAULT NULL,
    positionInGraduation VARCHAR(255) DEFAULT NULL,
    usn VARCHAR(255) DEFAULT NULL,
    emailId VARCHAR(255) DEFAULT NULL,
    address VARCHAR(255) DEFAULT NULL,
    mobileNumber VARCHAR(255) DEFAULT NULL,
    currentOrganisation VARCHAR(255) DEFAULT NULL,
    designation VARCHAR(255) DEFAULT NULL,
    visionMission VARCHAR(255) DEFAULT NULL,
    visionAgree VARCHAR(255) DEFAULT NULL,
    missionAgree VARCHAR(255) DEFAULT NULL,
    visionSuggestion VARCHAR(4096) DEFAULT NULL,
    missionSuggestion VARCHAR(4096) DEFAULT NULL,
    peoSuggestion VARCHAR(4096) DEFAULT NULL,
    k_r1 varchar(36) DEFAULT NULL,
    k_r2 varchar(36) DEFAULT NULL,
    k_r3 varchar(36) DEFAULT NULL,
    k_r4 varchar(36) DEFAULT NULL,
    k_r5 varchar(36) DEFAULT NULL,
    k_r6 varchar(36) DEFAULT NULL,
    c_r1 varchar(36) DEFAULT NULL,
    c_r2 varchar(36) DEFAULT NULL,
    c_r3 varchar(36) DEFAULT NULL,
    ip_r1 varchar(36) DEFAULT NULL,
    ip_r2 varchar(36) DEFAULT NULL,
    ip_r3 varchar(36) DEFAULT NULL,
    ip_r4 varchar(36) DEFAULT NULL,
    m_r1 varchar(36) DEFAULT NULL,
    m_r2 varchar(36) DEFAULT NULL,
    m_r3 varchar(36) DEFAULT NULL,
    response varchar(4096) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `Feedback_Student`;
CREATE TABLE IF NOT EXISTS Feedback_Student (
    id int NOT NULL AUTO_INCREMENT,
    page varchar(36) NOT NULL,
    nameOfTheStudent VARCHAR(255) DEFAULT NULL,
    semesterAndSection VARCHAR(255) DEFAULT NULL,
    academicYear VARCHAR(255) DEFAULT NULL,
    usn VARCHAR(255) DEFAULT NULL,
    emailId VARCHAR(255) DEFAULT NULL,
    phoneNo VARCHAR(255) DEFAULT NULL,
    visionMission VARCHAR(255) DEFAULT NULL,
    visionAgree VARCHAR(255) DEFAULT NULL,
    missionAgree VARCHAR(255) DEFAULT NULL,
    visionSuggestion VARCHAR(4096) DEFAULT NULL,
    missionSuggestion VARCHAR(4096) DEFAULT NULL,
    peoSuggestion VARCHAR(4096) DEFAULT NULL,
    r1 varchar(36) DEFAULT NULL,
    r2 varchar(36) DEFAULT NULL,
    r3 varchar(36) DEFAULT NULL,
    r4 varchar(36) DEFAULT NULL,
    r5 varchar(36) DEFAULT NULL,
    r6 varchar(36) DEFAULT NULL,
    r7 varchar(36) DEFAULT NULL,
    r8 varchar(36) DEFAULT NULL,
    r9 varchar(36) DEFAULT NULL,
    r10 varchar(36) DEFAULT NULL,
    r11 varchar(36) DEFAULT NULL,
    r12 varchar(36) DEFAULT NULL,
    r13 varchar(36) DEFAULT NULL,
    r14 varchar(36) DEFAULT NULL,
    response varchar(4096) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `Feedback_Employer`;
CREATE TABLE IF NOT EXISTS Feedback_Employer (
    id int NOT NULL AUTO_INCREMENT,
    page varchar(36) NOT NULL,
    name VARCHAR(255) DEFAULT NULL,
    companyName VARCHAR(255) DEFAULT NULL,
    designation VARCHAR(255) DEFAULT NULL,
    mobileNumber VARCHAR(255) DEFAULT NULL,
    emailId VARCHAR(255) DEFAULT NULL,
    visionMission VARCHAR(255) DEFAULT NULL,
    visionAgree VARCHAR(255) DEFAULT NULL,
    missionAgree VARCHAR(255) DEFAULT NULL,
    visionSuggestion VARCHAR(4096) DEFAULT NULL,
    missionSuggestion VARCHAR(4096) DEFAULT NULL,
    peoSuggestion VARCHAR(4096) DEFAULT NULL,
    r1 varchar(36) DEFAULT NULL,
    r2 varchar(36) DEFAULT NULL,
    r3 varchar(36) DEFAULT NULL,
    r4 varchar(36) DEFAULT NULL,
    r5 varchar(36) DEFAULT NULL,
    r6 varchar(36) DEFAULT NULL,
    r7 varchar(36) DEFAULT NULL,
    r8 varchar(36) DEFAULT NULL,
    response varchar(4096) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `nba`;
CREATE TABLE IF NOT EXISTS nba (
    id varchar(36) NOT NULL,
    page varchar(36) NOT NULL,
    criteria VARCHAR(255) DEFAULT NULL,
    heading VARCHAR(255) DEFAULT NULL,
    link VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `nba_previsit`;
CREATE TABLE IF NOT EXISTS nba_previsit (
    id varchar(36) NOT NULL,
    page varchar(36) NOT NULL,
    heading VARCHAR(255) DEFAULT NULL,
    description VARCHAR(255) DEFAULT NULL,
    link VARCHAR(255) DEFAULT NULL,
    ranking int(11) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS course (
    id varchar(36) NOT NULL,
    name VARCHAR(255) DEFAULT NULL,
    email VARCHAR(255) DEFAULT NULL,
    phone VARCHAR(255) DEFAULT NULL,
    course VARCHAR(255) DEFAULT NULL,
    address VARCHAR(4096) DEFAULT NULL,
    sslc DOUBLE DEFAULT NULL,
    puc DOUBLE DEFAULT NULL,
    created_on datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `naac`;
CREATE TABLE IF NOT EXISTS naac (
    id varchar(36) NOT NULL,
    year int(11) NOT NULL,
    criteria VARCHAR(255) DEFAULT NULL,
    heading VARCHAR(255) DEFAULT NULL,
    link VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `online_admission`;
CREATE TABLE `online_admission` (
  `id` varchar(255) NOT NULL,
  `admittedBranch` varchar(255) DEFAULT NULL,
  `candidateName` varchar(255) DEFAULT NULL,
  `caste` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `cetRank` varchar(255) DEFAULT NULL,
  `comedRank` varchar(255) DEFAULT NULL,
  `comedRegNo` varchar(255) DEFAULT NULL,
  `comedYear` varchar(255) DEFAULT NULL,
  `correspondenceAddress` varchar(1024) DEFAULT NULL,
  `correspondencePinCode` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `extraActivites` varchar(1024) DEFAULT NULL,
  `fathersAnnualIncome` varchar(255) DEFAULT NULL,
  `fathersEmail` varchar(255) DEFAULT NULL,
  `fathersMobileNo` varchar(255) DEFAULT NULL,
  `fathersName` varchar(255) DEFAULT NULL,
  `fathersOccupation` varchar(255) DEFAULT NULL,
  `foreignStudentsCertificateUrl` varchar(255) DEFAULT NULL,
  `guardiansMobileNo` varchar(255) DEFAULT NULL,
  `guardiansName` varchar(255) DEFAULT NULL,
  `jeeRank` varchar(255) DEFAULT NULL,
  `jeeRegNo` varchar(255) DEFAULT NULL,
  `jeeYear` varchar(255) DEFAULT NULL,
  `keaRank` varchar(255) DEFAULT NULL,
  `keaRegNo` varchar(255) DEFAULT NULL,
  `keaYear` varchar(255) DEFAULT NULL,
  `mathsMarks` varchar(255) DEFAULT NULL,
  `mathsMax` varchar(255) DEFAULT NULL,
  `migrationCertificateUrl` varchar(255) DEFAULT NULL,
  `mobileNo` varchar(255) DEFAULT NULL,
  `mothersAnnualIncome` varchar(255) DEFAULT NULL,
  `mothersEmail` varchar(255) DEFAULT NULL,
  `mothersMobileNo` varchar(255) DEFAULT NULL,
  `mothersName` varchar(255) DEFAULT NULL,
  `mothersOccupation` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `otherMarks` varchar(255) DEFAULT NULL,
  `otherMax` varchar(255) DEFAULT NULL,
  `othersRank` varchar(255) DEFAULT NULL,
  `othersRegNo` varchar(255) DEFAULT NULL,
  `othersYear` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `permanentAddress` varchar(1024) DEFAULT NULL,
  `permanentPinCode` varchar(255) DEFAULT NULL,
  `photoUrl` varchar(255) DEFAULT NULL,
  `physicalFitnessCertificateUrl` varchar(255) DEFAULT NULL,
  `physicsMarks` varchar(255) DEFAULT NULL,
  `physicsMax` varchar(255) DEFAULT NULL,
  `prevDateOfLeaving` varchar(255) DEFAULT NULL,
  `prevInstitution` varchar(255) DEFAULT NULL,
  `prevInstitutionBoard` varchar(255) DEFAULT NULL,
  `prevPassingYear` varchar(255) DEFAULT NULL,
  `prevRegisterNo` varchar(255) DEFAULT NULL,
  `pucMarksCardUrl` varchar(255) DEFAULT NULL,
  `rankCertificateUrl` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `screenshotUrl` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `sslcMarksCardUrl` varchar(255) DEFAULT NULL,
  `totalMarks` varchar(255) DEFAULT NULL,
  `totalMax` varchar(255) DEFAULT NULL,
  `transactionNo` varchar(255) DEFAULT NULL,
  `transferCertificateUrl` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `User` (`id`, `user_name`, `password`)
VALUES
('4028abdd61f705040161f70fe90d0001', 'admin', '$2a$10$sRffD6bWtw6jea3FXGylS.cs5oLEWFu.E.sVCOAmNA75J5fAZU6Oa');

DROP TABLE IF EXISTS `institutional_governance`;
CREATE TABLE `institutional_governance` (
    `id` varchar(255) NOT NULL,
    `heading` varchar(255) NOT NULL,
    `link` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `naac`;
CREATE TABLE IF NOT EXISTS naac (
    id varchar(36) NOT NULL,
    year int(11) NOT NULL,
    criteria VARCHAR(255) DEFAULT NULL,
    heading VARCHAR(255) DEFAULT NULL,
    link VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS `visit`;
CREATE TABLE IF NOT EXISTS visit (
    id varchar(36) NOT NULL,
    count int(11) NOT NULL,
    name VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
);

INSERT INTO `User_Page` (`id`, `user_id`, `page`)
VALUES
	('40288083737cab7601737cac5b3d0000', '4028abdd61f705040161f70fe90d0001', 'mech_page'),
	('40288083737cab7601737cac5b4e0001', '4028abdd61f705040161f70fe90d0001', 'sports_page'),
	('40288083737cab7601737cac5b5a0002', '4028abdd61f705040161f70fe90d0001', 'home_page'),
	('40288083737cab7601737cac5b6b0003', '4028abdd61f705040161f70fe90d0001', 'cse_page'),
	('40288083737cab7601737cac5b760004', '4028abdd61f705040161f70fe90d0001', 'ece_page'),
	('40288083737cab7601737cac5b840005', '4028abdd61f705040161f70fe90d0001', 'permissions_page'),
	('40288083737cab7601737cac5b920006', '4028abdd61f705040161f70fe90d0001', 'placement_page'),
	('40288083737cab7601737cac5b9f0007', '4028abdd61f705040161f70fe90d0001', 'science_page'),
	('40288083737cab7601737cac5bab0008', '4028abdd61f705040161f70fe90d0001', 'alumni_page'),
	('40288083737cab7601737cac5bb60009', '4028abdd61f705040161f70fe90d0001', 'transport_page'),
	('40288083737cab7601737cac5bc1000a', '4028abdd61f705040161f70fe90d0001', 'contact_us_page'),
	('40288083737cab7601737cac5bcf000b', '4028abdd61f705040161f70fe90d0001', 'library_page'),
	('40288083737cab7601737cac5bdb000c', '4028abdd61f705040161f70fe90d0001', 'tele_page'),
	('40288083737cab7601737cac5be6000d', '4028abdd61f705040161f70fe90d0001', 'course_page'),
	('40288083737cab7601737cac5bf1000e', '4028abdd61f705040161f70fe90d0001', 'about_page'),
	('40288083737cab7601737cac5bfc000f', '4028abdd61f705040161f70fe90d0001', 'administration_page'),
	('40288083737cab7601737cac5c080010', '4028abdd61f705040161f70fe90d0001', 'disciplinary_measures_page'),
	('40288083737cab7601737cac5c150011', '4028abdd61f705040161f70fe90d0001', 'nss_page'),
	('40288083737cab7601737cac5c210012', '4028abdd61f705040161f70fe90d0001', 'office_administration_page'),
	('40288083737cab7601737cac5c2c0013', '4028abdd61f705040161f70fe90d0001', 'public_press_page'),
	('40288083737cab7601737cac5c370014', '4028abdd61f705040161f70fe90d0001', 'ananya_page'),
	('40288083737cab7601737cac5c440015', '4028abdd61f705040161f70fe90d0001', 'fee_structure_page'),
	('40288083737cab7601737cac5c4f0016', '4028abdd61f705040161f70fe90d0001', 'academic_governing_council_page'),
	('40288083737cab7601737cac5c5a0017', '4028abdd61f705040161f70fe90d0001', 'grievance_page'),
	('40288083737cab7601737cac5c650018', '4028abdd61f705040161f70fe90d0001', 'academic_calender_page'),
	('40288083737cab7601737cac5c700019', '4028abdd61f705040161f70fe90d0001', 'redcross_page'),
	('40288083737cab7601737cac5c7e001a', '4028abdd61f705040161f70fe90d0001', 'newsletter_page'),
	('40288083737cab7601737cac5c8a001b', '4028abdd61f705040161f70fe90d0001', 'pg_page'),
	('40288083737cab7601737cac5c98001c', '4028abdd61f705040161f70fe90d0001', 'ug_page'),
	('40288083737cab7601737cac5ca3001d', '4028abdd61f705040161f70fe90d0001', 'nba_page'),
	('40288083737cab7601737cac5cad001e', '4028abdd61f705040161f70fe90d0001', 'naac_page');
