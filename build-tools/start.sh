#!/bin/bash

while true; do
	java -Xmx500m --add-opens java.base/java.lang=ALL-UNNAMED -jar -Denv=prod -Dspring.profiles.active=prod /home/ubuntu/ksit-0.0.1-SNAPSHOT.jar >> /var/log/ksit/app.log
	echo "Java program exited. Restarting..."
done
