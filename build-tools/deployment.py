"""NOTE:

    Code of deployer package has been modified to run this script.
    FILE: ....../site-packages/deployer/host/ssh.py:
        Method: start_interactive_shell

            ORIGINAL:

            if command:
                chan.send('exec %s\n' % command)
            ===============================

            MODIFIED:

            if command:
                chan.send('%s\n' % command)
"""

#!/usr/bin/env python
from deployer.client import start
from deployer.node import Node
from deployer.utils import esc1
from deployer.host import SSHHost
from deployer.host import LocalHost
import sys
import subprocess
import urllib
import time
import fileinput
import os
import shutil

class Jetty1(SSHHost):
    slug = 'jetty1'
    address = '10.160.0.8'
    username = 'prod'

class Jetty2(SSHHost):
    slug = 'jetty2'
    address = '10.160.0.15'
    username = 'prod'

class Nginx1(SSHHost):
    slug = 'nginx1'
    address = '10.160.0.14'
    username = 'prod'

def checkAvailability(host):
    """This methods checks for availability of the given node.

        It will/loop wait until server is Up.
    """
    print "Waiting for", host[0].slug, " server to start..."

    flag = True
    while flag:
        sys.stdout.write(".")
        time.sleep(2)
        try:
            status = urllib.urlopen("http://"+host[0].address +":8080/home").getcode()
            if  status == 200:
                print ""
                flag = False
            elif status == 503 or status == 500:
                print "Server Error", status, "has occured. Please fix it and start again."
                raise ValueError
        except IOError:
            sys.stdout.write(".")
    print host[0].slug, "server started"

class Deployment(Node):
    class Hosts:
        build = LocalHost
        jetty_servers = {Jetty1, Jetty2}
        balancers = {Nginx1}

    project_directory = '~'
    branch_name = 'master'
    code_directory = '/home/prod/repository/doorapp-services'
    no_nginx_location = '/tmp/nginx-config'
    repository = 'https://bitbucket.org/nobroker-repos/doorapp-services.git'


    def build(self):
        """Build target"""
        build = self.hosts.filter("build")
        with build.cd(self.code_directory, expand=True):
            build.run("git pull origin " + self.branch_name)
            print "Source code has been refreshed to latest"
            print "Now running mvn clean install"
            build.run("mvn clean install -DskipTests=true")
            print "Build executed successfully"

    def patch_server(self, server):
        """Copying jar to jetty server and patching it"""
        balancers = self.hosts.filter("balancers")
        jetty_servers = self.hosts.filter("jetty_servers")
        build = self.hosts.filter("build")
        self.remove_from_nginx(server.address)
        for balancer in balancers:
            balancer.put_file(self.no_nginx_location, "/etc/nginx/sites-available/default", True)
            balancer.run("sudo service nginx restart", use_sudo=True)
            print server.slug, "is off from " + balancer.slug

        print "Copying jar to the server"
        build.run("scp /home/prod/repository/doorapp-services/target/doorapp-services-0.0.1-SNAPSHOT.jar prod@" + server.address + ":/home/prod/.")
        print "Restarting the server"
        server.start_interactive_shell(command="~/jetty-deployment.sh start; sleep 3; tail -n 20 /var/log/stayeasy/app.log ;exit")

        checkAvailability(server)

        # putting back server to ngnix config file.
        self.add_to_nginx(server.address)

        for balancer in balancers:
            balancer.put_file("/home/prod/repository/doorapp-services/build-tools/nginx-config", "/etc/nginx/sites-available/default", True)
            balancer.run("sudo service nginx restart", use_sudo=True)
            print server.slug, "is added to " + balancer[0].slug

    def patch_a_server(self):
        name = raw_input("Enter server name: ")
        jetty_servers = self.hosts.filter("jetty_servers")
        for jetty_server in jetty_servers:
            if jetty_server.slug == name:
               self.patch_server(jetty_server)
               break;

    def remove_from_nginx(self, machine_ip):
	file_path = "/home/prod/repository/doorapp-services/build-tools/nginx-config"
	shutil.copy2( file_path, self.no_nginx_location);

	f = open(self.no_nginx_location, 'r+')
        search_string = "server "+ machine_ip
	replace_string = "##" + search_string

	for line in fileinput.input(file_path):
	    if search_string in line:
	        f.write(line.replace(search_string  ,replace_string))
	    else:
	        f.write(line);
	f.close()

    def add_to_nginx(self, machine_ip):
	file_path = "/home/prod/repository/doorapp-services/build-tools/nginx-config"
	shutil.copy2( file_path, self.no_nginx_location);

	f = open(self.no_nginx_location, 'r+')
	search_string = "##server "+ machine_ip
	replace_string = search_string[2:]

	for line in fileinput.input(file_path):
	    if search_string in line:
	        f.write(line.replace(search_string  ,replace_string))
	    else:
	        f.write(line);
	f.close()
    
if __name__ == '__main__':
    print '####### This is NoBroker Deployment script (01-08-2015 )##########'

    print ' start_servers               --- To deploy all ther server.'

    print ' --------------- OR ------------------------------'

    print ' build                       --- To prepare and test on pre-prod machine'
    print ' patch_a_server              --- Input: server name'

    start(Deployment)
